#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_ray_tracing : require

#include "../miscellaneous.hsl"

layout(location = 0) rayPayloadInEXT reflection_ray_payload_t reflection_ray_payload;

void main() {
}


