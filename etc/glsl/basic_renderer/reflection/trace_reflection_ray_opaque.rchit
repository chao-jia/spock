#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_ray_tracing : require

#include "../scene_bindings.hsl"

hitAttributeEXT vec2 attr;
layout(location = 0) rayPayloadInEXT reflection_ray_payload_t reflection_ray_payload;

void main() {
    const float s = 2.f * float(gl_HitKindEXT == gl_HitKindFrontFacingTriangleEXT) - 1.f; // negative if backface
    reflection_ray_payload.t = s * gl_RayTmaxEXT;
    reflection_ray_payload.bary_yz = attr;
    reflection_ray_payload.triangle_idx = gl_PrimitiveID + int(scene_offsets_of_mtl_types[gl_InstanceCustomIndexEXT].triangle_idx);
    reflection_ray_payload.hit_count += 1;
}


