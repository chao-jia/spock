#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_scalar_block_layout : require  // VK_EXT_scalar_block_layout: Promoted to Vulkan 1.2, no need to enable

#include "shared_definitions.h"

layout(set = 0, binding = 0, scalar) uniform ubo_frame_params_t { frame_params_t frame_params; };

layout(location = 0) out vec3 out_near_point;
layout(location = 1) out vec3 out_far_point;

vec2 plane[6] = vec2[](
    vec2(-1.f, -1.f), vec2(-1.f,  1.f), vec2( 1.f,  1.f),
    vec2(-1.f, -1.f), vec2( 1.f,  1.f), vec2( 1.f, -1.f) 
);

vec3 unproj(vec3 p0, mat4 inv_vp)
{
    vec4 p1 = inv_vp * vec4(p0, 1.f);
    return p1.xyz / p1.w;
}

void main() {
    vec2 p = plane[gl_VertexIndex];
    float ndc_far = frame_params.eye.w;
    float ndc_near = 1.f - ndc_far;
    out_near_point = unproj(vec3(p, ndc_near), frame_params.inv_vp);
    out_far_point =  unproj(vec3(p, ndc_far), frame_params.inv_vp);
    gl_Position = vec4(p, ndc_near, 1.f);
}

