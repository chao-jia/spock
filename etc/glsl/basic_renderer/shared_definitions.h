#if __VERSION__ == 110
#version 460
#endif
#ifndef SPOCK_COMMON_TYPES_H
#define SPOCK_COMMON_TYPES_H

#if __cplusplus
#include <glm/glm.hpp>
#endif

#define BLUE_NOISE_TEXTURE_COUNT 4

#define REFLECTION_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_X 16
#define REFLECTION_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_Y 16
#define REFLECTION_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_X 16
#define REFLECTION_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_Y 16
#define REFLECTION_A_TROUS_WORK_GROUP_SIZE_X 16
#define REFLECTION_A_TROUS_WORK_GROUP_SIZE_Y 16

#define SHADOW_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_X 16
#define SHADOW_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_Y 16
#define SHADOW_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_X 16
#define SHADOW_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_Y 16
#define SHADOW_A_TROUS_WORK_GROUP_SIZE_X 16
#define SHADOW_A_TROUS_WORK_GROUP_SIZE_Y 16

#define DDGI_UPDATE_DEPTH_WORK_GROUP_SIZE_X 16
#define DDGI_UPDATE_DEPTH_WORK_GROUP_SIZE_Y 16
#define DDGI_UPDATE_IRRADIANCE_WORK_GROUP_SIZE_X 16
#define DDGI_UPDATE_IRRADIANCE_WORK_GROUP_SIZE_Y 16

#define DDGI_PER_PROBE_RAY_COUNT_MAX 512

#define DDGI_PROBE_VIS_MAP_UV            1
#define DDGI_PROBE_VIS_MAP_DEPTH         2
#define DDGI_PROBE_VIS_MAP_DEPTH_SQ      4
#define DDGI_PROBE_VIS_MAP_IRRADIANCE    8
#define DDGI_PROBE_VIS_MAP_MASK         15
#define DDGI_PROBE_VIS_RAY_DIR_FLAG  16

#if __cplusplus
namespace glsl_shared {

using mat4 = glm::mat4;
using mat2x4 = glm::mat2x4;
using vec4 = glm::vec4;
using uvec2 = glm::uvec2;
using ivec2 = glm::ivec2;
using vec2 = glm::vec2;
using uint = uint32_t;

#endif


struct frame_params_t {
    mat4 view;
    mat4 inv_view;
    mat4 proj;
    mat4 vp;
    mat4 inv_vp;
    mat4 prev_vp;
    mat4 prev_inv_vp;
    mat4 t_gray_dot; // gray dot transform
    mat4 tn_gray_dot; // gray dot normal transform
    mat2x4 light_data; // [0]: xyz=position, w=packed_rgba; [1]:xy=oct_dir, z=soft_shadow_radius, w=intensity
    vec4 eye; // camera position, eye.w: ndc far(0.f or 1.f)
    vec4 prev_eye; // camera position, eye.w: ndc far(0.f or 1.f)
    vec4 background_color; // rgb: color, a: ndc far
    uvec2 viewport_dim;
    vec2 rcp_viewport_dim;
    uvec2 click_pixel_location;
    float animated_ms;
    uint render_frame_idx; // reset on scene changed, shader reloaded or screen resized

    float reflect_geometry_weight_factor;
    float reflect_accum_frame_count_max;
    float reflect_accum_base_power;
    float reflect_accum_curve;
    float reflect_temporal_weight_threshold;

    float svgf_temporal_alpha;
    float svgf_bilateral_rcp_sigma_d_sq; // pixel distance
    float svgf_bilateral_rcp_sigma_p_sq; // world position
    float svgf_bilateral_rcp_sigma_n_sq; // normal
    float svgf_edge_stop_rcp_sigma_p_sq; // world_position 
    float svgf_edge_stop_sigma_n;        // normal
    float svgf_edge_stop_sigma_l;        // luminance (shadow)

    int ddgi_is_first_frame;
    float ddgi_depth_sharpness;
    float ddgi_energy_preservation;
    float ddgi_probe_intensity;
    float ddgi_hysteresis;
    float ddgi_normal_bias;

    float ddgi_probe_vis_radius;
    uint ddgi_probe_vis_option; // DDGI_PROBE_VIS_NONE, DDGI_PROBE_VIS_DEPTH, DDGI_PROBE_VIS_DEPTH_SQ or DDGI_PROBE_VIS_IRRADIANCE

    vec4 ddgi_ray_dirs[DDGI_PER_PROBE_RAY_COUNT_MAX];
};

struct host_dev_msg_t {
    vec4 world_position; // xyz: world position
    ivec2 mtl_idx; // (mtl_type_idx, mtl_instance_idx)
};



#if __cplusplus
}
#endif

#endif // SPOCK_COMMON_TYPES_H

