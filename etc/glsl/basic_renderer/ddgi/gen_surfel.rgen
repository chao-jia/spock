#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_ray_tracing : require

#include "probe_params.hsl"

layout(set = 1, binding = 9) uniform accelerationStructureEXT tlas;
layout(set = 5, binding = 0, rgba32f) writeonly uniform image2D ddgi_surfel_image; // (ray distance, bary.yz, triangle_idx)

layout(location = 0) rayPayloadEXT ddgi_surfel_payload_t ddgi_surfel_payload;

void trace_surfel_ray(vec3 a_ray_origin, vec3 a_ray_dir, float a_t_max)
{
    uint flags = gl_RayFlagsOpaqueEXT; // disable anyhit, TODO: maybe fix for alpha
    float t_min = 0.001f;

    ddgi_surfel_payload.t = a_t_max;
    ddgi_surfel_payload.bary_yz = vec2(0.f);
    ddgi_surfel_payload.triangle_idx = -1;

    traceRayEXT(
        tlas,                       // topLevel
        flags,                      // rayFlags 
        0xFF,                       // cullMask
        0,                          // sbtRecordOffset
        0,                          // sbtRecordStride
        0,                          // missIndex
        a_ray_origin,               // origin
        t_min,                      // TMin
        a_ray_dir,                  // direction
        max(a_t_max, t_min),        // TMax
        0                           // payload
    );

}

// vkCmdTraceRaysKHR: (ray_count_per_probe * probe_grid_idx.x, probe_grid_idx.y, probe_grid_idx.z)
uint get_ray_dir_idx()
{
    const uint probe_in_grid_x_mask = (1u << ddgi_log2_per_probe_ray_count()) - 1;
    return gl_LaunchIDEXT.x & probe_in_grid_x_mask;
}

uvec3 get_probe_in_grid_idx()
{
    const uint probe_in_grid_idx_x = gl_LaunchIDEXT.x >> ddgi_log2_per_probe_ray_count();
    return uvec3(probe_in_grid_idx_x, gl_LaunchIDEXT.yz);
}


void main() {
    const uint ray_dir_idx = get_ray_dir_idx();
    const uvec3 probe_in_grid_idx = get_probe_in_grid_idx();
    const uint probe_idx = probe_idx_from_probe_in_grid_idx(probe_in_grid_idx);
    const uvec2 probe_in_image_idx = probe_in_image_idx_from_probe_idx(probe_idx);
    const uvec2 ray_in_surfel_idx = calc_ray_in_surfel_idx(probe_in_image_idx, ray_dir_idx);

    vec3 ray_dir = ray_dir_from_ray_dir_idx(ray_dir_idx);
    vec3 ray_origin = probe_position_from_probe_in_grid_idx(probe_in_grid_idx);
    trace_surfel_ray(ray_origin, ray_dir, SCENE_AABB_DIAG_LEN); //TODO: offset hit location along normal
    const vec4 hit_info = vec4(ddgi_surfel_payload.t, ddgi_surfel_payload.bary_yz, intBitsToFloat(ddgi_surfel_payload.triangle_idx));
    imageStore(ddgi_surfel_image, ivec2(ray_in_surfel_idx), hit_info);
}


