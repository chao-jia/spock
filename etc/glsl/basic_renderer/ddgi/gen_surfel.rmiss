#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_ray_tracing : require

#include "../scene_bindings.hsl"


layout(location = 0) rayPayloadInEXT ddgi_surfel_payload_t ddgi_surfel_payload;

void main() {
    ddgi_surfel_payload.triangle_idx = -1;
    ddgi_surfel_payload.t = INFINITE_FAR_DISTANCE;
}


