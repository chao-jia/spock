#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_shader_16bit_storage: require  // VK_KHR_16bit_storage: Promoted to Vulkan 1.1, no need to enable
#extension GL_EXT_scalar_block_layout : require  // VK_EXT_scalar_block_layout: Promoted to Vulkan 1.2, no need to enable
//#extension GL_EXT_control_flow_attributes: require
//#extension GL_EXT_nonuniform_qualifier: require

#include "lighting.hsl"

layout(set = 5, binding = 0, rgba32f) readonly uniform image2D ddgi_surfel_image; // (r, gb, oct_dir, ray dist)

#define PROBE_VIS_MAGENTA     vec3(1.00f, 0.08f, 0.27f)
#define PROBE_VIS_BLACK       vec3(0.00f, 0.00f, 0.00f)

layout(location = 0) flat in uvec2 probe_in_image_idx;
layout(location = 1) in vec3 probe_view_position;
layout(location = 0) out vec4 out_color;

#if defined(NEAREST_SAMPLE)
#define GET_DEPTH_MOMENTS   get_nearest_probe_depth_at_ray_dir
#define GET_RADIANCE        get_nearest_probe_radiance_at_ray_dir
#else
#define GET_DEPTH_MOMENTS   get_probe_depth_at_ray_dir
#define GET_RADIANCE        get_probe_radiance_at_ray_dir
#endif

void main() {
    const vec2 normal_xy = vec2(2.0, -2.0) * gl_PointCoord - vec2(1.0, -1.0);
    float mag = dot(normal_xy, normal_xy);
    if (mag > 1.f) discard;

    const vec3 view_normal = vec3(normal_xy, sqrt(1.0 - mag));
    const vec3 world_normal = vec3(frame_params.inv_view * vec4(view_normal, 0.f));
    vec4 frag_clip_position = frame_params.proj * vec4(probe_view_position + frame_params.ddgi_probe_vis_radius * view_normal, 1.f);
    gl_FragDepth = frag_clip_position.z / frag_clip_position.w;

    vec3 color = vec3(0.f);
    const vec2 oct_dir = oct_encode_dir(world_normal);
    const vec2 uv_in_probe = 0.5f * oct_dir + 0.5f;
    const uint ddgi_probe_vis_map_option = frame_params.ddgi_probe_vis_option & DDGI_PROBE_VIS_MAP_MASK;
    const bool ddgi_probe_vis_ray_normal = (frame_params.ddgi_probe_vis_option & DDGI_PROBE_VIS_RAY_DIR_FLAG) != 0;
    if (ddgi_probe_vis_map_option == DDGI_PROBE_VIS_MAP_DEPTH) {
        const vec2 depth_moments = GET_DEPTH_MOMENTS(probe_in_image_idx, uv_in_probe);
        const float depth = depth_moments.x;
        color = decode_srgb(vec3(depth * (1.f / SCENE_AABB_DIAG_LEN)));
    }
    else if (ddgi_probe_vis_map_option == DDGI_PROBE_VIS_MAP_DEPTH_SQ) {
        const vec2 depth_moments = GET_DEPTH_MOMENTS(probe_in_image_idx, uv_in_probe);
        const float depth_sq = depth_moments.y;
        color = decode_srgb(vec3(depth_sq * (1.f / (SCENE_AABB_DIAG_LEN * SCENE_AABB_DIAG_LEN))));
    }
    else if (ddgi_probe_vis_map_option == DDGI_PROBE_VIS_MAP_IRRADIANCE) {
        color = vec3(GET_RADIANCE(probe_in_image_idx, uv_in_probe));
    }
    else {
        const float half_uv_grid_dim = 7.f;
        vec3 oct_color = vec3(oct_dir, 0.f);
        if (oct_color.x > 0.f && oct_color.y > 0.f) 
            oct_color.z = 0.75f;
        if (oct_color.x > 0.f && oct_color.y < 0.f) 
            oct_color.z = 0.50f;
        if (oct_color.x < 0.f && oct_color.y > 0.f) 
            oct_color.z = 0.50f;
        if (oct_color.x < 0.f && oct_color.y < 0.f) 
            oct_color.z = 0.25f;
        if (world_normal.z > 0.f)
            oct_color.z = -1.f;
        
        vec2 clustered_oct_dir = floor(half_uv_grid_dim * oct_dir) / half_uv_grid_dim;
        vec2 diff = abs(clustered_oct_dir - oct_dir);
        float line_half_width = 0.04f;
        float grid_line_edge_sup = (1.f - line_half_width) / half_uv_grid_dim;
        float grid_line_edge_inf = line_half_width / half_uv_grid_dim;

        if (diff.x > grid_line_edge_sup || diff.x < grid_line_edge_inf) {
            oct_color += vec3(-1.f);
        }
        if (diff.y > grid_line_edge_sup || diff.y < grid_line_edge_inf){
            oct_color += vec3( 1.f);
        }
        color = normal_to_color(oct_color);
    }
    if (ddgi_probe_vis_ray_normal) {
        for (int i = 0; i < ddgi_per_probe_ray_count(); ++i) {
            const uvec2 ray_in_surfel_idx = calc_ray_in_surfel_idx(probe_in_image_idx, i);
            const vec3 ray_dir = ray_dir_from_ray_dir_idx(i);
            if (1.f - dot(ray_dir, world_normal) < 1e-3f) {
                const vec4 packed_surfel_data = imageLoad(ddgi_surfel_image, ivec2(ray_in_surfel_idx));
                float ray_distance = 0.f;
                vec3 surfel_radiance = vec3(0.f);
                vec3 surfel_normal = vec3(0.f);
                unpack_surfel_data(packed_surfel_data, surfel_radiance, surfel_normal, ray_distance);
                color = normal_to_color(surfel_normal);
                break;
            }
        }
    }
    out_color = vec4(color, 1.f);
}


