#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable

#include "probe_params.hsl"


layout(location = 0) out uvec2 probe_in_image_idx;
layout(location = 1) out vec3 probe_view_position;

// https://github.com/g-truc/glm/blob/0.9.5/glm/gtc/matrix_transform.inl#L208
// scene.hpp/.cpp: glm::mat4 perspective_reversed_z(float a_fovy, float a_aspect, float a_near_plane, float a_far_plane);
// scene.hpp/.cpp: void camera_t::update();
float cot_half_fovy(mat4 a_proj)
{
    return -a_proj[1][1];
}


float eye_to_near_plane_distance_in_pixel(float a_screen_height, mat4 a_proj)
{
    return (0.5f * a_screen_height) * cot_half_fovy(a_proj);
}


float probe_diameter_in_pixel(float a_probe_radius, float a_rcp_probe_eye_distance, float a_screen_height, mat4 a_proj)
{
    return eye_to_near_plane_distance_in_pixel(a_screen_height, a_proj) * 2.f * a_probe_radius * a_rcp_probe_eye_distance;
}


void main() {
    uvec3 probe_in_grid_idx = probe_in_grid_idx_from_probe_idx(gl_VertexIndex);
    vec3 probe_world_position = probe_position_from_probe_in_grid_idx(probe_in_grid_idx);
    probe_view_position = vec3(frame_params.view * vec4(probe_world_position, 1.f));
    float dist_to_eye = abs(probe_view_position.z) + 1e-6f;
    gl_PointSize = probe_diameter_in_pixel(frame_params.ddgi_probe_vis_radius, 1.f / dist_to_eye, float(frame_params.viewport_dim.y), frame_params.proj);
    probe_in_image_idx = probe_in_image_idx_from_probe_idx(gl_VertexIndex);
    gl_Position = frame_params.vp * vec4(probe_world_position, 1.f);

}

