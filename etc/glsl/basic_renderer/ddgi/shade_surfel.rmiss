#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_ray_tracing : require

#include "../miscellaneous.hsl"

layout(location = 0) rayPayloadInEXT shadow_ray_payload_t shadow_ray_payload;

void main() {
    shadow_ray_payload.hit = false;
}


