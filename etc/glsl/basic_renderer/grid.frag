#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_scalar_block_layout : require  // VK_EXT_scalar_block_layout: Promoted to Vulkan 1.2, no need to enable

#include "shared_definitions.h"

layout(set = 0, binding = 0, scalar) uniform ubo_frame_params_t { frame_params_t frame_params; };

layout(location = 0) in vec3 in_near_point;
layout(location = 1) in vec3 in_far_point;

layout(location = 0) out vec4 out_color;

float filter_width(vec2 p)
{
    vec2 w = max(abs(dFdx(p)), abs(dFdy(p)));
    return max(w.x, w.y);
}

vec2 bump(vec2 p)
{
    return floor( 0.5f * p) + 2.f * max(0.5f * p - floor(0.5f * p) - 0.5f, 0.f);
}


vec3 checker(vec2 world_pos, float cell_length)
{
    vec2 p = world_pos / cell_length;
    float width = filter_width(p);
    vec2 p0 = p - 0.5f * width;
    vec2 p1 = p + 0.5f * width;

    vec2 i = (bump(p1) - bump(p0)) / width;
    float c = i.x * i.y + (1.f - i.x) * (1.f - i.y);

    const float light = 0.9f;
    const float dark = 0.6f;
    return vec3(mix(dark, light, c));
}


void main() {
    const float plane_y = -0.05f;
    const float grid_cell_length = 2.f;

    const float Y = plane_y;
    float t = (Y - in_near_point.y) / (in_far_point.y - in_near_point.y);
    if (t < 0.f || frame_params.eye.y < Y) discard;
    vec3 world_pos = in_near_point + t * (in_far_point - in_near_point);
    vec3 view_pos = vec3(frame_params.view * vec4(world_pos, 1.f));
    vec4 clip_pos = frame_params.proj * frame_params.view * vec4(world_pos, 1.f);
    gl_FragDepth = clip_pos.z / clip_pos.w;

    const vec3 cell_color = checker(world_pos.xz, grid_cell_length);
    const float blur_factor = smoothstep(80.f, 120.f, -view_pos.z);
    out_color = vec4(mix(cell_color, vec3(0.75f), blur_factor), 1.f);

}

