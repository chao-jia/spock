#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_shader_16bit_storage: require  // VK_KHR_16bit_storage: Promoted to Vulkan 1.1, no need to enable
#extension GL_EXT_scalar_block_layout : require  // VK_EXT_scalar_block_layout: Promoted to Vulkan 1.2, no need to enable
//#extension GL_EXT_control_flow_attributes: require
//#extension GL_EXT_nonuniform_qualifier: require

#include "../scene_bindings.hsl"
#include "../ddgi/lighting.hsl"

layout(set = 2, binding = 1) uniform sampler2D g_motion_vec_tex; // rg16_snorm
layout(set = 2, binding = 2) uniform sampler2D g_base_color_metallic_tex; // rgba8 (rgb, metallic)
layout(set = 2, binding = 3) uniform sampler2D g_emissive_occlusion_tex; // rgba8 (rgb, occlusion)

layout(set = 3, binding = 0) uniform sampler2D g_depth_tex;
layout(set = 3, binding = 2) uniform sampler2D g_oct_normal_tex; // rg16_snorm (oct normal)
layout(set = 3, binding = 3) uniform sampler2D g_roughness_curvature_tex; // rg16f (roughness, curvature)

layout(set = 9, binding = 0, r16) uniform readonly image2D shadow_image; 
layout(set = 11, binding = 0, rgba16f) uniform readonly image2D reflection_traced_image; // rgb, traced ray distance
layout(set = 14, binding = 0, rgba16f) uniform readonly image2D reflection_color_image; 

layout(location = 0) out vec4 out_color;

void main() {
    const vec2 ndc = frag_coord_to_ndc(gl_FragCoord.xy, frame_params.rcp_viewport_dim);
    const ivec2 pixel_location = ivec2(gl_FragCoord.xy);
    const vec4 base_color_metallic = texelFetch(g_base_color_metallic_tex, pixel_location, 0);
    const vec3 base_color = vec3(base_color_metallic);
    const float metallic = base_color_metallic.w;
    const float roughness = texelFetch(g_roughness_curvature_tex, pixel_location, 0).r;
    const vec4 emissive_occlusion = texelFetch(g_emissive_occlusion_tex, pixel_location, 0);
    const vec3 emissive = vec3(emissive_occlusion);
    const float occlusion = emissive_occlusion.w;
    const vec3 N = oct_decode_dir(texelFetch(g_oct_normal_tex, pixel_location, 0).rg);
    const float depth = texelFetch(g_depth_tex, pixel_location, 0).r;
    const vec3 world_position = world_position_from_ndc(ndc, depth, frame_params.inv_vp);
    const vec3 V = normalize(vec3(frame_params.eye) - world_position);

    const float visibility = 1.f - imageLoad(shadow_image, pixel_location).r;
    const vec3 reflect_color = imageLoad(reflection_color_image, pixel_location).rgb;

    vec3 L, irradiance;
    float light_dist, soft_shadow_radius;
    eval_punctual_light_at(world_position, L, light_dist, irradiance, soft_shadow_radius);
    NVL_dots_t dots = calc_NVL_dots(N, V, L);

    vec3 c_diff = calc_primary_c_diff(base_color, metallic);
    vec3 f0 = calc_primary_f0(base_color, metallic);
    vec3 F = calc_primary_F(f0, dots.HdotV);
    vec3 f_diffuse = calc_primary_f_diffuse(F, c_diff);
    vec3 f_specular = calc_primary_f_specular(dots, F, roughness);
    vec3 color = max(dots.NdotL, 0.f) * irradiance * (f_diffuse + f_specular) * visibility + emissive;

    vec3 ddgi_irradiance = frame_params.ddgi_probe_intensity * eval_irradiance_field_at(world_position, N, V);
    vec3 reflect_dir = reflect(-V, N);
    NVL_dots_t r_dots = calc_NVL_dots(N, V, reflect_dir);
    vec3 r_F = calc_primary_F(f0, r_dots.HdotV);
    vec3 r_f_diffuse = calc_primary_f_diffuse(r_F, c_diff);
    vec3 r_f_specular = min(calc_primary_f_specular(r_dots, r_F, max(0.01, roughness)), 0.5f);
    color += ddgi_irradiance * (0.25 * F * roughness + f_diffuse) + reflect_color * max(dots.NdotV, 0.f) * r_f_specular;

    out_color = vec4(aces_film_tone_map(color), 1.f);
//    out_color = vec4(reflect_color, 1.f);
    gl_FragDepth = depth;
    
}


