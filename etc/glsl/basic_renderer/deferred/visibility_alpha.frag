#if __VERSION__ == 110
#version 460
#endif

#ifndef DEFERRED_VISIBILITY_ALPHA_FRAG
#define DEFERRED_VISIBILITY_ALPHA_FRAG

#extension GL_GOOGLE_include_directive: enable 

#include "visibility_primary.frag"

#endif


