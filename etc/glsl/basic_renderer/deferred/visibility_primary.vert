#if __VERSION__ == 110
#version 460
#endif

#ifndef DEFERRED_VISIBILITY_PRIMARY_VERT
#define DEFERRED_VISIBILITY_PRIMARY_VERT

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_scalar_block_layout : require  // VK_EXT_scalar_block_layout: Promoted to Vulkan 1.2, no need to enable

#include "../shared_definitions.h"

layout(set = 0, binding = 0, scalar) uniform ubo_frame_params_t { frame_params_t frame_params; };

layout(location = 0) in vec3 in_position;

#if defined(DEFERRED_VISIBILITY_ALPHA_VERT)
layout(location = 1) in vec2 in_uv;
layout(location = 0) out vec2 out_uv;
#endif

void main() {
    vec4 world_position = vec4(in_position, 1.f);

#if defined(DEFERRED_VISIBILITY_GRAY_DOT_VERT)
    world_position = frame_params.t_gray_dot * vec4(in_position, 1.f);

#elif defined(DEFERRED_VISIBILITY_ALPHA_VERT)
    out_uv = in_uv;
#endif

    gl_Position = frame_params.vp * world_position;
}


#endif
