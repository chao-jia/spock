#if __VERSION__ == 110
#version 460
#endif

#ifndef DEFERRED_VISIBILITY_PRIMARY_FRAG
#define DEFERRED_VISIBILITY_PRIMARY_FRAG

#extension GL_GOOGLE_include_directive: enable 

#include "../scene_bindings.hsl"

layout(location = 0) out int out_triangle_idx; // r32_sint

//#define DEFERRED_VISIBILITY_ALPHA_FRAG
//#define DEFERRED_VISIBILITY_GRAY_DOT_FRAG

#if defined(DEFERRED_VISIBILITY_ALPHA_FRAG)
#define SCENE_TRIANGLE_OFFSET       SCENE_TRIANGLE_OFFSET_ALPHA_CUTOFF
layout(location = 0) in vec2 in_uv;

#elif defined(DEFERRED_VISIBILITY_GRAY_DOT_FRAG)
#define SCENE_TRIANGLE_OFFSET       SCENE_TRIANGLE_OFFSET_GRAY_DOT
layout(early_fragment_tests) in;

#else // primary
#define SCENE_TRIANGLE_OFFSET       0
layout(early_fragment_tests) in;

#endif


void main() {
    const int triangle_idx = gl_PrimitiveID + SCENE_TRIANGLE_OFFSET;

#if defined(DEFERRED_VISIBILITY_ALPHA_FRAG)
    vec4 base_color;
    if (!test_alpha_cutoff(triangle_idx, in_uv, base_color)) {
        discard;
    }
    else {
        out_triangle_idx = triangle_idx;
    }

#else // defined(DEFERRED_VISIBILITY_ALPHA_FRAG)
    out_triangle_idx = triangle_idx;

#endif // defined(DEFERRED_VISIBILITY_ALPHA_FRAG)
}


#endif // DEFERRED_VISIBILITY_PRIMARY_FRAG


