#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_shader_16bit_storage: require  // VK_KHR_16bit_storage: Promoted to Vulkan 1.1, no need to enable
#extension GL_EXT_scalar_block_layout : require  // VK_EXT_scalar_block_layout: Promoted to Vulkan 1.2, no need to enable
//#extension GL_EXT_control_flow_attributes: require
//#extension GL_EXT_nonuniform_qualifier: require

#include "../scene_bindings.hsl"

layout(set = 3, binding = 1) uniform isampler2D g_triangle_idx_tex; // r32_sint 

layout(location = 0) out vec2 out_roughness_curvature; // rg16f: (roughness, curvature)
layout(location = 1) out vec2 out_motion_vec; // rg16_snorm
layout(location = 2) out vec4 out_base_color_metallic; // rgba8 (rgb, metallic)
layout(location = 3) out vec4 out_emissive_occlusion; // rgba8 (rgb, occlusion)
layout(location = 4) out float out_shadow_ray_origin_depth; // r32f
layout(location = 5) out vec2 out_oct_normal; // rg16_snorm (oct normal)

// TODO: as a subpass

void main() {
    const ivec2 pixel_location = ivec2(gl_FragCoord.xy);
    const int triangle_idx = texelFetch(g_triangle_idx_tex, pixel_location, 0).r;

    if (triangle_idx == -1) {
        // zero initialized by render pass, or unused (out_shadow_ray_origin_depth)
        discard;
        return;
//        out_roughness_curvature = vec2(0.f);
//        out_motion_vec = vec2(0.f);
//        out_base_color_metallic = vec4(0.f);
//        out_emissive_occlusion = vec4(0.f);
//        out_oct_normal = vec2(0.f);
//        out_shadow_ray_origin_depth = frame_params.eye.w;
    }
 
    const ivec2 mtl_idx = mtl_type_and_instance_idx(uint(triangle_mtl_idx_buffer[triangle_idx]));
    vec2 ndc = frag_coord_to_ndc(gl_FragCoord.xy, frame_params.rcp_viewport_dim);
    shading_info_t shading_info = shading_info_from_triangle_interpolation(triangle_idx, ndc, frame_params.eye.xyz);

    vec4 prev_clip_pos = frame_params.prev_vp * vec4(shading_info.world_position, 1.f); // TODO: projected outside view frustum?
    vec2 prev_ndc = prev_clip_pos.xy * (1.f / prev_clip_pos.w);
    vec4 clip_shadow_ray_origin = frame_params.vp * vec4(shading_info.shadow_ray_origin, 1.f);

    out_roughness_curvature = vec2(shading_info.roughness, shading_info.curvature);
    out_motion_vec = 0.5f * (prev_ndc - ndc);
    out_base_color_metallic = vec4(shading_info.base_color, shading_info.metallic);
    out_emissive_occlusion = vec4(shading_info.emissive, shading_info.occlusion);
    out_oct_normal = oct_encode_dir(shading_info.N);
    out_shadow_ray_origin_depth = clip_shadow_ray_origin.z / clip_shadow_ray_origin.w;
}


