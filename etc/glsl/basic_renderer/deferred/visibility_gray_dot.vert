#if __VERSION__ == 110
#version 460
#endif

#ifndef DEFERRED_VISIBILITY_GRAY_DOT_VERT
#define DEFERRED_VISIBILITY_GRAY_DOT_VERT

#extension GL_GOOGLE_include_directive: enable
#include "visibility_primary.vert"

#endif


