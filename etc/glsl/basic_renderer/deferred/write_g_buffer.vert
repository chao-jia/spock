#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable
#extension GL_EXT_scalar_block_layout : require

#include "../shared_definitions.h"

layout(set = 0, binding = 0, scalar) uniform ubo_frame_params_t { frame_params_t frame_params; };

void main() {
    uint idx = 2 - gl_VertexIndex; // ppl front face: CCW
    vec2 uv = vec2((idx << 1) & 2, idx & 2);
    const float ndc_near = 1.f - frame_params.eye.w;
    gl_Position = vec4(uv * 2.f - vec2(1.f), ndc_near, 1.f);
}

