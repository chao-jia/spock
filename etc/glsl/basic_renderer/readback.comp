#if __VERSION__ == 110
#version 460
#endif

#extension GL_GOOGLE_include_directive: enable 
#include "scene_bindings.hsl"

layout(set = 0, binding = 1) buffer ssbo_host_dev_msg_t { host_dev_msg_t host_dev_msg; };

layout(set = 3, binding = 0) uniform sampler2D g_depth;
layout(set = 3, binding = 1) uniform isampler2D g_triangle_idx; // r32_sint

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

void write_empty_response(int ret)
{
    host_dev_msg_t msg;
    msg.mtl_idx = ivec2(ret, ret);
    msg.world_position = vec4(0.f);
    host_dev_msg = msg;
}

void main() {
    const uvec2 pixel_location = frame_params.click_pixel_location;
    const uvec2 viewport_dim = frame_params.viewport_dim;
    const uvec2 tid = gl_GlobalInvocationID.xy;

    if( tid.x != 0 || tid.y != 0) {
        return;
    }
    if((pixel_location.x >= viewport_dim.x) || (pixel_location.y >= viewport_dim.y)) {
        write_empty_response(-2);
        return;
    }
    vec2 frag_coord = vec2(pixel_location) + vec2(0.5f);

    vec2 ndc = frag_coord_to_ndc(frag_coord, frame_params.rcp_viewport_dim);
    const int triangle_idx = texelFetch(g_triangle_idx, ivec2(pixel_location), 0).r;

    if (triangle_idx == -1) {
        write_empty_response(-3);
        return;
    }
    host_dev_msg.mtl_idx = mtl_type_and_instance_idx(uint(triangle_mtl_idx_buffer[triangle_idx]));
    float curr_depth = 0.f;
    vec3 emissive_color = vec3(0.f);
    const float depth = texelFetch(g_depth, ivec2(pixel_location), 0).r;
    const vec3 world_position = world_position_from_ndc(frag_coord, depth, frame_params.inv_vp);
    host_dev_msg.world_position = vec4(world_position, 1.f);

}

