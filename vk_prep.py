import enum
import os, shutil, re, pathlib

class cpp_names:
    generated_file_stem = 'generated'
    inc_dir = 'spock'
    namespace = 'spock'
    feature_wrapper_type = 'physical_dev_feature2_wrapper_t'
    all_features_type = 'physical_dev_all_features2_t'
    all_features_get_feature_func_name_prefix = 'get_'
    all_features_wrap_feature_func_name = 'wrap_feature'
    str_feature_map_var = 'str_feature_map' 
    str_feature_map_type = f'std::unordered_map<std::string_view, {feature_wrapper_type}>'
    get_vk_object_type_func_name = 'get_vk_object_type'
    get_extension_name_func_name = 'get_extension_name'
    init_str_feature_map_func_name = 'init_str_feature_map'
    vk_type_to_str_func_name = 'vk_type_to_str'
    str_to_vk_enum_func_name_prefix = 'str_to_'
    vk_enum_to_str_func_name_prefix = 'str_from_'

    feature_extension_str_map_var = 'k_physical_dev_feature_extension_str_map'
    str_vk_enum_map_var_prefix = 'k_str_vk_enum_map_'
    vk_enum_str_map_var_prefix = 'k_vk_enum_str_map_'
    global_lookup_table_struct_name = 'spock_generated_lookup_table_t'
    global_lookup_table_var_name = 'spock_generated_lookup_table'

    vk_enum_underlying_type = 'int'
    feature_request_type = 'const std::map<std::string_view, bool>&'
    macro_impl_def = 'SPOCK_GENERATED_IMPLEMENTATION'
    indent1 = '    '
    indent2 = f'{indent1}{indent1}'
    indent3 = f'{indent2}{indent1}'
    indent4 = f'{indent2}{indent2}'


class pat:
    vk_header_patch_version_def = re.compile(r'^#define\s+VK_HEADER_VERSION\s+(\d+)') # group 1: header patch version
    vk_header_version_def = re.compile(r'^#define\s+VK_HEADER_VERSION_COMPLETE\s+VK_MAKE_API_VERSION\((\d+),\s+(\d+),\s+(\d+)') # group 1: variant; group 2: major; group 3: minor
    vk_version_def = re.compile(r'^#define\s+(VK_VERSION_(\d+)_(\d+))\s+1') # group 1: version string; group 2: major; group 3: minor

    macro_if = re.compile(r'^#if([n]?def)?\s+(.+)$') # group1: def/ndef; group 2: macro
    macro_endif = re.compile(r'^#endif')

    vk_ext_name_def = re.compile(r'^#define\s+VK_\w+_EXTENSION_NAME\s+"(\w+)"') # group 1: extension name
    vk_handle_name_def = re.compile(r'^(VK_DEFINE_NON_DISPATCHABLE_HANDLE|VK_DEFINE_HANDLE)\((\w+)\)') # group 1: macro; group 2: handle name 

    vk_struct_def = re.compile(r'^typedef\s+struct\s+(Vk\w+)') # group 1: struct name
    vk_struct_mem_type_var_def = re.compile(r'^VkStructureType\s+sType;')
    vk_physical_dev_feature_def = re.compile(r'^VkPhysicalDevice(\w+)?Features\d*([A-Z]*)$') # group 1: name stem; group 2: vendor, exclude structs ended with Features\d*<SomeThingElse> 
    vk_physical_dev_properties2_def = re.compile(r'^VkPhysicalDevice(\w+)?Properties\d*([A-Z]*)$') # group 1: name stem; group 2: vendor, exclude VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR

    vk_enum_def = re.compile(r'^typedef\s+enum\s+(Vk\w+)') # group 1: enum type name
    vk_enum_item_def = re.compile(r'^(VK_\w+)\s+=\s+([\-]?\w+)') # group 1: enum item name; group 2: enum item value
    vk_flag_bits_def = re.compile(r'^Vk(\w+)FlagBits([A-Z]*)$') # group 1: name stem; group 2: vendor


class logger:
    msg_info_prefix = "[INFO] "
    msg_info_suffix = ""
    msg_warning_prefix = "[WARN] "
    msg_warning_suffix = ""
    msg_error_prefix = "[ERR] "
    msg_error_suffix = ""
    
    @classmethod
    def wrap_str(cls, prefix, msg, suffix, delim=' '):
        return prefix + delim + msg + delim + suffix
    
    @classmethod
    def info(cls, msg):
        print(logger.wrap_str(logger.msg_info_prefix, msg, logger.msg_info_suffix))
    
    @classmethod
    def err(cls, msg):
        print(logger.wrap_str(logger.msg_error_prefix, msg, logger.msg_error_suffix))
    
    @classmethod
    def warn(cls, msg):
        print(logger.wrap_str(logger.msg_warning_prefix, msg, logger.msg_warning_suffix))


class VkEnumItem:
    def __init__(self, name = "", value = ""):
        self.name = name
        self.value = value 


class TypedVkStruct:
    def __init__(self, type_enum_item = "", name = "", is_feature = False, is_properties2 = False):
        self.type_enum_item = type_enum_item
        self.name = name 
        self.is_feature = is_feature
        self.is_properties2 = is_properties2


class ObjectiveVkHandle:
    def __init__(self, name = "", object_type = ""):
        self.name = name
        self.object_type = object_type

def gen_cpp_enum(enum_type_name, enum_item_list, first_value = 0, namespace = "", alias_item_dict = {}, underlying_type = 'int32_t'):
    namespace_prefix = ''
    if namespace != "":
        namespace_prefix = namespace + '::'
    string_func_name = f'str_from_{enum_type_name}'
    string_func_ret_type = 'std::string_view'

    string_func_decl_code = f'{string_func_ret_type} {string_func_name}({enum_type_name});\n\n'
    string_func_def_code = f'''{string_func_ret_type} {namespace_prefix}{string_func_name}({enum_type_name} e) 
{{
    switch(e) {{
'''

    enum_def_code = f'enum {enum_type_name} : {underlying_type} {{\n'
    current_item_idx = 0
    total_item_count = len(enum_item_list) + len(alias_item_dict)
    for item in enum_item_list:
        string_func_def_code += f'{cpp_names.indent2}case {namespace_prefix}{item}: return "{item}";\n'
        enum_def_code += f'{cpp_names.indent1}{item}'
        if current_item_idx == 0:
            enum_def_code += f' = {str(first_value)}'
        if current_item_idx != total_item_count - 1:
            enum_def_code +=','
        enum_def_code += '\n'
        current_item_idx += 1
    for item in alias_item_dict:
        enum_def_code += f'{cpp_names.indent1}{item} = {alias_item_dict[item]}'
        if current_item_idx != total_item_count - 1:
            enum_def_code +=','
        enum_def_code += '\n'
        current_item_idx += 1
    enum_def_code += f'}}; // {enum_type_name}\n\n'
    string_func_def_code += f'{cpp_names.indent1}}};\n'
    string_func_def_code += f'{cpp_names.indent1}return "Unknown {enum_type_name}";\n'
    string_func_def_code += f'}} // {string_func_name}({enum_type_name})\n\n'

    return (enum_def_code, string_func_decl_code, string_func_def_code) 


def find_vk_struct_type_enum_item(vk_struct_name, vk_struct_type_enum_items):
    for enum_item in vk_struct_type_enum_items:
        enum_item1 = enum_item.replace("VK_STRUCTURE_TYPE", "VK")
        if vk_struct_name.upper() == enum_item1.replace('_', ''):
            return enum_item
    logger.warn(f"cannot find struct type enum for {vk_struct_name}")
    return ""


def find_object_type_of_vk_handle(vk_handle, vk_object_type_enum_items):
    for enum_item in vk_object_type_enum_items:
        enum_item1 = enum_item.replace('VK_OBJECT_TYPE', 'VK')
        if vk_handle.upper() == enum_item1.replace('_', ''):
            return enum_item
    logger.err(f"cannot find struct type enum for {vk_handle}")
    exit(1)



def verity_feature_extension_map(scoped_vk_feature_dict):
    for scope in scoped_vk_feature_dict:
        for feature in scoped_vk_feature_dict[scope]:
            feature_name = feature.name
            feature_search = pat.vk_physical_dev_feature_def.search(feature_name)
            if not feature_search:
                logger.err(f"invalid feature name {feature_name}")
            elif (not feature_search.group(2)) or (feature_name == 'VkPhysicalDeviceFeatures2'): 
                logger.info(f"core profile {scope} feature: {feature_name}")
            elif feature_search.group(2) == '':
                logger.info(f"{feature_name} has an empty vendor suffix") # if output, regex error 
            else:
                feature_name1 = f'VK{feature_search.group(2)}{feature_search.group(1)}'.upper()
                extension = scope.replace('_', '').upper()
                if feature_name1 != extension:
                    logger.warn(f'feature {feature_name} does not match extension {scope}')



def gen_cpp_enum_mtl_type():
    enum_item_list = [      
        "MTL_TYPE_PRIME",   
        "MTL_TYPE_BLEND",  
        "MTL_TYPE_COUNT"  
    ]
    return gen_cpp_enum('mtl_type_e', enum_item_list, 0, cpp_names.namespace, {}, 'uint8_t')


def gen_cpp_enum_punctual_light_type():
    enum_item_list = [
        "PUNCTUAL_LIGHT_TYPE_SPOT",
        "PUNCTUAL_LIGHT_TYPE_POINT",
        "PUNCTUAL_LIGHT_TYPE_DIRECTIONAL",
        "PUNCTUAL_LIGHT_TYPE_COUNT"
    ]
    return gen_cpp_enum('punctual_light_type_e', enum_item_list, 0, cpp_names.namespace, {})


def create_cpp_feature_wrapper_struct():
    code = f'''
struct {cpp_names.feature_wrapper_type} {{
    void* feature_ptr;
    void** feature_pNext_ptr;
    std::string_view feature_name;
    std::string_view extension_name;
    bool available;
    bool required;
    bool versioned;

    {cpp_names.feature_wrapper_type}() : feature_ptr(nullptr), feature_pNext_ptr(nullptr), available(false), required(false), versioned(false) {{}}
    {cpp_names.feature_wrapper_type}(void* a_feature_ptr, void** a_feature_pNext_ptr, std::string_view a_feature_name, std::string_view a_extension_name) 
        : feature_ptr(a_feature_ptr)
        , feature_pNext_ptr(a_feature_pNext_ptr)
        , feature_name(a_feature_name)
        , extension_name(a_extension_name)
        , available(false)
        , required(false)
        , versioned(false) 
    {{
        if (extension_name.length() > 10 && extension_name.substr(0, 11) == std::string_view{{ "VK_VERSION_" }}) {{
            versioned = true;
        }}
    }}
}};\n\n'''

    return code

def create_cpp_make_vk_structs(scoped_typed_vk_struct_dict, declare_only):
    code = ""
    for scope in scoped_typed_vk_struct_dict:
        code += f'#if {scope}\n'
        for vk_struct in scoped_typed_vk_struct_dict[scope]:
            if declare_only:
                code += f'{vk_struct.name} make_{vk_struct.name}();\n'
            else:
                code += f'{vk_struct.name} make_{vk_struct.name}() {{ return {vk_struct.name} {{ {vk_struct.type_enum_item} }}; }}\n'
        code += '#endif\n\n'
    code += '\n\n'

    return code 


def create_cpp_get_vk_object_type_func(scoped_objective_vk_handle_dict, declare_only):
    code = "#if VK_EXT_debug_utils\n"
    for scope in scoped_objective_vk_handle_dict:
        code += f'#if {scope}\n'
        for vk_handle in scoped_objective_vk_handle_dict[scope]:
            func_name = f'{cpp_names.get_vk_object_type_func_name}'
            code += f'VkObjectType {func_name}({vk_handle.name} a_vk_handle)'
            if declare_only:
                code += ';\n'
            else:
                code += f'\n{{\n{cpp_names.indent1}return {vk_handle.object_type};\n}}\n'
        code += '#endif\n\n'
    code += '#endif // VK_EXT_debug_utils\n\n'

    return code



def create_cpp_set_vk_handle_debug_name_func(scoped_objective_vk_handle_dict, declare_only):
    code = "#if VK_EXT_debug_utils\n"
    for scope in scoped_objective_vk_handle_dict:
        code += f'#if {scope}\n'
        for vk_handle in scoped_objective_vk_handle_dict[scope]:
            func_name = f'set_vk_handle_debug_name'
            code += f'void {func_name}(VkDevice a_vk_dev, {vk_handle.name} a_vk_handle, const std::string& a_name, bool a_debug_enabled'
            if declare_only:
                code += ' = true);\n'
            else:
                code += f''')
{{
    if (!a_debug_enabled) return;
    VkDebugUtilsObjectNameInfoEXT info{{ VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT, nullptr, {vk_handle.object_type}, (uint64_t)a_vk_handle, a_name.c_str()}};
    vkSetDebugUtilsObjectNameEXT(a_vk_dev, &info);
}}\n'''
        code += '#endif\n\n'
    code += '#endif // VK_EXT_debug_utils\n\n'

    return code



def create_cpp_vk_feature_vk_type_to_str_func(scoped_vk_feature_dict, declare_only):
    code = ''
    for scope in scoped_vk_feature_dict:
        code += f'#if {scope}\n'
        for vk_feature in scoped_vk_feature_dict[scope]:
            code += f'std::string_view {cpp_names.vk_type_to_str_func_name}({vk_feature.name})'
            if declare_only:
                code += ';\n'
            else:
                code += f' {{ return "{vk_feature.name}"; }}\n'
        code += '#endif\n\n'
    code += '\n\n'

    return code


def create_cpp_get_extension_name_func(scoped_vk_feature_dict, declare_only):
    get_extension_name_func_decl = f'std::string_view {cpp_names.get_extension_name_func_name}(std::string_view a_feature_name, bool* a_ok_ptr'
    if declare_only:
        return f'{get_extension_name_func_decl} = nullptr);\n'
    code = f'{get_extension_name_func_decl})\n{{\n'
    code += f'{cpp_names.indent1}using namespace std::literals;\n'
    code += f'{cpp_names.indent1}if (a_ok_ptr) *a_ok_ptr = true;\n'
    for scope in scoped_vk_feature_dict:
        code += f'#if {scope}\n'
        for vk_feature in scoped_vk_feature_dict[scope]:
            code += f'{cpp_names.indent1}if(a_feature_name == "{vk_feature.name}"sv) return "{scope}";\n'
        code += '#endif\n'
    code += f'{cpp_names.indent1}if (a_ok_ptr) *a_ok_ptr = false;\n'
    code += f'{cpp_names.indent1}return "Unknown_extension_name";\n'
    code += '}\n\n'

    return code


def create_cpp_get_extension_name_func_via_map(declare_only):
    get_extension_name_func_decl = f'std::string_view {cpp_names.get_extension_name_func_name}(std::string_view a_feature_name, bool* a_ok_ptr'
    feature_extension_str_map_var = f'{cpp_names.global_lookup_table_var_name}.{cpp_names.feature_extension_str_map_var}'
    if declare_only:
        return f'{get_extension_name_func_decl} = nullptr);\n'
    code = f'''{get_extension_name_func_decl})
{{
    const auto feature_search = {feature_extension_str_map_var}.find(a_feature_name);
    if (feature_search != {feature_extension_str_map_var}.end()) {{
        if (a_ok_ptr) *a_ok_ptr = true;
        return feature_search->second;
    }}
    if (a_ok_ptr) *a_ok_ptr = false;
    return "Unknown_extension_name";
}}\n\n'''

    return code


def create_cpp_str_to_vk_enum_func(vk_enum_name, vk_enum_item_list, beta_extensions_vk_enum_item_list, declare_only):
    code = f'{vk_enum_name} {cpp_names.str_to_vk_enum_func_name_prefix}{vk_enum_name}(std::string_view a_str, bool* a_ok_ptr'
    if declare_only:
        code += ' = nullptr);\n'
        return code

    code += f')\n{{\n'
    code += f'{cpp_names.indent1}using namespace std::literals;\n'
    code += f'{cpp_names.indent1}if (a_ok_ptr) *a_ok_ptr = true;\n'
    if len(vk_enum_item_list) > 0:
        for enum_item in vk_enum_item_list:
            code += f'{cpp_names.indent1}if (a_str == "{enum_item.name}"sv) return {enum_item.name};\n'
    if len(beta_extensions_vk_enum_item_list) > 0:
        code += f'#ifdef VK_ENABLE_BETA_EXTENSIONS\n'
        for enum_item in beta_extensions_vk_enum_item_list:
            code += f'{cpp_names.indent1}if (a_str == "{enum_item.name}"sv) return {enum_item.name};\n'
        code += '#endif\n'
    code += f'{cpp_names.indent1}if (a_ok_ptr) *a_ok_ptr = false;\n'
    code += f'{cpp_names.indent1}return static_cast<{vk_enum_name}>(0x7FFFFFFF);\n}}\n\n'

    return code


def create_cpp_str_to_vk_enum_via_map_func(vk_enum_name, declare_only):
    str_vk_enum_map_var = f'{cpp_names.global_lookup_table_var_name}.{cpp_names.str_vk_enum_map_var_prefix}{vk_enum_name}'
    code = f'{vk_enum_name} {cpp_names.str_to_vk_enum_func_name_prefix}{vk_enum_name}(std::string_view str, bool* a_ok_ptr'
    if declare_only:
        code += ' = nullptr);\n'
        return code

    code += f''') {{
    const auto vk_enum_search = {str_vk_enum_map_var}.find(str);
    if (vk_enum_search != {str_vk_enum_map_var}.end()) {{
        return vk_enum_search->second;
    }}
    except("cannot covert " + std::string(str) + " to {vk_enum_name}");
    return static_cast<{vk_enum_name}>(0x7FFFFFFF);
}}\n\n'''

    return code


def create_cpp_vk_enum_to_str_func(vk_enum_name, vk_enum_item_list, beta_extensions_vk_enum_item_list, declare_only):
    vk_enum_to_str_func_name = f'{cpp_names.vk_enum_to_str_func_name_prefix}{vk_enum_name}' 
    code = f'std::string_view {vk_enum_to_str_func_name}({vk_enum_name} e)'
    if declare_only:
        code += ';\n'
    else:
        code += f'\n{{\n{cpp_names.indent1}switch (e) {{\n'
        if len(vk_enum_item_list) > 0:
            current_item_list = []
            for enum_item in vk_enum_item_list:
                if not enum_item.value in current_item_list:
                    code += f'{cpp_names.indent2}case {enum_item.name}: return "{enum_item.name}";\n'
                current_item_list.append(enum_item.name)
                current_item_list.append(enum_item.value)
        if len(beta_extensions_vk_enum_item_list) > 0:
            current_item_list = []
            code += '#ifdef VK_ENABLE_BETA_EXTENSIONS\n'
            for enum_item in beta_extensions_vk_enum_item_list:
                if not enum_item.value in current_item_list:
                    code += f'{cpp_names.indent2}case {enum_item.name}: return "{enum_item.name}";\n'
                current_item_list.append(enum_item.name)
                current_item_list.append(enum_item.value)
            code += '#endif\n'
        code += f'{cpp_names.indent2}default: break;\n{cpp_names.indent1}}}\n'
        code += f'{cpp_names.indent1}return "Unknown {vk_enum_name}";\n}}\n\n'

    match_vk_flag_bits_def = pat.vk_flag_bits_def.search(vk_enum_name)
    if match_vk_flag_bits_def:
        flag_name = f'Vk{match_vk_flag_bits_def.group(1)}Flags' + str(match_vk_flag_bits_def.group(2) or '')
        vk_flag_to_str_func_name = f'{cpp_names.vk_enum_to_str_func_name_prefix}{flag_name}'
        code += f'std::string {vk_flag_to_str_func_name}({flag_name} f)'
        if declare_only:
            code += ';\n'
        else:
            code += f'''{{
    std::string result;
    int index = 0;
    while(f) {{
        if (f & 1) {{
            if( !result.empty()) result.append(" | ");
            result.append({vk_enum_to_str_func_name}(static_cast<{vk_enum_name}>(1U << index)));
        }}
        ++index;
        f >>= 1;
    }}
    if( result.empty()) result.append({vk_enum_to_str_func_name}(static_cast<{vk_enum_name}>(0)));
    return result;
}}\n\n '''

    return code


def create_cpp_vk_enum_to_str_via_map_func(vk_enum_name, declare_only):
    vk_enum_str_map_var = f'{cpp_names.global_lookup_table_var_name}.{cpp_names.vk_enum_str_map_var_prefix}{vk_enum_name}'
    vk_enum_to_str_func_name = f'{cpp_names.vk_enum_to_str_func_name_prefix}{vk_enum_name}' 
    code = f'std::string_view {vk_enum_to_str_func_name}({vk_enum_name} e)'
    if declare_only:
        code += ';\n'
    else:
        code += f''' {{
    const auto vk_enum_search = {vk_enum_str_map_var}.find(e);
    if (vk_enum_search != {vk_enum_str_map_var}.end()) {{
        return vk_enum_search->second;
    }}
    return "Unknown_{vk_enum_name}";
}}\n\n'''

    match_vk_flag_bits_def = pat.vk_flag_bits_def.search(vk_enum_name)
    if match_vk_flag_bits_def:
        flag_name = f'Vk{match_vk_flag_bits_def.group(1)}Flags' + str(match_vk_flag_bits_def.group(2) or '')
        vk_flag_to_str_func_name = f'{cpp_names.vk_enum_to_str_func_name_prefix}{flag_name}'
        code += f'std::string {vk_flag_to_str_func_name}({flag_name} f)'
        if declare_only:
            code += ';\n'
        else:
            code += f'''{{
    std::string result;
    int index = 0;
    while(f) {{
        if (f & 1) {{
            if( !result.empty()) result.append(" | ");
            result.append({vk_enum_to_str_func_name}(static_cast<{vk_enum_name}>(1U << index)));
        }}
        ++index;
        f >>= 1;
    }}
    if( result.empty()) result.append({vk_enum_to_str_func_name}(static_cast<{vk_enum_name}>(0)));
    return result;
}}\n\n '''

    return code


def create_cpp_all_vk_enum_str_conversion_functions(scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict, declare_only):
    code = ''
    for scope in scoped_vk_enum_dict:
        code += f'#if {scope}\n'
        for vk_enum_name in scoped_vk_enum_dict[scope]:
            code += create_cpp_str_to_vk_enum_func(vk_enum_name, vk_enum_items_dict[vk_enum_name], beta_extensions_vk_enum_items_dict[vk_enum_name], declare_only)
            code += create_cpp_vk_enum_to_str_func(vk_enum_name, vk_enum_items_dict[vk_enum_name], beta_extensions_vk_enum_items_dict[vk_enum_name], declare_only)
        code += '#endif\n'

    return code


def all_features_feature_member_name(vk_feature_name):
    return vk_feature_name[len('Vk'):]


def create_cpp_all_features_feature_members(scoped_vk_feature_dict):
    code = ''
    for scope in scoped_vk_feature_dict:
        code += f'#if {scope}\n'
        for vk_feature in scoped_vk_feature_dict[scope]:
            var_name = all_features_feature_member_name(vk_feature.name)
            code += f'{cpp_names.indent1}{vk_feature.name} {var_name}{{ {vk_feature.type_enum_item} }};\n'
        code += '#endif\n'
    code += '\n\n'

    return code


def create_cpp_all_features_init_str_feature_map_func(scoped_vk_feature_dict, declare_only):
    if declare_only:
        return f'{cpp_names.indent1}void {cpp_names.init_str_feature_map_func_name}();\n'

    code = f'void {cpp_names.all_features_type}::{cpp_names.init_str_feature_map_func_name}() {{\n'
    code += f'{cpp_names.indent1}{cpp_names.str_feature_map_var} = {cpp_names.str_feature_map_type} {{\n'
    for scope in scoped_vk_feature_dict:
        code += f'#if {scope}\n'
        for vk_feature in scoped_vk_feature_dict[scope]:
            var_name = all_features_feature_member_name(vk_feature.name)
            code += f'''{cpp_names.indent2}{{
            "{vk_feature.name}",
            {cpp_names.feature_wrapper_type}(
                &{var_name},
                &{var_name}.pNext,
                "{vk_feature.name}",
                "{scope}"
            )
        }}, 
'''
        code += '#endif\n'
    code += f'{cpp_names.indent1}}};\n}}\n\n'

    return code


def create_cpp_all_features_link(declare_only):
    ret_type = f'std::vector<{cpp_names.feature_wrapper_type}>'
    arg1 = f'{cpp_names.feature_request_type} a_feature_requests'
    arg2 = f'const std::unordered_set<std::string>& a_available_device_extensions'
    arg3 = f'bool& a_ok'
    arg4 = f'std::string& a_error_msg'
    if declare_only:
        return f'{cpp_names.indent1}{ret_type} link({arg1}, {arg2}, {arg3}, {arg4});\n'

    code = f'''
{ret_type} {cpp_names.all_features_type}::
link
(
    {arg1},
    {arg2},
    {arg3},
    {arg4}
)
{{
    {ret_type} feature_wrappers;
    a_ok = false;
    feature_wrappers.reserve(a_feature_requests.size());
    for (const auto& request : a_feature_requests) {{
        const std::string_view feature_name_sv{{ request.first }};
        const bool is_feature_required = request.second;
        bool extension_name_found = false;
        const auto extension_name = {cpp_names.get_extension_name_func_name}(feature_name_sv, &extension_name_found);
        if (extension_name_found) {{
            auto feature_wrapper = {cpp_names.all_features_wrap_feature_func_name}(feature_name_sv);
            bool ext_available = feature_wrapper.versioned || a_available_device_extensions.find(extension_name.data()) != a_available_device_extensions.end();
            if (is_feature_required && !ext_available) {{
                a_error_msg = std::string(feature_name_sv) + " is not available";
                return feature_wrappers;
            }}
            else {{
                a_ok = true;
                feature_wrapper.required = is_feature_required;
                feature_wrapper.available = ext_available;
                if (ext_available) {{
                    feature_wrappers.push_back(feature_wrapper);
                }}
                else {{
                    a_error_msg = std::string(feature_name_sv) + " is not available";
                }}
            }}
        }}
        else {{
            const std::string header_version_str = std::to_string(VK_API_VERSION_MAJOR(VK_HEADER_VERSION_COMPLETE)) + '.' + std::to_string(VK_API_VERSION_MINOR(VK_HEADER_VERSION_COMPLETE)) + '.' + std::to_string(VK_API_VERSION_PATCH(VK_HEADER_VERSION_COMPLETE));
            a_error_msg = std::string(feature_name_sv) + " is not available in Vulkan " + header_version_str;
            if (!is_feature_required) {{
                a_ok = true;
            }}
        }}
    }}
    const size_t num_enabled_features = feature_wrappers.size();
    if (num_enabled_features == 0) {{
        return feature_wrappers;
    }}
    for (size_t i = 0; i < num_enabled_features - 1; ++i) {{
        *feature_wrappers[i].feature_pNext_ptr = feature_wrappers[i + 1].feature_ptr;
    }}
    *feature_wrappers.back().feature_pNext_ptr = nullptr;

    return feature_wrappers;
}}\n\n'''

    return code


def create_cpp_all_features_wrap_feature_func(scoped_vk_feature_dict, declare_only):
    code = ''
    ret_type = f'{cpp_names.feature_wrapper_type}'
    arg1 = f'std::string_view a_feature_name'
    arg2 = 'bool* a_ok_ptr'
    if declare_only:
        return f'{cpp_names.indent1}{ret_type} {cpp_names.all_features_wrap_feature_func_name}({arg1}, {arg2} = nullptr);\n\n'

    code += f'''{ret_type} {cpp_names.all_features_type}::{cpp_names.all_features_wrap_feature_func_name}({arg1}, {arg2})
{{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
'''
    for scope in scoped_vk_feature_dict:
        code += f'#if {scope}\n'
        for vk_feature in scoped_vk_feature_dict[scope]:
            var_name = all_features_feature_member_name(vk_feature.name)
            code += f'{cpp_names.indent1}if (a_feature_name == "{vk_feature.name}"sv) return {cpp_names.feature_wrapper_type}{{&{var_name}, &{var_name}.pNext, "{vk_feature.name}", "{scope}"}};\n'
        code += '#endif\n\n'
    code += f'{cpp_names.indent1}if (a_ok_ptr) *a_ok_ptr = false;\n'
    code += f'{cpp_names.indent1}return {cpp_names.feature_wrapper_type}();\n}}\n\n'

    return code


def create_cpp_all_features_get_feature_functions(scoped_vk_feature_dict, declare_only):
    code = ''
    if declare_only:
        for scope in scoped_vk_feature_dict:
            code += f'#if {scope}\n'
            for vk_feature in scoped_vk_feature_dict[scope]:
                code += f'{cpp_names.indent1}{vk_feature.name}* {cpp_names.all_features_get_feature_func_name_prefix}{vk_feature.name}();\n'
            code += '#endif\n'
    else:
        for scope in scoped_vk_feature_dict:
            code += f'#if {scope}\n'
            for vk_feature in scoped_vk_feature_dict[scope]:
                code += f'{vk_feature.name}* {cpp_names.all_features_type}::{cpp_names.all_features_get_feature_func_name_prefix}{vk_feature.name}()\n'
                code += f'{{\n'
                var_name = all_features_feature_member_name(vk_feature.name)
                code += f'{cpp_names.indent1}return &{var_name};\n}}\n\n'
            code += '#endif\n\n'
    code += '\n'

    return code


def create_cpp_all_features_struct(scoped_vk_feature_dict):
    code = f'struct {cpp_names.all_features_type} {{\n'
    code += f'{cpp_names.indent1}{cpp_names.all_features_type}(bool a_requested_only = false) : requested_only{{ a_requested_only }} {{}}\n'
    code += create_cpp_all_features_link(True)
    code += create_cpp_all_features_wrap_feature_func(scoped_vk_feature_dict, True)
    # code += create_cpp_all_features_get_feature_functions(scoped_vk_feature_dict, True)

    code += f'{cpp_names.indent1}bool requested_only;\n'
    code += create_cpp_all_features_feature_members(scoped_vk_feature_dict)
    code += f'}}; // struct {cpp_names.all_features_type}\n\n'

    return code


# in anonymous namespace
def create_cpp_global_lookup_table(scoped_vk_feature_dict, scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict):
    num_features = 0
    namespace_prefix = ''
    if cpp_names.namespace != '':
        namespace_prefix = f'{cpp_names.namespace}::'
    for scope in scoped_vk_feature_dict:
        num_features += len(scoped_vk_feature_dict[scope])
    code = f'struct {cpp_names.global_lookup_table_struct_name} {{\n'
    code += f'{cpp_names.indent1}std::unordered_map<std::string_view, std::string_view> {cpp_names.feature_extension_str_map_var};\n'
    for scope in scoped_vk_enum_dict:
        code += f'#if {scope}\n'
        for vk_enum_name in scoped_vk_enum_dict[scope]:
            str_vk_enum_map_var = f'{cpp_names.str_vk_enum_map_var_prefix}{vk_enum_name}'
            vk_enum_str_map_var = f'{cpp_names.vk_enum_str_map_var_prefix}{vk_enum_name}'
            code += f'{cpp_names.indent1}std::unordered_map<std::string_view, {vk_enum_name}> {str_vk_enum_map_var};\n'
            code += f'{cpp_names.indent1}std::unordered_map<{vk_enum_name}, std::string_view> {vk_enum_str_map_var};\n'
        code += '#endif\n'

    code +='\n'
    code += f'{cpp_names.indent1}{cpp_names.global_lookup_table_struct_name}() {{\n'
    code += f'{cpp_names.indent2}{namespace_prefix}info("initializing {cpp_names.global_lookup_table_struct_name}");\n'
    code += f'{cpp_names.indent2}{cpp_names.feature_extension_str_map_var}.reserve({num_features});\n'
    for scope in scoped_vk_feature_dict:
        code += f'#if {scope}\n'
        for vk_feature in scoped_vk_feature_dict[scope]:
            code += f'{cpp_names.indent2}{cpp_names.feature_extension_str_map_var}.emplace("{vk_feature.name}", "{scope}");\n'
        code += '#endif\n'
    code + '\n'
    for scope in scoped_vk_enum_dict:
        code += f'#if {scope}\n'
        for vk_enum_name in scoped_vk_enum_dict[scope]:
            num_enum_items = len(vk_enum_items_dict[vk_enum_name]) + len(beta_extensions_vk_enum_items_dict[vk_enum_name])
            str_vk_enum_map_var = f'{cpp_names.str_vk_enum_map_var_prefix}{vk_enum_name}'
            vk_enum_str_map_var = f'{cpp_names.vk_enum_str_map_var_prefix}{vk_enum_name}'
            code += f'{cpp_names.indent2}{str_vk_enum_map_var}.reserve({num_enum_items});\n'
            code += f'{cpp_names.indent2}{vk_enum_str_map_var}.reserve({num_enum_items});\n\n'
            current_item_set = set() 
            for enum_item in vk_enum_items_dict[vk_enum_name]:
                code += f'{cpp_names.indent2}{str_vk_enum_map_var}.emplace("{enum_item.name}", {enum_item.name});\n'
                if enum_item.value in current_item_set:
                    code += '//'
                code += f'{cpp_names.indent2}{vk_enum_str_map_var}.emplace({enum_item.name}, "{enum_item.name}");\n'
                current_item_set.add(enum_item.name)
                current_item_set.add(enum_item.value)
            if len(beta_extensions_vk_enum_items_dict[vk_enum_name]) > 0:
                code += f'#ifdef VK_ENABLE_BETA_EXTENSIONS\n'
                for enum_item in beta_extensions_vk_enum_items_dict[vk_enum_name]:
                    code += f'{cpp_names.indent2}{str_vk_enum_map_var}.emplace("{enum_item.name}", {enum_item.name});\n'
                    if enum_item.value in current_item_set:
                        code += '//'
                    code += f'{cpp_names.indent2}{vk_enum_str_map_var}.emplace({enum_item.name}, "{enum_item.name}");\n'
                    current_item_set.add(enum_item.name)
                    current_item_set.add(enum_item.value)
                code += '#endif\n'
            code += '\n'
        code += '#endif\n\n'

    code += f'{cpp_names.indent1}}} //ctor of {cpp_names.global_lookup_table_struct_name}\n\n'
    code += f'}}; //end of {cpp_names.global_lookup_table_struct_name}\n'
    code += f'static {cpp_names.global_lookup_table_struct_name} {cpp_names.global_lookup_table_var_name};\n\n'

    return code



def generate_hpp_code(scoped_typed_vk_struct_dict, scoped_objective_vk_handle_dict, scoped_vk_feature_dict, scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict):
    code = f'''
#pragma once
#if defined(_MSC_VER)
#pragma warning(disable : 26812)
#endif
#if defined(__INTELLISENSE__)
#undef VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#else
#include <volk.h>
#endif

#include <map>
#include <unordered_set>
#include <string>
#include <string_view>

namespace {cpp_names.namespace} {{\n'''
    code += create_cpp_get_extension_name_func(scoped_vk_feature_dict, True)
    code += create_cpp_feature_wrapper_struct()
    code += create_cpp_all_features_struct(scoped_vk_feature_dict)

    code += create_cpp_all_vk_enum_str_conversion_functions(scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict, True)
    code += create_cpp_make_vk_structs(scoped_typed_vk_struct_dict, True)
    code += create_cpp_get_vk_object_type_func(scoped_objective_vk_handle_dict, True)
    # code += create_cpp_vk_feature_vk_type_to_str_func(scoped_vk_feature_dict, True)
    code += f'}} // namespace {cpp_names.namespace}\n\n'

    code += f'#if defined({cpp_names.macro_impl_def})\n\n'

    code += 'namespace {\n'
    # code += create_cpp_all_str_vk_enum_maps(scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict)
    # code += create_cpp_global_lookup_table(scoped_vk_feature_dict, scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict)
    code += '} // anonymous namespace\n\n'

    code += f'namespace {cpp_names.namespace}{{\n'

    code += create_cpp_all_features_link(False)
    code += create_cpp_all_features_wrap_feature_func(scoped_vk_feature_dict, False)
    # code += create_cpp_all_features_get_feature_functions(scoped_vk_feature_dict, False)

    code += create_cpp_get_extension_name_func(scoped_vk_feature_dict, False)
    code += create_cpp_all_vk_enum_str_conversion_functions(scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict, False)
    code += create_cpp_make_vk_structs(scoped_typed_vk_struct_dict, False)
    code += create_cpp_get_vk_object_type_func(scoped_objective_vk_handle_dict, False)
    # code += create_cpp_vk_feature_vk_type_to_str_func(scoped_vk_feature_dict, False)
    code += f'}} // namespace {cpp_names.namespace}\n\n'

    code += f'#endif // {cpp_names.macro_impl_def}\n\n'

    return code


def remove_empty_scopes(scoped_dict):
    empty_scopes = { k for k in scoped_dict if len(scoped_dict[k]) == 0 }
    for scope in empty_scopes:
        scoped_dict.pop(scope, None)

def parse_vk_header(vk_header_path, generated_file_stem):
    try:
        with open(vk_header_path, 'r') as vk_header: 
            vk_header_lines = [line.strip() for line in vk_header.readlines()]
    except Exception as e:
        print(type(e))
        print(e)
        exit(1)

    header_version_num_variant = "-1"
    header_version_num_major = "-1"
    header_version_num_minor = "-1"
    header_version_num_patch = "-1"
    vk_version_num_major_max = "-1"
    vk_version_num_minor_max = "-1"

    scope_list = []

    # for verification
    vk_object_type_enum_item_set = set()
    vk_struct_type_enum_item_set = set()

    vk_enum_items_dict = {}
    beta_extensions_vk_enum_items_dict = {}
    current_enum_name = ""

    in_vk_enum_def = False
    in_ifdef_vk_enable_beta_extensions = False
    ifdef_macros = []

    for i, line in enumerate(vk_header_lines):
        line_num = i + 1

        match_vk_header_patch_version = pat.vk_header_patch_version_def.search(line)
        match_vk_header_version = pat.vk_header_version_def.search(line)
        match_vk_version = pat.vk_version_def.search(line)

        match_macro_if = pat.macro_if.search(line)
        match_macro_endif = pat.macro_endif.search(line)

        match_vk_ext_name = pat.vk_ext_name_def.search(line)

        match_vk_enum_def = pat.vk_enum_def.search(line)
        match_vk_enum_item_def = pat.vk_enum_item_def.search(line)

        if match_vk_header_patch_version:
            header_version_num_patch = match_vk_header_patch_version.group(1)
        elif match_vk_header_version:
            header_version_num_variant = match_vk_header_version.group(1)
            header_version_num_major = match_vk_header_version.group(2)
            header_version_num_minor = match_vk_header_version.group(3)
        elif match_vk_version:
            scope_list.append(match_vk_version.group(1))
            vk_version_num_major_max = max(int(vk_version_num_major_max), int(match_vk_version.group(2)))
            vk_version_num_minor_max = max(int(vk_version_num_minor_max), int(match_vk_version.group(3)))

        elif match_macro_if:
            ifdef_macros.append(match_macro_if.group(2))
            if ifdef_macros[-1] == 'VK_ENABLE_BETA_EXTENSIONS':
                in_ifdef_vk_enable_beta_extensions = True
            elif in_ifdef_vk_enable_beta_extensions:
                logger.info(f'macro {ifdef_macros[-1]} nested into VK_ENABLE_BETA_EXTENSIONS')
        elif match_macro_endif:
            current_ifdef_macro = ifdef_macros.pop()
            if current_ifdef_macro == 'VK_ENABLE_BETA_EXTENSIONS':
                in_ifdef_vk_enable_beta_extensions = False

        elif match_vk_ext_name:
            scope_list.append(match_vk_ext_name.group(1))

        elif match_vk_enum_def:
            in_vk_enum_def = True
            current_enum_name = match_vk_enum_def.group(1)
            vk_enum_items_dict[current_enum_name] = []
            beta_extensions_vk_enum_items_dict[current_enum_name] = []
        elif in_vk_enum_def:
            match_vk_enum_end = re.search(r'^}}\s+{}'.format(current_enum_name), line)
            if match_vk_enum_end:
                in_vk_enum_def = False
            elif match_vk_enum_item_def:
                enum_item_name = match_vk_enum_item_def.group(1)
                enum_item_value = match_vk_enum_item_def.group(2)
                if current_enum_name == 'VkObjectType':
                    vk_object_type_enum_item_set.add(enum_item_name)
                elif current_enum_name == 'VkStructureType':
                    vk_struct_type_enum_item_set.add(enum_item_name)
                if in_ifdef_vk_enable_beta_extensions:
                    beta_extensions_vk_enum_items_dict[current_enum_name].append(VkEnumItem(enum_item_name, enum_item_value))
                else:
                    vk_enum_items_dict[current_enum_name].append(VkEnumItem(enum_item_name, enum_item_value))
   
    if header_version_num_major == '-1':
        header_version_num_major = vk_version_num_major_max
    if header_version_num_minor == '-1':
        header_version_num_minor = vk_version_num_minor_max

    current_struct_name = ""
    current_scope_idx = -1    
    current_scope_name = ""

    scoped_typed_vk_struct_dict = {}
    scoped_objective_vk_handle_dict = {}
    scoped_vk_enum_dict = {}

    scope_list.append("EndOfScope")
    for line in vk_header_lines:
        pat_next_scope = r'^#define\s+{}\s+1$'.format(scope_list[current_scope_idx + 1])
        match_next_scope = re.search(pat_next_scope, line)

        if match_next_scope:
            current_scope_idx += 1
            current_scope_name = scope_list[current_scope_idx]
            if not current_scope_name in scoped_typed_vk_struct_dict:
                scoped_typed_vk_struct_dict[current_scope_name] = {}
            if not current_scope_name in scoped_objective_vk_handle_dict:
                scoped_objective_vk_handle_dict[current_scope_name] = {}
            if not current_scope_name in scoped_vk_enum_dict:
                scoped_vk_enum_dict[current_scope_name] = []
        elif current_scope_idx < 0:
            continue

        # scoped search starts here
        match_vk_handle_name = pat.vk_handle_name_def.search(line)
        match_vk_struct_def  = pat.vk_struct_def.search(line)
        match_vk_struct_mem_type_var_def = pat.vk_struct_mem_type_var_def.search(line)
        match_vk_enum_def = pat.vk_enum_def.search(line)

        if current_struct_name != "":
            match_vk_struct_end = re.search(r'^}}\s+{}'.format(current_struct_name), line)
            if match_vk_struct_end:
                current_struct_name = ""
            elif match_vk_struct_mem_type_var_def:
                struct_type_enum_item = find_vk_struct_type_enum_item(current_struct_name, vk_struct_type_enum_item_set)
                if struct_type_enum_item != '':
                    typed_struct = TypedVkStruct(type_enum_item=struct_type_enum_item, name=current_struct_name)
                    match_vk_physical_dev_feature_def = pat.vk_physical_dev_feature_def.search(current_struct_name)
                    match_vk_physical_dev_properties2_def = pat.vk_physical_dev_properties2_def.search(current_struct_name)
                    if match_vk_physical_dev_feature_def:
                        typed_struct.is_feature = True
                    elif match_vk_physical_dev_properties2_def:
                        typed_struct.is_properties2 = True
                    scoped_typed_vk_struct_dict[current_scope_name][typed_struct] = None
        elif match_vk_struct_def:
            current_struct_name = match_vk_struct_def.group(1)
        elif match_vk_handle_name:
            vk_handle_name = match_vk_handle_name.group(2)
            object_type = find_object_type_of_vk_handle(vk_handle_name, vk_object_type_enum_item_set)
            scoped_objective_vk_handle_dict[current_scope_name][ObjectiveVkHandle(name=vk_handle_name, object_type=object_type)] = None
        elif match_vk_enum_def:
            scoped_vk_enum_dict[current_scope_name].append(match_vk_enum_def.group(1))


    remove_empty_scopes(scoped_typed_vk_struct_dict)
    remove_empty_scopes(scoped_objective_vk_handle_dict)
    remove_empty_scopes(scoped_vk_enum_dict)

    scoped_vk_feature_dict = {}
    for scope in scoped_typed_vk_struct_dict:
        scoped_vk_feature_dict[scope] = {}
        for vk_struct in scoped_typed_vk_struct_dict[scope]:
            if vk_struct.is_feature:
                scoped_vk_feature_dict[scope][vk_struct] = None

    remove_empty_scopes(scoped_vk_feature_dict)
    verity_feature_extension_map(scoped_vk_feature_dict)

    header_version_str = '// generated from vulkan header version: '
    header_version_str += header_version_num_major
    header_version_str += '.' +  header_version_num_minor
    if header_version_num_patch != '-1':
        header_version_str += '.' + header_version_num_patch
    if header_version_num_variant != '-1':
        header_version_str += f' (variant: {str(header_version_num_variant)})'
    header_version_str += '\n'

    try:
        with open(generated_file_stem + '.hpp', 'wb') as output:
            src_str = header_version_str + generate_hpp_code(scoped_typed_vk_struct_dict, scoped_objective_vk_handle_dict, scoped_vk_feature_dict, scoped_vk_enum_dict, vk_enum_items_dict, beta_extensions_vk_enum_items_dict)
            output.write(src_str.encode())
    except Exception as e:
        print(type(e))
        print(e)
        exit(1)
    
if __name__ == "__main__":
    vulkan_sdk = os.getenv("VULKAN_SDK", "")
    if vulkan_sdk == '':
        logger.err("could not find vulkan_core.h")
        exit(1)
    vulkan_core_path = os.path.join(vulkan_sdk, 'include', 'vulkan', 'vulkan_core.h')
    
    project_root_dir = os.getcwd()
    generated_file_path = os.path.join(project_root_dir, 'include', 'spock', cpp_names.generated_file_stem)
    parse_vk_header(str(pathlib.Path(vulkan_core_path).as_posix()), str(pathlib.Path(generated_file_path).as_posix()))

    (enum_mtl_type_def_code, enum_mtl_type_string_func_decl_code, enum_mtl_type_string_func_def_code) = gen_cpp_enum_mtl_type()
    (enum_punctual_light_type_def_code, enum_punctual_light_type_string_func_decl_code, enum_punctual_light_type_string_func_def_code) = gen_cpp_enum_punctual_light_type()

    print()
    print(enum_mtl_type_def_code)
    print(enum_punctual_light_type_def_code)
    print(enum_mtl_type_string_func_decl_code)
    print(enum_punctual_light_type_string_func_decl_code)
    print(enum_mtl_type_string_func_def_code)
    print(enum_punctual_light_type_string_func_def_code)


