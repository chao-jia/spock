#define SPOCK_GENERATED_IMPLEMENTATION
#include "../../include/spock/generated.hpp"
#undef SPOCK_GENERATED_IMPLEMENTATION
#include <iostream>

int main(int argc, char** argv)
{
    std::map<std::string_view, bool> requested_feature_names = {
        {"VkPhysicalDeviceRayTracingPipelineFeaturesKHR", true},
        {"VkPhysicalDeviceShaderFloat16Int8Features", true},
        {"VkPhysicalDeviceVulkan11Features", false},
    };
    std::unordered_set<std::string> available_device_extensions{
        "VK_VERSION_1_2",
        "VK_KHR_ray_query",
        "VK_KHR_ray_tracing_pipeline"
    };

    spock::physical_dev_all_features2_t all_features;
    bool ok = false;
    std::string err_msg;
    auto enabled_features = all_features.link(requested_feature_names, available_device_extensions, ok, err_msg);

    auto ft0 = all_features.PhysicalDeviceRayTracingPipelineFeaturesKHR;
    auto ft1 = all_features.PhysicalDeviceShaderFloat16Int8Features;
    auto ft2 = all_features.PhysicalDeviceVulkan11Features;

    return 0;
}

