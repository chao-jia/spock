#pragma once
#include "deferred.hpp"
#include "ddgi.hpp"
#include "reflection.hpp"

#include <spock/scene.hpp>
#include <spock/context.hpp>
#include <spock/utils.hpp>

#include <glm/glm.hpp>
#include <array>
#include <vector>

using namespace std::placeholders;
using namespace spock;


struct svgf_cfg_t {
    float temporal_alpha;
    float bilateral_sigma_d;     // pixel distance
    float bilateral_sigma_p;     // world position
    float bilateral_sigma_n;     // normal
    float edge_stop_sigma_p;     // world_position 
    float edge_stop_sigma_n;     // normal
    float edge_stop_sigma_l;     // luminance (shadow)
    int a_trous_iteration_count;

    svgf_cfg_t() 
        : bilateral_sigma_d{}, bilateral_sigma_p{}, bilateral_sigma_n{}
        , edge_stop_sigma_p{}, edge_stop_sigma_n{}, edge_stop_sigma_l{}
        , temporal_alpha{}, a_trous_iteration_count{}
    {}

    svgf_cfg_t(const ordered_json& a_svgf_json);
};


struct scene_cfg_t {
    std::string name;
    uint32_t gltf_count{};
    std::unique_ptr<std::string[]> gltf_list;
    uint32_t timer_filter_width{};
    float soft_shadow_radius;

    ddgi_t::cfg_t ddgi_cfg;

    scene_cfg_t(const ordered_json& a_scene_json, const std::string& a_scene_name = "");
};


struct input_cfg_t {
    svgf_cfg_t svgf_cfg;
    reflection_t::cfg_t reflection_cfg;
    std::vector<scene_cfg_t> scene_cfgs;
    std::vector<const char*> scene_name_list; // list for imgui, in the same order as scene_gltf_lists_

    input_cfg_t() {}
    input_cfg_t(const ordered_json& a_cfg_json);
};


class input_t
{
private:
    input_cfg_t input_cfg_;
    scene_t scene_;
    std::vector<acceleration_structure_t> scene_blases_;
    std::vector<VkAccelerationStructureInstanceKHR> scene_tlas_instances_;
    std::unordered_map<mtl_type_e, uint32_t> mtl_type_tlas_instance_idx_map_;
    acceleration_structure_t scene_tlas_;
    tlas_builder_t scene_tlas_builder_;

    glm::mat4 light_transform_;
    float light_strength_step_;

    descriptor_pool_t descriptor_pool_;
    std::array<descriptor_set_layout_t, INPUT_DS_LAYOUT_COUNT> descriptor_set_layouts_;
    std::array<VkDescriptorSet, INPUT_DS_COUNT> descriptor_sets_;

    std::unique_ptr<spirv_t[]> spirvs_;
    std::unique_ptr<pipeline_t[]> pipelines_;
    std::unique_ptr<image_t[]> images_;
    std::unique_ptr<image_view_t[]> image_views_;

    buffer_t frame_params_uniform_buffer_;
    buffer_t host_dev_msg_buffer_;

    bool tlas_has_gray_dot_;

    void load_blue_noise_images(const context_t& a_context, VkCommandBuffer a_cmd_buf, VkQueue a_queue);

    // depends on scene_.get_num_textures(), called within init_scene();
    void create_descriptor_set_layouts(VkDevice a_vk_dev);
    void write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler);
    void build_acceleration_structures(const context_t& a_context, const command_pool_t& a_cmd_pool, const queue_t& a_queue);
    

public:
    input_t();
    input_t(
        const spock::ordered_json& a_cfg_json, 
        const context_t& a_context, 
        VkCommandBuffer a_cmd_buf, 
        VkQueue a_queue,
        size_t a_frame_params_uniform_buffer_bytes,
        size_t a_host_dev_msg_buffer_bytes,
        uint32_t a_blue_noise_texture_count
    );
    void init_scene(
        int a_scene_idx, 
        const context_t& a_context, 
        const command_pool_t& a_cmd_pool, 
        const queue_t& a_queue, 
        VkSampler a_dummy_sampler,
        glm::mat2x4* a_packed_light_data
    );
    bool init_spirvs(uint32_t a_vk_api_version);
    bool refresh_spirvs();
    void create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, VkRenderPass a_shading_pass);
    void render_imgui_scene_settings(int* a_current_scene_idx);
    void render_imgui_light_guizmo(
        const glm::mat4& a_view,
        const glm::mat4& a_proj,
        const glm::vec2& a_viewport_pos,
        const glm::vec2& a_viewport_dim,
        glm::mat2x4* a_packed_light_data
    );
    void cmd_rebuild_tlas(VkCommandBuffer a_cmd_buf, const glm::mat4& a_gray_dot_transform);
    void cmd_exec_readback_pipeline(VkCommandBuffer a_cmd_buf);

    const buffer_t& host_dev_msg_buffer() const { return host_dev_msg_buffer_; }
    const buffer_t& frame_params_uniform_buffer() const { return frame_params_uniform_buffer_; }
    bool tlas_has_gray_dot() const { return tlas_has_gray_dot_; }
    const scene_t& get_scene() const { return scene_; }
    const svgf_cfg_t& get_svgf_cfg() const { return input_cfg_.svgf_cfg; }
    const reflection_t::cfg_t& get_reflection_cfg() const { return input_cfg_.reflection_cfg; }
    reflection_t::cfg_t* get_reflection_cfg_ptr() { return &input_cfg_.reflection_cfg; }
    uint32_t get_timer_filter_width(const int a_scene_idx) const { return input_cfg_.scene_cfgs[a_scene_idx].timer_filter_width; }
    const ddgi_t::cfg_t& get_ddgi_cfg(const int a_scene_idx) const { return input_cfg_.scene_cfgs[a_scene_idx].ddgi_cfg; }
    ddgi_t::cfg_t* get_ddgi_cfg_ptr(const int a_scene_idx) { return &input_cfg_.scene_cfgs[a_scene_idx].ddgi_cfg; }
    const size_t get_scene_count() const { return input_cfg_.scene_cfgs.size(); }
    const descriptor_set_layout_t* descriptor_set_layouts() const { return descriptor_set_layouts_.data(); }
    const VkDescriptorSet* descriptor_sets() const { return descriptor_sets_.data(); }

};


