#pragma once

#include <spock/utils.hpp>
#include <string>
#include <string_view>

using namespace spock;

#define BASIC_RENDERER_MT_SPIRV_RELOAD      1

enum input_ds_layout_e {
    INPUT_DS_LAYOUT_UI = 0,
    INPUT_DS_LAYOUT_SCENE,
    INPUT_DS_LAYOUT_COUNT
};

enum input_ds_e {
    INPUT_DS_UI = 0,
    INPUT_DS_SCENE,
    INPUT_DS_COUNT
};

enum deferred_ds_layout_e {
    DEFERRED_DS_LAYOUT_ONE_OFF = 0,
    DEFERRED_DS_LAYOUT_TEMPO,
    DEFERRED_DS_LAYOUT_COUNT
};

enum deferred_ds_e {
    DEFERRED_DS_ONE_OFF = 0,
    DEFERRED_DS_TEMPO_0,
    DEFERRED_DS_TEMPO_1,
    DEFERRED_DS_COUNT
};

enum shadow_ds_layout_e {
    SHADOW_DS_LAYOUT_ONE_OFF = 0,
    SHADOW_DS_LAYOUT_TEMPO,
    SHADOW_DS_LAYOUT_1ST_MOMENT,
    SHADOW_DS_LAYOUT_COUNT
};

enum shadow_ds_e {
    SHADOW_DS_ONE_OFF = 0,
    SHADOW_DS_TEMPO_0,
    SHADOW_DS_TEMPO_1,
    SHADOW_DS_1ST_MOMENT_0,
    SHADOW_DS_1ST_MOMENT_1,
    SHADOW_DS_COUNT
};


enum reflection_ds_layout_e {
    REFLECTION_DS_LAYOUT_ONE_OFF = 0,
    REFLECTION_DS_LAYOUT_TEMPO,
    REFLECTION_DS_LAYOUT_COLOR,
    REFLECTION_DS_LAYOUT_COUNT
};

enum reflection_ds_e {
    REFLECTION_DS_ONE_OFF = 0,
    REFLECTION_DS_TEMPO_0,
    REFLECTION_DS_TEMPO_1,
    REFLECTION_DS_COLOR_0,
    REFLECTION_DS_COLOR_1,
    REFLECTION_DS_COUNT
};


enum dev_timer_e {
    DEV_TIMER_FRAME = 0,
    DEV_TIMER_VISIBILITY_SCENE_OPAQUE,
    DEV_TIMER_VISIBILITY_SCENE_ALPHA,
    DEV_TIMER_WRITE_G_BUFFER,
    DEV_TIMER_TLAS_REBUILD,
    DEV_TIMER_DDGI_RAYGEN,
    DEV_TIMER_DDGI_LIGHTING,
    DEV_TIMER_DDGI_UPDATE,
    DEV_TIMER_DDGI_PROBE_VIS,
    DEV_TIMER_SHADOW_RT,
    DEV_TIMER_SHADOW_TEMPO,
    DEV_TIMER_SHADOW_A_TROUS,
    DEV_TIMER_REFLECTION_RT,
    DEV_TIMER_REFLECTION_TEMPO,
    DEV_TIMER_REFLECTION_A_TROUS,
    DEV_TIMER_PRESENT_SHADING,
    DEV_TIMER_PRESENT_IMGUI,
    DEV_TIMER_COUNT
};


struct push_constant_t {
    uint32_t a_trous_iter;
};


// relative path of a_p0 w.r.t. a_p1
std::string relative_path_str(const std::string& a_p0, const std::string& a_p1);

bool
create_spirvs(
    uint32_t a_vk_api_version, 
    uint32_t a_glsl_count, 
    const std::string* a_glsl_relative_paths, 
    const std::string& a_scene_name, 
    spirv_t* a_spirvs
);

bool reload_spirvs(spirv_t* a_spirv_begin, uint32_t a_spirv_count, const std::string& a_msg = {});



