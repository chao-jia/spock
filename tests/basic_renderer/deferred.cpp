#include "deferred.hpp"
#include <spock/utils.hpp>
#include <spock/scene.hpp>
#include <nlohmann/json.hpp>

namespace {

enum spv_e {
    SPV_VISIBILITY_PRIMARY_VERT = 0,
    SPV_VISIBILITY_PRIMARY_FRAG,
    SPV_VISIBILITY_GRAY_DOT_VERT,
    SPV_VISIBILITY_GRAY_DOT_FRAG,
    SPV_VISIBILITY_ALPHA_VERT,
    SPV_VISIBILITY_ALPHA_FRAG,
    SPV_WRITE_G_BUFFER_VERT,
    SPV_WRITE_G_BUFFER_FRAG,
    SPV_SHADING_VERT,
    SPV_SHADING_FRAG,
    SPV_COUNT,
    SPV_G_BUFFER_BEGIN = SPV_VISIBILITY_PRIMARY_VERT,
    SPV_G_BUFFER_COUNT = SPV_WRITE_G_BUFFER_FRAG + 1 - SPV_G_BUFFER_BEGIN,
    SPV_SHADING_BEGIN = SPV_SHADING_VERT,
    SPV_SHADING_COUNT = SPV_SHADING_FRAG + 1 - SPV_SHADING_BEGIN,
}; 


enum ppl_e {
    PPL_VISIBILITY_PRIMARY = 0,
    PPL_VISIBILITY_GRAY_DOT,
    PPL_VISIBILITY_ALPHA,
    PPL_WRITE_G,
    PPL_SHADING,
    PPL_COUNT
};


enum img_e {
    IMG_SHADOW_RAY_ORIGIN_DEPTH = 0,
    IMG_MOTION_VEC,
    IMG_BASE_COLOR_METALLIC,
    IMG_EMISSIVE_OCCLUSION,
    IMG_PRESENT_DEPTH,              //TODO: maybe reuse prev depth

    IMG_DEPTH_0,
    IMG_TRIANGLE_IDX_0,
    IMG_OCT_NORMAL_0,
    IMG_ROUGHNESS_CURVATURE_0,

    IMG_DEPTH_1,
    IMG_TRIANGLE_IDX_1,
    IMG_OCT_NORMAL_1,
    IMG_ROUGHNESS_CURVATURE_1,

    IMG_COUNT,
    IMG_TEMPO_COUNT = IMG_ROUGHNESS_CURVATURE_0 + 1 - IMG_DEPTH_0
};

constexpr uint32_t WRITE_G_ATTACHMENT_COUNT = 6;

inline bool 
is_depth_image(size_t a_image_idx) 
{ 
    return (a_image_idx - IMG_PRESENT_DEPTH) * (a_image_idx - IMG_DEPTH_0) * (a_image_idx - IMG_DEPTH_1) == 0; 
}


} // anonymous namespace


deferred_t::deferred_t(VkDevice a_vk_dev, VkFormat a_depth_format)
{
    const VkFormat formats[] = {
        VK_FORMAT_R32_SFLOAT,       // IMG_SHADOW_RAY_ORIGIN_DEPTH
        VK_FORMAT_R16G16_SNORM,     // IMG_MOTION_VEC
        VK_FORMAT_R8G8B8A8_UNORM,   // IMG_BASE_COLOR_METALLIC
        VK_FORMAT_R8G8B8A8_UNORM,   // IMG_EMISSIVE_OCCLUSION
        a_depth_format,             // IMG_PRESENT_DEPTH

        a_depth_format,         // IMG_DEPTH i
        VK_FORMAT_R32_SINT,     // IMG_TRIANGLE_IDX i
        VK_FORMAT_R16G16_SNORM, // IMG_OCT_NORMAL i
        VK_FORMAT_R16G16_SFLOAT,    // IMG_ROUGHNESS_CURVATURE i

        a_depth_format,         // IMG_DEPTH i
        VK_FORMAT_R32_SINT,     // IMG_TRIANGLE_IDX i
        VK_FORMAT_R16G16_SNORM, // IMG_OCT_NORMAL i
        VK_FORMAT_R16G16_SFLOAT,    // IMG_ROUGHNESS_CURVATURE i
    };
    SPOCK_PARANOID(length_of_c_array(formats) == static_cast<size_t>(IMG_COUNT), "deferred_t::deferred_t: length of formats must be IMG_COUNT");

    image_formats_ = std::make_unique<VkFormat[]>(IMG_COUNT);
    std::copy(formats, formats + IMG_COUNT, image_formats_.get());
    create_descriptor_set_layouts(a_vk_dev);
}


void deferred_t::
create_descriptor_set_layouts(VkDevice a_vk_dev)
{
    const VkShaderStageFlags flags = VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    VkDescriptorSetLayoutBinding one_off_bindings[IMG_DEPTH_0];
    for (size_t i = 0; i < static_cast<size_t>(IMG_DEPTH_0); ++i) {
        one_off_bindings[i] = build_VkDescriptorSetLayoutBinding(i, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, flags);
    }
    descriptor_set_layouts_[DEFERRED_DS_LAYOUT_ONE_OFF] = descriptor_set_layout_t(
        a_vk_dev, 
        build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(one_off_bindings), one_off_bindings)
    );

    VkDescriptorSetLayoutBinding tempo_bindings[IMG_TEMPO_COUNT];
    for (size_t i = 0; i < static_cast<size_t>(IMG_TEMPO_COUNT); ++i) {
        tempo_bindings[i] = build_VkDescriptorSetLayoutBinding(i, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, flags);
    }
    descriptor_set_layouts_[DEFERRED_DS_LAYOUT_TEMPO] = descriptor_set_layout_t(
        a_vk_dev, 
        build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(tempo_bindings), tempo_bindings)
    );

    VkDescriptorPoolSize pool_size{};
    pool_size.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    pool_size.descriptorCount = length_of_c_array(one_off_bindings) + 2 * length_of_c_array(tempo_bindings);

    auto pool_create_info = make_VkDescriptorPoolCreateInfo();
    pool_create_info.maxSets = DEFERRED_DS_COUNT;
    pool_create_info.poolSizeCount = 1;
    pool_create_info.pPoolSizes = &pool_size;

    descriptor_pool_ = descriptor_pool_t(a_vk_dev, pool_create_info);
    const VkDescriptorSetLayout layouts[] = {
        descriptor_set_layouts_[DEFERRED_DS_LAYOUT_ONE_OFF],
        descriptor_set_layouts_[DEFERRED_DS_LAYOUT_TEMPO],
        descriptor_set_layouts_[DEFERRED_DS_LAYOUT_TEMPO],

    };
    SPOCK_PARANOID(
        length_of_c_array(layouts) == static_cast<size_t>(DEFERRED_DS_COUNT), 
        "deferred_t::create_descriptor_set_layouts: mismatch between descriptor set count and descriptor set layout count"
    );
    descriptor_pool_.alloc_descriptor_sets(DEFERRED_DS_COUNT, layouts, descriptor_sets_.data());
}


void deferred_t::
create_render_passes(
    VkDevice a_vk_dev, 
    VkFormat a_surface_format,
    VkImageLayout a_surface_image_final_layout
)
{
    // visibility pass
    {
        VkAttachmentDescription attach_descs[] = {
            build_VkAttachmentDescription(
                image_formats_[IMG_TRIANGLE_IDX_0], 
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, // read by subsequent shading pass
                VK_ATTACHMENT_LOAD_OP_CLEAR, 
                VK_ATTACHMENT_STORE_OP_STORE,
                VK_SAMPLE_COUNT_1_BIT
            ),
            build_VkAttachmentDescription(
                image_formats_[IMG_DEPTH_0], 
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                VK_ATTACHMENT_LOAD_OP_CLEAR, 
                VK_ATTACHMENT_STORE_OP_STORE,
                VK_SAMPLE_COUNT_1_BIT
            )
        };

        const auto attach_color_ref = build_VkAttachmentReference(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        const auto attach_depth_ref = build_VkAttachmentReference(1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
        const auto subpass_desc = build_VkSubpassDescription(1, &attach_color_ref, &attach_depth_ref);

        VkSubpassDependency dependencies[] = {
            build_VkSubpassDependency(
                VK_SUBPASS_EXTERNAL, 
                0,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, // TODO: make it more precise
                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                VK_ACCESS_SHADER_READ_BIT,
                VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
            ),
            build_VkSubpassDependency(
                0,
                VK_SUBPASS_EXTERNAL,
                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                VK_ACCESS_SHADER_READ_BIT
            )
        };

        visibility_pass_ = render_pass_t(a_vk_dev, build_VkRenderPassCreateInfo(
            length_of_c_array(attach_descs), attach_descs,
            1, &subpass_desc,
            length_of_c_array(dependencies), dependencies
        ));
    }
    // write_g_pass
    {
        VkFormat formats[] = {
                image_formats_[IMG_ROUGHNESS_CURVATURE_0],
                image_formats_[IMG_MOTION_VEC],
                image_formats_[IMG_BASE_COLOR_METALLIC],
                image_formats_[IMG_EMISSIVE_OCCLUSION],
                image_formats_[IMG_SHADOW_RAY_ORIGIN_DEPTH],
                image_formats_[IMG_OCT_NORMAL_0],
        };
        static_assert(WRITE_G_ATTACHMENT_COUNT == length_of_c_array(formats), "mismatch between WRITE_G_ATTACHMENT_COUNT and formats count");

        VkAttachmentDescription attach_descs[WRITE_G_ATTACHMENT_COUNT];
        VkAttachmentReference attach_color_refs[WRITE_G_ATTACHMENT_COUNT];

        for (size_t i = 0; i < WRITE_G_ATTACHMENT_COUNT; ++i) {
            attach_descs[i] = build_VkAttachmentDescription(
                formats[i],
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, // read by subsequent shading pass
                VK_ATTACHMENT_LOAD_OP_CLEAR,
                VK_ATTACHMENT_STORE_OP_STORE,
                VK_SAMPLE_COUNT_1_BIT
            );
            attach_color_refs[i] = build_VkAttachmentReference(i, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        }
        
        const auto subpass_desc = build_VkSubpassDescription(WRITE_G_ATTACHMENT_COUNT, attach_color_refs, NULL);

        VkSubpassDependency dependencies[] = {
            build_VkSubpassDependency(
                VK_SUBPASS_EXTERNAL,
                0,
                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                VK_ACCESS_SHADER_READ_BIT
            ),
            build_VkSubpassDependency(
                0,
                VK_SUBPASS_EXTERNAL,
                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR | VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT | VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                VK_ACCESS_SHADER_READ_BIT
            )
        };

        write_g_pass_ = render_pass_t(a_vk_dev, build_VkRenderPassCreateInfo(
            WRITE_G_ATTACHMENT_COUNT, attach_descs,
            1, &subpass_desc,
            length_of_c_array(dependencies), dependencies
        ));
    }
    // shading_pass_
    {
        VkAttachmentDescription attach_descs[] = {
            build_VkAttachmentDescription(
                a_surface_format, 
                a_surface_image_final_layout,
                VK_ATTACHMENT_LOAD_OP_DONT_CARE, 
                VK_ATTACHMENT_STORE_OP_STORE
            ),
            build_VkAttachmentDescription(
                image_formats_[IMG_PRESENT_DEPTH], 
                VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                VK_ATTACHMENT_LOAD_OP_CLEAR, 
                VK_ATTACHMENT_STORE_OP_DONT_CARE
            )
        };

        const auto attach_color_ref = build_VkAttachmentReference(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
        const auto attach_depth_ref = build_VkAttachmentReference(1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
        const auto subpass_desc = build_VkSubpassDescription(1, &attach_color_ref, &attach_depth_ref);

        shading_pass_ = render_pass_t(a_vk_dev, build_VkRenderPassCreateInfo(
            length_of_c_array(attach_descs), attach_descs,
            1, &subpass_desc,
            0, nullptr
        ));
    }
}


bool deferred_t::
init_spirvs(uint32_t a_vk_api_version, const std::string& a_scene_name)
{
    const std::string glsl_relative_paths[] = {
        "deferred/visibility_primary.vert",
        "deferred/visibility_primary.frag",
        "deferred/visibility_gray_dot.vert",
        "deferred/visibility_gray_dot.frag",
        "deferred/visibility_alpha.vert",
        "deferred/visibility_alpha.frag",
        "deferred/write_g_buffer.vert",
        "deferred/write_g_buffer.frag",
        "deferred/shading.vert",
        "deferred/shading.frag",
    };
    constexpr auto glsl_count = length_of_c_array(glsl_relative_paths);
    spirvs_ = std::make_unique<spirv_t[]>(glsl_count);
    return create_spirvs(a_vk_api_version, length_of_c_array(glsl_relative_paths), glsl_relative_paths, a_scene_name, spirvs_.get());
}


bool deferred_t::
refresh_spirvs()
{
    return reload_spirvs(spirvs_.get(), SPV_COUNT, "deferred: ");
}


void deferred_t::
create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, const VkSpecializationInfo* a_ddgi_spec_const)
{
    host_timer_t timer;

    pipelines_ = std::make_unique<pipeline_t[]>(PPL_COUNT);
    shader_module_array_t g_buffer_shader_module_array{ a_vk_dev, spirvs_.get() + SPV_G_BUFFER_BEGIN, SPV_G_BUFFER_COUNT };
    const auto g_buffer_shader_stage_create_infos = build_shader_stage_create_infos(g_buffer_shader_module_array);
    VkGraphicsPipelineCreateInfo graphics_pipeline_create_infos[PPL_COUNT];
    graphics_pipeline_create_info_helper_t helpers[PPL_COUNT];
    const auto color_blend_attachment = build_VkPipelineColorBlendAttachmentState(VK_COLOR_COMPONENT_R_BIT);

    // visibility_primary
    {
        const auto& render_pass = visibility_pass_;
        const auto ppl_idx = PPL_VISIBILITY_PRIMARY;
        const auto spv_first_idx = SPV_VISIBILITY_PRIMARY_VERT;
        const uint32_t num_shader_stages = 2;

        auto& helper = helpers[ppl_idx];
        helper = graphics_pipeline_create_info_helper_t{};
        helper.vertex_input_descriptions = scene_t::get_vertex_input_descriptions(VERTEX_INPUT_POSITION_BIT);
        helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);
        helper.color_blend_attachments.back() = color_blend_attachment;

        graphics_pipeline_create_infos[ppl_idx] = helper.resolve(
            a_vk_dev, render_pass, 0,
            a_pipeline_layout,
            num_shader_stages, g_buffer_shader_stage_create_infos.get() + spv_first_idx
        );
    }
    // visibility_gray_dot
    {
        const auto& render_pass = visibility_pass_;
        const auto ppl_idx = PPL_VISIBILITY_GRAY_DOT;
        const auto spv_first_idx = SPV_VISIBILITY_GRAY_DOT_VERT;
        const uint32_t num_shader_stages = 2;

        auto& helper = helpers[ppl_idx];
        helper = graphics_pipeline_create_info_helper_t{};
        helper.vertex_input_descriptions = scene_t::get_vertex_input_descriptions(VERTEX_INPUT_POSITION_BIT);
        helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);
        helper.color_blend_attachments.back() = color_blend_attachment;

        graphics_pipeline_create_infos[ppl_idx] = helper.resolve(
            a_vk_dev, render_pass, 0,
            a_pipeline_layout,
            num_shader_stages, g_buffer_shader_stage_create_infos.get() + spv_first_idx
        );
    }
    // visibility_alpha
    {
        const auto& render_pass = visibility_pass_;
        const auto ppl_idx = PPL_VISIBILITY_ALPHA;
        const auto spv_first_idx = SPV_VISIBILITY_ALPHA_VERT;
        const uint32_t num_shader_stages = 2;

        auto& helper = helpers[ppl_idx];
        helper = graphics_pipeline_create_info_helper_t{};
        helper.rasterization_state_create_info.cullMode = VK_CULL_MODE_NONE;
        helper.vertex_input_descriptions = scene_t::get_vertex_input_descriptions(VERTEX_INPUT_POSITION_BIT | VERTEX_INPUT_UV_BIT);
        helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);
        helper.color_blend_attachments.back() = color_blend_attachment;

        graphics_pipeline_create_infos[ppl_idx] = helper.resolve(
            a_vk_dev, render_pass, 0,
            a_pipeline_layout,
            num_shader_stages, g_buffer_shader_stage_create_infos.get() + spv_first_idx
        );
    }
    // write_g 
    {
        VkColorComponentFlags comp_flags[] = {
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT, // roughness_curvature
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT, // motion_vec
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT, // base_color_metallic
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT, // emissive_occlusion
            VK_COLOR_COMPONENT_R_BIT, // shadow_ray_origin_depth
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT, // oct_normal
        };
        static_assert(length_of_c_array(comp_flags) == WRITE_G_ATTACHMENT_COUNT, "mismatch between WRITE_G_ATTACHMENT_COUNT and comp_flags");

        build_VkPipelineColorBlendAttachmentState(VK_COLOR_COMPONENT_R_BIT);
        const auto& render_pass = write_g_pass_;
        const auto ppl_idx = PPL_WRITE_G;
        const auto spv_first_idx = SPV_WRITE_G_BUFFER_VERT;
        const uint32_t num_shader_stages = 2;

        auto& helper = helpers[ppl_idx];
        helper = graphics_pipeline_create_info_helper_t{};
        helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);

        helper.color_blend_attachments.clear();
        helper.color_blend_attachments.reserve(WRITE_G_ATTACHMENT_COUNT);
        for (uint32_t i = 0; i < WRITE_G_ATTACHMENT_COUNT; ++i) {
            helper.color_blend_attachments.push_back(build_VkPipelineColorBlendAttachmentState(comp_flags[i]));
        }

        graphics_pipeline_create_infos[ppl_idx] = helper.resolve(
            a_vk_dev, render_pass, 0,
            a_pipeline_layout,
            num_shader_stages, g_buffer_shader_stage_create_infos.get() + spv_first_idx
        );
    }
    // shading
    shader_module_array_t shading_shader_module_array{ a_vk_dev, spirvs_.get() + SPV_SHADING_BEGIN, SPV_SHADING_COUNT };
    const auto shading_shader_stage_create_infos = build_shader_stage_create_infos(shading_shader_module_array, a_ddgi_spec_const);
    {
        const auto& render_pass = shading_pass_;
        const auto ppl_idx = PPL_SHADING;
        const uint32_t shader_stage_count = SPV_SHADING_COUNT;

        auto& helper = helpers[ppl_idx];
        helper = graphics_pipeline_create_info_helper_t{};
        helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);

        graphics_pipeline_create_infos[ppl_idx] = helper.resolve(
            a_vk_dev, render_pass, 0,
            a_pipeline_layout,
            shader_stage_count, shading_shader_stage_create_infos.get()
        );
    }
    spock::create_graphics_pipelines(a_vk_dev, PPL_COUNT, graphics_pipeline_create_infos, pipelines_.get());

    spock::info("deferred_t::create_pipelines took " + std::to_string(timer.elapsed_ms()) + " ms");
}


void deferred_t::
create_framebuffers(
    const context_t& a_context, 
    const VkExtent2D& a_surface_extent, 
    uint32_t a_surface_image_count, 
    const VkImageView* a_surface_image_views,
    VkCommandBuffer a_cmd_buf, 
    VkQueue a_queue

)
{
    write_g_framebuffers_ = {};
    visibility_framebuffers_ = {};
    shading_framebuffers_ = std::make_unique<framebuffer_t[]>(a_surface_image_count);

    image_views_ = std::make_unique<image_view_t[]>(IMG_COUNT);
    images_ = std::make_unique<image_t[]>(IMG_COUNT);

    constexpr VkImageUsageFlags usages[] = {
        VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
    };
    constexpr VkImageAspectFlags aspects[] = { VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_ASPECT_DEPTH_BIT };
    const VkExtent3D image_extent{ a_surface_extent.width, a_surface_extent.height, 1 };

    for (size_t i = 0; i < static_cast<size_t>(IMG_COUNT); ++i) {
        uint32_t flag_idx{ is_depth_image(i) };
        auto image_create_info = build_VkImageCreateInfo(image_formats_[i], image_extent, usages[flag_idx], 1, VK_SAMPLE_COUNT_1_BIT);
        images_[i] = image_t(a_context, image_create_info);
        image_views_[i] = image_view_t(a_context, build_VkImageViewCreateInfo(images_[i], build_VkImageSubresourceRange(aspects[flag_idx])));
    }
    for (size_t i = 0; i < 2; ++i) {
        const size_t offset = i * (IMG_DEPTH_1 - IMG_DEPTH_0);
        // visibility framebuffers
        {
            VkImageView attachments[] = {
                image_views_[IMG_TRIANGLE_IDX_0 + offset],
                image_views_[IMG_DEPTH_0 + offset]
            };
            const auto create_info = build_VkFramebufferCreateInfo(visibility_pass_, length_of_c_array(attachments), attachments, a_surface_extent);
            visibility_framebuffers_[i] = framebuffer_t(a_context, create_info);
        }
        // write_g_framebuffers_
        {
            VkImageView attachments[] = {
                image_views_[IMG_ROUGHNESS_CURVATURE_0 + offset],
                image_views_[IMG_MOTION_VEC],
                image_views_[IMG_BASE_COLOR_METALLIC],
                image_views_[IMG_EMISSIVE_OCCLUSION],
                image_views_[IMG_SHADOW_RAY_ORIGIN_DEPTH],
                image_views_[IMG_OCT_NORMAL_0 + offset],
            };
            static_assert(length_of_c_array(attachments) == WRITE_G_ATTACHMENT_COUNT, "mismatch between WRITE_G_ATTACHMENT_COUNT and attachments");
            const auto create_info = build_VkFramebufferCreateInfo(write_g_pass_, WRITE_G_ATTACHMENT_COUNT, attachments, a_surface_extent);
            write_g_framebuffers_[i] = framebuffer_t(a_context, create_info);
        }
    }
    for (size_t i = 0; i < a_surface_image_count; ++i) {
        VkImageView attachments[] = {
            a_surface_image_views[i],
            image_views_[IMG_PRESENT_DEPTH],
        };
        const auto create_info = build_VkFramebufferCreateInfo(shading_pass_, length_of_c_array(attachments), attachments, a_surface_extent);
        shading_framebuffers_[i] = framebuffer_t(a_context, create_info);
    }

    const VkImageLayout inital_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    const VkImageLayout final_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    VkImageMemoryBarrier image_barriers[] = { // tempo_1
        build_VkImageMemoryBarrier(images_[IMG_DEPTH_1], inital_layout, final_layout, 0, 0, build_VkImageSubresourceRange(VK_IMAGE_ASPECT_DEPTH_BIT)),
        build_VkImageMemoryBarrier(images_[IMG_TRIANGLE_IDX_1], inital_layout, final_layout, 0, 0),
        build_VkImageMemoryBarrier(images_[IMG_OCT_NORMAL_1], inital_layout, final_layout, 0, 0),
        build_VkImageMemoryBarrier(images_[IMG_ROUGHNESS_CURVATURE_1], inital_layout, final_layout, 0, 0),
    };
    auto cmd_buf_begin_info = build_VkCommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    SPOCK_VK_CALL(vkBeginCommandBuffer(a_cmd_buf, &cmd_buf_begin_info));
    vkCmdPipelineBarrier(a_cmd_buf,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0,
        0, nullptr, 0, nullptr, length_of_c_array(image_barriers), image_barriers
    );
    SPOCK_VK_CALL(vkEndCommandBuffer(a_cmd_buf));
    const auto submit_info = build_VkSubmitInfo(1, &a_cmd_buf);
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));
}


void deferred_t::
write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler)
{
    VkDescriptorImageInfo image_infos[IMG_COUNT];
    for (size_t i = 0; i < static_cast<size_t>(IMG_COUNT); ++i) {
        image_infos[i] = build_VkDescriptorImageInfo(a_dummy_sampler, image_views_[i], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }
    VkWriteDescriptorSet writes[IMG_COUNT];
    for (size_t i = 0; i < static_cast<size_t>(IMG_DEPTH_0); ++i) {
        writes[i] = build_VkWriteDescriptorSet(descriptor_sets_[0], i, 1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, image_infos + i);
    }
    for (size_t j = 0; j < 2; ++j) {
        size_t stride = IMG_DEPTH_1 - IMG_DEPTH_0;
        size_t tempo_offset = j * stride;
        for (size_t i = 0; i < stride; ++i) {
            size_t img_offset = IMG_DEPTH_0 + i + tempo_offset;
            const VkDescriptorImageInfo* image_info = image_infos + img_offset;
            writes[img_offset] = build_VkWriteDescriptorSet( descriptor_sets_[j + 1], i, 1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, image_info);
        }
    }
    vkUpdateDescriptorSets(a_vk_dev, IMG_COUNT, writes, 0, nullptr);
}


void deferred_t::
cmd_begin_visibility_pass(VkCommandBuffer a_cmd_buf, const VkRect2D& a_render_area, float a_ndc_far, uint32_t a_is_odd_frame)
{
    VkClearValue clear_color{};
    std::fill(clear_color.color.int32, clear_color.color.int32 + 4, -1);
    VkClearValue clear_depth{};
    clear_depth.depthStencil.depth = a_ndc_far;
    VkClearValue visibility_clear_values[] = { clear_color, clear_depth };

    auto render_pass_begin_info = make_VkRenderPassBeginInfo();
    render_pass_begin_info.renderPass = visibility_pass_;
    render_pass_begin_info.framebuffer = visibility_framebuffers_[a_is_odd_frame];
    render_pass_begin_info.renderArea = a_render_area;
    render_pass_begin_info.clearValueCount = length_of_c_array(visibility_clear_values);
    render_pass_begin_info.pClearValues = visibility_clear_values;

    vkCmdBeginRenderPass(a_cmd_buf, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
}


void deferred_t::
cmd_exec_visibility_pipelines(VkCommandBuffer a_cmd_buf, const scene_t & a_scene, dev_timer_array_t* a_dev_timer_array)
{
    SPOCK_PARANOID(a_dev_timer_array, "deferred_t::cmd_exec_visibility_pipelines: a_dev_timer_array cannot be NULL.");

    const auto& scene_index_offsets = a_scene.index_offsets();
    const auto num_primary_indices = scene_index_offsets[MTL_TYPE_GRAY_DOT];
    const auto num_gray_dot_indices = scene_index_offsets[MTL_TYPE_ALPHA_CUTOFF] - scene_index_offsets[MTL_TYPE_GRAY_DOT];
    const auto num_alpha_indices = scene_index_offsets[MTL_TYPE_COUNT] - scene_index_offsets[MTL_TYPE_ALPHA_CUTOFF];
    const auto scene_vis_opaque_vertex_bindings = a_scene.get_vertex_bindings(VERTEX_INPUT_POSITION_BIT);
    const auto scene_index_binding = a_scene.get_index_binding();

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_VISIBILITY_SCENE_OPAQUE);

    vkCmdBindVertexBuffers(a_cmd_buf, scene_vis_opaque_vertex_bindings.first, scene_vis_opaque_vertex_bindings.count,
        scene_vis_opaque_vertex_bindings.buffers.data(), scene_vis_opaque_vertex_bindings.buffer_offsets.data()
    );
    vkCmdBindIndexBuffer(a_cmd_buf, scene_index_binding.buffer, scene_index_binding.offset, scene_index_binding.index_type);

    if (num_primary_indices > 0) {
        // visibility primary
        vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines_[PPL_VISIBILITY_PRIMARY]);
        vkCmdDrawIndexed(a_cmd_buf, num_primary_indices, 1, 0, 0, 0);
    }
    if (num_gray_dot_indices > 0) {
        // visibility gray_dot
        vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines_[PPL_VISIBILITY_GRAY_DOT]);
        vkCmdDrawIndexed(a_cmd_buf, num_gray_dot_indices, 1, scene_index_offsets[MTL_TYPE_GRAY_DOT], 0, 0);
    }
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_VISIBILITY_SCENE_OPAQUE);

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_VISIBILITY_SCENE_ALPHA);
    if (num_alpha_indices > 0) {
        // visibility alpha
        vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines_[PPL_VISIBILITY_ALPHA]);

        const auto scene_vis_alpha_vertex_bindings = a_scene.get_vertex_bindings(VERTEX_INPUT_POSITION_BIT | VERTEX_INPUT_UV_BIT);
        vkCmdBindVertexBuffers(a_cmd_buf, scene_vis_alpha_vertex_bindings.first, scene_vis_alpha_vertex_bindings.count,
            scene_vis_alpha_vertex_bindings.buffers.data(), scene_vis_alpha_vertex_bindings.buffer_offsets.data()
        );
        vkCmdDrawIndexed(a_cmd_buf, num_alpha_indices, 1, scene_index_offsets[MTL_TYPE_ALPHA_CUTOFF], 0, 0);
    }
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_VISIBILITY_SCENE_ALPHA);
}


void deferred_t::
cmd_begin_write_g_pass(VkCommandBuffer a_cmd_buf, const VkRect2D& a_render_area, uint32_t a_is_odd_frame)
{
    // write_g_pass
    VkClearValue write_g_clear_values[WRITE_G_ATTACHMENT_COUNT] = {};
    auto render_pass_begin_info = make_VkRenderPassBeginInfo();
    render_pass_begin_info.renderPass = write_g_pass_;
    render_pass_begin_info.framebuffer = write_g_framebuffers_[a_is_odd_frame];
    render_pass_begin_info.renderArea = a_render_area;
    render_pass_begin_info.clearValueCount = WRITE_G_ATTACHMENT_COUNT;
    render_pass_begin_info.pClearValues = write_g_clear_values;

    vkCmdBeginRenderPass(a_cmd_buf, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
}


void deferred_t::cmd_exec_wirte_g_pipeline(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines_[PPL_WRITE_G]);
    vkCmdDraw(a_cmd_buf, 3, 1, 0, 0);
}


void deferred_t::
cmd_begin_shading_pass(VkCommandBuffer a_cmd_buf, const VkRect2D & a_render_area, float a_ndc_far, uint32_t a_surface_image_idx)
{
    constexpr VkClearValue clear_color{};
    VkClearValue clear_depth{};
    clear_depth.depthStencil.depth = a_ndc_far;
    VkClearValue clear_values[] = { clear_color, clear_depth };

    auto render_pass_begin_info = make_VkRenderPassBeginInfo();
    render_pass_begin_info.renderPass = shading_pass_;
    render_pass_begin_info.framebuffer = shading_framebuffers_[a_surface_image_idx];
    render_pass_begin_info.renderArea = a_render_area;
    render_pass_begin_info.clearValueCount = length_of_c_array(clear_values);
    render_pass_begin_info.pClearValues = clear_values;

    vkCmdBeginRenderPass(a_cmd_buf, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
}


void deferred_t::cmd_exec_shading_pipeline(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines_[PPL_SHADING]);
    vkCmdDraw(a_cmd_buf, 3, 1, 0, 0);
}



