#include "renderer.hpp"
#include <nlohmann/json.hpp>

#include <ImGuizmo.h>
#include <iostream>
#include <cstdint>
#include <filesystem>
#include <unordered_set>
#include <thread>
#include <atomic>
#include <fstream>
#include <sstream>
#include <stb_image.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/integer.hpp>
#include <glm/ext.hpp>

#if _MSC_VER
#pragma warning(disable : 6011) // dereferencing NULL pointer
#endif

namespace stdfs = std::filesystem;

namespace {

spock::physical_dev_all_features2_t g_all_features2;


inline void glfw_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    auto renderer = reinterpret_cast<renderer_t*>(glfwGetWindowUserPointer(window));
    renderer->key_callback(window, key, scancode, action, mods);
}


inline void glfw_mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    auto renderer = reinterpret_cast<renderer_t*>(glfwGetWindowUserPointer(window));
    renderer->mouse_button_callback(window, button, action, mods);
}


inline void glfw_scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    auto renderer = reinterpret_cast<renderer_t*>(glfwGetWindowUserPointer(window));
    renderer->scroll_callback(window, xoffset, yoffset);
}


struct gray_dot_gizmo_t {
    ImGuizmo::MODE mode{ ImGuizmo::LOCAL };
    ImGuizmo::OPERATION op{ ImGuizmo::TRANSLATE };

    // return true if a_transform is changed by this call
    bool prepare(
        const glm::mat4& a_view, 
        const glm::mat4& a_proj, 
        const glm::vec2& a_viewport_pos, 
        const glm::vec2& a_viewport_dim, 
        glm::mat4* a_transform
    )
    {
        if (ImGui::RadioButton("Translate", op == ImGuizmo::TRANSLATE)) {
            op = ImGuizmo::TRANSLATE;
        }
        ImGui::SameLine();
        if (ImGui::RadioButton("Rotate", op == ImGuizmo::ROTATE)) {
            op = ImGuizmo::ROTATE;
        }
        ImGui::SameLine();
        if (ImGui::RadioButton("Scale", op == ImGuizmo::SCALE)) {
            op = ImGuizmo::SCALE;
        }

        std::array<float, 3> t {}, r {}, s {};
        ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(*a_transform), t.data(), r.data(), s.data());
        bool tr_changed = ImGui::DragFloat3("Tr", t.data(), 0.01f);
        bool rt_changed = ImGui::DragFloat3("Rt", r.data(), 1.f);
        bool sc_changed = ImGui::DragFloat3("Sc", s.data(), 0.01f);
        ImGuizmo::RecomposeMatrixFromComponents(t.data(), r.data(), s.data(), glm::value_ptr(*a_transform));

        if (op != ImGuizmo::SCALE) {
            if (ImGui::RadioButton("Local", mode == ImGuizmo::LOCAL)) {
                mode = ImGuizmo::LOCAL;
            }
            ImGui::SameLine();
            if (ImGui::RadioButton("World", mode == ImGuizmo::WORLD)) {
                mode = ImGuizmo::WORLD;
            }
        }
        ImGuizmo::SetRect(a_viewport_pos.x, a_viewport_pos.y, a_viewport_dim.x, a_viewport_dim.y);
        bool manip_changed = ImGuizmo::Manipulate(glm::value_ptr(a_view), glm::value_ptr(a_proj), op, mode, glm::value_ptr(*a_transform));
        return tr_changed || rt_changed || sc_changed || manip_changed;
    }
};


void copy_ddgi_params(glsl_shared::frame_params_t* a_dst, const ddgi_t::cfg_t* a_src)
{
    a_dst->ddgi_depth_sharpness = a_src->depth_sharpness;
    a_dst->ddgi_energy_preservation = a_src->energy_preservation;
    a_dst->ddgi_probe_intensity = a_src->probe_intensity;
    a_dst->ddgi_hysteresis = a_src->hysteresis;
    a_dst->ddgi_normal_bias = a_src->normal_bias;
}


void copy_reflection_params(glsl_shared::frame_params_t* a_dst, const reflection_t::cfg_t* a_src)
{
    a_dst->reflect_geometry_weight_factor = a_src->geometry_weight_factor;
    a_dst->reflect_accum_frame_count_max = a_src->accum_frame_count_max;
    a_dst->reflect_accum_base_power = a_src->accum_base_power;
    a_dst->reflect_accum_curve = a_src->accum_curve;
    a_dst->reflect_temporal_weight_threshold = a_src->temporal_weight_threshold;
}


void copy_svgf_params(glsl_shared::frame_params_t* a_dst, const svgf_cfg_t* a_src)
{
    a_dst->svgf_temporal_alpha = a_src->temporal_alpha;
    a_dst->svgf_bilateral_rcp_sigma_d_sq = 1.f / (a_src->bilateral_sigma_d * a_src->bilateral_sigma_d);
    a_dst->svgf_bilateral_rcp_sigma_p_sq = 1.f / (a_src->bilateral_sigma_p * a_src->bilateral_sigma_p);
    a_dst->svgf_bilateral_rcp_sigma_n_sq = 1.f / (a_src->bilateral_sigma_n * a_src->bilateral_sigma_n);
    a_dst->svgf_edge_stop_rcp_sigma_p_sq = 1.f / (a_src->edge_stop_sigma_p * a_src->edge_stop_sigma_p);
    a_dst->svgf_edge_stop_sigma_n = a_src->edge_stop_sigma_n;
    a_dst->svgf_edge_stop_sigma_l = a_src->edge_stop_sigma_l;
}


enum spk_event_e{
    SPK_EVENT_CAMERA_BACKWARD = 0,
    SPK_EVENT_CAMERA_DOWN,
    SPK_EVENT_CAMERA_FORWARD,
    SPK_EVENT_CAMERA_LEFT,
    SPK_EVENT_CAMERA_RIGHT,
    SPK_EVENT_CAMERA_ROTATE,
    SPK_EVENT_CAMERA_ROTATE_DOWN,
    SPK_EVENT_CAMERA_ROTATE_LEFT,
    SPK_EVENT_CAMERA_ROTATE_RIGHT,
    SPK_EVENT_CAMERA_ROTATE_UP,
    SPK_EVENT_CAMERA_UP,
    SPK_EVENT_DDGI_RAY_DIR_RAND_NEXT,
    SPK_EVENT_DDGI_PROBE_VIS_RAY_DIR,
    SPK_EVENT_DDGI_PROBE_VIS_NEXT_MAP,
    SPK_EVENT_DDGI_PROBE_VIS_PREV_MAP,
    SPK_EVENT_DDGI_UPDATE_REQ,
    SPK_EVENT_GRAY_DOT_SELECTED,
    SPK_EVENT_GRAY_DOT_TO_EYE,
    SPK_EVENT_GRAY_DOT_MOVED,
    SPK_EVENT_HOST_DEV_MSG_REQ,
    SPK_EVENT_HOST_DEV_MSG_RES,
    SPK_EVENT_LIGHT_SELECTED,
    SPK_EVENT_MOVE_MODE_SLOW,
    SPK_EVENT_MOVE_MODE_FAST,
    SPK_EVENT_QUIT,
    SPK_EVENT_RELOAD_SHADERS,
    SPK_EVENT_RESIZE_WINDOW,
    SPK_EVENT_SWITCH_SCENE,
    SPK_EVENT_TOGGLE_DDGI,
    SPK_EVENT_TOGGLE_DDGI_PROBE_VIS,
    SPK_EVENT_TOGGLE_DDGI_STEPWISE_RAY_DIR_PERTURB,
    SPK_EVENT_TOGGLE_DDGI_STEPWISE_UPDATE,
    SPK_EVENT_TOGGLE_FULLSCREEN,
    SPK_EVENT_TOGGLE_IMGUI,
    SPK_EVENT_TOGGLE_LIGHT_SELECTION,
    SPK_EVENT_TOGGLE_PAUSE,
    SPK_EVENT_TOGGLE_REVERSE_Z,
    SPK_EVENT_COUNT
};

enum spk_cursor_position_e {
    SPK_CURSOR_POS_CURR = 0,
    SPK_CURSOR_POS_PREV,
    SPK_CURSOR_POS_COUNT
};

enum host_perf_timer_e {
    HOST_PERF_TIMER_FRAME = 0,
    HOST_PERF_TIMER_CMD_BUF,
    HOST_PERF_TIMER_IMGUI_PREPARE,
    HOST_PERF_TIMER_UBO_UPLOAD,
    HOST_PERF_TIMER_RENDER_SUBMIT,
    HOST_PERF_TIMER_PRESENT_SUBMIT,
    HOST_PERF_TIMER_COUNT
};

} // anonymous namespace


void renderer_t::
create_pipeline_layout()
{
    const auto& input_ds_layouts = input_.descriptor_set_layouts();
    const auto& deferred_ds_layouts = deferred_.descriptor_set_layouts();
    const auto& shadow_ds_layouts = shadow_.descriptor_set_layouts();
    const auto& reflection_ds_layouts = reflection_.descriptor_set_layouts();

    VkDescriptorSetLayout ds_layouts[] = {
            input_ds_layouts[INPUT_DS_LAYOUT_UI],
            input_ds_layouts[INPUT_DS_LAYOUT_SCENE],
            deferred_ds_layouts[DEFERRED_DS_LAYOUT_ONE_OFF],
            deferred_ds_layouts[DEFERRED_DS_LAYOUT_TEMPO],
            deferred_ds_layouts[DEFERRED_DS_LAYOUT_TEMPO],
            ddgi_.descriptor_set_layout(),
            shadow_ds_layouts[SHADOW_DS_LAYOUT_ONE_OFF],
            shadow_ds_layouts[SHADOW_DS_LAYOUT_TEMPO],
            shadow_ds_layouts[SHADOW_DS_LAYOUT_TEMPO],
            shadow_ds_layouts[SHADOW_DS_LAYOUT_1ST_MOMENT],
            shadow_ds_layouts[SHADOW_DS_LAYOUT_1ST_MOMENT],
            reflection_ds_layouts[REFLECTION_DS_LAYOUT_ONE_OFF],
            reflection_ds_layouts[REFLECTION_DS_LAYOUT_TEMPO],
            reflection_ds_layouts[REFLECTION_DS_LAYOUT_TEMPO],
            reflection_ds_layouts[REFLECTION_DS_LAYOUT_COLOR],
            reflection_ds_layouts[REFLECTION_DS_LAYOUT_COLOR],
    };
    constexpr uint32_t COUNT = length_of_c_array(ds_layouts);
    VkPushConstantRange pc_ranges[] = {
        build_VkPushConstantRange(VK_SHADER_STAGE_COMPUTE_BIT, sizeof(push_constant_t), 0),
    };
    const auto create_info = build_VkPipelineLayoutCreateInfo(COUNT, ds_layouts, length_of_c_array(pc_ranges), pc_ranges);
    pipeline_layout_ = pipeline_layout_t(context_, create_info);
}


void renderer_t::
write_sbt()
{
    host_timer_t timer;

    auto sbt_staging = buffer_t(context_,
        build_VkBufferCreateInfo(sbt_bytes_, VK_BUFFER_USAGE_TRANSFER_SRC_BIT),
        build_VmaAllocationCreateInfo(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
    );
    sbt_staging.map();
    auto sbt_host_addr = sbt_staging.mapped_data<uint8_t>();
    auto sbt_dev_addr = context_.get_buffer_device_address(sbt_);
    memset(sbt_host_addr, 0, sbt_bytes_);

    ddgi_.write_sbt(sbt_entry_size_, &sbt_host_addr, &sbt_dev_addr);
    shadow_.write_sbt(sbt_entry_size_, &sbt_host_addr, &sbt_dev_addr);
    reflection_.write_sbt(sbt_entry_size_, &sbt_host_addr, &sbt_dev_addr);

    auto sbt_copy_region = build_VkBufferCopy(sbt_bytes_);
    const auto cmd_buf_begin_info = build_VkCommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf_, &cmd_buf_begin_info));
    vkCmdCopyBuffer(cmd_buf_, sbt_staging, sbt_, 1, &sbt_copy_region);
    SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf_));
    const auto submit_info = build_VkSubmitInfo(1, &cmd_buf_());
    SPOCK_VK_CALL(vkQueueSubmit(queue_, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(queue_));

    spock::info("write_sbt took " + std::to_string(timer.elapsed_ms()) + " ms");
}


renderer_t::
renderer_t(const spock::ordered_json& a_config_json)
    : window_(a_config_json["window"])
    , camera_(window_.aspect_ratio(), a_config_json["camera"])
    , context_(a_config_json["vulkan"], g_all_features2, &window_, nullptr)
    , swapchain_(context_, VkExtent2D{ static_cast<uint32_t>(window_.width()), static_cast<uint32_t>(window_.height()) })
    , dummy_sampler_(context_)
    , queue_(context_.get_queue(spock::queue_t::FLAGS_GENERAL, VK_TRUE))
    , command_pool_(context_, queue_.family_index())
    , cmd_buf_(command_pool_)
    , render_done_semaphore_(context_)
    , image_ready_semaphore_(context_)
    , render_done_fence_(context_, VK_FENCE_CREATE_SIGNALED_BIT)
    , sbt_entry_size_{ context_.calc_sbt_entry_size() }
    , sbt_bytes_{ ddgi_t::get_sbt_bytes(sbt_entry_size_.stride) + shadow_t::get_sbt_bytes(sbt_entry_size_.stride) + reflection_t::get_sbt_bytes(sbt_entry_size_.stride) }
    , events_(std::make_unique<bool[]>(SPK_EVENT_COUNT))
    , cursor_positions_(std::make_unique<glm::dvec2[]>(SPK_CURSOR_POS_COUNT))
    , cursor_offset_{}
    , scroll_offset_{}
    , frame_params_{}
    , depth_format_{ VK_FORMAT_UNDEFINED }
    , current_scene_idx_{ 0 }
    , ddgi_probe_vis_map_idx_{ 0 }
    , frame_ms_{ 0.f }
    , paused_ { true }
    , imgui_on_{ true }
    , ddgi_on_{ true }
    , ddgi_probe_vis_on_{ true }
    , ddgi_stepwise_ray_dir_perturb_on_{ true }
    , ddgi_stepwise_update_on_{ true }
    , glsl_valid_{ true }
{
    const auto& feature = g_all_features2.PhysicalDeviceAccelerationStructureFeaturesKHR;

    std::cout << std::boolalpha
        << "\naccelerationStructure: " << (bool)feature.accelerationStructure
        << "\naccelerationStructureCaptureReplay: " << (bool)feature.accelerationStructureCaptureReplay
        << "\naccelerationStructureIndirectBuild: " << (bool)feature.accelerationStructureIndirectBuild
        << "\naccelerationStructureHostCommands: " << (bool)feature.accelerationStructureHostCommands
        << "\ndescriptorBindingAccelerationStructureUpdateAfterBind: " << (bool)feature.descriptorBindingAccelerationStructureUpdateAfterBind
        << "\n\n";

    sbt_ = buffer_t(context_,
        build_VkBufferCreateInfo(sbt_bytes_, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR),
        build_VmaAllocationCreateInfo(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
    );

    const std::vector< std::pair<std::string, float> > font_path_size_pairs = {
        { std::string(IMGUI_EMBEDED_FONT_DIR"/DroidSans.ttf"), 14.f },
        { std::string(IMGUI_EMBEDED_FONT_DIR"/Cousine-Regular.ttf"), 14.f }
    };
    imgui_ = imgui_t(font_path_size_pairs, BASIC_RENDERER_IMGUI_INI);

    const float font_scale = window_.get_scale_factor();
    const float aspect = swapchain_.image_aspect_ratio();
    camera_ = camera_t(aspect, a_config_json["camera"]);

#ifdef NDEBUG
    app_info_ = "[release] ";
#else
    app_info_ = "[debug] ";
#endif
    app_info_ += "vk debugging: ";
    app_info_ += context_.instance().is_debugging_off() ?  "off" : "on";
    
    glfwSetWindowUserPointer(window_, this);
    glfwSetKeyCallback(window_, glfw_key_callback);
    glfwSetMouseButtonCallback(window_, glfw_mouse_button_callback);
    glfwSetScrollCallback(window_, glfw_scroll_callback);

    auto depth_format_strs = a_config_json["render"]["depth_formats"].get<std::vector<std::string_view>>();
    std::vector<VkFormat> depth_format_candicates(depth_format_strs.size());
    std::transform(
        depth_format_strs.begin(), depth_format_strs.end(),
        depth_format_candicates.begin(),
        [](std::string_view format_str) { return str_to_VkFormat(format_str); }
    );
    depth_format_ = context_.supported_format(
        depth_format_candicates,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT,
        VK_IMAGE_TILING_OPTIMAL
    );
    spock::info(std::string("depth format: ") + str_from_VkFormat(depth_format_).data());

    const auto surface_format = swapchain_.surface_format();
    const auto format_str = std::string{ str_from_VkFormat(surface_format.format) };
    const auto color_space_str = std::string{ str_from_VkColorSpaceKHR(surface_format.colorSpace) };
    const std::string present_mode_str{ str_from_VkPresentModeKHR(swapchain_.present_mode()) };
    spock::info("surface format: (" + format_str + ", " + color_space_str + ")");
    spock::info("present mode: " + present_mode_str);

    deferred_ = deferred_t(context_, depth_format_);
    input_ = input_t(
        a_config_json, context_, cmd_buf_, queue_, 
        sizeof(glsl_shared::frame_params_t), sizeof(glsl_shared::host_dev_msg_t), BLUE_NOISE_TEXTURE_COUNT
    );
    shadow_ = shadow_t(context_);
    ddgi_ = ddgi_t(context_);
    reflection_ = reflection_t(context_);

    deferred_.create_render_passes(context_, surface_format.format);
    deferred_.create_framebuffers(context_, swapchain_.image_extent(), swapchain_.image_count(), swapchain_.vk_image_views(), cmd_buf_, queue_);
    deferred_.write_descriptor_sets(context_, dummy_sampler_);
    shadow_.create_images(context_, swapchain_.image_extent(), cmd_buf_, queue_);
    shadow_.write_descriptor_sets(context_, dummy_sampler_);
    reflection_.create_images(context_, swapchain_.image_extent(), cmd_buf_, queue_);
    reflection_.write_descriptor_sets(context_, dummy_sampler_);

    imgui_.init(window_, context_, cmd_buf_, queue_, deferred_.shading_pass(), 0, swapchain_.image_count(), VK_SAMPLE_COUNT_1_BIT);

    current_scene_idx_ = a_config_json["default_scene_idx"].get<int>();
    spock::ensure(current_scene_idx_ >= 0 && current_scene_idx_ < input_.get_scene_count(), "config.default_scene_idx is invalid");

    frame_params_.ddgi_probe_vis_option = DDGI_PROBE_VIS_RAY_DIR_FLAG | DDGI_PROBE_VIS_MAP_IRRADIANCE;
    frame_params_.ddgi_probe_vis_radius = 0.2f;

    change_scene();
}


void renderer_t::
change_scene()
{
    namespace stdfs = std::filesystem;

    SPOCK_PARANOID(current_scene_idx_ >= 0 && current_scene_idx_ < input_.get_scene_count(), "config.default_scene_idx is invalid");
    const auto timer_filter_width = input_.get_timer_filter_width(current_scene_idx_);

    dev_timer_array_ = dev_timer_array_t(context_, true, DEV_TIMER_COUNT, timer_filter_width, swapchain_.image_count(), 4000);
    spock::info("device timer filter width: " + std::to_string(timer_filter_width));

    host_perf_timers_ = std::make_unique<host_perf_timer_t[]>(HOST_PERF_TIMER_COUNT);
    for (size_t i = 0; i < static_cast<size_t>(HOST_PERF_TIMER_COUNT); ++i) {
        host_perf_timers_[i] = host_perf_timer_t(timer_filter_width, 4000);
    }
    events_ = std::make_unique<bool[]>(SPK_EVENT_COUNT);

    uint32_t ddgi_probe_vis_option = frame_params_.ddgi_probe_vis_option;
    float ddgi_probe_vis_radius = frame_params_.ddgi_probe_vis_radius;
    frame_params_ = glsl_shared::frame_params_t{};
    frame_params_.ddgi_probe_vis_option = ddgi_probe_vis_option;
    frame_params_.ddgi_probe_vis_radius = ddgi_probe_vis_radius;
    frame_params_.t_gray_dot = glm::identity<glm::mat4>();

    input_.init_scene(current_scene_idx_, context_, command_pool_, queue_, dummy_sampler_, &frame_params_.light_data);
    const scene_t& scene = input_.get_scene();
    const auto& ddgi_cfg = input_.get_ddgi_cfg(current_scene_idx_);
    ddgi_.init_params(
        ddgi_cfg.probe_grid_dim, 
        scene.calc_aabb(), scene.aabb_lower() + ddgi_cfg.probe_grid_offset,
        ddgi_cfg.probe_surfel_resolution,
        glm::uvec2(DDGI_UPDATE_DEPTH_WORK_GROUP_SIZE_X, DDGI_UPDATE_DEPTH_WORK_GROUP_SIZE_Y),
        glm::uvec2(DDGI_UPDATE_IRRADIANCE_WORK_GROUP_SIZE_X, DDGI_UPDATE_IRRADIANCE_WORK_GROUP_SIZE_Y)
    );
    SPOCK_PARANOID(
        ddgi_.per_probe_ray_count() <= DDGI_PER_PROBE_RAY_COUNT_MAX, 
        "ddgi_t::ctor: per_probe_ray_count_ cannot be larger than DDGI_PER_PROBE_RAY_COUNT_MAX"
    );
    ddgi_.create_images(context_);
    ddgi_.reset_images(context_, cmd_buf_, queue_);
    ddgi_.write_descriptor_sets(context_, dummy_sampler_);

    copy_ddgi_params(&frame_params_, &ddgi_cfg);
    frame_params_.ddgi_is_first_frame = 1;

    create_pipeline_layout();

    const auto vk_api_version = context_.vk_api_version();

    const bool input_glsl_valid = input_.init_spirvs(vk_api_version);
    input_.create_pipelines(context_, pipeline_layout_, deferred_.shading_pass());
    const bool deferred_glsl_valid = deferred_.init_spirvs(vk_api_version, scene.identifier());
    deferred_.create_pipelines(context_, pipeline_layout_, &ddgi_.get_ray_tracing_specialization_constant().info());
    const bool ddgi_glsl_valid = ddgi_.init_spirvs(vk_api_version, scene.identifier());
    ddgi_.create_pipelines(context_, pipeline_layout_, sbt_entry_size_.shader_group_handle_size, deferred_.shading_pass());
    const bool shadow_glsl_valid = shadow_.init_spirvs(vk_api_version, scene.identifier());
    shadow_.create_pipelines(context_, pipeline_layout_, sbt_entry_size_.shader_group_handle_size);
    const bool reflection_glsl_valid = reflection_.init_spirvs(vk_api_version, scene.identifier());
    reflection_.create_pipelines(context_, pipeline_layout_, sbt_entry_size_.shader_group_handle_size);
    glsl_valid_ = input_glsl_valid && deferred_glsl_valid && ddgi_glsl_valid && shadow_glsl_valid && reflection_glsl_valid;

    write_sbt();

}


void renderer_t::
resize()
{
    vkDeviceWaitIdle(context_);

    VkExtent2D extent{ static_cast<uint32_t>(window_.width()), static_cast<uint32_t>(window_.height()) };

    SPOCK_ENSURE(swapchain_.resize(extent), "swapchain_.resize failed"); // call VkDeviceWaitIdle() internally
    deferred_.create_framebuffers(context_, extent, swapchain_.image_count(), swapchain_.vk_image_views(), cmd_buf_, queue_);
    deferred_.write_descriptor_sets(context_, dummy_sampler_);
    shadow_.create_images(context_, extent, cmd_buf_, queue_);
    shadow_.write_descriptor_sets(context_, dummy_sampler_);
    reflection_.create_images(context_, extent, cmd_buf_, queue_);
    reflection_.write_descriptor_sets(context_, dummy_sampler_);
}


bool renderer_t::
reload_shaders()
{
    if (!glsl_valid_) {
        spock::warn("not all glsl files are valid, on-the-fly shader compile is disabled");
        return false;
    }
    vkDeviceWaitIdle(context_);

    bool sbt_outdated = false;
    bool shader_reloaded = false;
    if (input_.refresh_spirvs()) {
        input_.create_pipelines(context_, pipeline_layout_, deferred_.shading_pass());
        shader_reloaded = true;
    }
    if (deferred_.refresh_spirvs()) {
        deferred_.create_pipelines(context_, pipeline_layout_, &ddgi_.get_ray_tracing_specialization_constant().info());
        shader_reloaded = true;
    }
    if (ddgi_.refresh_spirvs()) {
        ddgi_.create_pipelines(context_, pipeline_layout_, sbt_entry_size_.shader_group_handle_size, deferred_.shading_pass());
        sbt_outdated = true;
        shader_reloaded = true;
    }
    if (shadow_.refresh_spirvs()) {
        shadow_.create_pipelines(context_, pipeline_layout_, sbt_entry_size_.shader_group_handle_size);
        sbt_outdated = true;
        shader_reloaded = true;
    }
    if (reflection_.refresh_spirvs()) {
        reflection_.create_pipelines(context_, pipeline_layout_, sbt_entry_size_.shader_group_handle_size);
        sbt_outdated = true;
        shader_reloaded = true;
    }
    if (sbt_outdated) {
        write_sbt();
    }
    return shader_reloaded;
}


void renderer_t::
record_command_buffer(uint32_t a_image_idx)
{
    host_perf_timers_[HOST_PERF_TIMER_CMD_BUF].start();

    auto cmd_buf_begin_info = make_VkCommandBufferBeginInfo();
    cmd_buf_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf_, &cmd_buf_begin_info));

    dev_timer_array_.cmd_start(cmd_buf_, DEV_TIMER_FRAME);

    const auto surface_extent = swapchain_.image_extent();
    const VkViewport viewport = build_VkViewport(static_cast<float>(surface_extent.width), static_cast<float>(surface_extent.height));
    const VkRect2D render_area = build_VkRect2D(surface_extent.width, surface_extent.height);
    vkCmdSetViewport(cmd_buf_, 0, 1, &viewport);
    vkCmdSetScissor(cmd_buf_, 0, 1, &render_area);
    vkCmdSetDepthCompareOpEXT(cmd_buf_, camera_.depth_compare_op());

    const uint32_t is_odd_frame = frame_params_.render_frame_idx & 1u;
    const VkDescriptorSet desc_sets[] = {
        input_.descriptor_sets()[INPUT_DS_UI],
        input_.descriptor_sets()[INPUT_DS_SCENE],
        deferred_.descriptor_sets()[DEFERRED_DS_ONE_OFF],
        deferred_.descriptor_sets()[DEFERRED_DS_TEMPO_0 + is_odd_frame],
        deferred_.descriptor_sets()[DEFERRED_DS_TEMPO_1 - is_odd_frame],
        ddgi_.descriptor_set(),
        shadow_.descriptor_sets()[SHADOW_DS_ONE_OFF],
        shadow_.descriptor_sets()[SHADOW_DS_TEMPO_0 + is_odd_frame],
        shadow_.descriptor_sets()[SHADOW_DS_TEMPO_1 - is_odd_frame],
        shadow_.descriptor_sets()[SHADOW_DS_1ST_MOMENT_0 + is_odd_frame],
        shadow_.descriptor_sets()[SHADOW_DS_1ST_MOMENT_1 - is_odd_frame],
        reflection_.descriptor_sets()[REFLECTION_DS_ONE_OFF],
        reflection_.descriptor_sets()[REFLECTION_DS_TEMPO_0 + is_odd_frame],
        reflection_.descriptor_sets()[REFLECTION_DS_TEMPO_1 - is_odd_frame],
        reflection_.descriptor_sets()[REFLECTION_DS_COLOR_0 + is_odd_frame],
        reflection_.descriptor_sets()[REFLECTION_DS_COLOR_1 - is_odd_frame]

    };
    constexpr uint32_t shadow_ds_1st_moment_0_idx = 9u;
    constexpr uint32_t reflection_ds_color_0_idx = 14u;
    constexpr uint32_t desc_set_count = length_of_c_array(desc_sets);

    vkCmdBindDescriptorSets(cmd_buf_, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout_, 0, desc_set_count, desc_sets, 0, nullptr);
    vkCmdBindDescriptorSets(cmd_buf_, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline_layout_, 0, desc_set_count, desc_sets, 0, nullptr);
    vkCmdBindDescriptorSets(cmd_buf_, VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipeline_layout_, 0, desc_set_count, desc_sets, 0, nullptr);

    // visibility pass
    deferred_.cmd_begin_visibility_pass(cmd_buf_, render_area, camera_.ndc_far(), is_odd_frame);
    deferred_.cmd_exec_visibility_pipelines(cmd_buf_, input_.get_scene(), &dev_timer_array_);
    vkCmdEndRenderPass(cmd_buf_); // visibility pass
    // write_g pass
    dev_timer_array_.cmd_start(cmd_buf_, DEV_TIMER_WRITE_G_BUFFER);
    deferred_.cmd_begin_write_g_pass(cmd_buf_, render_area, is_odd_frame);
    deferred_.cmd_exec_wirte_g_pipeline(cmd_buf_);
    vkCmdEndRenderPass(cmd_buf_); // write_g pass
    dev_timer_array_.cmd_stop(cmd_buf_, DEV_TIMER_WRITE_G_BUFFER);

    dev_timer_array_.cmd_start(cmd_buf_, DEV_TIMER_TLAS_REBUILD);
    if (events_[SPK_EVENT_GRAY_DOT_MOVED] && input_.tlas_has_gray_dot()) {
        input_.cmd_rebuild_tlas(cmd_buf_, frame_params_.t_gray_dot);
    }
    dev_timer_array_.cmd_stop(cmd_buf_, DEV_TIMER_TLAS_REBUILD);
    
    VkMemoryBarrier barrier0 = build_VkMemoryBarrier(
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR, 
        VK_ACCESS_SHADER_READ_BIT
    );
    vkCmdPipelineBarrier(cmd_buf_, 
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR, 
        VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT | VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR, 
        0, 1, &barrier0, 0, nullptr, 0, nullptr
    );

    if (ddgi_on_) {
       const bool ddgi_update_on = !ddgi_stepwise_update_on_ || events_[SPK_EVENT_DDGI_UPDATE_REQ];
       ddgi_.cmd_compute_irradiance_field(cmd_buf_, &dev_timer_array_, ddgi_update_on);
       frame_params_.ddgi_is_first_frame = (frame_params_.ddgi_is_first_frame && !ddgi_update_on);
    }
    else {
        ddgi_.cmd_dummy_compute(cmd_buf_, &dev_timer_array_);
    }
    if (events_[SPK_EVENT_HOST_DEV_MSG_REQ]) {
        input_.cmd_exec_readback_pipeline(cmd_buf_);
        events_[SPK_EVENT_HOST_DEV_MSG_REQ] = false;
        events_[SPK_EVENT_HOST_DEV_MSG_RES] = true;
    }
    shadow_.cmd_exec_shadow_pipelines(
        cmd_buf_, pipeline_layout_, shadow_ds_1st_moment_0_idx, 
        static_cast<uint32_t>(input_.get_svgf_cfg().a_trous_iteration_count), to_uvec2(surface_extent),
        glm::uvec2(SHADOW_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_X, SHADOW_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_Y),
        glm::uvec2(SHADOW_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_X, SHADOW_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_Y),
        glm::uvec2(SHADOW_A_TROUS_WORK_GROUP_SIZE_X, SHADOW_A_TROUS_WORK_GROUP_SIZE_Y),
        &dev_timer_array_
    );
    reflection_.cmd_exec_reflection_pipelines(
        cmd_buf_, pipeline_layout_, reflection_ds_color_0_idx,
        static_cast<uint32_t>(input_.get_svgf_cfg().a_trous_iteration_count), to_uvec2(surface_extent),
        glm::uvec2(REFLECTION_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_X, REFLECTION_ESTIMATE_VARIANCE_WORK_GROUP_SIZE_Y),
        glm::uvec2(REFLECTION_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_X, REFLECTION_VARIANCE_GAUSSIAN_WORK_GROUP_SIZE_Y),
        glm::uvec2(REFLECTION_A_TROUS_WORK_GROUP_SIZE_X, REFLECTION_A_TROUS_WORK_GROUP_SIZE_Y),
        &dev_timer_array_
    );
    VkMemoryBarrier barrier1 = build_VkMemoryBarrier(VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    vkCmdPipelineBarrier(cmd_buf_, 
        VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT | VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR, 
        VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT | VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 
        1, &barrier1, 0, nullptr, 0, nullptr
    );

    // shading pass
    deferred_.cmd_begin_shading_pass(cmd_buf_, render_area, camera_.ndc_far(), a_image_idx);

    dev_timer_array_.cmd_start(cmd_buf_, DEV_TIMER_DDGI_PROBE_VIS);
    if (ddgi_probe_vis_on_) {
        ddgi_.cmd_exec_vis_probe_pipeline(cmd_buf_);
    }
    dev_timer_array_.cmd_stop(cmd_buf_, DEV_TIMER_DDGI_PROBE_VIS);

    dev_timer_array_.cmd_start(cmd_buf_, DEV_TIMER_PRESENT_SHADING);
    deferred_.cmd_exec_shading_pipeline(cmd_buf_);
    dev_timer_array_.cmd_stop(cmd_buf_, DEV_TIMER_PRESENT_SHADING);

    dev_timer_array_.cmd_start(cmd_buf_, DEV_TIMER_PRESENT_IMGUI);
    if (imgui_on_) {
        imgui_.record_command_buffer(cmd_buf_);
    }
    dev_timer_array_.cmd_stop(cmd_buf_, DEV_TIMER_PRESENT_IMGUI);

    vkCmdEndRenderPass(cmd_buf_);// shading pass

    dev_timer_array_.cmd_stop(cmd_buf_, DEV_TIMER_FRAME);

    SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf_));
    host_perf_timers_[HOST_PERF_TIMER_CMD_BUF].stop();
}


/************************************************************************************* 
========================================= ui ========================================= 
**************************************************************************************/

void renderer_t::
key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (imgui_on_ && imgui_.want_keys() && action != GLFW_RELEASE) {
        return;
    }

    const static std::unordered_map<int, spk_event_e> press_key_event_map = {
        {GLFW_KEY_LEFT_ALT, SPK_EVENT_MOVE_MODE_SLOW},
        {GLFW_KEY_LEFT_SHIFT, SPK_EVENT_MOVE_MODE_FAST},
        {GLFW_KEY_A, SPK_EVENT_CAMERA_LEFT},
        {GLFW_KEY_D, SPK_EVENT_CAMERA_RIGHT},
        {GLFW_KEY_S, SPK_EVENT_CAMERA_BACKWARD},
        {GLFW_KEY_W, SPK_EVENT_CAMERA_FORWARD},
        {GLFW_KEY_E, SPK_EVENT_CAMERA_UP},
        {GLFW_KEY_Q, SPK_EVENT_CAMERA_DOWN},
        {GLFW_KEY_DOWN, SPK_EVENT_CAMERA_ROTATE_DOWN},
        {GLFW_KEY_LEFT, SPK_EVENT_CAMERA_ROTATE_LEFT},
        {GLFW_KEY_RIGHT, SPK_EVENT_CAMERA_ROTATE_RIGHT},
        {GLFW_KEY_UP, SPK_EVENT_CAMERA_ROTATE_UP},
        {GLFW_KEY_F5, SPK_EVENT_RELOAD_SHADERS},
        {GLFW_KEY_F11, SPK_EVENT_TOGGLE_FULLSCREEN},
        {GLFW_KEY_G, SPK_EVENT_GRAY_DOT_TO_EYE},
        {GLFW_KEY_I, SPK_EVENT_TOGGLE_IMGUI},
        {GLFW_KEY_L, SPK_EVENT_TOGGLE_LIGHT_SELECTION},
        {GLFW_KEY_B, SPK_EVENT_DDGI_PROBE_VIS_PREV_MAP},
        {GLFW_KEY_R, SPK_EVENT_DDGI_RAY_DIR_RAND_NEXT},
        {GLFW_KEY_N, SPK_EVENT_DDGI_UPDATE_REQ},
        {GLFW_KEY_V, SPK_EVENT_DDGI_PROBE_VIS_NEXT_MAP},
        {GLFW_KEY_X, SPK_EVENT_DDGI_PROBE_VIS_RAY_DIR},
        {GLFW_KEY_P, SPK_EVENT_TOGGLE_DDGI_STEPWISE_RAY_DIR_PERTURB},
        {GLFW_KEY_U, SPK_EVENT_TOGGLE_DDGI_STEPWISE_UPDATE},
        {GLFW_KEY_T, SPK_EVENT_TOGGLE_DDGI_PROBE_VIS},
        {GLFW_KEY_SPACE, SPK_EVENT_TOGGLE_PAUSE},
    };
    const static std::unordered_map<int, spk_event_e> release_key_event_map = {
        {GLFW_KEY_LEFT_ALT, SPK_EVENT_MOVE_MODE_SLOW},
        {GLFW_KEY_LEFT_SHIFT, SPK_EVENT_MOVE_MODE_FAST},
        {GLFW_KEY_A, SPK_EVENT_CAMERA_LEFT},
        {GLFW_KEY_D, SPK_EVENT_CAMERA_RIGHT},
        {GLFW_KEY_E, SPK_EVENT_CAMERA_UP},
        {GLFW_KEY_Q, SPK_EVENT_CAMERA_DOWN},
        {GLFW_KEY_S, SPK_EVENT_CAMERA_BACKWARD},
        {GLFW_KEY_W, SPK_EVENT_CAMERA_FORWARD},
        {GLFW_KEY_DOWN, SPK_EVENT_CAMERA_ROTATE_DOWN},
        {GLFW_KEY_LEFT, SPK_EVENT_CAMERA_ROTATE_LEFT},
        {GLFW_KEY_RIGHT, SPK_EVENT_CAMERA_ROTATE_RIGHT},
        {GLFW_KEY_UP, SPK_EVENT_CAMERA_ROTATE_UP}
    };

    if (action == GLFW_PRESS) {
        const auto search = press_key_event_map.find(key);
        if (search != press_key_event_map.end()) {
            events_[search->second] = true;
        }
    }
    else if (action == GLFW_RELEASE) {
        const auto search = release_key_event_map.find(key);
        if (search != release_key_event_map.end()) {
            events_[search->second] = false;
        }
    }
    else { // GLFW_REPEAT
    }
}


void renderer_t::
mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (imgui_on_ && imgui_.want_mouse() && action != GLFW_RELEASE) return;

    if (action == GLFW_PRESS) {
        switch (button) {
        case GLFW_MOUSE_BUTTON_LEFT:
            events_[SPK_EVENT_CAMERA_ROTATE] = true;
            break;
        case GLFW_MOUSE_BUTTON_RIGHT:
            events_[SPK_EVENT_HOST_DEV_MSG_REQ] = true;
            break;
        default:
            break;
        }
    }
    else if (action == GLFW_RELEASE) {
        switch (button) {
        case GLFW_MOUSE_BUTTON_LEFT:
            events_[SPK_EVENT_CAMERA_ROTATE] = false;
            break;
        case GLFW_MOUSE_BUTTON_RIGHT:
            break;
        default:
            break;
        }
    }
}


void renderer_t::
scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    if (imgui_on_ && imgui_.want_mouse()) {
        return;
    }
    scroll_offset_ = glm::dvec2(xoffset, yoffset);
}


void renderer_t::
prepare_imgui()
{
    uint32_t device_fps = static_cast<uint32_t>(std::round(1e3f / dev_timer_array_.elapsed_ms(DEV_TIMER_FRAME)));
    uint32_t host_fps = static_cast<uint32_t>(std::round(1e3f / host_perf_timers_[HOST_PERF_TIMER_FRAME].elapsed_ms()));

    imgui_.new_frame();
    ImGuizmo::BeginFrame();

    const std::string imgui_frame_name = "Settings: " + std::string(context_.physical_dev_name());
    const auto& eye = camera_.eye();
    int current_scene_idx = current_scene_idx_;

    imgui_.begin_render(imgui_frame_name.c_str());
    imgui_.push_font(1);

    bool reversed_z = camera_.reversed_z();
    events_[SPK_EVENT_TOGGLE_REVERSE_Z] = ImGui::Checkbox("R-z", &reversed_z);
    if (ImGui::IsItemHovered()) {
        ImGui::SetTooltip("reversed-z");
    }
    ImGui::SameLine();

    bool ddgi_on = ddgi_on_;
    events_[SPK_EVENT_TOGGLE_DDGI] = ImGui::Checkbox("ddgi", &ddgi_on);

    ImGui::Separator();
    input_.render_imgui_scene_settings(&current_scene_idx);
    reflection_.render_imgui_settings(input_.get_reflection_cfg_ptr());
    ddgi_.render_imgui_settings(input_.get_ddgi_cfg_ptr(current_scene_idx_));
    ddgi_.render_imgui_probe_vis_options(&frame_params_.ddgi_probe_vis_radius, &ddgi_probe_vis_map_idx_);

    events_[SPK_EVENT_SWITCH_SCENE] = (current_scene_idx != current_scene_idx_);
    current_scene_idx_ = current_scene_idx;

    static gray_dot_gizmo_t gizmo{};
    events_[SPK_EVENT_GRAY_DOT_MOVED] = false;
    const auto persp = glm::perspective(camera_.fovy(), camera_.aspect(), camera_.near_plane(), camera_.far_plane());
    if (events_[SPK_EVENT_GRAY_DOT_SELECTED]) {
        ImGui::Text("Transform");
        bool gray_dot_moved = gizmo.prepare(
            camera_.view_mat(), persp, 
            glm::vec2(0.f), glm::vec2(window_.width(), window_.height()), 
            &frame_params_.t_gray_dot
        );
        if (gray_dot_moved) {
            events_[SPK_EVENT_GRAY_DOT_MOVED] = true;
        }
    }
    else if (events_[SPK_EVENT_LIGHT_SELECTED]) {
        input_.render_imgui_light_guizmo(
            camera_.view_mat(), persp, 
            glm::vec2(0.f), glm::vec2(window_.width(), window_.height()), 
            &frame_params_.light_data
        );
    }
    ImGui::Separator();
    ImGui::Text("(%4d x %4d)", window_.width(), window_.height());
    ImGui::Text("ddgi stepwise: [update: %s][ray: %s]", ddgi_stepwise_update_on_ ? "on " : "off", ddgi_stepwise_ray_dir_perturb_on_ ? "on " : "off");
    ImGui::Text("eye: (%7.3f, %7.3f, %7.3f)", eye.x, eye.y, eye.z);
    ImGui::Separator();
    ImGui::Text("FPS: device = %4d / host = %3d", device_fps, host_fps);
    ImGui::Separator();
    if (ImGui::TreeNode("Perf")) {
        ImGui::Text("dev frame time:     %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_FRAME));
        ImGui::Spacing();
        ImGui::Text("vis:opaque:         %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_VISIBILITY_SCENE_OPAQUE));
        ImGui::Text("vis:alpha:          %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_VISIBILITY_SCENE_ALPHA));
        ImGui::Text("write_g_buffer:     %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_WRITE_G_BUFFER));
        ImGui::Text("tlas rebuild:       %7.4f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_TLAS_REBUILD));
        ImGui::Spacing();
        ImGui::Text("ddgi raygen:        %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_DDGI_RAYGEN));
        ImGui::Text("ddgi lighting:      %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_DDGI_LIGHTING));
        ImGui::Text("ddgi update:        %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_DDGI_UPDATE));
        ImGui::Spacing();
        ImGui::Text("shadow:rt:          %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_SHADOW_RT));
        ImGui::Text("shadow:tempo:       %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_SHADOW_TEMPO));
        ImGui::Text("shadow:a trous:     %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_SHADOW_A_TROUS));
        ImGui::Spacing();
        ImGui::Text("reflection:rt:      %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_REFLECTION_RT));
        ImGui::Text("reflection:tempo:   %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_REFLECTION_TEMPO));
        ImGui::Text("reflection:a trous: %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_REFLECTION_A_TROUS));
        ImGui::Spacing();
        ImGui::Text("present:probe_vis:  %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_DDGI_PROBE_VIS));
        ImGui::Text("present:shading:    %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_PRESENT_SHADING));
        ImGui::Text("present:imgui:      %6.3f ms", dev_timer_array_.elapsed_ms(DEV_TIMER_PRESENT_IMGUI));
        ImGui::Separator();
        ImGui::Text("host frame time:    %6.3f ms", host_perf_timers_[HOST_PERF_TIMER_FRAME].elapsed_ms());
        ImGui::Spacing();
        ImGui::Text("build cmd buf:      %6.3f ms", host_perf_timers_[HOST_PERF_TIMER_CMD_BUF].elapsed_ms());
        ImGui::Text("prepare imgui:      %6.3f ms", host_perf_timers_[HOST_PERF_TIMER_IMGUI_PREPARE].elapsed_ms());
        ImGui::Text("upload ubo:         %8.5f ms", host_perf_timers_[HOST_PERF_TIMER_UBO_UPLOAD].elapsed_ms());
        ImGui::Text("render submit:      %6.3f ms", host_perf_timers_[HOST_PERF_TIMER_RENDER_SUBMIT].elapsed_ms());
        ImGui::Text("present submit:     %6.3f ms", host_perf_timers_[HOST_PERF_TIMER_PRESENT_SUBMIT].elapsed_ms());
        ImGui::TreePop();
    }

    imgui_.pop_font();
    imgui_.end_render();
}


bool renderer_t::
handle_input()
{
    glfwSetWindowShouldClose(window_, events_[SPK_EVENT_QUIT]);
    window_.poll_event();
    if (events_[SPK_EVENT_TOGGLE_FULLSCREEN]) {
        window_.toggle_full_screen();
        events_[SPK_EVENT_TOGGLE_FULLSCREEN] = false;
    }
    auto window_state = window_.check_size();
    if (window_state == window_t::state_e::MINIMIZED) return false;
    if (window_state == window_t::state_e::RESIZED) events_[SPK_EVENT_RESIZE_WINDOW] = true;

    cursor_positions_[SPK_CURSOR_POS_PREV] = cursor_positions_[SPK_CURSOR_POS_CURR];
    glm::dvec2 curr_pos{};
    glfwGetCursorPos(window_, &curr_pos.x, &curr_pos.y);
    cursor_positions_[SPK_CURSOR_POS_CURR] = curr_pos;
    cursor_offset_ = glm::vec2(cursor_positions_[SPK_CURSOR_POS_CURR] - cursor_positions_[SPK_CURSOR_POS_PREV]);

    ddgi_probe_vis_map_idx_ = u32_log2(frame_params_.ddgi_probe_vis_option & DDGI_PROBE_VIS_MAP_MASK);

    host_perf_timers_[HOST_PERF_TIMER_IMGUI_PREPARE].start();
    if (imgui_on_) {
        prepare_imgui();
    }
    host_perf_timers_[HOST_PERF_TIMER_IMGUI_PREPARE].stop();

    if (events_[SPK_EVENT_TOGGLE_LIGHT_SELECTION]) {
        events_[SPK_EVENT_LIGHT_SELECTED] = !events_[SPK_EVENT_LIGHT_SELECTED];
        events_[SPK_EVENT_TOGGLE_LIGHT_SELECTION] = false;
    }

    if (events_[SPK_EVENT_LIGHT_SELECTED]) {
        events_[SPK_EVENT_GRAY_DOT_SELECTED] = false;
    }

    if (events_[SPK_EVENT_TOGGLE_DDGI_STEPWISE_RAY_DIR_PERTURB]) {
        ddgi_stepwise_ray_dir_perturb_on_ = !ddgi_stepwise_ray_dir_perturb_on_;
        events_[SPK_EVENT_TOGGLE_DDGI_STEPWISE_RAY_DIR_PERTURB] = false;
    }

    uint32_t probe_vis_ray_dir_bit = frame_params_.ddgi_probe_vis_option & DDGI_PROBE_VIS_RAY_DIR_FLAG;
    if (events_[SPK_EVENT_DDGI_PROBE_VIS_RAY_DIR]) {
        if (probe_vis_ray_dir_bit) {
            probe_vis_ray_dir_bit = 0;
        }
        else {
            probe_vis_ray_dir_bit = DDGI_PROBE_VIS_RAY_DIR_FLAG;
        }
        events_[SPK_EVENT_DDGI_PROBE_VIS_RAY_DIR] = false;
    }
    constexpr uint32_t PROBE_VIS_MAP_OPTION_COUNT = length_of_c_array(k_ddgi_probe_vis_map_options);
    if (events_[SPK_EVENT_TOGGLE_DDGI_PROBE_VIS]) {
        ddgi_probe_vis_on_ = !ddgi_probe_vis_on_;
        events_[SPK_EVENT_TOGGLE_DDGI_PROBE_VIS] = false;
    }
    if (events_[SPK_EVENT_DDGI_PROBE_VIS_NEXT_MAP]) {
        ddgi_probe_vis_map_idx_ = (ddgi_probe_vis_map_idx_ + 1) % PROBE_VIS_MAP_OPTION_COUNT;
        events_[SPK_EVENT_DDGI_PROBE_VIS_NEXT_MAP] = false;
    }
    if (events_[SPK_EVENT_DDGI_PROBE_VIS_PREV_MAP]) {
        ddgi_probe_vis_map_idx_ = (ddgi_probe_vis_map_idx_ + PROBE_VIS_MAP_OPTION_COUNT - 1) % PROBE_VIS_MAP_OPTION_COUNT;
        events_[SPK_EVENT_DDGI_PROBE_VIS_PREV_MAP] = false;
    }
    if (events_[SPK_EVENT_TOGGLE_DDGI_STEPWISE_UPDATE]) {
        ddgi_stepwise_update_on_ = !ddgi_stepwise_update_on_;
        events_[SPK_EVENT_TOGGLE_DDGI_STEPWISE_UPDATE] = false;
    }
    frame_params_.ddgi_probe_vis_option = probe_vis_ray_dir_bit | (1u << ddgi_probe_vis_map_idx_);

    if (events_[SPK_EVENT_TOGGLE_IMGUI]) {
        imgui_on_ = !imgui_on_;
        events_[SPK_EVENT_TOGGLE_IMGUI] = false;
    }
    if (events_[SPK_EVENT_TOGGLE_DDGI]) {
        ddgi_on_ = !ddgi_on_;
        if (!ddgi_on_) {
            vkDeviceWaitIdle(context_);
            ddgi_.reset_images(context_, cmd_buf_, queue_);
            ddgi_.write_descriptor_sets(context_, dummy_sampler_);
            frame_params_.ddgi_is_first_frame = true;
        }
        events_[SPK_EVENT_TOGGLE_DDGI] = false;
    }
    if (events_[SPK_EVENT_HOST_DEV_MSG_REQ]) {
        frame_params_.click_pixel_location = glm::uvec2(curr_pos);
    }

    float camera_rotation_speed = 0.6f;
    float camera_translation_speed = 0.05f;

    if (events_[SPK_EVENT_MOVE_MODE_SLOW]) {
        camera_translation_speed *= 0.1f;
    }
    if (events_[SPK_EVENT_MOVE_MODE_FAST]) {
        camera_translation_speed *= 10.f;
    }
    if (events_[SPK_EVENT_TOGGLE_REVERSE_Z]) {
        camera_.reverse_z();
        events_[SPK_EVENT_TOGGLE_REVERSE_Z] = false;
    }
    if (events_[SPK_EVENT_RESIZE_WINDOW]) {
        camera_.aspect(window_.aspect_ratio());
    }
    const glm::vec3 camera_translation = camera_translation_speed * glm::vec3 { 
        events_[SPK_EVENT_CAMERA_RIGHT] - events_[SPK_EVENT_CAMERA_LEFT], 
        events_[SPK_EVENT_CAMERA_UP] - events_[SPK_EVENT_CAMERA_DOWN], 
        events_[SPK_EVENT_CAMERA_BACKWARD] - events_[SPK_EVENT_CAMERA_FORWARD]
    };
    if (glm::dot(camera_translation, camera_translation) > std::numeric_limits<float>::epsilon()) {
        camera_.translate(camera_translation);
    }
    
    if (events_[SPK_EVENT_CAMERA_ROTATE]) {
        if (std::abs(cursor_offset_.x) > std::numeric_limits<float>::epsilon()) {
            const auto axis = glm::vec3(0.f, 1.f, 0.f);
            camera_.rotate(-cursor_offset_.x * camera_rotation_speed, axis);
        }
        if (std::abs(cursor_offset_.y) > std::numeric_limits<float>::epsilon()) {
            camera_.rotate(-cursor_offset_.y * camera_rotation_speed, camera_.right());
        }
    }
    else {
        constexpr float camera_key_rotation_speed = 0.5f;
        if (events_[SPK_EVENT_CAMERA_ROTATE_LEFT] || events_[SPK_EVENT_CAMERA_ROTATE_RIGHT]){
            const float d = static_cast<float>(events_[SPK_EVENT_CAMERA_ROTATE_LEFT] - events_[SPK_EVENT_CAMERA_ROTATE_RIGHT]);
            constexpr auto axis = glm::vec3(0.f, 1.f, 0.f);
            camera_.rotate(d * camera_key_rotation_speed, axis);
        }
        if (events_[SPK_EVENT_CAMERA_ROTATE_DOWN] || events_[SPK_EVENT_CAMERA_ROTATE_UP]) {
            const float d = static_cast<float>(events_[SPK_EVENT_CAMERA_ROTATE_UP] - events_[SPK_EVENT_CAMERA_ROTATE_DOWN]);
            camera_.rotate(d * camera_key_rotation_speed, camera_.right());
        }
    }
    if (camera_.outdated()) {
        camera_.update();
    }
    if (events_[SPK_EVENT_TOGGLE_PAUSE]) {
        paused_ = !paused_;
        events_[SPK_EVENT_TOGGLE_PAUSE] = false;
    }

    if (events_[SPK_EVENT_SWITCH_SCENE]) {
        vkDeviceWaitIdle(context_);
        change_scene();
        events_[SPK_EVENT_SWITCH_SCENE] = false;
    }
    else if (events_[SPK_EVENT_RESIZE_WINDOW]) {
        resize();
        frame_params_.render_frame_idx = 0;
        events_[SPK_EVENT_RESIZE_WINDOW] = false;
    }
    else if (events_[SPK_EVENT_RELOAD_SHADERS]) {
        if (reload_shaders()) {
            frame_params_.render_frame_idx = 0;
        }
        events_[SPK_EVENT_RELOAD_SHADERS] = false;
    }

    return true;
}


// sync before entering this function
void renderer_t::
prepare_frame_params()
{
    const float w = static_cast<float>(window_.width());
    const float h = static_cast<float>(window_.height());
    const float rcp_w = 1.f / w;
    const float rcp_h = 1.f / h;
    const glm::vec2 rcp_viewport_dim = glm::vec2(rcp_w, rcp_h);

    if (events_[SPK_EVENT_HOST_DEV_MSG_RES]) {
        const auto host_dev_msg = *input_.host_dev_msg_buffer().mapped_data<glsl_shared::host_dev_msg_t>();
        events_[SPK_EVENT_HOST_DEV_MSG_RES] = false;
        if (host_dev_msg.mtl_idx.x != static_cast<int32_t>(MTL_TYPE_GRAY_DOT)) {
            events_[SPK_EVENT_GRAY_DOT_SELECTED] = false;
        }
        else {
            events_[SPK_EVENT_GRAY_DOT_SELECTED] = true;
            events_[SPK_EVENT_LIGHT_SELECTED] = false;
        }
    }
    if (events_[SPK_EVENT_GRAY_DOT_TO_EYE]) {
        frame_params_.t_gray_dot[3] = glm::vec4(0.f, 0.f, 0.f, 1.f);
        frame_params_.t_gray_dot = glm::translate(frame_params_.t_gray_dot, camera_.eye() - camera_.near_plane() - 5.f * camera_.outward());
        events_[SPK_EVENT_GRAY_DOT_TO_EYE] = false;
        events_[SPK_EVENT_GRAY_DOT_MOVED] = true;
    }
    
    frame_params_.prev_vp = frame_params_.vp;
    frame_params_.prev_inv_vp = frame_params_.inv_vp;
    frame_params_.prev_eye = frame_params_.eye;

    frame_params_.view = camera_.view_mat();
    frame_params_.inv_view = camera_.inv_view_mat();
    frame_params_.proj = camera_.proj_mat();
    frame_params_.vp = frame_params_.proj * frame_params_.view;
    frame_params_.inv_vp = glm::inverse(frame_params_.vp);
    frame_params_.tn_gray_dot = glm::inverse(glm::transpose(frame_params_.t_gray_dot));
    frame_params_.eye = glm::vec4(camera_.eye(), camera_.ndc_far());
    frame_params_.background_color = glm::vec4(glm::vec3(k_background_color), camera_.ndc_far());
    frame_params_.viewport_dim = glm::uvec2(window_.width(), window_.height());
    frame_params_.rcp_viewport_dim = rcp_viewport_dim;

    copy_ddgi_params(&frame_params_, &input_.get_ddgi_cfg(current_scene_idx_));
    copy_svgf_params(&frame_params_, &input_.get_svgf_cfg());
    copy_reflection_params(&frame_params_, &input_.get_reflection_cfg());

    if (ddgi_on_) {
        if (!ddgi_stepwise_ray_dir_perturb_on_ || events_[SPK_EVENT_DDGI_RAY_DIR_RAND_NEXT]) {
            ddgi_.perturbe_per_probe_ray_dirs();
            events_[SPK_EVENT_DDGI_RAY_DIR_RAND_NEXT] = false;
        }
        const glm::vec4* ray_dirs = ddgi_.perturbed_per_probe_ray_dirs();
        std::copy(ray_dirs, ray_dirs + ddgi_.per_probe_ray_count(), frame_params_.ddgi_ray_dirs);
    }
}


VkResult renderer_t::
draw_frame()
{

    SPOCK_VK_CALL(vkQueueWaitIdle(queue_));
    vkWaitForFences(context_, 1, &render_done_fence_(), VK_TRUE, always<uint64_t>());

    uint32_t image_idx = npos<uint32_t>();
    auto ret = vkAcquireNextImageKHR(context_, swapchain_, always<uint64_t>(), image_ready_semaphore_, VK_NULL_HANDLE, &image_idx);

    if (ret == VK_ERROR_OUT_OF_DATE_KHR) {
        return ret;
    }
    SPOCK_ENSURE(ret == VK_SUCCESS || ret == VK_SUBOPTIMAL_KHR, "vkAcquireNextImageKHR failed: " + std::string{ str_from_VkResult(ret) });

    dev_timer_array_.end();

    prepare_frame_params();

    host_perf_timers_[HOST_PERF_TIMER_UBO_UPLOAD].start();
    memcpy(input_.frame_params_uniform_buffer().mapped_data<>(), &frame_params_, sizeof(glsl_shared::frame_params_t));
    host_perf_timers_[HOST_PERF_TIMER_UBO_UPLOAD].stop();

    dev_timer_array_.begin();
    record_command_buffer(image_idx);

    VkPipelineStageFlags wait_dst_stages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    VkSubmitInfo submit_info = build_VkSubmitInfo(1, &cmd_buf_(),  1, &image_ready_semaphore_(), &wait_dst_stages, 1, &render_done_semaphore_());

    SPOCK_VK_CALL(vkResetFences(context_, 1, &render_done_fence_()));

    host_perf_timers_[HOST_PERF_TIMER_RENDER_SUBMIT].start();
    SPOCK_VK_CALL(vkQueueSubmit(queue_, 1, &submit_info, render_done_fence_));
    host_perf_timers_[HOST_PERF_TIMER_RENDER_SUBMIT].stop();

    VkPresentInfoKHR present_info = make_VkPresentInfoKHR();
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores    = &render_done_semaphore_();
    present_info.swapchainCount     = 1;
    present_info.pSwapchains        = swapchain_.ptr();
    present_info.pImageIndices      = &image_idx;

    host_perf_timers_[HOST_PERF_TIMER_PRESENT_SUBMIT].start();
    ret = vkQueuePresentKHR(queue_, &present_info);
    host_perf_timers_[HOST_PERF_TIMER_PRESENT_SUBMIT].stop();

    if (ret == VK_ERROR_OUT_OF_DATE_KHR || ret == VK_SUBOPTIMAL_KHR) {
        return ret;
    }

    SPOCK_VK_CHECK(ret, "vkQueuePresentKHR failed: " + std::string{ str_from_VkResult(ret) });
    return VK_SUCCESS;
}


void renderer_t::
loop()
{
    try {
        while (!window_.should_close()) {

            if (!handle_input()) {
                using namespace std::chrono_literals;
                std::this_thread::sleep_for(100ms);
                continue;
            }

            host_perf_timers_[HOST_PERF_TIMER_FRAME].start();
            auto res = draw_frame();
            host_perf_timers_[HOST_PERF_TIMER_FRAME].stop();
            frame_ms_ = std::max(host_perf_timers_[HOST_PERF_TIMER_FRAME].elapsed_ms(), dev_timer_array_.elapsed_ms(DEV_TIMER_FRAME));

            frame_params_.render_frame_idx += 1;
            if (!paused_) {
                frame_params_.animated_ms += frame_ms_;
            }
            if (VK_SUCCESS != res) {
                events_[SPK_EVENT_RESIZE_WINDOW] = true;
            }
        }
    }
    catch (const std::exception& e) {
        vkDeviceWaitIdle(context_);
        std::throw_with_nested(std::runtime_error(std::string{ "exiting loop: " } + e.what()));
    }

    vkDeviceWaitIdle(context_);
}


int main(int argc, char** argv)
{
    try {
        std::ifstream ifs(BASIC_RENDERER_CONFIG_JSON_PATH);
        spock::ordered_json config_json = spock::ordered_json::parse(ifs, nullptr, true, true /* enable comments */);
        ifs.close();
        renderer_t renderer{ config_json };
        renderer.loop();
    }
    catch (const std::exception& e) {
        spock::err(spock::nested_exception_str(e));
    }
    catch (...) {
        spock::err("something went wrong...");
    }

    return 0;
}


