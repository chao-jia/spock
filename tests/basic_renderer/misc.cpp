#include "misc.hpp"
#include <filesystem>
#include <fstream>

#if _MSC_VER
#pragma warning( disable : 6011)
#endif

std::string 
relative_path_str(const std::string & a_p0, const std::string & a_p1)
{
    namespace stdfs = std::filesystem;
    auto d = stdfs::relative(a_p0, a_p1).string();

#if _WIN32
    std::replace(d.begin(), d.end(), '\\', '/');
#endif 
    return d;
}


bool 
create_spirvs(
    uint32_t a_vk_api_version, 
    uint32_t a_glsl_count, 
    const std::string* a_glsl_relative_paths, 
    const std::string& a_scene_name, 
    spirv_t* a_spirvs
)
{
    namespace stdfs = std::filesystem;
    constexpr std::string_view scene_params_hsl_name = "scene_params.hsl";
    SPOCK_PARANOID(a_spirvs != nullptr, "create_spirvs: a_spirvs should not be nullptr");
    SPOCK_PARANOID(a_glsl_count > 0, "create_spirvs: a_glsl_count should not be 0");

    bool all_glsl_valid = true;

    // TODO: parallelization
    for (uint32_t i = 0; i < a_glsl_count; ++i) {
        const std::string src_glsl_path = BASIC_RENDERER_GLSL_DIR"/" + a_glsl_relative_paths[i];
        const std::string dst_glsl_path = BASIC_RENDERER_CACHE_DIR"/" + a_scene_name + '/' + a_glsl_relative_paths[i];
        const std::string scene_params_hsl_path = BASIC_RENDERER_CACHE_DIR"/" + a_scene_name + '/' + scene_params_hsl_name.data();
        const std::string dst_spirv_path = dst_glsl_path + std::string(spirv_t::SPIRV_FILE_EXTENSION);
        const std::string dst_spirv_dir = stdfs::path(dst_spirv_path).parent_path().string();

        bool exists_src_glsl = stdfs::exists(src_glsl_path);
        bool exists_dst_glsl = stdfs::exists(dst_glsl_path);
        bool dst_glsl_outdated = false;
        if (exists_dst_glsl && exists_src_glsl) {
            const auto src_glsl_age = stdfs::last_write_time(src_glsl_path);
            const auto dst_glsl_age = stdfs::last_write_time(dst_glsl_path);
            dst_glsl_outdated = spock::earlier_than(dst_glsl_age, src_glsl_age);
        }

        bool exists_dst_spirv = stdfs::exists(dst_spirv_path);
        spock::ensure(exists_src_glsl || exists_dst_spirv, "neither src glsl nor dst spirv exists for " + std::string(a_glsl_relative_paths[i]));

        const std::string placeholder_scene_params_hsl_path{ BASIC_RENDERER_GLSL_DIR"/" + std::string(scene_params_hsl_name) };
        if (exists_src_glsl && (!exists_dst_glsl || dst_glsl_outdated)) {
            const auto& src_inc_files = get_glsl_included_files(src_glsl_path, a_vk_api_version);
            bool included_scene_params_hsl = std::any_of(
                src_inc_files.cbegin(), src_inc_files.cend(), 
                [&](const std::string& a_inc) { return stdfs::equivalent(a_inc, placeholder_scene_params_hsl_path); }
            );
            const auto dst_glsl_dir = stdfs::path(dst_glsl_path).parent_path().string();
            if (!stdfs::exists(dst_glsl_dir)) {
                warn("creating directory: " + dst_glsl_dir);
                stdfs::create_directories(dst_glsl_dir);
            }
            std::ofstream ofs(dst_glsl_path, std::ios::trunc | std::ios::binary);
            ofs << "#if __VERSION__ == 110\n#version 460\n#endif\n\n";
            ofs << "#extension GL_GOOGLE_include_directive : enable\n\n";
            if (included_scene_params_hsl) {
                ofs << "#include " << '"' << relative_path_str(scene_params_hsl_path, dst_glsl_dir) << '"' << '\n';
            }
            ofs << "#include \"" << relative_path_str(src_glsl_path, dst_glsl_dir) << "\"\n\n";
            ofs.close();
        }
        spirv_t spirv(dst_glsl_path, a_vk_api_version, dst_spirv_dir);
        all_glsl_valid = all_glsl_valid && spirv.glsl_file_valid();
        a_spirvs[i] = std::move(spirv);
    }
    return all_glsl_valid;
}


bool 
reload_spirvs(spirv_t * a_spirv_begin, uint32_t a_spirv_count, const std::string& a_msg)
{
    host_timer_t timer;

    auto spirv_reloaded_array = std::make_unique<uint32_t[]>(a_spirv_count);

#if BASIC_RENDERER_MT_SPIRV_RELOAD
    for_indexed_each(
        a_spirv_begin, a_spirv_begin + a_spirv_count,
        [&spirv_reloaded_array](spirv_t& a_spirv, size_t i) { spirv_reloaded_array[i] = a_spirv.reload(); }
    );
    spock::info(a_msg + "reloading shaders (mt) took " + std::to_string(timer.elapsed_ms()) + " ms");

#else
    for (size_t i = 0; i < static_cast<size_t>(SPV_COUNT); ++i) {
        spirv_reloaded_array[i] = spirvs_[i].reload();
    }
    spock::info("deferred: reloading spirvs took " + std::to_string(timer.elapsed_ms()) + " ms");

#endif

    return std::any_of(spirv_reloaded_array.get(), spirv_reloaded_array.get() + a_spirv_count, [](uint32_t i) { return i > 0; });
}




