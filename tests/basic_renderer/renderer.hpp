#pragma once
#include "input.hpp"
#include "deferred.hpp"
#include "ddgi.hpp"
#include "shadow.hpp"
#include "reflection.hpp"

#include <spock/scene.hpp>
#include <spock/context.hpp>
#include <spock/utils.hpp>

#include <glm/glm.hpp>
#include <array>
#include <vector>
#include "../../etc/glsl/basic_renderer/shared_definitions.h"


using namespace std::placeholders;
using namespace spock;

class renderer_t
{
private:
    static constexpr glm::vec4 k_background_color { 0., 0., 0.f, 1.f };

    std::string app_info_;
    window_t window_;
    camera_t camera_;
    context_t context_;
    swapchain_t swapchain_;
    sampler_t dummy_sampler_;

    queue_t queue_;
    command_pool_t command_pool_;
    command_buffer_t cmd_buf_;

    semaphore_t render_done_semaphore_;
    semaphore_t image_ready_semaphore_;
    fence_t render_done_fence_;

    deferred_t deferred_;
    input_t input_;
    reflection_t reflection_;
    shadow_t shadow_;
    ddgi_t ddgi_;

    sbt_entry_size_t sbt_entry_size_;
    uint32_t sbt_bytes_;
    buffer_t sbt_;

    pipeline_layout_t pipeline_layout_;

    imgui_t imgui_;

    dev_timer_array_t dev_timer_array_;
    std::unique_ptr<host_perf_timer_t[]> host_perf_timers_;

    std::unique_ptr<bool[]> events_; // on or off
    std::unique_ptr<glm::dvec2[]> cursor_positions_;
    glm::dvec2 cursor_offset_;
    glm::dvec2 scroll_offset_;

    glsl_shared::frame_params_t frame_params_;

    VkFormat depth_format_;
    int current_scene_idx_;
    int ddgi_probe_vis_map_idx_;

    float frame_ms_;

    bool paused_;
    bool imgui_on_;

    bool ddgi_on_;
    bool ddgi_probe_vis_on_;
    bool ddgi_stepwise_ray_dir_perturb_on_;
    bool ddgi_stepwise_update_on_;

    bool glsl_valid_; // can compile on the fly only if all glsl files are valid

    void create_pipeline_layout();
    void write_sbt();

    void record_command_buffer(uint32_t a_image_idx);

    void change_scene();
    void resize();
    bool reload_shaders(); // return true if at least one shader is reloaded
    void prepare_imgui();

    /// return false if this iteration of the loop need to be skipped due to e.g. window minimized...
    bool handle_input();

    void prepare_frame_params();
    VkResult draw_frame();

public:
    renderer_t(const spock::ordered_json& a_config_json);
    ~renderer_t() {}
    void loop();
    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
    void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
    void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

};


