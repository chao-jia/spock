#pragma once
#include "misc.hpp"
#include <spock/context.hpp>
#include <random>

constexpr char const* k_ddgi_probe_vis_map_options[] = {"uv", "depth", "depth^2", "irradiance"};

#define SPEC_CONST_HELPER(s, m) offsetof(s, m), sizeof(s::m)

class ddgi_t {
public:
    class specialization_constant_t {
        friend class ddgi_t;
    private:
        struct data_t {
            uint32_t log2_probe_grid_dim_x;
            uint32_t log2_probe_grid_dim_y;
            uint32_t log2_probe_grid_dim_z;
            float probe_cell_size_x;
            float probe_cell_size_y;
            float probe_cell_size_z;
            float probe_grid_lower_x;
            float probe_grid_lower_y;
            float probe_grid_lower_z;
            uint32_t log2_probe_count_per_image_row;
            uint32_t log2_probe_surfel_resolution_x;
            uint32_t log2_probe_surfel_resolution_y;
            uint32_t log2_probe_irradiance_resolution;
            uint32_t log2_probe_depth_resolution;
            uint32_t compute_local_size_x;
            uint32_t compute_local_size_y;
        };

        static constexpr VkSpecializationMapEntry entries[] = {
            {  0, SPEC_CONST_HELPER(data_t, log2_probe_grid_dim_x) },
            {  1, SPEC_CONST_HELPER(data_t, log2_probe_grid_dim_y) },
            {  2, SPEC_CONST_HELPER(data_t, log2_probe_grid_dim_z) },
            {  3, SPEC_CONST_HELPER(data_t, probe_cell_size_x) },
            {  4, SPEC_CONST_HELPER(data_t, probe_cell_size_y) },
            {  5, SPEC_CONST_HELPER(data_t, probe_cell_size_z) },
            {  6, SPEC_CONST_HELPER(data_t, probe_grid_lower_x) },
            {  7, SPEC_CONST_HELPER(data_t, probe_grid_lower_y) },
            {  8, SPEC_CONST_HELPER(data_t, probe_grid_lower_z) },
            {  9, SPEC_CONST_HELPER(data_t, log2_probe_count_per_image_row) },
            { 10, SPEC_CONST_HELPER(data_t, log2_probe_surfel_resolution_x) },
            { 11, SPEC_CONST_HELPER(data_t, log2_probe_surfel_resolution_y) },
            { 12, SPEC_CONST_HELPER(data_t, log2_probe_irradiance_resolution) },
            { 13, SPEC_CONST_HELPER(data_t, log2_probe_depth_resolution) },
            { 14, SPEC_CONST_HELPER(data_t, compute_local_size_x) },
            { 15, SPEC_CONST_HELPER(data_t, compute_local_size_y) }
        };

        std::unique_ptr<data_t> data_;
        VkSpecializationInfo info_{};

    public:
        const VkSpecializationInfo& info() const { return info_; }
    };

    struct cfg_t {
        float depth_sharpness{};
        float energy_preservation{};
        float probe_intensity{};
        float hysteresis{};
        float normal_bias{};
        glm::uvec3 probe_grid_dim{};
        glm::vec3 probe_grid_offset{};
        glm::uvec2 probe_surfel_resolution{};

        cfg_t(const ordered_json& a_json, const std::string& a_scene_name = "");
    };

    ddgi_t(const ddgi_t&) = delete;
    ddgi_t& operator=(const ddgi_t&) = delete;
    ddgi_t(ddgi_t&&) = default;
    ddgi_t& operator=(ddgi_t&&) = default;

    ddgi_t();
    ddgi_t(VkDevice a_vk_dev);

    void init_params(
        const glm::uvec3& a_probe_dim,
        const glm::vec3& a_scene_aabb_size,
        const glm::vec3& a_scene_lower,
        const glm::uvec2& a_probe_surfel_resolution,
        const glm::uvec2& a_depth_update_compute_local_size,
        const glm::uvec2& a_irradiance_update_compute_local_size,
        uint32_t a_probe_depth_resolution = 16,
        uint32_t a_probe_irradiance_resolution = 8
    );
    void create_images(const context_t& a_context);
    void reset_images(const context_t& a_context, VkCommandBuffer a_cmd_buf, VkQueue a_queue);

    void perturbe_per_probe_ray_dirs();
    const glm::vec4* perturbed_per_probe_ray_dirs() { return perturbed_per_probe_ray_dirs_.get(); }
    specialization_constant_t get_ray_tracing_specialization_constant() const;
    specialization_constant_t get_depth_update_specialization_constant() const;
    specialization_constant_t get_irradiance_update_specialization_constant() const;

    const uint32_t per_probe_ray_count() const { return per_probe_ray_count_; }
    const glm::uvec3 probe_grid_dim() const { return probe_grid_dim_; }
    const uint32_t probe_count() const { return probe_grid_dim_.x * probe_grid_dim_.y * probe_grid_dim_.z; }
    const glm::uvec2& depth_update_compute_local_size() const { return depth_update_compute_local_size_; }
    const glm::uvec2& irradiance_update_compute_local_size() const { return irradiance_update_compute_local_size_; }

    void write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler);
    bool init_spirvs(uint32_t a_vk_api_version, const std::string& a_scene_name);
    bool refresh_spirvs();
    void create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, uint32_t a_shader_group_handle_size, VkRenderPass a_shading_pass);
    static uint32_t get_sbt_bytes(uint32_t a_sbt_stride);
    void write_sbt(const sbt_entry_size_t& a_sbt_entry_size, uint8_t** a_sbt_host_addr, VkDeviceAddress* a_sbt_dev_addr);

    void render_imgui_settings(cfg_t* a_cfg);
    void render_imgui_probe_vis_options(float* a_probe_vis_radius, int* a_probe_vis_map_idx);
    
    void cmd_compute_irradiance_field(VkCommandBuffer a_cmd_buf, dev_timer_array_t* a_dev_timer_array, bool a_update_on);
    void cmd_dummy_compute(VkCommandBuffer a_cmd_buf, dev_timer_array_t* a_dev_timer_array);
    void cmd_exec_vis_probe_pipeline(VkCommandBuffer a_cmd_buf);

    VkDescriptorSetLayout descriptor_set_layout() const { return descriptor_set_layout_; }
    const VkDescriptorSet descriptor_set() const { return descriptor_set_; }

private:
    std::unique_ptr<image_t[]> images_;
    std::unique_ptr<image_view_t[]> image_views_;
    std::unique_ptr<glm::uvec2[]> image_extents_;
    std::unique_ptr<glm::uvec2[]> padded_image_extents_;

    glm::uvec3 probe_grid_dim_;
    glm::vec3 probe_grid_lower_;
    glm::vec3 probe_cell_size_;
    glm::uvec2 probe_surfel_resolution_;
    uint32_t probe_depth_resolution_;
    uint32_t probe_irradiance_resolution_;

    glm::uvec2 depth_update_compute_local_size_;
    glm::uvec2 irradiance_update_compute_local_size_;

    uint32_t per_probe_ray_count_; // probe_surfel_resolution_.x * probe_surfel_resolution_.y
    glm::uvec2 probe_count_per_image_;

    std::unique_ptr<glm::vec3[]> per_probe_ray_dirs_;
    std::unique_ptr<glm::vec4[]> perturbed_per_probe_ray_dirs_;
    glm::mat3 rotation_;

    std::unique_ptr<spirv_t[]> spirvs_;

    descriptor_pool_t descriptor_pool_;
    descriptor_set_layout_t descriptor_set_layout_;
    VkDescriptorSet descriptor_set_;

    std::unique_ptr<pipeline_t[]> pipelines_;
    std::unique_ptr<shader_binding_set_t[]> sb_sets_;
    shader_group_handle_pool_t shader_group_handle_pool_;
    
    static constexpr uint32_t RANDOM_VAR_COUNT = 2;
    std::unique_ptr<std::mt19937[]> random_generators_;
    using uniform_fpdf_t = std::uniform_real_distribution<float>;
    std::unique_ptr<uniform_fpdf_t[]> uniform_fpdfs_;

    float random_next(uint32_t i) { return uniform_fpdfs_[i](random_generators_[i]); }
    specialization_constant_t get_specialization_constant() const;

    void create_descriptor_set_layout(VkDevice a_vk_dev);

    void cmd_exec_gen_surfel_pipeline(VkCommandBuffer a_cmd_buf);
    void cmd_exec_shade_surfel_pipeline(VkCommandBuffer a_cmd_buf);
    void cmd_exec_update_pipelines(VkCommandBuffer a_cmd_buf);

};


