set(target_name "basic_renderer")
find_package(imguizmo CONFIG REQUIRED)

set (glsl_dir "${etc_dir}/glsl/${target_name}")
set (spirv_dir "${cache_dir}/spirv/${target_name}")

file (GLOB cfg_json "${etc_dir}/basic_renderer.json")
source_group("" FILES cfg_json)

file (GLOB cpp_src "*.cpp" "*.hpp" )
source_group("cpp" FILES ${cpp_src})

file (GLOB_RECURSE glsl_src "${glsl_dir}/*.*")
list(FILTER glsl_src EXCLUDE REGEX ".*\.spv")
source_group(TREE ${glsl_dir} PREFIX glsl FILES ${glsl_src})

add_executable(${target_name} ${cpp_src} ${glsl_src} ${cfg_json})

target_link_libraries(${target_name} PRIVATE ${lib_target} imguizmo::imguizmo)

target_compile_definitions (${target_name} PRIVATE 
    BASIC_RENDERER_ASSETS_DIR="${assets_relative_dir}"
    BASIC_RENDERER_BLUE_NOISE_DIR="${assets_relative_dir}/blue_noise"
    BASIC_RENDERER_GLTF_DIR="${assets_relative_dir}/gltf"
    BASIC_RENDERER_CONFIG_JSON_PATH="${etc_relative_dir}/basic_renderer.json"
    BASIC_RENDERER_GLSL_DIR="${etc_relative_dir}/glsl/${target_name}"
    BASIC_RENDERER_CACHE_DIR="${cache_relative_dir}/${target_name}"
    BASIC_RENDERER_IMGUI_INI="${cache_relative_dir}/${target_name}/imgui.ini"
    IMGUI_EMBEDED_FONT_DIR="${assets_relative_dir}/fonts"
)

if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
    target_compile_options (${target_name} 
        PRIVATE 
            /EHsc   # Full compiler support for the Standard C++ exception handling model that safely unwinds stack objects
            /MP     # Build with Multiple Processes
            "/wd6011;"
    )
    foreach (_output ARCHIVE LIBRARY RUNTIME)
        foreach (_config DEBUG RELEASE RELWITHDEBINFO)
            string (TOLOWER ${_config} _output_dir_name)
            set_target_properties (${target_name} PROPERTIES 
                ${_output}_OUTPUT_DIRECTORY_${_config} "${CMAKE_SOURCE_DIR}/win_${_output_dir_name}"
            )
        endforeach () # _config
    endforeach () # _output

    set_target_properties(${target_name} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/build")

endif () # CMAKE_CXX_COMPILER_ID MATCHES "MSVC"

if (CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    foreach (_output ARCHIVE LIBRARY RUNTIME)
        if (CMAKE_BUILD_TYPE MATCHES "Debug")
            set_target_properties (${target_name} PROPERTIES 
                ${_output}_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/nix_debug"
            )
        else ()
            set_target_properties (${target_name} PROPERTIES 
                ${_output}_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/nix_release"
            )
        endif ()
    endforeach () # _output

endif () # CMAKE_CXX_COMPILER_ID MATCHES "GNU"

