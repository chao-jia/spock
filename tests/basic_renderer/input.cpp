#include "input.hpp"
#include <nlohmann/json.hpp>

#include <ImGuizmo.h>
#include <stb_image.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/integer.hpp>
#include <glm/ext.hpp>

#if _MSC_VER
#pragma warning(disable : 6011) // dereferencing NULL pointer
#endif

namespace stdfs = std::filesystem;
using namespace std::placeholders;


namespace {

class stbi_buffer_t {
private:
    stbi_uc* ptr_;
public:
    stbi_buffer_t(const stbi_buffer_t&) = delete;
    stbi_buffer_t& operator=(const stbi_buffer_t&) = delete;

    stbi_buffer_t(stbi_buffer_t&& a_other) noexcept : ptr_{ std::exchange(a_other.ptr_, nullptr) } {}
    stbi_buffer_t& operator=(stbi_buffer_t&& a_other) noexcept { 
        if (this != &a_other) {
            stbi_image_free(ptr_);
            ptr_ = std::exchange(a_other.ptr_, nullptr);
        }
        return *this;
    }

    stbi_buffer_t() : ptr_{ nullptr } {}
    stbi_buffer_t(stbi_uc* a_ptr) : ptr_(a_ptr) {}
    ~stbi_buffer_t() { if (ptr_) stbi_image_free(ptr_); }
    operator stbi_uc* () const { return ptr_; }
};

inline glm::mat4 
get_scene_light_data(
    const scene_t& a_scene, 
    const float a_soft_shadow_radius, 
    float* a_light_strength_step,
    glm::mat2x4* a_packed_light_data
)
{
    glm::vec3 pos{};
    glm::vec3 dir{};
    glm::vec3 intensity{};
    *a_light_strength_step = 10.f;
    const glm::vec3 dir0 = glm::vec3(0.f, -1.f, 0.f);

    const auto spotlights = a_scene.get_spotlights();
    const auto point_lights = a_scene.get_point_lights();
    const auto directional_lights = a_scene.get_directional_lights();
    if (directional_lights.first > 0) {
        *a_light_strength_step = 0.1f;
        const auto aabb_lower = a_scene.aabb_lower();
        const auto aabb_upper = a_scene.aabb_upper();
        pos = 0.5f * (aabb_lower + aabb_upper);
        pos.y = aabb_upper.y;
        intensity = directional_lights.second->intensity;
        dir = directional_lights.second->direction;
    }
    else if (spotlights.first > 0) {
        intensity = spotlights.second->intensity;
        pos = spotlights.second->position;
        dir = spotlights.second->direction;
    }
    else if (point_lights.first > 0) {
        intensity = point_lights.second->intensity;
        pos = point_lights.second->position;
        dir = dir0;
    }
    else {
        err("scene " + a_scene.identifier() + " does not have a punctual light");
    }

    float color_factor = std::max({ intensity.x, intensity.y, intensity.z });

    SPOCK_PARANOID(
        color_factor > std::numeric_limits<float>::epsilon(), 
        "scene " + a_scene.identifier() + " has a punctual light of zero intensity"
    );

    glm::vec4 color{ intensity / color_factor, 1.f };
    (*a_packed_light_data)[0] = glm::vec4(pos, glm::uintBitsToFloat(glm::packUnorm4x8(color)));
    (*a_packed_light_data)[1] = glm::vec4(oct_encode_dir(dir), a_soft_shadow_radius, color_factor);

    glm::vec3 axis{ 1.f, 0.f, 0.f };
    const auto radians = 2.f * std::atan2(glm::length(dir - dir0), glm::length(dir + dir0));
    if (radians > glm::epsilon<float>()) {
        axis = glm::normalize(glm::cross(dir0, dir));
    }
    glm::mat4 xform = glm::rotate(glm::identity<glm::mat4>(), radians, axis);
    xform[3] = glm::vec4(pos, 1.f);
    return xform;
}


struct light_gizmo_t {
    ImGuizmo::MODE mode{ ImGuizmo::LOCAL };
    ImGuizmo::OPERATION op{ ImGuizmo::TRANSLATE };

    void prepare(
        const glm::mat4& a_view,
        const glm::mat4& a_proj,
        const glm::vec2& a_viewport_pos,
        const glm::vec2& a_viewport_dim,
        glm::mat4* a_transform
    )
    {
        if (ImGui::RadioButton("Translate", op == ImGuizmo::TRANSLATE)) {
            op = ImGuizmo::TRANSLATE;
        }
        ImGui::SameLine();
        if (ImGui::RadioButton("Rotate", op == ImGuizmo::ROTATE)) {
            op = ImGuizmo::ROTATE;
        }
        std::array<float, 3> t{}, r{}, s{};
        ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(*a_transform), t.data(), r.data(), s.data());
        ImGui::DragFloat3("Tr", t.data(), 0.1f);
        ImGui::DragFloat3("Rt", r.data(), 0.2f);
        ImGuizmo::RecomposeMatrixFromComponents(t.data(), r.data(), s.data(), glm::value_ptr(*a_transform));

        if (ImGui::RadioButton("Local", mode == ImGuizmo::LOCAL)) {
            mode = ImGuizmo::LOCAL;
        }
        ImGui::SameLine();
        if (ImGui::RadioButton("World", mode == ImGuizmo::WORLD)) {
            mode = ImGuizmo::WORLD;
        }
        ImGuizmo::SetRect(a_viewport_pos.x, a_viewport_pos.y, a_viewport_dim.x, a_viewport_dim.y);
        ImGuizmo::Manipulate(glm::value_ptr(a_view), glm::value_ptr(a_proj), op, mode, glm::value_ptr(*a_transform));
    }

};


enum spv_e {
    SPV_GRID_VERT = 0,
    SPV_GRID_FRAG,
    SPV_READBACK_COMP,
    SPV_COUNT,
    SPV_GRID_BEGIN = SPV_GRID_VERT,
    SPV_GRID_COUNT = SPV_GRID_FRAG + 1 - SPV_GRID_BEGIN
};


enum ppl_e {
    PPL_GRA_GRID = 0,
    PPL_COM_READBACK,
    PPL_COUNT
};

constexpr char* k_blue_noise_paths[] = {
    BASIC_RENDERER_BLUE_NOISE_DIR"/LDR_RGBA_0.png",
    BASIC_RENDERER_BLUE_NOISE_DIR"/LDR_RGBA_1.png",
    BASIC_RENDERER_BLUE_NOISE_DIR"/LDR_RGBA_2.png",
    BASIC_RENDERER_BLUE_NOISE_DIR"/LDR_RGBA_3.png",
};

} // anonymous namespace


svgf_cfg_t::
svgf_cfg_t(const ordered_json & a_svgf_json)
{
    SPOCK_ENSURE(a_svgf_json.contains("temporal_alpha"), "cfg.render.svgf does not have temporal_alpha");
    SPOCK_ENSURE(a_svgf_json.contains("bilateral_sigma_d"), "cfg.render.svgf does not have bilateral_sigma_d");
    SPOCK_ENSURE(a_svgf_json.contains("bilateral_sigma_p"), "cfg.render.svgf does not have bilateral_sigma_p");
    SPOCK_ENSURE(a_svgf_json.contains("bilateral_sigma_n"), "cfg.render.svgf does not have bilateral_sigma_n");
    SPOCK_ENSURE(a_svgf_json.contains("edge_stop_sigma_p"), "cfg.render.svgf does not have edge_stop_sigma_p");
    SPOCK_ENSURE(a_svgf_json.contains("edge_stop_sigma_n"), "cfg.render.svgf does not have edge_stop_sigma_n");
    SPOCK_ENSURE(a_svgf_json.contains("edge_stop_sigma_l"), "cfg.render.svgf does not have edge_stop_sigma_l");
    SPOCK_ENSURE(a_svgf_json.contains("a_trous_iteration_count"), "cfg.render.svgf does not have a_trous_iteration_count");

    temporal_alpha = a_svgf_json["temporal_alpha"].get<float>();
    bilateral_sigma_d = a_svgf_json["bilateral_sigma_d"].get<float>();
    bilateral_sigma_p = a_svgf_json["bilateral_sigma_p"].get<float>();
    bilateral_sigma_n = a_svgf_json["bilateral_sigma_n"].get<float>();
    edge_stop_sigma_p = a_svgf_json["edge_stop_sigma_p"].get<float>();
    edge_stop_sigma_n = a_svgf_json["edge_stop_sigma_n"].get<float>();
    edge_stop_sigma_l = a_svgf_json["edge_stop_sigma_l"].get<float>();
    a_trous_iteration_count = a_svgf_json["a_trous_iteration_count"].get<int>();
}


scene_cfg_t::
scene_cfg_t(const ordered_json & a_scene_json, const std::string & a_scene_name)
    : ddgi_cfg(a_scene_json["ddgi"])
{
    SPOCK_ENSURE(a_scene_json.contains("gltf"), a_scene_name + " does not have gltf list");
    SPOCK_ENSURE(a_scene_json.contains("timer_filter_width"), a_scene_name + " does not have timer_filter_width");
    SPOCK_ENSURE(a_scene_json.contains("soft_shadow_radius"), a_scene_name + " does not have soft_shadow_radius");
    SPOCK_ENSURE(a_scene_json.contains("ddgi"), a_scene_name + " does not have ddgi");
    name = a_scene_name;
    const auto& gltf_dir_list = a_scene_json["gltf"].get<std::vector<std::string>>();
    gltf_count = gltf_dir_list.size(); 

    spock::ensure(gltf_count > 0, a_scene_name + " should not be empty");

    gltf_list = std::make_unique<std::string[]>(gltf_count);
    for (size_t i = 0; i < gltf_count; ++i) {
        const auto& gltf_name = gltf_dir_list[i];
        gltf_list[i] = std::string(BASIC_RENDERER_GLTF_DIR"/" + gltf_name + '/' + gltf_name + ".gltf"); // dir name and gltf file name must be the same
    }
    timer_filter_width = a_scene_json["timer_filter_width"].get<uint32_t>();
    soft_shadow_radius = a_scene_json["soft_shadow_radius"].get<float>();
}


input_cfg_t::input_cfg_t(const ordered_json & a_cfg_json)
{
    SPOCK_ENSURE(a_cfg_json["render"].contains("svgf"), "cfg_json::renderer does not have svgf");
    SPOCK_ENSURE(a_cfg_json["render"].contains("reflection"), "cfg_json::renderer does not have reflection");
    svgf_cfg = svgf_cfg_t(a_cfg_json["render"]["svgf"]);
    reflection_cfg = reflection_t::cfg_t(a_cfg_json["render"]["reflection"]);

    const auto& scenes_config_json = a_cfg_json["scenes"];
    for (const auto& scene_config_json : scenes_config_json.items()) {
        scene_cfgs.emplace_back(scene_config_json.value(), scene_config_json.key());
    }
    for (const auto& scene_cfg : scene_cfgs) {
        scene_name_list.push_back(scene_cfg.name.c_str());
    }
    spock::ensure(!scene_cfgs.empty(), "config.scenes should not be empty");
}


void input_t::
load_blue_noise_images(const context_t& a_context, VkCommandBuffer a_cmd_buf, VkQueue a_queue)
{
    host_timer_t timer;
    constexpr uint32_t blue_noise_count = length_of_c_array(k_blue_noise_paths);
    VkExtent3D image_dims[blue_noise_count] = {};
    images_ = std::make_unique<image_t[]>(blue_noise_count);
    image_views_ = std::make_unique<image_view_t[]>(blue_noise_count);

    stbi_buffer_t buffers[blue_noise_count] = {};
    constexpr uint32_t required_channel_count = static_cast<uint32_t>(STBI_rgb_alpha);
    for (size_t i = 0; i < blue_noise_count; ++i) {
        int w = 0, h = 0, ch = 0;
        const char* file_path = k_blue_noise_paths[i];
        buffers[i] = stbi_buffer_t(stbi_load(file_path, &w, &h, &ch, required_channel_count));
        image_dims[i] = VkExtent3D{ static_cast<uint32_t>(w), static_cast<uint32_t>(h), 1 };
        SPOCK_ENSURE(buffers[i] != nullptr, std::string("failed to load blue noise image: ") + file_path);
        SPOCK_ENSURE(
            ch == required_channel_count, 
            std::string("blue noise texture does not have 4 channels: ") + file_path
        );
        const auto create_info = build_VkImageCreateInfo(
            VK_FORMAT_R8G8B8A8_UNORM, 
            VkExtent3D{static_cast<uint32_t>(w), static_cast<uint32_t>(h), 1u}, 
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT
        );
        images_[i] = image_t(a_context, create_info);
        image_views_[i] = image_view_t(a_context, build_VkImageViewCreateInfo(images_[i]));
    }

    auto cmd_buf_begin_info = build_VkCommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    SPOCK_VK_CALL(vkBeginCommandBuffer(a_cmd_buf, &cmd_buf_begin_info));
    buffer_t staging_buffers[blue_noise_count] = {};
    for (size_t i = 0; i < blue_noise_count; ++i) {
        const uint32_t bytes = image_dims[i].width * image_dims[i].height * required_channel_count;
        staging_buffers[i] = images_[i].cmd_upload(
            a_cmd_buf, 
            buffers[i], bytes, build_VkImageSubresourceLayers(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_ACCESS_SHADER_READ_BIT
        );
    }
    SPOCK_VK_CALL(vkEndCommandBuffer(a_cmd_buf));
    const auto submit_info = build_VkSubmitInfo(1, &a_cmd_buf);
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));
    info("input_t::load_blue_noise_images took " + std::to_string(timer.elapsed_ms()) + " ms.");
}


// depends on scene_.get_num_textures(), called within init_scene()
void input_t::
create_descriptor_set_layouts(VkDevice a_vk_dev)
{
    constexpr size_t blue_noise_count = length_of_c_array(k_blue_noise_paths);
    // UI
    {
        const VkShaderStageFlags stage_flags = VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_RAYGEN_BIT_KHR;
        const VkDescriptorSetLayoutBinding ui_bindings[] = {
            build_VkDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_ALL),
            build_VkDescriptorSetLayoutBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_COMPUTE_BIT),
            build_VkDescriptorSetLayoutBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, blue_noise_count, VK_SHADER_STAGE_ALL),
        };
        descriptor_set_layouts_[INPUT_DS_LAYOUT_UI] = descriptor_set_layout_t(
            a_vk_dev, build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(ui_bindings), ui_bindings)
        );
    }
    const uint32_t scene_num_textures = scene_.get_num_textures();
    // SCENE
    {
        const VkShaderStageFlags stage_flags = VK_SHADER_STAGE_FRAGMENT_BIT 
            | VK_SHADER_STAGE_COMPUTE_BIT 
            | VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR
            ;

        VkDescriptorSetLayoutBinding bindings[] = {
            build_VkDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, scene_num_textures, stage_flags), // mtl_textures
            build_VkDescriptorSetLayoutBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, stage_flags), // punctual_lights
            build_VkDescriptorSetLayoutBinding(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, stage_flags), // scene_offsets_of_mtl_types
            build_VkDescriptorSetLayoutBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, stage_flags), // mtl_param
            build_VkDescriptorSetLayoutBinding(4, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, stage_flags), // triangle_mtl_idx_buffer
            build_VkDescriptorSetLayoutBinding(5, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, stage_flags), // position_buffer
            build_VkDescriptorSetLayoutBinding(6, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, stage_flags), // normal_buffer
            build_VkDescriptorSetLayoutBinding(7, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, stage_flags), // uv_buffer
            build_VkDescriptorSetLayoutBinding(8, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, stage_flags), // index_buffer
            build_VkDescriptorSetLayoutBinding(9, VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1, stage_flags)
        };
        descriptor_set_layouts_[INPUT_DS_LAYOUT_SCENE] = descriptor_set_layout_t(
            a_vk_dev, build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(bindings), bindings)
        );
    }

    const VkDescriptorPoolSize descriptor_pool_sizes[] = {
        build_VkDescriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 4), // frame_params + punctual_lights + offsets_of_mtl_types +  mtl_param
        build_VkDescriptorPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 6), // position_buffer + normal + uv + index + triangle_mtl_idx_buffer + host_dev_msg
        build_VkDescriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, scene_num_textures + blue_noise_count),
        build_VkDescriptorPoolSize(VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1),
    };

    descriptor_pool_ = descriptor_pool_t(
        a_vk_dev, 
        build_VkDescriptorPoolCreateInfo(INPUT_DS_COUNT, length_of_c_array(descriptor_pool_sizes), descriptor_pool_sizes)
    );
    const VkDescriptorSetLayout layouts[] = {
        descriptor_set_layouts_[INPUT_DS_LAYOUT_UI],
        descriptor_set_layouts_[INPUT_DS_LAYOUT_SCENE],
    };
    SPOCK_PARANOID(
        length_of_c_array(layouts) == static_cast<size_t>(INPUT_DS_COUNT), 
        "deferred_t::create_descriptor_set_layouts: mismatch between descriptor set count and descriptor set layout count"
    );
    descriptor_pool_.alloc_descriptor_sets(INPUT_DS_COUNT, layouts, descriptor_sets_.data());

}


void input_t::
write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler)
{
    // UI
    {
        constexpr size_t blue_noise_count = length_of_c_array(k_blue_noise_paths);
        VkDescriptorImageInfo image_infos[blue_noise_count];
        for (size_t i = 0; i < blue_noise_count; ++i) {
            image_infos[i] = build_VkDescriptorImageInfo(a_dummy_sampler, image_views_[i], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        }
        const VkDescriptorBufferInfo buffer_infos[] = {
            build_VkDescriptorBufferInfo(frame_params_uniform_buffer_, 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(host_dev_msg_buffer_, 0, VK_WHOLE_SIZE),
        };
        VkWriteDescriptorSet writes[] = {
            build_VkWriteDescriptorSet(descriptor_sets_[INPUT_DS_UI], 0, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, nullptr, buffer_infos + 0),
            build_VkWriteDescriptorSet(descriptor_sets_[INPUT_DS_UI], 1, 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, nullptr, buffer_infos + 1),
            build_VkWriteDescriptorSet(descriptor_sets_[INPUT_DS_UI], 2, blue_noise_count, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, image_infos),
        };
        vkUpdateDescriptorSets(a_vk_dev, length_of_c_array(writes), writes, 0, nullptr);
    }
    // SCENE
    {
        enum bo_idx_e {
            BO_IDX_PUNCTUAL_LIGHT = 0,
            BO_IDX_OFFSETS_OF_MTL_TYPE,
            BO_IDX_MTL_PARAM,
            BO_IDX_TRIANGLE_MTL_IDX,
            BO_IDX_POSITION,
            BO_IDX_NORMAL,
            BO_IDX_UV,
            BO_IDX_INDEX,
            BO_IDX_COUNT
        };

        const VkDescriptorBufferInfo buffer_infos[] = {
            build_VkDescriptorBufferInfo(scene_.punctual_light_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.offsets_of_mtl_type_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.mtl_param_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.triangle_mtl_idx_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.position_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.normal_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.uv_buffer(), 0, VK_WHOLE_SIZE),
            build_VkDescriptorBufferInfo(scene_.index_buffer(), 0, VK_WHOLE_SIZE),
        };
        const auto tlas_write = build_VkWriteDescriptorSetAccelerationStructureKHR(1, &scene_tlas_());

        const auto& ds = descriptor_sets_[INPUT_DS_SCENE];
        const VkWriteDescriptorSet writes[] = {
            build_VkWriteDescriptorSet(ds, 0, scene_.get_num_textures(), VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, scene_.image_infos()),
            build_VkWriteDescriptorSet(ds, 1, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, nullptr, buffer_infos + BO_IDX_PUNCTUAL_LIGHT),
            build_VkWriteDescriptorSet(ds, 2, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, nullptr, buffer_infos + BO_IDX_OFFSETS_OF_MTL_TYPE),
            build_VkWriteDescriptorSet(ds, 3, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, nullptr, buffer_infos + BO_IDX_MTL_PARAM),
            build_VkWriteDescriptorSet(ds, 4, 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, nullptr, buffer_infos + BO_IDX_TRIANGLE_MTL_IDX),
            build_VkWriteDescriptorSet(ds, 5, 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, nullptr, buffer_infos + BO_IDX_POSITION),
            build_VkWriteDescriptorSet(ds, 6, 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, nullptr, buffer_infos + BO_IDX_NORMAL),
            build_VkWriteDescriptorSet(ds, 7, 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, nullptr, buffer_infos + BO_IDX_UV),
            build_VkWriteDescriptorSet(ds, 8, 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, nullptr, buffer_infos + BO_IDX_INDEX),
            build_VkWriteDescriptorSet(ds, 9, 1, VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, nullptr, nullptr, nullptr, 0, &tlas_write)
        };

        SPOCK_PARANOID(length_of_c_array(writes) == length_of_c_array(buffer_infos) + 2, "not every VkWriteDescriptorSet is initialized");
        vkUpdateDescriptorSets(a_vk_dev, length_of_c_array(writes), writes, 0, nullptr);
    }
}


void input_t::
build_acceleration_structures(const context_t& a_context, const command_pool_t& a_cmd_pool, const queue_t& a_queue)
{
    enum {
        AS_DEV_TIMER_BLAS = 0,
        AS_DEV_TIMER_TLAS,
        AS_DEV_TIMER_COUNT
    };
    enum {
        AS_HOST_TIMER_CLEAR = 0,
        AS_HOST_TIMER_PREPROC,
        AS_HOST_TIMER_BLAS_CREATE,
        AS_HOST_TIMER_BLAS_BUILD,
        AS_HOST_TIMER_TLAS,
        AS_HOST_TIMER_COUNT
    };

    dev_timer_array_t as_dev_timer_array(a_context, true, AS_DEV_TIMER_COUNT, 3, 1);
    spock::info("start building acceleration structures...");
    std::array<host_timer_t, AS_HOST_TIMER_COUNT> as_host_timers{};
    std::array<float, AS_HOST_TIMER_COUNT> as_host_elapsed_ms{};

    // reset
    as_host_timers[AS_HOST_TIMER_CLEAR].start();
    scene_tlas_instances_.clear();
    scene_blases_.clear();
    scene_tlas_ = acceleration_structure_t{};
    mtl_type_tlas_instance_idx_map_.clear();
    tlas_has_gray_dot_ = false;

    as_host_elapsed_ms[AS_HOST_TIMER_CLEAR] = as_host_timers[AS_HOST_TIMER_CLEAR].elapsed_ms();
    as_host_timers[AS_HOST_TIMER_PREPROC].start();

    scene_tlas_builder_ = {};
    blas_builder_array_t scene_blas_builder_array;

    VkDeviceAddress position_buffer_addr = a_context.get_buffer_device_address(scene_.position_buffer());
    VkDeviceAddress index_buffer_addr = a_context.get_buffer_device_address(scene_.index_buffer());

    const auto& vertex_offsets = scene_.vertex_offsets();
    const auto& index_offsets = scene_.index_offsets();

    const std::unordered_set<mtl_type_e> opaque_mtl_types = {MTL_TYPE_PRIMARY, MTL_TYPE_GRAY_DOT, MTL_TYPE_BLEND}; // TODO: fix blend
    for (uint32_t i = 0; i < static_cast<uint32_t>(MTL_TYPE_COUNT); ++i) {
        const auto mtl_type_idx = static_cast<mtl_type_e>(i);
        const auto next_mtl_type_idx = mtl_type_idx + 1;
        const auto index_count = index_offsets[next_mtl_type_idx] - index_offsets[mtl_type_idx];
        uint32_t sbt_offset = 0; // opaque
        VkGeometryFlagsKHR blas_geom_flags = VK_GEOMETRY_OPAQUE_BIT_KHR; // override with gl_RayFlagsNoOpaqueEXT to invoke any hit shader
        if (index_count > 0) {
            if (opaque_mtl_types.find(mtl_type_idx) == opaque_mtl_types.end()) {
                blas_geom_flags = 0;
                sbt_offset = 1; // alpha 
            }
            else {
                blas_geom_flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
                sbt_offset = 0; // opaque 
                if (mtl_type_idx == MTL_TYPE_GRAY_DOT) {
                    tlas_has_gray_dot_ = true;
                }
            }
            scene_blas_builder_array.add_build_info(
                build_VkAccelerationStructureGeometryKHR(
                    blas_geom_flags,
                    build_VkAccelerationStructureGeometryTrianglesDataKHR(
                        scene_t::k_vk_format_position,
                        { position_buffer_addr },
                        sizeof(scene_t::position_type),
                        vertex_offsets.back(), // index_offsets[next_mtl_type_idx]
                        scene_t::k_vk_index_type,
                        { index_buffer_addr }
                )),
                build_VkAccelerationStructureBuildRangeInfoKHR(
                    index_count / 3,
                    index_offsets[mtl_type_idx] * sizeof(scene_t::index_type),
                    0
                ),
                VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR, 
                false
            );
            mtl_type_tlas_instance_idx_map_[mtl_type_idx] = static_cast<uint32_t>(scene_tlas_instances_.size());
            scene_tlas_instances_.push_back(
                build_VkAccelerationStructureInstanceKHR(
                    glm::identity<glm::mat4>(),
                    sbt_offset,
                    0,
                    0, // will be updated by blas_builder_array_t::create_blases(...)
                    mtl_type_idx
            ));
        } // endif (index_count > 0)
    }
    const uint32_t scene_blas_count = scene_blas_builder_array.resolve_infos(a_context);
    scene_blases_.resize(scene_blas_count);
    as_host_elapsed_ms[AS_HOST_TIMER_PREPROC] = as_host_timers[AS_HOST_TIMER_PREPROC].elapsed_ms();

    as_host_timers[AS_HOST_TIMER_BLAS_CREATE].start();
    scene_blas_builder_array.create_blases(a_context, scene_blases_.data(), scene_tlas_instances_.data());
    as_host_elapsed_ms[AS_HOST_TIMER_BLAS_CREATE] = as_host_timers[AS_HOST_TIMER_BLAS_CREATE].elapsed_ms();

    as_dev_timer_array.begin();

    const uint32_t cmd_buf_idx = 0;
    const command_buffer_t cmd_buf{ a_cmd_pool };
    auto cmd_buf_begin_info = make_VkCommandBufferBeginInfo();
    cmd_buf_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf, &cmd_buf_begin_info));
    as_dev_timer_array.cmd_start(cmd_buf, AS_DEV_TIMER_BLAS);

    as_host_timers[AS_HOST_TIMER_BLAS_BUILD].start();
    auto blas_scratch_buffers = scene_blas_builder_array.cmd_initial_build(a_context, cmd_buf);

    auto barrier = make_VkMemoryBarrier();
    barrier.srcAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    barrier.dstAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR;
    vkCmdPipelineBarrier(cmd_buf,
        VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR, VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR,
        0, 1, &barrier, 0, nullptr, 0, nullptr
    );
    as_host_elapsed_ms[AS_HOST_TIMER_BLAS_BUILD] = as_host_timers[AS_HOST_TIMER_BLAS_BUILD].elapsed_ms();
    as_dev_timer_array.cmd_stop(cmd_buf, AS_DEV_TIMER_BLAS);

    as_host_timers[AS_HOST_TIMER_TLAS].start();
    as_dev_timer_array.cmd_start(cmd_buf, AS_DEV_TIMER_TLAS);
    scene_tlas_ = scene_tlas_builder_.cmd_initial_build( a_context, cmd_buf, 
        static_cast<uint32_t>(scene_tlas_instances_.size()), scene_tlas_instances_.data(),
        VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR
    );
    as_host_elapsed_ms[AS_HOST_TIMER_TLAS] = as_host_timers[AS_HOST_TIMER_TLAS].elapsed_ms();
    as_dev_timer_array.cmd_stop(cmd_buf, AS_DEV_TIMER_TLAS);

    SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf));
    const auto submit_info = build_VkSubmitInfo(1, &cmd_buf());
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));

    as_dev_timer_array.end();
    spock::info("building acceleration structure took: ");
    spock::info("    host: " + 
        std::to_string(as_host_elapsed_ms[AS_HOST_TIMER_CLEAR]) + " ms (clear); " + 
        std::to_string(as_host_elapsed_ms[AS_HOST_TIMER_PREPROC]) + " ms (preproc); " + 
        std::to_string(as_host_elapsed_ms[AS_HOST_TIMER_BLAS_CREATE]) + " ms (blas create); " + 
        std::to_string(as_host_elapsed_ms[AS_HOST_TIMER_BLAS_BUILD]) + " ms (blas build); " + 
        std::to_string(as_host_elapsed_ms[AS_HOST_TIMER_TLAS]) + " ms (tlas create + build)" 
    );
    spock::info("    dev:  " + 
        std::to_string(as_dev_timer_array.elapsed_ms(AS_DEV_TIMER_BLAS)) + " ms (blas); " + 
        std::to_string(as_dev_timer_array.elapsed_ms(AS_DEV_TIMER_TLAS)) + " ms (tlas)"
    );

}


input_t::input_t()
    : light_transform_{}
    , light_strength_step_{}
    , descriptor_set_layouts_{}
    , descriptor_sets_{}
    , tlas_has_gray_dot_{ false }
{
}


input_t::
input_t(
    const spock::ordered_json & a_cfg_json, 
    const context_t & a_context, 
    VkCommandBuffer a_cmd_buf, 
    VkQueue a_queue,
    size_t a_frame_params_uniform_buffer_bytes,
    size_t a_host_dev_msg_buffer_bytes,
    uint32_t a_blue_noise_texture_count
)
    : light_transform_{ glm::mat4(1.f) }
    , light_strength_step_{ 10.f }
    , tlas_has_gray_dot_{ false }
    , input_cfg_{ a_cfg_json }
{
    SPOCK_PARANOID(
        length_of_c_array(k_blue_noise_paths) == a_blue_noise_texture_count, 
        "input_t::input: mismatch between blue_noise_paths and a_blue_noise_texture_count"
    );
    load_blue_noise_images(a_context, a_cmd_buf, a_queue);
    // ubo: frame_params
    frame_params_uniform_buffer_ = buffer_t(a_context,
        build_VkBufferCreateInfo(a_frame_params_uniform_buffer_bytes, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT),
        build_VmaAllocationCreateInfo(
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        )
    );
    frame_params_uniform_buffer_.map();

    // ubo: host_dev_msg_t
    host_dev_msg_buffer_ = buffer_t(a_context,
        build_VkBufferCreateInfo(a_host_dev_msg_buffer_bytes, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT),
        build_VmaAllocationCreateInfo(
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        )
    );
    host_dev_msg_buffer_.map();
}


void input_t::
init_scene(
    int a_scene_idx, 
    const context_t& a_context, 
    const command_pool_t& a_cmd_pool, 
    const queue_t& a_queue, 
    VkSampler a_dummy_sampler,
    glm::mat2x4* a_packed_light_data
)
{
    namespace stdfs = std::filesystem;

    const auto& scene_cfg = input_cfg_.scene_cfgs[a_scene_idx];
    const std::string scene_cache_dir = BASIC_RENDERER_CACHE_DIR"/" + scene_cfg.name;
    if (!stdfs::exists(scene_cache_dir)) {
        stdfs::create_directories(scene_cache_dir);
    }
    const std::string scene_params_hsl_name = "scene_params.hsl";

    const spock_scene_flags_t scene_flags = SPOCK_SCENE_POSITION_NORMAL_UNIFORM_TEXEL_BUFFER_BIT 
        | SPOCK_SCENE_POSITION_NORMAL_STORAGE_BUFFER_BIT | SPOCK_SCENE_ALIGNED_BUFFER_STORAGE_BUFFER_BIT 
        | SPOCK_SCENE_ALIGNED_BUFFER_UNIFORM_TEXEL_BUFFER_BIT
        | SPOCK_SCENE_RAY_TRACING_SUPPORT_BIT
        ;
    scene_ = scene_t(
        a_context, a_cmd_pool, a_queue, scene_cfg.gltf_count, scene_cfg.gltf_list.get(),
        scene_cache_dir + "/scene.bin", scene_cache_dir + '/' + scene_params_hsl_name,
        scene_cfg.name.data(), scene_flags
    );
    light_transform_ = get_scene_light_data(scene_, scene_cfg.soft_shadow_radius, &light_strength_step_, a_packed_light_data);
    build_acceleration_structures(a_context, a_cmd_pool, a_queue);

    create_descriptor_set_layouts(a_context);
    write_descriptor_sets(a_context, a_dummy_sampler);
}


bool input_t::
init_spirvs(uint32_t a_vk_api_version)
{
    const std::string glsl_relative_paths[] = {
        "grid.vert",
        "grid.frag",
        "readback.comp",
    };
    constexpr auto glsl_count = length_of_c_array(glsl_relative_paths);
    spirvs_ = std::make_unique<spirv_t[]>(glsl_count);
    return create_spirvs(a_vk_api_version, length_of_c_array(glsl_relative_paths), glsl_relative_paths, scene_.identifier(), spirvs_.get());
}


bool input_t::
refresh_spirvs()
{
    return reload_spirvs(spirvs_.get(), SPV_COUNT, "input_t: ");
}



void input_t::
create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, VkRenderPass a_shading_pass)
{
    host_timer_t timer;

    shader_module_array_t shader_module_array{ a_vk_dev, spirvs_.get(), SPV_COUNT };
    pipelines_ = std::make_unique<pipeline_t[]>(PPL_COUNT);

    const auto shader_stage_create_infos = build_shader_stage_create_infos(shader_module_array);
    // GRID
    {
        const auto ppl_idx = PPL_GRA_GRID;
        const uint32_t shader_stage_count = SPV_GRID_COUNT;

        graphics_pipeline_create_info_helper_t helper{};
        helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);

        VkGraphicsPipelineCreateInfo ppl_create_info = helper.resolve(
            a_vk_dev, a_shading_pass, 0,
            a_pipeline_layout,
            shader_stage_count, shader_stage_create_infos.get() + SPV_GRID_BEGIN
        );
        spock::create_graphics_pipelines(a_vk_dev, 1, &ppl_create_info, pipelines_.get() + PPL_GRA_GRID);
    }
    // READBACK
    {
        const auto compute_pipeline_create_info = build_VkComputePipelineCreateInfo(shader_stage_create_infos[SPV_READBACK_COMP], a_pipeline_layout);
        create_compute_pipelines(a_vk_dev, 1, &compute_pipeline_create_info, pipelines_.get() + PPL_COM_READBACK);
    }

    spock::info("input_t::create_pipelines took " + std::to_string(timer.elapsed_ms()) + " ms");
}


void input_t::
render_imgui_scene_settings(int* a_current_scene_idx)
{
    ImGui::Combo("scene", a_current_scene_idx, input_cfg_.scene_name_list.data(), input_cfg_.scene_name_list.size());
    if (ImGui::TreeNode("SVGF settings")) {
        ImGui::DragFloat("alpha", &input_cfg_.svgf_cfg.temporal_alpha, 0.001f, 0.001f, 1.000f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("temporal_alpha");
        }
        ImGui::DragFloat("bi_sig_d", &input_cfg_.svgf_cfg.bilateral_sigma_d, 0.01f, 0.01f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("bilateral_sigma_d");
        }
        ImGui::DragFloat("bi_sig_p", &input_cfg_.svgf_cfg.bilateral_sigma_p, 0.01f, 0.01f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("bilateral_sigma_p");
        }
        ImGui::DragFloat("bi_sig_n", &input_cfg_.svgf_cfg.bilateral_sigma_n, 0.01f, 0.01f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("bilateral_sigma_n");
        }
        ImGui::DragFloat("es_sig_p", &input_cfg_.svgf_cfg.edge_stop_sigma_p, 0.01f, 0.01f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("edge_stop_sigma_p");
        }
        ImGui::DragFloat("es_sig_n", &input_cfg_.svgf_cfg.edge_stop_sigma_n, 0.01f, 0.01f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("edge_stop_sigma_n");
        }
        ImGui::DragFloat("es_sig_l", &input_cfg_.svgf_cfg.edge_stop_sigma_l, 0.01f, 0.01f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("edge_stop_sigma_l");
        }
        ImGui::SliderInt("a_trous_iter", &input_cfg_.svgf_cfg.a_trous_iteration_count, 0, 8);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("a_trous_iter");
        }
        ImGui::TreePop();
    }
}


void input_t::render_imgui_light_guizmo(
    const glm::mat4 & a_view, 
    const glm::mat4 & a_proj, 
    const glm::vec2 & a_viewport_pos, 
    const glm::vec2 & a_viewport_dim, 
    glm::mat2x4 * a_packed_light_data
)
{
    static light_gizmo_t gizmo;
    ImGui::Text("Light");
    gizmo.prepare(a_view, a_proj, a_viewport_pos, a_viewport_dim, &light_transform_);
    const glm::mat2x4& light_data = *a_packed_light_data;
    glm::vec3 light_color{ glm::unpackUnorm4x8(glm::floatBitsToUint(light_data[0].w)) };
    float soft_shadow_radius = light_data[1].z;
    float light_strength = light_data[1].w;
    ImGui::ColorEdit3("color", glm::value_ptr(light_color));
    ImGui::DragFloat("radius", &soft_shadow_radius, 0.01f, 0.01f, 3.f);
    ImGui::DragFloat("strength", &light_strength, light_strength_step_, 0.1f, 100000.f);
    glm::vec3 light_pos{ light_transform_[3] };
    glm::vec2 light_oct_dir = oct_encode_dir(glm::mat3(light_transform_) * glm::vec3(0.f, -1.f, 0.f));
    (*a_packed_light_data)[0] = glm::vec4(light_pos, glm::uintBitsToFloat(glm::packUnorm4x8(glm::vec4(light_color, 1.f))));
    (*a_packed_light_data)[1] = glm::vec4(light_oct_dir, soft_shadow_radius, light_strength);
}


void input_t::
cmd_rebuild_tlas(VkCommandBuffer a_cmd_buf, const glm::mat4 & a_gray_dot_transform)
{
    SPOCK_PARANOID(tlas_has_gray_dot_, "input_t::cmd_rebuild_tlas: no gray dot present");
    const auto tlas_instance_idx = mtl_type_tlas_instance_idx_map_.at(MTL_TYPE_GRAY_DOT);
    transform_instance(&scene_tlas_instances_[tlas_instance_idx], a_gray_dot_transform);

    scene_tlas_builder_.cmd_update_instance_buffer(a_cmd_buf, 1, scene_tlas_instances_.data() + tlas_instance_idx, tlas_instance_idx);
    scene_tlas_builder_.cmd_update_barrier(a_cmd_buf);
    scene_tlas_builder_.cmd_rebuild(a_cmd_buf);
}


void input_t::
cmd_exec_readback_pipeline(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipelines_[PPL_COM_READBACK]);
    vkCmdDispatch(a_cmd_buf, 1, 1, 1);
}



