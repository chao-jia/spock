#include "reflection.hpp"
#include <nlohmann/json.hpp>

namespace {

enum spv_e {
    SPV_RT_REFLECTION_RAY_RGEN = 0,
    SPV_RT_REFLECTION_RAY_RMISS,
    SPV_RT_REFLECTION_RAY_OPAQUE_RAHIT,
    SPV_RT_REFLECTION_RAY_OPAQUE_RCHIT,
    SPV_RT_REFLECTION_RAY_ALPHA_RAHIT,
    SPV_RT_REFLECTION_RAY_ALPHA_RCHIT,
    SPV_ESTIMATE_VARIANCE_COMP,
    SPV_GAUSSIAN_FILTER_VARIANCE_COMP,
    SPV_A_TROUS_FILTER_COMP,
    SPV_COUNT,
    SPV_RT_BEGIN = SPV_RT_REFLECTION_RAY_RGEN,
    SPV_RT_COUNT = SPV_RT_REFLECTION_RAY_ALPHA_RCHIT + 1 - SPV_RT_BEGIN,
    SPV_COMP_BEGIN = SPV_ESTIMATE_VARIANCE_COMP,
    SPV_COMP_COUNT = SPV_A_TROUS_FILTER_COMP + 1 - SPV_ESTIMATE_VARIANCE_COMP,
};

enum shader_group_handle_e {
    SGH_TRACE_REFLECTION_RAY_RGEN = 0,
    SGH_TRACE_REFLECTION_RAY_RMISS,
    SGH_TRACE_REFLECTION_RAY_HIT_OPAQUE,
    SGH_TRACE_REFLECTION_RAY_HIT_ALPHA,
    SGH_COUNT,
    SGH_TRACE_REFLECTION_RAY_OFFSET = SGH_TRACE_REFLECTION_RAY_RGEN,
    SGH_TRACE_REFLECTION_RAY_HIT_BEGIN = SGH_TRACE_REFLECTION_RAY_HIT_OPAQUE,
    SGH_TRACE_REFLECTION_RAY_HIT_COUNT = SGH_TRACE_REFLECTION_RAY_HIT_ALPHA + 1 - SGH_TRACE_REFLECTION_RAY_HIT_BEGIN,
};

constexpr static uint32_t k_shader_group_idx_offset[] = {
    SGH_TRACE_REFLECTION_RAY_OFFSET,
    SGH_COUNT
};

enum ppl_e {
    PPL_RT_TRACE_REFLECTION_RAY = 0,
    PPL_COM_ESTIMATE_VARIANCE,
    PPL_COM_GAUSSIAN_FILTER_VARIANCE,
    PPL_COM_A_TROUS_FILTER,
    PPL_COUNT,
    PPL_COM_BEGIN = PPL_COM_ESTIMATE_VARIANCE,
};

enum img_e {
    IMG_TRACED_REFLECTION,
    IMG_VARIANCE,
    IMG_GAUSSIAN_FILTERED_VARIANCE,

    IMG_MOMENTS_DIST_DURATION_0,
    IMG_MOMENTS_DIST_DURATION_1,

    IMG_COLOR_0,
    IMG_COLOR_1,

    IMG_COUNT
};

constexpr VkFormat k_img_formats[] = {
    VK_FORMAT_R16G16B16A16_SFLOAT,  // IMG_TRACED_REFLECTION
    VK_FORMAT_R16_UNORM,            // IMG_VARIANCE
    VK_FORMAT_R16G16_UNORM,         // IMG_GAUSSIAN_FILTERED_VARIANCE

    VK_FORMAT_R16G16B16A16_SFLOAT,  // IMG_MOMENTS_DIST_DURATION_0
    VK_FORMAT_R16G16B16A16_SFLOAT,  // IMG_MOMENTS_DIST_DURATION_0

    VK_FORMAT_R16G16B16A16_SFLOAT,  // IMG_COLOR_0
    VK_FORMAT_R16G16B16A16_SFLOAT   // IMG_COLOR_1
};


}


reflection_t::cfg_t::
cfg_t(const ordered_json& a_reflection_json)
{
        SPOCK_ENSURE(a_reflection_json.contains("geometry_weight_factor"), "cfg.render.reflection does not have geometry_weight_factor");
        SPOCK_ENSURE(a_reflection_json.contains("accum_frame_count_max"), "cfg.render.reflection does not have accum_frame_count_max");
        SPOCK_ENSURE(a_reflection_json.contains("accum_base_power"), "cfg.render.reflection does not have accum_base_power");
        SPOCK_ENSURE(a_reflection_json.contains("accum_curve"), "cfg.render.reflection does not have accum_curve");
        SPOCK_ENSURE(a_reflection_json.contains("temporal_weight_threshold"), "cfg.render.reflection does not have temporal_weight_threshold");

        geometry_weight_factor = a_reflection_json["geometry_weight_factor"].get<float>();
        accum_frame_count_max = a_reflection_json["accum_frame_count_max"].get<float>();
        accum_base_power = a_reflection_json["accum_base_power"].get<float>();
        accum_curve = a_reflection_json["accum_curve"].get<float>();
        temporal_weight_threshold = a_reflection_json["temporal_weight_threshold"].get<float>();
}


reflection_t::
reflection_t(VkDevice a_vk_dev)
{
    create_descriptor_set_layout(a_vk_dev);
}


void reflection_t::
create_images(const context_t & a_context, const VkExtent2D & a_surface_extent, VkCommandBuffer a_cmd_buf, VkQueue a_queue)
{
    constexpr VkImageUsageFlags usage_flags = VK_IMAGE_USAGE_STORAGE_BIT;
    VkImageMemoryBarrier image_barriers[IMG_COUNT];

    const VkImageLayout inital_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    const VkImageLayout final_layout = VK_IMAGE_LAYOUT_GENERAL;
    const auto extent = to_extent_3d(a_surface_extent, 1);

    images_ = std::make_unique<image_t[]>(IMG_COUNT);
    image_views_ = std::make_unique<image_view_t[]>(IMG_COUNT);

    for (uint32_t i = 0; i < static_cast<uint32_t>(IMG_COUNT); ++i) {
        auto img = image_t(a_context, build_VkImageCreateInfo(k_img_formats[i], extent, usage_flags));
        image_views_[i] = image_view_t(a_context, build_VkImageViewCreateInfo(img));
        image_barriers[i] = build_VkImageMemoryBarrier(img, inital_layout, final_layout, 0, 0);
        images_[i] = std::move(img);
    }

    auto cmd_buf_begin_info = build_VkCommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    SPOCK_VK_CALL(vkBeginCommandBuffer(a_cmd_buf, &cmd_buf_begin_info));
    vkCmdPipelineBarrier(a_cmd_buf,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0,
        0, nullptr, 0, nullptr, IMG_COUNT, image_barriers
    );
    SPOCK_VK_CALL(vkEndCommandBuffer(a_cmd_buf));
    const auto submit_info = build_VkSubmitInfo(1, &a_cmd_buf);
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));

}


void reflection_t::
write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler)
{
    VkDescriptorImageInfo image_infos[IMG_COUNT];
    for (uint32_t i = 0; i < static_cast<uint32_t>(IMG_COUNT); ++i) {
        image_infos[i] = build_VkDescriptorImageInfo(a_dummy_sampler, image_views_[i], VK_IMAGE_LAYOUT_GENERAL);
    }
    VkWriteDescriptorSet writes[IMG_COUNT]; 
    constexpr auto type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    for (uint32_t i = IMG_TRACED_REFLECTION; i <= static_cast<uint32_t>(IMG_GAUSSIAN_FILTERED_VARIANCE); ++i) {
        writes[i] = build_VkWriteDescriptorSet(descriptor_sets_[REFLECTION_DS_ONE_OFF], i, 1, type, image_infos + i);
    }
    writes[IMG_MOMENTS_DIST_DURATION_0] = build_VkWriteDescriptorSet(descriptor_sets_[REFLECTION_DS_TEMPO_0], 0, 1, type, image_infos + IMG_MOMENTS_DIST_DURATION_0);
    writes[IMG_MOMENTS_DIST_DURATION_1] = build_VkWriteDescriptorSet(descriptor_sets_[REFLECTION_DS_TEMPO_1], 0, 1, type, image_infos + IMG_MOMENTS_DIST_DURATION_1);
    writes[IMG_COLOR_0] = build_VkWriteDescriptorSet(descriptor_sets_[REFLECTION_DS_COLOR_0], 0, 1, type, image_infos + IMG_COLOR_0);
    writes[IMG_COLOR_1] = build_VkWriteDescriptorSet(descriptor_sets_[REFLECTION_DS_COLOR_1], 0, 1, type, image_infos + IMG_COLOR_1);
    vkUpdateDescriptorSets(a_vk_dev, IMG_COUNT, writes, 0, nullptr);
}


bool reflection_t::
init_spirvs(uint32_t a_vk_api_version, const std::string & a_scene_name)
{
    const std::string glsl_relative_paths[] = {
        "reflection/trace_reflection_ray.rgen",
        "reflection/trace_reflection_ray.rmiss",
        "reflection/trace_reflection_ray_opaque.rahit",
        "reflection/trace_reflection_ray_opaque.rchit",
        "reflection/trace_reflection_ray_alpha.rahit",
        "reflection/trace_reflection_ray_alpha.rchit",
        "reflection/estimate_variance.comp",
        "reflection/gaussian_filter_variance.comp",
        "reflection/a_trous_filter.comp",
    };
    constexpr auto glsl_count = length_of_c_array(glsl_relative_paths);
    spirvs_ = std::make_unique<spirv_t[]>(glsl_count);
    return create_spirvs(a_vk_api_version, length_of_c_array(glsl_relative_paths), glsl_relative_paths, a_scene_name, spirvs_.get());
}


bool reflection_t::
refresh_spirvs()
{
    return reload_spirvs(spirvs_.get(), SPV_COUNT, "reflection: ");
}


void reflection_t::
create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, uint32_t a_shader_group_handle_size)
{
    shader_module_array_t rt_shader_module_array{ a_vk_dev, spirvs_.get() + SPV_RT_BEGIN, SPV_RT_COUNT };
    shader_group_handle_pool_ = shader_group_handle_pool_t(1, k_shader_group_idx_offset, a_shader_group_handle_size);
    pipelines_ = std::make_unique<pipeline_t[]>(PPL_COUNT);

    host_timer_t timer;
    const auto rt_shader_stage_create_infos = build_shader_stage_create_infos(rt_shader_module_array);

    VkRayTracingShaderGroupCreateInfoKHR shader_group_create_infos[] = {
    build_VkRayTracingShaderGroupCreateInfoKHR_general(SPV_RT_REFLECTION_RAY_RGEN),
    build_VkRayTracingShaderGroupCreateInfoKHR_general(SPV_RT_REFLECTION_RAY_RMISS),
    build_VkRayTracingShaderGroupCreateInfoKHR_hit(SPV_RT_REFLECTION_RAY_OPAQUE_RCHIT, SPV_RT_REFLECTION_RAY_OPAQUE_RAHIT),
    build_VkRayTracingShaderGroupCreateInfoKHR_hit(SPV_RT_REFLECTION_RAY_ALPHA_RCHIT, SPV_RT_REFLECTION_RAY_ALPHA_RAHIT),
    };
    VkRayTracingPipelineCreateInfoKHR rt_pipeline_create_info = build_VkRayTracingPipelineCreateInfoKHR(
        a_pipeline_layout,
        SPV_RT_COUNT, rt_shader_stage_create_infos.get(),
        length_of_c_array(shader_group_create_infos), shader_group_create_infos,
        1
    );
    create_raytracing_pipelines(
        a_vk_dev, 1, &rt_pipeline_create_info,
        shader_group_handle_pool_.byte_offsets.get(),
        pipelines_.get() + PPL_RT_TRACE_REFLECTION_RAY,
        shader_group_handle_pool_.data.get()
    );

    spock::info("reflection_t: create_raytracing_pipelines took " + std::to_string(timer.elapsed_ms()) + " ms");
    timer.start();

    shader_module_array_t comp_shader_module_array{ a_vk_dev, spirvs_.get() + SPV_COMP_BEGIN, SPV_COMP_COUNT };
    VkComputePipelineCreateInfo compute_pipeline_create_infos[SPV_COMP_COUNT];
    for (uint32_t i = 0; i < static_cast<uint32_t>(SPV_COMP_COUNT); ++i) {
        compute_pipeline_create_infos[i] = build_VkComputePipelineCreateInfo(
            build_VkPipelineShaderStageCreateInfo(
                comp_shader_module_array.vk_shader_stages()[i], comp_shader_module_array.vk_shader_modules()[i]
            ),
            a_pipeline_layout
        );
    }
    create_compute_pipelines(
        a_vk_dev, length_of_c_array(compute_pipeline_create_infos), compute_pipeline_create_infos, pipelines_.get() + PPL_COM_BEGIN
    );

    spock::info("reflection_t: create_compute_pipelines took " + std::to_string(timer.elapsed_ms()) + " ms");
}


uint32_t reflection_t::
get_sbt_bytes(uint32_t a_sbt_stride)
{
    return SGH_COUNT * a_sbt_stride;
}


void reflection_t::
write_sbt(const sbt_entry_size_t & a_sbt_entry_size, uint8_t ** a_sbt_host_addr, VkDeviceAddress * a_sbt_dev_addr)
{
    const auto host_begin = *a_sbt_host_addr;
    const auto dev_begin = *a_sbt_dev_addr;

    const uint32_t sbt_bytes = get_sbt_bytes(a_sbt_entry_size.stride);
    *a_sbt_host_addr += sbt_bytes;
    *a_sbt_dev_addr += sbt_bytes;

    for (size_t i = 0; i < static_cast<size_t>(SGH_COUNT); ++i) {
        uint8_t* dst = host_begin + i * a_sbt_entry_size.stride;
        const uint8_t* src = shader_group_handle_pool_.data.get() + i * a_sbt_entry_size.shader_group_handle_size;
        memcpy(dst, src, a_sbt_entry_size.shader_group_handle_size);
    }
    sb_set_ = {
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_TRACE_REFLECTION_RAY_RGEN) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride
        ),
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_TRACE_REFLECTION_RAY_RMISS) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride
        ),
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_TRACE_REFLECTION_RAY_HIT_BEGIN) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride * static_cast<uint64_t>(SGH_TRACE_REFLECTION_RAY_HIT_COUNT)
        ),
    };

}


void reflection_t::render_imgui_settings(cfg_t * a_cfg)
{
    SPOCK_PARANOID(a_cfg, "reflection_t::render_imgui_settings: a_cfg cannot be nullptr");

    if (ImGui::TreeNode("Reflection settings")) {
        ImGui::DragFloat("geometry_w", &a_cfg->geometry_weight_factor, 10.f, 10.f, 1000.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("geometry_weight_factor");
        }
        ImGui::DragFloat("accum_frame", &a_cfg->accum_frame_count_max, 2.f, 8.f, 64.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("accum_frame_count_max");
        }
        ImGui::DragFloat("accum_base", &a_cfg->accum_base_power, 0.1f, 0.1f, 1.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("accum_base_power");
        }
        ImGui::DragFloat("accum_curve", &a_cfg->accum_curve, 0.1f, 0.2f, 1.f);
        ImGui::DragFloat("w_threshold", &a_cfg->temporal_weight_threshold, 0.01f, 0.01f, 1.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("temporal_weight_threshold");
        }
        ImGui::TreePop();
    }
}


void reflection_t::
cmd_exec_reflection_pipelines(
    VkCommandBuffer a_cmd_buf, 
    VkPipelineLayout a_pipeline_layout,
    uint32_t a_ds_color_0_idx,
    uint32_t a_a_trous_iteration_count, 
    const glm::uvec2 & a_surface_extent, 
    const glm::uvec2 & a_estimate_variance_workgroup_size, 
    const glm::uvec2 & a_filter_variance_workgroup_size, 
    const glm::uvec2 & a_a_trous_filter_workgroup_size, 
    dev_timer_array_t * a_dev_timer_array
)
//TODO: use image barrier
{
    SPOCK_PARANOID(a_dev_timer_array, "deferred_t::cmd_exec_visibility_pipelines: a_dev_timer_array cannot be NULL.");

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_REFLECTION_RT);
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelines_[PPL_RT_TRACE_REFLECTION_RAY]);
    cmd_trace_rays(a_cmd_buf, sb_set_, to_uvec3(images_[IMG_TRACED_REFLECTION].extent()));

    VkMemoryBarrier barrier = build_VkMemoryBarrier(VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT); 
    vkCmdPipelineBarrier(a_cmd_buf, 
        VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 
        0, 1, &barrier, 0, nullptr, 0, nullptr
    );
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_REFLECTION_RT);

    const auto estimate_var_dim = calc_workgroup_count(a_surface_extent, a_estimate_variance_workgroup_size);
    const auto filter_var_dim = calc_workgroup_count(a_surface_extent, a_filter_variance_workgroup_size);
    const auto a_trous_dim = calc_workgroup_count(a_surface_extent, a_filter_variance_workgroup_size);

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_REFLECTION_TEMPO);
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipelines_[PPL_COM_ESTIMATE_VARIANCE]);
    vkCmdDispatch(a_cmd_buf, estimate_var_dim.x, estimate_var_dim.y, 1);
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_REFLECTION_TEMPO);

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_REFLECTION_A_TROUS);
    for (uint32_t i = 0; i < a_a_trous_iteration_count; ++i) {
        vkCmdPipelineBarrier(a_cmd_buf,
            VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            0, 1, &barrier, 0, nullptr, 0, nullptr
        );

        vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipelines_[PPL_COM_GAUSSIAN_FILTER_VARIANCE]);
        vkCmdDispatch(a_cmd_buf, filter_var_dim.x, filter_var_dim.y, 1);

        vkCmdPipelineBarrier(a_cmd_buf,
            VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            0, 1, &barrier, 0, nullptr, 0, nullptr
        );

        vkCmdPushConstants(
            a_cmd_buf, a_pipeline_layout, VK_SHADER_STAGE_COMPUTE_BIT,
            offsetof(push_constant_t, a_trous_iter), sizeof(push_constant_t::a_trous_iter), &i
        );
        vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipelines_[PPL_COM_A_TROUS_FILTER]);
        vkCmdDispatch(a_cmd_buf, a_trous_dim.x, a_trous_dim.y, 1);

        std::swap(descriptor_sets_[REFLECTION_DS_COLOR_0], descriptor_sets_[REFLECTION_DS_COLOR_1]);
        const VkDescriptorSet desc_sets[] = { descriptor_sets_[REFLECTION_DS_COLOR_0], descriptor_sets_[REFLECTION_DS_COLOR_1] };
        vkCmdBindDescriptorSets(
            a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, a_pipeline_layout, 
            a_ds_color_0_idx, length_of_c_array(desc_sets), desc_sets, 0, nullptr
        );
    }
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_REFLECTION_A_TROUS);

    if (a_a_trous_iteration_count & 1u) {
        const VkDescriptorSet desc_sets[] = { descriptor_sets_[REFLECTION_DS_COLOR_0], descriptor_sets_[REFLECTION_DS_COLOR_1] };
        vkCmdBindDescriptorSets(
            a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, a_pipeline_layout,  // need to access the right one in shading pass
            a_ds_color_0_idx, length_of_c_array(desc_sets), desc_sets, 0, nullptr
        );
    }
    
}


void reflection_t::
create_descriptor_set_layout(VkDevice a_vk_dev)
{
    const VkShaderStageFlags img_traced_reflection_flags = VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    VkDescriptorSetLayoutBinding one_off_bindings[] = {
        build_VkDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, img_traced_reflection_flags),       // IMG_TRACED_REFLECTION
        build_VkDescriptorSetLayoutBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, VK_SHADER_STAGE_COMPUTE_BIT),   // IMG_VARIANCE
        build_VkDescriptorSetLayoutBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, VK_SHADER_STAGE_COMPUTE_BIT),   // IMG_FILTERED_VARIANCE
    };
    descriptor_set_layouts_[REFLECTION_DS_LAYOUT_ONE_OFF] = descriptor_set_layout_t(
        a_vk_dev,
        build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(one_off_bindings), one_off_bindings)
    );

    VkDescriptorSetLayoutBinding tempo_bindings[] = {
        build_VkDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, VK_SHADER_STAGE_COMPUTE_BIT),   // IMG_2ND_MOMENT_DURATION
    };
    descriptor_set_layouts_[REFLECTION_DS_LAYOUT_TEMPO] = descriptor_set_layout_t(
        a_vk_dev,
        build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(tempo_bindings), tempo_bindings)
    );

    const VkShaderStageFlags img_1st_moment_flags = VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    VkDescriptorSetLayoutBinding reflection_1st_moment_bindings[] = {
        build_VkDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, img_1st_moment_flags),   // IMG_1ST_MOMENT
    };
    descriptor_set_layouts_[REFLECTION_DS_LAYOUT_COLOR] = descriptor_set_layout_t(
        a_vk_dev,
        build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(reflection_1st_moment_bindings), reflection_1st_moment_bindings)
    );

    const VkDescriptorPoolSize pool_sizes[] = { 
        build_VkDescriptorPoolSize(VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, IMG_COUNT) 
    };
    auto pool_create_info = build_VkDescriptorPoolCreateInfo(REFLECTION_DS_COUNT, length_of_c_array(pool_sizes), pool_sizes);

    const VkDescriptorSetLayout layouts[] = {
        descriptor_set_layouts_[REFLECTION_DS_LAYOUT_ONE_OFF],
        descriptor_set_layouts_[REFLECTION_DS_LAYOUT_TEMPO],
        descriptor_set_layouts_[REFLECTION_DS_LAYOUT_TEMPO],
        descriptor_set_layouts_[REFLECTION_DS_LAYOUT_COLOR],
        descriptor_set_layouts_[REFLECTION_DS_LAYOUT_COLOR],
    };

    SPOCK_PARANOID(
        length_of_c_array(layouts) == static_cast<size_t>(REFLECTION_DS_COUNT), 
        "reflection_t::create_descriptor_set_layouts: mismatch between descriptor set count and descriptor set layout count"
    );

    descriptor_pool_ = descriptor_pool_t(a_vk_dev, pool_create_info);
    descriptor_pool_.alloc_descriptor_sets(REFLECTION_DS_COUNT, layouts, descriptor_sets_.data());
}

