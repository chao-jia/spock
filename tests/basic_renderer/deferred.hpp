#pragma once

#include "misc.hpp"

#include <spock/context.hpp>
#include <spock/scene.hpp>
#include <array>

using namespace spock;

class deferred_t {
private:
    std::unique_ptr<spirv_t[]> spirvs_;
    std::unique_ptr<image_t[]> images_;
    std::unique_ptr<VkFormat[]> image_formats_;
    std::unique_ptr<image_view_t[]> image_views_;

    descriptor_pool_t descriptor_pool_;
    std::array<descriptor_set_layout_t, DEFERRED_DS_LAYOUT_COUNT> descriptor_set_layouts_;

    render_pass_t visibility_pass_;
    render_pass_t write_g_pass_;
    render_pass_t shading_pass_;

    std::array<VkDescriptorSet, DEFERRED_DS_COUNT> descriptor_sets_;
    std::array<framebuffer_t, 2> visibility_framebuffers_;
    std::array<framebuffer_t, 2> write_g_framebuffers_;
    std::unique_ptr<framebuffer_t[]> shading_framebuffers_;

    std::unique_ptr<pipeline_t[]> pipelines_;

    void create_descriptor_set_layouts(VkDevice a_vk_dev);

public:
    deferred_t() : descriptor_sets_{} {}
    deferred_t(VkDevice a_vk_dev, VkFormat a_depth_format);
    void create_render_passes(
        VkDevice a_vk_dev,
        VkFormat a_surface_format,
        VkImageLayout a_surface_image_final_layout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    );
    bool init_spirvs(uint32_t a_vk_api_version, const std::string& a_scene_name);
    bool refresh_spirvs();
    void create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, const VkSpecializationInfo* a_ddgi_spec_const);
    void create_framebuffers(
        const context_t& a_context, 
        const VkExtent2D& a_surface_extent, 
        uint32_t a_surface_image_count, 
        const VkImageView* a_surface_image_views,
        VkCommandBuffer a_cmd_buf, 
        VkQueue a_queue
    );
    void write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler);
    // bind descriptor sets, set depth compare op, viewport, scissor before begin render pass
    void cmd_begin_visibility_pass(VkCommandBuffer a_cmd_buf, const VkRect2D& a_render_area, float a_ndc_far, uint32_t a_is_odd_frame);
    void cmd_exec_visibility_pipelines(VkCommandBuffer a_cmd_buf, const scene_t& a_scene, dev_timer_array_t* a_dev_timer_array);
    void cmd_begin_write_g_pass(VkCommandBuffer a_cmd_buf, const VkRect2D& a_render_area, uint32_t a_is_odd_frame);
    void cmd_exec_wirte_g_pipeline(VkCommandBuffer a_cmd_buf);
    void cmd_begin_shading_pass(VkCommandBuffer a_cmd_buf, const VkRect2D& a_render_area, float a_ndc_far, uint32_t a_surface_image_idx);
    void cmd_exec_shading_pipeline(VkCommandBuffer a_cmd_buf);

    const descriptor_set_layout_t* descriptor_set_layouts() const { return descriptor_set_layouts_.data(); }
    const VkDescriptorSet* descriptor_sets() const { return descriptor_sets_.data(); }

    VkRenderPass shading_pass() const { return shading_pass_; }
    

};


