#include "ddgi.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/integer.hpp>
#include <glm/gtc/random.hpp>
#include <glm/ext.hpp>

#include <spock/generated.hpp>
#include <spock/utils.hpp>
#include <nlohmann/json.hpp>


namespace {

enum spv_e {
    SPV_GEN_SURFEL_RGEN = 0,
    SPV_GEN_SURFEL_RMISS,
    SPV_GEN_SURFEL_OPAQUE_RAHIT,
    SPV_GEN_SURFEL_OPAQUE_RCHIT,
    SPV_GEN_SURFEL_ALPHA_RAHIT,
    SPV_GEN_SURFEL_ALPHA_RCHIT,
    SPV_SHADE_SURFEL_RGEN,
    SPV_SHADE_SURFEL_RMISS,
    SPV_SHADE_SURFEL_OPAQUE_RAHIT,
    SPV_SHADE_SURFEL_ALPHA_RAHIT,
    SPV_UPDATE_DEPTH_COMP,
    SPV_UPDATE_IRRADIANCE_COMP,
    SPV_VISUALIZE_PROBE_VERT,
    SPV_VISUALIZE_PROBE_FRAG,
    SPV_COUNT,
    SPV_GEN_SURFEL_BEGIN = SPV_GEN_SURFEL_RGEN,
    SPV_GEN_SURFEL_COUNT = SPV_GEN_SURFEL_ALPHA_RCHIT + 1 - SPV_GEN_SURFEL_BEGIN,
    SPV_SHADE_SURFEL_BEGIN = SPV_SHADE_SURFEL_RGEN,
    SPV_SHADE_SURFEL_COUNT = SPV_SHADE_SURFEL_ALPHA_RAHIT + 1 - SPV_SHADE_SURFEL_BEGIN,
    SPV_RT_BEGIN = SPV_GEN_SURFEL_RGEN,
    SPV_RT_COUNT = SPV_GEN_SURFEL_COUNT + SPV_SHADE_SURFEL_COUNT,
    SPV_VISUALIZE_PROBE_COUNT = SPV_VISUALIZE_PROBE_FRAG + 1 - SPV_VISUALIZE_PROBE_VERT
};

enum shader_group_handle_e {
    SGH_GEN_SURFEL_RGEN = 0,
    SGH_GEN_SURFEL_RMISS,
    SGH_GEN_SURFEL_HIT_OPAQUE,
    SGH_GEN_SURFEL_HIT_ALPHA,
    SGH_SHADE_SURFEL_RGEN,
    SGH_SHADE_SURFEL_RMISS,
    SGH_SHADE_SURFEL_HIT_OPAQUE,
    SGH_SHADE_SURFEL_HIT_ALPHA,
    SGH_COUNT,
    SGH_GEN_SURFEL_OFFSET = SGH_GEN_SURFEL_RGEN,
    SGH_GEN_SURFEL_COUNT = SGH_GEN_SURFEL_HIT_ALPHA + 1 - SGH_GEN_SURFEL_OFFSET,
    SGH_GEN_SURFEL_HIT_BEGIN = SGH_GEN_SURFEL_HIT_OPAQUE,
    SGH_GEN_SURFEL_HIT_COUNT = SGH_GEN_SURFEL_HIT_ALPHA + 1 - SGH_GEN_SURFEL_HIT_BEGIN,
    SGH_SHADE_SURFEL_OFFSET = SGH_SHADE_SURFEL_RGEN,
    SGH_SHADE_SURFEL_COUNT = SGH_SHADE_SURFEL_HIT_ALPHA + 1 - SGH_SHADE_SURFEL_OFFSET,
    SGH_SHADE_SURFEL_HIT_BEGIN = SGH_SHADE_SURFEL_HIT_OPAQUE,
    SGH_SHADE_SURFEL_HIT_COUNT = SGH_SHADE_SURFEL_HIT_ALPHA + 1 - SGH_SHADE_SURFEL_HIT_BEGIN,
};


constexpr static uint32_t k_shader_group_idx_offset[] = {
    SGH_GEN_SURFEL_OFFSET,
    SGH_SHADE_SURFEL_OFFSET,
    SGH_COUNT
};

enum ppl_e {
    PPL_RT_GEN_SURFEL = 0,
    PPL_RT_SHADE_SURFEL,
    PPL_COM_UPDATE_DEPTH,
    PPL_COM_UPDATE_IRRADIANCE,
    PPL_GRA_VIS_PROBE,
    PPL_COUNT,
    PPL_RT_BEGIN = PPL_RT_GEN_SURFEL,
};

enum sb_set_e {
    SB_SET_GEN_SURFEL = 0,
    SB_SET_SHADE_SURFEL,
    SB_SET_COUNT,
};

enum img_e {
    IMG_SURFEL,
    IMG_DEPTH,
    IMG_IRRADIANCE,
    IMG_COUNT
};

constexpr VkFormat k_img_formats[] = {
    VK_FORMAT_R32G32B32A32_SFLOAT, //surfel: I: (ray dist, bary.y, bary.z, triangle_idx); O: (r, gb, oct_dir, ray dist), rgba32f
    VK_FORMAT_R16G16_SFLOAT, // (depth, depth^2); rg16f
    VK_FORMAT_R16G16B16A16_SFLOAT, // irradiance
};


}


ddgi_t::cfg_t::
cfg_t(const ordered_json & a_json, const std::string& a_scene_name)
{
    SPOCK_ENSURE(a_json.contains("depth_sharpness"), "[ddgi] depth_sharpness not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("energy_preservation"), "[ddgi] energy_preservation not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("probe_intensity"), "[ddgi] probe_intensity not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("hysteresis"), "[ddgi] hysteresis not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("normal_bias"), "[ddgi] normal_bias not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("probe_grid_dim"), "[ddgi] probe_grid_dim not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("probe_grid_offset"), "[ddgi] probe_grid_offset not found for scene " + a_scene_name);
    SPOCK_ENSURE(a_json.contains("probe_surfel_resolution"), "[ddgi] probe_surfel_resolution not found for scene " + a_scene_name);

    const auto probe_grid_dim_v = a_json["probe_grid_dim"].get<std::vector<uint32_t>>();
    const auto probe_grid_offset_v = a_json["probe_grid_offset"].get<std::vector<float>>();
    const auto probe_surfel_resolution_v = a_json["probe_surfel_resolution"].get<std::vector<uint32_t>>();
    SPOCK_ENSURE(probe_grid_dim_v.size() == 3, "config.scenes[" + a_scene_name + "][probe_grid_dim] is invalid");
    SPOCK_ENSURE(probe_grid_offset_v.size() == 3, "config.scenes[" + a_scene_name + "][probe_grid_offset] is invalid");
    SPOCK_ENSURE(probe_surfel_resolution_v.size() == 2, "config.scenes[" + a_scene_name + "][probe_surfel_resolution] is invalid");

    depth_sharpness = a_json["depth_sharpness"].get<float>();
    energy_preservation = a_json["energy_preservation"].get<float>();
    probe_intensity = a_json["probe_intensity"].get<float>();
    hysteresis = a_json["hysteresis"].get<float>();
    normal_bias = a_json["normal_bias"].get<float>();
    probe_grid_dim = glm::make_vec3(probe_grid_dim_v.data());
    probe_grid_offset = glm::make_vec3(probe_grid_offset_v.data());
    probe_surfel_resolution = glm::make_vec2(probe_surfel_resolution_v.data());
}


ddgi_t::
ddgi_t() 
    : probe_grid_dim_{}
    , probe_grid_lower_{}
    , probe_cell_size_{}
    , probe_surfel_resolution_{}
    , probe_depth_resolution_{}
    , probe_irradiance_resolution_{}
    , depth_update_compute_local_size_{}
    , irradiance_update_compute_local_size_{}
    , per_probe_ray_count_{}
    , probe_count_per_image_{}
    , rotation_{ glm::identity<glm::mat3>() }
    , descriptor_set_{ VK_NULL_HANDLE }
{}


ddgi_t::
ddgi_t(VkDevice a_vk_dev)
    : probe_grid_dim_{}
    , probe_grid_lower_{}
    , probe_cell_size_{}
    , probe_surfel_resolution_{}
    , probe_depth_resolution_{}
    , probe_irradiance_resolution_{}
    , depth_update_compute_local_size_{}
    , irradiance_update_compute_local_size_{}
    , per_probe_ray_count_{}
    , probe_count_per_image_{}
    , rotation_{ glm::identity<glm::mat3>() }
{
    create_descriptor_set_layout(a_vk_dev);
}

void ddgi_t::
init_params
(
    const glm::uvec3& a_probe_dim,
    const glm::vec3& a_scene_aabb_size,
    const glm::vec3& a_scene_lower,
    const glm::uvec2& a_probe_surfel_resolution, 
    const glm::uvec2& a_depth_update_compute_local_size,
    const glm::uvec2& a_irradiance_update_compute_local_size,
    uint32_t a_probe_depth_resolution,
    uint32_t a_probe_irradiance_resolution
)
{

    probe_grid_dim_ = a_probe_dim;
    probe_surfel_resolution_ = a_probe_surfel_resolution;
    probe_depth_resolution_ = a_probe_depth_resolution;
    probe_irradiance_resolution_ = a_probe_irradiance_resolution;
    depth_update_compute_local_size_ = a_depth_update_compute_local_size;
    irradiance_update_compute_local_size_ = a_irradiance_update_compute_local_size;
    per_probe_ray_count_ = probe_surfel_resolution_.x * probe_surfel_resolution_.y;
    rotation_ = glm::identity<glm::mat3>();
    random_generators_.reset(new std::mt19937[RANDOM_VAR_COUNT]{ std::mt19937{ 3 }, std::mt19937{ 5 } });
    uniform_fpdfs_.reset(new uniform_fpdf_t[RANDOM_VAR_COUNT] { uniform_fpdf_t{0.f, 1.f}, uniform_fpdf_t{0.f, 1.f} });

    SPOCK_PARANOID(
        std::all_of(&probe_grid_dim_[0], &probe_grid_dim_[0] + 3, [](uint32_t d) { return is_power_of_two(d); }), 
        "ddgi_t::ctor: light probe grid dimension must be power of 2"
    );
    SPOCK_PARANOID(glm::all(glm::greaterThan(a_scene_aabb_size, glm::vec3(0.f))), "ddgi_t::calc_probe_grid_dim: a_scene_aabb_size must be positive");
    SPOCK_PARANOID(is_power_of_two(probe_surfel_resolution_.x), "ddgi_t::ctor: probe_surfel_resolution_.x must be power of 2");
    SPOCK_PARANOID(is_power_of_two(probe_surfel_resolution_.y), "ddgi_t::ctor: probe_surfel_resolution_.y must be power of 2");
    SPOCK_PARANOID(is_power_of_two(probe_irradiance_resolution_), "ddgi_t::ctor: probe irradiance resolution must be power of 2");
    SPOCK_PARANOID(is_power_of_two(probe_depth_resolution_), "ddgi_t::ctor: probe distance resolution must be power of 2");
    SPOCK_PARANOID(
        depth_update_compute_local_size_.x * depth_update_compute_local_size_.y >= probe_depth_resolution_ * probe_depth_resolution_, 
        "ddgi_t::ctor: depth_update_compute_local_size_ is too small"
    );
    SPOCK_PARANOID(
        irradiance_update_compute_local_size_.x * irradiance_update_compute_local_size_.y >= probe_irradiance_resolution_ * probe_irradiance_resolution_, 
        "ddgi_t::ctor: irradiance_update_compute_local_size_ is too small"
    );

    probe_cell_size_ = a_scene_aabb_size / glm::vec3(probe_grid_dim_);
    probe_grid_lower_ = a_scene_lower + 0.5f * probe_cell_size_;

    uint32_t probe_count = probe_grid_dim_.x * probe_grid_dim_.y * probe_grid_dim_.z;
    uint32_t log2_probe_count = ctz(probe_count); // probe_count is power of two
    uint32_t log2_probe_count_per_image_row = log2_probe_count >> 1;
    probe_count_per_image_.x = 1u << (log2_probe_count_per_image_row);
    probe_count_per_image_.y = probe_count >> log2_probe_count_per_image_row;

    const auto surfel_image_extent = probe_count_per_image_ * probe_surfel_resolution_;
    image_extents_ = std::make_unique<glm::uvec2[]>(IMG_COUNT);
    image_extents_[IMG_SURFEL] = surfel_image_extent;
    image_extents_[IMG_DEPTH] = probe_count_per_image_ * probe_depth_resolution_;
    image_extents_[IMG_IRRADIANCE] = probe_count_per_image_ * probe_irradiance_resolution_;

    const uint32_t probe_depth_padded_res = probe_depth_resolution_ + 2;
    const uint32_t probe_irradiance_padded_res = probe_irradiance_resolution_ + 2;
    padded_image_extents_ = std::make_unique<glm::uvec2[]>(IMG_COUNT);
    padded_image_extents_[IMG_SURFEL] = surfel_image_extent;
    padded_image_extents_[IMG_DEPTH] = probe_count_per_image_ * probe_depth_padded_res;
    padded_image_extents_[IMG_IRRADIANCE] = probe_count_per_image_ * probe_irradiance_padded_res;

    per_probe_ray_dirs_ = std::make_unique<glm::vec3[]>(per_probe_ray_count_);
    spock::fibonacci_sphere(per_probe_ray_count_, per_probe_ray_dirs_.get());
    perturbed_per_probe_ray_dirs_ = std::make_unique<glm::vec4[]>(per_probe_ray_count_);
    std::transform(
        per_probe_ray_dirs_.get(), per_probe_ray_dirs_.get() + per_probe_ray_count_, 
        perturbed_per_probe_ray_dirs_.get(), [](const glm::vec3& v) -> glm::vec4 { return glm::vec4(v, 0.f); }
    );
}


void ddgi_t::
create_images(const context_t& a_context)
{
    constexpr VkImageUsageFlags usage_flags[]{
        VK_IMAGE_USAGE_STORAGE_BIT, // surfel
        VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, // depth
        VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, // irradiance 
    };

    images_ = std::make_unique<image_t[]>(IMG_COUNT);
    image_views_ = std::make_unique<image_view_t[]>(IMG_COUNT);
    for (size_t i = 0; i < static_cast<size_t>(IMG_COUNT); ++i) {
        const auto extent = to_extent_3d(glm::uvec3(padded_image_extents_[i], 1));
        auto img = image_t(a_context, build_VkImageCreateInfo(k_img_formats[i], extent, usage_flags[i]));
        image_views_[i] = image_view_t(a_context, build_VkImageViewCreateInfo(img));
        images_[i] = std::move(img);
    }
}


void ddgi_t::
reset_images(const context_t& a_context, VkCommandBuffer a_cmd_buf, VkQueue a_queue)
{
    const VkImageLayout inital_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    const VkImageLayout final_layout = VK_IMAGE_LAYOUT_GENERAL;

    VkImageMemoryBarrier image_barriers[IMG_COUNT];
    for (size_t i = 0; i < static_cast<size_t>(IMG_COUNT); ++i) {
        image_barriers[i] = build_VkImageMemoryBarrier(images_[i], inital_layout, final_layout, 0, VK_ACCESS_TRANSFER_WRITE_BIT);
    }
    auto cmd_buf_begin_info = build_VkCommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    dev_timer_array_t dev_timer(a_context, true, 1, 1, 1);
    dev_timer.begin();
    host_timer_t timer;

    SPOCK_VK_CALL(vkBeginCommandBuffer(a_cmd_buf, &cmd_buf_begin_info));

    dev_timer.cmd_start(a_cmd_buf, 0);
    vkCmdPipelineBarrier(a_cmd_buf,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0,
        0, nullptr, 0, nullptr, IMG_COUNT, image_barriers
    );
    const auto range = build_VkImageSubresourceRange();
    VkClearColorValue value{};
    vkCmdClearColorImage(a_cmd_buf, images_[IMG_DEPTH], final_layout, &value, 1, &range); //TODO: check depth initial value
    vkCmdClearColorImage(a_cmd_buf, images_[IMG_IRRADIANCE], final_layout, &value, 1, &range);
    dev_timer.cmd_stop(a_cmd_buf, 0);

    SPOCK_VK_CALL(vkEndCommandBuffer(a_cmd_buf));
    const auto submit_info = build_VkSubmitInfo(1, &a_cmd_buf);
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));

    const float img_barrier_cpu_ms = timer.elapsed_ms();
    dev_timer.end();

    spock::info("ddgi layout transition + clear took host: " + std::to_string(img_barrier_cpu_ms) + " ms; dev: " + std::to_string(dev_timer.elapsed_ms(0)) + " ms");

}


void ddgi_t::
perturbe_per_probe_ray_dirs()
{
    const auto r0 = random_next(0);
    const auto r1 = random_next(1);

    // spherical coordinate system: https://en.wikipedia.org/wiki/Spherical_coordinate_system#/media/File:3D_Spherical.svg
    // theta in (0, pi): polar angle  (angle between normal and +z)
    // phi unbounded: azimuthal angle (angle between normal and +x)
    // Global Illumination Compendium: formula 33), p. 19

    const auto phi = 2.f * glm::pi<float>() * r0;
    const auto cos_theta = 1.f - 2.f * r1;
    const auto disk_radius = 2.f * std::sqrt(r1 * (1 - r1));
    constexpr glm::vec3 v0{ 1.f, 0.f, 0.f };
    const glm::vec3 v1{ std::cos(phi) * disk_radius, std::sin(phi) * disk_radius, 1.f - 2.f * r1 };
    auto axis = glm::vec3(1.f, 0.f, 0.f);
    float cos_angle = glm::dot(v0, v1);
    float radians = 0.f;
    if ((1.f - std::abs(cos_angle)) <= std::numeric_limits<float>::epsilon()) {
        radians = glm::pi<float>() * (cos_angle > 0.f);
    }
    else {
        axis = glm::normalize(glm::cross(v0, v1));
        radians = 2.f * std::atan2(glm::length(v1 - v0), glm::length(v1 + v0)); // angle between 3D vectors https://math.stackexchange.com/a/1782769/217767
    }
    rotation_ = rotation_ * glm::mat3(glm::rotate(glm::identity<glm::mat4>(), radians, axis));
    //const auto rotation = glm::mat3(
    //    glm::rotate(
    //        glm::identity<glm::mat4>(), 
    //        glm::pi<float>() * 2.f * r0, 
    //        glm::sphericalRand(1.f)
    //    ));
    for (uint32_t i = 0; i < per_probe_ray_count_; ++i) {
        const glm::vec3& fibonacci_ray_dir = per_probe_ray_dirs_[i];
        perturbed_per_probe_ray_dirs_[i] = glm::vec4(rotation_ * fibonacci_ray_dir, 0.f);
    }
}



ddgi_t::specialization_constant_t ddgi_t::
get_ray_tracing_specialization_constant() const
{
    specialization_constant_t k = get_specialization_constant();
    k.info_ = build_VkSpecializationInfo(
        length_of_c_array(specialization_constant_t::entries) - 2,
        specialization_constant_t::entries,
        sizeof(specialization_constant_t::data_t) - 2 * sizeof(uint32_t),
        k.data_.get()
    );
    return k;
}


ddgi_t::specialization_constant_t ddgi_t::
get_depth_update_specialization_constant() const
{
    specialization_constant_t k = get_specialization_constant();
    k.data_->compute_local_size_x = depth_update_compute_local_size_.x;
    k.data_->compute_local_size_y = depth_update_compute_local_size_.y;

    k.info_ = build_VkSpecializationInfo(
        length_of_c_array(specialization_constant_t::entries),
        specialization_constant_t::entries,
        sizeof(specialization_constant_t::data_t),
        k.data_.get()
    );
    return k;
}


ddgi_t::specialization_constant_t ddgi_t::
get_irradiance_update_specialization_constant() const
{
    specialization_constant_t k = get_specialization_constant();
    k.data_->compute_local_size_x = irradiance_update_compute_local_size_.x;
    k.data_->compute_local_size_y = irradiance_update_compute_local_size_.y;

    k.info_ = build_VkSpecializationInfo(
        length_of_c_array(specialization_constant_t::entries),
        specialization_constant_t::entries,
        sizeof(specialization_constant_t::data_t),
        k.data_.get()
    );
    return k;
}


void ddgi_t::
write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler)
{
    VkDescriptorImageInfo image_infos[IMG_COUNT];
    VkWriteDescriptorSet writes[IMG_COUNT];
    for (uint32_t i = 0; i < static_cast<uint32_t>(IMG_COUNT); ++i) {
        image_infos[i] = build_VkDescriptorImageInfo(a_dummy_sampler, image_views_[i], VK_IMAGE_LAYOUT_GENERAL);
        writes[i] = build_VkWriteDescriptorSet(descriptor_set_, i, 1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, image_infos + i);
    }
    vkUpdateDescriptorSets(a_vk_dev, IMG_COUNT, writes, 0, nullptr);
}


bool ddgi_t::
init_spirvs(uint32_t a_vk_api_version, const std::string & a_scene_name)
{
    const std::string glsl_relative_paths[] = {
        "ddgi/gen_surfel.rgen",
        "ddgi/gen_surfel.rmiss",
        "ddgi/gen_surfel_opaque.rahit",
        "ddgi/gen_surfel_opaque.rchit",
        "ddgi/gen_surfel_alpha.rahit",
        "ddgi/gen_surfel_alpha.rchit",
        "ddgi/shade_surfel.rgen",
        "ddgi/shade_surfel.rmiss",
        "ddgi/shade_surfel_opaque.rahit",
        "ddgi/shade_surfel_alpha.rahit",
        "ddgi/update_depth.comp",
        "ddgi/update_irradiance.comp",
        "ddgi/visualize_probe.vert",
        "ddgi/visualize_probe.frag",
    };
    constexpr auto glsl_count = length_of_c_array(glsl_relative_paths);
    SPOCK_PARANOID(glsl_count == static_cast<uint32_t>(SPV_COUNT), "mismatch between glsl count and spv count");

    spirvs_ = std::make_unique<spirv_t[]>(glsl_count);
    return create_spirvs(a_vk_api_version, glsl_count, glsl_relative_paths, a_scene_name, spirvs_.get());
}


bool ddgi_t::
refresh_spirvs()
{
    return reload_spirvs(spirvs_.get(), SPV_COUNT, "ddgi: ");
}


void ddgi_t::
create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, uint32_t a_shader_group_handle_size, VkRenderPass a_shading_pass)
{
    shader_module_array_t rt_shader_module_array{ a_vk_dev, spirvs_.get() + SPV_RT_BEGIN, SPV_RT_COUNT };
    shader_group_handle_pool_ = shader_group_handle_pool_t(SB_SET_COUNT, k_shader_group_idx_offset, a_shader_group_handle_size);

    pipelines_ = std::make_unique<pipeline_t[]>(PPL_COUNT);

    host_timer_t timer;

    const auto rt_specialization = get_ray_tracing_specialization_constant();
    const auto rt_shader_stage_create_infos = build_shader_stage_create_infos(rt_shader_module_array, &rt_specialization.info());

    VkRayTracingShaderGroupCreateInfoKHR gen_surfel_shader_group_create_infos[] = {
        build_VkRayTracingShaderGroupCreateInfoKHR_general(SPV_GEN_SURFEL_RGEN),
        build_VkRayTracingShaderGroupCreateInfoKHR_general(SPV_GEN_SURFEL_RMISS),
        build_VkRayTracingShaderGroupCreateInfoKHR_hit(SPV_GEN_SURFEL_OPAQUE_RCHIT, SPV_GEN_SURFEL_OPAQUE_RAHIT),
        build_VkRayTracingShaderGroupCreateInfoKHR_hit(SPV_GEN_SURFEL_ALPHA_RCHIT, SPV_GEN_SURFEL_ALPHA_RAHIT),
    };
    VkRayTracingShaderGroupCreateInfoKHR shade_surfel_shader_group_create_infos[] = {
        build_VkRayTracingShaderGroupCreateInfoKHR_general(SPV_SHADE_SURFEL_RGEN - SPV_SHADE_SURFEL_BEGIN),
        build_VkRayTracingShaderGroupCreateInfoKHR_general(SPV_SHADE_SURFEL_RMISS - SPV_SHADE_SURFEL_BEGIN),
        build_VkRayTracingShaderGroupCreateInfoKHR_hit(VK_SHADER_UNUSED_KHR, SPV_SHADE_SURFEL_OPAQUE_RAHIT - SPV_SHADE_SURFEL_BEGIN),
        build_VkRayTracingShaderGroupCreateInfoKHR_hit(VK_SHADER_UNUSED_KHR, SPV_SHADE_SURFEL_ALPHA_RAHIT - SPV_SHADE_SURFEL_BEGIN),
    };
    VkRayTracingPipelineCreateInfoKHR rt_pipeline_create_infos[] = {
        build_VkRayTracingPipelineCreateInfoKHR(
            a_pipeline_layout,
            SPV_GEN_SURFEL_COUNT, rt_shader_stage_create_infos.get() + SPV_GEN_SURFEL_BEGIN,
            length_of_c_array(gen_surfel_shader_group_create_infos), gen_surfel_shader_group_create_infos,
            1
        ),
        build_VkRayTracingPipelineCreateInfoKHR(
            a_pipeline_layout,
            SPV_SHADE_SURFEL_COUNT, rt_shader_stage_create_infos.get() + SPV_SHADE_SURFEL_BEGIN,
            length_of_c_array(shade_surfel_shader_group_create_infos), shade_surfel_shader_group_create_infos,
            1
        ),
    };
    create_raytracing_pipelines(
        a_vk_dev, length_of_c_array(rt_pipeline_create_infos), rt_pipeline_create_infos,
        shader_group_handle_pool_.byte_offsets.get(),
        pipelines_.get() + PPL_RT_BEGIN,
        shader_group_handle_pool_.data.get()
    );
    spock::info("creating ddgi_rt_pipelines_ took " + std::to_string(timer.elapsed_ms()) + " ms");
    timer.start();

    const auto depth_update_specialization = get_depth_update_specialization_constant();
    const auto irradiance_update_specialization = get_irradiance_update_specialization_constant();

    shader_module_t depth_update_shader_module{
        a_vk_dev,
        spirvs_[SPV_UPDATE_DEPTH_COMP].shader_stage_flag_bits(),
        spirvs_[SPV_UPDATE_DEPTH_COMP].spirv_bin()
    };
    shader_module_t irradiance_update_shader_module{
        a_vk_dev,
        spirvs_[SPV_UPDATE_IRRADIANCE_COMP].shader_stage_flag_bits(),
        spirvs_[SPV_UPDATE_IRRADIANCE_COMP].spirv_bin()
    };

    VkComputePipelineCreateInfo compute_pipeline_create_infos[] = {
        // ddgi_depth_update
        build_VkComputePipelineCreateInfo(
            build_VkPipelineShaderStageCreateInfo(
                depth_update_shader_module.shader_stage(), depth_update_shader_module, &depth_update_specialization.info()
            ),
            a_pipeline_layout
        ),
        // ddgi_irradiance_update
        build_VkComputePipelineCreateInfo(
            build_VkPipelineShaderStageCreateInfo(
                irradiance_update_shader_module.shader_stage(), irradiance_update_shader_module, &irradiance_update_specialization.info()
            ),
            a_pipeline_layout
        )
    };
    create_compute_pipelines(
        a_vk_dev, length_of_c_array(compute_pipeline_create_infos), compute_pipeline_create_infos, pipelines_.get() + PPL_COM_UPDATE_DEPTH
    );
    spock::info("creating ddgi_compute_pipelines_ took " + std::to_string(timer.elapsed_ms()) + " ms");

    // SPIRV_DDGI_PROBE_VIS_VERT, SPIRV_DDGI_PROBE_VIS_FRAG
    shader_module_array_t probe_vis_shader_module_array{ a_vk_dev, spirvs_.get() + SPV_VISUALIZE_PROBE_VERT, SPV_VISUALIZE_PROBE_COUNT };
    const auto probe_vis_shader_stage_create_infos = build_shader_stage_create_infos(probe_vis_shader_module_array, &rt_specialization.info());

    graphics_pipeline_create_info_helper_t helper{};

    helper.input_assembly_state_create_info.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    helper.multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    helper.dynamic_states.push_back(VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT);

    const auto pipeline_create_info = helper.resolve(
        a_vk_dev, a_shading_pass, 0, a_pipeline_layout, SPV_VISUALIZE_PROBE_COUNT, probe_vis_shader_stage_create_infos.get()
    );

    spock::create_graphics_pipelines(a_vk_dev, 1, &pipeline_create_info, pipelines_.get() + PPL_GRA_VIS_PROBE);

    spock::info("creating ddgi_probe_vis_pipeline_ took " + std::to_string(timer.elapsed_ms()) + " ms");
}


uint32_t ddgi_t::
get_sbt_bytes(uint32_t a_sbt_stride)
{
    return SGH_COUNT * a_sbt_stride;
}


void ddgi_t::
write_sbt(const sbt_entry_size_t& a_sbt_entry_size, uint8_t** a_sbt_host_addr, VkDeviceAddress* a_sbt_dev_addr)
{
    sb_sets_ = std::make_unique<shader_binding_set_t[]>(SB_SET_COUNT);
    const auto host_begin = *a_sbt_host_addr;
    const auto dev_begin = *a_sbt_dev_addr;

    const uint32_t sbt_bytes = get_sbt_bytes(a_sbt_entry_size.stride);
    *a_sbt_host_addr += sbt_bytes;
    *a_sbt_dev_addr += sbt_bytes;

    for (size_t i = 0; i < static_cast<size_t>(SGH_COUNT); ++i) {
        uint8_t* dst = host_begin + i * a_sbt_entry_size.stride;
        const uint8_t* src = shader_group_handle_pool_.data.get() + i * a_sbt_entry_size.shader_group_handle_size;
        memcpy(dst, src, a_sbt_entry_size.shader_group_handle_size);
    }
    sb_sets_[SB_SET_GEN_SURFEL] = {
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_GEN_SURFEL_RGEN) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride
        ),
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_GEN_SURFEL_RMISS) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride
        ),
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_GEN_SURFEL_HIT_BEGIN) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride * static_cast<uint64_t>(SGH_GEN_SURFEL_HIT_COUNT)
        ),
    };

    sb_sets_[SB_SET_SHADE_SURFEL] = {
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_SHADE_SURFEL_RGEN) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride
        ),
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_SHADE_SURFEL_RMISS) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride
        ),
        build_VkStridedDeviceAddressRegionKHR(
            dev_begin + static_cast<uint64_t>(SGH_SHADE_SURFEL_HIT_BEGIN) * a_sbt_entry_size.stride,
            a_sbt_entry_size.stride,
            a_sbt_entry_size.stride * static_cast<uint64_t>(SGH_SHADE_SURFEL_HIT_COUNT)
        ),
    };
}


void ddgi_t::
render_imgui_settings(cfg_t* a_cfg)
{
    SPOCK_PARANOID(a_cfg, "ddgi_t::render_imgui_settings: a_cfg cannot be nullptr");

    if (ImGui::TreeNode("DDGI settings")) {
        ImGui::DragFloat("sharpness", &a_cfg->depth_sharpness, 0.1f, 0.1f, 100.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("ddgi_depth_sharpness");
        }
        ImGui::DragFloat("preserve", &a_cfg->energy_preservation, 0.001f, 0.f, 1.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("ddgi_energy_preservation");
        }
        ImGui::DragFloat("intensity", &a_cfg->probe_intensity, 0.01f, 0.f, 10.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("ddgi_probe_intensity");
        }
        ImGui::DragFloat("hysteresis", &a_cfg->hysteresis, 0.001f, 0.7f, 0.999f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("ddgi_hysteresis");
        }
        ImGui::DragFloat("N_bias", &a_cfg->normal_bias, 0.001f, 0.f, 2.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("ddgi_normal_bias");
        }
        ImGui::TreePop();
    }
}


void ddgi_t::
render_imgui_probe_vis_options(float * a_probe_vis_radius, int * a_probe_vis_map_idx)
{
    SPOCK_PARANOID(a_probe_vis_radius, "ddgi_t::render_imgui_probe_vis_options: a_probe_vis_radius cannot be nullptr");
    SPOCK_PARANOID(a_probe_vis_map_idx, "ddgi_t::render_imgui_probe_vis_options: a_probe_vis_map_idx cannot be nullptr");

    if (ImGui::TreeNode("DDGI probe visualization")) {
        ImGui::Combo("probe vis", a_probe_vis_map_idx, k_ddgi_probe_vis_map_options, length_of_c_array(k_ddgi_probe_vis_map_options));
        ImGui::DragFloat("radius", a_probe_vis_radius, 0.01f, 0.1f, 1.f);
        if (ImGui::IsItemHovered()) {
            ImGui::SetTooltip("ddgi_probe_vis_radius");
        }
        ImGui::TreePop();
    }
}


void ddgi_t::
cmd_exec_gen_surfel_pipeline(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelines_[PPL_RT_GEN_SURFEL]);
    auto dim = probe_grid_dim_;
    dim.x *= per_probe_ray_count_;
    cmd_trace_rays(a_cmd_buf, sb_sets_[SB_SET_GEN_SURFEL], dim);
}


void ddgi_t::
cmd_exec_shade_surfel_pipeline(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipelines_[PPL_RT_SHADE_SURFEL]);
    const auto dim = glm::uvec3(image_extents_[IMG_SURFEL], 1);
    cmd_trace_rays(a_cmd_buf, sb_sets_[SB_SET_SHADE_SURFEL], dim);
}


void ddgi_t::
cmd_exec_update_pipelines(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipelines_[PPL_COM_UPDATE_DEPTH]);
    auto dim = (image_extents_[IMG_DEPTH] + depth_update_compute_local_size_ - glm::uvec2(1)) / depth_update_compute_local_size_;
    vkCmdDispatch(a_cmd_buf, dim.x, dim.y, 1);

    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_COMPUTE, pipelines_[PPL_COM_UPDATE_IRRADIANCE]);
    dim = (image_extents_[IMG_IRRADIANCE] + irradiance_update_compute_local_size_ - glm::uvec2(1)) / irradiance_update_compute_local_size_;
    vkCmdDispatch(a_cmd_buf, dim.x, dim.y, 1);
}


void ddgi_t::
cmd_compute_irradiance_field(VkCommandBuffer a_cmd_buf, dev_timer_array_t* a_dev_timer_array, bool a_update_on)
{
    SPOCK_PARANOID(a_dev_timer_array, "deferred_t::cmd_exec_visibility_pipelines: a_dev_timer_array cannot be NULL.");
    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_DDGI_RAYGEN);
    cmd_exec_gen_surfel_pipeline(a_cmd_buf);
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_DDGI_RAYGEN);

    VkMemoryBarrier barrier1 = build_VkMemoryBarrier(VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    const auto rt_stage_bit = VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR;
    vkCmdPipelineBarrier(a_cmd_buf, rt_stage_bit, rt_stage_bit, 0, 1, &barrier1, 0, nullptr, 0, nullptr);

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_DDGI_LIGHTING);
    cmd_exec_shade_surfel_pipeline(a_cmd_buf);
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_DDGI_LIGHTING);

    VkMemoryBarrier barrier2 = build_VkMemoryBarrier(VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    vkCmdPipelineBarrier(a_cmd_buf, rt_stage_bit, rt_stage_bit | VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 1, &barrier2, 0, nullptr, 0, nullptr);

    a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_DDGI_UPDATE);
    if (a_update_on) {
        cmd_exec_update_pipelines(a_cmd_buf);
    }
    a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_DDGI_UPDATE);
}


void ddgi_t::cmd_dummy_compute(VkCommandBuffer a_cmd_buf, dev_timer_array_t * a_dev_timer_array)
{
        a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_DDGI_RAYGEN);
        a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_DDGI_RAYGEN);
        a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_DDGI_LIGHTING);
        a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_DDGI_LIGHTING);
        a_dev_timer_array->cmd_start(a_cmd_buf, DEV_TIMER_DDGI_UPDATE);
        a_dev_timer_array->cmd_stop(a_cmd_buf, DEV_TIMER_DDGI_UPDATE);
}


void ddgi_t::
cmd_exec_vis_probe_pipeline(VkCommandBuffer a_cmd_buf)
{
    vkCmdBindPipeline(a_cmd_buf, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines_[PPL_GRA_VIS_PROBE]);
    vkCmdDraw(a_cmd_buf, probe_count(), 1, 0, 0);
}


ddgi_t::specialization_constant_t ddgi_t::
get_specialization_constant() const
{
    specialization_constant_t k{};
    k.data_ = std::make_unique<specialization_constant_t::data_t>();
    k.data_->log2_probe_grid_dim_x            = u32_log2(probe_grid_dim_.x);
    k.data_->log2_probe_grid_dim_y            = u32_log2(probe_grid_dim_.y);
    k.data_->log2_probe_grid_dim_z            = u32_log2(probe_grid_dim_.z);
    k.data_->probe_cell_size_x                = probe_cell_size_.x;
    k.data_->probe_cell_size_y                = probe_cell_size_.y;
    k.data_->probe_cell_size_z                = probe_cell_size_.z;
    k.data_->probe_grid_lower_x               = probe_grid_lower_.x;
    k.data_->probe_grid_lower_y               = probe_grid_lower_.y;
    k.data_->probe_grid_lower_z               = probe_grid_lower_.z;
    k.data_->log2_probe_count_per_image_row   = u32_log2(probe_count_per_image_.x);
    k.data_->log2_probe_surfel_resolution_x   = u32_log2(probe_surfel_resolution_.x);
    k.data_->log2_probe_surfel_resolution_y   = u32_log2(probe_surfel_resolution_.y);
    k.data_->log2_probe_irradiance_resolution = u32_log2(probe_irradiance_resolution_);
    k.data_->log2_probe_depth_resolution      = u32_log2(probe_depth_resolution_);

    return k;

}


void ddgi_t::
create_descriptor_set_layout(VkDevice a_vk_dev)
{
    const VkShaderStageFlags stage_flags = VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_RAYGEN_BIT_KHR;
    VkDescriptorSetLayoutBinding bindings[] = {
        build_VkDescriptorSetLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, stage_flags), // surfel_image
        build_VkDescriptorSetLayoutBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, stage_flags), // depth_image
        build_VkDescriptorSetLayoutBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, stage_flags), // irradiance_image
    };
    const auto descriptor_layout_create_info = build_VkDescriptorSetLayoutCreateInfo(length_of_c_array(bindings), bindings);
    descriptor_set_layout_ = descriptor_set_layout_t(a_vk_dev, descriptor_layout_create_info);

    VkDescriptorPoolSize pool_size{};
    pool_size.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    pool_size.descriptorCount = IMG_COUNT;

    auto pool_create_info = make_VkDescriptorPoolCreateInfo();
    pool_create_info.maxSets = 1;
    pool_create_info.poolSizeCount = 1;
    pool_create_info.pPoolSizes = &pool_size;

    descriptor_pool_ = descriptor_pool_t(a_vk_dev, pool_create_info);
    descriptor_pool_.alloc_descriptor_sets(1, &descriptor_set_layout_(), &descriptor_set_);
}



