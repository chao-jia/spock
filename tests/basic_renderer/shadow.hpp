#pragma once

#include "misc.hpp"
#include <spock/context.hpp>

class shadow_t{
public:
    shadow_t(const shadow_t&) = delete;
    shadow_t& operator=(const shadow_t&) = delete;
    shadow_t(shadow_t&&) = default;
    shadow_t& operator=(shadow_t&&) = default;

    shadow_t() : descriptor_sets_{} {}
    shadow_t(VkDevice a_vk_dev);

    void create_images(const context_t& a_context, const VkExtent2D& a_surface_extent, VkCommandBuffer a_cmd_buf, VkQueue a_queue);
    void write_descriptor_sets(VkDevice a_vk_dev, VkSampler a_dummy_sampler);
    bool init_spirvs(uint32_t a_vk_api_version, const std::string& a_scene_name);
    bool refresh_spirvs();
    void create_pipelines(VkDevice a_vk_dev, VkPipelineLayout a_pipeline_layout, uint32_t a_shader_group_handle_size);
    static uint32_t get_sbt_bytes(uint32_t a_sbt_stride);
    void write_sbt(const sbt_entry_size_t& a_sbt_entry_size, uint8_t** a_sbt_host_addr, VkDeviceAddress* a_sbt_dev_addr);

    void cmd_exec_shadow_pipelines(
        VkCommandBuffer a_cmd_buf, 
        VkPipelineLayout a_pipeline_layout,
        uint32_t a_ds_1st_moment_0_idx,
        uint32_t a_a_trous_iteration_count,
        const glm::uvec2& a_surface_extent, 
        const glm::uvec2& a_estimate_variance_workgroup_size,
        const glm::uvec2& a_filter_variance_workgroup_size,
        const glm::uvec2& a_a_trous_filter_workgroup_size,
        dev_timer_array_t* a_dev_timer_array
    );

    const descriptor_set_layout_t* descriptor_set_layouts() const { return descriptor_set_layouts_.data(); }
    const VkDescriptorSet* descriptor_sets() const { return descriptor_sets_.data(); }

private:
    std::unique_ptr<image_t[]> images_;
    std::unique_ptr<image_view_t[]> image_views_;

    std::unique_ptr<spirv_t[]> spirvs_;
    std::unique_ptr<pipeline_t[]> pipelines_;

    shader_binding_set_t sb_set_;
    shader_group_handle_pool_t shader_group_handle_pool_;

    descriptor_pool_t descriptor_pool_;
    std::array<descriptor_set_layout_t, SHADOW_DS_LAYOUT_COUNT> descriptor_set_layouts_;
    std::array<VkDescriptorSet, SHADOW_DS_COUNT> descriptor_sets_;

    void create_descriptor_set_layout(VkDevice a_vk_dev);

};




