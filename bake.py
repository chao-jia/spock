import os
import shutil
import platform
import pathlib
import shlex
import subprocess
import argparse
import json

class logger:
    msg_info_prefix = "########"
    msg_info_suffix = "########"
    msg_warning_prefix = "\n???????? WARNING:"
    msg_warning_suffix = "????????\n"
    msg_error_prefix = "\n!!!!!!!! ERROR:"
    msg_error_suffix = "!!!!!!!!\n"
    
    @classmethod
    def wrap_str(cls, prefix, msg, suffix, delim=' '):
        return prefix + delim + msg + delim + suffix
    
    @classmethod
    def info(cls, msg):
        print(logger.wrap_str(logger.msg_info_prefix, msg, logger.msg_info_suffix))
    
    @classmethod
    def err(cls, msg):
        print(logger.wrap_str(logger.msg_error_prefix, msg, logger.msg_error_suffix))
    
    @classmethod
    def warn(cls, msg):
        print(logger.wrap_str(logger.msg_warning_prefix, msg, logger.msg_warning_suffix))


def make_vcpkg_triplet_option(triplet):
    return '-DVCPKG_TARGET_TRIPLET={}'.format(triplet)

def get_cmake_arch_option(a_cmake_arch):
    arch_option = ''
    if a_cmake_arch != '':
        arch_option = '-A {}'.format(a_cmake_arch)
        return arch_option

    cpu = platform.machine()
    if cpu in ['AMD64', 'x86_64']:
        arch_option = '-A x64'
    elif cpu in ['x86', 'i386', 'i686'] :
        arch_option = '-A x86'
    elif cpu in ['arm64', 'aarch64_be', 'aarch64', 'armv8b', 'armv8l']:
        arch_option = '-A arm64'
    elif cpu in ['arm']:
        arch_option = '-A arm'
    return arch_option

def get_vcpkg_triplet_option(triplet_conf = ''):
    if (triplet_conf != ''):
        return make_vcpkg_triplet_option(triplet_conf)

    default_triplet = os.getenv("VCPKG_DEFAULT_TRIPLET", "")
    if (default_triplet != ''):
        return make_vcpkg_triplet_option(default_triplet)

    cpu = platform.machine()
    if platform.system() == 'Windows':        
        if cpu in ['AMD64', 'x86_64']:
            default_triplet = 'x64-windows'
        elif cpu in ['x86', 'i386', 'i686'] :
            default_triplet = 'x86-windows'
        elif cpu in ['arm64', 'aarch64_be', 'aarch64', 'armv8b', 'armv8l']:
            default_triplet = 'arm64-windows'
    elif platform.system() == 'Linux':
        if cpu in ['AMD64', 'x86_64']:
            default_triplet = 'x64-linux'
        elif cpu in ['arm64', 'aarch64_be', 'aarch64', 'armv8b', 'armv8l']:
            default_triplet = 'arm64-linux'
    elif platform.system() == 'Darwin':
        if cpu in ['AMD64', 'x86_64']:
            default_triplet = 'x64-osx'

    return make_vcpkg_triplet_option(default_triplet)


def fixup_cmake_target(pkg_root_dir, pkg_name):
    print ("fixing up cmake debug target...")
    
    share_dir = os.path.join(pkg_root_dir, 'share', pkg_name)
    debug_include_dir = os.path.join(pkg_root_dir, 'debug', 'include')
    debug_share_dir = os.path.join(pkg_root_dir, 'debug', 'share', pkg_name)
    debug_share_parent = os.path.join(pkg_root_dir, 'debug', 'share')
    
    if not os.path.isdir(debug_share_dir):
        print ("Error: cannot find {}, exiting...".format(debug_share_dir))
        return

    target_name = ''
    for filename in os.listdir(share_dir):
        target_name_end = filename.find('-targets.cmake')
        if target_name_end != -1:
            target_name = filename[:target_name_end]
            break
        
    src_target_config = os.path.join(debug_share_dir, '{}-targets-debug.cmake'.format(target_name))
    dst_target_config = os.path.join(share_dir, '{}-targets-debug.cmake'.format(target_name))
    if (not os.path.isfile(src_target_config)):
        print ("Error: cannot find {}, exiting...".format(src_target_config))
        return
    fi = open(src_target_config, 'r')
    fo = open(dst_target_config, 'w')
    for line in fi:
        pattern = '${_IMPORT_PREFIX}/'
        replace_str = '${_IMPORT_PREFIX}/debug/'
        fo.write(line.replace(pattern, replace_str))
    fi.close()
    fo.close()
    
    if os.path.isdir(debug_share_dir):
        shutil.rmtree(debug_share_dir)
        if not os.listdir(debug_share_parent):
            os.rmdir(debug_share_parent)
    if os.path.isdir(debug_include_dir):
        shutil.rmtree(debug_include_dir)

# configure the following options in json file "conf.json"
# "conf.template.json" serves as a starting point
vcpkg_root = os.getenv("VCPKG_ROOT", "")
cmake_generator = ""
cmake_arch = ""
vcpkg_triplet = ""
installed_dir = ""

conf_json_file = os.path.join(os.getcwd(), 'config-bake.json')
if os.path.isfile(conf_json_file):
    try:    
        with open(conf_json_file) as conf_json:
            conf = json.load(conf_json)
            if 'vcpkg_root' in conf:
                vcpkg_root = conf["vcpkg_root"]
            if 'cmake_generator' in conf:
                cmake_generator = conf["cmake_generator"]
            if 'vcpkg_triplet' in conf:
                vcpkg_triplet = conf["vcpkg_triplet"]
            if "cmake_arch" in conf:
                cmake_arch = conf["cmake_arch"]
            if 'installed_dir' in conf:
                installed_dir = conf["installed_dir"]
    except Exception as e:
        print(type(e))
        print(e)
        exit(1)

cmake_generator_option = ""
if cmake_generator != '':
    cmake_generator_option = "-G {}".format(cmake_generator)

vcpkg_triplet_option = ""
vcpkg_toolchain_option = ""
if vcpkg_root != "" and os.path.isdir(vcpkg_root):
    vcpkg_triplet_option = get_vcpkg_triplet_option(vcpkg_triplet)
    vcpkg_toolchain_option = "-DCMAKE_TOOLCHAIN_FILE={}/scripts/buildsystems/vcpkg.cmake {}".format(str(pathlib.Path(vcpkg_root).as_posix()), vcpkg_triplet_option)


parser = argparse.ArgumentParser(description='configure and compile')
parser.add_argument('--clean', action='store_true', required=False, help='remove build files')
parser.add_argument('--force', action='store_true', required=False, help='remove build files, then reconfigure and recompile.')
parser.add_argument('--conf', action='store_true', required=False, help='run cmake configuration only')
parser.add_argument('--release', action='store_true', required=False, help='build release version')
parser.add_argument('--debug', action='store_true', required=False, help='build debug version')
parser.add_argument('--install', action='store_true', required=False, help='install library')


args = parser.parse_args()

cmake_arch_option = ""
platform_dir_prefix = 'nix'
if platform.system() == 'Windows':
    platform_dir_prefix = 'win'
    cmake_arch_option = get_cmake_arch_option(cmake_arch)

project_root_dir = os.getcwd()
cache_dir = os.path.join(project_root_dir, 'cache')
build_dir = os.path.join(project_root_dir, 'build', platform_dir_prefix)
debug_dir = os.path.join(project_root_dir, platform_dir_prefix + '_debug')
release_dir = os.path.join(project_root_dir, platform_dir_prefix + '_release')
imgui_ini_path = os.path.join(project_root_dir, 'build', "imgui.ini")

if installed_dir == "":
    installed_dir = os.path.join(project_root_dir, 'installed')

if args.clean or args.force:
    if os.path.isdir(cache_dir):
        logger.info("remove directory {}".format(cache_dir))
        shutil.rmtree(cache_dir)

    if os.path.isdir(build_dir):
        logger.info("remove directory {}".format(build_dir))
        shutil.rmtree(build_dir)

    if os.path.isdir(debug_dir):
        logger.info("remove directory {}".format(debug_dir))
        shutil.rmtree(debug_dir)

    if os.path.isdir(release_dir):
        logger.info("remove directory {}".format(release_dir))
        shutil.rmtree(release_dir)

    if os.path.isdir(installed_dir):
        logger.info("remove directory {}".format(installed_dir))
        shutil.rmtree(installed_dir)

    if os.path.isfile(imgui_ini_path):
        logger.info(f"remove file {imgui_ini_path}")
        os.remove(imgui_ini_path)

    if args.clean:
        exit(0)

if not os.path.isdir(build_dir):
    os.makedirs(build_dir)

config_cmd = 'cmake {} {} {} {}'.format(vcpkg_toolchain_option, cmake_generator_option, cmake_arch_option, str(pathlib.Path(project_root_dir).as_posix()))

build_nix_cmd = 'cmake --build .'
build_debug_cmd = 'cmake --build . --config Debug'
build_release_cmd = 'cmake --build . --config Release'

install_debug_cmd = 'cmake --install . --config Debug --prefix {}'.format(os.path.join(installed_dir, 'debug'))
install_release_cmd = 'cmake --install . --config Release  --prefix {}'.format(installed_dir)

logger.info("config: {}".format(config_cmd))
subprocess.run(shlex.split(config_cmd), cwd=build_dir)

if args.conf:
    exit(0)

if platform.system() == 'Windows':
    if not args.release or args.debug:
        logger.info("debug build: {}".format(build_debug_cmd))
        subprocess.run(shlex.split(build_debug_cmd), cwd=build_dir)

    if args.release or not args.debug:
        logger.info("release build: {}".format(build_release_cmd))
        subprocess.run(shlex.split(build_release_cmd), cwd=build_dir)
else:
    logger.info("nix build: {}".format(build_debug_cmd))
    subprocess.run(shlex.split(build_debug_cmd), cwd=build_dir)

if args.install:
    if os.path.isdir(installed_dir):
        logger.info("remove directory {}".format(installed_dir))
        shutil.rmtree(installed_dir)
    subprocess.run(shlex.split(install_debug_cmd), cwd=build_dir)
    subprocess.run(shlex.split(install_release_cmd), cwd=build_dir)
    fixup_cmake_target(installed_dir, 'spock')

