#include <spock/utils.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <optional>
#include <iterator>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <glslang/StandAlone/ResourceLimits.h>
#include <glslang/StandAlone/DirStackFileIncluder.h>
#include <spock/generated.hpp>
#include <cstdlib>
#if _MSC_VER
#include <intrin.h>
#endif

namespace {

template <
    typename I,
    std::enable_if_t<std::is_integral_v<I> || std::is_enum_v<I>, bool> = true
>
inline std::string
to_hex_string(I w, size_t hex_len = sizeof(I) << 1)
{
    static const char* digits = "0123456789ABCDEF";
    std::string rc(hex_len, '0');
    for (size_t i = 0, j = (hex_len - 1) * 4; i < hex_len; ++i, j -= 4)
        rc[i] = digits[(w >> j) & 0x0f];
    return "0x" + rc;
}

inline void 
read_nested_exception(const std::exception& a_exception, int a_level, std::string& a_output_str)
{
    a_output_str += std::string(a_level, ' ') + a_exception.what() + '\n';
    try {
        std::rethrow_if_nested(a_exception);
    }
    catch (const std::exception& e) {
        read_nested_exception(e, a_level + 1, a_output_str);
    }
    catch (...) {

    }

}

// for base 64 encode/decode, https://stackoverflow.com/a/37109258/2277106

const char* k_base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

const int k_base64_indices[256] =
{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  62, 63, 62, 62, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0,  0,  0,  0,  0,  0,
    0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,  63,
    0,  26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
};


//https://stackoverflow.com/q/355967/2277106
inline uint32_t popcnt( uint32_t x )
{
    x -= ((x >> 1) & 0x55555555);
    x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
    x = (((x >> 4) + x) & 0x0f0f0f0f);
    x += (x >> 8);
    x += (x >> 16);
    return x & 0x0000003f;
}


inline EShLanguage
glslang_stage_from_vk_flags(VkShaderStageFlagBits stage)
{
    switch (stage)
    {
    case VK_SHADER_STAGE_VERTEX_BIT:
        return EShLanguage::EShLangVertex;

    case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
        return EShLanguage::EShLangTessControl;

    case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
        return EShLanguage::EShLangTessEvaluation;

    case VK_SHADER_STAGE_GEOMETRY_BIT:
        return EShLanguage::EShLangGeometry;

    case VK_SHADER_STAGE_FRAGMENT_BIT:
        return EShLanguage::EShLangFragment;

    case VK_SHADER_STAGE_COMPUTE_BIT:
        return EShLanguage::EShLangCompute;

    case VK_SHADER_STAGE_RAYGEN_BIT_KHR:
        return EShLanguage::EShLangRayGen;

    case VK_SHADER_STAGE_ANY_HIT_BIT_KHR:
        return EShLanguage::EShLangAnyHit;

    case VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR:
        return EShLanguage::EShLangClosestHit;

    case VK_SHADER_STAGE_MISS_BIT_KHR:
        return EShLanguage::EShLangMiss;

    case VK_SHADER_STAGE_INTERSECTION_BIT_KHR:
        return EShLanguage::EShLangIntersect;

    case VK_SHADER_STAGE_CALLABLE_BIT_NV:
        return EShLanguage::EShLangCallable;

    case VK_SHADER_STAGE_TASK_BIT_NV:
        return EShLanguage::EShLangTaskNV;

    case VK_SHADER_STAGE_MESH_BIT_NV:
        return EShLanguage::EShLangMeshNV;
    }

    throw std::runtime_error{ "Unknown shader stage of flag bits: " + to_hex_string(stage) };
}



std::pair<glslang::EShTargetClientVersion, glslang::EShTargetLanguageVersion>
glslang_version_from_vk_api_version(uint32_t a_vk_api_version)
{
    switch (a_vk_api_version) {
    case VK_MAKE_VERSION(1, 0, 0):
        return std::make_pair(glslang::EShTargetVulkan_1_0, glslang::EShTargetSpv_1_0);
    case VK_MAKE_VERSION(1, 1, 0):
        return std::make_pair(glslang::EShTargetVulkan_1_1, glslang::EShTargetSpv_1_3);
    case VK_MAKE_VERSION(1, 2, 0):
        return std::make_pair(glslang::EShTargetVulkan_1_2, glslang::EShTargetSpv_1_5);
    default:
        throw std::runtime_error{ "unknown vulkan api version: " + spock::vulkan_api_version_str(a_vk_api_version) };
    }
}

constexpr EShMessages k_esh_messages = static_cast<EShMessages>(EShMsgSpvRules | EShMsgVulkanRules | EShMsgDefault);
static TBuiltInResource g_glslang_builtin_resource; // initialized in glslang_initializer_t
static spock::glslang_initializer_t glslang_process_initializer; 

std::unique_ptr<glslang::TShader> 
make_glslang_shader
(
    uint32_t a_vulkan_api_version, 
    EShLanguage a_shader_stage, 
    char const * const * a_glsl_src_c_str, 
    char const * const * a_file_path_c_str = nullptr
)
{
    auto shader = std::make_unique<glslang::TShader>(a_shader_stage);
    auto [client_version, target_version] = glslang_version_from_vk_api_version(a_vulkan_api_version);
    if (*a_file_path_c_str) {
        shader->setStringsWithLengthsAndNames(a_glsl_src_c_str, NULL, a_file_path_c_str, 1);
    }
    else {
        shader->setStrings(a_glsl_src_c_str, 1);
    }
    shader->setNanMinMaxClamp(false);
    shader->setEnvInput(glslang::EShSourceGlsl, a_shader_stage, glslang::EShClientVulkan, spock::spirv_t::CLIENT_INPUT_SEMANTICS_VERSION);
    shader->setEnvClient(glslang::EShClientVulkan, client_version);
    shader->setEnvTarget(glslang::EShTargetSpv, target_version);
    return shader;
}


inline DirStackFileIncluder
make_includer(const std::vector<std::string>& a_glsl_inc_dirs)
{
    DirStackFileIncluder includer;
    std::for_each(
        a_glsl_inc_dirs.rbegin(),
        a_glsl_inc_dirs.rend(),
        [&includer](const std::string& dir) {
            includer.pushExternalLocalDirectory(dir);
        }
    );
    return includer;
}


// a_shader should have been parsed
bool
build_spirv_from_shader
(
    const std::string& a_glsl_file_path,
    glslang::TShader& a_shader,
    std::vector<uint32_t>& a_spirv
)
{

    glslang::TProgram program;
    program.addShader(&a_shader);

    //TODO: reflection
    //program.buildReflection(ReflectOptions);
    //program.dumpReflection();

    if (!program.link(k_esh_messages) || !program.mapIO()) {
        spock::err("program link failed on " + a_glsl_file_path);
        spock::err(program.getInfoLog());
        spock::err(program.getInfoDebugLog());
        return false;
    }
    glslang::TIntermediate* intermediate = program.getIntermediate(a_shader.getStage());
    if (!intermediate) {
        spock::err("program failed to get intermediate code of " + a_glsl_file_path);
        return false;
    }

    spv::SpvBuildLogger build_logger;
    glslang::SpvOptions spv_options;

    spv_options.disableOptimizer = false;
    spv_options.optimizeSize = false;
    spv_options.disassemble = false;
    spv_options.validate = false;

    glslang::GlslangToSpv(*intermediate, a_spirv, &build_logger, &spv_options);
    auto build_msg = build_logger.getAllMessages();
    build_msg.erase(std::remove_if(build_msg.begin(), build_msg.end(), isspace), build_msg.end());

    if (!build_msg.empty()) {
        spock::warn("spirv build messages: [[ " + build_msg + " ]]");
    }
    if (a_spirv.empty() ||a_spirv[0] != spock::spirv_t::SPIRV_MAGIC_NUMBER) {
        spock::err("build spirv from " + a_glsl_file_path + " failed");
        return false;
    }
    return true;

}





} // anonymous namespace

namespace spock {

void
verbose(const std::string& a_msg)
{
    //std::cout << "verbose: " << a_msg << '\n';
}

void
info(const std::string& a_msg)
{
    std::cout << "info: " << a_msg << '\n';
}

void
warn(const std::string& a_msg)
{
    std::cerr << "warning: " << a_msg << '\n';
}

void
err(const std::string& a_msg)
{
    std::cerr << "error: " << a_msg << '\n';
}

std::string 
nested_exception_str(const std::exception& a_exception)
{
    std::string e_str{};
    read_nested_exception(a_exception, 0, e_str);
    return e_str;
}

void 
except(const std::string& a_msg)
{
    throw std::runtime_error{ a_msg };
}


void spock::nested_except(const std::string & a_msg)
{
    std::throw_with_nested(std::runtime_error{ a_msg });
}


uint32_t spock::ctz(uint32_t x)
{
    if (x == 0) return 32;

#if defined(_MSC_VER)
    unsigned long idx = 0;
    _BitScanForward(&idx, x);
    return static_cast<uint32_t>(idx);
#elif defined(__GNUC__) || defined(__clang__)
    return static_cast<uint32_t>(__builtin_ctz(x));
#else
    return popcnt((x & -x) - 1); // https://stackoverflow.com/q/355967/2277106
#endif
}


uint32_t spock::clz(uint32_t x)
{
    if (x == 0) return 32;

#if defined(_MSC_VER)
    unsigned long idx = 0;
    _BitScanReverse(&idx, x);
    return static_cast<uint32_t>(31 - idx);
#elif defined(__GNUC__) || defined(__clang__)
    return static_cast<uint32_t>(__builtin_clz(x));
#else
    // https://stackoverflow.com/q/355967/2277106

    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);
    return 32 - popcnt(x);
#endif
}


uint32_t spock::
power_of_two_ceil(uint32_t x)
{
    if (is_power_of_two(x)) return x;
    return 1u << (32 - clz(x));
}


std::string 
to_lower(const std::string& str)
{
    std::string result{ str };
    std::transform(str.begin(), str.end(), result.begin(), [](unsigned char c) { return std::tolower(c); });
    return result;
}


std::string 
encode_base64(const uint8_t * a_buffer, size_t a_count)
{
    std::string result((a_count + 2) / 3 * 4, '=');
    char* str = &result[0];
    size_t j = 0, pad = a_count % 3;
    const size_t last = a_count - pad;

    for (size_t i = 0; i < last; i += 3)
    {
        int n = int(a_buffer[i]) << 16 | int(a_buffer[i + 1]) << 8 | a_buffer[i + 2];
        str[j++] = k_base64_chars[n >> 18];
        str[j++] = k_base64_chars[n >> 12 & 0x3F];
        str[j++] = k_base64_chars[n >> 6 & 0x3F];
        str[j++] = k_base64_chars[n & 0x3F];
    }
    if (pad)  /// Set padding
    {
        int n = --pad ? int(a_buffer[last]) << 8 | a_buffer[last + 1] : a_buffer[last];
        str[j++] = k_base64_chars[pad ? n >> 10 & 0x3F : n >> 2];
        str[j++] = k_base64_chars[pad ? n >> 4 & 0x03F : n << 4 & 0x3F];
        str[j++] = pad ? k_base64_chars[n << 2 & 0x3F] : '=';
    }
    return result;
}


std::vector<uint8_t> 
decode_base64(const char* a_c_str, size_t a_count)
{
    if (a_count == 0) return std::vector<uint8_t>{};

    size_t j = 0;
    size_t pad1 = a_count % 4 || a_c_str[a_count - 1] == '=';
    size_t pad2 = pad1 && (a_count % 4 > 2 || a_c_str[a_count - 2] != '=');
    const size_t last = (a_count - pad1) / 4 << 2;
    std::vector<uint8_t> result(last / 4 * 3 + pad1 + pad2, '\0');
    uint8_t* str = result.data();

    for (size_t i = 0; i < last; i += 4)
    {
        int n = k_base64_indices[a_c_str[i]] << 18 
            | k_base64_indices[a_c_str[i + 1]] << 12 
            | k_base64_indices[a_c_str[i + 2]] << 6 
            | k_base64_indices[a_c_str[i + 3]];
        str[j++] = n >> 16;
        str[j++] = n >> 8 & 0xFF;
        str[j++] = n & 0xFF;
    }
    if (pad1)
    {
        int n = k_base64_indices[a_c_str[last]] << 18 | k_base64_indices[a_c_str[last + 1]] << 12;
        str[j++] = n >> 16;
        if (pad2)
        {
            n |= k_base64_indices[a_c_str[last + 2]] << 6;
            str[j++] = n >> 8 & 0xFF;
        }
    }
    return result;
}


int put_env(const char * a_name, const char * a_value)
{
#if defined(_MSC_VER)
    return _putenv_s(a_name, a_value);
#else
    std::string str = a_name + std::string("=") + a_value;
    return putenv(str.c_str());
#endif
}


std::string
vulkan_api_version_str(uint32_t a_vk_api_version)
{
    return std::to_string(VK_VERSION_MAJOR(a_vk_api_version))
        + '.' + std::to_string(VK_VERSION_MINOR(a_vk_api_version))
        + '.' + std::to_string(VK_VERSION_PATCH(a_vk_api_version));
}

std::string
vulkan_driver_version_str(uint32_t a_vendor_id, uint32_t a_version)
{
    std::string vendor;
    uint32_t major = -1, minor = -1, patch = -1;

    switch (a_vendor_id)
    {
    case 0x10DE: // Nvidia
        vendor = "Nvidia";
        major = (a_version >> 22) & 0x3ff;
        minor = (a_version >> 14) & 0x0ff;
        patch = (a_version) & 0x3fff;
        break;
    case 0x8086: // Intel
        vendor = "Intel";
        major = (a_version >> 14);
        minor = (a_version) & 0x3fff;
        patch = 0;
        break;
    case 0x1002:
        vendor = "AMD";
        major = (a_version >> 14);
        minor = (a_version) & 0x3fff;
        patch = 0;
        break;
    case 0x1010:
        vendor = "ImgTec";
        major = (a_version >> 14);
        minor = (a_version) & 0x3fff;
        patch = 0;
        break;
    case 0x13B5:
        vendor = "ARM";
        major = (a_version >> 14);
        minor = (a_version) & 0x3fff;
        patch = 0;
        break;
    case 0x5143:
        vendor = "Qualcomm";
        major = (a_version >> 14);
        minor = (a_version) & 0x3fff;
        patch = 0;
        break;
    default:
        vendor = "Unknown vendor";
        major = a_vendor_id;
        minor = a_version;
        patch = 0;
    }

    return vendor + ": " + std::to_string(major) + '.' + std::to_string(minor) + '.' + std::to_string(patch);
}


std::string
device_properties_str(const VkPhysicalDeviceProperties& dev_prop)
{
    
    const std::string indent(6, ' ');
    const std::string indent2(12, ' ');
    return '\n' + indent + dev_prop.deviceName + ":\n"
        + indent2 + "type: " + str_from_VkPhysicalDeviceType(dev_prop.deviceType).data() + '\n'
        + indent2 + "api version: " + vulkan_api_version_str(dev_prop.apiVersion) + '\n'
        + indent2 + "driver version: " + vulkan_driver_version_str(dev_prop.vendorID, dev_prop.driverVersion) + '\n';
}

std::string spock::file_to_str(const std::string_view a_file_path)
{
    std::ifstream ifs(a_file_path, std::ios::binary | std::ios::ate);
    spock::ensure(ifs.is_open(), std::string("failed to open file ") + a_file_path.data());
    size_t bytes = ifs.tellg();
    std::string out_str(bytes, 0);
    ifs.seekg(0);
    ifs.read(out_str.data(), bytes);
    return out_str;
}

void spock::c_str_to_file(const char * a_c_str, size_t a_bytes, const std::string_view a_file_path)
{
    std::ofstream ofs(a_file_path, std::ios::out | std::ios::binary | std::ios::trunc);
    spock::ensure(ofs.is_open(), std::string("failed to open file ") + a_file_path.data());
    ofs.write(a_c_str, a_bytes);
    ofs.close();
}

// vkspec 1.2.187: 43.3 required format support
bool vk_format_feature_filter_linear_supported(VkFormat a_format)
{
    switch (a_format) {
    // table 65:
    case VK_FORMAT_B4G4R4A4_UNORM_PACK16:
    case VK_FORMAT_R5G6B5_UNORM_PACK16:
    case VK_FORMAT_A1R5G5B5_UNORM_PACK16:
    // table 66:
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R8_SNORM:
    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R8G8_SNORM:
    // table 67:
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_SNORM:
    case VK_FORMAT_R8G8B8A8_SRGB:
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_SRGB:
    case VK_FORMAT_A8B8G8R8_UNORM_PACK32:
    case VK_FORMAT_A8B8G8R8_SNORM_PACK32:
    case VK_FORMAT_A8B8G8R8_SRGB_PACK32:
    // table 68:
    case VK_FORMAT_A2B10G10R10_UNORM_PACK32:
    // table 69:
    case VK_FORMAT_R16_SFLOAT:
    case VK_FORMAT_R16G16_SFLOAT:
    case VK_FORMAT_R16G16B16A16_UINT:
    case VK_FORMAT_R16G16B16A16_SINT:
    case VK_FORMAT_R16G16B16A16_SFLOAT:
    // table 71:
    case VK_FORMAT_B10G11R11_UFLOAT_PACK32:
    case VK_FORMAT_E5B9G9R9_UFLOAT_PACK32:
        return true;
    default:
        break;
    }
    return false;
}

//  basic implementation http://extremelearning.com.au/how-to-evenly-distribute-points-on-a-sphere-more-effectively-than-the-canonical-fibonacci-lattice/#more-3069
void spock::
fibonacci_sphere(uint32_t a_sample_count, glm::vec3* a_normals)
{
    SPOCK_PARANOID(a_normals != nullptr, "fibonacci_sphere: a_normals cannot be nullptr");

    const float rcp_goldren_ratio = 0.5f * std::sqrt(5.f) - 0.5f;
    const float rcp_sample_count = 1.f / static_cast<float>(a_sample_count);
    const float golden_angle = 2.f * glm::pi<float>() * rcp_goldren_ratio;

    // spherical coordinate system: https://en.wikipedia.org/wiki/Spherical_coordinate_system#/media/File:3D_Spherical.svg
    // theta in (0, pi): polar angle  (angle between normal and +z)
    // phi unbounded: azimuthal angle (angle between normal and +x)
    for (uint32_t i = 0; i < a_sample_count; ++i) {
        const float phi = i * golden_angle;

        const auto cos_theta = 1.f - 2.f * (static_cast<float>(i) + 0.5f) * rcp_sample_count;
        const auto sin_theta = std::sqrt(1 - cos_theta * cos_theta); // guaranteed to be non-negative
        const auto sin_phi = std::sin(phi);
        const auto cos_phi = std::cos(phi);

        a_normals[i] = glm::vec3(cos_phi * sin_theta, sin_phi * sin_theta, cos_theta);
    }
}


std::string get_glsl_ext(const std::string& a_glsl_file_name)
{
    namespace stdfs = std::filesystem;
    auto file_path = stdfs::path(a_glsl_file_name);
    auto ext = file_path.extension().string();
    if (ext == ".glsl") {
        auto path = file_path.stem();
        ext = path.extension().string();
    }

    return ext;
}

VkShaderStageFlagBits
vk_shader_stage_from_file_name(const std::string& a_glsl_file_name)
{
    auto ext = get_glsl_ext(a_glsl_file_name);

    if (ext == ".vert") {
        return VK_SHADER_STAGE_VERTEX_BIT;
    }
    else if (ext == ".tesc") {
        return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    }
    else if (ext == ".tese") {
        return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    }
    else if (ext == ".geom") {
        return VK_SHADER_STAGE_GEOMETRY_BIT;
    }
    else if (ext == ".frag") {
        return VK_SHADER_STAGE_FRAGMENT_BIT;
    }
    else if (ext == ".comp") {
        return VK_SHADER_STAGE_COMPUTE_BIT;
    }
    else if (ext == ".rgen") {
        return VK_SHADER_STAGE_RAYGEN_BIT_KHR;
    }
    else if (ext == ".rmiss") {
        return VK_SHADER_STAGE_MISS_BIT_KHR;
    }
    else if (ext == ".rint") {
        return VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
    }
    else if (ext == ".rahit") {
        return VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
    }
    else if (ext == ".rchit") {
        return VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
    }
    else if (ext == ".rmiss") {
        return VK_SHADER_STAGE_MISS_BIT_KHR;
    }
    else if (ext == ".rcall") {
        return VK_SHADER_STAGE_CALLABLE_BIT_KHR;
    }
    else if (ext == ".task") {
        return VK_SHADER_STAGE_TASK_BIT_NV;
    }
    else if (ext == ".mesh") {
        return VK_SHADER_STAGE_MESH_BIT_NV;
    }

    else {
        throw std::runtime_error{ "Unknown shader stage of shader file: " + ext };
    }
}


spock::host_perf_timer_t::
host_perf_timer_t()
    : moving_average_filter_width_{ 0 }
    , frame_queue_len_max_{ 0 }
    , frame_queue_tail_idx_{ 0 }
    , frame_queue_len_{ 0 }
    , filtered_elapsed_ms_{ 0.f }
{}


spock::host_perf_timer_t::
host_perf_timer_t(uint32_t a_moving_average_filter_width, uint32_t a_frame_queue_len_max)
    : moving_average_filter_width_{ a_moving_average_filter_width }
    , frame_queue_len_max_{ std::max({a_moving_average_filter_width, a_frame_queue_len_max}) }
    , frame_queue_tail_idx_{ 0 }
    , frame_queue_len_{ 0 }
    , filtered_elapsed_ms_{ 0.f }
{
    SPOCK_PARANOID(moving_average_filter_width_ > 0, "host_perf_timer_t::begin: moving_average_filter_width_ must > 0");
    SPOCK_PARANOID(frame_queue_len_max_ > 0, "host_perf_timer_t::begin: frame_queue_len_max_ must > 0");

    frame_queue_ = std::unique_ptr<float[]>(new float[frame_queue_len_max_]);
}


void spock::host_perf_timer_t::
stop()
{
    const float tmp = host_timer_.elapsed_ms();
    const auto prev_filter_width = std::min(moving_average_filter_width_, frame_queue_len_);
    const auto old_frame_idx = (frame_queue_tail_idx_ - prev_filter_width + frame_queue_len_max_) % frame_queue_len_max_;
    float accu_ms = filtered_elapsed_ms_ * prev_filter_width;
    const float old_weight = static_cast<float>(prev_filter_width == moving_average_filter_width_);
    frame_queue_[frame_queue_tail_idx_] = tmp;
    frame_queue_len_ = std::min(frame_queue_len_ + 1, frame_queue_len_max_);
    const auto filter_width = std::min(moving_average_filter_width_, frame_queue_len_);
    filtered_elapsed_ms_ = (accu_ms + tmp - old_weight * frame_queue_[old_frame_idx]) / filter_width;
    frame_queue_tail_idx_ = (frame_queue_tail_idx_ + 1) % frame_queue_len_max_;
}


glslang_initializer_t::
glslang_initializer_t() 
{
    spock::info("initializing glslang");
    g_glslang_builtin_resource = glslang::DefaultTBuiltInResource;
    glslang::InitializeProcess();
}

glslang_initializer_t::
~glslang_initializer_t() 
{
    spock::info("deinitializing glslang");
    glslang::FinalizeProcess();
}


// glslangValidator: option `-V` also enables `-l` (link all input files together to form a single module)
/* --target-env vulkan1.2
*   ClientVersion = glslang::EShTargetVulkan_1_2;
*	Client = glslang::EShClientVulkan;
*			Options |= EOptionSpv;
*			Options |= EOptionVulkanRules;
*			Options |= EOptionLinkProgram;
*/

const std::vector<uint32_t>
spirv_from_glsl_src(
    const char* a_glsl_src_c_str,
    VkShaderStageFlagBits a_vk_flags,
    uint32_t a_vulkan_api_version,
    const char* a_glsl_file_path_c_str,
    const std::vector<std::string>& a_glsl_inc_dirs,
    std::set<std::string>* a_includes
)
{
    auto stage = glslang_stage_from_vk_flags(a_vk_flags);
    auto shader = make_glslang_shader(a_vulkan_api_version, stage, &a_glsl_src_c_str, &a_glsl_file_path_c_str);
    auto includer = make_includer(a_glsl_inc_dirs);

    bool parse_ok = false;
    if (a_glsl_file_path_c_str) {
        parse_ok = shader->parse(&g_glslang_builtin_resource, spock::spirv_t::DEFAULT_VERSION, false, k_esh_messages);
    }
    else {
        parse_ok = shader->parse(&g_glslang_builtin_resource, spock::spirv_t::DEFAULT_VERSION, false, k_esh_messages, includer);
    }
    std::string glsl_file_path_str = a_glsl_file_path_c_str ? std::string(a_glsl_file_path_c_str) : std::string("a glsl src str");
    if (!parse_ok) {
        spock::err("failed to parse " + glsl_file_path_str);
        spock::err(shader->getInfoLog());
        spock::err(shader->getInfoDebugLog());
        return std::vector<uint32_t>();
    }
    if (a_includes) {
        *a_includes = includer.getIncludedFiles();
    }
    std::vector<uint32_t> spirv;
    build_spirv_from_shader(glsl_file_path_str, *shader, spirv);
    return spirv;
}


std::set<std::string> get_glsl_included_files(
    const std::string& a_glsl_path_str, 
    uint32_t a_vulkan_api_version, 
    const std::vector<std::string>& a_glsl_inc_dirs
)
{
    const std::string glsl_src_src = file_to_str(a_glsl_path_str);
    char const* glsl_src_c_src = glsl_src_src.c_str();
    char const* glsl_file_path_c_str = a_glsl_path_str.c_str();

    const auto stage = glslang_stage_from_vk_flags(vk_shader_stage_from_file_name(a_glsl_path_str));
    auto unique_shader = make_glslang_shader(a_vulkan_api_version, stage, &glsl_src_c_src, &glsl_file_path_c_str);
    DirStackFileIncluder includer = make_includer(a_glsl_inc_dirs);

    std::string preprocess_out_str;
    bool preprocess_ok = unique_shader->preprocess(
            &g_glslang_builtin_resource,
            spirv_t::DEFAULT_VERSION, ENoProfile, false, false,
            k_esh_messages,
            &preprocess_out_str,
            includer
        );
        if (!preprocess_ok) {
            err("failed to preprocess " + a_glsl_path_str + ": " + preprocess_out_str);
        }

        return includer.getIncludedFiles();
}


std::vector<uint32_t>
spirv_from_glsl_file(const std::string& a_file_path, uint32_t a_vulkan_api_version)
{
    auto spirv = spirv_from_glsl_src(
        file_to_str(a_file_path).c_str(),
        vk_shader_stage_from_file_name(a_file_path),
        a_vulkan_api_version,
        a_file_path.c_str()
    );
    return spirv;
}


std::vector<uint32_t> spock::
load_spirv_from_file(const std::filesystem::path& a_spirv_file_path)
{
    std::vector<uint32_t> spirv_bin;
    std::ifstream ifs(a_spirv_file_path, std::ios::in | std::ios::ate | std::ios::binary);
    ensure(ifs.is_open(), "failed to open spirv file " + a_spirv_file_path.string());

    auto spirv_bytes = static_cast<size_t>(ifs.tellg());
    ensure(spirv_bytes > 0, "spirv file " + a_spirv_file_path.string() + " is empty");
    ensure(spirv_bytes % 4 == 0, "invalid spirv file " + a_spirv_file_path.string());

    auto spirv_uint32_count = spirv_bytes / 4;
    spirv_bin.resize(spirv_uint32_count);
    ifs.seekg(0);
    ifs.read(reinterpret_cast<char*>(spirv_bin.data()), spirv_bytes);
    ifs.close();

    ensure(spirv_bin[0] == spirv_t::SPIRV_MAGIC_NUMBER, "spirv file " + a_spirv_file_path.string() + " is invalid");

    return spirv_bin;
}


bool spock::spirv_t::load_prebuilt_spirv()
{
    ensure(exists(spirv_file_path_), "invalid glsl and spirv path: " + glsl_file_path_.string() + "; " + spirv_file_path_.string());
    auto latest_spirv_file_timestamp = last_write_time(spirv_file_path_);
    if (!earlier_than(spirv_bin_timestamp_, latest_spirv_file_timestamp)) {
        verbose("spirv_bin_ is already in sync with " + spirv_file_path_.string());
        return false;
    }
    else {
        spirv_bin_ = load_spirv_from_file(spirv_file_path_);
        spirv_bin_timestamp_ = latest_spirv_file_timestamp;
        verbose("loaded spirv from file " + spirv_file_path_.string());
        return true;
    }
}


spock::spirv_t::
spirv_t(spirv_t&& a_spirv) noexcept
    : spirv_bin_                { std::move(a_spirv.spirv_bin_) } 
    , vulkan_api_version_       { std::exchange(a_spirv.vulkan_api_version_, 0) }
    , spirv_bin_timestamp_      { std::move(a_spirv.spirv_bin_timestamp_) }
    , shader_stage_flag_bits_   { std::exchange(a_spirv.shader_stage_flag_bits_, VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM) }
    , glsl_file_path_           { std::move(a_spirv.glsl_file_path_) }
    , spirv_file_path_          { std::move(a_spirv.spirv_file_path_) }
    , glsl_file_valid_          { std::exchange(a_spirv.glsl_file_valid_, false) }
    , initialized_              { std::exchange(a_spirv.initialized_, false) }
{}


spock::spirv_t& spock::spirv_t::
operator= (spirv_t&& a_spirv) noexcept
{
    if (this != &a_spirv) {
        spirv_bin_                = std::move(a_spirv.spirv_bin_);
        vulkan_api_version_       = std::exchange(a_spirv.vulkan_api_version_, 0);
        spirv_bin_timestamp_      = std::move(a_spirv.spirv_bin_timestamp_);
        shader_stage_flag_bits_   = std::exchange(a_spirv.shader_stage_flag_bits_, VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM);
        glsl_file_path_           = std::move(a_spirv.glsl_file_path_);
        spirv_file_path_          = std::move(a_spirv.spirv_file_path_);
        glsl_file_valid_          = std::exchange(a_spirv.glsl_file_valid_, false);
        initialized_              = std::exchange(a_spirv.initialized_, false);
    }
    return *this;
}


bool spock::spirv_t::
reload(bool a_force_reload, const std::vector<std::string>& a_glsl_inc_dirs)
{
    using namespace std::filesystem;
    if (!glsl_file_valid_) {
        return load_prebuilt_spirv();
    }

    const auto glsl_file_path_str = glsl_file_path_.string();
    ensure(exists(glsl_file_path_), "invalid glsl file path " + glsl_file_path_str + ": glsl file does not exist");
    const std::string glsl_src_src = file_to_str(glsl_file_path_str);
    char const* glsl_src_c_src = glsl_src_src.c_str();
    char const* glsl_file_path_c_str = glsl_file_path_str.c_str();

    const auto stage = glslang_stage_from_vk_flags(vk_shader_stage_from_file_name(glsl_file_path_.string()));
    auto unique_shader = make_glslang_shader(vulkan_api_version_, stage, &glsl_src_c_src, &glsl_file_path_c_str);
    DirStackFileIncluder includer = make_includer(a_glsl_inc_dirs);

    if (!a_force_reload) {
        std::set<std::string> glsl_inc_paths;
        std::string preprocess_out_str;
        bool preprocess_ok = unique_shader->preprocess(
            &g_glslang_builtin_resource,
            spirv_t::DEFAULT_VERSION, ENoProfile, false, false,
            k_esh_messages,
            &preprocess_out_str,
            includer
        );
        if (!preprocess_ok) {
            err("failed to preprocess " + glsl_file_path_str + ": " + preprocess_out_str);
            if (!initialized_) {
                glsl_file_valid_ = false;
                return load_prebuilt_spirv();
            }
            return false;
        }

        glsl_inc_paths = includer.getIncludedFiles();

        auto latest_glsl_file_timestamp = last_write_time(glsl_file_path_);
        for (const auto& p : glsl_inc_paths) {
            auto p_timestamp = last_write_time(p);
            if (!earlier_than(p_timestamp, latest_glsl_file_timestamp)) {
                latest_glsl_file_timestamp = p_timestamp;
            }
        }
        if (!earlier_than(spirv_bin_timestamp_, latest_glsl_file_timestamp)) {
            verbose("spirv for " + glsl_file_path_.string() + " is already up-to-date");
            return false;
        }
        if (exists(spirv_file_path_)) {
            auto spirv_file_timestamp = last_write_time(spirv_file_path_);
            if (!earlier_than(spirv_file_timestamp, latest_glsl_file_timestamp)) {
                spirv_bin_ = load_spirv_from_file(spirv_file_path_);
                spirv_bin_timestamp_ = spirv_file_timestamp;
                verbose("loaded spirv from file " + spirv_file_path_.string());
                return true;
            }
        }
    }

    std::vector<uint32_t> spirv;
    includer = make_includer(a_glsl_inc_dirs);
    bool parse_ok = unique_shader->parse(&g_glslang_builtin_resource, spock::spirv_t::DEFAULT_VERSION, false, k_esh_messages, includer);
    if (!parse_ok) {
        spock::err("failed to parse " + glsl_file_path_str);
        spock::err(std::string("\n") + unique_shader->getInfoLog());
        spock::err(std::string("\n") + unique_shader->getInfoDebugLog());
        return false;
    }
    if (build_spirv_from_shader(glsl_file_path_str, *unique_shader, spirv)) {
        spirv_bin_ = std::move(spirv);
        c_str_to_file(reinterpret_cast<const char*>(spirv_bin_.data()), sizeof(uint32_t) * spirv_bin_.size(), spirv_file_path_.string());
        spirv_bin_timestamp_ = last_write_time(spirv_file_path_);
        verbose("write compiled spirv to file " + spirv_file_path_.string());
        return true;
    }
    else {
        err("failed to build spirv from " + glsl_file_path_.string());
        return false;
    }

}


spock::spirv_t::
spirv_t
(
    const std::string & a_glsl_file_path_str, 
    uint32_t a_vulkan_api_version, 
    const std::string & a_spirv_dir_path_str, 
    const std::vector<std::string>& a_glsl_inc_dirs
)
    : shader_stage_flag_bits_{ VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM }
    , vulkan_api_version_{ a_vulkan_api_version }
    , glsl_file_path_( a_glsl_file_path_str )
    , glsl_file_valid_{ false }
    , initialized_{ false }

{
    using namespace std::filesystem;
    using namespace std::chrono_literals;

    auto glsl_file_name = glsl_file_path_.filename().string();
    ensure(!glsl_file_name.empty(), "invalid glsl file path " + glsl_file_path_.string() + ": glsl file name is empty");

    if (!a_spirv_dir_path_str.empty()) {
        if (!exists(a_spirv_dir_path_str)) {
            warn("creating spirv directory " + a_spirv_dir_path_str);
            create_directories(a_spirv_dir_path_str);
        }
        spirv_file_path_ = path(a_spirv_dir_path_str) / (glsl_file_name + SPIRV_FILE_EXTENSION);
    }
    else {
        spirv_file_path_ = path(a_glsl_file_path_str + SPIRV_FILE_EXTENSION);
    }

    const auto vk_stage = vk_shader_stage_from_file_name(glsl_file_path_.string());
    if (exists(glsl_file_path_)) {
        spirv_bin_timestamp_ = last_write_time(glsl_file_path_) - 24h;
        glsl_file_valid_ = true; // will be updated in reload(...) based on whether all included files are valid
    }
    else{
        ensure(exists(spirv_file_path_), "invalid glsl and spirv path: " + glsl_file_path_.string() + "; " + spirv_file_path_.string());
        spirv_bin_timestamp_ = last_write_time(spirv_file_path_) - 24h;
    }
    reload(false, a_glsl_inc_dirs);

    if (!glsl_file_valid_) {
        warn("glsl file " + glsl_file_path_.string() + " does not exist, won't be able to compile on the fly");
    }

    initialized_ = true;
    shader_stage_flag_bits_ = vk_stage;
}

VKAPI_ATTR VkBool32 VKAPI_CALL spock::
debug_utils_messenger_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT a_severity_flag,
    VkDebugUtilsMessageTypeFlagsEXT a_msg_type_flag,
    const VkDebugUtilsMessengerCallbackDataEXT* a_callback_data,
    void* a_user_data
) 
{
    std::string msg;
    const std::string indent(6, ' ');
    msg = str_from_VkDebugUtilsMessageSeverityFlagBitsEXT(a_severity_flag).data() + std::string(":\n")
        + indent + str_from_VkDebugUtilsMessageTypeFlagsEXT(a_msg_type_flag) 
        + " (id: " + std::to_string(a_callback_data->messageIdNumber) + ", name: " + a_callback_data->pMessageIdName + "):\n"
        + indent + a_callback_data->pMessage + '\n';

    spock::info(msg);
    return VK_FALSE;
}

} // namespace spock

