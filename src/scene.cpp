#include <spock/scene.hpp>
#include <spock/utils.hpp>
#include <spock/context.hpp>
#include <thread>
#include <execution>
#include <stb_image_write.h>
#include <rgbcx/rgbcx.hpp>
#include <tiny_gltf.h>
#include <glm/gtc/color_space.hpp>
#include <glm/gtc/vec1.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cstring>
#include <cmath>
#include <fstream>
#include <unordered_map>
#include <nlohmann/json.hpp>


namespace {

constexpr uint32_t rgbcx_qual = 6u;

void 
copy_strided(void* a_dst, size_t a_dst_byte_stride, const void* a_src, size_t a_src_byte_stride, size_t a_src_bytes)
{
    auto src = reinterpret_cast<const uint8_t*>(a_src);
    auto dst = reinterpret_cast<uint8_t*>(a_dst);
    if (a_src_byte_stride == a_dst_byte_stride) {
        std::copy(std::execution::par_unseq, src, src + a_src_bytes, dst);
    }
    else {
        auto copy_worker = [=](size_t a_begin_count, size_t a_end_count) {
            for (size_t i = a_begin_count; i < a_end_count; ++i) {
                memcpy(dst + i * a_dst_byte_stride, src + i * a_src_byte_stride, a_dst_byte_stride);
            }
        };
        const uint32_t num_threads = std::max(std::thread::hardware_concurrency(), 2u);
        const size_t total_count = a_src_bytes / a_src_byte_stride;
        const size_t batch_count = (total_count + num_threads - 1) / num_threads;
        std::vector<std::thread> thread_pool;
        thread_pool.reserve(num_threads);
        for (size_t i = 0; i < batch_count; ++i) {
            size_t begin_count = i * batch_count;
            size_t end_count = std::min(total_count, (i + 1) * batch_count);
            thread_pool.emplace_back(copy_worker, begin_count, end_count);
        }
        for (auto& t : thread_pool) t.join();
    }
}

/// a_index_count: always single-sided
void 
copy_indices(
    uint32_t* a_dst, 
    const void* a_src, size_t a_src_byte_stride, uint32_t a_index_count, 
    uint32_t a_vertex_offset, bool a_double_sided
)
{
    SPOCK_PARANOID(a_src != nullptr, "copy indices: a_src cannot be nullptr");
    SPOCK_PARANOID(a_index_count % 3 == 0, "copy indices: a_index_count must be multiple of 3");
    if (!a_double_sided) {
        if (a_src_byte_stride == sizeof(uint32_t)) {
            const uint32_t* src = static_cast<const uint32_t*>(a_src);
            std::transform(std::execution::par_unseq,
                src, src + a_index_count, a_dst,
                [a_vertex_offset](uint32_t i) {return i + a_vertex_offset; }
            );
        }
        else if (a_src_byte_stride == sizeof(uint16_t)) {
            const uint16_t* src = static_cast<const uint16_t*>(a_src);
            std::transform(std::execution::par_unseq,
                src, src + a_index_count, a_dst,
                [a_vertex_offset](uint16_t i) {return i + a_vertex_offset; }
            );
        }
        else if (a_src_byte_stride == sizeof(uint8_t)) {
            const uint8_t* src = static_cast<const uint8_t*>(a_src);
            std::transform(std::execution::par_unseq,
                src, src + a_index_count, a_dst,
                [a_vertex_offset](uint8_t i) {return i + a_vertex_offset; }
            );
        }
        else {
            spock::except("copy indices: invalid a_src_byte_stride");
        }
    }
    else { // double-sided
        using dst_type = std::array<glm::u32vec3, 2>;
        auto dst = reinterpret_cast<dst_type*>(a_dst);
        const uint32_t num_triangles = a_index_count / 3;

        if (a_src_byte_stride == sizeof(uint32_t)) {
            using src_type = glm::u32vec3;
            const src_type* src = static_cast<const src_type*>(a_src);
            std::transform(std::execution::par_unseq,
                src, src + num_triangles, dst,
                [a_vertex_offset](const src_type& s) {
                    const glm::u32vec3 d1 = glm::u32vec3(s) + glm::u32vec3(a_vertex_offset);
                    const glm::u32vec3 d2{ d1[2], d1[1], d1[0] };
                    return dst_type{ d1, d2 };
                }
            );
        }
        else if (a_src_byte_stride == sizeof(uint16_t)) {
            using src_type = glm::u16vec3;
            const src_type* src = static_cast<const src_type*>(a_src);
            std::transform(std::execution::par_unseq,
                src, src + num_triangles, dst,
                [a_vertex_offset](const src_type& s) {
                    const glm::u32vec3 d1 = glm::u32vec3(s) + glm::u32vec3(a_vertex_offset);
                    const glm::u32vec3 d2{ d1[2], d1[1], d1[0] };
                    return dst_type{ d1, d2 };
                }
            );
        }
        else if (a_src_byte_stride == sizeof(uint8_t)) {
            using src_type = glm::u8vec3;
            const src_type* src = static_cast<const src_type*>(a_src);
            std::transform(std::execution::par_unseq,
                src, src + num_triangles, dst,
                [a_vertex_offset](const src_type& s) {
                    const glm::u32vec3 d1 = glm::u32vec3(s) + glm::u32vec3(a_vertex_offset);
                    const glm::u32vec3 d2{ d1[2], d1[1], d1[0] };
                    return dst_type{ d1, d2 };
                }
            );
        }
        else {
            spock::except("copy indices: invalid a_src_byte_stride");
        }
    }

}


/// a_index_count: always single-sided
void iota_indices(uint32_t* a_dst, uint32_t a_index_count, uint32_t a_vertex_offset, bool a_double_sided)
{
    const uint32_t num_triangles = a_index_count / 3;
    if (!a_double_sided) {
        spock::for_indexed_each(a_dst, a_dst + a_index_count, [a_vertex_offset](uint32_t& idx, size_t i) {
            idx = i + a_vertex_offset;
        });
    }
    else {
        using dst_type = std::array<glm::u32vec3, 2>;
        auto dst = reinterpret_cast<dst_type*>(a_dst);
        spock::for_indexed_each(dst, dst + num_triangles, [a_vertex_offset](dst_type& dt, size_t i) {
            uint32_t i0 = static_cast<uint32_t>(i) * 3u + a_vertex_offset;
            dt[0] = glm::u32vec3(i0, i0 + 1, i0 + 2);
            dt[1] = glm::u32vec3(i0 + 2, i0 + 1, i0);
        }); 
    }

}


inline glm::i8vec2 
sf32_to_i8_vec2(const glm::vec2& f32_n)
{
    return glm::i8vec2{ spock::sf32_to_i8(f32_n.x), spock::sf32_to_i8(f32_n.y) };
}

inline glm::u8vec2 
sf32_to_u8_vec2(const glm::vec2& f32_n)
{
    return glm::u8vec2{ spock::sf32_to_u8(f32_n.x), spock::sf32_to_u8(f32_n.y) };
}

inline void
u8_to_uf32_linear_rgba_mip_2d(
    const glm::u8vec4* a_u8_texels, uint32_t a_num_texels, bool a_is_src_srgb, glm::vec4* a_uf32_texels)
{
    if (a_is_src_srgb) {
        std::transform(std::execution::par_unseq,
            a_u8_texels, a_u8_texels + a_num_texels, a_uf32_texels,
            [](const glm::u8vec4& u8_rgba) ->glm::vec4 {
                return glm::convertSRGBToLinear(spock::u8_to_uf32_vec4(u8_rgba));
            }
        );
    }
    else
    {
        std::transform(std::execution::par_unseq,
            a_u8_texels, a_u8_texels + a_num_texels, a_uf32_texels,
            [](const glm::u8vec4& u8_rgba) ->glm::vec4 {
                return spock::u8_to_uf32_vec4(u8_rgba);
            }
        );
    }
}


struct texel_block_info_t {
    uint8_t texel_block_bytes;
    glm::u8vec2 texel_block_dim;
    uint8_t is_compressed;
};

/// for uncompressed format: texel_block_bytes = num_channels, texel_block_dim_ = {1, 1} 
static const std::unordered_map<VkFormat, texel_block_info_t > k_format_texel_block_info_map = {
    { VK_FORMAT_R8G8_SNORM, { 2, {1, 1}, 0 } },
    { VK_FORMAT_R8G8_UNORM, { 2, {1, 1}, 0 } },

    { VK_FORMAT_R8G8B8_SNORM, { 3, {1, 1}, 0 } },
    { VK_FORMAT_R8G8B8_UNORM, { 3, {1, 1}, 0 } },

    { VK_FORMAT_R8G8B8A8_SRGB, { 4, {1, 1}, 0 } },
    { VK_FORMAT_R8G8B8A8_UNORM, { 4, {1, 1}, 0 } },

    { VK_FORMAT_R32G32B32_SFLOAT, { 12, {1, 1}, 0 } },
    { VK_FORMAT_R32G32B32A32_SFLOAT, { 16, {1, 1}, 0 } },

    { VK_FORMAT_BC1_RGB_UNORM_BLOCK, { 8, {4, 4}, 1 } },
    { VK_FORMAT_BC1_RGB_SRGB_BLOCK, { 8, {4, 4}, 1 } },
    { VK_FORMAT_BC1_RGBA_UNORM_BLOCK, { 8, {4, 4}, 1 } },
    { VK_FORMAT_BC1_RGBA_SRGB_BLOCK, { 8, {4, 4}, 1 } },

    { VK_FORMAT_BC2_UNORM_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC2_SRGB_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC3_UNORM_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC3_SRGB_BLOCK, { 16, {4, 4}, 1 } },

    { VK_FORMAT_BC4_UNORM_BLOCK, { 8, {4, 4}, 1 } },
    { VK_FORMAT_BC4_SNORM_BLOCK, { 8, {4, 4}, 1 } },

    { VK_FORMAT_BC5_UNORM_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC5_SNORM_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC6H_UFLOAT_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC6H_SFLOAT_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC7_UNORM_BLOCK, { 16, {4, 4}, 1 } },
    { VK_FORMAT_BC7_SRGB_BLOCK, { 16, {4, 4}, 1 } }
};


/// if a_mip0_occlusion_texels is nullptr, the R channel of a_mip0_metalrough_texels will be set to 1.f, otherwise
/// t_occlusion_texel:  glm::vec[1, 2, 3, 4]
/// t_metalrough_texel: glm::vec[3, 4]
/// R channel of a_mip0_occlusion_texels will be copied to that of a_mip0_metalrough_texels
template<typename t_occlusion_texel, typename t_metalrough_texel> inline void
merge_mip0_occlusion_into_metalrough_texture(
    t_metalrough_texel* a_mip0_metalrough_texels, 
    uint32_t a_num_texels,
    const t_occlusion_texel* a_mip0_occlusion_texels = nullptr
)
{
    static_assert(
        sizeof(t_metalrough_texel) / sizeof(t_metalrough_texel::r) >= 3, 
        "t_metalrough_texel must have at least 3 channels"
    );

    if (a_mip0_occlusion_texels == nullptr) {
        std::for_each(std::execution::par_unseq,
            a_mip0_metalrough_texels, a_mip0_metalrough_texels + a_num_texels,
            [](t_metalrough_texel& a_metalrough) { a_metalrough.r = 1.f; }
        );
    }
    else {
        std::transform(std::execution::par_unseq,
            a_mip0_metalrough_texels, a_mip0_metalrough_texels + a_num_texels,
            a_mip0_occlusion_texels, a_mip0_occlusion_texels + a_num_texels,
            a_mip0_metalrough_texels,
            [](const t_metalrough_texel& a_metalrough, const t_occlusion_texel& a_occlusion) -> t_metalrough_texel {
                auto occlusion_metalrough = a_metalrough;
                occlusion_metalrough.r = a_occlusion.r;
                return occlusion_metalrough;
            }
        );
    }
}


/// a_texels: pre-allocated memory for all mip levels with mip0 texels initialized
/// t_fn_box_filter(const t_texel& t_00, const t_texel& t_10, const t_texel& t_01, const t_texel& t_11) -> t_texel;
template<typename t_texel, typename t_fn_box_filter> inline void
gen_mipmap_2d_with_box_filter(
    t_texel* a_texels, 
    const std::vector<VkExtent2D>& a_mip_dims, const std::vector<uint32_t>& a_mip_texel_offsets, 
    t_fn_box_filter&& a_fn_box_filter
)
{
    SPOCK_PARANOID(
        a_mip_texel_offsets == spock::calc_mip_texel_offsets(a_mip_dims),
        "gen_mipmap_2d_with_box_filter: incompatible a_mip_texel_offsets and a_mip_dims"
    );

    uint32_t num_mips = static_cast<uint32_t>(a_mip_dims.size());
    auto global_texel_offset_from_local_coord = [&a_mip_texel_offsets, &a_mip_dims]
        (uint32_t a_x, uint32_t a_y, uint32_t a_mip_level) -> uint32_t
    {
        const auto dim = a_mip_dims[a_mip_level];
        // SPOCK_PARANOID(a_y < extent.height&& a_x < extent.width, "mip coord out of bounds");
        // in case only one dimension is already down to 1, that dimension will be duplicated
        uint32_t x = std::clamp(a_x, 0u, dim.width - 1);
        uint32_t y = std::clamp(a_y, 0u, dim.height - 1);

        return a_mip_texel_offsets[a_mip_level] + dim.width * y + x;
    };

    auto gen_mip_worker =
        [&](uint32_t a_y_begin, uint32_t a_y_end, uint32_t a_mip_level)
    {
        SPOCK_PARANOID(a_mip_level != 0, "should not generate mipmap for mip level 0");

        const auto prev_mip_level = a_mip_level - 1u;
        const uint32_t width = a_mip_dims[a_mip_level].width;

        for (uint32_t y = a_y_begin; y < a_y_end; ++y) {
            for (uint32_t x = 0u; x < width; ++x) {
                auto next_idx = global_texel_offset_from_local_coord(x, y, a_mip_level);
                auto prev_00_idx = global_texel_offset_from_local_coord(2 * x + 0, 2 * y + 0, prev_mip_level);
                auto prev_10_idx = global_texel_offset_from_local_coord(2 * x + 1, 2 * y + 0, prev_mip_level);
                auto prev_01_idx = global_texel_offset_from_local_coord(2 * x + 0, 2 * y + 1, prev_mip_level);
                auto prev_11_idx = global_texel_offset_from_local_coord(2 * x + 1, 2 * y + 1, prev_mip_level);

                a_texels[next_idx] = a_fn_box_filter(
                    a_texels[prev_00_idx], a_texels[prev_01_idx], a_texels[prev_10_idx], a_texels[prev_11_idx]
                );
            }
        }
    };

    spock::host_timer_t timer;

    const uint32_t num_threads = std::max(std::thread::hardware_concurrency(), 2u);
    std::vector<std::thread> thread_pool;
    thread_pool.reserve(num_threads);
    for (uint32_t mip_level = 1; mip_level < num_mips; ++mip_level) {
        const uint32_t height = a_mip_dims[mip_level].height;
        const uint32_t num_rows_per_thread = (height + num_threads - 1) / num_threads;
        if (num_rows_per_thread < 2) {
            gen_mip_worker(0, height, mip_level);
        }
        else {
            for (uint32_t tid = 0; tid < num_threads; ++tid) {
                const auto y_begin = tid * num_rows_per_thread;
                const auto y_end = std::min(height, (tid + 1) * num_rows_per_thread);
                thread_pool.emplace_back(gen_mip_worker, y_begin, y_end, mip_level);
            }
            for (auto& t : thread_pool) {
                t.join();
            }
            thread_pool.clear();
        }
    }
    spock::verbose("cpu gen mipmap took " + std::to_string(timer.elapsed_ms()) + " ms");
}

/// t_fn_src_to_flt_texel(const t_src_texel&) -> t_flt_texel;
/// t_fn_box_filter:
///     (const t_flt_texel& t_00, const t_flt_texel& t_10, const t_flt_texel& t_01, const t_flt_texel& t_11)->t_flt_texel;
/// t_fn_flt_texel(const t_flt_texel&) -> t_dst_texel;
template<typename t_src_texel, typename t_flt_texel, typename t_dst_texel, 
    typename t_fn_src_to_flt_texel, typename t_fn_box_filter, typename t_fn_flt_to_dst_texel> 
std::unique_ptr<t_dst_texel[]>
convert_and_gen_mipmap_with_box_filter(
    const t_src_texel* a_src_mip0_texels, const std::vector<VkExtent2D>& a_mip_dims,
    t_fn_src_to_flt_texel&& a_fn_src_to_flt_texel,
    t_fn_box_filter&& a_fn_box_filter,
    t_fn_flt_to_dst_texel&& a_fn_flt_to_dst_texel
)
{
    auto mip_texel_offsets = spock::calc_mip_texel_offsets(a_mip_dims);
    const uint32_t num_mip_texels = mip_texel_offsets.back();
    const uint32_t num_mip0_texels = mip_texel_offsets[1];

    spock::host_timer_t timer;

    std::unique_ptr<t_dst_texel[]> unique_dst_texels(new t_dst_texel[num_mip_texels]);
    t_dst_texel* dst_texels = unique_dst_texels.get();
    std::unique_ptr<t_flt_texel[]> unique_flt_texels(new t_flt_texel[num_mip_texels]);
    t_flt_texel* flt_texels = unique_flt_texels.get();

    spock::verbose("alloc flt and dst texels took "+ std::to_string(timer.elapsed_ms()) + " ms");
    timer.start();

    std::transform(std::execution::par_unseq,
        a_src_mip0_texels, a_src_mip0_texels + num_mip0_texels, flt_texels, 
        a_fn_src_to_flt_texel
    );

    spock::verbose("from src mip0 to flt full map took " + std::to_string(timer.elapsed_ms()) + " ms");

    gen_mipmap_2d_with_box_filter(flt_texels, a_mip_dims, mip_texel_offsets, a_fn_box_filter);

    timer.start();

    std::transform(std::execution::par_unseq,
        flt_texels, flt_texels + num_mip_texels, dst_texels,
        a_fn_flt_to_dst_texel
    );

    spock::verbose("full mip from flt to dst texels took " + std::to_string(timer.elapsed_ms()) + " ms");

    return unique_dst_texels;
}


// signature of fn_compress_block: void (uint8_t* out, const uint8_t* in)
template<typename t_fn_compress_block> std::unique_ptr<uint8_t[]>
compress_texture_2d(
    const spock::mip_2d_info_t& a_src_mip_2d_info, const spock::mip_2d_layout_t& a_src_mip_2d_layout, 
    const uint8_t* a_src_mip_buffer,
    const spock::mip_2d_info_t& a_dst_mip_2d_info, const spock::mip_2d_layout_t& a_dst_mip_2d_layout,
    t_fn_compress_block&& fn_compress_block
)
{
    const auto num_mips = static_cast<uint32_t>(a_src_mip_2d_layout.dims.size());
    const uint32_t src_num_channels = spock::get_texel_block_bytes(a_src_mip_2d_info.format());

    SPOCK_PARANOID(
        a_src_mip_2d_info.calc_mip_2d_layout() == a_src_mip_2d_layout,
        "compress_texture_2d: incompatible a_src_mip_2d_info and a_src_mip_2d_layout"
    );
    SPOCK_PARANOID(
        !spock::is_texel_block_compressed(a_src_mip_2d_info.format()),
        std::string("compress_texture_2d: a_src_mip_2d_info.format cannot be compressed format: ") + spock::str_from_VkFormat(a_src_mip_2d_info.format()).data()
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_info.calc_mip_2d_layout() == a_dst_mip_2d_layout,
        "compress_texture_2d: incompatible a_dst_mip_2d_info and a_dst_mip_2d_layout"
    );
    SPOCK_PARANOID(
        spock::is_texel_block_compressed(a_dst_mip_2d_info.format()),
        std::string("compress_texture_2d: a_dst_mip_2d_info.format cannot be compressed format: ") + spock::str_from_VkFormat(a_src_mip_2d_info.format()).data()
    );

    spock::host_timer_t timer;

    rgbcx::init();

    const auto dst_mip_2d_buffer_bytes = a_dst_mip_2d_layout.byte_offsets.back();
    std::unique_ptr<uint8_t[]> dst_mip_2d_buffer{ new uint8_t[dst_mip_2d_buffer_bytes] };

    std::vector<uint32_t> dst_block_counts;
    dst_block_counts.reserve(num_mips);
    const auto dst_texel_block_bytes = spock::get_texel_block_bytes(a_dst_mip_2d_info.format());
    const uint32_t dst_total_num_blocks = dst_mip_2d_buffer_bytes / dst_texel_block_bytes;

    SPOCK_PARANOID(dst_mip_2d_buffer_bytes % dst_texel_block_bytes == 0, "dst_mip_2d_buffer_bytes is invalid");

    std::transform(
        a_dst_mip_2d_layout.byte_offsets.begin() + 1, a_dst_mip_2d_layout.byte_offsets.end(),
        a_dst_mip_2d_layout.byte_offsets.begin(),
        std::back_inserter(dst_block_counts),
        [dst_texel_block_bytes](VkDeviceSize offset_1, VkDeviceSize offset_0)-> uint32_t {
            return static_cast<uint32_t>(offset_1 - offset_0) / dst_texel_block_bytes;
        }
    );

    SPOCK_PARANOID(
        dst_total_num_blocks == std::reduce(dst_block_counts.begin(), dst_block_counts.end()),
        "bugs in texel block counts calculation"
    );

    const uint32_t num_threads = std::min(std::max(std::thread::hardware_concurrency(), 2u), dst_total_num_blocks);
    const auto per_thread_num_blocks = (dst_total_num_blocks + num_threads - 1) / num_threads;

    struct block_range_t {
        uint32_t mip_level;
        uint32_t global_block_offset;
        uint32_t local_block_offset;
        uint32_t num_blocks;
    };

    std::vector< std::vector<block_range_t> > per_thread_block_ranges(num_threads, std::vector<block_range_t>{});
    uint32_t curr_mip_level = 0;
    uint32_t curr_local_block_offset = 0;
    uint32_t total_assigned_num_blocks = 0;

    for (uint32_t i = 0; i < num_threads; ++i) {
        uint32_t thread_assigned_num_blocks = 0;
        do {
            auto num_available_blocks = std::min(
                per_thread_num_blocks - thread_assigned_num_blocks,
                dst_block_counts[curr_mip_level] - curr_local_block_offset
            );

            per_thread_block_ranges[i].push_back(
                block_range_t{ curr_mip_level, total_assigned_num_blocks, curr_local_block_offset, num_available_blocks }
            );
            thread_assigned_num_blocks += num_available_blocks;
            total_assigned_num_blocks += num_available_blocks;

            curr_local_block_offset += num_available_blocks;
            bool curr_mip_done = (curr_local_block_offset == dst_block_counts[curr_mip_level]);
            curr_mip_level += curr_mip_done;
            curr_local_block_offset = (1 - curr_mip_done) * curr_local_block_offset;

            if (total_assigned_num_blocks == dst_total_num_blocks) {
                break;
            }
        } while (thread_assigned_num_blocks < per_thread_num_blocks);
    }

    const auto dst_texel_block_dim = spock::get_texel_block_dim(a_dst_mip_2d_info.format());
    auto calc_top_left_texel_idx = 
        [&block_dim = std::as_const(dst_texel_block_dim)]
        (uint32_t a_h_num_blocks, uint32_t a_block_local_offset) -> glm::uvec2 {
            glm::uvec2 result{ 0, 0 };
            result.x = block_dim.x * (a_block_local_offset % a_h_num_blocks);
            result.y = block_dim.y * (a_block_local_offset / a_h_num_blocks);
            return result;
        };

    auto compress_worker = [&](const std::vector<block_range_t>& a_block_ranges) {
        const uint32_t src_array_bytes = src_num_channels * dst_texel_block_dim.x * dst_texel_block_dim.y;
        std::unique_ptr<uint8_t[]> src_array{ new uint8_t[src_array_bytes] };
        
        for (const block_range_t& block_range : a_block_ranges) {
            const auto h_num_blocks = a_dst_mip_2d_layout.padded_dims[block_range.mip_level].width / dst_texel_block_dim.x;
            const uint32_t src_mip_w = a_src_mip_2d_layout.dims[block_range.mip_level].width;
            const uint32_t src_mip_h = a_src_mip_2d_layout.dims[block_range.mip_level].height;
            const VkDeviceSize src_mip_byte_offset = a_src_mip_2d_layout.byte_offsets[block_range.mip_level];

            for (uint32_t i = 0; i < block_range.num_blocks; ++i) {
                VkDeviceSize global_block_offset = block_range.global_block_offset + i;
                VkDeviceSize dst_mip_byte_offset = global_block_offset * dst_texel_block_bytes;

                uint32_t local_block_offset = block_range.local_block_offset + i;
                auto top_left_texel_idx = calc_top_left_texel_idx(h_num_blocks, local_block_offset);

                for (uint32_t dy = 0; dy < dst_texel_block_dim.y; ++dy) {
                    for (uint32_t dx = 0; dx < dst_texel_block_dim.x; ++dx) {
                        const auto texel_idx = top_left_texel_idx + glm::uvec2(dx, dy);
                        const VkDeviceSize src_array_offset = src_num_channels * (dy * dst_texel_block_dim.x + dx);
                        if (texel_idx.x < src_mip_w && texel_idx.y < src_mip_h) {
                            auto global_src_offset = src_mip_byte_offset + src_num_channels * (texel_idx.y * src_mip_w + texel_idx.x);
                            for (VkDeviceSize ch = 0; ch < src_num_channels; ++ch) {
                                src_array[src_array_offset + ch] = a_src_mip_buffer[global_src_offset + ch];
                            }
                        }
                        else {
                            for (VkDeviceSize ch = 0; ch < src_num_channels; ++ch) {
                                src_array[src_array_offset + ch] = 0;
                            }
                        }
                    }
                }
                fn_compress_block(dst_mip_2d_buffer.get() + dst_mip_byte_offset, src_array.get());
            }
        }
    };

    std::vector<std::thread> workers;
    for (const auto& block_ranges : per_thread_block_ranges) {
        workers.emplace_back(compress_worker, block_ranges);
    }
    for (auto& worker : workers) {
        worker.join();
    }

    spock::verbose("cpu block compression took " + std::to_string(timer.elapsed_ms()) + " ms");

    return dst_mip_2d_buffer;
}


spock::directional_light_t directional_light_from_gltf(const tinygltf::Light& a_gltf_light)
{
    SPOCK_PARANOID(a_gltf_light.type == "directional", "cannot convert light of type " + a_gltf_light.type + " to directional_light");
    spock::directional_light_t directional_light;
    directional_light.intensity = glm::vec3(a_gltf_light.intensity);
    directional_light.direction = glm::vec3{ 0.f, 0.f, -1.f }; // will be transformed while handling gltf nodes
    if (a_gltf_light.color.size() == 3) {
        directional_light.intensity *= glm::vec3(glm::make_vec3(a_gltf_light.color.data()));
    }
    return directional_light;
}


spock::point_light_t point_light_from_gltf(const tinygltf::Light& a_gltf_light)
{
    SPOCK_PARANOID(a_gltf_light.type == "point", "cannot convert light of type " + a_gltf_light.type + " to point_light");
    spock::point_light_t point_light;
    point_light.position = glm::vec3{ 0.f, 0.f, 0.f }; // will be transformed while handling gltf nodes
    point_light.intensity = glm::vec3(a_gltf_light.intensity);
    if (a_gltf_light.color.size() == 3) {
        point_light.intensity *= glm::make_vec3(a_gltf_light.color.data());
    }
    if (a_gltf_light.range > 0.f) {
        point_light.rcp_range = 1.f / a_gltf_light.range;
    }
    return point_light;
}


spock::spotlight_t spotlight_from_gltf(const tinygltf::Light& a_gltf_light)
{
    SPOCK_PARANOID(a_gltf_light.type == "spot", "cannot convert light of type " + a_gltf_light.type + " to spotlight");
    spock::spotlight_t spotlight;
    spotlight.position = glm::vec3{ 0.f, 0.f, 0.f }; // will be transformed while handling gltf nodes
    spotlight.direction = glm::vec3{ 0.f, 0.f, -1.f };
    spotlight.intensity = glm::vec3(a_gltf_light.intensity);
    if (a_gltf_light.color.size() == 3) {
        spotlight.intensity *= glm::vec3(glm::make_vec3(a_gltf_light.color.data()));
    }
    if (a_gltf_light.range > 0.f) {
        spotlight.rcp_range = 1.f / a_gltf_light.range;
    }
    const float inner = a_gltf_light.spot.innerConeAngle;
    const float outer = a_gltf_light.spot.outerConeAngle;
    std::tie(spotlight.angle_scale, spotlight.angle_offset) = spock::calc_spotlight_falloff_params(inner, outer);

    return spotlight;
}


std::string short_str_from_punctual_light_type(spock::punctual_light_type_e e)
{
    const std::string prefix_str = "PUNCTUAL_LIGHT_TYPE_";
    return std::string(str_from_punctual_light_type_e(e)).substr(prefix_str.size());
}


std::string short_str_from_mtl_type(spock::mtl_type_e e)
{
    const std::string prefix_str = "MTL_TYPE_";
    return std::string(str_from_mtl_type_e(e)).substr(prefix_str.size());
}


template <uint32_t BYTES> std::array<uint8_t, BYTES>
serialize_pod(const void* this_ptr) 
{
    std::array<uint8_t, BYTES> buffer;
    const uint8_t* src = reinterpret_cast<const uint8_t*>(this_ptr);
    std::copy(src, src + BYTES, buffer.data());
    return buffer;
}


template <uint32_t BYTES> void
deserialize_pod(uint8_t** a_bin_stream, void* this_ptr)
{
    uint8_t* dst = reinterpret_cast<uint8_t*>(this_ptr);
    const uint8_t* src = *a_bin_stream;
    std::copy(src, src + BYTES, dst);
    *a_bin_stream += BYTES;
}


static const std::unordered_map<int32_t, VkFilter> k_gltf_vk_filter_map = {
    { TINYGLTF_TEXTURE_FILTER_NEAREST,                  VK_FILTER_NEAREST },
    { TINYGLTF_TEXTURE_FILTER_LINEAR,                   VK_FILTER_LINEAR },
    { TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST,   VK_FILTER_NEAREST },
    { TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST,    VK_FILTER_LINEAR },
    { TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR,    VK_FILTER_NEAREST },
    { TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_LINEAR,     VK_FILTER_LINEAR },
};

static const std::unordered_map<int32_t, VkSamplerMipmapMode> k_gltf_vk_sampler_mipmap_mode_map = {
    { TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST,   VK_SAMPLER_MIPMAP_MODE_NEAREST },
    { TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST,    VK_SAMPLER_MIPMAP_MODE_NEAREST },
    { TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR,    VK_SAMPLER_MIPMAP_MODE_LINEAR },
    { TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_LINEAR,     VK_SAMPLER_MIPMAP_MODE_LINEAR },
};

static const std::unordered_map<int32_t, VkSamplerAddressMode> k_gltf_vk_sampler_address_mode_map = {
    { TINYGLTF_TEXTURE_WRAP_REPEAT,             VK_SAMPLER_ADDRESS_MODE_REPEAT },
    { TINYGLTF_TEXTURE_WRAP_CLAMP_TO_EDGE,      VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE },
    { TINYGLTF_TEXTURE_WRAP_MIRRORED_REPEAT,    VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT }
};

spock::sampler_2d_info_t
sampler_2d_info_from_gltf_sampler(const tinygltf::Sampler& a_gltf_sampler)
{
    spock::sampler_2d_info_t sampler_2d_info;
    const auto i_min_filter = k_gltf_vk_filter_map.find(a_gltf_sampler.minFilter);
    if (i_min_filter != k_gltf_vk_filter_map.end()) {
        sampler_2d_info.min_filter(i_min_filter->second);
    }
    const auto i_mag_filter = k_gltf_vk_filter_map.find(a_gltf_sampler.magFilter);
    if (i_mag_filter != k_gltf_vk_filter_map.end()) {
        sampler_2d_info.min_filter(i_mag_filter->second);
    }
    const auto i_mipmap_mode = k_gltf_vk_sampler_mipmap_mode_map.find(a_gltf_sampler.minFilter);
    if (i_mipmap_mode != k_gltf_vk_sampler_mipmap_mode_map.end()) {
        sampler_2d_info.mipmap_mode(i_mipmap_mode->second);
    }
    const auto i_u_addr_mode = k_gltf_vk_sampler_address_mode_map.find(a_gltf_sampler.wrapS);
    if (i_u_addr_mode != k_gltf_vk_sampler_address_mode_map.end()) {
        sampler_2d_info.u_address_mode(i_u_addr_mode->second);
    }
    const auto i_v_addr_mode = k_gltf_vk_sampler_address_mode_map.find(a_gltf_sampler.wrapT);
    if (i_v_addr_mode != k_gltf_vk_sampler_address_mode_map.end()) {
        sampler_2d_info.v_address_mode(i_v_addr_mode->second);
    }
    return sampler_2d_info;
}


template<typename T> inline glm::vec3
to_glm_vec3(const std::vector<T>& v) 
{
    SPOCK_PARANOID(v.size() >= 3, "too few components in the input vector for glm::vec3");
    return glm::vec3{ v[0], v[1], v[2] };
}

std::vector<tinygltf::Model> 
parse_gltf_models(const std::vector<std::string>& a_gltf_paths)
{
    using namespace std::filesystem;

    std::vector<tinygltf::Model> gltf_models(a_gltf_paths.size(), tinygltf::Model{});
    tinygltf::Model* gltf_model_ptr = gltf_models.data();

    for (const auto& gltf_path : a_gltf_paths) {
        tinygltf::TinyGLTF loader;
        spock::ensure(!loader.GetPreserveImageChannels(), "tinygltf loader must expand texture images to 4 channels (rgba)");

        std::string gltf_err_msg;
        std::string gltf_warn_msg;
        auto ext = path(gltf_path).extension().string();
        bool loaded = false;
        if (ext == ".glb") {
            loaded = loader.LoadBinaryFromFile(gltf_model_ptr, &gltf_err_msg, &gltf_warn_msg, gltf_path.data());
        }
        else {
            loaded = loader.LoadASCIIFromFile(gltf_model_ptr, &gltf_err_msg, &gltf_warn_msg, gltf_path.data());
        }
        if (!gltf_err_msg.empty()) {
            spock::err("Error parsing gltf " + gltf_path + ": \n" + gltf_err_msg);
        }
        if (!gltf_warn_msg.empty()) {
            spock::warn("Warning parsing gltf " + gltf_path + ": \n" + gltf_warn_msg);
        }
        spock::ensure(loaded, "failed to load gltf " + gltf_path);

        ++gltf_model_ptr;
    }
    return gltf_models;
}


static const std::unordered_map<int32_t, const char*> k_gltf_primitive_mode_str_map = {
    {TINYGLTF_MODE_POINTS, "GLTF_MODE_POINTS"},
    {TINYGLTF_MODE_LINE, "GLTF_MODE_LINE"},
    {TINYGLTF_MODE_LINE_LOOP, "GLTF_MODE_LINE_LOOP"},
    {TINYGLTF_MODE_LINE_STRIP, "GLTF_MODE_LINE_STRIP"},
    {TINYGLTF_MODE_TRIANGLES, "GLTF_MODE_TRIANGLES"},
    {TINYGLTF_MODE_TRIANGLE_STRIP, "GLTF_MODE_TRIANGLE_STRIP"},
    {TINYGLTF_MODE_TRIANGLE_FAN, "GLTF_MODE_TRIANGLE_FAN"}
};


inline const char* string_gltf_primitive_mode(int32_t a_gltf_primitive_mode)
{
    const auto i = k_gltf_primitive_mode_str_map.find(a_gltf_primitive_mode);
    if (i == k_gltf_primitive_mode_str_map.end()) {
        return "unknown gltf primitive mode";
    }
    return i->second;
}


// a_winding: 1 -> ccw, -1 -> cw
void handle_gltf_mesh(
    const tinygltf::Model& a_gltf_model, const tinygltf::Mesh& a_gltf_mesh, const std::string& a_mesh_msg_head, 
    int32_t a_winding, int32_t a_curr_transform_idx, const std::vector<spock::mtl_cfg_t>* a_mtl_cfgs,
    int32_t a_mtl_global_offset, const std::pair<spock::mtl_type_e, int32_t>& a_default_mtl_type_instance_idx,
    std::vector<std::pair<spock::mtl_type_e, int32_t>>& a_mtl_type_instance_idx_pairs,
    std::vector<spock::primitive_info_t>& a_primitive_infos, uint32_t* a_vertex_counts, uint32_t* a_index_counts
)
{
    spock::primitive_info_t primitive_info;
    primitive_info.winding = a_winding;
    primitive_info.transform_idx = a_curr_transform_idx;
    for (size_t ip = 0; ip < a_gltf_mesh.primitives.size(); ++ip) {
        const std::string primitive_msg_head = a_mesh_msg_head + ": primitive " + std::to_string(ip);
        primitive_info.name = primitive_msg_head;
        const auto& gltf_primitive = a_gltf_mesh.primitives[ip];
        if (gltf_primitive.mode != TINYGLTF_MODE_TRIANGLES) {
            spock::except(primitive_msg_head + ": topology type " + string_gltf_primitive_mode(gltf_primitive.mode) + ": is not supported");
        }
        // mtl
        if (gltf_primitive.material > -1) {
            const auto global_mtl_idx = a_mtl_global_offset + gltf_primitive.material;
            std::tie(primitive_info.mtl_type, primitive_info.mtl_instance_idx) = a_mtl_type_instance_idx_pairs[global_mtl_idx];
        }
        else {
            std::tie(primitive_info.mtl_type, primitive_info.mtl_instance_idx) = a_default_mtl_type_instance_idx;
        }
        primitive_info.double_sided = a_mtl_cfgs[primitive_info.mtl_type][primitive_info.mtl_instance_idx].double_sided;
        // position buffer
        auto i_position = gltf_primitive.attributes.find("POSITION");
        if (i_position == gltf_primitive.attributes.end()) {
            spock::except(primitive_msg_head + " has no position attribute");
        }
        const auto& gltf_position_accessor = a_gltf_model.accessors[i_position->second];
        if (gltf_position_accessor.sparse.isSparse) {
            spock::except(primitive_msg_head + " has sparse position buffer accessor, which is not supported");
        }
        const auto& gltf_position_buffer_view = a_gltf_model.bufferViews[gltf_position_accessor.bufferView];
        const auto gltf_position_buffer_offset = gltf_position_accessor.byteOffset + gltf_position_buffer_view.byteOffset;
        const auto gltf_position_buffer_ptr = a_gltf_model.buffers[gltf_position_buffer_view.buffer].data.data();
        primitive_info.position_buffer_ptr = reinterpret_cast<const float*>(gltf_position_buffer_ptr + gltf_position_buffer_offset);
        primitive_info.num_vertices = static_cast<uint32_t>(gltf_position_accessor.count);
        primitive_info.position_byte_stride = gltf_position_accessor.ByteStride(gltf_position_buffer_view);
        if (primitive_info.position_byte_stride == -1) {
            spock::except(primitive_msg_head + ": position buffer has incorrect stride information");
        }

        // normal
        auto i_normal = gltf_primitive.attributes.find("NORMAL");
        if (i_normal != gltf_primitive.attributes.end()) {
            const auto& gltf_normal_accessor = a_gltf_model.accessors[i_normal->second];
            if (gltf_normal_accessor.sparse.isSparse) {
                spock::except(primitive_msg_head + " has sparse normal buffer accessor, which is not supported");
            }
            const auto& gltf_normal_buffer_view = a_gltf_model.bufferViews[gltf_normal_accessor.bufferView];
            const auto gltf_normal_buffer_offset = gltf_normal_accessor.byteOffset + gltf_normal_buffer_view.byteOffset;
            const auto gltf_normal_buffer_ptr = a_gltf_model.buffers[gltf_normal_buffer_view.buffer].data.data();
            primitive_info.normal_buffer_ptr = reinterpret_cast<const float*>(gltf_normal_buffer_ptr + gltf_normal_buffer_offset);
            primitive_info.normal_byte_stride = gltf_normal_accessor.ByteStride(gltf_normal_buffer_view);
            if (primitive_info.normal_byte_stride == -1) {
                spock::except(primitive_msg_head + ": normal buffer has incorrect stride information");
            }
            if (primitive_info.num_vertices != static_cast<uint32_t>(gltf_normal_accessor.count)) {
                spock::except(primitive_msg_head + ": " + std::to_string(primitive_info.num_vertices) + " positions != " + std::to_string(gltf_normal_accessor.count) + " normals");
            }
        }
        else {
            spock::warn(primitive_msg_head + ": normals will be calculated");
        }
        // uv0
        auto i_uv0 = gltf_primitive.attributes.find("TEXCOORD_0");
        if (i_uv0 != gltf_primitive.attributes.end()) {
            const auto& gltf_uv0_accessor = a_gltf_model.accessors[i_uv0->second];
            if (gltf_uv0_accessor.sparse.isSparse) {
                spock::except(primitive_msg_head + ": sparse uv0 buffer accessor, which is not supported");
            }
            const auto& gltf_uv0_buffer_view = a_gltf_model.bufferViews[gltf_uv0_accessor.bufferView];
            const auto gltf_uv0_buffer_offset = gltf_uv0_accessor.byteOffset + gltf_uv0_buffer_view.byteOffset;
            const auto gltf_uv0_buffer_ptr = a_gltf_model.buffers[gltf_uv0_buffer_view.buffer].data.data();
            primitive_info.uv_buffer_ptr = reinterpret_cast<const float*>(gltf_uv0_buffer_ptr + gltf_uv0_buffer_offset);
            primitive_info.uv_byte_stride = gltf_uv0_accessor.ByteStride(gltf_uv0_buffer_view);
            if (primitive_info.uv_byte_stride == -1) {
                spock::except(primitive_msg_head + ": uv0 buffer has incorrect stride information");
            }
            if (primitive_info.num_vertices != static_cast<uint32_t>(gltf_uv0_accessor.count)) {
                spock::except(primitive_msg_head + ": " + std::to_string(primitive_info.num_vertices) + " positions != " + std::to_string(gltf_uv0_accessor.count) + " uv0s");
            }
        }
        else {
            spock::verbose("will generate dummy uv0 for " + primitive_msg_head);
        }
        a_vertex_counts[primitive_info.mtl_type] += primitive_info.num_vertices;

        // not supported features
        if (gltf_primitive.attributes.find("TEXCOORD_1") != gltf_primitive.attributes.end()) {
            spock::warn(primitive_msg_head + ": ignore TEXCOORD_1");
        }
        if (gltf_primitive.attributes.find("JOINTS_0") != gltf_primitive.attributes.end()) {
            spock::warn(primitive_msg_head + ": ignore JOINTS_0");
        }
        if (gltf_primitive.attributes.find("WEIGHTS_0") != gltf_primitive.attributes.end()) {
            spock::warn(primitive_msg_head + ": ignore WEIGHTS_0");
        }

        // index
        if (gltf_primitive.indices > -1) {
            const auto& gltf_index_accessor = a_gltf_model.accessors[gltf_primitive.indices];
            const auto& gltf_index_buffer_view = a_gltf_model.bufferViews[gltf_index_accessor.bufferView];
            const auto gltf_index_buffer_offset = gltf_index_accessor.byteOffset + gltf_index_buffer_view.byteOffset;
            const auto gltf_index_buffer_ptr = a_gltf_model.buffers[gltf_index_buffer_view.buffer].data.data();
            primitive_info.index_buffer_ptr = gltf_index_buffer_ptr + gltf_index_buffer_offset;
            primitive_info.num_indices = static_cast<uint32_t>(gltf_index_accessor.count);
            primitive_info.index_byte_stride = tinygltf::GetComponentSizeInBytes(gltf_index_accessor.componentType);
            if (primitive_info.index_byte_stride == -1) {
                spock::except(primitive_msg_head + ": index buffer has incorrect stride information");
            }
        }
        else {
            primitive_info.num_indices = primitive_info.num_vertices;
            // generate indices later
            spock::warn(primitive_msg_head + ": index buffer will be generated");
        }
        spock::ensure(a_index_counts[primitive_info.mtl_type] % 3 == 0, primitive_msg_head + ":  index count is not multiple of 3");

        // duplicate indices (reversed) for double-sided
        a_index_counts[primitive_info.mtl_type] += primitive_info.num_indices * (1 + primitive_info.double_sided);
        a_primitive_infos.push_back(primitive_info);

    } // loop over primitives of each node mesh
}


static const std::unordered_map<spock::mtl_type_e, uint32_t> k_mtl_type_texture_stride_map {
    {spock::MTL_TYPE_PRIMARY,  3 },
    {spock::MTL_TYPE_GRAY_DOT,  3 },
    {spock::MTL_TYPE_ALPHA_CUTOFF,  3 },
    {spock::MTL_TYPE_BLEND,  3 }
};


inline spock::scene_t::triangle_mtl_idx_type 
calc_triangle_mtl_idx(spock::mtl_type_e a_mtl_type, int32_t a_mtl_instance_idx)
{
    const auto mtl_type_idx = static_cast<uint64_t>(a_mtl_type);
    const auto mtl_instance_idx = static_cast<uint64_t>(a_mtl_instance_idx);

    SPOCK_PARANOID(
        mtl_instance_idx <= spock::scene_t::k_mtl_instance_idx_max, 
        "mtl_instance_idx cannot be negative or larger than mtl_instance_idx_max: " + std::to_string(spock::scene_t::k_mtl_instance_idx_max)
    );
    return static_cast<spock::scene_t::triangle_mtl_idx_type>((mtl_type_idx << spock::scene_t::k_mtl_instance_idx_bit_count) | mtl_instance_idx);
}



} // anonymous namespace


glm::mat4 spock::
perspective_reversed_z(float a_fovy, float a_aspect, float a_near_plane, float a_far_plane)
{
    glm::mat4 m = glm::identity<glm::mat4>();
    m[1][1] = -1.f / glm::tan(a_fovy * 0.5f); // y 
    m[0][0] = -m[1][1] / a_aspect; // x
    m[2][2] = a_near_plane / (a_far_plane - a_near_plane); // A
    m[3][2] = a_far_plane * m[2][2]; // B
    m[2][3] = -1.f;
    m[3][3] = 0.f;

    return m;
}


void spock::camera_t::
init()
{
    SPOCK_PARANOID(glm::dot(up_, up_) > 0.f, "camera_t ctor: right vector is zero");
    SPOCK_PARANOID(glm::dot(outward_, outward_) > 0.f, "camera_t ctor: outward vector is zero");

    outward_ = glm::normalize(outward_);
    right_ = glm::normalize(glm::cross(up_, outward_));
    up_ = glm::cross(outward_, right_);
}


spock::camera_t::
camera_t() 
    : reversed_z_{ false }
    , view_outdated_{ true }
    , proj_outdated_{ true }
    , fovy_{ 60.f }
    , aspect_{ 1.778f }
    , near_plane_{ 0.01f }
    , far_plane_{ 10.f }
    , eye_{ 0.f, -5.f, 5.f }
    , right_{ 1.f, 0.f, 0.f }
    , outward_{ glm::normalize(eye_) }
    , up_{ glm::cross(outward_, right_) }
{
    update();
}

spock::camera_t::
camera_t(float a_aspect, const ordered_json& a_camera_config_json)
    : aspect_{ a_aspect }
    , reversed_z_{ true }
    , view_outdated_{ true }
    , proj_outdated_{ true }
{
    fovy_ = glm::radians(a_camera_config_json["fovy"].get<float>());
    near_plane_ = a_camera_config_json["near"].get<float>();
    far_plane_ = a_camera_config_json["far"].get<float>();
    auto eye_v = a_camera_config_json["eye"].get<std::vector<float>>();
    auto outward_v = a_camera_config_json["outward"].get<std::vector<float>>();
    auto up_v = a_camera_config_json["up"].get<std::vector<float>>();
    if (a_camera_config_json.contains("reversed_z")) {
        reversed_z_ = a_camera_config_json["reversed_z"].get<bool>();
    }

    SPOCK_PARANOID(eye_v.size() == 3, "invalid camera eye position: 3 components are needed");
    SPOCK_PARANOID(outward_v.size() == 3, "invalid camera outward vector: 3 components are needed");
    SPOCK_PARANOID(up_v.size() == 3, "invalid camera up vector: 3 components are needed");

    memcpy(&eye_[0], eye_v.data(), eye_v.size() * sizeof(float));
    memcpy(&outward_[0], outward_v.data(), outward_v.size() * sizeof(float));
    memcpy(&up_[0], up_v.data(), up_v.size() * sizeof(float));

    init();
    update();
}




spock::camera_t::
camera_t(
    bool a_reversed_z, float a_fovy, float a_aspect, float a_near_plane, float a_far_plane,
    const glm::vec3& a_eye, const glm::vec3& a_up, const glm::vec3& a_outward
)
    : reversed_z_{ a_reversed_z }
    , fovy_ { a_fovy }
    , aspect_{ a_aspect }
    , near_plane_{ a_near_plane }
    , far_plane_{ a_far_plane }
    , eye_{ a_eye }
    , view_outdated_{ true }
    , proj_outdated_{ true }
{
    init();
    update();
}


const glm::mat4& spock::camera_t::
view_mat() const 
{
    SPOCK_PARANOID(!view_outdated_, "camera view mat outdated; call update() first.");
    return view_mat_;
}


const glm::mat4& spock::camera_t::
inv_view_mat() const 
{
    SPOCK_PARANOID(!view_outdated_, "camera view mat outdated; call update() first.");
    return inv_view_mat_;
}


const glm::mat4& spock::camera_t::
view_proj_mat() const 
{
    SPOCK_PARANOID(!view_outdated_ && !proj_outdated_, "camera view or proj mat outdated; call update() first");
    return view_proj_mat_;
}


const glm::mat4& spock::camera_t::
proj_mat() const 
{
    SPOCK_PARANOID(!proj_outdated_, "camera proj mat outdated; call update() first.");
    return proj_mat_;
}


void spock::camera_t::
update()
{
    SPOCK_PARANOID(view_outdated_ || proj_outdated_, "redundant camera update call: already up-to-date");

    if (view_outdated_) {
        glm::mat4 view_mat_new = glm::identity<glm::mat4>();
        view_mat_new[0] = glm::vec4(right_, glm::dot(right_, -eye_));
        view_mat_new[1] = glm::vec4(up_, glm::dot(up_, -eye_));
        view_mat_new[2] = glm::vec4(outward_, glm::dot(outward_, -eye_));
        view_mat_ = glm::transpose(view_mat_new);
        inv_view_mat_ = glm::inverse(view_mat_);
    }

    if (proj_outdated_) {
        if (reversed_z_) {
            proj_mat_ = perspective_reversed_z(fovy_, aspect_, near_plane_, far_plane_);
        }
        else {
            auto proj_mat_new = glm::perspective(fovy_, aspect_, near_plane_, far_plane_);
            proj_mat_new[1][1] *= -1;
            proj_mat_ = proj_mat_new;
        }
    }

    view_proj_mat_ = proj_mat_ * view_mat_;

    view_outdated_ = false;
    proj_outdated_ = false;
}

/// camera space: x = right; y = up; z = outward
/// view_mat: 0th row = right, 1st row = up, 2nd row: outward
void spock::camera_t::
translate(const glm::vec3& a_delta_in_camera_space) 
{
    auto delta_in_world_space = glm::vec3(inv_view_mat_ * glm::vec4(a_delta_in_camera_space, 0.f));
    eye_ += delta_in_world_space;
    view_outdated_ = true;
}

void spock::camera_t::
rotate(float a_degrees, const glm::vec3& a_axis)
{
    const glm::mat3 rotation_mat = glm::mat3(glm::rotate(glm::radians(a_degrees), a_axis));
    right_ = rotation_mat * right_;
    up_ = rotation_mat * up_;
    outward_ = rotation_mat * outward_;
    view_outdated_ = true;
}

void spock::camera_t::
rotate(const glm::mat3& a_rotation_mat)
{
    right_ = a_rotation_mat * right_;
    up_ = a_rotation_mat * up_;
    outward_ = a_rotation_mat * outward_;
    view_outdated_ = true;
}


void spock::camera_t::
reverse_z()
{
    reversed_z_ = !reversed_z_;
    proj_outdated_ = true;
}

void  spock::camera_t::
aspect(float a_aspect)
{
    aspect_ = a_aspect;
    proj_outdated_ = true;
}

/// in radians
void spock::camera_t::
fovy(float a_fovy)
{
    fovy_ = a_fovy;
    proj_outdated_ = true;
}

void spock::camera_t::
near_plane(float a_near_plane)
{
    near_plane_ = a_near_plane;
    proj_outdated_ = true;
}

void spock::camera_t::
far_plane(float a_far_plane)
{
    far_plane_ = a_far_plane;
    proj_outdated_ = true;
}


spock::sampler_2d_info_t::
sampler_2d_info_t() 
    : mag_min_filters_{ VK_FILTER_LINEAR, VK_FILTER_LINEAR }
    , mipmap_mode_{ VK_SAMPLER_MIPMAP_MODE_LINEAR }
    , uv_address_modes_{ VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT } 
{}


spock::sampler_2d_info_t::
sampler_2d_info_t(VkFilter a_filter, VkSamplerMipmapMode a_mipmap_mode, VkSamplerAddressMode a_address_mode) 
    : mag_min_filters_{ a_filter, a_filter }
    , mipmap_mode_{ a_mipmap_mode }
    , uv_address_modes_{ a_address_mode, a_address_mode } 
{}


spock::sampler_2d_info_t::
sampler_2d_info_t(
    const std::array<VkFilter,2>& a_mag_min_filters, 
    VkSamplerMipmapMode a_mipmap_mode, 
    const std::array<VkSamplerAddressMode, 2>& a_uv_address_modes
) 
    : mag_min_filters_{ a_mag_min_filters }
    , mipmap_mode_{ a_mipmap_mode }
    , uv_address_modes_{ a_uv_address_modes } 
{}


spock::sampler_2d_info_t::
sampler_2d_info_t(const uint8_t ** a_bin_stream_ptr)
{
    deserialize(a_bin_stream_ptr);
}


std::vector<uint8_t> spock::sampler_2d_info_t::
serialize() const
{
    constexpr VkDeviceSize bin_stream_bytes = calc_serialized_bytes();
    std::vector<uint8_t> bin_stream(bin_stream_bytes, 0u);
    std::vector<uint32_t> u32s = {
        static_cast<uint32_t>(mag_min_filters_[0]),
        static_cast<uint32_t>(mag_min_filters_[1]),
        static_cast<uint32_t>(mipmap_mode_),
        static_cast<uint32_t>(uv_address_modes_[0]),
        static_cast<uint32_t>(uv_address_modes_[1])
    };
    SPOCK_PARANOID(
        bin_stream_bytes == u32s.size() * sizeof(uint32_t), 
        "bugs in sampler_2d_info_t::calc_serialized_bytes() or serialize()"
    );
    memcpy(bin_stream.data(), u32s.data(), bin_stream_bytes);
    return bin_stream;
}


void spock::sampler_2d_info_t::deserialize(const uint8_t** a_bin_stream_ptr)
{
    enum u32_idx_t {
        u32_idx_mag_filter_e = 0,
        u32_idx_min_filter_e,
        u32_idx_mipmap_mode_e,
        u32_idx_u_addr_mode_e,
        u32_idx_v_addr_mode_e,
        u32_idx_count
    };

    std::vector<uint32_t> u32s(u32_idx_count, 0u);
    size_t u32s_bytes = u32s.size() * sizeof(uint32_t);

    memcpy(u32s.data(), *a_bin_stream_ptr, u32s_bytes);
    *a_bin_stream_ptr += u32s_bytes;

    mag_min_filters_ = {
        static_cast<VkFilter>(u32s[u32_idx_mag_filter_e]),
        static_cast<VkFilter>(u32s[u32_idx_min_filter_e])
    };
    mipmap_mode_ = static_cast<VkSamplerMipmapMode>(u32s[u32_idx_mipmap_mode_e]);
    uv_address_modes_ = {
        static_cast<VkSamplerAddressMode>(u32s[u32_idx_u_addr_mode_e]),
        static_cast<VkSamplerAddressMode>(u32s[u32_idx_v_addr_mode_e]),
    };
}


VkSamplerCreateInfo spock::sampler_2d_info_t::
get_vk_sampler_create_info(float a_max_anisotropy) const
{
    VkSamplerCreateInfo create_info = make_VkSamplerCreateInfo();
    create_info.magFilter = mag_min_filters_[0];
    create_info.minFilter = mag_min_filters_[1];
    create_info.addressModeU = uv_address_modes_[0];
    create_info.addressModeV = uv_address_modes_[1];
    create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    create_info.anisotropyEnable = VK_TRUE;
    create_info.maxAnisotropy = a_max_anisotropy;
    create_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    create_info.unnormalizedCoordinates = VK_FALSE;
    create_info.compareEnable = VK_FALSE;
    create_info.compareOp = VK_COMPARE_OP_ALWAYS;
    create_info.mipmapMode = mipmap_mode_;
    create_info.minLod = 0.f;
    create_info.maxLod = VK_LOD_CLAMP_NONE;
    create_info.mipLodBias = 0.f;
    return create_info;
}


const uint8_t spock::k_mip_2d_padding_zeroes[spock::mip_2d_info_t::BLOCK_ALIGNMENT_BYTES] = {};


uint32_t spock::
get_texel_block_bytes(VkFormat a_format)
{
    auto res = k_format_texel_block_info_map.find(a_format);
    SPOCK_PARANOID(
        res != k_format_texel_block_info_map.end(),
        str_from_VkFormat(a_format).data() + std::string{ " is not a supported blocked format" }
    );
    return static_cast<uint32_t>((res->second).texel_block_bytes);
}

glm::uvec2 spock::
get_texel_block_dim(VkFormat a_format)
{
    auto res = k_format_texel_block_info_map.find(a_format);
    SPOCK_PARANOID(
        res != k_format_texel_block_info_map.end(),
        str_from_VkFormat(a_format).data() + std::string{ " is not a supported blocked format" }
    );
    return glm::uvec2((res->second).texel_block_dim);
}

bool spock::
is_texel_block_compressed(VkFormat a_format)
{
    auto res = k_format_texel_block_info_map.find(a_format);
    SPOCK_PARANOID(
        res != k_format_texel_block_info_map.end(),
        str_from_VkFormat(a_format).data() + std::string{ " is not a supported blocked format" }
    );
    return static_cast<bool>((res->second).is_compressed);
}


bool spock::is_format_srgb(VkFormat a_format)
{
    SPOCK_PARANOID(
        k_format_texel_block_info_map.find(a_format) != k_format_texel_block_info_map.end(),
        str_from_VkFormat(a_format).data() + std::string{ " is not a supported blocked format" }
    );

    std::string format_name{ str_from_VkFormat(a_format) };
    return format_name.find("_SRGB") != std::string::npos;
}


std::vector<VkExtent2D> spock::
calc_mip_dims(const VkExtent2D& a_mip0_dim, uint8_t a_num_mips)
{
    SPOCK_PARANOID(a_num_mips <= calc_num_mips(a_mip0_dim), "request too many mip levels");
    std::vector<VkExtent2D> mip_dims(a_num_mips, a_mip0_dim);

    for (uint32_t i = 1; i < a_num_mips; ++i) {
        mip_dims[i].width = std::max(mip_dims[i - 1].width >> 1, 1u);
        mip_dims[i].height = std::max(mip_dims[i - 1].height >> 1, 1u);
    }
    return mip_dims;
}


std::vector<VkExtent3D> spock::
calc_mip_dims(const VkExtent3D& a_mip0_dim, uint8_t a_num_mips)
{
    SPOCK_PARANOID(a_num_mips <= calc_num_mips(a_mip0_dim), "request too many mip levels");
    std::vector<VkExtent3D> mip_dims(a_num_mips, a_mip0_dim);

    for (uint32_t i = 1; i < a_num_mips; ++i) {
        mip_dims[i].width = std::max(mip_dims[i - 1].width >> 1, 1u);
        mip_dims[i].height = std::max(mip_dims[i - 1].height >> 1, 1u);
        mip_dims[i].depth = std::max(mip_dims[i - 1].depth >> 1, 1u);
    }
    return mip_dims;
}


bool spock::
operator==(const mip_2d_layout_t & a, const mip_2d_layout_t & b)
{
    return a.byte_offsets == b.byte_offsets && a.dims == b.dims && a.padded_dims == b.padded_dims;
}


void spock::
relocate_mip_buffer(VkDeviceSize a_location, std::vector<VkDeviceSize>& mip_byte_offsets)
{
    SPOCK_PARANOID(
        a_location % mip_2d_info_t::BLOCK_ALIGNMENT_BYTES == 0,
        "relocate buffer failed: offset is not multiple of " + std::to_string(mip_2d_info_t::BLOCK_ALIGNMENT_BYTES)
    );

    std::for_each(
        mip_byte_offsets.begin(), 
        mip_byte_offsets.end() - 1, 
        [a_location](VkDeviceSize& i) {return i += a_location; });

    SPOCK_PARANOID(mip_byte_offsets[0] == a_location, "bugs in relocate_mip_buffer");
}


spock::mip_2d_info_t::
mip_2d_info_t(const uint8_t ** a_bin_stream_ptr)
    :format_{ VK_FORMAT_UNDEFINED }
    , mip0_dim_{}
    , sampler_2d_idx_{}
    , num_mips_{}
{
    deserialize(a_bin_stream_ptr);
}


spock::mip_2d_info_t::
mip_2d_info_t(VkFormat a_format, const VkExtent2D & a_mip0_dim, uint16_t a_sampler_2d_idx, uint8_t a_max_num_mips)
    : format_{ a_format }
    , mip0_dim_{ a_mip0_dim }
    , sampler_2d_idx_ { a_sampler_2d_idx }
{
    SPOCK_PARANOID(a_max_num_mips > 0, "mip_2d_info_t::mip_2d_info_t: a_max_num_mips cannot be 0");
    num_mips_ = std::min(a_max_num_mips, calc_num_mips(a_mip0_dim));
}


spock::mip_2d_info_t::
mip_2d_info_t(VkFormat a_format, const mip_2d_info_t& a_src_mip_2d_info)
    : format_{ a_format }
    , mip0_dim_{ a_src_mip_2d_info.mip0_dim_ }
    , sampler_2d_idx_{ a_src_mip_2d_info.sampler_2d_idx_ }
    , num_mips_{ a_src_mip_2d_info.num_mips_ }
{
    SPOCK_PARANOID(num_mips_ > 0, "mip_2d_info_t::mip_2d_info_t: a_max_num_mips cannot be 0");
}


spock::mip_2d_layout_t spock::mip_2d_info_t::
calc_mip_2d_layout() const
{
    SPOCK_PARANOID(valid(), "mip_2d_info_t::calc_mip_2d_layout() failed: invalid mip_2d_info_t");

    mip_2d_layout_t mip_2d_layout;
    mip_2d_layout.dims = calc_mip_dims(mip0_dim_, num_mips_);
    
    if (!is_texel_block_compressed(format_)) {
        mip_2d_layout.padded_dims = mip_2d_layout.dims;
    }
    else {
        auto texel_block_dim = get_texel_block_dim(format_);
        
        mip_2d_layout.padded_dims.reserve(num_mips_);
        for (size_t i = 0; i < num_mips_; ++i) {
            mip_2d_layout.padded_dims.push_back({
                calc_padded(mip_2d_layout.dims[i].width, texel_block_dim.x),
                calc_padded(mip_2d_layout.dims[i].height, texel_block_dim.y)
            });
        }
    }

    mip_2d_layout.byte_offsets.resize(num_mips_ + 1u, 0u);
    const auto texel_block_dim = get_texel_block_dim(format_);
    const auto texel_block_bytes = get_texel_block_bytes(format_);

    for (uint8_t i = 0; i < num_mips_; ++i) {
        auto num_blocks_per_row = mip_2d_layout.padded_dims[i].width / texel_block_dim.x;
        auto num_blocks_per_col = mip_2d_layout.padded_dims[i].height / texel_block_dim.y;
        mip_2d_layout.byte_offsets[i + 1] = mip_2d_layout.byte_offsets[i] + num_blocks_per_row * num_blocks_per_col * texel_block_bytes;
    }

    return mip_2d_layout;
}


VkDeviceSize spock::mip_2d_info_t::calc_mip_buffer_bytes() const
{
    return calc_mip_2d_layout().byte_offsets.back();
}


bool spock::mip_2d_info_t::
valid() const
{
    return format_ != VK_FORMAT_UNDEFINED &&
        mip0_dim_.width > 0 && 
        mip0_dim_.height > 0 && 
        num_mips_ > 0;
}


bool spock::mip_2d_info_t::
empty() const
{
    return format_ == VK_FORMAT_UNDEFINED &&
        mip0_dim_ == VkExtent2D{ 0, 0 } &&
        num_mips_ == 0;
}


VkDeviceSize spock::mip_2d_info_t::
calc_serialized_bytes()
{
    return sizeof(uint32_t) + sizeof(mip0_dim_) + sizeof(sampler_2d_idx_) + sizeof(num_mips_);
}


std::vector<uint8_t> spock::mip_2d_info_t::
serialize() const
{
    static_assert(sizeof(uint32_t) >= sizeof(VkFormat), "VkFormat cannot be represented by uint32_t");
    static_assert(std::is_same_v<uint32_t, decltype(VkExtent2D::width)>, "VkExtent2D is not composed of uint32_t");

    SPOCK_PARANOID(valid(), "cannot serialize invalid mip_2d_info_t");

    std::vector<uint32_t> u32s = {
        static_cast<uint32_t>(format_),
        mip0_dim_.width,
        mip0_dim_.height
    };
    size_t u32s_bytes = u32s.size() * sizeof(uint32_t);

    // format, mip0_dim_ -> sampler_2d_idx_ -> num_mips_
    VkDeviceSize bin_stream_bytes = u32s_bytes + 1 * sizeof(sampler_2d_idx_) + 1 * sizeof(num_mips_);
    SPOCK_PARANOID(bin_stream_bytes == calc_serialized_bytes(), "mismatch between serialized_bytes and calc_serialized_bytes()");

    std::vector<uint8_t> bin_stream(bin_stream_bytes, 0);
    uint8_t* dst = bin_stream.data();
    
    memcpy(dst, u32s.data(), u32s_bytes);
    dst += u32s_bytes;

    memcpy(dst, &sampler_2d_idx_, sizeof(sampler_2d_idx_));
    dst += sizeof(sampler_2d_idx_);

    bin_stream.back() = num_mips_;
    return bin_stream;
}


void spock::mip_2d_info_t::
deserialize(const uint8_t** a_bin_stream_ptr)
{
    static_assert(sizeof(uint32_t) >= sizeof(VkFormat), "VkFormat cannot be represented by uint32_t");
    static_assert(std::is_same_v<uint64_t, VkDeviceSize>, "VkDeviceSize is not uint64_t");
    static_assert(std::is_same_v<uint32_t, decltype(VkExtent2D::width)>, "VkExtent2D is not composed of uint32_t");
    SPOCK_PARANOID(empty(), "cannot deserialize, texels_info_t is not empty");

    enum u32_idx_e {
        u32_idx_format_idx_e,
        u32_idx_mip0_width_e,
        u32_idx_mip0_height_e,
        u32_idx_count_e
    };
    std::vector<uint32_t> u32s (u32_idx_count_e, 0u);
    size_t u32s_bytes = u32s.size() * sizeof(uint32_t);

    memcpy(u32s.data(), *a_bin_stream_ptr, u32s_bytes);
    *a_bin_stream_ptr += u32s_bytes;

    format_ = static_cast<VkFormat>(u32s[u32_idx_format_idx_e]);
    mip0_dim_ = VkExtent2D{ u32s[u32_idx_mip0_width_e], u32s[u32_idx_mip0_height_e] };

    memcpy(&sampler_2d_idx_, *a_bin_stream_ptr, sizeof(sampler_2d_idx_));
    *a_bin_stream_ptr += sizeof(sampler_2d_idx_);

    num_mips_ = **a_bin_stream_ptr;
    *a_bin_stream_ptr += 1;
}


VkImageCreateInfo spock::mip_2d_info_t::
get_vk_image_create_info() const
{
    VkImageCreateInfo create_info = make_VkImageCreateInfo();
    create_info.imageType = VK_IMAGE_TYPE_2D;
    create_info.format = format_;
    create_info.extent = to_extent_3d(mip0_dim_, 1u);
    create_info.mipLevels = num_mips_;
    create_info.arrayLayers = 1;
    create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    return create_info;
}


VkImageViewCreateInfo spock::mip_2d_info_t::
get_vk_image_view_create_info(VkImage a_vk_image) const
{
    VkImageViewCreateInfo create_info = make_VkImageViewCreateInfo();
    create_info.image = a_vk_image;
    create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    create_info.format = format_;
    create_info.components = {};
    create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    create_info.subresourceRange.baseMipLevel = 0;
    create_info.subresourceRange.levelCount = num_mips_;
    create_info.subresourceRange.baseArrayLayer = 0;
    create_info.subresourceRange.layerCount = 1;
    return create_info;
}


void spock::
serialize_mip_2d_to_file(
    const sampler_2d_info_t & a_sampler_2d_info, 
    const mip_2d_info_t & a_mip_2d_info, 
    const uint8_t * a_mip_buffer, 
    VkDeviceSize a_mip_buffer_bytes, 
    const std::string & a_file_path
)
{
    SPOCK_PARANOID(
        a_mip_buffer_bytes == a_mip_2d_info.calc_mip_buffer_bytes(), 
        a_file_path + " contains invalid mip_2d buffer"
    );
    auto padding_bytes = calc_padding(a_mip_buffer_bytes, mip_2d_info_t::BLOCK_ALIGNMENT_BYTES);

    auto sampler_2d_info_bin_stream = a_sampler_2d_info.serialize();
    auto mip_2d_info_bin_stream = a_mip_2d_info.serialize();
    std::ofstream ofs(a_file_path, std::ios::out | std::ios::binary);
    ofs.write(reinterpret_cast<const char*>(sampler_2d_info_bin_stream.data()), sampler_2d_info_bin_stream.size());
    ofs.write(reinterpret_cast<const char*>(mip_2d_info_bin_stream.data()), mip_2d_info_bin_stream.size());
    ofs.write(reinterpret_cast<const char*>(a_mip_buffer), a_mip_buffer_bytes);
    if (padding_bytes > 0u) {
        ofs.write(reinterpret_cast<const char*>(k_mip_2d_padding_zeroes), padding_bytes);
    }
    ofs.close();
}


void spock::deserialize_mip_2d_from_file(
    const std::string& a_file_path, 
    sampler_2d_info_t* a_sampler_2d_info, 
    mip_2d_info_t* a_mip_2d_info, 
    std::unique_ptr<uint8_t[]>* a_mip_buffer
)
{
    SPOCK_PARANOID(a_sampler_2d_info != nullptr, "deserialize_mip_2d_from_file: a_sampler_2d_info cannot be nullptr");
    SPOCK_PARANOID(a_mip_2d_info != nullptr, "deserialize_mip_2d_from_file: a_mip_2d_info cannot be nullptr");
    SPOCK_PARANOID(a_mip_buffer != nullptr, "deserialize_mip_2d_from_file: a_mip_buffer cannot be nullptr");

    std::ifstream ifs(a_file_path, std::ios::in | std::ios::ate | std::ios::binary);
    auto file_bytes = static_cast<size_t>(ifs.tellg());
    constexpr auto sampler_info_bin_bytes = sampler_2d_info_t::calc_serialized_bytes();
    const auto mip_info_bin_bytes = mip_2d_info_t::calc_serialized_bytes();
    const auto info_bin_bytes = sampler_info_bin_bytes + mip_info_bin_bytes;
    SPOCK_PARANOID(file_bytes > info_bin_bytes, a_file_path + " is not a valid mip_2d file");
    const auto buffer_bytes = file_bytes - info_bin_bytes;

    std::unique_ptr<uint8_t[]> unique_info_bin_stream(new uint8_t[info_bin_bytes]);
    std::unique_ptr<uint8_t[]> unique_mip_buffer(new uint8_t[buffer_bytes]);

    ifs.seekg(0);
    ifs.read(reinterpret_cast<char*>(unique_info_bin_stream.get()), info_bin_bytes);
    ifs.read(reinterpret_cast<char*>(unique_mip_buffer.get()), buffer_bytes);
    ifs.close();

    const uint8_t* info_bin_stream = unique_info_bin_stream.get();
    sampler_2d_info_t sampler_2d_info(&info_bin_stream);
    mip_2d_info_t mip_2d_info(&info_bin_stream);

    SPOCK_PARANOID(mip_2d_info.valid(), a_file_path + " contains invalid mip_2d_info");
    SPOCK_PARANOID(
        buffer_bytes == calc_padded(mip_2d_info.calc_mip_buffer_bytes(), mip_2d_info_t::BLOCK_ALIGNMENT_BYTES), 
        a_file_path + " contains invalid mip_2d buffer");

    *a_sampler_2d_info = sampler_2d_info;
    *a_mip_2d_info = mip_2d_info;
    *a_mip_buffer = std::move(unique_mip_buffer);
}


spock::buffer_t spock::cmd_upload_to_image_2d (
    const context_t& a_context,
    VkCommandBuffer a_command_buffer, 
    const image_t& a_image,
    const mip_2d_layout_t& a_mip_2d_layout,
    const uint8_t* a_mip_2d_buffer,
    VkImageAspectFlags a_image_aspect_flags,
    VkImageLayout a_final_image_layout,
    VkPipelineStageFlags a_next_stages,
    VkAccessFlags a_next_accesses
)
{
    const auto num_mips = a_image.num_mip_levels();

    SPOCK_PARANOID(
        num_mips == static_cast<uint32_t>(a_mip_2d_layout.dims.size()),
        "num_mip_levels mismatch between image(" + std::to_string(num_mips) +
        ") and provided texels (" + std::to_string(a_mip_2d_layout.dims.size()) + ")"
    );

    SPOCK_PARANOID(
        a_image.extent() == to_extent_3d(a_mip_2d_layout.dims[0], 1u),
        "image extent mismatch"
    );

    //const auto mip_dims = a_texture_2d_info.mip_dims();
    //const auto padded_mip_dims = a_texture_2d_info.padded_mip_dims(mip_dims);
    //const auto mip_byte_offsets = a_texture_2d_info.mip_byte_offsets(padded_mip_dims);
    //const auto buffer_bytes = mip_byte_offsets.back();

    auto staging_buffer = stage(a_context, a_mip_2d_buffer, a_mip_2d_layout.byte_offsets.back());

    cmd_copy_staging_mip_buffer_to_image_2d(a_command_buffer,
        a_mip_2d_layout, staging_buffer, 
        a_image, a_image_aspect_flags, 
        a_final_image_layout, a_next_stages, a_next_accesses
    );

    return staging_buffer;
}


void spock::cmd_copy_staging_mip_buffer_to_image_2d(
    VkCommandBuffer a_command_buffer, 
    const mip_2d_layout_t & a_mip_2d_layout, VkBuffer a_staging_mip_buffer, 
    VkImage a_dst_image, VkImageAspectFlags a_image_aspect_flags, 
    VkImageLayout a_final_image_layout, VkPipelineStageFlags a_next_stages, VkAccessFlags a_next_accesses
)
{
    const uint32_t num_mips = static_cast<uint32_t>(a_mip_2d_layout.dims.size());
    auto image_memory_barrier = make_VkImageMemoryBarrier();
    image_memory_barrier.srcAccessMask = 0;
    image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    image_memory_barrier.image = a_dst_image;
    image_memory_barrier.subresourceRange.aspectMask = a_image_aspect_flags;
    image_memory_barrier.subresourceRange.baseMipLevel = 0;
    image_memory_barrier.subresourceRange.levelCount = num_mips;
    image_memory_barrier.subresourceRange.baseArrayLayer = 0;
    image_memory_barrier.subresourceRange.layerCount = 1;

    vkCmdPipelineBarrier(
        a_command_buffer,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        1, &image_memory_barrier
    );

    std::vector<VkBufferImageCopy> buffer_image_copies;
    buffer_image_copies.resize(num_mips, VkBufferImageCopy{});
    
    for (uint32_t i = 0; i < num_mips; ++i) {
        buffer_image_copies[i].bufferOffset = a_mip_2d_layout.byte_offsets[i];
        buffer_image_copies[i].bufferRowLength = a_mip_2d_layout.padded_dims[i].width;
        buffer_image_copies[i].bufferImageHeight = a_mip_2d_layout.padded_dims[i].height;
        buffer_image_copies[i].imageSubresource.aspectMask = a_image_aspect_flags;
        buffer_image_copies[i].imageSubresource.mipLevel = i;
        buffer_image_copies[i].imageSubresource.baseArrayLayer = 0;
        buffer_image_copies[i].imageSubresource.layerCount = 1;
        buffer_image_copies[i].imageOffset = { 0, 0, 0 };
        buffer_image_copies[i].imageExtent = to_extent_3d(a_mip_2d_layout.dims[i], 1u);
    }

    vkCmdCopyBufferToImage(
        a_command_buffer,
        a_staging_mip_buffer, a_dst_image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        num_mips, buffer_image_copies.data()
    );

    // reuse image_memory_barrier.image and part of image_memory_barrier.subresourceRange
    image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    image_memory_barrier.dstAccessMask = a_next_accesses;
    image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    image_memory_barrier.newLayout = a_final_image_layout;

    vkCmdPipelineBarrier(
        a_command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        a_next_stages,
        0,
        0, nullptr,
        0, nullptr,
        1, &image_memory_barrier
    );

}


spock::buffer_t spock::
dev_gen_mip_2d_buffer(
    const context_t & a_context, const command_pool_t & a_command_pool, const queue_t & a_queue, 
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout, const void* a_src_mip0_buffer,
    VkFormat a_src_mip0_buffer_format
)
{
    VkFormat mip_2d_format = a_dst_mip_2d_info.format();
    const auto mip0_dim = a_dst_mip_2d_info.mip0_dim();
    const size_t num_mips = static_cast<size_t>(a_dst_mip_2d_info.num_mips());

    SPOCK_PARANOID(get_texel_block_bytes(mip_2d_format) == 4, std::string{ "invalid dst format: " } + str_from_VkFormat(mip_2d_format).data());
    SPOCK_PARANOID(num_mips == a_dst_mip_2d_layout.dims.size(), "a_dst_mip_2d_info and a_dst_mip_2d_layout have incompatible num_mips");
    SPOCK_PARANOID(a_dst_mip_2d_info.mip0_dim() == a_dst_mip_2d_layout.dims[0], "a_dst_mip_2d_info and a_dst_mip_2d_layout has incompatible mip0 dim");

    host_timer_t timer;

    auto image_create_info = a_dst_mip_2d_info.get_vk_image_create_info();
    image_create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

    VmaAllocationCreateInfo allocation_create_info{};
    allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    image_t dst_image(a_context, image_create_info, allocation_create_info);
    image_t tmp_image; // may be unused
    buffer_t staging_buffer;
    buffer_t mip_2d_dev_buffer;

    VkImageSubresourceLayers image_subresource_layers{};
    image_subresource_layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_subresource_layers.mipLevel = 0;
    image_subresource_layers.baseArrayLayer = 0;
    image_subresource_layers.layerCount = 1;

    command_buffer_t cmd_buf{ a_command_pool };
    auto vk_command_buffer_begin_info = make_VkCommandBufferBeginInfo();
    vk_command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf, &vk_command_buffer_begin_info));

    if (a_src_mip0_buffer_format == a_dst_mip_2d_info.format() || a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED) {
        staging_buffer = dst_image.cmd_upload(
            cmd_buf,
            a_src_mip0_buffer, static_cast<size_t>(a_dst_mip_2d_layout.get_mip0_buffer_bytes()),
            image_subresource_layers,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_ACCESS_TRANSFER_READ_BIT
        );
    }
    else {
        SPOCK_PARANOID(
            get_texel_block_bytes(a_src_mip0_buffer_format) == 4, 
            std::string{ "invalid src buffer format: " } + str_from_VkFormat(a_src_mip0_buffer_format).data()
        );
        auto tmp_mip_2d_info = mip_2d_info_t(
            a_src_mip0_buffer_format, a_dst_mip_2d_info.mip0_dim(), a_dst_mip_2d_info.sampler_2d_idx(), 1u
        );
        auto tmp_image_create_info = tmp_mip_2d_info.get_vk_image_create_info();
        tmp_image_create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
        tmp_image = image_t(a_context, tmp_image_create_info, allocation_create_info);
        staging_buffer = tmp_image.cmd_upload(
            cmd_buf,
            a_src_mip0_buffer, static_cast<size_t>(a_dst_mip_2d_layout.get_mip0_buffer_bytes()),
            image_subresource_layers,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_ACCESS_TRANSFER_READ_BIT
        );
        tmp_image.cmd_blit_mip0_to_image(
            cmd_buf, dst_image, 
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_ACCESS_TRANSFER_READ_BIT
        );
    }

    if (num_mips > 1) {
        auto mip_extents = dst_image.cmd_gen_mipmaps_from_mip0(
            cmd_buf,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_ACCESS_TRANSFER_READ_BIT
        );
        SPOCK_PARANOID(
            std::equal(
                mip_extents.begin(), mip_extents.end(),
                a_dst_mip_2d_layout.dims.begin(),
                [](const VkExtent3D& a, const VkExtent2D& b) {return a == to_extent_3d(b, 1); }
            ),
            "number of generated mip levels is not equal to requested"
        );
    }
    mip_2d_dev_buffer = dst_image.cmd_copy_to_host_visible_buffer(cmd_buf, get_texel_block_bytes(mip_2d_format));

    SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf));
    auto submit_info = build_VkSubmitInfo(1, &cmd_buf());
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));

    spock::verbose("gpu gen mipmap took " + std::to_string(timer.elapsed_ms()) + " ms");

    return mip_2d_dev_buffer;

}


std::unique_ptr<uint8_t[]> spock::
dev_gen_mip_and_bc1_encode_2d(
    const context_t & a_context, const command_pool_t & a_command_pool, const queue_t & a_queue, 
    const mip_2d_info_t & a_src_mip_2d_info, const void * a_src_mip0_buffer, 
    const mip_2d_info_t & a_dst_mip_2d_info, const mip_2d_layout_t & a_dst_mip_2d_layout, 
    VkFormat a_src_mip0_buffer_format
)
{
    const auto src_format = a_src_mip_2d_info.format();
    const auto dst_format = a_dst_mip_2d_info.format();

    SPOCK_PARANOID(a_src_mip_2d_info.valid(), "a_mip_2d_info is invalid");
    SPOCK_PARANOID(
        a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED || 
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_SRGB || 
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_UNORM, 
        std::string{ "src buffer format " } + str_from_VkFormat(a_src_mip0_buffer_format).data() + " is not supported"
    );
    SPOCK_PARANOID(src_format == VK_FORMAT_R8G8B8A8_UNORM, "src format must be VK_FORMAT_R8G8B8A8_UNORM");
    SPOCK_PARANOID(dst_format == VK_FORMAT_BC1_RGB_UNORM_BLOCK, "dst format must be VK_FORMAT_BC1_RGB_UNORM_BLOCK");
    SPOCK_PARANOID(
        a_src_mip_2d_info.num_mips() == a_dst_mip_2d_info.num_mips(),
        "src_num_mip_levels and dst_num_mip_levels are not equal"
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_layout == a_dst_mip_2d_info.calc_mip_2d_layout(),
        "a_dst_mip_2d_info and a_dst_mip_2d_layout are not compatible"
    );

    auto src_mip_2d_layout = a_src_mip_2d_info.calc_mip_2d_layout();
    buffer_t host_visible_mip_buffer = dev_gen_mip_2d_buffer(
        a_context, a_command_pool, a_queue,
        a_src_mip_2d_info, src_mip_2d_layout, a_src_mip0_buffer, a_src_mip0_buffer_format
    );
    host_visible_mip_buffer.map();
    const uint8_t* mip_2d_buffer = host_visible_mip_buffer.mapped_data<const uint8_t>();

    return compress_texture_2d(
        a_src_mip_2d_info, src_mip_2d_layout, mip_2d_buffer,
        a_dst_mip_2d_info, a_dst_mip_2d_layout, 
        [](uint8_t* out, const uint8_t* in) { rgbcx::encode_bc1(rgbcx_qual, out, in, false, false); }
    );
}


std::unique_ptr<uint8_t[]> spock::
dev_gen_mip_and_bc3_encode_2d(
    const context_t & a_context, const command_pool_t & a_command_pool, const queue_t & a_queue, 
    const mip_2d_info_t & a_src_mip_2d_info, const void * a_src_mip0_buffer, 
    const mip_2d_info_t & a_dst_mip_2d_info, const mip_2d_layout_t & a_dst_mip_2d_layout, 
    VkFormat a_src_mip0_buffer_format
)
{
    const auto src_format = a_src_mip_2d_info.format();
    const auto dst_format = a_dst_mip_2d_info.format();

    SPOCK_PARANOID(a_src_mip_2d_info.valid(), "a_mip_2d_info is invalid");
    SPOCK_PARANOID(
        a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED ||
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_SRGB ||
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_UNORM,
        std::string{ "src buffer format " } + str_from_VkFormat(a_src_mip0_buffer_format).data() + " is not supported"
    );
    SPOCK_PARANOID(src_format == VK_FORMAT_R8G8B8A8_UNORM, "src format must be VK_FORMAT_R8G8B8A8_UNORM");
    SPOCK_PARANOID(dst_format == VK_FORMAT_BC3_UNORM_BLOCK, "dst format must be VK_FORMAT_BC3_RGB_UNORM_BLOCK");
    SPOCK_PARANOID(
        a_src_mip_2d_info.num_mips() == a_dst_mip_2d_info.num_mips(),
        "src_num_mip_levels and dst_num_mip_levels are not equal"
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_layout == a_dst_mip_2d_info.calc_mip_2d_layout(), 
        "a_dst_mip_2d_info and a_dst_mip_2d_layout are not compatible"
    );

    auto src_mip_2d_layout = a_src_mip_2d_info.calc_mip_2d_layout();
    buffer_t host_visible_mip_buffer = dev_gen_mip_2d_buffer(
        a_context, a_command_pool, a_queue,
        a_src_mip_2d_info, src_mip_2d_layout, a_src_mip0_buffer, a_src_mip0_buffer_format
    );
    host_visible_mip_buffer.map();
    uint8_t* mip_2d_buffer = host_visible_mip_buffer.mapped_data<uint8_t>();

    return compress_texture_2d(
        a_src_mip_2d_info, src_mip_2d_layout, mip_2d_buffer,
        a_dst_mip_2d_info, a_dst_mip_2d_layout, 
        [](uint8_t* out, const uint8_t* in) { rgbcx::encode_bc3(rgbcx_qual, out, in); }
    );
}


std::unique_ptr<uint8_t[]> spock::
gen_mipmap_and_bc1_encode_2d(
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    VkFormat a_src_mip0_buffer_format
)
{
    const auto src_format = a_src_mip_2d_info.format();
    const auto dst_format = a_dst_mip_2d_info.format();

    SPOCK_PARANOID(a_src_mip_2d_info.valid(), "a_mip_2d_info is invalid");
    SPOCK_PARANOID(
        a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED ||
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_SRGB ||
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_UNORM,
        std::string{ "src buffer format " } + str_from_VkFormat(a_src_mip0_buffer_format).data() + " is not supported"
    );
    SPOCK_PARANOID(src_format == VK_FORMAT_R8G8B8A8_UNORM, "src format must be VK_FORMAT_R8G8B8A8_SRGB");
    SPOCK_PARANOID(dst_format == VK_FORMAT_BC1_RGB_UNORM_BLOCK, "dst format must be VK_FORMAT_BC1_RGB_UNORM_BLOCK");
    SPOCK_PARANOID(
        a_src_mip_2d_info.num_mips() == a_dst_mip_2d_info.num_mips(),
        "src_num_mip_levels and dst_num_mip_levels are not equal"
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_layout == a_dst_mip_2d_info.calc_mip_2d_layout(), 
        "a_dst_mip_2d_info and a_dst_mip_2d_layout are not compatible"
    );

    const auto& mip_dims = a_dst_mip_2d_layout.dims;
    const glm::u8vec4* src_texels = reinterpret_cast<const glm::u8vec4*>(a_src_mip0_buffer);
    std::unique_ptr<glm::u8vec4[]> unique_u8_rgba_texels;
    auto fn_box_filter = [](const glm::vec4& t_00, const glm::vec4& t_10, const glm::vec4& t_01, const glm::vec4& t_11) {
        return 0.25f * (t_00 + t_10 + t_01 + t_11);
    };
    if (a_src_mip0_buffer_format == src_format || a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED) {
        unique_u8_rgba_texels = convert_and_gen_mipmap_with_box_filter<glm::u8vec4, glm::vec4, glm::u8vec4>(
            src_texels, mip_dims,
            [](const glm::u8vec4& u8_rgba) ->glm::vec4 { return u8_to_uf32_vec4(u8_rgba); },
            fn_box_filter,
            [](const glm::vec4& v) { return uf32_to_u8_vec4(v); }
        );
    }
    else {
        unique_u8_rgba_texels = convert_and_gen_mipmap_with_box_filter<glm::u8vec4, glm::vec4, glm::u8vec4>(
            src_texels, mip_dims,
            [](const glm::u8vec4& u8_rgba) ->glm::vec4 { return convertSRGBToLinear(u8_to_uf32_vec4(u8_rgba)); },
            fn_box_filter,
            [](const glm::vec4& v) { return uf32_to_u8_vec4(v); }
        );
    }

    auto src_mip_2d_layout = a_src_mip_2d_info.calc_mip_2d_layout();
    return compress_texture_2d(
        a_src_mip_2d_info, src_mip_2d_layout, reinterpret_cast<uint8_t*>(unique_u8_rgba_texels.get()),
        a_dst_mip_2d_info, a_dst_mip_2d_layout, 
        [](uint8_t* out, const uint8_t* in) { rgbcx::encode_bc1(rgbcx_qual, out, in, false, false); }
    );
}


std::unique_ptr<uint8_t[]> spock::
gen_mipmap_and_bc3_encode_2d(
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    VkFormat a_src_mip0_buffer_format
)
{
    const auto src_format = a_src_mip_2d_info.format();
    const auto dst_format = a_dst_mip_2d_info.format();

    SPOCK_PARANOID(a_src_mip_2d_info.valid(), "a_mip_2d_info is invalid");
    SPOCK_PARANOID(
        a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED ||
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_SRGB ||
        a_src_mip0_buffer_format == VK_FORMAT_R8G8B8A8_UNORM,
        std::string{ "src buffer format " } + str_from_VkFormat(a_src_mip0_buffer_format).data() + " is not supported"
    );
    SPOCK_PARANOID(src_format == VK_FORMAT_R8G8B8A8_UNORM, "src format must be VK_FORMAT_R8G8B8A8_SRGB");
    SPOCK_PARANOID(dst_format == VK_FORMAT_BC3_UNORM_BLOCK, "dst format must be VK_FORMAT_BC3_RGB_UNORM_BLOCK");
    SPOCK_PARANOID(
        a_src_mip_2d_info.num_mips() == a_dst_mip_2d_info.num_mips(),
        "src_num_mip_levels and dst_num_mip_levels are not equal"
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_layout == a_dst_mip_2d_info.calc_mip_2d_layout(), 
        "a_dst_mip_2d_info and a_dst_mip_2d_layout are not compatible"
    );

    const auto& mip_dims = a_dst_mip_2d_layout.dims;
    const glm::u8vec4* src_texels = reinterpret_cast<const glm::u8vec4*>(a_src_mip0_buffer);
    std::unique_ptr<glm::u8vec4[]> unique_u8_rgba_texels;
    auto fn_box_filter = [](const glm::vec4& t_00, const glm::vec4& t_10, const glm::vec4& t_01, const glm::vec4& t_11) {
        return 0.25f * (t_00 + t_10 + t_01 + t_11);
    };
    if (a_src_mip0_buffer_format == src_format || a_src_mip0_buffer_format == VK_FORMAT_UNDEFINED) {
        unique_u8_rgba_texels = convert_and_gen_mipmap_with_box_filter<glm::u8vec4, glm::vec4, glm::u8vec4>(
            src_texels, mip_dims,
            [](const glm::u8vec4& u8_rgba) ->glm::vec4 { return u8_to_uf32_vec4(u8_rgba); },
            fn_box_filter,
            [](const glm::vec4& v) { return uf32_to_u8_vec4(v); }
        );

    }
    else {
        unique_u8_rgba_texels = convert_and_gen_mipmap_with_box_filter<glm::u8vec4, glm::vec4, glm::u8vec4>(
            src_texels, mip_dims,
            [](const glm::u8vec4& u8_rgba) ->glm::vec4 { return convertSRGBToLinear(u8_to_uf32_vec4(u8_rgba)); },
            fn_box_filter,
            [](const glm::vec4& v) { return uf32_to_u8_vec4(v); }
        );
    }
    auto src_mip_2d_layout = a_src_mip_2d_info.calc_mip_2d_layout();
    glm::u8vec4* u8_rgba_texels = unique_u8_rgba_texels.get();

    return compress_texture_2d(
        a_src_mip_2d_info, src_mip_2d_layout, reinterpret_cast<uint8_t*>(u8_rgba_texels),
        a_dst_mip_2d_info, a_dst_mip_2d_layout, 
        [](uint8_t* out, const uint8_t* in) { rgbcx::encode_bc3(rgbcx_qual, out, in); }
    );
}


std::unique_ptr<uint8_t[]> spock::
xyz_to_bc5_encode_oct_normal_texture(
    const mip_2d_info_t & a_src_mip_2d_info, const void * a_src_mip0_buffer, 
    const mip_2d_info_t & a_dst_mip_2d_info, const mip_2d_layout_t & a_dst_mip_2d_layout
)
{
    const auto src_format = a_src_mip_2d_info.format();
    const auto dst_format = a_dst_mip_2d_info.format();
    const auto num_mips = a_src_mip_2d_info.num_mips();

    SPOCK_PARANOID(a_src_mip_2d_info.valid(), "input a_src_mip_2d_info is invalid");
    SPOCK_PARANOID(src_format == VK_FORMAT_R8G8B8A8_UNORM, "input format must be VK_FORMAT_R8G8B8A8_UNORM");
    SPOCK_PARANOID(dst_format == VK_FORMAT_BC5_UNORM_BLOCK, "output format must be VK_FORMAT_BC5_UNORM_BLOCK");
    SPOCK_PARANOID(
        num_mips == a_dst_mip_2d_info.num_mips(),
        "src_num_mip_levels and dst_num_mip_levels are not equal"
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_layout == a_dst_mip_2d_info.calc_mip_2d_layout(),
        "a_dst_mip_2d_info and a_dst_mip_2d_layout are not compatible"
    );

    host_timer_t timer;

    const mip_2d_info_t f32_mip_2d_info(VK_FORMAT_R32G32B32_SFLOAT, a_src_mip_2d_info);
    const mip_2d_info_t oct_mip_2d_info(VK_FORMAT_R8G8_UNORM, a_src_mip_2d_info);

    const auto oct_mip_2d_layout = oct_mip_2d_info.calc_mip_2d_layout();
    const auto& mip_dims = oct_mip_2d_layout.dims;
    const auto mip_texel_offsets = calc_mip_texel_offsets(mip_dims);
    const uint32_t num_mip_texels = mip_texel_offsets.back();
    const auto num_mip0_texels = mip_texel_offsets[1];

    
    const glm::u8vec4* src_texels = reinterpret_cast<const glm::u8vec4*>(a_src_mip0_buffer);
    bool found_nonpositive_z = false;
    std::unique_ptr<glm::u8vec2[]> unique_u8_oct_texels =
        convert_and_gen_mipmap_with_box_filter<glm::u8vec4, glm::vec3, glm::u8vec2>(
            src_texels, mip_dims,
            [&found_nonpositive_z](const glm::u8vec4& u8_xyz) ->glm::fvec3 {
                if (u8_xyz.x == 0 && u8_xyz.y == 0 && u8_xyz.z == 0) { // dummy normal texture
                    return glm::vec3(0.f);
                }
                glm::vec3 f32_xyz = u8_to_sf32_vec3(glm::u8vec3(u8_xyz));
                found_nonpositive_z = found_nonpositive_z || (f32_xyz.z <= 0.f);
                return glm::normalize(f32_xyz);
            },
            [](const glm::vec3& t_00, const glm::vec3& t_10, const glm::vec3& t_01, const glm::vec3& t_11) {
                const auto avg = 0.25f * (t_00 + t_10 + t_01 + t_11);
                if (glm::dot(avg, avg) <= std::numeric_limits<float>::epsilon()) return glm::vec3(0.f);
                return glm::normalize(avg);
            },
            [](const glm::vec3& v) -> glm::u8vec2 { 
                if (glm::dot(v, v) <= std::numeric_limits<float>::epsilon()) return glm::u8vec2(0);
                return sf32_to_u8_vec2(oct_encode_dir(v)); // u8 for bc5 encode
            }
        );

    SPOCK_ALERT(!found_nonpositive_z, "normal vector: z component should not be non-positive");

    auto u8_oct_texels = unique_u8_oct_texels.get();

    return compress_texture_2d(
        oct_mip_2d_info, oct_mip_2d_layout, reinterpret_cast<const uint8_t*>(u8_oct_texels),
        a_dst_mip_2d_info, a_dst_mip_2d_layout,
        [](uint8_t* out, const uint8_t* in) { rgbcx::encode_bc5(out, in, 0u, 1u, 2u); }
    );
}


/// input image always has format R8G8B8A8_SRGB or R8G8B8A8_UNORM, output is always VK_FORMAT_BC1_RGBA_UNORM_BLOCK
/// srgb to linear before mipmap generation, and use linear color for block compression
std::unique_ptr<uint8_t[]> spock::
gen_mipmap_and_bc1_alpha_encode_2d(
    const mip_2d_info_t & a_src_mip_2d_info, const void * a_src_mip0_buffer, 
    const mip_2d_info_t & a_dst_mip_2d_info, const mip_2d_layout_t & a_dst_mip_2d_layout, 
    float a_alpha_cutoff
)
{
    const auto src_format = a_src_mip_2d_info.format();
    const auto dst_format = a_dst_mip_2d_info.format();

    SPOCK_PARANOID(a_src_mip_2d_info.valid(), "a_mip_2d_info is invalid");
    SPOCK_PARANOID(get_texel_block_bytes(src_format) == 4, "src_format must have 4 channels for bc1 encode");
    SPOCK_PARANOID(src_format == VK_FORMAT_R8G8B8A8_SRGB, "src format must be VK_FORMAT_R8G8B8A8_SRGB");
    SPOCK_PARANOID(dst_format == VK_FORMAT_BC1_RGB_UNORM_BLOCK, "dst format must be VK_FORMAT_BC1_RGB_UNORM_BLOCK");
    SPOCK_PARANOID(
        a_src_mip_2d_info.num_mips() == a_dst_mip_2d_info.num_mips(),
        "src_num_mip_levels and dst_num_mip_levels are not equal"
    );
    SPOCK_PARANOID(
        a_dst_mip_2d_layout == a_dst_mip_2d_info.calc_mip_2d_layout(), 
        "a_dst_mip_2d_info and a_dst_mip_2d_layout are incompatible"
    );

    const auto src_mip_2d_layout = a_src_mip_2d_info.calc_mip_2d_layout();

    host_timer_t timer;

    const mip_2d_info_t f32_rgba_mip_2d_info(VK_FORMAT_R32G32B32A32_SFLOAT, a_src_mip_2d_info);
    const mip_2d_info_t u8_rgba_mip_2d_info(VK_FORMAT_R8G8B8A8_UNORM, a_src_mip_2d_info);

    const auto u8_rgba_mip_2d_layout = u8_rgba_mip_2d_info.calc_mip_2d_layout();
    const auto& mip_dims = u8_rgba_mip_2d_layout.dims;
    const auto mip_texel_offsets = calc_mip_texel_offsets(mip_dims);
    const uint32_t num_mip_texels = mip_texel_offsets.back();
    const auto num_mip0_texels = mip_texel_offsets[1];

    const glm::u8vec4* src_texels = reinterpret_cast<const glm::u8vec4*>(a_src_mip0_buffer);
    std::unique_ptr<glm::u8vec4[]> unique_u8_rgba_texels =
        convert_and_gen_mipmap_with_box_filter<glm::u8vec4, glm::vec4, glm::u8vec4>(
            src_texels, mip_dims,
            [a_alpha_cutoff](const glm::u8vec4& u8_rgba) {
                glm::vec4 f32_rgba = convertSRGBToLinear(u8_to_uf32_vec4(u8_rgba));
                return f32_rgba * static_cast<float>(f32_rgba.a >= a_alpha_cutoff);
            },
            [a_alpha_cutoff](const glm::vec4& t_00, const glm::vec4& t_10, const glm::vec4& t_01, const glm::vec4& t_11) {
                auto avg = 0.25f * (t_00 + t_10 + t_01 + t_11);
                avg.a = std::max({ t_00.a, t_10.a, t_01.a, t_11.a });
                return avg * static_cast<float>(avg.a >= a_alpha_cutoff);
            },
            [](const glm::vec4& v) { 
                return uf32_to_u8_vec4(v);
            }
        );

    auto u8_rgba_texels = unique_u8_rgba_texels.get();

    return compress_texture_2d(
        u8_rgba_mip_2d_info, u8_rgba_mip_2d_layout, reinterpret_cast<const uint8_t*>(u8_rgba_texels),
        a_dst_mip_2d_info, a_dst_mip_2d_layout,
        [](uint8_t* out, const uint8_t* in) { rgbcx::encode_bc1(rgbcx_qual, out, in, false, true); }
    );
}


void spock::
write_mipmaps_to_bmp(
    const std::string & a_image_path_base, 
    uint32_t a_num_channels, const mip_2d_layout_t & a_mip_2d_layout, const uint8_t * a_mip_buffer
)
{
    size_t num_mips = a_mip_2d_layout.dims.size();
    for (size_t i = 0; i < num_mips; ++i) {
        const uint8_t* buffer = a_mip_buffer + a_mip_2d_layout.byte_offsets[i];
        std::string mip_path = a_image_path_base + std::to_string(i) + ".bmp";
        int w = static_cast<int>(a_mip_2d_layout.dims[i].width);
        int h = static_cast<int>(a_mip_2d_layout.dims[i].height);
        int ch = static_cast<int>(a_num_channels);
        stbi_write_bmp(mip_path.c_str(), w, h, ch, buffer);
    }
}


void spock::
write_mipmaps_to_hdr(const std::string& a_image_path_base, uint32_t a_num_channels, const mip_2d_layout_t& a_mip_2d_layout, const float* a_mip_buffer)
{
    size_t num_mips = a_mip_2d_layout.dims.size();
    for (size_t i = 0; i < num_mips; ++i) {
        const float* data = a_mip_buffer + a_mip_2d_layout.byte_offsets[i] / sizeof(float);
        std::string mip_path = a_image_path_base + std::to_string(i) + ".hdr";
        int w = static_cast<int>(a_mip_2d_layout.dims[i].width);
        int h = static_cast<int>(a_mip_2d_layout.dims[i].height);
        int ch = static_cast<int>(a_num_channels);
        stbi_write_hdr(mip_path.c_str(), w, h, ch, data);
    }

}


std::string_view spock::str_from_mtl_type_e(mtl_type_e e)
{
    switch(e) {
        case spock::MTL_TYPE_PRIMARY: return "MTL_TYPE_PRIMARY";
        case spock::MTL_TYPE_GRAY_DOT: return "MTL_TYPE_GRAY_DOT";
        case spock::MTL_TYPE_ALPHA_CUTOFF: return "MTL_TYPE_ALPHA_CUTOFF";
        case spock::MTL_TYPE_BLEND: return "MTL_TYPE_BLEND";
        case spock::MTL_TYPE_COUNT: return "MTL_TYPE_COUNT";
    };
    return "Unknown mtl_type_e";
} // str_from_mtl_type_e(mtl_type_e)


std::string_view spock::str_from_punctual_light_type_e(punctual_light_type_e e)
{
    switch(e) {
        case spock::PUNCTUAL_LIGHT_TYPE_SPOT: return "PUNCTUAL_LIGHT_TYPE_SPOT";
        case spock::PUNCTUAL_LIGHT_TYPE_POINT: return "PUNCTUAL_LIGHT_TYPE_POINT";
        case spock::PUNCTUAL_LIGHT_TYPE_DIRECTIONAL: return "PUNCTUAL_LIGHT_TYPE_DIRECTIONAL";
        case spock::PUNCTUAL_LIGHT_TYPE_COUNT: return "PUNCTUAL_LIGHT_TYPE_COUNT";
    };
    return "Unknown punctual_light_type_e";
} // str_from_punctual_light_type_e(punctual_light_type_e)


uint32_t spock::
get_mtl_texture_stride(mtl_type_e a_mtl_type)
{
    auto ret = k_mtl_type_texture_stride_map.find(a_mtl_type);
    if (ret != k_mtl_type_texture_stride_map.end()) {
        return ret->second;
    }
    err(str_from_mtl_type_e(a_mtl_type).data() + std::string(" does not have textures"));
    return npos<uint32_t>();
}


bool spock::is_mtl_alpha_cutoff(mtl_type_e a_mtl_type)
{
    switch (a_mtl_type) {
    case MTL_TYPE_ALPHA_CUTOFF:
        return true;
    }
    return false;
}


spock::texture_descriptor_array_t::
texture_descriptor_array_t(texture_descriptor_array_t&& a_other) noexcept
    : context_                   { std::exchange(a_other.context_, nullptr ) }
    , vma_allocations_           { std::move(a_other.vma_allocations_) }
    , vk_images_                 { std::move(a_other.vk_images_) }
    , vk_image_views_            { std::move(a_other.vk_image_views_) }
    , vk_descriptor_image_infos_ { std::move(a_other.vk_descriptor_image_infos_) }
{
}


auto spock::texture_descriptor_array_t::
operator=(texture_descriptor_array_t && a_other) noexcept -> texture_descriptor_array_t&
{
    if (this != &a_other) {
        if (context_ != nullptr) {
            verbose("destroy texture_descriptor_array_t...");
            vk_descriptor_image_infos_.clear();
            for (auto vk_image_view : vk_image_views_) {
                vkDestroyImageView(*context_, vk_image_view, nullptr);
            }
            for (size_t i = 0; i < vk_images_.size(); ++i) {
                vmaDestroyImage(*context_, vk_images_[i], vma_allocations_[i]);
            }
        }
        context_                   = std::exchange(a_other.context_, nullptr );
        vma_allocations_           = std::move(a_other.vma_allocations_);
        vk_images_                 = std::move(a_other.vk_images_);
        vk_image_views_            = std::move(a_other.vk_image_views_);
        vk_descriptor_image_infos_ = std::move(a_other.vk_descriptor_image_infos_);
    }
    return *this;
}


spock::texture_descriptor_array_t::
texture_descriptor_array_t(const context_t & a_context)
    : context_ { &a_context }
{
}


spock::texture_descriptor_array_t::~texture_descriptor_array_t()
{
    if (context_ != nullptr ) {
        verbose("destroy texture_descriptor_array_t...");
        vk_descriptor_image_infos_.clear();
        for (auto vk_image_view : vk_image_views_) {
            vkDestroyImageView(*context_ , vk_image_view, nullptr);
        }
        for (size_t i = 0; i < vk_images_.size(); ++i) {
            vmaDestroyImage(*context_, vk_images_[i], vma_allocations_[i]);
        }
    }
}


bool spock::texture_descriptor_array_t::
consistent() const
{
    return vma_allocations_.size() == vk_images_.size() &&
        vk_images_.size() == vk_image_views_.size() && 
        vk_images_.size() == vk_descriptor_image_infos_.size();
}


bool spock::texture_descriptor_array_t::
empty() const
{
    return vma_allocations_.empty() &&
        vk_images_.empty() &&
        vk_image_views_.empty() &&
        vk_descriptor_image_infos_.empty();
}


void spock::texture_descriptor_array_t::
reserve(uint32_t num_textures)
{
    vma_allocations_.reserve(num_textures);
    vk_images_.reserve(num_textures);
    vk_image_views_.reserve(num_textures);
    vk_descriptor_image_infos_.reserve(num_textures);
}


/// texture_descriptor_array layout:
///     uint16_t: num_textures; 
///     mip_2d_info_t[num_textures];
///     mip_buffer[num_textures]; (with padding in between if necessary; alignment: mip_2d_info_t::BLOCK_ALIGNMENT_BYTES)
const uint8_t* spock::texture_descriptor_array_t::
upload(
    const context_t & a_context, const command_pool_t & a_command_pool, const queue_t & a_queue, 
    const std::vector<VkSampler>& a_vk_samplers,
    const uint8_t* a_bin_stream, uint64_t a_batch_max_mebibytes,
    VkPipelineStageFlags a_next_stages, VkAccessFlags a_next_accesses
)
{
    SPOCK_PARANOID(context_ != nullptr, "texture_descriptor_array_t is not properly initialized");
    SPOCK_PARANOID(empty(), "texture_descriptor_array_t is not empty");

    const auto batch_max_bytes = a_batch_max_mebibytes * 1024u * 1024u;
    const uint8_t* bin_stream = a_bin_stream;

    uint32_t num_textures = *reinterpret_cast<const uint16_t*>(bin_stream);
    SPOCK_PARANOID(num_textures > 0, "num_textures must be greater than 0");

    bin_stream += sizeof(uint16_t);

    vma_allocations_.resize(num_textures, VK_NULL_HANDLE);
    vk_images_.resize(num_textures, VK_NULL_HANDLE);
    vk_image_views_.resize(num_textures, VK_NULL_HANDLE);
    vk_descriptor_image_infos_.resize(num_textures, VkDescriptorImageInfo{});

    std::vector<mip_2d_info_t> mip_2d_infos;
    std::vector<mip_2d_layout_t> mip_2d_layouts;
    std::vector<VkDeviceSize> padded_mip_buffer_byteses;

    mip_2d_infos.reserve(num_textures);
    mip_2d_layouts.reserve(num_textures);
    padded_mip_buffer_byteses.reserve(num_textures);

    for (uint32_t i = 0; i < num_textures; ++i) {
        mip_2d_info_t mip_2d_info(&bin_stream);
        SPOCK_PARANOID(mip_2d_info.valid(), "mip_2d_info_t[" + std::to_string(i) + "] is not valid");

        auto mip_2d_layout = mip_2d_info.calc_mip_2d_layout();
        padded_mip_buffer_byteses.push_back(
            calc_padded(mip_2d_layout.get_mip_buffer_bytes(), mip_2d_info_t::BLOCK_ALIGNMENT_BYTES)
        );
        mip_2d_layouts.push_back(std::move(mip_2d_layout));
        mip_2d_infos.push_back(mip_2d_info);
    }
    // bin_stream now pointing to the first byte of the mip buffer

    uint32_t first_idx = 0, last_idx = 0; // begin_idx and last_idx: inclusive
    do {
        VkDeviceSize batch_bytes = 0u;
        while (last_idx < num_textures && batch_bytes < batch_max_bytes) {
            batch_bytes += padded_mip_buffer_byteses[last_idx];
            ++last_idx;
        }
        --last_idx;
        if (batch_bytes > batch_max_bytes && last_idx > first_idx) {
            batch_bytes -= padded_mip_buffer_byteses[last_idx];
            --last_idx;
        }
        uint32_t batch_num_textures = last_idx - first_idx + 1u;
        auto staging_buffer = stage(*context_, bin_stream, batch_bytes);
        bin_stream += batch_bytes;

        command_buffer_t cmd_buf{ a_command_pool };
        auto vk_command_buffer_begin_info = spock::make_VkCommandBufferBeginInfo();
        vk_command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf, &vk_command_buffer_begin_info));

        VmaAllocationCreateInfo allocation_create_info{};
        allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        VkDeviceSize uploaded_bytes = 0u;
        for (uint32_t i = first_idx; i <= last_idx; ++i) {
            const auto& mip_2d_info = mip_2d_infos[i];
            auto& mip_2d_layout = mip_2d_layouts[i];
            relocate_mip_buffer(uploaded_bytes, mip_2d_layout.byte_offsets);
            uploaded_bytes += padded_mip_buffer_byteses[i];

            auto vk_image_create_info = mip_2d_info.get_vk_image_create_info();
            SPOCK_VK_CALL(
                vmaCreateImage(
                    *context_, &vk_image_create_info, &allocation_create_info,
                    &vk_images_[i], &vma_allocations_[i], nullptr
                )
            );

            auto vk_image_view_create_info = mip_2d_info.get_vk_image_view_create_info(vk_images_[i]);
            SPOCK_VK_CALL(vkCreateImageView(*context_, &vk_image_view_create_info, nullptr, &vk_image_views_[i]));
            const auto sampler_idx = mip_2d_info.sampler_2d_idx();
            
            vk_descriptor_image_infos_[i].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            vk_descriptor_image_infos_[i].imageView   = vk_image_views_[i];
            vk_descriptor_image_infos_[i].sampler     = a_vk_samplers[sampler_idx];

            cmd_copy_staging_mip_buffer_to_image_2d(
                cmd_buf, mip_2d_layout, staging_buffer, vk_images_[i], VK_IMAGE_ASPECT_COLOR_BIT,
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
            );
        }

        SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf));
        const auto submit_info = build_VkSubmitInfo(1, &cmd_buf());
        SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
        SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));

        ++last_idx;
        first_idx = last_idx;

    } while (first_idx < num_textures);

    return bin_stream;
}


void spock::texture_descriptor_array_t::
upload_one_texture(
    const context_t & a_context, const command_pool_t & a_command_pool, const queue_t & a_queue, VkSampler a_vk_sampler, 
    const mip_2d_info_t & a_mip_2d_info, const mip_2d_layout_t & a_mip_2d_layout, const uint8_t* a_mip_2d_buffer,
    VkPipelineStageFlags a_next_stages, VkAccessFlags a_next_accesses
)
{
    SPOCK_PARANOID(context_ != nullptr, "texture_descriptor_array_t is not properly initialized");
    SPOCK_PARANOID(a_mip_2d_info.valid(), "a_mip_2d_info is not valid");
    SPOCK_PARANOID(a_mip_2d_info.calc_mip_2d_layout() == a_mip_2d_layout, "a_mip_2d_info and a_mip_2d_layout do not match");
    SPOCK_PARANOID(a_vk_sampler != VK_NULL_HANDLE, "a_vk_sampler cannot be a null handle");
    SPOCK_PARANOID(consistent(), "texture_descriptor_array_t is not consistent");

    VmaAllocationCreateInfo allocation_create_info{};
    allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    auto vk_image_create_info = a_mip_2d_info.get_vk_image_create_info();
    image_t image(*context_, vk_image_create_info, allocation_create_info);
    
    VkDescriptorImageInfo vk_descriptor_image_info{};

    auto vk_image_view_create_info = a_mip_2d_info.get_vk_image_view_create_info(image);
    image_view_t image_view(*context_, vk_image_view_create_info);

    command_buffer_t cmd_buf{ a_command_pool };
    auto vk_command_buffer_begin_info = make_VkCommandBufferBeginInfo();
    vk_command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf, &vk_command_buffer_begin_info));

    auto staging_buffer = cmd_upload_to_image_2d(
        a_context, cmd_buf, image, a_mip_2d_layout, a_mip_2d_buffer, VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
    );

    SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf));
    const auto submit_info = build_VkSubmitInfo(1, &cmd_buf());
    SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
    SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));

    VmaAllocation vma_allocation = VK_NULL_HANDLE;
    VkImage vk_image = VK_NULL_HANDLE;
    std::tie(vma_allocation, vk_image) = image.release();
    VkImageView vk_image_view = image_view.release();

    vk_descriptor_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    vk_descriptor_image_info.imageView   = vk_image_view;
    vk_descriptor_image_info.sampler     = a_vk_sampler;

    vma_allocations_.push_back(vma_allocation);
    vk_images_.push_back(vk_image);
    vk_image_views_.push_back(vk_image_view);
    vk_descriptor_image_infos_.push_back(vk_descriptor_image_info);
}


spock::sampler_array_t::
sampler_array_t(sampler_array_t && a_sampler_array) noexcept
    : vk_dev_ (std::exchange(a_sampler_array.vk_dev_, VK_NULL_HANDLE))
    , vk_samplers_(std::move(a_sampler_array.vk_samplers_))
{
}


spock::sampler_array_t & spock::sampler_array_t::
operator=(sampler_array_t && a_sampler_array) noexcept
{
    vk_dev_ = a_sampler_array.vk_dev_;
    for (auto vk_sampler : vk_samplers_) {
        vkDestroySampler(vk_dev_, vk_sampler, nullptr);
    }
    vk_samplers_ = std::move(a_sampler_array.vk_samplers_);
    a_sampler_array.vk_dev_ = VK_NULL_HANDLE;
    return *this;
}


spock::sampler_array_t::
sampler_array_t(const context_t& a_context, const std::vector<sampler_2d_info_t>& a_sampler_2d_infos)
    : vk_dev_{ a_context }
{
    const auto num_samplers = a_sampler_2d_infos.size();
    const float sampler_anisotropy = a_context.max_sampler_anisotropy();
    vk_samplers_.resize(num_samplers, VK_NULL_HANDLE);
    VkResult result = VK_SUCCESS;
    uint32_t num_created = 0;
    while (num_created < num_samplers) {
        auto vk_sampler_create_info = a_sampler_2d_infos[num_created].get_vk_sampler_create_info(sampler_anisotropy);
        result = vkCreateSampler(vk_dev_, &vk_sampler_create_info, nullptr, &vk_samplers_[num_created]);
        if (result != VK_SUCCESS) {
            err(std::string("vkCreateSampler failed: ") + std::string(str_from_VkResult(result)));
            break;
        }
        num_created += 1;
    }
    if (num_created != num_samplers) {
        for (uint32_t i = 0; i < num_created; ++i) {
            vkDestroySampler(vk_dev_, vk_samplers_[i], nullptr);
        }
        except("failed to construct sampler_array_t");
    }
}


spock::sampler_array_t::
~sampler_array_t()
{
    if (vk_samplers_.size() > 0) {
        verbose("destroy sampler array...");
        for (auto vk_sampler : vk_samplers_) {
            vkDestroySampler(vk_dev_, vk_sampler, nullptr);
        }
    }
}


spock::mtl_type_e spock::mtl_cfg_t::
get_mtl_type() const
{
    SPOCK_PARANOID(!(alpha_mask_enabled && alpha_blend_enabled), "bug: alpha cutoff and alpha blend cannot be both on");
    using namespace std::literals;
    constexpr std::string_view gray_dot_mtl = "SPOCK_GRAY_DOT"sv;
    if (name.rfind(gray_dot_mtl) == name.length() - gray_dot_mtl.length()) {
        ensure(!(alpha_mask_enabled || alpha_blend_enabled), name + " error: neither alpha cutoff nor alpha blend should be on for gray dot mtl");
        return MTL_TYPE_GRAY_DOT;
    }
    if (alpha_mask_enabled) {
        return MTL_TYPE_ALPHA_CUTOFF;
    }
    if (alpha_blend_enabled) {
        return MTL_TYPE_BLEND;
    }
    return MTL_TYPE_PRIMARY;
}


spock::directional_light_t::
directional_light_t()
    : direction{ -1.f, -1.f, -0.5f } // when loading from gltf, { 0.f,  0.f, -1.f }
    , padding0{ 0.f }
    , intensity{ 5.f, 5.f, 5.f } // when loading from gltf, { 1.f, 1.f, 1.f }
    , padding1{ 0.f }
{
    static_assert(sizeof(directional_light_t) == DIRECTIONAL_LIGHT_BYTES, "DIRECTIONAL_LIGHT_BYTES is incorrect");
    direction = glm::normalize(direction);
}


std::array<uint8_t, spock::directional_light_t::DIRECTIONAL_LIGHT_BYTES> spock::directional_light_t::
serialize() const 
{
    return serialize_pod<DIRECTIONAL_LIGHT_BYTES>(this);
}


void spock::directional_light_t::
deserialize(uint8_t** a_bin_stream) 
{
    deserialize_pod<DIRECTIONAL_LIGHT_BYTES>(a_bin_stream, this);
}


std::string spock::directional_light_t::
definition_to_glsl_str() {
    return std::string{
        "struct scene_directional_light_t {\n"
        "    vec3 direction;\n"
        "    float padding0;\n"
        "    vec3 intensity;\n"
        "    float padding1;\n"
        "};\n\n"
    };
}


spock::point_light_t::
point_light_t()
    : position{ 0.f, 5.f, 0.f} // when loading from gltf, { 0.f,  0.f, 0.f }
    , padding{ 0.f }
    , intensity {10.f, 10.f, 10.f} // when loading from gltf, { 1.f, 1.f, 1.f }
    , rcp_range{ 0.f }
{
    static_assert(sizeof(point_light_t) == POINT_LIGHT_BYTES, "POINT_LIGHT_BYTES is incorrect");
}


std::array<uint8_t, spock::point_light_t::POINT_LIGHT_BYTES> spock::point_light_t::
serialize() const 
{
    return serialize_pod<POINT_LIGHT_BYTES>(this);
}


void spock::point_light_t::
deserialize(uint8_t** a_bin_stream) 
{
    deserialize_pod<POINT_LIGHT_BYTES>(a_bin_stream, this);
}


std::string spock::point_light_t::
definition_to_glsl_str() {
    return std::string{
        "struct scene_point_light_t {\n"
        "    vec3 position;\n"
        "    float rcp_range;\n"
        "    vec3 intensity;\n"
        "    float padding;\n"
        "};\n\n"
    };
}


std::pair<float,float> spock::
calc_spotlight_falloff_params(float a_inner_cone_angle, float a_outer_cone_angle)
{
    const float cos_outer_cone_angle = std::cos(a_outer_cone_angle);
    const float angle_scale = 1.f / std::max(0.001f, std::cos(a_inner_cone_angle) - cos_outer_cone_angle);
    const float angle_offset = -cos_outer_cone_angle * angle_scale;
    return std::make_pair(angle_scale, angle_offset);
}


spock::spotlight_t::spotlight_t() 
    : position{ 0.f, 5.f, 0.f } // when loading from gltf, { 0.f,  0.f,  0.f }
    , rcp_range{ 0.f }
    , direction{ 0.f, -1.f, 0.f } // when loading from gltf, { 0.f,  0.f, -1.f }
    , intensity{ 10.f, 10.f, 10.f } // when loading from gltf, { 1.f, 1.f, 1.f }
{
    static_assert(sizeof(spotlight_t) == SPOTLIGHT_BYTES, "SPOTLIGHT_BYTES is incorrect");

    direction = glm::normalize(direction);
    const float inner_cone_angle = 0.f;
    constexpr float outer_cone_angle = glm::pi<float>() / 4.f;
    const float cos_outer_cone_angle = std::cos(outer_cone_angle);
    std::tie(angle_scale, angle_offset) = calc_spotlight_falloff_params(inner_cone_angle, outer_cone_angle);
}


std::array<uint8_t, spock::spotlight_t::SPOTLIGHT_BYTES> spock::spotlight_t::
serialize() const 
{
    return serialize_pod<SPOTLIGHT_BYTES>(this);
}


void spock::spotlight_t::
deserialize(uint8_t** a_bin_stream) 
{
    deserialize_pod<SPOTLIGHT_BYTES>(a_bin_stream, this);
}


std::string spock::spotlight_t::
definition_to_glsl_str() {
    return std::string{
        "struct scene_spotlight_t {\n"
        "    vec3 position;\n"
        "    float rcp_range;\n"
        "    vec3 direction;\n"
        "    float angle_scale;\n"
        "    vec3 intensity;\n"
        "    float angle_offset;\n"
        "};\n\n"
    };
}


void spock::scene_t::init_buffer_usage_flags(spock_scene_flags_t a_scene_flags)
{
    position_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    normal_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    uv_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    index_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
    triangle_mtl_idx_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    offsets_of_mtl_type_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    punctual_light_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    mtl_param_buffer_usage_ = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

    if (a_scene_flags & SPOCK_SCENE_POSITION_NORMAL_UNIFORM_TEXEL_BUFFER_BIT) {
        position_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
        normal_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
    }
    if (a_scene_flags & SPOCK_SCENE_POSITION_NORMAL_STORAGE_BUFFER_BIT) {
        position_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        normal_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    }
    if (a_scene_flags & SPOCK_SCENE_ALIGNED_BUFFER_UNIFORM_TEXEL_BUFFER_BIT) {
        uv_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
        index_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
        triangle_mtl_idx_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
        punctual_light_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
        mtl_param_buffer_usage_ |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
    }
    if (a_scene_flags & SPOCK_SCENE_ALIGNED_BUFFER_STORAGE_BUFFER_BIT) {
        uv_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        index_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        triangle_mtl_idx_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        punctual_light_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        mtl_param_buffer_usage_ |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    }
    if (a_scene_flags & SPOCK_SCENE_RAY_TRACING_SUPPORT_BIT) {
        position_buffer_usage_ |= VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
        index_buffer_usage_ |= VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
    }
}


uint32_t spock::scene_t::
get_curr_cache_version() const
{
    return SCENE_CACHE_VERSION;
}


// *a_bin_stream_ptr: points to the beginning of gltf_paths_count
///     uint16_t: gltf_paths_count
///     uint8_t[gltf_paths_bytes]: gltf_paths_count of null-terminated strings ; cache_body begins from next line
std::vector<const char*> spock::scene_t::
deserialize_gltf_paths(const uint8_t ** a_bin_stream_ptr)
{
    const uint16_t num_files = *reinterpret_cast<const uint16_t*>(*a_bin_stream_ptr);
    SPOCK_PARANOID(num_files > 0, "scene_t::deserialize_gltf_paths failed: there is no gltf file");
    *a_bin_stream_ptr += sizeof(uint16_t);
    std::vector<const char*> paths;
    paths.reserve(num_files);
    for (uint16_t i = 0; i < num_files; ++i) {
        paths.push_back(reinterpret_cast<const char*>(*a_bin_stream_ptr));
        while (**a_bin_stream_ptr != 0) {
            *a_bin_stream_ptr += 1;
        }
        *a_bin_stream_ptr += 1;
    }
    return paths;
}


///     uint32_t: gltf_paths_bytes (does not include this uint32_t, because deserialization starts from gltf_path_count)
///     uint16_t: gltf_paths_count
///     uint8_t[gltf_paths_bytes]: gltf_paths_count of null-terminated strings ; cache_body begins from next line
void spock::scene_t::
serialize_gltf_paths(const std::vector<std::string> & a_file_paths, std::ofstream& a_ofs)
{
    constexpr uint16_t num_gltf_files_max = std::numeric_limits<uint16_t>::max();

    SPOCK_PARANOID(
        a_file_paths.size() <= num_gltf_files_max, 
        "there can not be more than " +std::to_string(num_gltf_files_max) + " gltf files"
    );

    VkDeviceSize bin_stream_bytes =  sizeof(uint16_t); // bytes of strings, number of strings
    for (const auto& p : a_file_paths) {
        bin_stream_bytes += p.size() + 1; // NULL(0) terminated
    }
    std::unique_ptr<uint8_t[]> unique_bin_stream{ new uint8_t[bin_stream_bytes] };
    uint8_t* bin_stream = unique_bin_stream.get();
    *reinterpret_cast<uint16_t*>(bin_stream) = static_cast<uint16_t>(a_file_paths.size());
    bin_stream += sizeof(uint16_t);

    for (const auto& p : a_file_paths) {
        const char* c_p = p.c_str();
        const auto bytes = p.size() + 1;
        memcpy(bin_stream, c_p, bytes);
        bin_stream += bytes;
    }
    SPOCK_PARANOID(bin_stream == unique_bin_stream.get() + bin_stream_bytes, "bugs in scene_t::serialize_gltf_paths");
    a_ofs.write(reinterpret_cast<const char*>(&bin_stream_bytes), sizeof(uint32_t));
    a_ofs.write(reinterpret_cast<const char*>(unique_bin_stream.get()), bin_stream_bytes);

}


void spock::scene_t::
load_sampler_from_cache(const uint8_t** a_bin_stream)
{
    uint32_t num_samplers = *reinterpret_cast<const uint16_t*>(*a_bin_stream);
    SPOCK_PARANOID(num_samplers > 0, "there must be at least one sampler in the scene cache");
    *a_bin_stream += sizeof(uint16_t);

    std::vector<sampler_2d_info_t> sampler_2d_infos;
    sampler_2d_infos.reserve(num_samplers);
    for (uint32_t i = 0; i < num_samplers; ++i) {
        sampler_2d_info_t sampler_2d_info(a_bin_stream);
        auto vk_sampler_create_info = sampler_2d_info.get_vk_sampler_create_info(context_->max_sampler_anisotropy());
        sampler_2d_infos.push_back(sampler_2d_info);
    }
    sampler_array_ = sampler_array_t(*context_, sampler_2d_infos);
}


/// scene body binary layout:
///     float[3]: aabb_lower_ // cache_body starts from this line
///     float[3]: aabb_upper_
///     uint32_t[num_material_types + 1]: vertex_offsets_
///     uint32_t[num_material_types + 1]: index_offsets_
///     uint32_t[num_material_types + 1]: mtl_param_emissive_idx_offsets_
///     uint32_t[num_material_types + 1]: mtl_param_alpha_cutoff_offsets_
///     uint32_t: num_emissive_textures_; // at least one black texture
///     uint32_t[MTL_TYPE_COUNT]: texture_strides_
///     uint32_t[MTL_TYPE_COUNT + 1]: texture_offsets_; // texture index offset for each mtl_type
///     position_buffer
///     normal_buffer
///     uv_buffer
///     index_buffer
///     triangle_mtl_idx_buffer
///     mtl_param_buffer_; // scene_t::mtl_param_type[], uniform texel buffer
///     uint16_t: num_sampler_2d_info
///     sampler_2d_info_t[num_samplers]
///     texture_descriptor_array_
///         uint16_t: num_textures; 
///         mip_2d_info_t[num_textures];
///         mip_buffer[num_textures]; (with padding in between if necessary; alignment: mip_2d_info_t::BLOCK_ALIGNMENT_BYTES)
void spock::scene_t::
load_from_cache(const command_pool_t& a_command_pool, const queue_t& a_queue, const uint8_t** a_bin_stream)
{
    memcpy(&aabb_lower_[0], *a_bin_stream, sizeof(glm::vec3));
    *a_bin_stream += sizeof(glm::vec3);
    memcpy(&aabb_upper_[0], *a_bin_stream, sizeof(glm::vec3));
    *a_bin_stream += sizeof(glm::vec3);
    const size_t vertex_offsets_bytes = sizeof(vertex_offsets_);
    memcpy(vertex_offsets_.data(), *a_bin_stream, vertex_offsets_bytes);
    *a_bin_stream += vertex_offsets_bytes;
    const size_t index_offsets_bytes = sizeof(index_offsets_);
    memcpy(index_offsets_.data(), *a_bin_stream, index_offsets_bytes);
    *a_bin_stream += index_offsets_bytes;
    const size_t mtl_param_emissive_idx_offsets_bytes = sizeof(mtl_param_emissive_idx_offsets_);
    memcpy(&mtl_param_emissive_idx_offsets_, *a_bin_stream, mtl_param_emissive_idx_offsets_bytes);
    *a_bin_stream += mtl_param_emissive_idx_offsets_bytes;
    SPOCK_PARANOID(mtl_param_emissive_idx_offsets_bytes > 0, "mtl_param_emissive_idx_offsets_ must be non-empty");
    const size_t mtl_param_alpha_cutoff_offsets_bytes = sizeof(mtl_param_alpha_cutoff_offsets_);
    memcpy(&mtl_param_alpha_cutoff_offsets_, *a_bin_stream, mtl_param_alpha_cutoff_offsets_bytes);
    *a_bin_stream += mtl_param_alpha_cutoff_offsets_bytes;
    SPOCK_PARANOID(mtl_param_alpha_cutoff_offsets_bytes > 0, "mtl_param_alpha_cutoff_offsets_ must be non-empty");

    memcpy(&num_emissive_textures_, *a_bin_stream, sizeof(uint32_t));
    *a_bin_stream += sizeof(uint32_t);
    const size_t texture_strides_bytes = sizeof(uint32_t) * MTL_TYPE_COUNT;
    memcpy(texture_strides_.data(), *a_bin_stream, texture_strides_bytes);
    *a_bin_stream += texture_strides_bytes;
    const size_t texture_offsets_bytes = sizeof(uint32_t) * (MTL_TYPE_COUNT + 1);
    memcpy(texture_offsets_.data(), *a_bin_stream, texture_offsets_bytes);
    *a_bin_stream += texture_offsets_bytes;

    const size_t punctual_light_buffer_bytes = get_punctual_light_buffer_bytes(); // buffer stores in cache header
    if (punctual_light_buffer_bytes > k_maxUniformBufferRange_min) {
        warn("punctual_light_buffer_bytes is larger than lower bound of VkPhysicalDeviceLimits::maxUniformBufferRange");
    }
    if (punctual_light_buffer_bytes > context_->max_uniform_buffer_range()) {
        warn("punctual_light_buffer_bytes should not exceed VkPhysicalDeviceLimits::maxUniformBufferRange");
    }

    const auto offsets_of_mtl_types = assemble_offsets_of_mtl_types();

    const size_t position_buffer_bytes = vertex_offsets_[MTL_TYPE_COUNT] * sizeof(glm::vec3);
    const size_t normal_buffer_bytes = position_buffer_bytes;
    const auto num_vertices = vertex_offsets_[MTL_TYPE_COUNT];
    const size_t uv_buffer_bytes = num_vertices * sizeof(glm::vec2);
    const size_t index_buffer_bytes = index_offsets_[MTL_TYPE_COUNT] * sizeof(uint32_t);
    const size_t triangle_mtl_idx_buffer_bytes = index_offsets_[MTL_TYPE_COUNT] / 3 * sizeof(triangle_mtl_idx_type);
    const size_t mtl_param_buffer_bytes = get_mtl_param_buffer_bytes();

    if (mtl_param_buffer_bytes > k_maxUniformBufferRange_min) {
        warn("mtl_param_buffer_bytes is larger than the lower bound of VkPhysicalDeviceLimits::maxUniformBufferRange");
    }
    if (mtl_param_buffer_bytes > context_->max_uniform_buffer_range()) {
        warn("mtl_param_buffer_bytes is larger than VkPhysicalDeviceLimits::maxUniformBufferRange");
    }

    VmaAllocationCreateInfo mem_allocation_create_info{};
    mem_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    auto buffer_create_info = make_VkBufferCreateInfo();

    buffer_create_info.size = punctual_light_buffer_bytes;
    buffer_create_info.usage = punctual_light_buffer_usage_;
    punctual_light_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = get_offsets_of_mtl_type_buffer_bytes();
    buffer_create_info.usage = offsets_of_mtl_type_buffer_usage_;
    offsets_of_mtl_type_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = position_buffer_bytes;
    buffer_create_info.usage = position_buffer_usage_;
    position_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);
    buffer_create_info.size = normal_buffer_bytes;
    buffer_create_info.usage = normal_buffer_usage_;
    normal_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);
    buffer_create_info.size = uv_buffer_bytes;
    buffer_create_info.usage = uv_buffer_usage_;
    uv_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = index_buffer_bytes;
    buffer_create_info.usage = index_buffer_usage_;
    index_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = triangle_mtl_idx_buffer_bytes;
    buffer_create_info.usage = triangle_mtl_idx_buffer_usage_;
    triangle_mtl_idx_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = mtl_param_buffer_bytes;
    buffer_create_info.usage = mtl_param_buffer_usage_;
    mtl_param_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    // upload buffers
    {
        command_buffer_t cmd_buf{ a_command_pool };
        auto vk_command_buffer_begin_info = spock::make_VkCommandBufferBeginInfo();
        vk_command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf, &vk_command_buffer_begin_info));

        auto punctual_light_staging_buffer = punctual_light_buffer_.cmd_upload(cmd_buf, unique_punctual_light_buffer_.get());

        auto offsets_of_mtl_tpye_staging_buffer = offsets_of_mtl_type_buffer_.cmd_upload(cmd_buf, offsets_of_mtl_types.data());

        auto position_staging_buffer = position_buffer_.cmd_upload(cmd_buf, *a_bin_stream);
        *a_bin_stream += position_buffer_bytes;
        auto normal_staging_buffer = normal_buffer_.cmd_upload(cmd_buf, *a_bin_stream);
        *a_bin_stream += normal_buffer_bytes;
        buffer_t uv_staging_buffer;
        uv_staging_buffer = uv_buffer_.cmd_upload(cmd_buf, *a_bin_stream);
        *a_bin_stream += uv_buffer_bytes;
        auto index_staging_buffer = index_buffer_.cmd_upload(cmd_buf, *a_bin_stream);
        *a_bin_stream += index_buffer_bytes;

        auto triangle_mtl_idx_staging_buffer = triangle_mtl_idx_buffer_.cmd_upload(cmd_buf, *a_bin_stream);
        *a_bin_stream += triangle_mtl_idx_buffer_bytes;

        auto mtl_param_staging_buffer = mtl_param_buffer_.cmd_upload(cmd_buf, *a_bin_stream);
        *a_bin_stream += mtl_param_buffer_bytes;

        SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf));
        const auto submit_info = build_VkSubmitInfo(1, &cmd_buf());
        SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
        SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));
    }
    load_sampler_from_cache(a_bin_stream);
    *a_bin_stream = texture_descriptor_array_.upload(
        *context_, a_command_pool, a_queue, 
        sampler_array_.vk_samplers(), *a_bin_stream, 256, 
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
    );

}


std::unique_ptr<uint8_t[]> spock::scene_t::
get_cache_body(
    const std::string& a_cache_path,
    const std::vector<std::string>& a_gltf_paths,
    const directional_light_t& a_fallback_directional_light
)
{
    //TODO: enable load from cache
    //return nullptr;
    using namespace std::filesystem;

    std::unique_ptr<uint8_t[]> cache_body;
    std::ifstream ifs(a_cache_path, std::ios::in | std::ios::ate | std::ios::binary);
    const auto cache_file_bytes = static_cast<size_t>(ifs.tellg());
    auto cache_body_bytes = cache_file_bytes;
    const uint32_t curr_cache_version = get_curr_cache_version();
    uint32_t trailing_cache_version = 0;
    if (cache_file_bytes < 2 * sizeof(uint32_t)) {
        info("size of cache file " + a_cache_path + " is invalid");
        return cache_body;
    }
    ifs.seekg(cache_file_bytes - sizeof(uint32_t));

    ifs.read(reinterpret_cast<char*>(&trailing_cache_version), sizeof(uint32_t));
    cache_body_bytes -= sizeof(uint32_t);

    if (trailing_cache_version != curr_cache_version) {
        info("trailing cache version of cache file " + a_cache_path + " is invalid");
        return cache_body;
    }

    uint32_t leading_cache_version = 0;
    uint32_t cached_num_material_types = 0;

    ifs.seekg(0);

    ifs.read(reinterpret_cast<char*>(&leading_cache_version), sizeof(uint32_t));
    ifs.read(reinterpret_cast<char*>(&cached_num_material_types), sizeof(uint32_t));
    cache_body_bytes -= 2 * sizeof(uint32_t);

    if (leading_cache_version != curr_cache_version) {
        info("leading cache version of cache file " + a_cache_path + " is invalid");
        return cache_body;
    }
    if (cached_num_material_types != mtl_type_e::MTL_TYPE_COUNT) {
        info("material type count of cache file " + a_cache_path + " is invalid");
        return cache_body;
    }

    uint32_t cached_gltf_paths_bytes = 0;

    ifs.read(reinterpret_cast<char*>(&cached_gltf_paths_bytes), sizeof(uint32_t)); // this size is not included in cached_gltf_paths_bytes
    cache_body_bytes -= sizeof(uint32_t);

    std::unique_ptr<uint8_t[]> unique_cached_gltf_paths_bin_stream{ new uint8_t[cached_gltf_paths_bytes] };

    ifs.read(reinterpret_cast<char*>(unique_cached_gltf_paths_bin_stream.get()), cached_gltf_paths_bytes);
    ensure(cache_body_bytes > cached_gltf_paths_bytes, "cached_gltf_paths_bytes is too large");
    cache_body_bytes -= cached_gltf_paths_bytes;

    ifs.read(reinterpret_cast<char*>(punctual_light_counts_.data()), sizeof(punctual_light_counts_));
    cache_body_bytes -= sizeof(punctual_light_counts_);

    ifs.read(reinterpret_cast<char*>(punctual_light_offsets_.data()), sizeof(punctual_light_offsets_));
    cache_body_bytes -= sizeof(punctual_light_offsets_);

    unique_punctual_light_buffer_.reset(new glm::vec4[punctual_light_offsets_.back()]);
    const auto punctual_light_buffer_bytes = get_punctual_light_buffer_bytes();
    ifs.read(reinterpret_cast<char*>(unique_punctual_light_buffer_.get()), punctual_light_buffer_bytes);
    cache_body_bytes -= sizeof(punctual_light_buffer_bytes);

    if (punctual_light_counts_[PUNCTUAL_LIGHT_TYPE_COUNT] == -1) {
        SPOCK_PARANOID(
            punctual_light_counts_[PUNCTUAL_LIGHT_TYPE_DIRECTIONAL] == 1, 
            "there should be only one user-provided directional light"
        );
        directional_light_t* cached_light = reinterpret_cast<directional_light_t*>(unique_punctual_light_buffer_.get());

        if (*cached_light != a_fallback_directional_light) {
            info("user-provided directional light in cache file " + a_cache_path + " is not equal to the current fallback directional light");
            return cache_body;
        }
    }

    const uint8_t* cached_gltf_paths_bin_stream = unique_cached_gltf_paths_bin_stream.get();
    auto cached_gltf_paths = deserialize_gltf_paths(&cached_gltf_paths_bin_stream);

    const size_t num_valid_gltf_paths = std::count_if(
        a_gltf_paths.cbegin(), a_gltf_paths.cend(), [](const std::string_view& gltf_path) { return exists(gltf_path); }
    );
    if (num_valid_gltf_paths > 0) {
        ensure(num_valid_gltf_paths == a_gltf_paths.size(), "only some of the specified gltf paths are valid");
        if (cached_gltf_paths.size() != a_gltf_paths.size()) return cache_body;

        bool cached_paths_outdated = std::mismatch(
            cached_gltf_paths.cbegin(), cached_gltf_paths.cend(),
            a_gltf_paths.cbegin(), a_gltf_paths.cend(),
            [](const char* cached, const std::string_view& gltf_path) {return strcmp(cached, gltf_path.data()) == 0; }
        ).first != cached_gltf_paths.cend();

        if (cached_paths_outdated) {
            info("cached gltf paths outdated");
            return cache_body;
        }
        const auto cache_timestamp = last_write_time(a_cache_path);
        const bool cache_timestamp_outdated = std::any_of(
            a_gltf_paths.cbegin(), a_gltf_paths.cend(),
            [&cache_timestamp](const std::string_view& gltf_path) {
                return earlier_than(cache_timestamp, last_write_time(gltf_path));
            }
        );
        if (cache_timestamp_outdated) {
            info("gltf files changed");
            return cache_body;
        }
    }
    else {
        info("src gltf files not found, cache update is disabled");
    }
    cache_body.reset(new uint8_t[cache_body_bytes]);
    ifs.read(reinterpret_cast<char*>(cache_body.get()), cache_body_bytes);
    return cache_body;
}


void spock::scene_t::serialize_buffers(
    const command_pool_t& a_command_pool, const queue_t& a_queue, const std::vector<mtl_cfg_t>* a_mtl_cfgs,
    const uint32_t* a_vertex_counts, const uint32_t* a_index_counts,
    const std::vector<primitive_info_t>& a_primitive_infos, const std::vector<glm::mat4>& a_mesh_transforms,
    std::ofstream& a_ofs
)
{
    // material param buffer
    // mtl_param_emissive_idx
    for (size_t i = 1; i < MTL_TYPE_COUNT + 1; ++i) {
        const uint32_t param_count = static_cast<uint32_t>(a_mtl_cfgs[i - 1].size()) * k_mtl_param_emissive_idx_stride;
        mtl_param_emissive_idx_offsets_[i] = param_count;
        mtl_param_emissive_idx_offsets_[i] += mtl_param_emissive_idx_offsets_[i - 1];
    }
    const uint32_t mtl_param_emissive_idx_padding = calc_padding(mtl_param_emissive_idx_offsets_[MTL_TYPE_COUNT], k_mtl_param_alignment);
    mtl_param_emissive_idx_offsets_[MTL_TYPE_COUNT] += mtl_param_emissive_idx_padding;

    // mtl_param_alpha_cutoff
    mtl_param_alpha_cutoff_offsets_[0] = mtl_param_emissive_idx_offsets_[MTL_TYPE_COUNT];
    for (size_t i = 1; i < MTL_TYPE_COUNT + 1; ++i) {
        const auto last_mtl_type = static_cast<mtl_type_e>(i - 1);
        if (is_mtl_alpha_cutoff(last_mtl_type)) {
            const uint32_t param_count = static_cast<uint32_t>(a_mtl_cfgs[i - 1].size()) * k_mtl_param_alpha_cutoff_stride;
            mtl_param_alpha_cutoff_offsets_[i] = param_count;
        }
        mtl_param_alpha_cutoff_offsets_[i] += mtl_param_alpha_cutoff_offsets_[i - 1];
    }
    const uint32_t mtl_param_alpha_cutoff_padding = calc_padding(mtl_param_alpha_cutoff_offsets_[MTL_TYPE_COUNT], k_mtl_param_alignment);
    mtl_param_alpha_cutoff_offsets_[MTL_TYPE_COUNT] += mtl_param_alpha_cutoff_padding;

    const size_t mtl_param_buffer_bytes = get_mtl_param_buffer_bytes();

    SPOCK_PARANOID(mtl_param_buffer_bytes > 0, "mtl_param_rgba_unorm_buffer_ should not be empty");
    if (mtl_param_buffer_bytes > k_maxUniformBufferRange_min) {
        warn("mtl_param_buffer_bytes is larger than lower bound of VkPhysicalDeviceLimits::maxUniformBufferRange");
    }
    if (mtl_param_buffer_bytes > context_->max_uniform_buffer_range()) {
        warn("mtl_param_buffer_bytes is greater than VkPhysicalDeviceLimits::maxUniformBufferRange");
    }
    std::unique_ptr<uint8_t> unique_mtl_param_buffer{ new uint8_t[mtl_param_buffer_bytes] };
    auto mtl_param_base_ptr = reinterpret_cast<mtl_param_type*>(unique_mtl_param_buffer.get());
    for (size_t i = 0; i < static_cast<size_t>(MTL_TYPE_COUNT); ++i) {
        std::transform(std::execution::par_unseq,
            a_mtl_cfgs[i].cbegin(), a_mtl_cfgs[i].cend(),
            mtl_param_base_ptr + mtl_param_emissive_idx_offsets_[i],
            [](const auto& mtl_cfg) { 
                return mtl_cfg.emissive_texture_idx; 
            }
        );
        if (is_mtl_alpha_cutoff(static_cast<mtl_type_e>(i))) {
            std::transform(std::execution::par_unseq,
                a_mtl_cfgs[i].cbegin(), a_mtl_cfgs[i].cend(),
                mtl_param_base_ptr + mtl_param_alpha_cutoff_offsets_[i],
                [](const auto& mtl_cfg) { 
                    return glm::floatBitsToUint(mtl_cfg.alpha_mask_cutoff); 
                }
            );
        }
    }

    // initialize padding
    if (mtl_param_emissive_idx_padding > 0) {
        SPOCK_PARANOID(mtl_param_emissive_idx_padding < k_mtl_param_alignment, "mtl_param_emissive_idx_padding cannot be larger than k_mtl_param_alignment");
        auto padding_begin = mtl_param_base_ptr + mtl_param_emissive_idx_offsets_[MTL_TYPE_COUNT] - mtl_param_emissive_idx_padding;
        std::fill(padding_begin, padding_begin + mtl_param_emissive_idx_padding, 0);
    }
    if (mtl_param_alpha_cutoff_padding > 0) {
        SPOCK_PARANOID(mtl_param_alpha_cutoff_padding < k_mtl_param_alignment, "mtl_param_alpha_cutoff_padding cannot be larger than k_mtl_param_alignment");
        auto padding_begin = mtl_param_base_ptr + mtl_param_alpha_cutoff_offsets_[MTL_TYPE_COUNT] - mtl_param_alpha_cutoff_padding;
        std::fill(padding_begin, padding_begin + mtl_param_alpha_cutoff_padding, 0);
    }

    // mesh buffers 
    std::exclusive_scan(a_vertex_counts, a_vertex_counts + MTL_TYPE_COUNT, vertex_offsets_.begin(), 0u);
    vertex_offsets_[MTL_TYPE_COUNT] = vertex_offsets_[MTL_TYPE_COUNT - 1] + a_vertex_counts[MTL_TYPE_COUNT - 1];
    std::exclusive_scan(a_index_counts, a_index_counts + MTL_TYPE_COUNT, index_offsets_.begin(), 0u);
    index_offsets_[MTL_TYPE_COUNT] = index_offsets_[MTL_TYPE_COUNT - 1] + a_index_counts[MTL_TYPE_COUNT - 1];

    SPOCK_PARANOID(vertex_offsets_[MTL_TYPE_COUNT] > 0, "the number of vertices cannot be 0");
    SPOCK_PARANOID(index_offsets_[MTL_TYPE_COUNT] > 0, "the number of indices cannot be 0");

    std::unique_ptr<glm::vec3> unique_position_buffer{ new glm::vec3[vertex_offsets_[MTL_TYPE_COUNT]] };
    std::unique_ptr<glm::vec3> unique_normal_buffer{ new glm::vec3[vertex_offsets_[MTL_TYPE_COUNT]] };
    std::unique_ptr<glm::vec2> unique_uv_buffer{ new glm::vec2[vertex_offsets_[MTL_TYPE_COUNT]] };
    std::unique_ptr<uint32_t> unique_index_buffer{ new uint32_t[index_offsets_[MTL_TYPE_COUNT]] };

    const uint32_t num_triangles = index_offsets_[MTL_TYPE_COUNT] / 3;
    std::unique_ptr<triangle_mtl_idx_type> unique_triangle_mtl_idx_buffer{ new triangle_mtl_idx_type[num_triangles] };

    uint32_t curr_vertex_offsets[MTL_TYPE_COUNT];
    uint32_t curr_index_offsets[MTL_TYPE_COUNT];
    std::copy(vertex_offsets_.begin(), vertex_offsets_.begin() + MTL_TYPE_COUNT, curr_vertex_offsets);
    std::copy(index_offsets_.begin(), index_offsets_.begin() + MTL_TYPE_COUNT, curr_index_offsets);
    for (const auto& primitive_info : a_primitive_infos) {
        const auto vertex_offset = curr_vertex_offsets[primitive_info.mtl_type];
        const auto index_offset = curr_index_offsets[primitive_info.mtl_type];

        constexpr size_t dst_position_byte_stride = sizeof(glm::vec3);
        constexpr size_t dst_normal_byte_stride = sizeof(glm::vec3);
        constexpr size_t dst_uv_byte_stride = sizeof(glm::vec2);
        constexpr size_t dst_index_byte_stride = sizeof(uint32_t);

        glm::vec3* dst_position_buffer = unique_position_buffer.get() + vertex_offset;
        glm::vec3* dst_normal_buffer = unique_normal_buffer.get() + vertex_offset;
        glm::vec2* dst_uv_buffer = unique_uv_buffer.get() + vertex_offset;
        uint32_t* dst_index_buffer = unique_index_buffer.get() + index_offset;

        copy_strided(
            dst_position_buffer, dst_position_byte_stride,
            primitive_info.position_buffer_ptr, primitive_info.position_byte_stride,
            primitive_info.position_byte_stride * primitive_info.num_vertices
        );
        ensure(primitive_info.normal_buffer_ptr, primitive_info.name + ": does not have a normal buffer");
        copy_strided(
            dst_normal_buffer, dst_normal_byte_stride,
            primitive_info.normal_buffer_ptr, primitive_info.normal_byte_stride,
            primitive_info.normal_byte_stride * primitive_info.num_vertices
        );
        
        if (primitive_info.uv_buffer_ptr) {
            const float* src_uv_buffer = primitive_info.uv_buffer_ptr;
            const int32_t src_uv_byte_stride = primitive_info.uv_byte_stride;
            copy_strided(
                dst_uv_buffer, dst_uv_byte_stride,
                src_uv_buffer, src_uv_byte_stride,
                src_uv_byte_stride * primitive_info.num_vertices
            );
        }
        else {
            info("generating uv buffer for " + primitive_info.name);
            std::fill(std::execution::par_unseq,
                dst_uv_buffer, dst_uv_buffer + primitive_info.num_vertices, glm::vec2{ 0.f, 0.f }
            );
        }
        if (primitive_info.transform_idx != -1) {
            const auto& model_mat = a_mesh_transforms[primitive_info.transform_idx];
            std::transform(std::execution::par_unseq,
                dst_position_buffer, dst_position_buffer + primitive_info.num_vertices, dst_position_buffer,
                [model_mat](const glm::vec3& p) {return glm::vec3(model_mat * glm::vec4(p, 1.f)); }
            );
            const auto normal_mat = glm::transpose(glm::inverse(model_mat));
            std::transform(std::execution::par_unseq,
                dst_normal_buffer, dst_normal_buffer + primitive_info.num_vertices, dst_normal_buffer,
                [normal_mat](const glm::vec3& n) {return glm::vec3(normal_mat * glm::vec4(n, 0.f)); }
            );
        }
        if (primitive_info.index_buffer_ptr) {
            copy_indices(
                dst_index_buffer,
                primitive_info.index_buffer_ptr, primitive_info.index_byte_stride, primitive_info.num_indices,
                vertex_offset, primitive_info.double_sided
            ); // mtl_cfg_t::double_sided is set to false in create_cache(...), effectively disable index duplication of indices for all primitives
        }
        else {
            iota_indices(dst_index_buffer, primitive_info.num_indices, vertex_offset, primitive_info.double_sided);
        }

        const auto triangle_offset = index_offset / 3;
        const auto curr_index_count = primitive_info.num_indices * (1u + primitive_info.double_sided);
        const auto curr_triangle_count = curr_index_count / 3;
        triangle_mtl_idx_type* triangle_mtl_idx_begin = unique_triangle_mtl_idx_buffer.get() + triangle_offset;
        const auto mtl_idx = calc_triangle_mtl_idx(primitive_info.mtl_type, primitive_info.mtl_instance_idx);
        std::fill(std::execution::par_unseq, triangle_mtl_idx_begin, triangle_mtl_idx_begin + curr_triangle_count, mtl_idx);

        curr_vertex_offsets[primitive_info.mtl_type] += primitive_info.num_vertices;
        curr_index_offsets[primitive_info.mtl_type] += curr_index_count;
    }

    aabb_lower_ = std::reduce(std::execution::par_unseq,
        unique_position_buffer.get(), unique_position_buffer.get() + vertex_offsets_[MTL_TYPE_COUNT], 
        glm::vec3(std::numeric_limits<float>::infinity()),
        [](const glm::vec3& p0, const glm::vec3& p1) { return glm::min(p0, p1); }
    );
    aabb_upper_ = std::reduce(std::execution::par_unseq,
        unique_position_buffer.get(), unique_position_buffer.get() + vertex_offsets_[MTL_TYPE_COUNT],
        glm::vec3(-std::numeric_limits<float>::infinity()),
        [](const glm::vec3& p0, const glm::vec3& p1) { return glm::max(p0, p1); }
    );
    std::for_each(std::execution::par_unseq, 
        unique_normal_buffer.get(), unique_normal_buffer.get() + vertex_offsets_[MTL_TYPE_COUNT], 
        [](glm::vec3& n) { n = glm::normalize(n); }
    );

    const size_t punctual_light_buffer_bytes = get_punctual_light_buffer_bytes();
    const auto offsets_of_mtl_types = assemble_offsets_of_mtl_types();
    const size_t position_buffer_bytes = vertex_offsets_[MTL_TYPE_COUNT] * sizeof(glm::vec3);
    const size_t normal_buffer_bytes = position_buffer_bytes;
    const size_t uv_buffer_bytes = vertex_offsets_[MTL_TYPE_COUNT] * sizeof(glm::vec2);
    const size_t index_buffer_bytes = index_offsets_[MTL_TYPE_COUNT] * sizeof(uint32_t);
    const size_t triangle_mtl_idx_buffer_bytes = num_triangles * sizeof(triangle_mtl_idx_type);

    VmaAllocationCreateInfo mem_allocation_create_info{};
    mem_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    auto buffer_create_info = make_VkBufferCreateInfo();
    buffer_create_info.size = punctual_light_buffer_bytes;
    buffer_create_info.usage = punctual_light_buffer_usage_;
    punctual_light_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = get_offsets_of_mtl_type_buffer_bytes();
    buffer_create_info.usage = offsets_of_mtl_type_buffer_usage_;
    offsets_of_mtl_type_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = position_buffer_bytes;
    buffer_create_info.usage = position_buffer_usage_;
    position_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);
    buffer_create_info.size = normal_buffer_bytes;
    buffer_create_info.usage = normal_buffer_usage_;
    normal_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);
    buffer_create_info.size = uv_buffer_bytes;
    buffer_create_info.usage = uv_buffer_usage_;
    uv_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = index_buffer_bytes;
    buffer_create_info.usage = index_buffer_usage_;
    index_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = triangle_mtl_idx_buffer_bytes;
    buffer_create_info.usage = triangle_mtl_idx_buffer_usage_;
    triangle_mtl_idx_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    buffer_create_info.size = mtl_param_buffer_bytes;
    buffer_create_info.usage = mtl_param_buffer_usage_;
    mtl_param_buffer_ = buffer_t(*context_, buffer_create_info, mem_allocation_create_info);

    // upload buffers
    std::thread upload_worker{ [&]() {
        command_buffer_t cmd_buf{ a_command_pool };
        auto vk_command_buffer_begin_info = spock::make_VkCommandBufferBeginInfo();
        vk_command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        SPOCK_VK_CALL(vkBeginCommandBuffer(cmd_buf, &vk_command_buffer_begin_info));

        auto punctual_light_staging_buffer = punctual_light_buffer_.cmd_upload(cmd_buf, unique_punctual_light_buffer_.get());

        auto offsets_of_mtl_tpye_staging_buffer = offsets_of_mtl_type_buffer_.cmd_upload(cmd_buf, offsets_of_mtl_types.data());

        auto position_staging_buffer = position_buffer_.cmd_upload(cmd_buf, unique_position_buffer.get());
        auto normal_staging_buffer = normal_buffer_.cmd_upload(cmd_buf, unique_normal_buffer.get());
        buffer_t uv_staging_buffer = uv_buffer_.cmd_upload(cmd_buf, unique_uv_buffer.get());
        auto index_staging_buffer = index_buffer_.cmd_upload(cmd_buf, unique_index_buffer.get());

        auto triangle_mtl_idx_staging_buffer = triangle_mtl_idx_buffer_.cmd_upload(cmd_buf, unique_triangle_mtl_idx_buffer.get());
        auto mtl_param_staging_buffer = mtl_param_buffer_.cmd_upload(cmd_buf, unique_mtl_param_buffer.get());

        SPOCK_VK_CALL(vkEndCommandBuffer(cmd_buf));
        const auto submit_info = build_VkSubmitInfo(1, &cmd_buf());
        SPOCK_VK_CALL(vkQueueSubmit(a_queue, 1, &submit_info, VK_NULL_HANDLE));
        SPOCK_VK_CALL(vkQueueWaitIdle(a_queue));
    } };

    a_ofs.write(reinterpret_cast<const char*>(&aabb_lower_[0]), sizeof(glm::vec3));
    a_ofs.write(reinterpret_cast<const char*>(&aabb_upper_[0]), sizeof(glm::vec3));
    a_ofs.write(reinterpret_cast<const char*>(vertex_offsets_.data()), sizeof(vertex_offsets_));
    a_ofs.write(reinterpret_cast<const char*>(index_offsets_.data()), sizeof(index_offsets_));
    a_ofs.write(reinterpret_cast<const char*>(mtl_param_emissive_idx_offsets_.data()), sizeof(mtl_param_emissive_idx_offsets_));
    a_ofs.write(reinterpret_cast<const char*>(mtl_param_alpha_cutoff_offsets_.data()), sizeof(mtl_param_alpha_cutoff_offsets_));

    a_ofs.write(reinterpret_cast<const char*>(&num_emissive_textures_), sizeof(uint32_t)); // emissive texture count
    a_ofs.write(reinterpret_cast<const char*>(texture_strides_.data()), sizeof(texture_strides_));
    a_ofs.write(reinterpret_cast<const char*>(texture_offsets_.data()), sizeof(texture_offsets_));

    a_ofs.write(reinterpret_cast<const char*>(unique_position_buffer.get()), position_buffer_bytes);
    a_ofs.write(reinterpret_cast<const char*>(unique_normal_buffer.get()), normal_buffer_bytes);
    a_ofs.write(reinterpret_cast<const char*>(unique_uv_buffer.get()), uv_buffer_bytes);
    a_ofs.write(reinterpret_cast<const char*>(unique_index_buffer.get()), index_buffer_bytes);

    a_ofs.write(reinterpret_cast<const char*>(unique_triangle_mtl_idx_buffer.get()), triangle_mtl_idx_buffer_bytes);
    a_ofs.write(reinterpret_cast<const char*>(unique_mtl_param_buffer.get()), mtl_param_buffer_bytes);

    upload_worker.join();
}


void spock::scene_t::
create_cache(
    const command_pool_t& a_command_pool, const queue_t& a_queue, 
    const std::vector<std::string>& a_gltf_paths, const std::string_view& a_cache_path,
    const directional_light_t& a_fallback_directional_light,
    bool* a_cache_created
)
{
    host_timer_t timer;
    const auto gltf_models = parse_gltf_models(a_gltf_paths);
    info("parsing gltf files took " + std::to_string(timer.elapsed_ms()) + " ms");
    const int32_t num_gltf_models = static_cast<int32_t>(gltf_models.size());

    // lights
    unique_punctual_light_buffer_.reset();
    punctual_light_counts_.fill(0);
    punctual_light_offsets_.fill(0);

    std::vector<spotlight_t> spotlights;
    std::vector<point_light_t> point_lights;
    std::vector<directional_light_t> directional_lights;

    std::unordered_map<int32_t, std::pair<punctual_light_type_e, size_t>> light_idx_map;
    std::vector<int32_t> light_offsets;
    int32_t num_handled_lights = 0;
    for (int32_t i = 0; i < num_gltf_models; ++i) {
        light_offsets.push_back(num_handled_lights);
        for (const auto& gltf_light : gltf_models[i].lights) {
            if (gltf_light.type == "spot") {
                light_idx_map[num_handled_lights] = std::make_pair(PUNCTUAL_LIGHT_TYPE_SPOT, spotlights.size());
                spotlights.push_back(spotlight_from_gltf(gltf_light));
            }
            else if (gltf_light.type == "point") {
                light_idx_map[num_handled_lights] = std::make_pair(PUNCTUAL_LIGHT_TYPE_POINT, point_lights.size());
                point_lights.push_back(point_light_from_gltf(gltf_light));
            }
            else if (gltf_light.type == "directional") {
                light_idx_map[num_handled_lights] = std::make_pair(PUNCTUAL_LIGHT_TYPE_DIRECTIONAL, directional_lights.size());
                directional_lights.push_back(directional_light_from_gltf(gltf_light));
            }
            else {
                warn("ignore " + a_gltf_paths[i] + ": " + gltf_light.name + ": unknown light type " + gltf_light.type);
            }
            ++num_handled_lights;
        }
    }

    // samplers
    std::vector<sampler_2d_info_t> sampler_2d_infos;
    sampler_2d_infos.push_back(sampler_2d_info_t{}); // first one created with default parameters

    std::vector<int32_t> sampler_offsets;
    sampler_offsets.reserve(num_gltf_models);
    int32_t num_handled_gltf_samplers = 1;

    for (int32_t i = 0; i < num_gltf_models; ++i) {
        sampler_offsets.push_back(num_handled_gltf_samplers);
        const auto& gltf_samplers = gltf_models[i].samplers;
        const auto num_samplers = static_cast<int32_t>(gltf_samplers.size());
        for (int32_t j = 0; j < num_samplers; ++j) {
            auto sampler_2d_info = sampler_2d_info_from_gltf_sampler(gltf_samplers[j]);
            sampler_2d_infos.push_back(sampler_2d_info);
        }
        num_handled_gltf_samplers += num_samplers;
    }
    sampler_array_ = sampler_array_t(*context_, sampler_2d_infos);

    // mtl
    struct emissive_info_t {
        const tinygltf::Image* image_ptr;
        uint16_t sampler_idx;
        glm::vec3 factor;
        emissive_info_t() : image_ptr{ nullptr }, sampler_idx{ 0 }, factor{ glm::vec3(0.f) } {}
    };
    std::vector<emissive_info_t> emissive_infos = { emissive_info_t{} };
    std::vector<int32_t> mtl_offsets;
    mtl_offsets.reserve(num_gltf_models);
    std::vector<mtl_cfg_t> mtl_cfgs[MTL_TYPE_COUNT];
    std::vector<std::pair<mtl_type_e, int32_t>> gltf_model_default_mtl_type_instance_idx_pairs;
    int32_t num_handled_mtls = 0;
    std::vector<std::pair<mtl_type_e, int32_t>> mtl_type_instance_idx_pairs; // for each global index
    for (int32_t i = 0; i < num_gltf_models; ++i) {
        mtl_offsets.push_back(num_handled_mtls);
        const std::string& gltf_model_path = a_gltf_paths[i];
        for (size_t j = 0; j < gltf_models[i].materials.size(); ++j) {
            const auto& gltf_mtl = gltf_models[i].materials[j];

            auto get_gltf_image_ptr_and_sampler_idx = [&gltf_model = std::as_const(gltf_models[i]), sampler_offset = sampler_offsets[i]] 
                (int32_t a_texture_idx) -> std::pair<const tinygltf::Image*, uint16_t> {
                    if (a_texture_idx < 0) return std::pair<const tinygltf::Image*, uint16_t>(nullptr, 0);
                    int32_t ii = gltf_model.textures[a_texture_idx].source;
                    int32_t is = 0;
                    if (gltf_model.textures[a_texture_idx].sampler > -1) {
                        is = sampler_offset + gltf_model.textures[a_texture_idx].sampler;
                    }
                    SPOCK_PARANOID(ii >= 0, "image idx of a texture cannot be negative");
                    return std::make_pair(&gltf_model.images[ii], static_cast<uint16_t>(is));
                };

            mtl_cfg_t mtl_cfg;
            mtl_cfg.name = gltf_model_path + ": " + gltf_mtl.name;
            mtl_cfg.gltf_model_idx = i;
            const auto& gltf_pbr = gltf_mtl.pbrMetallicRoughness;

            mtl_cfg.base_color_factor = glm::vec4(glm::make_vec4(gltf_pbr.baseColorFactor.data()));
            std::tie(mtl_cfg.gltf_base_color_image_ptr, mtl_cfg.base_color_sampler_idx) = 
                get_gltf_image_ptr_and_sampler_idx(gltf_pbr.baseColorTexture.index);

            mtl_cfg.occlusion_strength = gltf_mtl.occlusionTexture.strength;
            std::tie(mtl_cfg.gltf_occlusion_image_ptr, mtl_cfg.occlusion_sampler_idx) = 
                get_gltf_image_ptr_and_sampler_idx(gltf_mtl.occlusionTexture.index);

            mtl_cfg.roughness_factor = gltf_pbr.roughnessFactor;
            mtl_cfg.metallic_factor = gltf_pbr.metallicFactor;
            std::tie(mtl_cfg.gltf_metallic_roughness_image_ptr, mtl_cfg.metallic_roughness_sampler_idx) = 
                get_gltf_image_ptr_and_sampler_idx(gltf_mtl.pbrMetallicRoughness.metallicRoughnessTexture.index);

            std::tie(mtl_cfg.gltf_normal_image_ptr, mtl_cfg.normal_sampler_idx) = 
                get_gltf_image_ptr_and_sampler_idx(gltf_mtl.normalTexture.index);
 
            emissive_info_t emissive_info;
            std::tie(emissive_info.image_ptr, emissive_info.sampler_idx) = get_gltf_image_ptr_and_sampler_idx(gltf_mtl.emissiveTexture.index);
            if (gltf_mtl.emissiveFactor.size() == 3) {
                emissive_info.factor = glm::vec3(glm::make_vec3(gltf_mtl.emissiveFactor.data()));
            }
            if (equals_zero_vec(emissive_info.factor)) {
                mtl_cfg.emissive_texture_idx = 0;
                if (emissive_info.image_ptr != nullptr) {
                    info("ignore emissive texture in " + mtl_cfg.name);
                }
            }
            else {
                // generate constant emissive texture if emissive_info.image_ptr == nullptr
                mtl_cfg.emissive_texture_idx = static_cast<uint32_t>(emissive_infos.size());
                emissive_infos.push_back(emissive_info);
            }
            //mtl_cfg.double_sided = gltf_mtl.doubleSided;
            // disable index duplication as it slows down visibility pass, and backface culling should be (most of the time) disabled in ray tracing anyway; 
            // TODO:think more about this 
            mtl_cfg.double_sided = false; 
            if (gltf_mtl.alphaMode == "BLEND") {
                mtl_cfg.alpha_blend_enabled = true;
            }
            else if (gltf_mtl.alphaMode == "MASK") {
                mtl_cfg.alpha_mask_enabled = true;
                mtl_cfg.alpha_mask_cutoff = gltf_mtl.alphaCutoff;
            }
            auto mtl_type = mtl_cfg.get_mtl_type();
            const int32_t mtl_instance_idx = static_cast<int32_t>(mtl_cfgs[mtl_type].size());
            mtl_cfgs[mtl_type].push_back(mtl_cfg);
            mtl_type_instance_idx_pairs.emplace_back(mtl_type, mtl_instance_idx);
            if (j == 0) {
                gltf_model_default_mtl_type_instance_idx_pairs.emplace_back(mtl_type, mtl_instance_idx);
            }
        }
        num_handled_mtls += static_cast<int32_t>(gltf_models[i].materials.size());
    }
    static_assert(static_cast<size_t>(MTL_TYPE_COUNT) <= k_mtl_type_idx_max, "there cannot be more than scene_t::k_mtl_type_idx_max material types");
    ensure(
        std::any_of(mtl_cfgs, mtl_cfgs + MTL_TYPE_COUNT, [](const std::vector<mtl_cfg_t>& v) { return v.size() > 0; }), 
        "there must be at least one material type"
    );
    ensure(
        std::all_of(mtl_cfgs, mtl_cfgs + MTL_TYPE_COUNT, [](const std::vector<mtl_cfg_t>& v) {return v.size() <= k_mtl_instance_idx_max; }),
        "no material type can have more than scene_t::k_mtl_instance_idx_max materials"
    );
    num_emissive_textures_ = static_cast<uint32_t>(emissive_infos.size());

    // node 
    std::vector<glm::mat4> node_transforms;
    uint32_t vertex_counts[MTL_TYPE_COUNT] = {}; // counter for each type; zero initialized
    uint32_t index_counts[MTL_TYPE_COUNT] = {}; // counter for each type; zero initialized
    std::vector<primitive_info_t> primitive_infos;

    struct node_t {
        const tinygltf::Node* gltf_node_ptr;
        int32_t parent_transform_idx;
        node_t() 
            : gltf_node_ptr{ nullptr }, parent_transform_idx { -1 } {}
        node_t(const tinygltf::Node* a_gltf_node_ptr, int32_t a_parent_transform_idx) 
            : gltf_node_ptr{ a_gltf_node_ptr }, parent_transform_idx { a_parent_transform_idx } {}
    };

    for (int im = 0; im < num_gltf_models; ++im) {
        const auto& gltf_model = gltf_models[im];
        if (gltf_model.scenes.empty()) {
            except(a_gltf_paths[im] + " does not contain any scene");
        }
        const auto default_gltf_scene_idx = gltf_model.defaultScene;
        const auto& gltf_scene = default_gltf_scene_idx == -1 ? gltf_model.scenes[0] : gltf_model.scenes[default_gltf_scene_idx];
        const auto& gltf_nodes = gltf_scene.nodes;
        std::vector<node_t> node_stack; // some nodes may only have transform but no mesh data
        for (auto i_node = gltf_nodes.crbegin(); i_node != gltf_nodes.crend(); ++i_node) {
            const auto& gltf_node = gltf_model.nodes[*i_node];
            node_stack.emplace_back(&gltf_node, -1);
        }
        while (!node_stack.empty()) {
            const auto& gltf_node = *(node_stack.back().gltf_node_ptr);
            const auto parent_transform_idx = node_stack.back().parent_transform_idx;
            int32_t curr_transform_idx = -1;
            node_stack.pop_back();

            glm::mat4 curr_transform = glm::identity<glm::mat4>();
            if (gltf_node.translation.empty() && gltf_node.rotation.empty() && gltf_node.scale.empty() && gltf_node.matrix.empty()) {
                curr_transform_idx = parent_transform_idx;
            }
            else {
                if (gltf_node.matrix.size() == 16) {
                    curr_transform = glm::make_mat4(gltf_node.matrix.data());
                }
                else {
                    if (gltf_node.scale.size() == 3) {
                        const glm::vec3 scale_v = glm::make_vec3(gltf_node.scale.data());
                        curr_transform = glm::scale(glm::identity<glm::mat4>(), scale_v) * curr_transform;
                    }
                    if (gltf_node.rotation.size() == 4) {
                        const glm::quat rotation = glm::make_quat(gltf_node.rotation.data());
                        curr_transform = glm::mat4_cast(rotation) * curr_transform;
                    }
                    if (gltf_node.translation.size() == 3) {
                        const glm::vec3 translation = glm::make_vec3(gltf_node.translation.data());
                        curr_transform = glm::translate(glm::identity<glm::mat4>(), translation) * curr_transform;
                    }
                }
                if (parent_transform_idx > -1) {
                    curr_transform = node_transforms[parent_transform_idx] * curr_transform;
                }
                curr_transform_idx = static_cast<int32_t>(node_transforms.size());
                node_transforms.push_back(curr_transform);
            }
            if (!gltf_node.children.empty()) {
                for (auto i_child_node = gltf_node.children.crbegin(); i_child_node != gltf_node.children.crend(); ++i_child_node) {
                    const auto& gltf_child_node = gltf_model.nodes[*i_child_node];
                    node_stack.emplace_back(&gltf_child_node, curr_transform_idx);
                }
            }
            const std::string gltf_node_msg_head = a_gltf_paths[im] + ": node: " + gltf_node.name;
            if (gltf_node.mesh > -1) {
                const int32_t winding = static_cast<int32_t>(std::copysign(1.f, glm::determinant(curr_transform)));
                const auto& default_mtl_type_instance_idx = gltf_model_default_mtl_type_instance_idx_pairs[im];
                const auto& gltf_mesh = gltf_model.meshes[gltf_node.mesh];
                const std::string gltf_mesh_msg_head =  gltf_node_msg_head + ": mesh " + gltf_mesh.name;

                handle_gltf_mesh(
                    gltf_model, gltf_mesh, gltf_mesh_msg_head, 
                    winding, curr_transform_idx, mtl_cfgs,
                    mtl_offsets[im], default_mtl_type_instance_idx, mtl_type_instance_idx_pairs, 
                    primitive_infos, vertex_counts, index_counts
                );
            } // handle node mesh

            auto gltf_khr_lights_punctual_extension = gltf_node.extensions.find("KHR_lights_punctual");
            if (gltf_khr_lights_punctual_extension != gltf_node.extensions.end()) {
                const std::string gltf_light_msg_head = gltf_node_msg_head + "KHR_lights_punctual";
                const int32_t local_gltf_light_idx = gltf_khr_lights_punctual_extension->second.Get("light").Get<int32_t>();
                ensure(local_gltf_light_idx >= 0, gltf_light_msg_head + ": light index cannot be negative");
                const int32_t global_gltf_light_idx = local_gltf_light_idx + light_offsets[im];
                SPOCK_PARANOID(
                    light_idx_map.find(global_gltf_light_idx) != light_idx_map.end(), 
                    gltf_light_msg_head + ": invalid index or bug"
                );
                const auto [light_type, light_instance_idx] = light_idx_map.at(global_gltf_light_idx);
                if (light_type == PUNCTUAL_LIGHT_TYPE_SPOT) {
                    auto& spotlight = spotlights[light_instance_idx];
                    spotlight.position = glm::vec3(curr_transform * glm::vec4(spotlight.position, 1.f));
                    spotlight.direction = glm::normalize(glm::vec3(curr_transform * glm::vec4(spotlight.direction, 0.f)));
                }
                else if (light_type == PUNCTUAL_LIGHT_TYPE_POINT) {
                    auto& point_light = point_lights[light_instance_idx];
                    point_light.position = glm::vec3(curr_transform * glm::vec4(point_light.position, 1.f));
                }
                else if (light_type == PUNCTUAL_LIGHT_TYPE_DIRECTIONAL) {
                    auto& directional_light = directional_lights[light_instance_idx];
                    directional_light.direction = glm::normalize(glm::vec3(curr_transform * glm::vec4(directional_light.direction, 0.f)));
                }
                else {
                    warn(gltf_light_msg_head + ": unknown light");
                }
            }
        } // loop nodes in node stack
    } // loop gltf_models

    int64_t num_total_lights = static_cast<int64_t>(spotlights.size() + point_lights.size() + directional_lights.size());
    ensure(num_total_lights <= 0x7FFF, "the number of spotlights cannot be greater than " + std::to_string(0x7FFF));
    if (num_total_lights == 0) {
        const auto direction = glm::normalize(a_fallback_directional_light.direction);
        directional_lights.push_back(a_fallback_directional_light);
        directional_lights.back().direction = direction;
        num_total_lights = -1;
    }
    punctual_light_counts_ = {
        static_cast<int16_t>(spotlights.size()),
        static_cast<int16_t>(point_lights.size()),
        static_cast<int16_t>(directional_lights.size()),
        static_cast<int16_t>(num_total_lights)
    };
    constexpr uint32_t punctual_light_size_in_vec4[] = { 
        spotlight_t::SPOTLIGHT_SIZE_IN_VEC4, 
        point_light_t::POINT_LIGHT_SIZE_IN_VEC4, 
        directional_light_t::DIRECTIONAL_LIGHT_SIZE_IN_VEC4 
    };
    const glm::vec4* punctual_light_ptrs[] = {
        reinterpret_cast<const glm::vec4*>(spotlights.data()),
        reinterpret_cast<const glm::vec4*>(point_lights.data()),
        reinterpret_cast<const glm::vec4*>(directional_lights.data())
    };
    for (size_t i = 1; i < 1 + PUNCTUAL_LIGHT_TYPE_COUNT; ++i) {
        punctual_light_offsets_[i] = punctual_light_offsets_[i - 1] + punctual_light_counts_[i - 1] * punctual_light_size_in_vec4[i - 1];
    }

    const auto punctual_light_buffer_bytes = get_punctual_light_buffer_bytes();
    if (punctual_light_buffer_bytes > k_maxUniformBufferRange_min) {
        warn("punctual_light_buffer_bytes is larger than lower bound of VkPhysicalDeviceLimits::maxUniformBufferRange");
    }
    if (punctual_light_buffer_bytes > context_->max_uniform_buffer_range()) {
        warn("punctual_light_buffer_bytes is greater than VkPhysicalDeviceLimits::maxUniformBufferRange");
    }

    unique_punctual_light_buffer_.reset(new glm::vec4[punctual_light_offsets_.back()]);
    for (int i = 0; i < +PUNCTUAL_LIGHT_TYPE_COUNT; ++i) {
        if (punctual_light_counts_[i] > 0) {
            std::copy(
                punctual_light_ptrs[i],
                punctual_light_ptrs[i] + punctual_light_size_in_vec4[i] * punctual_light_counts_[i],
                unique_punctual_light_buffer_.get() + punctual_light_offsets_[i]
            );
        }
    }

    uint32_t curr_texture_offset = num_emissive_textures_; // emissive textures come first in array of textures
    for (size_t i = 0; i < static_cast<size_t>(MTL_TYPE_COUNT); ++i) {
        const auto texture_stride = get_mtl_texture_stride(static_cast<mtl_type_e>(i));
        texture_strides_[i] = texture_stride;
        const auto curr_texture_count = static_cast<uint32_t>(mtl_cfgs[i].size()) * texture_stride;
        texture_offsets_[i] = curr_texture_offset;
        curr_texture_offset += curr_texture_count;
    }
    texture_offsets_[MTL_TYPE_COUNT] = curr_texture_offset; // total texture count

    std::ofstream ofs(a_cache_path, std::ios::out | std::ios::binary);
    const uint32_t curr_cache_version = get_curr_cache_version();
    ofs.write(reinterpret_cast<const char*>(&curr_cache_version), sizeof(uint32_t));
    const uint32_t num_mtl_types = MTL_TYPE_COUNT;
    ofs.write(reinterpret_cast<const char*>(&num_mtl_types), sizeof(uint32_t));
    serialize_gltf_paths(a_gltf_paths, ofs);

    ofs.write(reinterpret_cast<const char*>(punctual_light_counts_.data()), sizeof(punctual_light_counts_));
    ofs.write(reinterpret_cast<const char*>(punctual_light_offsets_.data()), sizeof(punctual_light_offsets_));
    ofs.write(reinterpret_cast<const char*>(unique_punctual_light_buffer_.get()), punctual_light_buffer_bytes);

    serialize_buffers(
        a_command_pool, a_queue, mtl_cfgs, 
        vertex_counts, index_counts, primitive_infos, node_transforms, ofs
    ); 

    uint16_t num_sampler_2d_info = static_cast<uint16_t>(sampler_2d_infos.size());
    ofs.write(reinterpret_cast<const char*>(&num_sampler_2d_info), sizeof(uint16_t));
    for (const auto& sampler_2d_info : sampler_2d_infos) {
        const auto bin_stream = sampler_2d_info.serialize();
        ofs.write(reinterpret_cast<const char*>(bin_stream.data()), bin_stream.size());
    }


    const auto preset_image_extent = VkExtent2D{ 1, 1 };

    std::vector<mip_2d_info_t> dst_mip_2d_infos; // write to binary cache file
    dst_mip_2d_infos.reserve(curr_texture_offset);
    texture_descriptor_array_.reserve(curr_texture_offset);

    // emissive textures
    for (uint32_t i = 0; i < num_emissive_textures_; ++i) {
        const auto& emissive_info = emissive_infos[i];
        auto image_extent = preset_image_extent;
        const VkFormat vk_format = VK_FORMAT_BC1_RGB_UNORM_BLOCK;
        if (emissive_info.image_ptr) {
            const auto gltf_image = reinterpret_cast<const tinygltf::Image*>(emissive_info.image_ptr);
            image_extent = VkExtent2D{ (uint32_t)gltf_image->width, (uint32_t)gltf_image->height };
        }
        dst_mip_2d_infos.emplace_back(vk_format, image_extent, emissive_info.sampler_idx);
    }
    // MTL_TYPE_PRIMARY (0), MTL_TYPE_GRAY_DOT (1), MTL_TYPE_ALPHA_CUTOFF(2), MTL_TYPE_BLEND (3)
    for (size_t i = 0; i <= static_cast<size_t>(MTL_TYPE_BLEND); ++i) {
        for (const auto& mtl_cfg : mtl_cfgs[i]) {
            const auto im = mtl_cfg.gltf_model_idx;
            ensure(im >= 0, "gltf_model_idx cannot be invalid");
            const auto& gltf_model = gltf_models[im];
            const auto& gltf_images = gltf_model.images;
            const bool is_non_opaque = mtl_cfg.alpha_mask_enabled || mtl_cfg.alpha_blend_enabled;
            const auto dst_base_color_format = is_non_opaque ? VK_FORMAT_BC3_UNORM_BLOCK : VK_FORMAT_BC1_RGB_UNORM_BLOCK;
            std::array<VkFormat, 3> dst_vk_formats{
                dst_base_color_format,
                VK_FORMAT_BC1_RGB_UNORM_BLOCK,  // dst metallic roughness
                VK_FORMAT_BC5_UNORM_BLOCK // dst normal
            };
            const void* gltf_pbr_image_ptr = nullptr;
            uint16_t gltf_pbr_sampler_idx = 0;
            if (mtl_cfg.gltf_metallic_roughness_image_ptr) {
                gltf_pbr_image_ptr = mtl_cfg.gltf_metallic_roughness_image_ptr;
                gltf_pbr_sampler_idx = mtl_cfg.metallic_roughness_sampler_idx;
            }
            else if (mtl_cfg.gltf_occlusion_image_ptr) {
                gltf_pbr_image_ptr = mtl_cfg.gltf_occlusion_image_ptr;
                gltf_pbr_sampler_idx = mtl_cfg.occlusion_sampler_idx;
            }
            const std::array<const void*, 3> gltf_image_ptrs{
                mtl_cfg.gltf_base_color_image_ptr,
                gltf_pbr_image_ptr,
                mtl_cfg.gltf_normal_image_ptr
            };
            const std::array<uint16_t, 3> sampler_indices{
                mtl_cfg.base_color_sampler_idx, 
                gltf_pbr_sampler_idx,
                mtl_cfg.normal_sampler_idx
            };
            for (uint8_t j = 0; j < 3; ++j) {
                auto image_extent = preset_image_extent;
                if (gltf_image_ptrs[j]) {
                    const auto& gltf_image = *(reinterpret_cast<const tinygltf::Image*>(gltf_image_ptrs[j]));
                    image_extent = VkExtent2D{ (uint32_t)gltf_image.width, (uint32_t)gltf_image.height };
                    ensure(gltf_image.bits == 8, "cannot handle " + std::to_string(gltf_image.bits) + " -bit image");
                    ensure(gltf_image.pixel_type == TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE, "can only handle TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE");
                }
                dst_mip_2d_infos.emplace_back(dst_vk_formats[j], image_extent, sampler_indices[j]);
            }
        }
    }
    // dst_mip_2d_infos should be complete at this point

    SPOCK_PARANOID(dst_mip_2d_infos.size() <= 0xFFFF, "texture count cannot be greater than 0xFFFF");
    const uint16_t num_textures = static_cast<uint16_t>(dst_mip_2d_infos.size());
    SPOCK_PARANOID(texture_offsets_[MTL_TYPE_COUNT] == num_textures, "texture_offsets_ is not consistent with num_textures");
    ofs.write(reinterpret_cast<const char*>(&num_textures), sizeof(uint16_t));
    for (const auto& dst_mip_2d_info : dst_mip_2d_infos) {
        const auto bin_stream = dst_mip_2d_info.serialize();
        ofs.write(reinterpret_cast<const char*>(bin_stream.data()), bin_stream.size());
    }

    size_t curr_mip_2d_info_idx = 0;

    for (uint32_t i = 0; i < num_emissive_textures_; ++i) {
        const auto& emissive_info = emissive_infos[i];
        const uint8_t* gltf_buffer = nullptr;
        if (emissive_info.image_ptr) {
            gltf_buffer = reinterpret_cast<const tinygltf::Image*>(emissive_info.image_ptr)->image.data();
        }
        serialize_emissive_image(a_command_pool, a_queue, emissive_info.factor, gltf_buffer, dst_mip_2d_infos[curr_mip_2d_info_idx++], ofs);
    }

    // MTL_TYPE_PRIMARY (0), MTL_TYPE_GRAY_DOT (1), MTL_TYPE_ALPHA_CUTOFF(1), MTL_TYPE_BLEND (2)
    for (size_t i = 0; i <= static_cast<size_t>(MTL_TYPE_BLEND); ++i) {
        for (const auto& mtl_cfg : mtl_cfgs[i]) {
            const auto im = mtl_cfg.gltf_model_idx;
            const auto& gltf_model = gltf_models[im];
            const auto& gltf_images = gltf_model.images;

            // base color
            const auto& dst_base_color_mip_2d_info = dst_mip_2d_infos[curr_mip_2d_info_idx++];
            const uint8_t* base_color_buffer = nullptr;
            if (mtl_cfg.gltf_base_color_image_ptr) {
                base_color_buffer = reinterpret_cast<const tinygltf::Image*>(mtl_cfg.gltf_base_color_image_ptr)->image.data();
            }
            serialize_base_color_image(a_command_pool, a_queue, mtl_cfg, base_color_buffer, dst_base_color_mip_2d_info, ofs);

            // metallic_roughness and occlusion
            const auto& dst_metallic_roughness_mip_2d_info = dst_mip_2d_infos[curr_mip_2d_info_idx++];
            const uint8_t* metallic_roughness_buffer = nullptr;
            const uint8_t* occlusion_buffer = nullptr;
            if (mtl_cfg.gltf_metallic_roughness_image_ptr) {
                metallic_roughness_buffer = reinterpret_cast<const tinygltf::Image*>(mtl_cfg.gltf_metallic_roughness_image_ptr)->image.data();
            }
            if (mtl_cfg.gltf_occlusion_image_ptr) {
                const auto& gltf_occlusion_image = *(reinterpret_cast<const tinygltf::Image*>(mtl_cfg.gltf_occlusion_image_ptr));
                const auto mip0_dim = dst_metallic_roughness_mip_2d_info.mip0_dim();
                if (gltf_occlusion_image.width == mip0_dim.width && gltf_occlusion_image.height == mip0_dim.height) {
                    occlusion_buffer = gltf_occlusion_image.image.data();
                }
                else {
                    if (mtl_cfg.gltf_metallic_roughness_image_ptr) {
                        const auto& mr_name = reinterpret_cast<const tinygltf::Image*>(mtl_cfg.gltf_metallic_roughness_image_ptr)->name;
                        warn(mtl_cfg.name + ": ignore occlusion image " + gltf_occlusion_image.name + ": not the same size as image " + mr_name);
                    }
                }
            }
            serialize_metallic_roughness_image(
                a_command_pool, a_queue,
                mtl_cfg, metallic_roughness_buffer, occlusion_buffer, dst_metallic_roughness_mip_2d_info, 
                ofs
            );

            const auto& dst_normal_mip_2d_info = dst_mip_2d_infos[curr_mip_2d_info_idx++];
            const uint8_t* normal_image_buffer = nullptr;
            if (mtl_cfg.gltf_normal_image_ptr) {
                normal_image_buffer = reinterpret_cast<const tinygltf::Image*>(mtl_cfg.gltf_normal_image_ptr)->image.data();
            }
            serialize_normal_image(a_command_pool, a_queue, 
                mtl_cfg, normal_image_buffer, dst_normal_mip_2d_info, ofs
            );
        }
    }
    ofs.write(reinterpret_cast<const char*>(&curr_cache_version), sizeof(uint32_t));
    *a_cache_created = true;
}


void spock::scene_t::
serialize_emissive_image(
    const command_pool_t & a_command_pool, const queue_t & a_queue, 
    const glm::vec3& a_factor, const uint8_t* a_gltf_buffer, const mip_2d_info_t& a_dst_mip_2d_info, 
    std::ofstream & a_ofs
)
{
    mip_2d_info_t src_mip_2d_info{ VK_FORMAT_R8G8B8A8_UNORM, a_dst_mip_2d_info };
    VkFormat src_mip0_buffer_format = VK_FORMAT_R8G8B8A8_SRGB;
    const auto dst_mip_2d_layout = a_dst_mip_2d_info.calc_mip_2d_layout();
    const auto dst_buffer_bytes = dst_mip_2d_layout.get_mip_buffer_bytes();
    std::vector<glm::u8vec4> preset_buffer; // when no base color image specified in gltf
    std::unique_ptr<glm::u8vec4> unique_buffer; // when base color image need to be multiplied by a factor
    const uint8_t* buffer = a_gltf_buffer;
    const uint32_t num_texels = calc_num_texels(src_mip_2d_info.mip0_dim());
    if (a_gltf_buffer == nullptr) {
        src_mip0_buffer_format = VK_FORMAT_R8G8B8A8_UNORM;
        glm::u8vec4 base_color = uf32_to_u8_vec4(glm::vec4(a_factor, 1.f));
        preset_buffer.resize(num_texels, base_color);
        buffer = reinterpret_cast<uint8_t*>(preset_buffer.data());
    }
    else if (!equals_one_vec(a_factor)) {
        src_mip0_buffer_format = VK_FORMAT_R8G8B8A8_UNORM;
        const glm::u8vec4* gltf_buffer = reinterpret_cast<const glm::u8vec4*>(a_gltf_buffer);
        unique_buffer.reset(new glm::u8vec4[num_texels]);
        const auto& color_factor = glm::vec4(a_factor, 0.f);
        std::transform(std::execution::par_unseq,
            gltf_buffer, gltf_buffer + num_texels, unique_buffer.get(), [&color_factor](const glm::u8vec4& gltf_color) {
                const glm::vec4 linear_color = color_factor * glm::convertSRGBToLinear(spock::u8_to_uf32_vec4(gltf_color));
                return uf32_to_u8_vec4(linear_color);
            }
        );
        buffer = reinterpret_cast<uint8_t*>(unique_buffer.get());
    }
    auto dst_buffer_padding_bytes = calc_padding(dst_buffer_bytes, mip_2d_info_t::BLOCK_ALIGNMENT_BYTES);
    std::unique_ptr<uint8_t[]> unique_dst_buffer = dev_gen_mip_and_bc1_encode_2d(
        *context_, a_command_pool, a_queue,
        src_mip_2d_info, buffer,
        a_dst_mip_2d_info, dst_mip_2d_layout,
        src_mip0_buffer_format
    );
    a_ofs.write(reinterpret_cast<const char*>(unique_dst_buffer.get()), dst_buffer_bytes);
    if (dst_buffer_padding_bytes > 0u) {
        a_ofs.write(reinterpret_cast<const char*>(k_mip_2d_padding_zeroes), dst_buffer_padding_bytes);
    }
    texture_descriptor_array_.upload_one_texture(
        *context_, a_command_pool, a_queue, sampler_array_[a_dst_mip_2d_info.sampler_2d_idx()],
        a_dst_mip_2d_info, dst_mip_2d_layout, unique_dst_buffer.get(),
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
    );
}


void spock::scene_t::
serialize_base_color_image(
    const command_pool_t& a_command_pool, const queue_t& a_queue,
    const mtl_cfg_t& a_mtl_cfg, const uint8_t* a_gltf_buffer, const mip_2d_info_t& a_dst_mip_2d_info,
    std::ofstream& a_ofs
)
{
    mip_2d_info_t src_mip_2d_info{ VK_FORMAT_R8G8B8A8_UNORM, a_dst_mip_2d_info };
    VkFormat src_mip0_buffer_format = VK_FORMAT_R8G8B8A8_SRGB;
    const auto dst_mip_2d_layout = a_dst_mip_2d_info.calc_mip_2d_layout();
    const auto dst_buffer_bytes = dst_mip_2d_layout.get_mip_buffer_bytes();
    std::vector<glm::u8vec4> preset_buffer; // when no base color image specified in gltf
    std::unique_ptr<glm::u8vec4> unique_buffer; // when base color image need to be multiplied by a factor
    const uint8_t* buffer = a_gltf_buffer;
    const uint32_t num_texels = calc_num_texels(src_mip_2d_info.mip0_dim());
    if (a_gltf_buffer == nullptr) {
        src_mip0_buffer_format = VK_FORMAT_R8G8B8A8_UNORM;
        glm::u8vec4 base_color = uf32_to_u8_vec4(a_mtl_cfg.base_color_factor);
        preset_buffer.resize(num_texels, base_color);
        buffer = reinterpret_cast<uint8_t*>(preset_buffer.data());
    }
    else if (!equals_one_vec(a_mtl_cfg.base_color_factor)) {
        src_mip0_buffer_format = VK_FORMAT_R8G8B8A8_UNORM;
        const glm::u8vec4* gltf_buffer = reinterpret_cast<const glm::u8vec4*>(a_gltf_buffer);
        unique_buffer.reset(new glm::u8vec4[num_texels]);
        const auto& color_factor = a_mtl_cfg.base_color_factor;
        std::transform(std::execution::par_unseq,
            gltf_buffer, gltf_buffer + num_texels, unique_buffer.get(), [&color_factor](const glm::u8vec4& gltf_color) {
                const glm::vec4 linear_color = color_factor * glm::convertSRGBToLinear(spock::u8_to_uf32_vec4(gltf_color));
                return uf32_to_u8_vec4(linear_color);
            }
        );
        buffer = reinterpret_cast<uint8_t*>(unique_buffer.get());
    }
    auto dst_buffer_padding_bytes = calc_padding(dst_buffer_bytes, mip_2d_info_t::BLOCK_ALIGNMENT_BYTES);
    std::unique_ptr<uint8_t[]> unique_dst_buffer;
    if (a_dst_mip_2d_info.format() == VK_FORMAT_BC1_RGB_UNORM_BLOCK) {
        unique_dst_buffer = dev_gen_mip_and_bc1_encode_2d(
            *context_, a_command_pool, a_queue,
            src_mip_2d_info, buffer,
            a_dst_mip_2d_info, dst_mip_2d_layout,
            src_mip0_buffer_format
        );
    }
    else {
        unique_dst_buffer = dev_gen_mip_and_bc3_encode_2d(
            *context_, a_command_pool, a_queue,
            src_mip_2d_info, buffer,
            a_dst_mip_2d_info, dst_mip_2d_layout,
            src_mip0_buffer_format
        );
    }
    a_ofs.write(reinterpret_cast<const char*>(unique_dst_buffer.get()), dst_buffer_bytes);
    if (dst_buffer_padding_bytes > 0u) {
        a_ofs.write(reinterpret_cast<const char*>(k_mip_2d_padding_zeroes), dst_buffer_padding_bytes);
    }
    texture_descriptor_array_.upload_one_texture(
        *context_, a_command_pool, a_queue, sampler_array_[a_dst_mip_2d_info.sampler_2d_idx()],
        a_dst_mip_2d_info, dst_mip_2d_layout, unique_dst_buffer.get(),
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
    );
}


void spock::scene_t::
serialize_metallic_roughness_image(
    const command_pool_t& a_command_pool, const queue_t& a_queue,
    const mtl_cfg_t& a_mtl_cfg,
    const uint8_t* a_metallic_roughness_buffer, const uint8_t* a_occlusion_buffer, const mip_2d_info_t& a_dst_mip_2d_info,
    std::ofstream& a_ofs
)
{
    mip_2d_info_t src_mip_2d_info{ VK_FORMAT_R8G8B8A8_UNORM, a_dst_mip_2d_info };
    const auto dst_mip_2d_layout = a_dst_mip_2d_info.calc_mip_2d_layout();
    const auto dst_buffer_bytes = dst_mip_2d_layout.get_mip_buffer_bytes();
    std::vector<glm::u8vec4> preset_buffer; // when no base color image specified in gltf
    std::unique_ptr<glm::u8vec4> unique_buffer; // when base color image need to be multiplied by a factor
    const uint8_t* buffer = a_metallic_roughness_buffer;
    const uint32_t num_texels = calc_num_texels(src_mip_2d_info.mip0_dim());
    if (a_metallic_roughness_buffer == nullptr && a_occlusion_buffer == nullptr) {
        const glm::vec4 metallic_roughness_factor = glm::vec4(1.f, a_mtl_cfg.roughness_factor, a_mtl_cfg.metallic_factor, 1.f);
        glm::u8vec4 metallic_roughness = uf32_to_u8_vec4(metallic_roughness_factor);
        preset_buffer.resize(num_texels, metallic_roughness);
        buffer = reinterpret_cast<uint8_t*>(preset_buffer.data());
    }
    else {
        unique_buffer.reset(new glm::u8vec4[num_texels]);
        if (a_metallic_roughness_buffer != nullptr && a_occlusion_buffer == nullptr) {
            const glm::vec2 roughness_metallic_factor = glm::vec2(a_mtl_cfg.roughness_factor, a_mtl_cfg.metallic_factor);
            const glm::u8vec4* gltf_metallic_roughness_buffer = reinterpret_cast<const glm::u8vec4*>(a_metallic_roughness_buffer);
            std::transform(std::execution::par_unseq,
                gltf_metallic_roughness_buffer, gltf_metallic_roughness_buffer + num_texels, unique_buffer.get(), 
                [&roughness_metallic_factor](const glm::u8vec4& gltf_color) {
                    glm::u8vec2 gltf_roughness_metallic = glm::u8vec2(gltf_color.g, gltf_color.b);
                    glm::vec2 linear_color = roughness_metallic_factor * spock::u8_to_uf32_vec2(gltf_roughness_metallic);
                    return uf32_to_u8_vec4(glm::vec4(1.f, linear_color, 1.f));
                }
            );
        }
        else if (a_metallic_roughness_buffer == nullptr && a_occlusion_buffer != nullptr) {
            const glm::u8vec4* gltf_occlusion_buffer = reinterpret_cast<const glm::u8vec4*>(a_occlusion_buffer);
            const float occlusion_strength = a_mtl_cfg.occlusion_strength;
            const uint8_t u8_roughness = uf32_to_u8(a_mtl_cfg.roughness_factor);
            const uint8_t u8_metallic = uf32_to_u8(a_mtl_cfg.metallic_factor);
            std::transform(std::execution::par_unseq,
                gltf_occlusion_buffer, gltf_occlusion_buffer + num_texels, unique_buffer.get(), 
                [occlusion_strength, u8_roughness, u8_metallic](const glm::u8vec4& gltf_occlusion) {
                    uint8_t occlusion = uf32_to_u8(1.f + occlusion_strength * (u8_to_uf32(gltf_occlusion.r) - 1.f));
                    return glm::u8vec4(occlusion, u8_roughness, u8_metallic, 255);
                }
            );
        }
        else { // a_metallic_roughness_buffer != nullptr && a_occlusion_buffer != nullptr
            const glm::vec2 roughness_metallic_factor = glm::vec2(a_mtl_cfg.roughness_factor, a_mtl_cfg.metallic_factor);
            const glm::u8vec4* gltf_metallic_roughness_buffer = reinterpret_cast<const glm::u8vec4*>(a_metallic_roughness_buffer);
            const glm::u8vec4* gltf_occlusion_buffer = reinterpret_cast<const glm::u8vec4*>(a_occlusion_buffer);
            const float occlusion_strength = a_mtl_cfg.occlusion_strength;
            std::transform(std::execution::par_unseq,
                gltf_metallic_roughness_buffer, gltf_metallic_roughness_buffer + num_texels, gltf_occlusion_buffer, 
                unique_buffer.get(),
                [&roughness_metallic_factor, occlusion_strength](glm::u8vec4 gltf_roughness_metallic_texel,glm::u8vec4 gltf_occlusion_texel) {
                    glm::u8vec2 gltf_roughness_metallic = glm::u8vec2(gltf_roughness_metallic_texel.g, gltf_roughness_metallic_texel.b);
                    glm::vec2 linear_roughness_metallic = roughness_metallic_factor * spock::u8_to_uf32_vec2(gltf_roughness_metallic);
                    uint8_t occlusion = uf32_to_u8(1.f + occlusion_strength * (u8_to_uf32(gltf_occlusion_texel.r) - 1.f));
                    return glm::u8vec4(occlusion, uf32_to_u8_vec2(linear_roughness_metallic), 255);
                }
            );
        }
        buffer = reinterpret_cast<uint8_t*>(unique_buffer.get());
    }
    auto dst_buffer_padding_bytes = calc_padding(dst_buffer_bytes, mip_2d_info_t::BLOCK_ALIGNMENT_BYTES);
    auto unique_dst_buffer = dev_gen_mip_and_bc1_encode_2d(
        *context_, a_command_pool, a_queue,
        src_mip_2d_info, buffer,
        a_dst_mip_2d_info, dst_mip_2d_layout
    );
    a_ofs.write(reinterpret_cast<const char*>(unique_dst_buffer.get()), dst_buffer_bytes);
    if (dst_buffer_padding_bytes > 0u) {
        a_ofs.write(reinterpret_cast<const char*>(k_mip_2d_padding_zeroes), dst_buffer_padding_bytes);
    }
    texture_descriptor_array_.upload_one_texture(
        *context_, a_command_pool, a_queue, sampler_array_[a_dst_mip_2d_info.sampler_2d_idx()],
        a_dst_mip_2d_info, dst_mip_2d_layout, unique_dst_buffer.get(),
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
    );
}


void spock::scene_t::
serialize_normal_image(
    const command_pool_t& a_command_pool, const queue_t& a_queue,
    const mtl_cfg_t & a_mtl_cfg, 
    const uint8_t * a_gltf_buffer, const mip_2d_info_t & a_dst_mip_2d_info, 
    std::ofstream & a_ofs
)
{
    mip_2d_info_t src_mip_2d_info{ VK_FORMAT_R8G8B8A8_UNORM, a_dst_mip_2d_info };
    const auto dst_mip_2d_layout = a_dst_mip_2d_info.calc_mip_2d_layout();
    const auto dst_buffer_bytes = dst_mip_2d_layout.get_mip_buffer_bytes();
    std::vector<glm::u8vec4> preset_buffer; // when no normal image specified in gltf
    const uint8_t* buffer = a_gltf_buffer;
    const uint32_t num_texels = calc_num_texels(src_mip_2d_info.mip0_dim());
    if (a_gltf_buffer == nullptr) {
        src_mip_2d_info = mip_2d_info_t{ VK_FORMAT_R8G8B8A8_UNORM, a_dst_mip_2d_info };
        const glm::u8vec4 preset_normal(0, 0, 0, 0); // if (0, 0, 255, 0), the preset normal got distorted by padded 0, lead to artifacts in shader
        preset_buffer.resize(num_texels, preset_normal);
        buffer = reinterpret_cast<uint8_t*>(preset_buffer.data());
    }
    auto dst_buffer_padding_bytes = calc_padding(dst_buffer_bytes, mip_2d_info_t::BLOCK_ALIGNMENT_BYTES);
    auto unique_dst_buffer = xyz_to_bc5_encode_oct_normal_texture(
        src_mip_2d_info, buffer,
        a_dst_mip_2d_info, dst_mip_2d_layout
    );
    a_ofs.write(reinterpret_cast<const char*>(unique_dst_buffer.get()), dst_buffer_bytes);
    if (dst_buffer_padding_bytes > 0u) {
        a_ofs.write(reinterpret_cast<const char*>(k_mip_2d_padding_zeroes), dst_buffer_padding_bytes);
    }
    texture_descriptor_array_.upload_one_texture(
        *context_, a_command_pool, a_queue, sampler_array_[a_dst_mip_2d_info.sampler_2d_idx()],
        a_dst_mip_2d_info, dst_mip_2d_layout, unique_dst_buffer.get(),
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
    );
}

std::array<spock::scene_t::offsets_of_mtl_type_t, spock::MTL_TYPE_COUNT> spock::scene_t::
assemble_offsets_of_mtl_types() const
{
    std::array<offsets_of_mtl_type_t, MTL_TYPE_COUNT> offsets_of_mtl_types;

    for (uint8_t i = 0; i < +MTL_TYPE_COUNT; ++i) {
        offsets_of_mtl_types[i] = offsets_of_mtl_type_t{
            mtl_param_emissive_idx_offsets_[i],
            mtl_param_alpha_cutoff_offsets_[i],
            texture_strides_[i],
            texture_offsets_[i],
            index_offsets_[i] / 3,
            0, 
            0,
            0
        };
    }
    return offsets_of_mtl_types;
}


void spock::scene_t::write_glsl(const std::string & a_glsl_path) const
{
    using namespace std::filesystem;
    const auto glsl_dir = path(a_glsl_path).parent_path();
    if (!exists(glsl_dir)) {
        ensure(create_directories(glsl_dir), "failed to create scene cache dir " + glsl_dir.string());
    }
    std::ofstream ofs(a_glsl_path, std::ios::out | std::ios::trunc | std::ios::binary);
    const std::string_view guard{ "SPOCK_SCENE_PARAMS_HSL" };

    ofs << "#if __VERSION__ == 110\n#version 460\n#endif\n\n";

    ofs << "#ifndef " << guard << '\n';
    ofs << "#define " << guard << '\n' << '\n';
    ofs << "#define SCENE_MTL_TYPE_COUNT " << +MTL_TYPE_COUNT << '\n' << '\n';

    std::vector<std::string> mtl_type_strs;
    size_t mtl_type_str_len_max = 0;
    std::vector<std::string> mtl_type_short_strs;
    size_t mtl_type_short_str_len_max = 0;
    for (size_t i = 0; i < static_cast<size_t>(MTL_TYPE_COUNT); ++i) {
        mtl_type_strs.push_back(str_from_mtl_type_e(static_cast<mtl_type_e>(i)).data());
        mtl_type_str_len_max = std::max(mtl_type_strs.back().length(), mtl_type_str_len_max);
        mtl_type_short_strs.push_back(short_str_from_mtl_type(static_cast<mtl_type_e>(i)));
        mtl_type_short_str_len_max = std::max(mtl_type_short_strs.back().length(), mtl_type_short_str_len_max);
    }
    for (size_t i = 0; i < static_cast<size_t>(MTL_TYPE_COUNT); ++i) {
        auto curr_len = mtl_type_strs[i].length();
        mtl_type_strs[i].append(mtl_type_str_len_max - curr_len + 1, ' ');
        curr_len = mtl_type_short_strs[i].length();
        mtl_type_short_strs[i].append(mtl_type_short_str_len_max - curr_len + 1, ' ');
    }
    for (size_t i = 0; i < static_cast<size_t>(MTL_TYPE_COUNT); ++i) {
        ofs << "#define SCENE_" << mtl_type_strs[i] << ' ' << i << '\n';
    }
    ofs << '\n';

    // lighting_utils.h.glsl
    //ofs << spotlight_t::definition_to_glsl_str() << '\n';
    //ofs << point_light_t::definition_to_glsl_str() << '\n';
    //ofs << directional_light_t::definition_to_glsl_str() << '\n';

    // common_utils.h.glsl
    //ofs << "struct scene_offsets_of_mtl_type_t { \n"
    //    << "    uint mtl_param_emissive_idx;\n"
    //    << "    uint mtl_param_alpha_cutoff;\n"
    //    << "    uint tex_stride;\n"
    //    << "    uint tex;\n"
    //    << "};\n\n";

    const uint32_t punctual_light_size_in_vec4[] = {
        spotlight_t::SPOTLIGHT_SIZE_IN_VEC4,
        point_light_t::POINT_LIGHT_SIZE_IN_VEC4,
        directional_light_t::DIRECTIONAL_LIGHT_SIZE_IN_VEC4
    };

    for (size_t i = 0; i < static_cast<size_t>(PUNCTUAL_LIGHT_TYPE_COUNT); ++i) {
        const auto light_type_str = short_str_from_punctual_light_type(static_cast<punctual_light_type_e>(i));
        ofs << "#define SCENE_LIGHT_COUNT_" << light_type_str << "         " << punctual_light_counts_[i] << '\n';
        ofs << "#define SCENE_LIGHT_OFFSET_" << light_type_str << "        " << punctual_light_offsets_[i] << '\n';
        ofs << "#define SCENE_LIGHT_SIZE_IN_VEC4_" << light_type_str << "  " << punctual_light_size_in_vec4[i] << '\n' << '\n';
    }
    ofs << "#define SCENE_PUNCTUAL_LIGHT_COUNT         " << punctual_light_counts_.back(); 
    if (punctual_light_counts_.back() < 0) {
        ofs << "    /* a built-in directional light is added to the scene */";
    }
    ofs << '\n';
    ofs << "#define SCENE_PUNCTUAL_LIGHT_SIZE_IN_VEC4  " << punctual_light_offsets_.back() << '\n' << '\n';

    ofs << "#define SCENE_VERTEX_COUNT    " << vertex_offsets_.back() << '\n';
    ofs << "#define SCENE_INDEX_COUNT     " << index_offsets_.back() << '\n';
    ofs << "#define SCENE_TRIANGLE_COUNT  " << index_offsets_.back() / 3 << '\n' << '\n';

    for (size_t i = 0; i < static_cast<size_t>(MTL_TYPE_COUNT); ++i) {
        ofs << "#define SCENE_TRIANGLE_OFFSET_" << mtl_type_short_strs[i] << ' ' << index_offsets_[i] / 3 << '\n';
    }
    SPOCK_PARANOID(mtl_param_alpha_cutoff_offsets_.back() % k_mtl_param_alignment == 0, "mtl_param is not properly aligned.");
    ofs << "\n#define SCENE_MTL_PARAM_SIZE_IN_UVEC" << k_mtl_param_alignment << "  " << mtl_param_alpha_cutoff_offsets_.back() / k_mtl_param_alignment << '\n' << '\n';

    ofs << "#define SCENE_EMISSIVE_TEXTURE_COUNT  " << num_emissive_textures_ << '\n';
    ofs << "#define SCENE_TOTAL_TEXTURE_COUNT     " << texture_offsets_.back() << '\n' << '\n';

    ofs << "#define SCENE_AABB_LOWER_X   " << aabb_lower_.x << '\n';
    ofs << "#define SCENE_AABB_LOWER_Y   " << aabb_lower_.y << '\n';
    ofs << "#define SCENE_AABB_LOWER_Z   " << aabb_lower_.z << '\n' << '\n';
    ofs << "#define SCENE_AABB_UPPER_X   " << aabb_upper_.x << '\n';
    ofs << "#define SCENE_AABB_UPPER_Y   " << aabb_upper_.y << '\n';
    ofs << "#define SCENE_AABB_UPPER_Z   " << aabb_upper_.z << '\n';
    ofs << "#define SCENE_AABB_DIAG_LEN  " << glm::length(aabb_upper_ - aabb_lower_) << '\n';

    ofs << "\n#endif\n\n";

}


spock::scene_t::
scene_t(
    const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue,
    uint32_t a_gltf_count, const std::string* a_gltf_paths, const std::string & a_cache_path, const std::string& a_glsl_path,
    const std::string & a_identifier, spock_scene_flags_t a_scene_flags,
    const directional_light_t& a_fallback_directional_light
)
    : identifier_{ a_identifier }
    , context_{ &a_context }
    , punctual_light_counts_{}
    , punctual_light_offsets_{}
    , aabb_lower_{  glm::vec3(std::numeric_limits<float>::infinity()) }
    , aabb_upper_{ -glm::vec3(std::numeric_limits<float>::infinity()) }
    , vertex_offsets_{}
    , index_offsets_{}
    , mtl_param_emissive_idx_offsets_{}
    , mtl_param_alpha_cutoff_offsets_{} 
    , num_emissive_textures_ { 0 }
    , texture_strides_{}
    , texture_offsets_{}
    , texture_descriptor_array_ (a_context)

{
    using namespace std::filesystem;

    host_timer_t timer;

    init_buffer_usage_flags(a_scene_flags);
    std::vector<std::string> gltf_paths;
    gltf_paths.reserve(a_gltf_count);
    for (uint32_t i = 0; i < a_gltf_count; ++i) {
        const std::string& p = a_gltf_paths[i];
        if (std::find(gltf_paths.cbegin(), gltf_paths.cend(), p) != gltf_paths.cend()) {
            warn(identifier_ + " scene: duplicate gltf path: " + p);
        }
        else {
            gltf_paths.push_back(p);
        }
    }

    bool loaded_from_cache = false;

    if (exists(a_cache_path)) {
        std::unique_ptr<uint8_t[]> cache_body = get_cache_body(a_cache_path, gltf_paths, a_fallback_directional_light);
        const uint8_t* cache_body_bin_stream = cache_body.get();
        if (cache_body_bin_stream != nullptr) {
            load_from_cache(a_command_pool, a_queue, &cache_body_bin_stream);
            loaded_from_cache = true;
            info(identifier_ + " scene: loaded from cache");
        }
    }
    if (!loaded_from_cache) {
        info(identifier_ + " scene: creating cache...");
        ensure(
            std::all_of(gltf_paths.cbegin(), gltf_paths.cend(), [](const std::string& p) {return exists(p); }),
            "not all input gltf paths are valid"
        );
        bool cache_created = false;

        const auto cache_dir = path(a_cache_path).parent_path();
        if (!exists(cache_dir)) {
            ensure(create_directories(cache_dir), identifier_ + " scene: failed to create scene cache dir " + cache_dir.string());
        }

        create_cache(a_command_pool, a_queue, gltf_paths, a_cache_path, a_fallback_directional_light, &cache_created);
        write_glsl(a_glsl_path);

        if (!cache_created) {
            remove(a_cache_path);
            except(identifier_ + " scene: failed to create cache");
        }
        info(identifier_ + " scene: created cache");
    }

    position_buffer_view_ = buffer_view_t(*context_, &position_buffer_, k_vk_format_position);
    normal_buffer_view_ = buffer_view_t(*context_, &normal_buffer_, k_vk_format_normal);
    uv_buffer_view_ = buffer_view_t(*context_, &uv_buffer_, k_vk_format_uv);
    index_buffer_view_ = buffer_view_t(*context_, &index_buffer_, k_vk_format_index);

    triangle_mtl_idx_buffer_view_ = buffer_view_t(*context_, &triangle_mtl_idx_buffer_, k_vk_format_triangle_mtl_idx);
    mtl_param_buffer_view_ = buffer_view_t(*context_, &mtl_param_buffer_, k_vk_format_mtl_param);

    info(identifier_ + " scene: loading took " + std::to_string(timer.elapsed_ms()) + " ms");
}


spock::vertex_input_descriptions_t 
spock::scene_t::get_vertex_input_descriptions(vertex_input_flags_t a_flags)
{
    const uint32_t binding_count = k_vertex_binding_count;
    vertex_input_descriptions_t descriptions;

    const uint32_t strides[] = {
        static_cast<uint32_t>(sizeof(position_type)),
        static_cast<uint32_t>(sizeof(normal_type)),
        static_cast<uint32_t>(sizeof(uv_type))
    };

    const VkFormat formats[] = {
        k_vk_format_position, // position
        k_vk_format_normal, // normal
        k_vk_format_uv     // uv
    };
    const vertex_input_flag_bits_e flag_bits[] = {
        VERTEX_INPUT_POSITION_BIT, 
        VERTEX_INPUT_NORMAL_BIT,
        VERTEX_INPUT_UV_BIT
    };
    for (uint32_t i = 0, j = 0; i < binding_count; ++i) {
        if (a_flags & flag_bits[i]) {
            descriptions.bindings.push_back(VkVertexInputBindingDescription{});
            descriptions.attributes.push_back(VkVertexInputAttributeDescription{});

            descriptions.bindings[j].binding = j;
            descriptions.bindings[j].stride = strides[i];
            descriptions.bindings[j].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

            descriptions.attributes[j].binding = j;
            descriptions.attributes[j].location = j;
            descriptions.attributes[j].format = formats[i];
            descriptions.attributes[j].offset = 0;

            ++j;
        }
    }
    return descriptions;
}


spock::vertex_bindings_t spock::scene_t::
get_vertex_bindings(vertex_input_flags_t a_flags) const
{
    std::vector<VkBuffer> buffers;
    const VkBuffer all_buffers[] = { position_buffer_, normal_buffer_, uv_buffer_ };
    std::array<vertex_input_flag_bits_e, 3> flag_bits = { VERTEX_INPUT_POSITION_BIT, VERTEX_INPUT_NORMAL_BIT, VERTEX_INPUT_UV_BIT };
    for (size_t i = 0; i < flag_bits.size(); ++i) {
        if (a_flags & flag_bits[i]) {
            buffers.push_back(all_buffers[i]);
        }
    }
    uint32_t binding_count = static_cast<uint32_t>(buffers.size());
    std::vector<VkDeviceSize> offsets(binding_count, 0);
    return vertex_bindings_t{ 0, binding_count, std::move(buffers), std::move(offsets) };
}


spock::index_binding_t spock::scene_t::
get_index_binding() const
{
    return index_binding_t{ index_buffer_, 0, k_vk_index_type, index_offsets_[MTL_TYPE_COUNT]};
}


std::pair<uint32_t, const spock::directional_light_t*> spock::scene_t::
get_directional_lights() const
{
    const uint32_t count = punctual_light_counts_[PUNCTUAL_LIGHT_TYPE_DIRECTIONAL];
    if (count == 0) return std::make_pair(0, nullptr);

    const uint32_t begin_vec4_idx = punctual_light_offsets_[PUNCTUAL_LIGHT_TYPE_DIRECTIONAL];
    return std::make_pair(count, reinterpret_cast<const directional_light_t*>(unique_punctual_light_buffer_.get() + begin_vec4_idx));
}


std::pair<uint32_t,const spock::point_light_t*> spock::scene_t::get_point_lights() const
{
    const uint32_t count = punctual_light_counts_[PUNCTUAL_LIGHT_TYPE_POINT];
    if (count == 0) return std::make_pair(0, nullptr);

    const uint32_t begin_vec4_idx = punctual_light_offsets_[PUNCTUAL_LIGHT_TYPE_POINT];
    return std::make_pair(count, reinterpret_cast<const point_light_t*>(unique_punctual_light_buffer_.get() + begin_vec4_idx));
}


std::pair<uint32_t, const spock::spotlight_t*> spock::scene_t::get_spotlights() const
{
    const uint32_t count = punctual_light_counts_[PUNCTUAL_LIGHT_TYPE_SPOT];
    if (count == 0) return std::make_pair(0, nullptr);

    const uint32_t begin_vec4_idx = punctual_light_offsets_[PUNCTUAL_LIGHT_TYPE_SPOT];
    return std::make_pair(count, reinterpret_cast<const spotlight_t*>(unique_punctual_light_buffer_.get() + begin_vec4_idx));
}


size_t spock::scene_t::
get_mtl_param_buffer_bytes() const
{
    uint32_t count = mtl_param_alpha_cutoff_offsets_[MTL_TYPE_COUNT];
    return count * sizeof(mtl_param_type);
}


