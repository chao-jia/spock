// <glfw3.h> (in <spock/swapchain.hpp>) must be after 
// <windows.h> (in <vk_mem_alloc.h> -- <vulkan/vulkan.h> -- <spock/context.hpp>)
// to avoid warning C4005: 'APIENTRY': macro redefinition

#include <spock/context.hpp>
#include <spock/utils.hpp>
#include <spock/generated.hpp>
#include <nlohmann/json.hpp>
#include <algorithm>
#include <backends/imgui_impl_glfw.h>

#if defined(__INTELLISENSE__)
#undef VCPKG_IMGUI_USE_VOLK
#endif
#include <backends/imgui_impl_vulkan.h>

using namespace std::filesystem;

namespace {

// This function returns the monitor that contains the greater window area https://stackoverflow.com/a/31526753/2277106
GLFWmonitor* 
get_current_monitor(GLFWwindow *window)
{
    int nmonitors, i;
    int wx, wy, ww, wh;
    int mx, my, mw, mh;
    int overlap, bestoverlap;
    GLFWmonitor *bestmonitor;
    GLFWmonitor **monitors;
    const GLFWvidmode *mode;

    bestoverlap = 0;
    bestmonitor = NULL;

    glfwGetWindowPos(window, &wx, &wy);
    glfwGetWindowSize(window, &ww, &wh);
    monitors = glfwGetMonitors(&nmonitors);

    for (i = 0; i < nmonitors; i++) {
        mode = glfwGetVideoMode(monitors[i]);
        glfwGetMonitorPos(monitors[i], &mx, &my);
        mw = mode->width;
        mh = mode->height;

        overlap =
            std::max(0, std::min(wx + ww, mx + mw) - std::max(wx, mx)) *
            std::max(0, std::min(wy + wh, my + mh) - std::max(wy, my));

        if (bestoverlap < overlap) {
            bestoverlap = overlap;
            bestmonitor = monitors[i];
        }
    }
    return bestmonitor;
}


inline void 
imgui_check_vulkan_callback(VkResult a_vk_result)
{
    SPOCK_VK_CHECK(a_vk_result, std::string("error: imgui vulkan: ") + spock::str_from_VkResult(a_vk_result).data());
}


const std::string k_imgui_ini_scale_factor_prefix = "; spock_scale_factor = "; // lines starting with ';' in imgui.ini are ignored by imgui


} // anonymous namespace


spock::window_t::
window_t(const json& a_window_config_json)
    : full_screen_{ false }
    , window_pos_x_{ 0 }
    , window_pos_y_{ 0 }
    , monitor_scale_x_{ 1.f }
    , monitor_scale_y_{ 1.f }
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // no opengl api
    if (a_window_config_json.contains("full_screen")) {
        full_screen_ = a_window_config_json["full_screen"].get<bool>();
    }
    windowed_width_ = a_window_config_json["width"].get<int>();
    windowed_height_ = a_window_config_json["height"].get<int>();
    const std::string title = a_window_config_json["title"].get<std::string>().c_str();

    int init_width = windowed_width_;
    int init_height = windowed_height_;

    GLFWmonitor* monitor = NULL;
    monitor = glfwGetPrimaryMonitor();
    ensure(monitor != NULL, "glfw cannot find the primary monitor");
    glfwGetMonitorContentScale(monitor, &monitor_scale_x_, &monitor_scale_y_);

    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
    if (mode) {
        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
        window_pos_x_ = (mode->width - windowed_width_) / 2;
        window_pos_y_ = (mode->height - windowed_height_) / 2;
        if (full_screen_) {
            init_width = mode->width;
            init_height = mode->height;
        }
        else {
            glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
            monitor = NULL;
        }
    }
    else {
        err("glfw cannot get the video mode of the primary monitor");
    }
    
    window_ = glfwCreateWindow( init_width, init_height, title.c_str(), monitor, NULL);

    ensure(window_ != NULL, "glfw creating window failed");

    glfwGetFramebufferSize(window_, &width_, &height_);
    if (!full_screen_) {
        glfwGetWindowSize(window_, &windowed_width_, &windowed_height_);
        glfwGetWindowPos(window_, &window_pos_x_, &window_pos_y_);
    }
}


spock::window_t::
~window_t()
{
    verbose("destroy glfw window...");
    glfwDestroyWindow(window_);
    glfwTerminate();
}

auto spock::window_t::
check_size() -> state_e
{
    int w = width_, h = height_;
    glfwGetFramebufferSize(window_, &width_, &height_);
    if (width_ == 0 || height_ == 0) {
        return state_e::MINIMIZED;
    }

    auto monitor = get_current_monitor(window_);
    if (monitor) {
        glfwGetMonitorContentScale(monitor, &monitor_scale_x_, &monitor_scale_y_);
    }
    else {
        warn("glfw failed to get current monitor");
    }

    if (w != width_ || h != height_) {
        return state_e::RESIZED;
    }
    return state_e::UNCHANGED;
}


void spock::window_t::toggle_full_screen()
{
    if (!full_screen_) {
        glfwGetWindowSize(window_, &windowed_width_, &windowed_height_);
        glfwGetWindowPos(window_, &window_pos_x_, &window_pos_y_);
        auto monitor = get_current_monitor(window_);
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        if (mode) {
            glfwSetWindowMonitor(window_, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
            glfwGetMonitorContentScale(monitor, &monitor_scale_x_, &monitor_scale_y_);
            full_screen_ = true;
        }
        else {
            warn("glfw failed to enter full screen mode");
        }
    }
    else {
        glfwSetWindowMonitor(window_, NULL, window_pos_x_, window_pos_y_, windowed_width_, windowed_height_, GLFW_DONT_CARE);
        full_screen_ = false;
    }
}


VkSurfaceKHR spock::window_t::
create_surface(VkInstance a_vk_instance) const
{
    VkSurfaceKHR vk_surface;
    SPOCK_VK_CALL(glfwCreateWindowSurface(a_vk_instance, window_, nullptr, &vk_surface));

    ensure(glfwVulkanSupported(), "GLFW: Vulkan Not Supported");
    
    return vk_surface;
}

std::vector<const char*> spock::window_t::
instance_extensions()
{
    uint32_t num_instance_extensions = 0;
    const char** instance_extension_cstrs;
    instance_extension_cstrs = glfwGetRequiredInstanceExtensions(&num_instance_extensions);
    return std::vector<const char*>(instance_extension_cstrs, instance_extension_cstrs + num_instance_extensions);
}


void spock::imgui_t::
set_style()
{
    ImGuiStyle& style = ImGui::GetStyle();
    style = ImGuiStyle();
    style.ScaleAllSizes(current_scale_factor_);

    style.GrabRounding = 4.0f * current_scale_factor_;
    style.FrameRounding = 4.0f * current_scale_factor_;
    style.WindowRounding = 3.0f * current_scale_factor_;

    ImVec4* colors = style.Colors;

    colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
    colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
    colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.06f, 0.06f, 0.94f);
    colors[ImGuiCol_ChildBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.00f);
    colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
    colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
    colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
    colors[ImGuiCol_FrameBg] = ImVec4(0.20f, 0.21f, 0.22f, 0.54f);
    colors[ImGuiCol_FrameBgHovered] = ImVec4(0.40f, 0.40f, 0.40f, 0.40f);
    colors[ImGuiCol_FrameBgActive] = ImVec4(0.18f, 0.18f, 0.18f, 0.67f);
    colors[ImGuiCol_TitleBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
    colors[ImGuiCol_TitleBgActive] = ImVec4(0.12f, 0.12f, 0.12f, 1.00f);
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
    colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
    colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
    colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
    colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
    colors[ImGuiCol_CheckMark] = ImVec4(0.94f, 0.94f, 0.94f, 1.00f);
    colors[ImGuiCol_SliderGrab] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
    colors[ImGuiCol_SliderGrabActive] = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);
    colors[ImGuiCol_Button] = ImVec4(0.44f, 0.44f, 0.44f, 0.40f);
    colors[ImGuiCol_ButtonHovered] = ImVec4(0.46f, 0.47f, 0.48f, 1.00f);
    colors[ImGuiCol_ButtonActive] = ImVec4(0.42f, 0.42f, 0.42f, 1.00f);
    colors[ImGuiCol_Header] = ImVec4(0.70f, 0.70f, 0.70f, 0.31f);
    colors[ImGuiCol_HeaderHovered] = ImVec4(0.70f, 0.70f, 0.70f, 0.80f);
    colors[ImGuiCol_HeaderActive] = ImVec4(0.48f, 0.50f, 0.52f, 1.00f);
    colors[ImGuiCol_Separator] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
    colors[ImGuiCol_SeparatorHovered] = ImVec4(0.72f, 0.72f, 0.72f, 0.78f);
    colors[ImGuiCol_SeparatorActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
    colors[ImGuiCol_ResizeGrip] = ImVec4(0.91f, 0.91f, 0.91f, 0.25f);
    colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.81f, 0.81f, 0.81f, 0.67f);
    colors[ImGuiCol_ResizeGripActive] = ImVec4(0.46f, 0.46f, 0.46f, 0.95f);
    colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
    colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
    colors[ImGuiCol_PlotHistogram] = ImVec4(0.73f, 0.60f, 0.15f, 1.00f);
    colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
    colors[ImGuiCol_TextSelectedBg] = ImVec4(0.87f, 0.87f, 0.87f, 0.35f);
    colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
    colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
    colors[ImGuiCol_NavHighlight] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
    colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);

}


void spock::imgui_t::
deinit()
{
    if (vk_descriptor_pool_ != VK_NULL_HANDLE) {
        vkDestroyDescriptorPool(vk_dev_, vk_descriptor_pool_, NULL);
        vk_descriptor_pool_ = VK_NULL_HANDLE;
        vk_dev_ = VK_NULL_HANDLE;
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ini_data_ = ImGui::SaveIniSettingsToMemory();
    }
}


void spock::imgui_t::
read_imgui_ini_file()
{
    if (std::filesystem::exists(ini_path_str_)) {
        const auto ini_ifs_str = file_to_str(ini_path_str_);
        const auto scale_factor_prefix_len = k_imgui_ini_scale_factor_prefix.length();
        if (ini_ifs_str.substr(0, scale_factor_prefix_len) == k_imgui_ini_scale_factor_prefix) {
            const auto end_scale_factor_pos = ini_ifs_str.find_first_of('\n');
            SPOCK_PARANOID(end_scale_factor_pos != std::string::npos, "bug: no new line for imgui scale factor written to " + ini_path_str_);
            const auto& scale_factor_base64_str = ini_ifs_str.substr(scale_factor_prefix_len, end_scale_factor_pos - scale_factor_prefix_len);
            const auto scale_factor_u8s = decode_base64(scale_factor_base64_str.c_str(), scale_factor_base64_str.length());
            ini_scale_factor_ = *reinterpret_cast<const float*>(scale_factor_u8s.data());
            if (ini_ifs_str.size() > end_scale_factor_pos + 1) {
                ini_data_ = ini_ifs_str.substr(end_scale_factor_pos + 1);
            }
        }
    }
}


void spock::imgui_t::
write_imgui_ini_file()
{
    const auto scale_factor_prefix_len = k_imgui_ini_scale_factor_prefix.length();
    // write current scale factor
    const uint8_t* scale_factor_ptr = reinterpret_cast<uint8_t*>(&current_scale_factor_);
    const auto& scale_factor_base64_str = encode_base64(scale_factor_ptr, sizeof(current_scale_factor_));
    std::string output = k_imgui_ini_scale_factor_prefix + scale_factor_base64_str + '\n' + ini_data_;
    c_str_to_file(output.c_str(), output.length(), ini_path_str_);
}


void spock::imgui_t::init_imgui_fonts()
{
    fonts_.clear();
    imgui_io_->Fonts->Clear();
    fonts_.reserve(font_path_size_pairs_.size());
    for (const auto& p : font_path_size_pairs_) {
        if (!std::filesystem::exists(p.first)) {
            spock::warn("font file " + p.first + " does not exist");
        }
        else {
            auto im_font = imgui_io_->Fonts->AddFontFromFileTTF(p.first.c_str(), p.second * current_scale_factor_);
            if (!im_font) {
                spock::warn("imgui cannot add font file " + p.first);
            }
            else {
                fonts_.push_back(im_font);
            }
        }
    }
    if (fonts_.empty()) {
        ImFontConfig font_cfg;
        font_cfg.SizePixels = 14 * current_scale_factor_;
        fonts_.push_back(imgui_io_->Fonts->AddFontDefault(&font_cfg)); // fallback font
    }
    fonts_.shrink_to_fit();
}


spock::imgui_t::imgui_t(const std::vector< std::pair<std::string, float> >& a_font_path_size_pairs, const std::string& a_imgui_ini_path_str)
    : im_draw_data_{ nullptr }
    , vk_dev_{ VK_NULL_HANDLE }
    , vk_descriptor_pool_{ VK_NULL_HANDLE }
    , current_scale_factor_{ 0.f }
    , ini_scale_factor_{ 0.f }
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    imgui_io_ = &ImGui::GetIO();
    if (a_imgui_ini_path_str.empty()) {
        ini_path_str_ = imgui_io_->IniFilename;
    }
    else {
        ini_path_str_ = a_imgui_ini_path_str;
    }
    imgui_io_->IniFilename = NULL; // ini file i/o managed by imgui_t https://github.com/ocornut/imgui/issues/4294#issuecomment-874720489
    read_imgui_ini_file();

    for (const auto& p : a_font_path_size_pairs) {
        if (!std::filesystem::exists(p.first)) {
            spock::warn("font file " + p.first + " does not exist");
        }
        else {
            font_path_size_pairs_.push_back(p);
        }
    }
}


void spock::imgui_t::
init
(
    const window_t& a_window,
    const context_t& a_context,
    VkCommandBuffer a_vk_cmd_buf,
    const queue_t& a_queue,
    VkRenderPass a_vk_render_pass,
    uint32_t a_subpass,
    uint32_t a_image_count,
    VkSampleCountFlagBits a_sample_count_flag_bits
)
{
    deinit();
    const float scale_factor = a_window.get_scale_factor();
    if (scale_factor != current_scale_factor_) {
        current_scale_factor_ = scale_factor;
        init_imgui_fonts();
    }
    if (ini_scale_factor_ != current_scale_factor_) {
        ini_data_ = "";
    }
    ImGui::LoadIniSettingsFromMemory(ini_data_.c_str());
    auto pool_size = imgui_t::imgui_descriptor_pool_size();
    auto descriptor_pool_create_info = make_VkDescriptorPoolCreateInfo();
    descriptor_pool_create_info.maxSets = 1;
    descriptor_pool_create_info.poolSizeCount = 1;
    descriptor_pool_create_info.pPoolSizes = &pool_size;

    vk_dev_ = a_context;
    SPOCK_VK_CALL( vkCreateDescriptorPool( vk_dev_, &descriptor_pool_create_info, nullptr, &vk_descriptor_pool_) );

    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForVulkan(a_window, true);
    ImGui_ImplVulkan_InitInfo init_info{};
    init_info.Instance = a_context.vk_instance();
    init_info.PhysicalDevice = a_context.vk_physical_dev();
    init_info.Device = a_context;
    init_info.QueueFamily = a_queue.family_index();
    init_info.Queue = a_queue;
    init_info.PipelineCache = VK_NULL_HANDLE;
    init_info.DescriptorPool = vk_descriptor_pool_;
    init_info.Subpass = a_subpass;
    init_info.MinImageCount = 2;
    init_info.ImageCount = a_image_count;
    init_info.MSAASamples = a_sample_count_flag_bits;
    init_info.Allocator = NULL;
    init_info.CheckVkResultFn = imgui_check_vulkan_callback;
    ImGui_ImplVulkan_Init(&init_info, a_vk_render_pass);

    auto vk_command_buffer_begin_info = spock::make_VkCommandBufferBeginInfo();
    vk_command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    imgui_check_vulkan_callback(vkBeginCommandBuffer(a_vk_cmd_buf, &vk_command_buffer_begin_info));

    ImGui_ImplVulkan_CreateFontsTexture(a_vk_cmd_buf);
    vkEndCommandBuffer(a_vk_cmd_buf);
    auto vk_submit_info = make_VkSubmitInfo();
    vk_submit_info.commandBufferCount = 1;
    vk_submit_info.pCommandBuffers = &a_vk_cmd_buf;

    imgui_check_vulkan_callback(vkQueueSubmit(a_queue, 1, &vk_submit_info, VK_NULL_HANDLE));
    vkQueueWaitIdle(a_queue);

    ImGui_ImplVulkan_DestroyFontUploadObjects();
    set_style();  
}


spock::imgui_t::
imgui_t()
    : imgui_io_{ NULL }
    , im_draw_data_{ NULL }
    , vk_dev_{ VK_NULL_HANDLE }
    , vk_descriptor_pool_{ VK_NULL_HANDLE }
    , current_scale_factor_{ 0.f }
    , ini_scale_factor_{ 0.f }
{}


spock::imgui_t::
imgui_t(imgui_t&& a_imgui) noexcept
    : imgui_io_{ std::exchange(a_imgui.imgui_io_, nullptr) }
    , im_draw_data_{ std::exchange(a_imgui.im_draw_data_, nullptr) }
    , vk_dev_ { std::exchange(a_imgui.vk_dev_, VK_NULL_HANDLE) }
    , vk_descriptor_pool_{ std::exchange(a_imgui.vk_descriptor_pool_, VK_NULL_HANDLE) }
    , current_scale_factor_{ a_imgui.current_scale_factor_ }
    , font_path_size_pairs_{ std::move(a_imgui.font_path_size_pairs_) }
    , fonts_{ std::move(a_imgui.fonts_) }
    , ini_scale_factor_{ a_imgui.ini_scale_factor_ }
    , ini_path_str_{ std::move(ini_path_str_) }
    , ini_data_{ std::move(a_imgui.ini_data_) }
{
}


spock::imgui_t& spock::imgui_t::
operator=(imgui_t&& a_imgui) noexcept
{
    if (this != &a_imgui) {
        imgui_io_ = std::exchange(a_imgui.imgui_io_, nullptr);
        im_draw_data_ = std::exchange(a_imgui.im_draw_data_, nullptr);
        vk_dev_ = std::exchange(a_imgui.vk_dev_, VK_NULL_HANDLE);
        vk_descriptor_pool_ = std::exchange(a_imgui.vk_descriptor_pool_, VK_NULL_HANDLE);
        current_scale_factor_ = a_imgui.current_scale_factor_;
        font_path_size_pairs_ = std::move(a_imgui.font_path_size_pairs_);
        fonts_ = std::move(a_imgui.fonts_);
        ini_scale_factor_ = a_imgui.ini_scale_factor_;
        ini_path_str_ = a_imgui.ini_path_str_;
        ini_data_ = a_imgui.ini_data_;
    }
    return *this;
}


spock::imgui_t::
~imgui_t()
{
    if (imgui_io_ != nullptr) {
        verbose("destroy imgui");
        deinit();
        ImGui::DestroyContext();
        write_imgui_ini_file();
    }
}


void spock::imgui_t::
push_font(size_t i)
{
    const auto idx = std::clamp(i, static_cast<size_t>(0), fonts_.size() - 1);
    ImGui::PushFont(fonts_[idx]);
}


void spock::imgui_t::new_frame()
{
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}


void spock::imgui_t::
begin_render(const char* a_name, bool* a_open, ImGuiWindowFlags a_flags)
{
    if (ini_data_.empty()) {
        ImGui::SetNextWindowSize({ 0.f, 0.f });
    }
    ImGui::Begin(a_name, a_open, a_flags);
}


void spock::imgui_t::
end_render()
{
    ImGui::End();
    ImGui::Render();

    im_draw_data_ = ImGui::GetDrawData();
    if (imgui_io_->WantSaveIniSettings) {
        ini_data_ = ImGui::SaveIniSettingsToMemory();
        ini_scale_factor_ = current_scale_factor_;
        imgui_io_->WantSaveIniSettings = false;
    }
}


void spock::imgui_t::
record_command_buffer(VkCommandBuffer a_vk_command_buffer)
{
    const bool is_minimized = (im_draw_data_->DisplaySize.x <= 0.0f || im_draw_data_->DisplaySize.y <= 0.0f);
    if (!is_minimized) {
        ImGui_ImplVulkan_RenderDrawData(im_draw_data_, a_vk_command_buffer);
    }
}


VkDescriptorPoolSize spock::imgui_t::
imgui_descriptor_pool_size()
{
    VkDescriptorPoolSize pool_size{};
    pool_size.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    pool_size.descriptorCount = 1;
    return pool_size;
}





