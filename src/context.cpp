#include <spock/context.hpp>
#include <spock/utils.hpp>
#include <unordered_set>
#include <execution>
#include <string_view>
#include <fstream>
#include <optional>
#include <new>
#include <memory>
#include <algorithm>
#include <cstring>
#include <cstddef>
#include <filesystem>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <nlohmann/json.hpp>

#if defined(__INTELLISENSE__)
VkResult volkInitialize() {}
void volkLoadInstance(VkInstance instance) {}
#endif


VkAccelerationStructureBuildRangeInfoKHR spock::
build_VkAccelerationStructureBuildRangeInfoKHR(
    uint32_t    primitiveCount,
    uint32_t    primitiveOffset,
    uint32_t    firstVertex,
    uint32_t    transformOffset
)
{
    VkAccelerationStructureBuildRangeInfoKHR info{};

    info.primitiveCount  = primitiveCount;
    info.primitiveOffset = primitiveOffset;
    info.firstVertex     = firstVertex;
    info.transformOffset = transformOffset;

    return info;
}


VkAccelerationStructureGeometryKHR spock::
build_VkAccelerationStructureGeometryKHR(
    VkGeometryFlagsKHR                                  flags,
    const VkAccelerationStructureGeometryAabbsDataKHR&  aabbs,
    const void*                                         pNext
)
{
    auto geom = make_VkAccelerationStructureGeometryKHR();

    geom.pNext          = pNext;
    geom.geometryType   = VK_GEOMETRY_TYPE_AABBS_KHR;
    geom.geometry.aabbs = aabbs;
    geom.flags          = flags;

    return geom;
}


VkAccelerationStructureGeometryKHR spock::
build_VkAccelerationStructureGeometryKHR(
    VkGeometryFlagsKHR                                      flags,
    const VkAccelerationStructureGeometryInstancesDataKHR&  instances,
    const void*                                             pNext
)
{
    auto geom = make_VkAccelerationStructureGeometryKHR();

    geom.pNext              = pNext;
    geom.geometryType       = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    geom.geometry.instances = instances;
    geom.flags              = flags;

    return geom;
}


VkAccelerationStructureGeometryKHR spock::
build_VkAccelerationStructureGeometryKHR(
    VkGeometryFlagsKHR                                      flags,
    const VkAccelerationStructureGeometryTrianglesDataKHR&  triangles,
    const void*                                             pNext
)
{
    auto geom = make_VkAccelerationStructureGeometryKHR();

    geom.pNext              = pNext;
    geom.geometryType       = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    geom.geometry.triangles = triangles;
    geom.flags              = flags;

    return geom;
}


VkAccelerationStructureGeometryAabbsDataKHR spock::
build_VkAccelerationStructureGeometryAabbsDataKHR(
    VkDeviceSize                     stride,
    VkDeviceOrHostAddressConstKHR    data,
    const void*                      pNext
)
{
    auto aabbs_data = make_VkAccelerationStructureGeometryAabbsDataKHR();

    aabbs_data.pNext = pNext;
    aabbs_data.data = data;
    aabbs_data.stride = stride;

    return aabbs_data;
}


VkAccelerationStructureGeometryInstancesDataKHR spock::
build_VkAccelerationStructureGeometryInstancesDataKHR(
    VkDeviceOrHostAddressConstKHR    data,
    VkBool32                         arrayOfPointers,
    const void*                      pNext
)
{
    auto instances_data = make_VkAccelerationStructureGeometryInstancesDataKHR();

    instances_data.pNext = pNext;
    instances_data.arrayOfPointers = arrayOfPointers;
    instances_data.data = data;

    return instances_data;
}


VkAccelerationStructureGeometryTrianglesDataKHR spock::
build_VkAccelerationStructureGeometryTrianglesDataKHR(
    VkFormat                         vertexFormat,
    VkDeviceOrHostAddressConstKHR    vertexData,
    VkDeviceSize                     vertexStride,
    uint32_t                         maxVertex,
    VkIndexType                      indexType,
    VkDeviceOrHostAddressConstKHR    indexData,
    VkDeviceOrHostAddressConstKHR    transformData,
    const void*                      pNext
)
{
    auto triangles_data = make_VkAccelerationStructureGeometryTrianglesDataKHR();

    triangles_data.pNext         = pNext;
    triangles_data.vertexFormat  = vertexFormat;
    triangles_data.vertexData    = vertexData;
    triangles_data.vertexStride  = vertexStride;
    triangles_data.maxVertex     = maxVertex;
    triangles_data.indexType     = indexType;
    triangles_data.indexData     = indexData;
    triangles_data.transformData = transformData;

    return triangles_data;
}


VkAccelerationStructureInstanceKHR spock::
build_VkAccelerationStructureInstanceKHR(
    const glm::mat4&              transform,
    uint32_t                      instanceShaderBindingTableRecordOffset,
    VkGeometryInstanceFlagsKHR    flags,
    uint64_t                      accelerationStructureReference,
    uint32_t                      instanceCustomIndex,
    uint32_t                      mask
)
{
    constexpr uint32_t INSTANCE_CUSTOM_INDEX_MAX                       = spock::bitset_max<uint32_t, 24>();
    constexpr uint32_t MASK_MAX                                        = spock::bitset_max<uint32_t, 8>();
    constexpr uint32_t INSTANCE_SHADER_BINDING_TABLE_RECORD_OFFSET_MAX = spock::bitset_max<uint32_t, 24>();
    constexpr uint32_t FLAGS_MAX                                       = spock::bitset_max<uint32_t, 8>();

    SPOCK_PARANOID(instanceCustomIndex <= INSTANCE_CUSTOM_INDEX_MAX, "instanceCustomIndex is too large");
    SPOCK_PARANOID(mask <= MASK_MAX, "mask is too large");
    SPOCK_PARANOID(instanceShaderBindingTableRecordOffset <= INSTANCE_SHADER_BINDING_TABLE_RECORD_OFFSET_MAX, "instanceShaderBindingTableRecordOffset is too large");
    SPOCK_PARANOID(flags <= FLAGS_MAX, "flags is too large");
    SPOCK_PARANOID(glm::row(transform, 3) == glm::vec4(0.f, 0.f, 0.f, 1.f), "transform is invalid");

    VkAccelerationStructureInstanceKHR instance{};

    *reinterpret_cast<glm::mat3x4*>(instance.transform.matrix) = glm::transpose(glm::mat4x3(transform));
    instance.instanceCustomIndex                               = instanceCustomIndex;
    instance.mask                                              = mask;
    instance.instanceShaderBindingTableRecordOffset            = instanceShaderBindingTableRecordOffset;
    instance.flags                                             = flags;
    instance.accelerationStructureReference                    = accelerationStructureReference;

    return instance;
}


VkAttachmentDescription spock::
build_VkAttachmentDescription(
    VkFormat                       format, 
    VkImageLayout                  finalLayout, 
    VkAttachmentLoadOp             loadOp, 
    VkAttachmentStoreOp            storeOp, 
    VkSampleCountFlagBits          samples, 
    VkImageLayout                  initialLayout, 
    VkAttachmentLoadOp             stencilLoadOp, 
    VkAttachmentStoreOp            stencilStoreOp, 
    VkAttachmentDescriptionFlags   flags
)
{
    VkAttachmentDescription desc{};

    desc.flags          = flags;
    desc.format         = format;
    desc.samples        = samples;
    desc.loadOp         = loadOp;
    desc.storeOp        = storeOp;
    desc.stencilLoadOp  = stencilLoadOp;
    desc.stencilStoreOp = stencilStoreOp;
    desc.initialLayout  = initialLayout;
    desc.finalLayout    = finalLayout;

    return desc;
}


VkBufferCopy spock::
build_VkBufferCopy(
    VkDeviceSize a_bytes, 
    VkDeviceSize a_src_byte_offset, 
    VkDeviceSize a_dst_byte_offset
)
{
    VkBufferCopy copy{};

    copy.srcOffset = a_src_byte_offset;
    copy.dstOffset = a_dst_byte_offset;
    copy.size      = a_bytes;

    return copy;
}


VkBufferCreateInfo spock::
build_VkBufferCreateInfo(
    VkDeviceSize           size,
    VkBufferUsageFlags     usage,
    VkBufferCreateFlags    flags,                 
    VkSharingMode          sharingMode,           
    uint32_t               queueFamilyIndexCount, 
    const uint32_t*        pQueueFamilyIndices,   
    const void*            pNext                 
)
{
    auto info = make_VkBufferCreateInfo();

    info.pNext                 = pNext;
    info.flags                 = flags;
    info.size                  = size;
    info.usage                 = usage;
    info.sharingMode           = sharingMode;
    info.queueFamilyIndexCount = queueFamilyIndexCount;
    info.pQueueFamilyIndices   = pQueueFamilyIndices;

    return info;
}


VkCommandBufferBeginInfo spock::
build_VkCommandBufferBeginInfo(
    VkCommandBufferUsageFlags                flags, 
    const VkCommandBufferInheritanceInfo*    pInheritanceInfo, 
    const void*                              pNext
)
{
    auto info = make_VkCommandBufferBeginInfo();

    info.pNext            = pNext;
    info.flags            = flags;
    info.pInheritanceInfo = pInheritanceInfo;

    return info;
}


VkComponentMapping spock::build_VkComponentMapping(
    VkComponentSwizzle r, 
    VkComponentSwizzle g, 
    VkComponentSwizzle b, 
    VkComponentSwizzle a
)
{
    VkComponentMapping mapping{};

    mapping.r = r;
    mapping.g = g;
    mapping.b = b;
    mapping.a = a;

    return mapping;
}


VkComputePipelineCreateInfo spock::
build_VkComputePipelineCreateInfo(
    const VkPipelineShaderStageCreateInfo&    stage,
    VkPipelineLayout                          layout,
    VkPipelineCreateFlags                     flags,
    VkPipeline                                basePipelineHandle,
    int32_t                                   basePipelineIndex,
    const void*                               pNext
)
{
    auto info = make_VkComputePipelineCreateInfo();

    info.pNext = pNext;
    info.flags = flags;
    info.stage = stage;
    info.layout = layout;
    info.basePipelineHandle = basePipelineHandle;
    info.basePipelineIndex = basePipelineIndex;

    return info;
}


VkDescriptorBufferInfo spock::
build_VkDescriptorBufferInfo(
    VkBuffer       a_buffer, 
    VkDeviceSize   a_offset, 
    VkDeviceSize   a_range
)
{
    VkDescriptorBufferInfo info{};
    info.buffer = a_buffer;
    info.offset = a_offset;
    info.range  = a_range;

    return info;
}


VkFramebufferCreateInfo spock::
build_VkFramebufferCreateInfo(
    VkRenderPass              renderPass, 
    uint32_t                  attachmentCount, 
    const VkImageView*        pAttachments, 
    const VkExtent2D&         extent, 
    uint32_t                  layers, 
    VkFramebufferCreateFlags  flags, 
    const void *              pNext
)
{
    auto info = make_VkFramebufferCreateInfo();

    info.pNext           = pNext;
    info.flags           = flags;
    info.renderPass      = renderPass;
    info.attachmentCount = attachmentCount;
    info.pAttachments    = pAttachments;
    info.width           = extent.width;
    info.height          = extent.height;
    info.layers          = layers;

    return info;
}


VkDescriptorImageInfo spock::
build_VkDescriptorImageInfo(
    VkSampler      sampler,
    VkImageView    imageView,
    VkImageLayout  imageLayout
)
{
    VkDescriptorImageInfo info{};

    info.sampler     = sampler;
    info.imageView   = imageView;
    info.imageLayout = imageLayout;

    return info;
}


VkDescriptorPoolCreateInfo spock::
build_VkDescriptorPoolCreateInfo(
    uint32_t                       maxSets,
    uint32_t                       poolSizeCount,
    const VkDescriptorPoolSize*    pPoolSizes,
    VkDescriptorPoolCreateFlags    flags,
    const void*                    pNext
)
{
    auto info = make_VkDescriptorPoolCreateInfo();

    info.pNext = pNext;
    info.flags = flags;
    info.maxSets = maxSets;
    info.poolSizeCount = poolSizeCount;
    info.pPoolSizes = pPoolSizes;

    return info;
}


VkDescriptorSetAllocateInfo spock::
build_VkDescriptorSetAllocateInfo(
    VkDescriptorPool                descriptorPool, 
    uint32_t                        descriptorSetCount,   
    const VkDescriptorSetLayout*    pSetLayouts, 
    const void *                    pNext
)
{
    auto info = make_VkDescriptorSetAllocateInfo();

    info.descriptorPool     = descriptorPool;
    info.descriptorSetCount = descriptorSetCount;
    info.pSetLayouts        = pSetLayouts;
    info.pNext              = pNext;

    return info;
}


VkDescriptorSetLayoutBinding spock::
build_VkDescriptorSetLayoutBinding(
    uint32_t             binding,
    VkDescriptorType     descriptorType,
    uint32_t             descriptorCount,
    VkShaderStageFlags   stageFlags,
    const VkSampler*     pImmutableSamplers
)
{
    VkDescriptorSetLayoutBinding layout_binding{};

    layout_binding.binding            = binding;
    layout_binding.descriptorType     = descriptorType;
    layout_binding.descriptorCount    = descriptorCount;
    layout_binding.stageFlags         = stageFlags;
    layout_binding.pImmutableSamplers = pImmutableSamplers;

    return layout_binding;
}


VkDescriptorSetLayoutCreateInfo spock::
build_VkDescriptorSetLayoutCreateInfo(
    uint32_t                              bindingCount,
    const VkDescriptorSetLayoutBinding*   pBindings,
    VkDescriptorSetLayoutCreateFlags      flags

)
{
    auto info = make_VkDescriptorSetLayoutCreateInfo();

    info.flags        = flags;
    info.bindingCount = bindingCount;
    info.pBindings    = pBindings;

    return info;
}


VkShaderModuleCreateInfo spock::
build_VkShaderModuleCreateInfo(
    size_t                       codeSize,
    const uint32_t*              pCode,
    VkShaderModuleCreateFlags    flags,
    const void*                  pNext
)
{
    auto info = make_VkShaderModuleCreateInfo();

    info.pNext = pNext;
    info.flags = flags;
    info.codeSize = codeSize;
    info.pCode = pCode;

    return info;
}


VkImageCreateInfo spock::
build_VkImageCreateInfo(
    VkFormat                 format, 
    const VkExtent3D&        extent, 
    VkImageUsageFlags        usage, 
    uint32_t                 mipLevels, 
    VkSampleCountFlagBits    samples,
    VkImageTiling            tiling,
    uint32_t                 arrayLayers,
    VkSharingMode            sharingMode,
    uint32_t                 queueFamilyIndexCount,
    uint32_t*                pQueueFamilyIndices,
    VkImageLayout            initialLayout,
    const void*              pNext
)
{
    auto create_info = make_VkImageCreateInfo();

    create_info.pNext                 = pNext;
    create_info.imageType             = extent.depth > 1 ? VK_IMAGE_TYPE_3D : (extent.height > 1 ? VK_IMAGE_TYPE_2D : VK_IMAGE_TYPE_1D);
    create_info.format                = format;
    create_info.extent                = extent;
    create_info.mipLevels             = mipLevels;
    create_info.arrayLayers           = arrayLayers;
    create_info.samples               = samples;
    create_info.tiling                = tiling;
    create_info.usage                 = usage;
    create_info.sharingMode           = sharingMode;
    create_info.queueFamilyIndexCount = queueFamilyIndexCount;
    create_info.pQueueFamilyIndices   = pQueueFamilyIndices;
    create_info.initialLayout         = initialLayout;

    return create_info;
}


VkImageSubresourceLayers spock::
build_VkImageSubresourceLayers(
    VkImageAspectFlags    aspectMask,
    uint32_t              mipLevel,
    uint32_t              layerCount,
    uint32_t              baseArrayLayer
)
{
    VkImageSubresourceLayers subres{};

    subres.aspectMask = aspectMask;
    subres.mipLevel = mipLevel;
    subres.baseArrayLayer = baseArrayLayer;
    subres.layerCount = layerCount;

    return subres;
}


VkImageSubresourceRange spock::
build_VkImageSubresourceRange(
    VkImageAspectFlags   aspectMask,
    uint32_t             levelCount,
    uint32_t             baseMipLevel,
    uint32_t             layerCount,
    uint32_t             baseArrayLayer
)
{
    VkImageSubresourceRange range{};

    range.aspectMask     = aspectMask;
    range.baseMipLevel   = baseMipLevel; 
    range.levelCount     = levelCount; 
    range.baseArrayLayer = baseArrayLayer; 
    range.layerCount     = layerCount; 

    return range;
}


VkImageMemoryBarrier spock::
build_VkImageMemoryBarrier(
    VkImage a_vk_image, 
    VkImageLayout a_old_layout,
    VkImageLayout a_new_layout,
    VkAccessFlags a_src_access_flags,
    VkAccessFlags a_dst_access_flags,
    const VkImageSubresourceRange& a_range,
    uint32_t a_src_queue_family_idx,
    uint32_t a_dst_queue_family_idx,
    const void* a_next
)
{
    auto barrier = make_VkImageMemoryBarrier();

    barrier.pNext               = a_next;
    barrier.srcAccessMask       = a_src_access_flags;
    barrier.dstAccessMask       = a_dst_access_flags;
    barrier.oldLayout           = a_old_layout;
    barrier.newLayout           = a_new_layout == VK_IMAGE_LAYOUT_MAX_ENUM ? a_old_layout : a_new_layout;
    barrier.srcQueueFamilyIndex = a_src_queue_family_idx;
    barrier.dstQueueFamilyIndex = a_dst_queue_family_idx;
    barrier.image               = a_vk_image;
    barrier.subresourceRange    = a_range;

    return barrier;
}


void spock::
advance_VkImageMemoryBarrier(
    VkImageMemoryBarrier * a_barrier, 
    VkImageLayout a_new_layout, 
    VkAccessFlags a_src_access_flags, 
    VkAccessFlags a_dst_access_flags, 
    const VkImageSubresourceRange & a_range, 
    uint32_t a_src_queue_family_idx, 
    uint32_t a_dst_queue_family_idx
)
{
    SPOCK_PARANOID(a_barrier->image != VK_NULL_HANDLE, "set_VkImageMemoryBarrier: a_barrier is not properly initialized");

    a_barrier->oldLayout = std::exchange(a_barrier->newLayout, a_new_layout);

    if (a_src_access_flags != (VkAccessFlags)VK_ACCESS_FLAG_BITS_MAX_ENUM) {
        a_barrier->srcAccessMask = a_src_access_flags;
    }
    if (a_dst_access_flags != (VkAccessFlags)VK_ACCESS_FLAG_BITS_MAX_ENUM) {
        a_barrier->dstAccessMask = a_dst_access_flags;
    }
    if (a_src_queue_family_idx != VK_QUEUE_FAMILY_IGNORED) {
        a_barrier->srcQueueFamilyIndex = a_src_queue_family_idx;
    }
    if (a_dst_queue_family_idx != VK_QUEUE_FAMILY_IGNORED) {
        a_barrier->dstQueueFamilyIndex = a_dst_queue_family_idx;
    }
    if (a_range.aspectMask != (VkImageAspectFlags)VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM) {
        a_barrier->subresourceRange = a_range;
    }

}


VkImageViewCreateInfo spock::
build_VkImageViewCreateInfo(
    VkImage                          image, 
    VkFormat                         format, 
    const VkImageSubresourceRange&   subresourceRange,
    VkImageViewType                  viewType,
    const VkComponentMapping&        components,
    const void*                      pNext
)
{
    auto create_info = make_VkImageViewCreateInfo();

    create_info.pNext            = pNext;
    create_info.image            = image;
    create_info.viewType         = viewType;
    create_info.format           = format;
    create_info.components       = components;
    create_info.subresourceRange = subresourceRange;

    return create_info;
}


VkImageViewCreateInfo spock::
build_VkImageViewCreateInfo(
    const image_t&                   a_image, 
    const VkImageSubresourceRange&   subresourceRange,
    VkImageViewType                  viewType,
    const VkComponentMapping&        components,
    const void*                      pNext
)
{
    return build_VkImageViewCreateInfo(a_image, a_image.format(), subresourceRange, viewType, components, pNext);
}


VkMemoryBarrier spock::
build_VkMemoryBarrier(
    VkAccessFlags a_src_access_flags, 
    VkAccessFlags a_dst_access_flags,
    const void* a_next

)
{
    auto barrier = make_VkMemoryBarrier();

    barrier.pNext         = a_next;
    barrier.srcAccessMask = a_src_access_flags;
    barrier.dstAccessMask = a_dst_access_flags;

    return barrier;
}


VkPipelineColorBlendAttachmentState spock::
build_VkPipelineColorBlendAttachmentState(
    VkColorComponentFlags    colorWriteMask,
    VkBool32                 blendEnable,
    VkBlendFactor            srcColorBlendFactor,
    VkBlendFactor            dstColorBlendFactor,
    VkBlendOp                colorBlendOp,
    VkBlendFactor            srcAlphaBlendFactor,
    VkBlendFactor            dstAlphaBlendFactor,
    VkBlendOp                alphaBlendOp
)
{
    VkPipelineColorBlendAttachmentState state{};

    state.blendEnable = blendEnable;
    state.srcColorBlendFactor = srcColorBlendFactor;
    state.dstColorBlendFactor = dstColorBlendFactor;
    state.colorBlendOp = colorBlendOp;
    state.srcAlphaBlendFactor = srcAlphaBlendFactor;
    state.dstAlphaBlendFactor = dstAlphaBlendFactor;
    state.alphaBlendOp = alphaBlendOp;
    state.colorWriteMask = colorWriteMask;

    return state;
}


VkPipelineLayoutCreateInfo spock::
build_VkPipelineLayoutCreateInfo(
    uint32_t a_set_layout_count,
    const VkDescriptorSetLayout* a_set_layouts,
    uint32_t a_push_constant_range_count,
    const VkPushConstantRange* a_push_constant_ranges,
    VkPipelineLayoutCreateFlags a_flags,
    const void* a_next
)
{
    auto info = make_VkPipelineLayoutCreateInfo();

    info.pNext                  = a_next;
    info.flags                  = a_flags;
    info.setLayoutCount         = a_set_layout_count;
    info.pSetLayouts            = a_set_layouts;
    info.pushConstantRangeCount = a_push_constant_range_count;
    info.pPushConstantRanges    = a_push_constant_ranges;

    return info;
}


VkPipelineShaderStageCreateInfo spock::
build_VkPipelineShaderStageCreateInfo(
    VkShaderStageFlagBits               stage,
    VkShaderModule                      module,
    const VkSpecializationInfo*         pSpecializationInfo,
    VkPipelineShaderStageCreateFlags    flags,
    const char*                         pName,
    const void*                         pNext
)
{
    auto info = make_VkPipelineShaderStageCreateInfo();

    info.pNext               = pNext;
    info.flags               = flags;
    info.stage               = stage;
    info.module              = module;
    info.pName               = pName;
    info.pSpecializationInfo = pSpecializationInfo;

    return info;

}


VkPushConstantRange spock::
build_VkPushConstantRange(
    VkShaderStageFlags    stageFlags,
    uint32_t              size,
    uint32_t              offset
)
{
    VkPushConstantRange range{};

    range.stageFlags = stageFlags;
    range.offset = offset;
    range.size = size;

    return range;
}


VkRayTracingPipelineCreateInfoKHR spock::
build_VkRayTracingPipelineCreateInfoKHR(
    VkPipelineLayout                                     layout,
    uint32_t                                             stageCount,
    const VkPipelineShaderStageCreateInfo*               pStages,
    uint32_t                                             groupCount,
    const VkRayTracingShaderGroupCreateInfoKHR*          pGroups,
    uint32_t                                             maxPipelineRayRecursionDepth,
    VkPipelineCreateFlags                                flags,
    const VkPipelineDynamicStateCreateInfo*              pDynamicState,
    const VkPipelineLibraryCreateInfoKHR*                pLibraryInfo,
    const VkRayTracingPipelineInterfaceCreateInfoKHR*    pLibraryInterface,
    VkPipeline                                           basePipelineHandle,
    int32_t                                              basePipelineIndex,
    const void*                                          pNext
)
{
    auto info = make_VkRayTracingPipelineCreateInfoKHR();

    info.pNext                        = pNext;
    info.flags                        = flags;
    info.stageCount                   = stageCount;
    info.pStages                      = pStages;
    info.groupCount                   = groupCount;
    info.pGroups                      = pGroups;
    info.maxPipelineRayRecursionDepth = maxPipelineRayRecursionDepth;
    info.pLibraryInfo                 = pLibraryInfo;
    info.pLibraryInterface            = pLibraryInterface;
    info.pDynamicState                = pDynamicState;
    info.layout                       = layout;
    info.basePipelineHandle           = basePipelineHandle;
    info.basePipelineIndex            = basePipelineIndex;

    return info;
}


VkRayTracingShaderGroupCreateInfoKHR spock::
build_VkRayTracingShaderGroupCreateInfoKHR(
    VkRayTracingShaderGroupTypeKHR    type,
    uint32_t                          generalShader,
    uint32_t                          closestHitShader,
    uint32_t                          anyHitShader,
    uint32_t                          intersectionShader,
    const void*                       pShaderGroupCaptureReplayHandle,
    const void*                       pNext
)
{
    auto info = make_VkRayTracingShaderGroupCreateInfoKHR();

    info.pNext                           = pNext;
    info.type                            = type;
    info.generalShader                   = generalShader;
    info.closestHitShader                = closestHitShader;
    info.anyHitShader                    = anyHitShader;
    info.intersectionShader              = intersectionShader;
    info.pShaderGroupCaptureReplayHandle = pShaderGroupCaptureReplayHandle;

    return info;
}


VkRayTracingShaderGroupCreateInfoKHR spock::
build_VkRayTracingShaderGroupCreateInfoKHR_hit(
    uint32_t                          closestHitShader,
    uint32_t                          anyHitShader,
    uint32_t                          intersectionShader,
    bool                              a_is_triangle_hit, /* VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR if false */
    const void*                       pShaderGroupCaptureReplayHandle,
    const void*                       pNext
)
{
    return build_VkRayTracingShaderGroupCreateInfoKHR(
        a_is_triangle_hit ? VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR : VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR,
        VK_SHADER_UNUSED_KHR,
        closestHitShader,
        anyHitShader,
        intersectionShader,
        pShaderGroupCaptureReplayHandle,
        pNext
    );
}


VkRayTracingShaderGroupCreateInfoKHR spock::
build_VkRayTracingShaderGroupCreateInfoKHR_general(
    uint32_t                          generalShader,
    const void*                       pShaderGroupCaptureReplayHandle,
    const void*                       pNext
)
{
    return build_VkRayTracingShaderGroupCreateInfoKHR(
        VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR, 
        generalShader,
        VK_SHADER_UNUSED_KHR,
        VK_SHADER_UNUSED_KHR,
        VK_SHADER_UNUSED_KHR,
        pShaderGroupCaptureReplayHandle, 
        pNext
    );
}


VkRenderPassCreateInfo spock::
build_VkRenderPassCreateInfo(
    uint32_t                          attachmentCount,
    const VkAttachmentDescription*    pAttachments,
    uint32_t                          subpassCount,
    const VkSubpassDescription*       pSubpasses,
    uint32_t                          dependencyCount,
    const VkSubpassDependency*        pDependencies,
    VkRenderPassCreateFlags           flags,
    const void*                       pNext
)
{
    auto info = make_VkRenderPassCreateInfo();

    info.pNext = pNext;
    info.flags = flags;
    info.attachmentCount = attachmentCount;
    info.pAttachments = pAttachments;
    info.subpassCount = subpassCount;
    info.pSubpasses = pSubpasses;
    info.dependencyCount = dependencyCount;
    info.pDependencies = pDependencies;

    return info;
}


VkSamplerCreateInfo spock::
build_VkSamplerCreateInfo(
    float a_max_anisotropy, 
    VkFilter a_mag_filter, 
    VkSamplerAddressMode a_addr_mode_u, 
    VkSamplerMipmapMode a_mipmap_mode, 
    VkFilter a_min_filter, 
    VkSamplerAddressMode a_addr_mode_v, 
    VkSamplerAddressMode a_addr_mode_w, 
    float a_min_lod, 
    float a_max_lod, 
    float a_mip_lod_bias, 
    VkBool32 a_anisotropy_enabled, 
    VkBool32 a_unnormalized_coordinates, 
    VkBool32 a_compare_enabled, 
    VkCompareOp a_compare_op, 
    VkBorderColor a_border_color,
    const void* a_next
)
{
    auto sampler_create_info = make_VkSamplerCreateInfo();

    sampler_create_info.pNext                   = a_next;
    sampler_create_info.magFilter               = a_mag_filter;
    sampler_create_info.minFilter               = a_min_filter == VK_FILTER_MAX_ENUM ? a_mag_filter : a_min_filter;
    sampler_create_info.addressModeU            = a_addr_mode_u;
    sampler_create_info.addressModeV            = a_addr_mode_v == VK_SAMPLER_ADDRESS_MODE_MAX_ENUM ? a_addr_mode_u : a_addr_mode_v;
    sampler_create_info.addressModeW            = a_addr_mode_w == VK_SAMPLER_ADDRESS_MODE_MAX_ENUM ? a_addr_mode_u : a_addr_mode_w;
    sampler_create_info.anisotropyEnable        = a_anisotropy_enabled;
    sampler_create_info.maxAnisotropy           = a_max_anisotropy;
    sampler_create_info.borderColor             = a_border_color;
    sampler_create_info.unnormalizedCoordinates = a_unnormalized_coordinates;
    sampler_create_info.compareEnable           = a_compare_enabled;
    sampler_create_info.compareOp               = a_compare_op;
    sampler_create_info.mipmapMode              = a_mipmap_mode;
    sampler_create_info.minLod                  = a_min_lod;
    sampler_create_info.maxLod                  = a_max_lod;
    sampler_create_info.mipLodBias              = a_mip_lod_bias;

    return sampler_create_info;
}


VkSamplerCreateInfo spock::
dummy_VkSamplerCreateInfo()
{
    auto create_info = make_VkSamplerCreateInfo();

    create_info.magFilter               = VK_FILTER_NEAREST;
    create_info.minFilter               = VK_FILTER_NEAREST;
    create_info.mipmapMode              = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    create_info.addressModeU            = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    create_info.addressModeV            = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    create_info.addressModeW            = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    create_info.mipLodBias              = 0.f;
    create_info.anisotropyEnable        = VK_FALSE;
    create_info.maxAnisotropy           = 0.f;
    create_info.compareEnable           = VK_FALSE;
    create_info.compareOp               = VK_COMPARE_OP_ALWAYS;
    create_info.minLod                  = 0.f;
    create_info.maxLod                  = 0.f;
    create_info.borderColor             = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    create_info.unnormalizedCoordinates = VK_TRUE;

    return create_info;
}


VkStridedDeviceAddressRegionKHR spock::
build_VkStridedDeviceAddressRegionKHR(
    VkDeviceAddress    deviceAddress,
    VkDeviceSize       stride,
    VkDeviceSize       size
)
{
    VkStridedDeviceAddressRegionKHR region{};

    region.deviceAddress = deviceAddress;
    region.stride        = stride;
    region.size          = size;

    return region;
}




VkSubmitInfo spock::
build_VkSubmitInfo(
    uint32_t commandBufferCount, 
    const VkCommandBuffer * pCommandBuffers, 
    uint32_t waitSemaphoreCount, 
    const VkSemaphore * pWaitSemaphores, 
    const VkPipelineStageFlags * pWaitDstStageMask, 
    uint32_t signalSemaphoreCount, 
    const VkSemaphore * pSignalSemaphores, 
    const void * pNext
)
{
    auto info = make_VkSubmitInfo();

    info.pNext                = pNext;
    info.waitSemaphoreCount   = waitSemaphoreCount;
    info.pWaitSemaphores      = pWaitSemaphores;
    info.pWaitDstStageMask    = pWaitDstStageMask;
    info.commandBufferCount   = commandBufferCount;
    info.pCommandBuffers      = pCommandBuffers;
    info.signalSemaphoreCount = signalSemaphoreCount;
    info.pSignalSemaphores    = pSignalSemaphores;
    
    return info;
}

VkSubpassDependency spock::
build_VkSubpassDependency(
    uint32_t                srcSubpass,
    uint32_t                dstSubpass,
    VkPipelineStageFlags    srcStageMask,
    VkPipelineStageFlags    dstStageMask,
    VkAccessFlags           srcAccessMask,
    VkAccessFlags           dstAccessMask,
    VkDependencyFlags       dependencyFlags
)
{
    VkSubpassDependency dep{};

    dep.srcSubpass      = srcSubpass;
    dep.dstSubpass      = dstSubpass;
    dep.srcStageMask    = srcStageMask;
    dep.dstStageMask    = dstStageMask;
    dep.srcAccessMask   = srcAccessMask;
    dep.dstAccessMask   = dstAccessMask;
    dep.dependencyFlags = dependencyFlags;

    return dep;
}


VkSubpassDescription spock::
build_VkSubpassDescription(
    uint32_t colorAttachmentCount, 
    const VkAttachmentReference * pColorAttachments, 
    const VkAttachmentReference * pDepthStencilAttachment, 
    const VkAttachmentReference * pResolveAttachments, 
    uint32_t inputAttachmentCount, 
    const VkAttachmentReference * pInputAttachments, 
    uint32_t preserveAttachmentCount, 
    const uint32_t * pPreserveAttachments, 
    VkSubpassDescriptionFlags flags, 
    VkPipelineBindPoint pipelineBindPoint
)
{
    VkSubpassDescription desc{};

    desc.flags                   = flags;
    desc.pipelineBindPoint       = pipelineBindPoint;
    desc.inputAttachmentCount    = inputAttachmentCount;
    desc.pInputAttachments       = pInputAttachments;
    desc.colorAttachmentCount    = colorAttachmentCount;
    desc.pColorAttachments       = pColorAttachments;
    desc.pResolveAttachments     = pResolveAttachments;
    desc.pDepthStencilAttachment = pDepthStencilAttachment;
    desc.preserveAttachmentCount = preserveAttachmentCount;
    desc.pPreserveAttachments    = pPreserveAttachments;

    return desc;
}


VkViewport spock::
build_VkViewport(
    float    width,
    float    height,
    float    x,
    float    y,
    float    minDepth,
    float    maxDepth
)
{
    VkViewport viewport;

    viewport.x = x;
    viewport.y = y;
    viewport.width = width;
    viewport.height = height;
    viewport.minDepth = minDepth;
    viewport.maxDepth = maxDepth;

    return viewport;
}


VkWriteDescriptorSet spock::
build_VkWriteDescriptorSet
(
    VkDescriptorSet a_dst_set, 
    uint32_t a_dst_binding, 
    uint32_t a_descriptor_count, 
    VkDescriptorType a_descriptor_type, 
    const VkDescriptorImageInfo * a_image_info, 
    const VkDescriptorBufferInfo * a_buffer_info, 
    const VkBufferView * a_texel_buffer_view, 
    uint32_t a_dst_array_element,
    const void* a_next
)
{
    auto write = make_VkWriteDescriptorSet();

    write.pNext = a_next;
    write.dstSet = a_dst_set;
    write.dstBinding = a_dst_binding;
    write.dstArrayElement = a_dst_array_element;
    write.descriptorCount = a_descriptor_count;
    write.descriptorType = a_descriptor_type;
    write.pImageInfo = a_image_info;
    write.pBufferInfo = a_buffer_info;
    write.pTexelBufferView = a_texel_buffer_view;

    return write;
}


VkWriteDescriptorSetAccelerationStructureKHR spock::
build_VkWriteDescriptorSetAccelerationStructureKHR(
    uint32_t                             accelerationStructureCount,
    const VkAccelerationStructureKHR*    pAccelerationStructures,
    const void*                          pNext
)
{
    auto write = make_VkWriteDescriptorSetAccelerationStructureKHR();

    write.pNext                      = pNext;
    write.accelerationStructureCount = accelerationStructureCount;
    write.pAccelerationStructures    = pAccelerationStructures;

    return write;
}


VmaAllocationCreateInfo spock::
build_VmaAllocationCreateInfo(
    VkMemoryPropertyFlags a_required_flags, 
    VkMemoryPropertyFlags a_preferred_flags, 
    VmaAllocationCreateFlags a_allocation_create_flags, 
    VmaMemoryUsage a_vma_memory_usage,
    VmaPool a_vma_pool,
    void* a_user_data,
    float a_priority,
    uint32_t a_vma_memory_type_bits
)
{
    VmaAllocationCreateInfo create_info{};

    create_info.flags          = a_allocation_create_flags;
    create_info.usage          = a_vma_memory_usage;
    create_info.requiredFlags  = a_required_flags;
    create_info.preferredFlags = a_preferred_flags;
    create_info.memoryTypeBits = a_vma_memory_type_bits;
    create_info.pool           = a_vma_pool;
    create_info.pUserData      = a_user_data;
    create_info.priority       = a_priority;

    return create_info;
}


spock::buffer_t spock::
stage(VkDevice a_vk_dev, VmaAllocator a_vma_allocator, const void* a_src, size_t a_bytes)
{
    auto staging_buffer_create_info = make_VkBufferCreateInfo();
    staging_buffer_create_info.size = static_cast<VkDeviceSize>(a_bytes);
    staging_buffer_create_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    VmaAllocationCreateInfo staging_buffer_allocation_create_info{};
    staging_buffer_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    buffer_t staging_buffer(a_vk_dev, a_vma_allocator, staging_buffer_create_info, staging_buffer_allocation_create_info);

    staging_buffer.map();
    memcpy(staging_buffer.mapped_data<>(), a_src, a_bytes);

    return staging_buffer;
}


spock::sbt_entry_size_t spock::
calc_sbt_entry_size(const VkPhysicalDeviceRayTracingPipelinePropertiesKHR& a_rt_ppl_properties, uint32_t a_shader_record_buffer_bytes)
{
    const uint32_t sbt_entry_bytes = a_rt_ppl_properties.shaderGroupHandleSize + a_shader_record_buffer_bytes;
    const uint32_t sbt_base_alignment = a_rt_ppl_properties.shaderGroupBaseAlignment;
    const uint32_t sbt_handle_alignment = a_rt_ppl_properties.shaderGroupHandleAlignment;
    spock::ensure(sbt_handle_alignment != 0, "shaderGroupHandleAlignment cannot be zero");
    spock::ensure(sbt_base_alignment % sbt_handle_alignment == 0, "shaderGroupBaseAlignment is not multiple of shaderGroupHandleAlignment");
    const auto stride = sbt_base_alignment * ((sbt_entry_bytes + sbt_base_alignment - 1) / sbt_base_alignment);
    spock::ensure(stride < a_rt_ppl_properties.maxShaderGroupStride, "cannot have sbt stride larger than VkPhysicalDeviceRayTracingPipelinePropertiesKHR::maxShaderGroupStride");

    return sbt_entry_size_t{ a_rt_ppl_properties.shaderGroupHandleSize, stride };
}


spock::instance_t::
instance_t(
    const ordered_json& a_vk_config_json,
    const std::vector<const char*>& a_window_extensions,
    PFN_vkDebugUtilsMessengerCallbackEXT a_messenger_callback,
    void* a_messenger_user_data
)
    : vk_instance_(VK_NULL_HANDLE)
    , states_{}

{
    SPOCK_VK_CALL(volkInitialize());

    const auto& vk_app_config_json = a_vk_config_json["application"];
    VkApplicationInfo app_info = {};
    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pApplicationName = vk_app_config_json["name"].get_ptr<const ordered_json::string_t*>()->c_str();

    app_info.pEngineName =
        (vk_app_config_json["engine"].contains("name") && !vk_app_config_json["engine"]["name"].is_null()) ?
        vk_app_config_json["engine"]["name"].get_ptr<const ordered_json::string_t*>()->data() :
        nullptr;
    const auto& vk_ver = vk_app_config_json["api"]["version"];

    const uint32_t major = vk_ver["major"].get<int>();
    const uint32_t minor = vk_ver["minor"].get<int>();
    vk_api_version_ = VK_MAKE_API_VERSION(0, major, minor, 0);

    app_info.apiVersion = vk_api_version_;

    uint32_t available_extension_count = 0;
    std::vector<VkExtensionProperties> extension_properties;
    std::unordered_set<std::string_view> available_extension_names; // string_view of each VkExtensionProperties::extensionName
    vkEnumerateInstanceExtensionProperties(nullptr, &available_extension_count, nullptr);
    if (available_extension_count > 0) {
        extension_properties.resize(available_extension_count);
        SPOCK_VK_CALL(vkEnumerateInstanceExtensionProperties(nullptr, &available_extension_count, extension_properties.data()));

        available_extension_names.max_load_factor(0.7f); // tc++pl 4th: p.919
        available_extension_names.reserve(available_extension_count);
        for (const auto& prop: extension_properties) {
            available_extension_names.insert(prop.extensionName);
        }
    }
    uint32_t available_layer_count = 0;
    std::vector<VkLayerProperties> layer_properties;
    std::unordered_set<std::string_view> available_layer_names;
    vkEnumerateInstanceLayerProperties(&available_layer_count, nullptr);
    if (available_layer_count > 0) {
        layer_properties.resize(available_layer_count);
        SPOCK_VK_CALL(vkEnumerateInstanceLayerProperties(&available_layer_count, layer_properties.data()));

        available_layer_names.max_load_factor(0.7f);
        available_layer_names.reserve(available_extension_count);
        for (const auto& layer : layer_properties) {
            available_layer_names.insert(layer.layerName);
        }
    }

    const auto& instance_config_json = a_vk_config_json["instance"];
    std::vector<const char*> enabled_extensions;
    std::vector<const char*> enabled_layers;
    debug_utils_messenger_create_info_ = make_VkDebugUtilsMessengerCreateInfoEXT();
    auto vk_validation_features = make_VkValidationFeaturesEXT();
    std::vector<VkValidationFeatureEnableEXT> enabled_validation_features;
    std::vector<VkValidationFeatureDisableEXT> disabled_validation_features;

    if (instance_config_json.contains("debugging")) {
        const auto& debugging_config_json = instance_config_json["debugging"];
        const bool debugging_off = debugging_config_json["disable"];
        if (!debugging_off) {
            // layers
            uint32_t enabled_layer_count = 0;
            if (debugging_config_json.contains("layers")) {
                for (const auto& requested_layer : debugging_config_json["layers"].items()) {
                    const std::string_view layer_name{ requested_layer.key().c_str() };
                    const bool layer_required = requested_layer.value().get<bool>();
                    const bool layer_available = available_layer_names.find(layer_name) != available_layer_names.end();
                    SPOCK_ENSURE(layer_available || !layer_required,  "error: instance layer not available: " + std::string(layer_name));
                    if (layer_available) {
                        enabled_layers.push_back(layer_name.data());
                        states_[STATE_VALIDATION_LAYER_ON] = layer_name.find("validation") != std::string_view::npos;
                    }
                    else {
                        warn("instance layer not available: " + std::string(layer_name));
                    }
                }
                states_[STATE_INSTANCE_LAYER_ON] = !enabled_layers.empty();
            }
            // debug_utils_messenger
            if (debugging_config_json.contains("debug_utils_messenger")) {
                const auto& messenger_cfg_json = debugging_config_json["debug_utils_messenger"];
                const bool messenger_available = available_extension_names.find(VK_EXT_DEBUG_UTILS_EXTENSION_NAME) != available_extension_names.end();
                const bool messenger_required = messenger_cfg_json["required"];
                const bool messenger_on = messenger_cfg_json["on"];
                SPOCK_ENSURE(messenger_available || !messenger_required, "error: instance extension not available: " + std::string(VK_EXT_DEBUG_UTILS_EXTENSION_NAME));
                if (messenger_on && messenger_available) {
                    bool flags_ok = true;
                    for (const auto& flag : messenger_cfg_json["severity_flags"]) {
                        debug_utils_messenger_create_info_.messageSeverity |= str_to_VkDebugUtilsMessageSeverityFlagBitsEXT(flag.get<std::string_view>(), &flags_ok);
                        SPOCK_ENSURE(flags_ok, "invalid VkDebugUtilsMessageSeverityFlagBitsEXT: " + flag.get<std::string>());
                    }
                    for (const auto& flag : messenger_cfg_json["type_flags"]) {
                        debug_utils_messenger_create_info_.messageType |= str_to_VkDebugUtilsMessageTypeFlagBitsEXT(flag.get<std::string_view>(), &flags_ok);
                        SPOCK_ENSURE(flags_ok, "invalid VkDebugUtilsMessageTypeFlagBitsEXT: " + flag.get<std::string>());
                    }
                    debug_utils_messenger_create_info_.pfnUserCallback = a_messenger_callback ? a_messenger_callback : debug_utils_messenger_callback;
                    debug_utils_messenger_create_info_.pUserData = a_messenger_user_data;
                    states_[STATE_DEBUG_UTILS_MESSENGER_ON] = true;
                    enabled_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
                }
            }
            // validation_features
            if (debugging_config_json.contains("validation_features")) {
                const auto& validation_features_cfg_json = debugging_config_json["validation_features"];
                const bool validation_features_on = validation_features_cfg_json["on"];
                if (validation_features_on) {
                    bool feature_ok = true;
                    for (const auto& enabled_feature : validation_features_cfg_json["enabled"]) {
                        enabled_validation_features.push_back(str_to_VkValidationFeatureEnableEXT(enabled_feature.get<std::string_view>(), &feature_ok));
                        SPOCK_ENSURE(feature_ok, "invalid VkValidationFeatureEnableEXT: " + enabled_feature.get<std::string>());
                        if (enabled_validation_features.back() == VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT) {
                            states_[STATE_DEBUG_PRINTF_ON] = true;
                        }
                    }
                    for (const auto& disabled_feature : validation_features_cfg_json["disabled"]) {
                        disabled_validation_features.push_back(str_to_VkValidationFeatureDisableEXT(disabled_feature.get<std::string_view>(), &feature_ok));
                        SPOCK_ENSURE(feature_ok, "invalid VkValidationFeatureDisableEXT: " + disabled_feature.get<std::string>());
                    }
                    states_[STATE_VALIDATION_FEATURE_ON] = true;
                    vk_validation_features.enabledValidationFeatureCount = static_cast<uint32_t>(enabled_validation_features.size());
                    vk_validation_features.pEnabledValidationFeatures = enabled_validation_features.empty() ? nullptr : enabled_validation_features.data();
                    vk_validation_features.disabledValidationFeatureCount = static_cast<uint32_t>(disabled_validation_features.size());
                    vk_validation_features.pDisabledValidationFeatures = disabled_validation_features.empty() ? nullptr : disabled_validation_features.data();
                }
            }
        } // !debugging_off
    } // config contains debugging

    for (const auto& window_extension : a_window_extensions) {
        SPOCK_ENSURE(
            available_extension_names.find(window_extension) != available_extension_names.end(), 
            "error: instance extension is not available: " + std::string(window_extension)
        );
        enabled_extensions.push_back(window_extension);
    }

    if (instance_config_json.contains("extensions")) {
        for (const auto& ext_json : instance_config_json["extensions"].items()) {
            const bool ext_required = ext_json.value();
            const std::string_view ext_name{ ext_json.key() };
            const bool ext_available = available_extension_names.find(ext_name) != available_extension_names.end(); 
            SPOCK_ENSURE(ext_available || !ext_required, "error: instance extension is not available: " + std::string(ext_name));
            if (ext_available) {
                enabled_extensions.push_back(ext_name.data());
            }
            else {
                warn("instance extension is not available: " + std::string(ext_name));
            }
        }
    }

    VkInstanceCreateInfo instance_create_info = make_VkInstanceCreateInfo();
    instance_create_info.pNext = NULL;
    instance_create_info.pApplicationInfo = &app_info;
    const void** pp_next = &instance_create_info.pNext;
    if (states_[STATE_VALIDATION_FEATURE_ON]) {
        *pp_next = &vk_validation_features;
        pp_next = &vk_validation_features.pNext;
    }
    if (states_[STATE_DEBUG_UTILS_MESSENGER_ON]) { // debug_utils_messenger_create_info_.pNext must be NULL
        *pp_next = &debug_utils_messenger_create_info_;
        pp_next = &debug_utils_messenger_create_info_.pNext;
    }
    if (!enabled_layers.empty()) {
        instance_create_info.enabledLayerCount = static_cast<uint32_t>(enabled_layers.size());
        instance_create_info.ppEnabledLayerNames = enabled_layers.data();
    }
    if (!enabled_extensions.empty()) {
        instance_create_info.enabledExtensionCount = static_cast<uint32_t>(enabled_extensions.size());
        instance_create_info.ppEnabledExtensionNames = enabled_extensions.data();
    }
    SPOCK_VK_CALL(vkCreateInstance(&instance_create_info, nullptr, &vk_instance_));

    // TODO: use `void volkLoadDevice(VkDevice device);` to avoid vulkan loader dispatch code.
    volkLoadInstance(vk_instance_);
    info("volk loaded instance");

}


spock::instance_t::
instance_t(instance_t&& a_other) noexcept
    : vk_instance_{ std::exchange(a_other.vk_instance_, VK_NULL_HANDLE) }
    , debug_utils_messenger_create_info_{ std::exchange(a_other.debug_utils_messenger_create_info_, VkDebugUtilsMessengerCreateInfoEXT{}) }
    , vk_api_version_{ a_other.vk_api_version_ }
    , states_{ a_other.states_ }
{
}


spock::instance_t & spock::instance_t::
operator=(instance_t && a_other) noexcept
{
    if (this != &a_other) {
        vk_instance_                       = std::exchange(a_other.vk_instance_, VK_NULL_HANDLE);
        debug_utils_messenger_create_info_ = std::exchange(a_other.debug_utils_messenger_create_info_, VkDebugUtilsMessengerCreateInfoEXT{});
        vk_api_version_                    = a_other.vk_api_version_;
        states_                            = a_other.states_;
    }
    return *this;
}


spock::instance_t::
~instance_t()
{
    if (vk_instance_ != VK_NULL_HANDLE) {
        verbose("destroy instance...");
        vkDestroyInstance(vk_instance_, nullptr);
    }
}


bool spock::instance_t::
is_debugging_off() const
{ 
    return !states_[STATE_INSTANCE_LAYER_ON]
        && !states_[STATE_DEBUG_UTILS_MESSENGER_ON]
        && !states_[STATE_VALIDATION_FEATURE_ON];
}


spock::debug_utils_messenger_t::
debug_utils_messenger_t(debug_utils_messenger_t&& a_other) noexcept
    : vk_instance_{ std::exchange(a_other.vk_instance_, VK_NULL_HANDLE) }
    , vk_debug_utils_messenger_{ std::exchange(a_other.vk_debug_utils_messenger_, VK_NULL_HANDLE) }
{
}


spock::debug_utils_messenger_t& spock::debug_utils_messenger_t::
operator=(debug_utils_messenger_t&& a_other) noexcept
{
    if (this != &a_other) {
        vk_instance_ = std::exchange(a_other.vk_instance_, VK_NULL_HANDLE);
        vk_debug_utils_messenger_ = std::exchange(a_other.vk_debug_utils_messenger_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::debug_utils_messenger_t::
debug_utils_messenger_t(VkInstance a_vk_instance, const VkDebugUtilsMessengerCreateInfoEXT& a_create_info)
    : vk_instance_{ a_vk_instance }
{
    SPOCK_VK_CALL(vkCreateDebugUtilsMessengerEXT(vk_instance_, &a_create_info, nullptr, &vk_debug_utils_messenger_));
}


spock::debug_utils_messenger_t::
~debug_utils_messenger_t()
{
    if (VK_NULL_HANDLE != vk_debug_utils_messenger_) {
        verbose("destroy debug_utils_messenger_t...");
        vkDestroyDebugUtilsMessengerEXT(vk_instance_, vk_debug_utils_messenger_, nullptr);
    }
}


spock::surface_t::
surface_t(surface_t&& a_other) noexcept
    : vk_instance_{ std::exchange(a_other.vk_instance_, VK_NULL_HANDLE) }
    , vk_surface_{ std::exchange(a_other.vk_surface_, VK_NULL_HANDLE) }
{

}

spock::surface_t& spock::surface_t::
operator=(surface_t && a_other) noexcept
{
    if (this != &a_other) {
        vk_instance_ = std::exchange(a_other.vk_instance_, VK_NULL_HANDLE);
        vk_surface_  = std::exchange(a_other.vk_surface_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::surface_t::
~surface_t()
{
    if (vk_surface_ != VK_NULL_HANDLE) {
        verbose("destroy surface...");
        vkDestroySurfaceKHR(vk_instance_, vk_surface_, nullptr);
    }
}


spock::physical_device_t::
physical_device_t(physical_device_t && a_other) noexcept
    : vk_physical_dev_{ std::exchange(a_other.vk_physical_dev_, VK_NULL_HANDLE) }
    , properties_{ std::move(a_other.properties_) }
    , memory_properties_{ std::move(a_other.memory_properties_) }
    , available_extensions_{ std::move(available_extensions_) }
{}


spock::physical_device_t& spock::physical_device_t::
operator=(physical_device_t && a_other) noexcept
{
    if (this != &a_other) {
        vk_physical_dev_      = std::exchange(a_other.vk_physical_dev_, VK_NULL_HANDLE);
        properties_           = std::move(a_other.properties_);
        memory_properties_    = std::move(a_other.memory_properties_);
        available_extensions_ = std::move(a_other.available_extensions_);
    }
    return *this;
}


spock::physical_device_t::
physical_device_t
(
    const std::vector<VkPhysicalDevice> & a_physical_devices, 
    const ordered_json& a_vk_physical_dev_config_json
)
    : properties_{ new VkPhysicalDeviceProperties }
    , memory_properties_{ new VkPhysicalDeviceMemoryProperties }
{
    const uint32_t num_gpus = static_cast<uint32_t>(a_physical_devices.size());
    std::vector <VkPhysicalDeviceProperties> physical_dev_properties(num_gpus);
    for (uint32_t i = 0; i < num_gpus; ++i) {
        vkGetPhysicalDeviceProperties(a_physical_devices[i], &physical_dev_properties[i]);
    }

    std::optional <uint32_t> picked_dev_idx;
    for (uint32_t i = 0; i < num_gpus && !picked_dev_idx.has_value(); ++i) {
        std::string dev_type_str{ str_from_VkPhysicalDeviceType(physical_dev_properties[i].deviceType) };
        if (a_vk_physical_dev_config_json.contains("type") &&
            a_vk_physical_dev_config_json["type"].get<std::string>() == dev_type_str) {
            info("picked gpu of type: " + dev_type_str + device_properties_str(physical_dev_properties[i]));
            picked_dev_idx = i;
        }
    }
    for (uint32_t i = 0; i < num_gpus && !picked_dev_idx.has_value(); ++i) {
        std::string dev_name_str{ physical_dev_properties[i].deviceName };
        if (a_vk_physical_dev_config_json.contains("name")) {
            std::string required_dev_name = to_lower(a_vk_physical_dev_config_json["name"].get<std::string>());
            const auto dev_name_str_lower = to_lower(dev_name_str);
            if (dev_name_str_lower.find(required_dev_name) != std::string::npos) {
                info("picked gpu with name: " + dev_name_str + device_properties_str(physical_dev_properties[i]));
                picked_dev_idx = i;
            }
        }
    }

    if (!picked_dev_idx.has_value()) {
        picked_dev_idx = 0;
        info("defaulted to first device: "
            + std::string{ physical_dev_properties[0].deviceName } + " of type "
            + std::string{ str_from_VkPhysicalDeviceType(physical_dev_properties[0].deviceType) }
        );
        if (num_gpus > 1) {
            std::string msg{ "there are " + std::to_string(num_gpus) + " physical devices available:" };
            for (const auto& dev_prop : physical_dev_properties) {
                msg += device_properties_str(dev_prop);
            }
            info(msg);
        }
    }
    vk_physical_dev_ = a_physical_devices[*picked_dev_idx];
    *properties_ = physical_dev_properties[*picked_dev_idx];

    vkGetPhysicalDeviceMemoryProperties(vk_physical_dev_, memory_properties_.get());

    uint32_t num_available_extensions{};

    SPOCK_VK_CALL(vkEnumerateDeviceExtensionProperties(vk_physical_dev_, nullptr, &num_available_extensions, nullptr));
    
    if (num_available_extensions > 0) {
        available_extensions_.reserve(num_available_extensions);
        std::vector<VkExtensionProperties> extension_properties (num_available_extensions);
        SPOCK_VK_CALL(vkEnumerateDeviceExtensionProperties(vk_physical_dev_, nullptr, &num_available_extensions, extension_properties.data()));
        for (const auto& prop : extension_properties) {
            available_extensions_.insert(prop.extensionName);
        }
    }
}


std::string spock::physical_device_t::
get_available_extension_list() const
{
    std::string ext_list;
    for (const auto& ext : available_extensions_) {
        ext_list += ext + '\n';
    }
    return ext_list;
}

bool spock::physical_device_t::
has_extension(const std::string& a_extension_name) const 
{
    return available_extensions_.end() != std::find(
        available_extensions_.begin(), 
        available_extensions_.end(), 
        a_extension_name);
}


spock::queue_t::
queue_t(VkDevice a_vk_device, uint32_t a_family_index, uint32_t a_queue_index)
    : family_index_(a_family_index)
    , queue_index_(a_queue_index)
{
    vkGetDeviceQueue(a_vk_device, a_family_index, a_queue_index, &vk_queue_);
}


void spock::device_t::
find_dedicated_queue_families()
{
    std::vector<uint32_t> compute_families;
    std::vector<uint32_t> transfer_families;
    for (const auto& queue_family : queue_families_) {
        if (queue_family.family_index_ == *queue_family_general_index_) {
            continue;
        }
        if (has_flag_bits(queue_family.properties_.queueFlags, VK_QUEUE_COMPUTE_BIT)) {
            compute_families.push_back(queue_family.family_index_);
        }
        if (has_flag_bits(queue_family.properties_.queueFlags, VK_QUEUE_TRANSFER_BIT)) {
            transfer_families.push_back(queue_family.family_index_);
        }
    }
    info("general queue family: " + std::to_string(*queue_family_general_index_));

    if (compute_families.empty() && transfer_families.empty()) {
        info("no dedicated compute and transfer families available");
        return;
    }

    assert(std::is_sorted(transfer_families.begin(), transfer_families.end()));
    assert(std::is_sorted(compute_families.begin(), compute_families.end()));

    std::vector<uint32_t> transfer_only_families;
    std::vector<uint32_t> compute_only_families;
    std::set_difference(
        transfer_families.begin(), transfer_families.end(),
        compute_families.begin(), compute_families.end(),
        std::inserter(transfer_only_families, transfer_only_families.begin())
    );
    std::set_difference(
        compute_families.begin(), compute_families.end(),
        transfer_families.begin(), transfer_families.end(),
        std::inserter(compute_only_families, compute_only_families.begin())
    );
    if (!transfer_only_families.empty()) {
        queue_family_transfer_index_ = transfer_only_families[0];
        if (!compute_only_families.empty()) {
            queue_family_compute_index_ = compute_only_families[0];
        }
        else if (!compute_families.empty()) {
            queue_family_compute_index_ = compute_families[0];
        }
    }
    else if (!compute_only_families.empty()) { // transfer_only_families is empty
        queue_family_compute_index_ = compute_only_families[0];
        if (!transfer_families.empty()) {
            queue_family_transfer_index_ = transfer_families[0];
        }
    }
    // both compute and transfer only families are empty, compute_families == transfer_families,
    // and since at least one of compute_families and transfer_families are non-empty, neither is empty
    else if (compute_families.size() == 1) { 
        queue_family_compute_index_ = compute_families[0]; // prefer to have a dedicated compute queue family
    }
    else {
        auto families_with_min_max_queues = std::minmax_element(
            compute_families.begin(),
            compute_families.end(),
            [&queue_families = std::as_const(queue_families_)](const uint32_t& a, const uint32_t& b) -> bool {
                return queue_families[a].properties_.queueCount < queue_families[b].properties_.queueCount;
            }
        );

        if (*families_with_min_max_queues.first != *families_with_min_max_queues.second) {
            // assume gpu is likely to have more compute queues than transfer queues
            queue_family_compute_index_ = *families_with_min_max_queues.second;
            queue_family_transfer_index_ = *families_with_min_max_queues.first;
        }
        else {
            queue_family_compute_index_ = compute_families[0];
            queue_family_transfer_index_ = compute_families[1];
        }
    }

    if (queue_family_transfer_index_.has_value()) {
        info("dedicated transfer queue family index = " + std::to_string(*queue_family_transfer_index_));
    }

    if (queue_family_compute_index_.has_value()) {
        info("dedicated compute queue family index = " + std::to_string(*queue_family_compute_index_));
    }

}

auto spock::device_t::
get_enabled_queue_families(const ordered_json& a_queue_config_json) -> enabled_queue_families_t
{
    enabled_queue_families_t enabled_queue_families;
    // if requested dedicated queue not entirely fulfilled, will try to create extra general queues
    uint32_t num_extra_general_queues = 0;

    auto enable_queue_family = [
        &enabled_queue_families, 
        &num_extra_general_queues,
        &queue_families_ = this->queue_families_,
        &queue_config_json = std::as_const(a_queue_config_json)
    ](
        const std::string& family_type_str,
        const std::optional<uint32_t>& family_idx
    ){
        if (!family_idx.has_value()) {
            warn("invalid family index for queue family of type " + family_type_str);
            return;
        }

        if (queue_config_json.contains(family_type_str)) {
            uint32_t num_created_queues = device_t::num_created_queues_default;
            float priority = device_t::queue_priority_default;
            const auto& family_config_json = queue_config_json[family_type_str];
            if (family_config_json.contains("num_requested")) {
                uint32_t num_request_queues = family_config_json["num_requested"].get<uint32_t>();
                num_created_queues = std::min(
                    queue_families_[*family_idx].num_queues_max(),
                    num_request_queues
                );
                num_extra_general_queues += num_request_queues - num_created_queues;
                if (num_created_queues == 0) {
                    return;
                }
                queue_families_[*family_idx].num_created_queues_ = num_created_queues;
            }
            if (family_config_json.contains("priority")) {
                priority = std::clamp(family_config_json["priority"].get<float>(), 0.f, 1.f);
            }
            enabled_queue_families[family_type_str] = queue_family_info_t{ *family_idx, num_created_queues, priority };
        }
    };

    enable_queue_family(general_queue_name, queue_family_general_index_);
    enable_queue_family(transfer_queue_name, queue_family_transfer_index_);
    enable_queue_family(compute_queue_name, queue_family_compute_index_);

    if (num_extra_general_queues > 0) {
        if (enabled_queue_families.find(general_queue_name) != enabled_queue_families.end()) {
            auto& enabled_general_queue = enabled_queue_families[general_queue_name];
            uint32_t num_needed_general_queue = num_extra_general_queues + enabled_general_queue.queue_count;
            enabled_general_queue.queue_count = std::min(
                queue_families_[*queue_family_general_index_].num_queues_max(),
                num_needed_general_queue
            );
        }
        else {
            enabled_queue_families[general_queue_name] = queue_family_info_t{
                *queue_family_general_index_, 
                num_extra_general_queues,
                device_t::queue_priority_default
            };
        }
    }

    return enabled_queue_families;
}


spock::device_t::queue_family_t::
queue_family_t(
    uint32_t a_family_index, 
    const VkQueueFamilyProperties& a_properties, 
    VkBool32 a_surface_supported
)
    : family_index_{a_family_index}
    , properties_{a_properties}
    , num_created_queues_{ 0 }
    , num_used_queues_{ 0 }
    , surface_supported_{VK_FALSE}
{}


uint32_t spock::device_t::queue_family_t::
next_queue_index()
{
    if (num_used_queues_ + 1 > num_created_queues_) {
        return npos<uint32_t>();
    }
    else {
        return num_used_queues_++;
    }
}


spock::device_t::
device_t(device_t && a_other) noexcept
    : vk_dev_{ std::exchange(a_other.vk_dev_, VK_NULL_HANDLE) }
    , queue_families_{ std::move(a_other.queue_families_) }
    , queue_family_general_index_{ a_other.queue_family_general_index_ }
    , queue_family_compute_index_{ a_other.queue_family_compute_index_ }
    , queue_family_transfer_index_{ a_other.queue_family_transfer_index_ }
{
}


spock::device_t & spock::device_t::operator=(device_t && a_other) noexcept
{
    if (this != &a_other) {
        vk_dev_ = std::exchange(a_other.vk_dev_, VK_NULL_HANDLE);
        queue_families_ = std::move(a_other.queue_families_);
        queue_family_general_index_ = a_other.queue_family_general_index_;
        queue_family_compute_index_ = a_other.queue_family_compute_index_;
        queue_family_transfer_index_ = a_other.queue_family_transfer_index_;
    }
    return *this;
}


spock::device_t::
device_t(
    const instance_t& a_instance,
    const physical_device_t& a_physical_dev, 
    const ordered_json& a_vk_dev_config_json,
    physical_dev_all_features2_t& a_all_features2,
    VkSurfaceKHR a_vk_surface,
    std::vector<physical_dev_feature2_wrapper_t>* a_features2_chain
)
{
    uint32_t num_queue_family{ 0 };
    vkGetPhysicalDeviceQueueFamilyProperties(a_physical_dev, &num_queue_family, nullptr);

    std::vector<VkQueueFamilyProperties> queue_family_properties(num_queue_family);
    vkGetPhysicalDeviceQueueFamilyProperties(a_physical_dev, &num_queue_family, queue_family_properties.data());

    queue_families_.reserve(num_queue_family);

    bool found_general = false;
    for (uint32_t i = 0; i < static_cast<uint32_t>(queue_family_properties.size()); ++i) {
        queue_families_.emplace_back(i, queue_family_properties[i]);
        
        if (a_vk_surface != VK_NULL_HANDLE) {
            SPOCK_VK_CALL(
                vkGetPhysicalDeviceSurfaceSupportKHR(a_physical_dev, i, a_vk_surface, &queue_families_.back().surface_supported_)
            );
        }
        
        const std::string queue_family_msg = "queue family " + std::to_string(i)
            + ": flag = " + str_from_VkQueueFlags(queue_family_properties[i].queueFlags)
            + "; queue count: " + std::to_string(queue_family_properties[i].queueCount)
            + "; surface support: " + (queue_families_.back().surface_supported_ ? "true" : "false");
        info(queue_family_msg);

        if (!found_general) {
            if (has_flag_bits(queue_family_properties[i].queueFlags, queue_t::FLAGS_GENERAL)) {
                found_general = true;
                queue_family_general_index_ = i;
            }
            else if (has_flag_bits(queue_family_properties[i].queueFlags, queue_t::FLAGS_LESS_GENERAL)) {
                queue_family_general_index_ = i;
            }
        }
    }

    ensure(queue_family_general_index_.has_value(), "general queue family not found");

    ensure(
        (a_vk_surface == VK_NULL_HANDLE) || queue_families_[*queue_family_general_index_].surface_supported_,
        "general queue family does not support surface"
    );

    find_dedicated_queue_families();
    
    enabled_queue_families_t enabled_queue_families;

    if (a_vk_dev_config_json.contains("queues")) {
        // general queue will not be automatically added, has to be explicitly specified in config.json if needed
        enabled_queue_families = get_enabled_queue_families(a_vk_dev_config_json["queues"]);
    }
    else {
        enabled_queue_families[general_queue_name] = queue_family_info_t{
            *queue_family_general_index_, 
            device_t::num_created_queues_default, 
            device_t::queue_priority_default
        };
    }

    std::vector<VkDeviceQueueCreateInfo> queue_create_infos(
        enabled_queue_families.size(), 
        { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO }
    );
    std::vector<std::vector<float>> queue_priorities(enabled_queue_families.size());
    
    {
        std::string queue_msg;
        size_t i = 0;
        for (const auto& enabled_queue_family : enabled_queue_families) {
            const auto& queue_family_info = enabled_queue_family.second;
            queue_priorities[i].resize(queue_family_info.queue_count, queue_family_info.priority);
        
            queue_create_infos[i].queueFamilyIndex = queue_family_info.family_idx;
            queue_create_infos[i].queueCount       = queue_family_info.queue_count;
            queue_create_infos[i].pQueuePriorities = queue_priorities[i].data();
            ++i;
            queue_msg += enabled_queue_family.first + " queue: "
                + "number created: " + std::to_string(queue_family_info.queue_count)
                + "; priority: " + std::to_string(queue_family_info.priority)
                + '\n';
        }
        info(queue_msg);
    }

    std::map<std::string_view, bool> feature_requests;
    if (a_vk_dev_config_json.contains("features2")) {
        for (const auto& feature_json : a_vk_dev_config_json["features2"].items()) {
            const char* feature_c_str = feature_json.key().data();
            feature_requests[std::string_view(feature_c_str)] = feature_json.value();
        }
    }
    // required by ray tracing: vkGetBufferDeviceAddress(...); visibility rendering: *ArrayNonUniformIndexing
    if (feature_requests.find("VkPhysicalDeviceVulkan12Features") == feature_requests.end()) {
        feature_requests["VkPhysicalDeviceVulkan12Features"] = false;
    }
    std::string feature_chain_error_msg;
    bool feature_chain_ok = false;
    auto enabled_feature_wrappers = a_all_features2.link(feature_requests, a_physical_dev.available_extensions(), feature_chain_ok, feature_chain_error_msg);
    if (feature_chain_ok && !feature_chain_error_msg.empty()) {
        warn(feature_chain_error_msg);
    }
    else if (!feature_chain_error_msg.empty()) {
        err(feature_chain_error_msg);
    }

    std::vector<const char*> required_device_extensions;
    if (a_vk_surface != VK_NULL_HANDLE) {
        required_device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
    }
    if (a_instance.is_debug_printf_on()) {
        SPOCK_ENSURE(a_physical_dev.has_extension(VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME), 
            "device extension not available: " VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME
        );
        required_device_extensions.push_back(VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME);
    }
    if (a_vk_dev_config_json.contains("extensions")) {
        bool extensions_missing = false;
        for (const auto& ext_json : a_vk_dev_config_json["extensions"].items()) {
            const char* ext_c_str = ext_json.key().data();
            bool required = ext_json.value();

            const std::string warn_msg = "extension " + std::string{ ext_c_str } + " is not available on physical device " + a_physical_dev.name();
            const std::string err_msg = warn_msg + "\nall available extensions: \n" + a_physical_dev.get_available_extension_list();

            SPOCK_ENSURE(!required || a_physical_dev.has_extension(ext_c_str), err_msg);

            if (a_physical_dev.has_extension(ext_c_str)) {
                required_device_extensions.push_back(ext_c_str);
            }
            else {
                extensions_missing = true;
                info(warn_msg);
            }
        }
        if (extensions_missing) {
            verbose("all available extensions: \n" + a_physical_dev.get_available_extension_list());
        }
    }

    for (const auto& feature_wrapper : enabled_feature_wrappers) {
        if (!feature_wrapper.versioned) {
            required_device_extensions.push_back(feature_wrapper.extension_name.data());
        }
    }

    if (a_features2_chain) {
        a_features2_chain->clear();
        a_features2_chain->reserve(1 + enabled_feature_wrappers.size());
        a_features2_chain->push_back(a_all_features2.wrap_feature("VkPhysicalDeviceFeatures2"));
        a_features2_chain->insert(a_features2_chain->end(), enabled_feature_wrappers.begin(), enabled_feature_wrappers.end());
    }

    VkDeviceCreateInfo dev_create_info = make_VkDeviceCreateInfo();
    dev_create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());;
    dev_create_info.pQueueCreateInfos    = queue_create_infos.data();

    VkPhysicalDeviceFeatures2* physical_dev_features2 = &a_all_features2.PhysicalDeviceFeatures2; 

    if (!enabled_feature_wrappers.empty()) {
        physical_dev_features2->pNext = enabled_feature_wrappers[0].feature_ptr;
    }

    if (!a_all_features2.requested_only) { // if requested_only, the user must manually fill the requested feature fields
        vkGetPhysicalDeviceFeatures2(a_physical_dev, physical_dev_features2);

        if (a_vk_dev_config_json.contains("bad_features")) {
            const auto& bad_features_json = a_vk_dev_config_json["bad_features"];
            if (bad_features_json.contains("robustBufferAccess")) {
                physical_dev_features2->features.robustBufferAccess = bad_features_json["robustBufferAccess"].get<bool>();
            }
        }
    }

    dev_create_info.pEnabledFeatures = nullptr;
    dev_create_info.pNext            = physical_dev_features2;

    if (!required_device_extensions.empty()) {
        // optional, deduplicate 
        auto str_equal = [](const char* lhs, const char* rhs) { return std::strcmp(lhs, rhs) == 0; };
        auto str_less = [](const char* lhs, const char* rhs) { return std::strcmp(lhs, rhs) < 0; };
        std::sort(required_device_extensions.begin(), required_device_extensions.end(), str_less);
        auto last = std::unique(required_device_extensions.begin(), required_device_extensions.end(), str_equal);
        required_device_extensions.erase(last, required_device_extensions.end());

        dev_create_info.enabledExtensionCount   = static_cast<uint32_t>(required_device_extensions.size());
        dev_create_info.ppEnabledExtensionNames = required_device_extensions.data();
    }
    SPOCK_VK_CALL(
        vkCreateDevice(a_physical_dev, &dev_create_info, nullptr, &vk_dev_)
    );
}


spock::device_t::
~device_t()
{
    if (vk_dev_ != VK_NULL_HANDLE) {
        verbose("destroy device...");
        vkDestroyDevice(vk_dev_, nullptr);
    }
}


spock::queue_t spock::device_t::
get_queue(VkQueueFlags a_requested_queue_flags, VkBool32 a_surface_supported)
{
    auto family_idx = npos<uint32_t>();
    auto queue_idx = npos<uint32_t>();

    if (a_requested_queue_flags == queue_t::FLAGS_GENERAL) {
        family_idx = *queue_family_general_index_;
        queue_idx = queue_families_[family_idx].next_queue_index();
    }
    else if (a_requested_queue_flags == static_cast<VkQueueFlags>(VK_QUEUE_COMPUTE_BIT) && queue_family_compute_index_.has_value()) {
        family_idx = *queue_family_compute_index_;
        queue_idx = queue_families_[family_idx].next_queue_index();
    }
    else if (a_requested_queue_flags == static_cast<VkQueueFlags>(VK_QUEUE_TRANSFER_BIT) && queue_family_transfer_index_.has_value()) {
        family_idx = *queue_family_transfer_index_;
        queue_idx = queue_families_[family_idx].next_queue_index();
    }

    if (family_idx == npos<uint32_t>() || queue_idx == npos<uint32_t>()) {
        for (const auto& family : queue_families_) {
            if (has_flag_bits(family.properties_.queueFlags, a_requested_queue_flags) && (!a_surface_supported || family.surface_supported_)) {
                family_idx = family.family_index_;
                queue_idx = queue_families_[family_idx].next_queue_index();
                if (queue_idx != npos<uint32_t>()) {
                    break;
                }
            }
        }
    }

    if (family_idx == npos<uint32_t>() || queue_idx == npos<uint32_t>()) {
        auto msg = "no suitable queue found available: " + str_from_VkQueueFlags(a_requested_queue_flags);
        if (a_surface_supported) {
            msg += " with surface support";
        }
        except(msg);
    }

    return queue_t(vk_dev_, family_idx, queue_idx);

}


spock::context_t::
context_t(
    const ordered_json& a_config_json,
    physical_dev_all_features2_t& a_all_features2,
    const window_t* a_window,
    std::vector<physical_dev_feature2_wrapper_t>* a_features2_chain,
    PFN_vkDebugUtilsMessengerCallbackEXT a_debug_utils_messenger_callback,
    void* a_debug_utils_messenger_user_data
) 
    : config_json_{ std::make_unique<const ordered_json>(a_config_json) }
    , alloc_{ VK_NULL_HANDLE }

{
    std::vector<const char*> window_extensions;
    if (a_window) {
        window_extensions = a_window->instance_extensions();
    }
    instance_ = instance_t(*config_json_, window_extensions, a_debug_utils_messenger_callback, a_debug_utils_messenger_user_data);
    if (instance_.is_debug_utils_messenger_on()) {
        debug_utils_messenger_ = debug_utils_messenger_t(instance_, instance_.debug_utils_messenger_create_info());
    }
    uint32_t instance_version = 0;
    vkEnumerateInstanceVersion(&instance_version);
    ensure(instance_version > VK_MAKE_API_VERSION(0, 1, 2, 0), "supported vulkan instance version is older than 1.2");
    info("supported vulkan instance version: " + vulkan_api_version_str(instance_version));

    if (a_window) {
        surface_ = surface_t(instance_, a_window->create_surface(instance_));
    }
    uint32_t num_gpus = 0;
    SPOCK_VK_CALL(vkEnumeratePhysicalDevices(instance_, &num_gpus, nullptr));
    SPOCK_ENSURE(num_gpus > 0, "no physical devices found");

    std::vector<VkPhysicalDevice> physical_devices(num_gpus);
    SPOCK_VK_CALL(vkEnumeratePhysicalDevices(instance_, &num_gpus, physical_devices.data()));
    physical_dev_ = physical_device_t(physical_devices, (*config_json_)["physical_device"]);

    device_ = device_t(instance_, physical_dev_, (*config_json_)["device"], a_all_features2, surface_, a_features2_chain);

    VmaVulkanFunctions vma_vk_functions{};
    vma_vk_functions.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
    vma_vk_functions.vkGetDeviceProcAddr = vkGetDeviceProcAddr;
    VmaAllocatorCreateInfo vma_alloc_create_info = {};
    if (a_all_features2.PhysicalDeviceVulkan12Features.bufferDeviceAddress) {
        vma_alloc_create_info.flags |= VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    }
    vma_alloc_create_info.physicalDevice   = physical_dev_;
    vma_alloc_create_info.device           = device_;
    vma_alloc_create_info.pVulkanFunctions = &vma_vk_functions;
    vma_alloc_create_info.vulkanApiVersion = instance_.vk_api_version();
    vma_alloc_create_info.instance         = instance_;
    SPOCK_VK_CALL(vmaCreateAllocator(&vma_alloc_create_info, &alloc_)); // okay to throw, previously fully constructed objects will be safely destructed.
}


spock::context_t::
~context_t()
{
    if (alloc_ != VK_NULL_HANDLE) {
        verbose("destroy vma_allocator...");
        vmaDestroyAllocator(alloc_);
    }
}


VkSampleCountFlagBits spock::context_t::
sample_count_flag_bits_max() const
{
    const auto& props = *physical_dev_.properties_;
    auto sample_counts = props.limits.framebufferColorSampleCounts & props.limits.framebufferDepthSampleCounts;
    info(std::string{ "supported sample count: " } + str_from_VkSampleCountFlags(sample_counts));

    if (sample_counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
    if (sample_counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
    if (sample_counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
    if (sample_counts & VK_SAMPLE_COUNT_8_BIT)  { return VK_SAMPLE_COUNT_8_BIT; }
    if (sample_counts & VK_SAMPLE_COUNT_4_BIT)  { return VK_SAMPLE_COUNT_4_BIT; }
    if (sample_counts & VK_SAMPLE_COUNT_2_BIT)  { return VK_SAMPLE_COUNT_2_BIT; }
    return VK_SAMPLE_COUNT_1_BIT;
}


VkFormat spock::context_t::
supported_format(
    const std::vector<VkFormat>& a_format_cadidates,
    VkFormatFeatureFlags a_format_features,
    VkImageTiling a_image_tiling
) const
{
    ensure(!a_format_cadidates.empty(), "no format candidates provided");
    for (auto format : a_format_cadidates) {
        VkFormatProperties format_properties;
        vkGetPhysicalDeviceFormatProperties(physical_dev_, format, &format_properties);

        VkFormatFeatureFlags feature_flags{};

        switch (a_image_tiling) {
        case VK_IMAGE_TILING_LINEAR:
            feature_flags = format_properties.linearTilingFeatures & a_format_features;
            break;
        case VK_IMAGE_TILING_OPTIMAL:
            feature_flags = format_properties.optimalTilingFeatures & a_format_features;
            break;
        default:
            feature_flags = format_properties.bufferFeatures;
        }
        if (feature_flags & a_format_features) {
            return format;
        }
    }
    except("failed to find a supported format");
    return VK_FORMAT_UNDEFINED;
}


bool spock::context_t::
format_features_supported(VkFormatFeatureFlags a_format_features, VkFormat a_format, VkImageTiling a_image_tiling) const
{
    VkFormatProperties format_properties;
    vkGetPhysicalDeviceFormatProperties(physical_dev_, a_format, &format_properties);

    if (a_image_tiling == VK_IMAGE_TILING_LINEAR) {
        if ( (format_properties.linearTilingFeatures & a_format_features) == a_format_features) {
            return true;
        }
    }
    else {
        if ( (format_properties.optimalTilingFeatures & a_format_features) == a_format_features) {
            return true;
        }
    }
    std::string msg = image_format_features_str(a_format, a_image_tiling) + " < required features: " + str_from_VkFormatFeatureFlags(a_format_features);
    warn(msg);
    return false;
}


std::string spock::context_t::
image_format_features_str(VkFormat a_format, VkImageTiling a_image_tiling) const
{
    VkFormatProperties format_properties;
    vkGetPhysicalDeviceFormatProperties(physical_dev_, a_format, &format_properties);

    std::string features_str;

    if (a_image_tiling == VK_IMAGE_TILING_LINEAR) {
        features_str = str_from_VkFormatFeatureFlags(format_properties.linearTilingFeatures);
    }
    else {
        features_str = str_from_VkFormatFeatureFlags(format_properties.optimalTilingFeatures);
    }
    return std::string("[") + str_from_VkFormat(a_format).data()
        + std::string(", ") + str_from_VkImageTiling(a_image_tiling).data()
        + std::string("]: ") + features_str;
}


void spock::context_t::
get_physical_dev_properties2(void* a_next_ptr) const
{
    auto properties2 = make_VkPhysicalDeviceProperties2();
    properties2.pNext = a_next_ptr;
    vkGetPhysicalDeviceProperties2(physical_dev_, &properties2);
}


VkDeviceAddress spock::context_t::
get_buffer_device_address(VkBuffer a_buffer) const
{
  auto info = make_VkBufferDeviceAddressInfo();
  info.buffer = a_buffer;
  return vkGetBufferDeviceAddress(device_, &info);
}


spock::sbt_entry_size_t spock::context_t::
calc_sbt_entry_size(uint32_t a_stride_min)
{
    auto rt_ppl_props = make_VkPhysicalDeviceRayTracingPipelinePropertiesKHR();
    get_physical_dev_properties2(&rt_ppl_props);
    return spock::calc_sbt_entry_size(rt_ppl_props, a_stride_min);
}


spock::semaphore_t::
semaphore_t(semaphore_t&& a_semaphore) noexcept
    : vk_dev_ { std::exchange(a_semaphore.vk_dev_, VK_NULL_HANDLE) }
    , vk_semaphore_ { std::exchange(a_semaphore.vk_semaphore_, VK_NULL_HANDLE) }
{
}


spock::semaphore_t& spock::semaphore_t::
operator= (semaphore_t&& a_semaphore) noexcept
{
    if (this != &a_semaphore) {
        if (valid()) {
            vkDestroySemaphore(vk_dev_, vk_semaphore_, nullptr);
        }
        vk_dev_       = std::exchange(a_semaphore.vk_dev_, VK_NULL_HANDLE);
        vk_semaphore_ = std::exchange(a_semaphore.vk_semaphore_, VK_NULL_HANDLE);
    }
    return *this;
}


/// VkSemaphoreCreateFlags: reserved for future use
spock::semaphore_t::
semaphore_t(VkDevice a_vk_dev, VkSemaphoreCreateFlags a_vk_semaphore_create_flags)
    : vk_dev_       { a_vk_dev }
{
    auto vk_semaphore_create_info = make_VkSemaphoreCreateInfo();
    vk_semaphore_create_info.flags = a_vk_semaphore_create_flags;

    SPOCK_VK_CALL(vkCreateSemaphore(vk_dev_, &vk_semaphore_create_info, nullptr, &vk_semaphore_));
}


spock::semaphore_t::
~semaphore_t()
{
    if (valid()) {
        vkDestroySemaphore(vk_dev_, vk_semaphore_, nullptr);
    }
}


spock::fence_t::
fence_t(fence_t&& a_fence) noexcept
    : vk_dev_{ std::exchange(a_fence.vk_dev_, VK_NULL_HANDLE) }
    , vk_fence_ { std::exchange(a_fence.vk_fence_, VK_NULL_HANDLE) }
{}


spock::fence_t& spock::fence_t::
operator= (fence_t&& a_fence) noexcept
{
    if (this != &a_fence) {
        if (valid()) {
            vkDestroyFence(vk_dev_, vk_fence_, nullptr);
        }
        vk_dev_    = std::exchange(a_fence.vk_dev_, VK_NULL_HANDLE);
        vk_fence_  = std::exchange(a_fence.vk_fence_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::fence_t::
fence_t(VkDevice a_vk_dev, VkFenceCreateFlags a_vk_fence_create_flags)
    : vk_dev_ { a_vk_dev }
{
    auto vk_fence_create_info = make_VkFenceCreateInfo();
    vk_fence_create_info.flags = a_vk_fence_create_flags;

    SPOCK_VK_CALL(vkCreateFence(vk_dev_, &vk_fence_create_info, nullptr, &vk_fence_));
}


spock::fence_t::
~fence_t()
{
    if (valid()) {
        vkDestroyFence(vk_dev_, vk_fence_, nullptr);
    }
}


spock::command_pool_t::
command_pool_t(command_pool_t&& a_other) noexcept
    : vk_dev_ { std::exchange(a_other.vk_dev_, VK_NULL_HANDLE) }
    , vk_command_pool_ { std::exchange(a_other.vk_command_pool_, VK_NULL_HANDLE) }
    , queue_family_index_ { std::exchange(a_other.queue_family_index_, npos<uint32_t>()) }
{
}


spock::command_pool_t& spock::command_pool_t::
operator= (command_pool_t&& a_other) noexcept
{
    if (this != &a_other) {
        if (valid()) {
            verbose("destroy command pool...");
            vkDestroyCommandPool(vk_dev_, vk_command_pool_, nullptr);
        }
        vk_dev_             = std::exchange(a_other.vk_dev_, VK_NULL_HANDLE);
        vk_command_pool_    = std::exchange(a_other.vk_command_pool_, VK_NULL_HANDLE);
        queue_family_index_ = std::exchange(a_other.queue_family_index_, npos<uint32_t>());
    }
    return *this;
}


spock::command_pool_t::
command_pool_t(VkDevice a_vk_dev, const VkCommandPoolCreateInfo& a_command_pool_create_info)
    : vk_dev_                   { a_vk_dev }
    , vk_command_pool_          { VK_NULL_HANDLE }
    , queue_family_index_       { npos<uint32_t>() }
{
    SPOCK_VK_CALL(vkCreateCommandPool(vk_dev_, &a_command_pool_create_info, nullptr, &vk_command_pool_));
    queue_family_index_ = a_command_pool_create_info.queueFamilyIndex;
}


spock::command_pool_t::
command_pool_t(VkDevice a_vk_dev, uint32_t a_queue_family_index, VkCommandPoolCreateFlags a_flags)
    : vk_dev_ { a_vk_dev }
    , queue_family_index_{ a_queue_family_index }
{
    auto vk_command_pool_create_info = spock::make_VkCommandPoolCreateInfo();
    vk_command_pool_create_info.flags = a_flags;
    vk_command_pool_create_info.queueFamilyIndex = a_queue_family_index;

    SPOCK_VK_CALL(vkCreateCommandPool(vk_dev_, &vk_command_pool_create_info, nullptr, &vk_command_pool_));
}


spock::command_pool_t::
~command_pool_t()
{
    if (valid()) {
        verbose("destroy command pool...");
        vkDestroyCommandPool(vk_dev_, vk_command_pool_, nullptr);
    }
}


spock::command_buffer_t::
command_buffer_t(command_buffer_t&& a_other) noexcept
    : vk_dev_{ std::exchange(vk_dev_, a_other.vk_dev_)}
    , vk_command_pool_{ std::exchange(a_other.vk_command_pool_, nullptr) }
    , vk_command_buffer_{ std::exchange(a_other.vk_command_buffer_, VK_NULL_HANDLE) }
{
}


spock::command_buffer_t& spock::command_buffer_t::
operator=(command_buffer_t&& a_other) noexcept
{
    if (this != &a_other) {
        if (valid()) {
            verbose("destroy command buffer");
            vkFreeCommandBuffers(vk_dev_, vk_command_pool_, 1, &vk_command_buffer_);
        }
        vk_dev_            = std::exchange(vk_dev_, a_other.vk_dev_);
        vk_command_pool_   = std::exchange(a_other.vk_command_pool_, nullptr);
        vk_command_buffer_ = std::exchange(a_other.vk_command_buffer_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::command_buffer_t::
command_buffer_t(const command_pool_t& a_command_pool, VkCommandBufferLevel a_level)
    : vk_dev_{ a_command_pool.vk_dev() }
    , vk_command_pool_{ a_command_pool }
{
    SPOCK_PARANOID(a_command_pool.valid(), "command_buffer_t::ctor: a_command_pool is invalid");

    auto vk_command_buffer_alloc_info = make_VkCommandBufferAllocateInfo();
    vk_command_buffer_alloc_info.commandPool = vk_command_pool_;
    vk_command_buffer_alloc_info.level = a_level;
    vk_command_buffer_alloc_info.commandBufferCount = 1;

    SPOCK_VK_CALL(vkAllocateCommandBuffers(vk_dev_, &vk_command_buffer_alloc_info, &vk_command_buffer_));
}


spock::command_buffer_t::
~command_buffer_t()
{
    if (valid()) {
        verbose("destroy command buffer");
        vkFreeCommandBuffers(vk_dev_, vk_command_pool_, 1, &vk_command_buffer_);
    }
}


spock::command_buffer_array_t::
command_buffer_array_t(command_buffer_array_t && a_other) noexcept
    : vk_dev_ { std::exchange(a_other.vk_dev_, VK_NULL_HANDLE) }
    , vk_command_pool_{ std::exchange(a_other.vk_command_pool_, VK_NULL_HANDLE) }
    , vk_command_buffers_{ std::move(a_other.vk_command_buffers_) }
{
}


spock::command_buffer_array_t& spock::command_buffer_array_t::
operator=(command_buffer_array_t && a_other) noexcept
{
    if (this != &a_other) {
        if (!vk_command_buffers_.empty()) {
            verbose("destroy command_buffer_array_t");
            vkFreeCommandBuffers(vk_dev_, vk_command_pool_, length(), vk_command_buffers_.data());
        }
        vk_dev_ = std::exchange(a_other.vk_dev_, VK_NULL_HANDLE);
        vk_command_pool_= std::exchange(a_other.vk_command_pool_, VK_NULL_HANDLE);
        vk_command_buffers_= std::move(a_other.vk_command_buffers_);
    }
    return *this;
}


spock::command_buffer_array_t::
command_buffer_array_t(const command_pool_t& a_command_pool,  uint32_t a_length, VkCommandBufferLevel a_level)
    : vk_dev_ { a_command_pool.vk_dev() }
    , vk_command_pool_{ a_command_pool }
    , vk_command_buffers_(a_length, VK_NULL_HANDLE)
{
    SPOCK_PARANOID(a_command_pool.valid(), "command_buffer_t::ctor: a_command_pool is invalid");

    auto vk_command_buffer_alloc_info = make_VkCommandBufferAllocateInfo();
    vk_command_buffer_alloc_info.commandPool = vk_command_pool_;
    vk_command_buffer_alloc_info.level = a_level;
    vk_command_buffer_alloc_info.commandBufferCount = a_length;

    SPOCK_VK_CALL(vkAllocateCommandBuffers(vk_dev_, &vk_command_buffer_alloc_info, vk_command_buffers_.data()));
}


spock::command_buffer_array_t::~command_buffer_array_t()
{
    if (!vk_command_buffers_.empty()) {
        verbose("destroy command_buffer_array_t");
        vkFreeCommandBuffers(vk_dev_, vk_command_pool_, length(), vk_command_buffers_.data());
    }
}


const VkCommandBuffer& spock::command_buffer_array_t::
at(uint32_t a_command_buffer_idx) const
{
    SPOCK_PARANOID(a_command_buffer_idx < vk_command_buffers_.size(), "a_command_buffer_idx out of bounds");
    return vk_command_buffers_[a_command_buffer_idx];
}


spock::descriptor_set_layout_t::
descriptor_set_layout_t(descriptor_set_layout_t&& a_descriptor_set_layout) noexcept
    : vk_dev_ { std::exchange(a_descriptor_set_layout.vk_dev_, VK_NULL_HANDLE) }
    , vk_descriptor_set_layout_ { std::exchange(a_descriptor_set_layout.vk_descriptor_set_layout_, VK_NULL_HANDLE) }
{}


spock::descriptor_set_layout_t& spock::descriptor_set_layout_t::
operator= (descriptor_set_layout_t&& a_descriptor_set_layout) noexcept
{
    if (this != &a_descriptor_set_layout) {
        if (valid()) {
            verbose("destroy descriptor set layout...");
            vkDestroyDescriptorSetLayout(vk_dev_, vk_descriptor_set_layout_, nullptr);
        }
        vk_dev_                   = std::exchange(a_descriptor_set_layout.vk_dev_, VK_NULL_HANDLE);
        vk_descriptor_set_layout_ = std::exchange(a_descriptor_set_layout.vk_descriptor_set_layout_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::descriptor_set_layout_t::
descriptor_set_layout_t(VkDevice a_vk_dev, const VkDescriptorSetLayoutCreateInfo& a_vk_descriptor_set_layout_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateDescriptorSetLayout(vk_dev_, &a_vk_descriptor_set_layout_create_info, nullptr, &vk_descriptor_set_layout_));
}


spock::descriptor_set_layout_t::
~descriptor_set_layout_t()
{
    if (valid()) {
        verbose("destroy descriptor set layout...");
        vkDestroyDescriptorSetLayout(vk_dev_, vk_descriptor_set_layout_, nullptr);
    }
}


spock::descriptor_pool_t::
descriptor_pool_t(descriptor_pool_t&& a_descriptor_pool) noexcept
    : vk_dev_ { std::exchange(a_descriptor_pool.vk_dev_, VK_NULL_HANDLE) }
    , vk_descriptor_pool_ { std::exchange(a_descriptor_pool.vk_descriptor_pool_, VK_NULL_HANDLE) }
{}


spock::descriptor_pool_t& spock::descriptor_pool_t::
operator= (descriptor_pool_t&& a_descriptor_pool) noexcept
{
    if (this != &a_descriptor_pool) {
        if (valid()) {
            verbose("destroy descriptor pool...");
            vkDestroyDescriptorPool(vk_dev_, vk_descriptor_pool_, nullptr);
        }
        vk_dev_             = std::exchange(a_descriptor_pool.vk_dev_, VK_NULL_HANDLE);
        vk_descriptor_pool_ = std::exchange(a_descriptor_pool.vk_descriptor_pool_, VK_NULL_HANDLE);
    }
    return *this;
}



spock::descriptor_pool_t::
descriptor_pool_t(VkDevice a_vk_dev, const VkDescriptorPoolCreateInfo& a_vk_descriptor_pool_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateDescriptorPool( vk_dev_, &a_vk_descriptor_pool_create_info, nullptr, &vk_descriptor_pool_) );
}


spock::descriptor_pool_t::
~descriptor_pool_t()
{

    if (valid()) {
        verbose("destroy descriptor pool...");
        vkDestroyDescriptorPool(vk_dev_, vk_descriptor_pool_, nullptr);
    }
}


void spock::descriptor_pool_t::
alloc_descriptor_sets(uint32_t a_layout_count, const VkDescriptorSetLayout* a_layouts, VkDescriptorSet* a_descriptor_sets)
{
    SPOCK_PARANOID(a_layout_count > 0, "descriptor_pool_t::alloc_descriptor_sets: a_layout_count cannot be 0");
    SPOCK_PARANOID(a_layouts != nullptr, "descriptor_pool_t::alloc_descriptor_sets: a_layouts cannot be nullptr");
    SPOCK_PARANOID(a_descriptor_sets != nullptr, "descriptor_pool_t::alloc_descriptor_sets: a_descriptor_sets cannot be nullptr");

    auto alloc_info = build_VkDescriptorSetAllocateInfo(vk_descriptor_pool_, a_layout_count, a_layouts);
    SPOCK_VK_CALL(vkAllocateDescriptorSets(vk_dev_, &alloc_info, a_descriptor_sets));
}


void spock::descriptor_pool_t::
alloc_descriptor_set(const VkDescriptorSetLayout& a_layout, VkDescriptorSet* a_descriptor_set)
{
    SPOCK_PARANOID(a_descriptor_set != nullptr, "descriptor_pool_t::alloc_descriptor_set: a_descriptor_set cannot be nullptr");
    auto alloc_info = build_VkDescriptorSetAllocateInfo(vk_descriptor_pool_, 1, &a_layout);
    SPOCK_VK_CALL(vkAllocateDescriptorSets(vk_dev_, &alloc_info, a_descriptor_set));
}


spock::shader_module_t::
shader_module_t(shader_module_t&& a_shader_module) noexcept
    : vk_dev_{ std::exchange(a_shader_module.vk_dev_, VK_NULL_HANDLE) }
    , vk_shader_stage_{ std::exchange(a_shader_module.vk_shader_stage_, VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM) }
    , vk_shader_module_{ std::exchange(a_shader_module.vk_shader_module_, VK_NULL_HANDLE) }
{}


spock::shader_module_t& spock::shader_module_t::
operator= (shader_module_t&& a_shader_module) noexcept
{
    if (this != &a_shader_module) {
        if (valid()) {
            verbose("destroy shader module...");
            vkDestroyShaderModule(vk_dev_, vk_shader_module_, nullptr);
        }
        vk_dev_           = std::exchange(a_shader_module.vk_dev_, VK_NULL_HANDLE);
        vk_shader_stage_  = std::exchange(a_shader_module.vk_shader_stage_, VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM);
        vk_shader_module_ = std::exchange(a_shader_module.vk_shader_module_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::shader_module_t::
shader_module_t(VkDevice a_vk_dev, VkShaderStageFlagBits a_vk_shader_stage, const std::vector<uint32_t>& a_spirv)
    : vk_dev_ { a_vk_dev }
    , vk_shader_stage_{ a_vk_shader_stage }
{
    auto vk_shader_module_create_info = make_VkShaderModuleCreateInfo();
    vk_shader_module_create_info.codeSize = a_spirv.size() * sizeof(uint32_t);
    vk_shader_module_create_info.pCode    = a_spirv.data();
    SPOCK_VK_CALL(vkCreateShaderModule(a_vk_dev, &vk_shader_module_create_info, nullptr, &vk_shader_module_));
}


spock::shader_module_t::
~shader_module_t()
{
    if (valid()) {
        verbose("destroy shader module...");
        vkDestroyShaderModule(vk_dev_, vk_shader_module_, nullptr);
    }
}


spock::shader_module_array_t::
shader_module_array_t(shader_module_array_t && a_other) noexcept
    : vk_shader_stages_{ std::move(a_other.vk_shader_stages_) }
    , vk_shader_modules_{ std::move(a_other.vk_shader_modules_) }
    , vk_dev_{ std::exchange(a_other.vk_dev_, VK_NULL_HANDLE) }
    , length_{ std::exchange(a_other.length_, 0) }
{}


spock::shader_module_array_t & spock::shader_module_array_t::
operator=(shader_module_array_t && a_other) noexcept
{
    if (this != &a_other) {
        for (uint32_t i = 0; i < length_; ++i) {
            vkDestroyShaderModule(vk_dev_, vk_shader_modules_[i], nullptr);
        }
        vk_shader_stages_= std::move(a_other.vk_shader_stages_);
        vk_shader_modules_= std::move(a_other.vk_shader_modules_);
        vk_dev_= std::exchange(a_other.vk_dev_, VK_NULL_HANDLE);
        length_ = std::exchange(a_other.length_, 0);
    }
    return *this;
}


spock::shader_module_array_t::
shader_module_array_t(
    VkDevice a_vk_dev, 
    const spirv_t* a_spirv_begin, 
    uint32_t a_spirv_count,
    VkShaderModuleCreateFlags a_flags,
    const void* const* a_next_pptr
)
    : vk_dev_{ a_vk_dev }
    , length_{ static_cast<uint32_t>(a_spirv_count) }
{
    SPOCK_PARANOID(a_spirv_begin != nullptr, "shader_module_array_t::ctor: a_spirv_begin is invalid");
    SPOCK_PARANOID(a_spirv_count > 0, "shader_module_array_t::ctor: a_spirv_count cannot be 0");

    vk_shader_modules_ = std::make_unique<VkShaderModule[]>(length_);
    vk_shader_stages_ = std::make_unique<VkShaderStageFlagBits[]>(length_);

    const void* const * next_pptr = nullptr;
    std::vector<const void*> next_ptrs;
    if (a_next_pptr) {
        next_pptr = a_next_pptr;
    }
    else {
        next_ptrs.resize(length_, nullptr);
        next_pptr = next_ptrs.data();
    };

    size_t num_created = 0;
    auto vk_shader_module_create_info = make_VkShaderModuleCreateInfo();
    while(num_created < length_) {
        vk_shader_stages_[num_created] = a_spirv_begin[num_created].shader_stage_flag_bits();

        const auto& spirv_bin = a_spirv_begin[num_created].spirv_bin();
        const auto create_info = build_VkShaderModuleCreateInfo(
            spirv_bin.size() * sizeof(uint32_t),
            spirv_bin.data(),
            a_flags,
            next_pptr[num_created]
        );
        auto result = vkCreateShaderModule(vk_dev_, &create_info, nullptr, &vk_shader_modules_[num_created]);
        if (result != VK_SUCCESS) {
            err(std::string{ "vkCreateShaderModule failed: " } + str_from_VkResult(result).data());
            break;
        }
        num_created += 1;
    }
    if (num_created != length_) {
        for (size_t i = 0; i < num_created; ++i) {
            vkDestroyShaderModule(vk_dev_, vk_shader_modules_[i], nullptr);
        }
        except("failed to construct shader_module_array_t");
    }
}


spock::shader_module_array_t::
~shader_module_array_t()
{
    for (uint32_t i = 0; i < length_; ++i) {
        vkDestroyShaderModule(vk_dev_, vk_shader_modules_[i], nullptr);
    }
}


std::unique_ptr<VkPipelineShaderStageCreateInfo[]> spock::
build_shader_stage_create_infos(
    const shader_module_array_t&        a_shader_modules, 
    const VkSpecializationInfo*         a_specialization_info,
    VkPipelineShaderStageCreateFlags    a_flags,
    const char*                         a_name,
    const void* const *                 a_next_pptr
)
{

    const auto length = a_shader_modules.length();

    const void* const * next_pptr = nullptr;
    std::vector<const void*> next_ptrs;
    if (a_next_pptr) {
        next_pptr = a_next_pptr;
    }
    else {
        next_ptrs.resize(length, nullptr);
        next_pptr = next_ptrs.data();
    };

    auto create_infos = std::unique_ptr<VkPipelineShaderStageCreateInfo[]>(new VkPipelineShaderStageCreateInfo[length]);
    for (uint32_t i = 0; i < length; ++i) {
        create_infos[i] = build_VkPipelineShaderStageCreateInfo(
            a_shader_modules.vk_shader_stages()[i],
            a_shader_modules.vk_shader_modules()[i],
            a_specialization_info,
            a_flags,
            a_name,
            next_pptr[i]
        );
    }
    return create_infos;
}


spock::pipeline_layout_t::
pipeline_layout_t(pipeline_layout_t&& a_pipeline_layout) noexcept
    : vk_dev_             { std::exchange(a_pipeline_layout.vk_dev_, VK_NULL_HANDLE) }
    , vk_pipeline_layout_ { std::exchange(a_pipeline_layout.vk_pipeline_layout_, VK_NULL_HANDLE) }
{}


spock::pipeline_layout_t& spock::pipeline_layout_t::
operator= (pipeline_layout_t&& a_pipeline_layout) noexcept
{
    if (this != &a_pipeline_layout) {
        if (valid()) {
            verbose("destroy pipeline layout...");
            vkDestroyPipelineLayout(vk_dev_, vk_pipeline_layout_, nullptr);
        }
        vk_dev_             = std::exchange(a_pipeline_layout.vk_dev_, VK_NULL_HANDLE);
        vk_pipeline_layout_ = std::exchange(a_pipeline_layout.vk_pipeline_layout_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::pipeline_layout_t::
pipeline_layout_t(VkDevice a_vk_dev, const VkPipelineLayoutCreateInfo& a_vk_pipeline_layout_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreatePipelineLayout(vk_dev_, &a_vk_pipeline_layout_create_info, nullptr, &vk_pipeline_layout_));
}


spock::pipeline_layout_t::
~pipeline_layout_t()
{
    if (valid()) {
        verbose("destroy pipeline layout...");
        vkDestroyPipelineLayout(vk_dev_, vk_pipeline_layout_, nullptr);
    }
}


spock::pipeline_t::
pipeline_t(pipeline_t && a_pipeline) noexcept
    : vk_dev_{ std::exchange(a_pipeline.vk_dev_, VK_NULL_HANDLE) }
    , vk_pipeline_{ std::exchange(a_pipeline.vk_pipeline_, VK_NULL_HANDLE) }
{
}


spock::pipeline_t& spock::pipeline_t::
operator=(pipeline_t && a_pipeline) noexcept
{
    if (this != &a_pipeline) {
        if (valid()) {
            vkDestroyPipeline(vk_dev_, vk_pipeline_, nullptr);
        }
        vk_dev_      = std::exchange(a_pipeline.vk_dev_, VK_NULL_HANDLE);
        vk_pipeline_ = std::exchange(a_pipeline.vk_pipeline_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::pipeline_t::
pipeline_t(VkDevice a_vk_dev, const VkComputePipelineCreateInfo& a_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateComputePipelines(vk_dev_, VK_NULL_HANDLE, 1, &a_create_info, nullptr, &vk_pipeline_));
}


spock::pipeline_t::
pipeline_t(VkDevice a_vk_dev, const VkGraphicsPipelineCreateInfo& a_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateGraphicsPipelines(vk_dev_, VK_NULL_HANDLE, 1, &a_create_info, nullptr, &vk_pipeline_));
}


spock::pipeline_t::
~pipeline_t()
{
    if (valid()) {
        vkDestroyPipeline(vk_dev_, vk_pipeline_, nullptr);
    }
}


spock::graphics_pipeline_create_info_helper_t::
graphics_pipeline_create_info_helper_t(uint32_t a_viewport_width, uint32_t a_viewport_height)
{
    create_flags = 0;
    vertex_input_state_create_info = make_VkPipelineVertexInputStateCreateInfo();

    input_assembly_state_create_info                        = make_VkPipelineInputAssemblyStateCreateInfo();
    input_assembly_state_create_info.topology               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    input_assembly_state_create_info.primitiveRestartEnable = VK_FALSE;

    tessellation_state_create_info = make_VkPipelineTessellationStateCreateInfo();

    if (a_viewport_width > 0 && a_viewport_height > 0) {
        VkViewport viewport{};
        viewport.x        = 0.f;
        viewport.y        = 0.f;
        viewport.width    = static_cast<float>(a_viewport_width);
        viewport.height   = static_cast<float>(a_viewport_height);
        viewport.minDepth = 0.f;
        viewport.maxDepth = 1.f;

        VkRect2D scissor{};
        scissor.offset = { 0, 0 };
        scissor.extent = VkExtent2D{ a_viewport_width, a_viewport_height };
    
        viewports.push_back(viewport);
        scissors.push_back(scissor);
    }

    viewport_state_create_info = make_VkPipelineViewportStateCreateInfo();

    rasterization_state_create_info = make_VkPipelineRasterizationStateCreateInfo();
    rasterization_state_create_info.depthClampEnable        = VK_FALSE;
    rasterization_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    rasterization_state_create_info.polygonMode             = VK_POLYGON_MODE_FILL;
    rasterization_state_create_info.cullMode                = VK_CULL_MODE_BACK_BIT;
    rasterization_state_create_info.frontFace               = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterization_state_create_info.depthBiasEnable         = VK_FALSE;

    multisample_state_create_info = make_VkPipelineMultisampleStateCreateInfo();
    multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisample_state_create_info.sampleShadingEnable  = VK_FALSE;

    depth_stencil_state_create_info = make_VkPipelineDepthStencilStateCreateInfo();
    depth_stencil_state_create_info.depthTestEnable       = VK_TRUE;
    depth_stencil_state_create_info.depthWriteEnable      = VK_TRUE;
    depth_stencil_state_create_info.depthCompareOp        = VK_COMPARE_OP_LESS;
    depth_stencil_state_create_info.depthBoundsTestEnable = VK_FALSE;
    depth_stencil_state_create_info.stencilTestEnable     = VK_FALSE;

    VkPipelineColorBlendAttachmentState color_blend_attachment{};
    color_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    color_blend_attachment.blendEnable    = VK_FALSE;
    color_blend_attachments.push_back(color_blend_attachment);

    color_blend_state_create_info = make_VkPipelineColorBlendStateCreateInfo();
    color_blend_state_create_info.logicOpEnable = VK_FALSE;
    color_blend_state_create_info.logicOp       = VK_LOGIC_OP_COPY;
    float* blend_constants                      = color_blend_state_create_info.blendConstants;
    std::fill(blend_constants, blend_constants + 4, 0.f);

    if (a_viewport_width == 0 || a_viewport_height == 0) {
        dynamic_states.push_back(VK_DYNAMIC_STATE_VIEWPORT);
        dynamic_states.push_back(VK_DYNAMIC_STATE_SCISSOR);
    }
    dynamic_state_create_info = make_VkPipelineDynamicStateCreateInfo();

    base_pipeline_handle = VK_NULL_HANDLE;
    base_pipeline_index = -1;
}


VkGraphicsPipelineCreateInfo spock::graphics_pipeline_create_info_helper_t::
resolve
(
    VkDevice a_vk_dev, VkRenderPass a_render_pass, uint32_t a_subpass, 
    const VkPipelineLayout a_vk_pipeline_layout,
    uint32_t a_num_shader_stages, 
    const VkPipelineShaderStageCreateInfo* a_shader_stage_create_info_begin
)
{
    if (!vertex_input_descriptions.bindings.empty()) {
        vertex_input_state_create_info.vertexBindingDescriptionCount = static_cast<uint32_t>(vertex_input_descriptions.bindings.size());
        vertex_input_state_create_info.pVertexBindingDescriptions    = vertex_input_descriptions.bindings.data();
    }
    if (!vertex_input_descriptions.attributes.empty()) {
        vertex_input_state_create_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertex_input_descriptions.attributes.size());
        vertex_input_state_create_info.pVertexAttributeDescriptions    = vertex_input_descriptions.attributes.data();
    }
    viewport_state_create_info.viewportCount = std::max(1u, static_cast<uint32_t>(viewports.size()));
    viewport_state_create_info.pViewports    = viewports.empty() ? nullptr : viewports.data();
    viewport_state_create_info.scissorCount  = std::max(1u, static_cast<uint32_t>(scissors.size()));
    viewport_state_create_info.pScissors     = scissors.empty() ? nullptr : scissors.data();

    color_blend_state_create_info.attachmentCount = static_cast<uint32_t>(color_blend_attachments.size());
    color_blend_state_create_info.pAttachments    = color_blend_attachments.empty() ? nullptr : color_blend_attachments.data();

    dynamic_state_create_info.dynamicStateCount = static_cast<uint32_t>(dynamic_states.size());
    dynamic_state_create_info.pDynamicStates = dynamic_states.empty() ? nullptr : dynamic_states.data();

    auto create_info = make_VkGraphicsPipelineCreateInfo();

    create_info.flags               = create_flags;
    create_info.stageCount          = a_num_shader_stages;
    create_info.pStages             = a_shader_stage_create_info_begin;
    create_info.pVertexInputState   = &vertex_input_state_create_info;
    create_info.pInputAssemblyState = &input_assembly_state_create_info;
    create_info.pTessellationState  = &tessellation_state_create_info;
    create_info.pViewportState      = &viewport_state_create_info;
    create_info.pRasterizationState = &rasterization_state_create_info;
    create_info.pMultisampleState   = &multisample_state_create_info;
    create_info.pDepthStencilState  = &depth_stencil_state_create_info;
    create_info.pColorBlendState    = &color_blend_state_create_info;
    create_info.pDynamicState       = &dynamic_state_create_info;
    create_info.layout              = a_vk_pipeline_layout;
    create_info.renderPass          = a_render_pass;
    create_info.subpass             = a_subpass;
    create_info.basePipelineHandle  = base_pipeline_handle;
    create_info.basePipelineIndex   = base_pipeline_index;

    return create_info;
}


void spock::
create_compute_pipelines(
    VkDevice a_vk_dev, 
    uint32_t a_num_create_infos, 
    const VkComputePipelineCreateInfo * a_vk_create_infos, 
    pipeline_t* a_pipeline_begin,
    VkPipelineCache a_pipeline_cache
)
{
    SPOCK_PARANOID(a_num_create_infos != 0, "create_graphics_pipelines: a_num_create_infos cannot be 0");
    SPOCK_PARANOID(a_vk_create_infos != nullptr, "create_compute_pipelines: a_vk_create_infos cannot be NULL");
    SPOCK_PARANOID(a_pipeline_begin != nullptr, "create_compute_pipelines: a_pipelines cannot be NULL");

    std::vector<VkPipeline> vk_pipelines(a_num_create_infos, VK_NULL_HANDLE);
    SPOCK_VK_CALL(vkCreateComputePipelines(a_vk_dev, a_pipeline_cache, a_num_create_infos, a_vk_create_infos, nullptr, vk_pipelines.data()));

    for (size_t i = 0; i < a_num_create_infos; ++i) {
        *(a_pipeline_begin + i) = pipeline_t(a_vk_dev, vk_pipelines[i]);
    }
}


void spock::
create_graphics_pipelines(
    VkDevice a_vk_dev, 
    uint32_t a_num_create_infos, 
    const VkGraphicsPipelineCreateInfo* a_vk_create_infos, 
    pipeline_t* a_pipeline_begin,
    VkPipelineCache a_pipeline_cache
)
{
    SPOCK_PARANOID(a_num_create_infos != 0, "create_graphics_pipelines: a_num_create_infos cannot be 0");
    SPOCK_PARANOID(a_vk_create_infos != nullptr, "create_graphics_pipelines: a_vk_create_infos cannot be NULL");
    SPOCK_PARANOID(a_pipeline_begin != nullptr, "create_graphics_pipelines: a_pipelines cannot be NULL");

    std::vector<VkPipeline> vk_pipelines(a_num_create_infos, VK_NULL_HANDLE);
    SPOCK_VK_CALL(vkCreateGraphicsPipelines(a_vk_dev, a_pipeline_cache, a_num_create_infos, a_vk_create_infos, nullptr, vk_pipelines.data()));
    
    for (size_t i = 0; i < a_num_create_infos; ++i) {
        *(a_pipeline_begin + i) = pipeline_t(a_vk_dev, vk_pipelines[i]);
    }
}


spock::shader_group_handle_pool_t::
shader_group_handle_pool_t(uint32_t a_pipeline_count, const uint32_t* a_shader_group_idx_offsets, uint32_t a_shader_group_handle_size)
    : pipeline_count{ a_pipeline_count }
{
    byte_offsets = std::unique_ptr<uint32_t[]>(new uint32_t[pipeline_count + 1]); // not value-initialized
    for (uint32_t i = 0; i < pipeline_count + 1; ++i) {
        byte_offsets[i] = a_shader_group_idx_offsets[i] * a_shader_group_handle_size;
    }
    data = std::unique_ptr<uint8_t[]>(new uint8_t[data_size_in_bytes()]); // not value-initialized
}


void spock::
create_raytracing_pipelines(
    VkDevice a_vk_dev, 
    uint32_t a_create_info_count, 
    const VkRayTracingPipelineCreateInfoKHR * a_vk_create_infos, 
    const uint32_t* a_group_handle_data_byte_offsets,
    pipeline_t* a_pipeline_begin, 
    uint8_t* a_group_handle_data_begin,
    VkDeferredOperationKHR a_deferred_operation, 
    VkPipelineCache a_pipeline_cache
)
{
    SPOCK_PARANOID(a_create_info_count != 0, "create_graphics_pipelines: a_num_create_infos cannot be 0");
    SPOCK_PARANOID(a_vk_create_infos != nullptr, "create_graphics_pipelines: a_vk_create_infos cannot be NULL");
    SPOCK_PARANOID(a_group_handle_data_byte_offsets != nullptr, "create_graphics_pipelines: a_group_handle_data_byte_offsets cannot be nullptr");
    SPOCK_PARANOID(a_pipeline_begin != nullptr, "create_graphics_pipelines: a_pipelines cannot be NULL");
    SPOCK_PARANOID(a_group_handle_data_begin != nullptr, "create_graphics_pipelines: a_group_handle_data_begin cannot be nullptr");

    std::vector<VkPipeline> vk_pipelines(a_create_info_count, VK_NULL_HANDLE);
    SPOCK_VK_CALL(vkCreateRayTracingPipelinesKHR(
        a_vk_dev, a_deferred_operation, a_pipeline_cache, a_create_info_count, a_vk_create_infos, nullptr, vk_pipelines.data() 
    )); 

    for (size_t i = 0; i < a_create_info_count; ++i) {
        *(a_pipeline_begin + i) = pipeline_t(a_vk_dev, vk_pipelines[i]);
        const auto& create_info = a_vk_create_infos[i];
        uint8_t* data_ptr = a_group_handle_data_begin + a_group_handle_data_byte_offsets[i];
        const auto bytes = a_group_handle_data_byte_offsets[i + 1] - a_group_handle_data_byte_offsets[i];
        SPOCK_VK_CALL(vkGetRayTracingShaderGroupHandlesKHR(a_vk_dev, vk_pipelines[i], 0, create_info.groupCount, bytes, data_ptr));
    }

}


spock::framebuffer_t::
framebuffer_t(framebuffer_t&& a_framebuffer) noexcept
    : vk_dev_           { std::exchange(a_framebuffer.vk_dev_, VK_NULL_HANDLE) }
    , vk_framebuffer_   { std::exchange(a_framebuffer.vk_framebuffer_, VK_NULL_HANDLE) }
{}


spock::framebuffer_t& spock::framebuffer_t::
operator= (framebuffer_t&& a_framebuffer) noexcept
{
    if (this != &a_framebuffer) {
        if (valid()) {
            verbose("move to framebuffer...");
            vkDestroyFramebuffer(vk_dev_, vk_framebuffer_, nullptr);
        }
        vk_dev_           = std::exchange(a_framebuffer.vk_dev_, VK_NULL_HANDLE);
        vk_framebuffer_   = std::exchange(a_framebuffer.vk_framebuffer_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::framebuffer_t::
framebuffer_t(VkDevice a_vk_dev, const VkFramebufferCreateInfo& a_vk_framebuffer_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateFramebuffer(vk_dev_, &a_vk_framebuffer_create_info, nullptr, &vk_framebuffer_));
}


spock::framebuffer_t::
~framebuffer_t()
{
    if (valid()) {
        verbose("destroy framebuffer...");
        vkDestroyFramebuffer(vk_dev_, vk_framebuffer_, nullptr);
    }
}


spock::render_pass_t::
render_pass_t(render_pass_t&& a_render_pass) noexcept
    : vk_dev_           { std::exchange(a_render_pass.vk_dev_, VK_NULL_HANDLE) }
    , vk_render_pass_   { std::exchange(a_render_pass.vk_render_pass_, VK_NULL_HANDLE) }
{}


spock::render_pass_t& spock::render_pass_t::
operator= (render_pass_t&& a_render_pass) noexcept
{
    if (this != &a_render_pass) {
        if (valid()) {
            verbose("move to render pass...");
            vkDestroyRenderPass(vk_dev_, vk_render_pass_, nullptr);
        }
        vk_dev_           = std::exchange(a_render_pass.vk_dev_, VK_NULL_HANDLE);
        vk_render_pass_   = std::exchange(a_render_pass.vk_render_pass_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::render_pass_t::
render_pass_t(VkDevice a_vk_dev, const VkRenderPassCreateInfo& a_vk_render_pass_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateRenderPass(vk_dev_, &a_vk_render_pass_create_info, nullptr, &vk_render_pass_));
}


spock::render_pass_t::
~render_pass_t()
{
    if (valid()) {
        verbose("destroy render pass...");
        vkDestroyRenderPass(vk_dev_, vk_render_pass_, nullptr);
    }
}

spock::impl::alloc_t::
alloc_t() 
    : vk_dev_{ VK_NULL_HANDLE }
    , vma_allocator_{ VK_NULL_HANDLE }
    , vma_allocation_{ VK_NULL_HANDLE }
    , flags_{}
    , mapped_ptr_{ nullptr }
{}


spock::impl::alloc_t::
alloc_t(alloc_t&& a_alloc) noexcept
    : vk_dev_         { std::exchange(a_alloc.vk_dev_, VK_NULL_HANDLE) }
    , vma_allocator_  { std::exchange(a_alloc.vma_allocator_, VK_NULL_HANDLE) }
    , vma_allocation_ { std::exchange(a_alloc.vma_allocation_, VK_NULL_HANDLE) }
    , flags_          { std::exchange(a_alloc.flags_, 0) }
    , mapped_ptr_     { std::exchange(a_alloc.mapped_ptr_, nullptr) }
{
}


spock::impl::alloc_t& spock::impl::alloc_t::
operator= (alloc_t&& a_alloc) noexcept
{
    if (this != &a_alloc) {
        vk_dev_         = std::exchange(a_alloc.vk_dev_, VK_NULL_HANDLE);
        vma_allocator_  = std::exchange(a_alloc.vma_allocator_, VK_NULL_HANDLE);
        vma_allocation_ = std::exchange(a_alloc.vma_allocation_, VK_NULL_HANDLE);
        flags_          = std::exchange(a_alloc.flags_, 0);
        mapped_ptr_     = std::exchange(a_alloc.mapped_ptr_, nullptr);
    }
    return *this;
}


spock::impl::alloc_t::
alloc_t(const context_t& a_context)
    : alloc_t(a_context, a_context)  // since c++11, delegating constructor
{}


spock::impl::alloc_t::
alloc_t(VkDevice a_vk_dev, VmaAllocator a_vma_allocator)
    : vk_dev_{ a_vk_dev }
    , vma_allocator_{ a_vma_allocator }
    , vma_allocation_{ VK_NULL_HANDLE }
    , flags_{}
    , mapped_ptr_{ nullptr }
{}


void spock::impl::alloc_t::
map()
{
    SPOCK_PARANOID(host_visible(), "mapping memory failed: memory is not host visible");
    SPOCK_PARANOID(!mapped(), "mapping memory failed: memory is already mapped");

    SPOCK_VK_CALL(vmaMapMemory(vma_allocator_, vma_allocation_, &mapped_ptr_));
}


void spock::impl::alloc_t::
unmap()
{
    SPOCK_PARANOID(mapped(), "unmapping buffer failed: memory is not mapped");

    vmaUnmapMemory(vma_allocator_, vma_allocation_);
    mapped_ptr_ = nullptr;
}


void spock::impl::alloc_t::
flush_alloc(VkDeviceSize a_offset, VkDeviceSize a_size)
{
    SPOCK_PARANOID(!host_coherent(), "no need to flush host coherent memory");

    SPOCK_VK_CALL(vmaFlushAllocation(vma_allocator_, vma_allocation_, a_offset, a_size));
}


void spock::impl::alloc_t::
invalidate_alloc(VkDeviceSize a_offset, VkDeviceSize a_size)
{
    SPOCK_PARANOID(!host_coherent(), "no need to invalidate host coherent memory");

    SPOCK_VK_CALL(vmaInvalidateAllocation(vma_allocator_, vma_allocation_, a_offset, a_size));
}


spock::buffer_t::
buffer_t(buffer_t&& a_buffer) noexcept
    : impl::alloc_t { std::move(a_buffer) }
    , vk_buffer_{ std::exchange(a_buffer.vk_buffer_, VK_NULL_HANDLE) }
    , bytes_{ std::exchange(a_buffer.bytes_, 0) }
{}


// The Drawbacks of Implementing Move Assignment in Terms of Swap: https://scottmeyers.blogspot.com/2014/06/the-drawbacks-of-implementing-move.html
spock::buffer_t& spock::buffer_t::
operator=(buffer_t&& a_buffer) noexcept
{
    if (this != &a_buffer) {
        if (valid()) {
            verbose("move assign to buffer...");
            // If a memory object is mapped at the time it is freed, it is implicitly unmapped. (vkspec-1.2.187: 11.2.11. Freeing Device Memory, p.734)
            // vma requires unmap first
            if (mapped()) unmap();
            vmaDestroyBuffer(vma_allocator_, vk_buffer_, vma_allocation_);
        }
        impl::alloc_t::operator=(std::move(a_buffer));
        vk_buffer_ = std::exchange(a_buffer.vk_buffer_, VK_NULL_HANDLE);
        bytes_     = std::exchange(a_buffer.bytes_, 0);
    }
    return *this;
}


spock::buffer_t::
buffer_t
(
    const context_t& a_context,
    const VkBufferCreateInfo& a_buffer_create_info,
    const VmaAllocationCreateInfo& a_vma_alloc_create_info
)
    : buffer_t(a_context, a_context, a_buffer_create_info, a_vma_alloc_create_info) // since c++11, delegating constructor
{}


spock::buffer_t::
buffer_t
(
    VkDevice a_vk_dev, 
    VmaAllocator a_vma_allocator, 
    const VkBufferCreateInfo& a_buffer_create_info,
    const VmaAllocationCreateInfo& a_vma_alloc_create_info
)
    : impl::alloc_t (a_vk_dev, a_vma_allocator)
    , vk_buffer_{ VK_NULL_HANDLE }
{
    VmaAllocationInfo vma_allocation_info{};
    SPOCK_VK_CALL(vmaCreateBuffer(vma_allocator_, &a_buffer_create_info, &a_vma_alloc_create_info, &vk_buffer_, &vma_allocation_, &vma_allocation_info));
    bytes_ = a_buffer_create_info.size;
    vmaGetMemoryTypeProperties(vma_allocator_, vma_allocation_info.memoryType, &flags_);
}


spock::buffer_t::
~buffer_t()
{
    if (valid()) {
        // If a memory object is mapped at the time it is freed, it is implicitly unmapped. (vkspec-1.2.187: 11.2.11. Freeing Device Memory, p.734)
        // vma requires unmap first
        if (mapped()) unmap();
        verbose("destroy buffer..."); 
        vmaDestroyBuffer(vma_allocator_, vk_buffer_, vma_allocation_);
    }
}


std::pair<VmaAllocation,VkBuffer> spock::buffer_t::
release()
{
    auto ret = std::make_pair(vma_allocation_, vk_buffer_);
    impl::alloc_t::operator=(impl::alloc_t{});
    vk_buffer_ = VK_NULL_HANDLE;
    bytes_ = 0;
    return ret;
}


auto spock::buffer_t::
cmd_upload(VkCommandBuffer a_command_buffer, const void * a_src) -> buffer_t
{
    auto staging_buffer = stage(vk_dev_, vma_allocator_, a_src, bytes_);
    VkBufferCopy buffer_copy{};
    buffer_copy.size = static_cast<VkDeviceSize>(bytes_);
    vkCmdCopyBuffer(a_command_buffer, staging_buffer, vk_buffer_, 1, &buffer_copy);
    return staging_buffer;
}


spock::buffer_view_t::
buffer_view_t(buffer_view_t&& a_buffer_view) noexcept
    : vk_dev_           { std::exchange(a_buffer_view.vk_dev_, VK_NULL_HANDLE) }
    , vk_buffer_view_    { std::exchange(a_buffer_view.vk_buffer_view_, VK_NULL_HANDLE) }
{}

spock::buffer_view_t& spock::buffer_view_t::operator=(buffer_view_t&& a_buffer_view) noexcept
{
    if (this != &a_buffer_view) {
        if (valid()) {
            verbose("destroy buffer view...");
            vkDestroyBufferView(vk_dev_, vk_buffer_view_, nullptr);
        }
        vk_dev_           = std::exchange(a_buffer_view.vk_dev_, VK_NULL_HANDLE);
        vk_buffer_view_    = std::exchange(a_buffer_view.vk_buffer_view_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::buffer_view_t::
buffer_view_t(VkDevice a_vk_dev, const VkBufferViewCreateInfo& a_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateBufferView(vk_dev_, &a_create_info, nullptr, &vk_buffer_view_));
}


spock::buffer_view_t::
buffer_view_t(VkDevice a_vk_dev, buffer_t const * a_buffer, VkFormat a_vk_format)
    : vk_dev_       { a_vk_dev }
{
    auto vk_buffer_view_create_info = make_VkBufferViewCreateInfo();
    vk_buffer_view_create_info.buffer = *a_buffer;
    vk_buffer_view_create_info.format = a_vk_format;
    vk_buffer_view_create_info.offset = 0;
    vk_buffer_view_create_info.range = VK_WHOLE_SIZE;

    SPOCK_VK_CALL(vkCreateBufferView(vk_dev_, &vk_buffer_view_create_info, nullptr, &vk_buffer_view_));
}


spock::buffer_view_t::~buffer_view_t()
{
    if (valid()) {
        verbose("destroy buffer view...");
        vkDestroyBufferView(vk_dev_, vk_buffer_view_, nullptr);
    }
}


VkBufferView spock::buffer_view_t::release()
{
    auto ret = vk_buffer_view_;
    vk_dev_ = VK_NULL_HANDLE;
    vk_buffer_view_ = VK_NULL_HANDLE;
    return ret;
}

spock::image_t::
image_t(image_t&& a_image)  noexcept
    : impl::alloc_t   { std::move(a_image) }
    , vk_image_       { std::exchange(a_image.vk_image_, VK_NULL_HANDLE) }
    , format_         { std::exchange(a_image.format_, VK_FORMAT_UNDEFINED) }
    , extent_         { std::exchange(a_image.extent_, VkExtent3D{}) }
    , num_mip_levels_ { std::exchange(a_image.num_mip_levels_, 0) }
{}


spock::image_t& spock::image_t::
operator=(image_t&& a_image)  noexcept
{
    if (this != &a_image) {
        if (valid()) {
            verbose("move assign to image...");
            // If a memory object is mapped at the time it is freed, it is implicitly unmapped. (vkspec-1.2.187: 11.2.11. Freeing Device Memory, p.734)
            // vma requires unmap first
            if (mapped()) unmap();
            vmaDestroyImage(vma_allocator_, vk_image_, vma_allocation_);
        }
        impl::alloc_t::operator=(std::move(a_image));
        vk_image_              = std::exchange(a_image.vk_image_, VK_NULL_HANDLE) ;
        format_                = std::exchange(a_image.format_, VK_FORMAT_UNDEFINED) ;
        extent_                = std::exchange(a_image.extent_, VkExtent3D{}) ;
        num_mip_levels_        = std::exchange(a_image.num_mip_levels_, 0) ;
    }
    return *this;
}


spock::image_t::
image_t
(
    const context_t& a_context,
    const VkImageCreateInfo& a_image_create_info,
    const VmaAllocationCreateInfo& a_alloc_create_info
)
    : image_t(a_context, a_context, a_image_create_info, a_alloc_create_info) // since c++11, delegating constructor 
{}


spock::image_t::
image_t
(
    VkDevice a_vk_dev, 
    VmaAllocator a_vma_allocator, 
    const VkImageCreateInfo & a_image_create_info, 
    const VmaAllocationCreateInfo & a_alloc_create_info
)
    : impl::alloc_t { a_vk_dev, a_vma_allocator}
{
    VmaAllocationInfo vma_allocation_info{};
    SPOCK_VK_CALL(vmaCreateImage(vma_allocator_, &a_image_create_info, &a_alloc_create_info, &vk_image_, &vma_allocation_, &vma_allocation_info));

    format_         = a_image_create_info.format;
    extent_         = a_image_create_info.extent;
    num_mip_levels_ = a_image_create_info.mipLevels;

    vmaGetMemoryTypeProperties(vma_allocator_, vma_allocation_info.memoryType, &flags_);
}


spock::image_t::
~image_t()
{
    if (valid()) {
        verbose("destroy image...");
        // If a memory object is mapped at the time it is freed, it is implicitly unmapped. (vkspec-1.2.187: 11.2.11. Freeing Device Memory, p.734)
        // vma requires unmap first
        if (mapped()) unmap();
        vmaDestroyImage(vma_allocator_, vk_image_, vma_allocation_);
    }
}


std::pair<VmaAllocation,VkImage> spock::image_t::release()
{
    auto ret = std::make_pair(vma_allocation_, vk_image_);
    impl::alloc_t::operator=(impl::alloc_t{});
    vk_image_              = VK_NULL_HANDLE;
    format_                = VK_FORMAT_UNDEFINED;
    extent_                = {};
    num_mip_levels_        = 0;
    return ret;
}


auto spock::image_t::
cmd_upload(
    VkCommandBuffer a_command_buffer,
    const void * a_src, 
    size_t a_bytes,
    const VkImageSubresourceLayers& a_image_subresource_layers,
    VkImageLayout a_final_image_layout,
    VkPipelineStageFlags a_next_stages,
    VkAccessFlags a_next_accesses
) const -> buffer_t
{
    auto staging_buffer = stage(vk_dev_, vma_allocator_, a_src, a_bytes);

    auto image_memory_barrier = make_VkImageMemoryBarrier();
    image_memory_barrier.srcAccessMask                   = 0;
    image_memory_barrier.dstAccessMask                   = VK_ACCESS_TRANSFER_WRITE_BIT;
    image_memory_barrier.oldLayout                       = VK_IMAGE_LAYOUT_UNDEFINED;
    image_memory_barrier.newLayout                       = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    image_memory_barrier.image                           = *this;
    image_memory_barrier.subresourceRange.aspectMask     = a_image_subresource_layers.aspectMask;
    image_memory_barrier.subresourceRange.baseMipLevel   = a_image_subresource_layers.mipLevel;
    image_memory_barrier.subresourceRange.levelCount     = num_mip_levels_;
    image_memory_barrier.subresourceRange.baseArrayLayer = a_image_subresource_layers.baseArrayLayer;
    image_memory_barrier.subresourceRange.layerCount     = a_image_subresource_layers.layerCount;

    if (a_image_subresource_layers.mipLevel == 0) {
        vkCmdPipelineBarrier(
            a_command_buffer,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0, nullptr,
            0, nullptr,
            1, &image_memory_barrier
        );
    }

    VkBufferImageCopy buffer_image_copy{};
    buffer_image_copy.bufferOffset      = 0;
    buffer_image_copy.bufferRowLength   = 0;
    buffer_image_copy.bufferImageHeight = 0;
    buffer_image_copy.imageSubresource  = a_image_subresource_layers;
    buffer_image_copy.imageOffset       = { 0, 0, 0 };
    buffer_image_copy.imageExtent       = extent_;

    vkCmdCopyBufferToImage(
        a_command_buffer, 
        staging_buffer, *this, 
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
        1, &buffer_image_copy
    );
    
    // reuse image_memory_barrier.image and part of image_memory_barrier.subresourceRange
    image_memory_barrier.subresourceRange.levelCount = 1;
    image_memory_barrier.srcAccessMask               = VK_ACCESS_TRANSFER_WRITE_BIT;
    image_memory_barrier.dstAccessMask               = a_next_accesses;
    image_memory_barrier.oldLayout                   = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    image_memory_barrier.newLayout                   = a_final_image_layout;

    vkCmdPipelineBarrier(
        a_command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        a_next_stages,
        0,
        0, nullptr,
        0, nullptr,
        1, &image_memory_barrier
    );

    return staging_buffer;
}


/// assume mipmap 0 is in VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, the rest in VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
/// return extent of all mip levels 
std::vector<VkExtent3D> spock::image_t::
cmd_gen_mipmaps_from_mip0(
    VkCommandBuffer a_command_buffer, 
    VkImageLayout a_final_image_layout,
    VkPipelineStageFlags a_next_stages,
    VkAccessFlags a_next_accesses
) const
{
    const auto num_mips = num_mip_levels_;

    if (num_mips == 1) {
        warn("There is only one mip level; no need to generate mipmaps");
        return std::vector<VkExtent3D>{ extent_ };
    }

    uint32_t mip_width = extent_.width;
    uint32_t mip_height = extent_.height;
    uint32_t mip_depth = extent_.depth;

    std::vector<VkExtent3D> extents;
    extents.reserve(num_mips);
    extents.push_back(extent_);

    auto barrier = make_VkImageMemoryBarrier();
    barrier.image                           = vk_image_;
    barrier.srcQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount     = 1;
    barrier.subresourceRange.levelCount     = 1;

    VkImageBlit image_blit{};
    image_blit.srcOffsets[0]                 = { 0, 0, 0 };
    image_blit.srcSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    image_blit.srcSubresource.baseArrayLayer = 0;
    image_blit.srcSubresource.layerCount     = 1;
    image_blit.dstOffsets[0]                 = { 0, 0, 0 };
    image_blit.dstSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    image_blit.dstSubresource.baseArrayLayer = 0;
    image_blit.dstSubresource.layerCount     = 1;

    for (uint32_t i = 1; i < num_mips; ++i) {
        image_blit.srcOffsets[1]           = { (int32_t)mip_width, (int32_t)mip_height, (int32_t)mip_depth };
        image_blit.srcSubresource.mipLevel = i - 1;

        mip_width = std::max(mip_width >> 1, 1u);
        mip_height = std::max(mip_height >> 1, 1u);
        mip_depth = std::max(mip_depth >> 1, 1u);
        extents.push_back(VkExtent3D{ mip_width, mip_height, mip_depth });

        image_blit.dstOffsets[1]           = { (int32_t)mip_width, (int32_t)mip_height, (int32_t)mip_depth };
        image_blit.dstSubresource.mipLevel = i;

        vkCmdBlitImage(a_command_buffer,
            vk_image_, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            vk_image_, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1, &image_blit,
            VK_FILTER_LINEAR);

        if (a_final_image_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
            barrier.subresourceRange.baseMipLevel = i - 1;
            barrier.oldLayout                     = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.newLayout                     = a_final_image_layout;
            barrier.srcAccessMask                 = VK_ACCESS_TRANSFER_READ_BIT;
            barrier.dstAccessMask                 = a_next_accesses;

            vkCmdPipelineBarrier(
                a_command_buffer,
                VK_PIPELINE_STAGE_TRANSFER_BIT, a_next_stages, 0,
                0, nullptr,
                0, nullptr,
                1, &barrier
            );
        }

        VkImageLayout mip_i_new_layout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        VkPipelineStageFlags mip_i_dst_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        VkAccessFlags mip_i_dst_access_mask = VK_ACCESS_TRANSFER_READ_BIT;

        if (i == num_mips - 1) {
            mip_i_new_layout = a_final_image_layout;
            mip_i_dst_stage = a_next_stages;
            mip_i_dst_access_mask = a_next_accesses;
        }

        barrier.subresourceRange.baseMipLevel = i;
        barrier.oldLayout                     = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout                     = mip_i_new_layout;
        barrier.srcAccessMask                 = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask                 = mip_i_dst_access_mask;

        vkCmdPipelineBarrier(
            a_command_buffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT, mip_i_dst_stage, 0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }
    return extents;
}


void spock::image_t::
cmd_blit_mip0_to_image(
    VkCommandBuffer a_command_buffer, 
    image_t& a_dst_image, 
    VkImageLayout a_dst_final_image_layout, VkPipelineStageFlags a_dst_next_stages, VkAccessFlags a_dst_next_accesses
) const
{
    SPOCK_PARANOID(a_dst_image.valid(), "dst image is not valid");
    SPOCK_PARANOID(a_dst_image.extent() == extent_, "src and dst image extent do not match");

    auto image_memory_barrier = make_VkImageMemoryBarrier();
    image_memory_barrier.srcAccessMask = 0;
    image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    image_memory_barrier.image = a_dst_image;
    image_memory_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_memory_barrier.subresourceRange.baseMipLevel = 0;
    image_memory_barrier.subresourceRange.levelCount = a_dst_image.num_mip_levels();
    image_memory_barrier.subresourceRange.baseArrayLayer = 0;
    image_memory_barrier.subresourceRange.layerCount = 1;

    vkCmdPipelineBarrier(
        a_command_buffer,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        1, &image_memory_barrier
    );

    VkImageBlit image_blit{};
    image_blit.srcOffsets[0]                 = { 0, 0, 0 };
    image_blit.srcOffsets[1]                 = to_offset_3d(extent_);
    image_blit.srcSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    image_blit.srcSubresource.mipLevel       = 0;
    image_blit.srcSubresource.baseArrayLayer = 0;
    image_blit.srcSubresource.layerCount     = 1;
    image_blit.dstOffsets[0]                 = { 0, 0, 0 };
    image_blit.dstOffsets[1]                 = to_offset_3d(a_dst_image.extent());
    image_blit.dstSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
    image_blit.dstSubresource.mipLevel       = 0;
    image_blit.dstSubresource.baseArrayLayer = 0;
    image_blit.dstSubresource.layerCount     = 1;

    vkCmdBlitImage(a_command_buffer,
        vk_image_, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        a_dst_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1, &image_blit,
        VK_FILTER_NEAREST
    );

    image_memory_barrier.subresourceRange.levelCount = 1; // only mip0 of dst_image can be transfer src
    image_memory_barrier.srcAccessMask               = VK_ACCESS_TRANSFER_WRITE_BIT;
    image_memory_barrier.dstAccessMask               = a_dst_next_accesses;
    image_memory_barrier.oldLayout                   = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    image_memory_barrier.newLayout                   = a_dst_final_image_layout;

    vkCmdPipelineBarrier(
        a_command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        a_dst_next_stages,
        0,
        0, nullptr,
        0, nullptr,
        1, &image_memory_barrier
    );

}


auto spock::image_t::
cmd_copy_to_host_visible_buffer(
    VkCommandBuffer a_command_buffer, 
    VkDeviceSize a_texel_block_bytes, 
    bool a_mip_0_only
) const -> buffer_t 
{
    uint32_t mip_level_max = 0;
    std::vector<VkDeviceSize> buffer_offsets;
    std::vector<VkExtent3D> extents;
    if (a_mip_0_only) {
        buffer_offsets.push_back(0);
        extents.push_back(VkExtent3D{});
    }
    else {
        mip_level_max = num_mip_levels_ - 1;
        buffer_offsets.resize(num_mip_levels_, 0);
        extents.resize(num_mip_levels_, VkExtent3D{});
    }
    VkDeviceSize buffer_bytes = 0;
    for (size_t i = 0; i <= mip_level_max; ++i) {
        uint32_t w = std::max(1u, extent_.width >> i);
        uint32_t h = std::max(1u, extent_.height >> i);
        uint32_t d = std::max(1u, extent_.depth >> i);
        buffer_offsets[i] = buffer_bytes;
        buffer_bytes += d * h * w * a_texel_block_bytes;
        extents[i] = { w, h, d };
    }

    auto buffer_create_info = make_VkBufferCreateInfo();
    buffer_create_info.size = static_cast<VkDeviceSize>(buffer_bytes);
    buffer_create_info.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    VmaAllocationCreateInfo buffer_allocation_create_info{};
    buffer_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
    auto buffer = buffer_t(vk_dev_, vma_allocator_, buffer_create_info, buffer_allocation_create_info);

    std::vector<VkBufferImageCopy> buffer_image_copy_regions(mip_level_max + 1, VkBufferImageCopy{});
    for (uint32_t i = 0; i <= mip_level_max; ++i) {
        buffer_image_copy_regions[i].bufferOffset = buffer_offsets[i];
        buffer_image_copy_regions[i].bufferRowLength = extents[i].width;
        buffer_image_copy_regions[i].bufferImageHeight = extents[i].height;
        buffer_image_copy_regions[i].imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        buffer_image_copy_regions[i].imageSubresource.mipLevel = i;
        buffer_image_copy_regions[i].imageSubresource.baseArrayLayer = 0;
        buffer_image_copy_regions[i].imageSubresource.layerCount = 1;
        buffer_image_copy_regions[i].imageOffset = { 0, 0, 0 };
        buffer_image_copy_regions[i].imageExtent = extents[i];
    }
    vkCmdCopyImageToBuffer(
        a_command_buffer,
        *this, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        buffer,
        mip_level_max + 1, buffer_image_copy_regions.data()
    );

    return buffer;
}


spock::image_view_t::
image_view_t(image_view_t&& a_other) noexcept
    : vk_dev_{ std::exchange(a_other.vk_dev_, VK_NULL_HANDLE) }
    , vk_image_view_{ std::exchange(a_other.vk_image_view_, VK_NULL_HANDLE) }
    , vk_image_subresource_range_{ std::exchange(a_other.vk_image_subresource_range_, VkImageSubresourceRange{}) }
{
}


spock::image_view_t& spock::image_view_t::
operator= (image_view_t&& a_other) noexcept
{
    if (this != &a_other) {
        if (valid()) {
            verbose("destroy image view...");
            vkDestroyImageView(vk_dev_, vk_image_view_, nullptr);
        }
        vk_dev_                    = std::exchange(a_other.vk_dev_, nullptr);
        vk_image_view_             = std::exchange(a_other.vk_image_view_, VK_NULL_HANDLE);
        vk_image_subresource_range_= std::exchange(a_other.vk_image_subresource_range_, VkImageSubresourceRange{});
    }
    return *this;
}


spock::image_view_t::
image_view_t(VkDevice a_vk_dev, const VkImageViewCreateInfo& a_create_info)
    : vk_dev_{ a_vk_dev }
    , vk_image_view_{ VK_NULL_HANDLE }
    , vk_image_subresource_range_{ a_create_info.subresourceRange }
{
        SPOCK_VK_CALL(vkCreateImageView(vk_dev_, &a_create_info, nullptr, &vk_image_view_));
}


spock::image_view_t::
~image_view_t()
{
    if (valid()) {
        verbose("destroy image view...");
        vkDestroyImageView(vk_dev_, vk_image_view_, nullptr);
    }
}


VkImageView spock::image_view_t::
release()
{
    return std::exchange(vk_image_view_, VK_NULL_HANDLE);
}

spock::sampler_t::
sampler_t(sampler_t&& a_sampler) noexcept
    : vk_dev_{ std::exchange(a_sampler.vk_dev_, VK_NULL_HANDLE) }
    , vk_sampler_{ std::exchange(a_sampler.vk_sampler_, VK_NULL_HANDLE) }
{}


spock::sampler_t& spock::sampler_t::
 operator= (sampler_t&& a_sampler) noexcept
{
    if (this != &a_sampler) {
        if (valid()) {
            verbose("move assign to sampler...");
            vkDestroySampler(vk_dev_, vk_sampler_, nullptr);
        }
        vk_dev_ = std::exchange(a_sampler.vk_dev_, VK_NULL_HANDLE);
        vk_sampler_ = std::exchange(a_sampler.vk_sampler_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::sampler_t::sampler_t(VkDevice a_vk_dev)
    : vk_dev_{ a_vk_dev }
{
    auto sampler_create_info = dummy_VkSamplerCreateInfo();
    SPOCK_VK_CALL(vkCreateSampler(vk_dev_, &sampler_create_info, nullptr, &vk_sampler_));
}


spock::sampler_t::
sampler_t(VkDevice a_vk_dev, const VkSamplerCreateInfo& a_vk_sampler_create_info)
    : vk_dev_       { a_vk_dev }
    , vk_sampler_   { VK_NULL_HANDLE }
{
    SPOCK_VK_CALL(vkCreateSampler(vk_dev_, &a_vk_sampler_create_info, nullptr, &vk_sampler_));
}


spock::sampler_t::
sampler_t(const context_t & a_context, VkFilter a_filter, VkSamplerAddressMode a_addr_mode)
    : vk_dev_{ a_context }
    , vk_sampler_{ VK_NULL_HANDLE }
{
    auto sampler_create_info = build_VkSamplerCreateInfo(a_context.max_sampler_anisotropy(), a_filter, a_addr_mode);
    SPOCK_VK_CALL(vkCreateSampler(vk_dev_, &sampler_create_info, nullptr, &vk_sampler_));
}


spock::sampler_t::
~sampler_t()
{
    if (valid()) {
        verbose("destroy sampler...");
        vkDestroySampler(vk_dev_, vk_sampler_, nullptr);
    }
}


bool spock::swapchain_t::
choose_surface_format()
{
    uint32_t num_surface_formats = 0;
    auto ret = vkGetPhysicalDeviceSurfaceFormatsKHR(context_->vk_physical_dev(), context_->vk_surface(), &num_surface_formats, nullptr);
    if (ret != VK_SUCCESS) {
        err("swapchain_t::choose_surface_format: vkGetPhysicalDeviceSurfaceFormatsKHR failed: " + std::string(str_from_VkResult(ret)));
        return false;
    }
    if (num_surface_formats == 0) {
        err("no surface format supported");
        return false;
    }
    std::vector<VkSurfaceFormatKHR> surface_formats(num_surface_formats);
    ret = vkGetPhysicalDeviceSurfaceFormatsKHR(context_->vk_physical_dev(), context_->vk_surface(), &num_surface_formats, surface_formats.data());
    if (ret != VK_SUCCESS) {
        err("swapchain_t::choose_surface_format: vkGetPhysicalDeviceSurfaceFormatsKHR failed: " + std::string(str_from_VkResult(ret)));
        return false;
    }
    const auto& config_json = context_->config_json()["swapchain"];

    if (config_json.contains("formats") && config_json.contains("color_spaces")) {
        const auto& formats_cfg_array = config_json["formats"];
        const auto& color_spaces_cfg_array = config_json["color_spaces"];

        for (const auto& format_cfg : formats_cfg_array) {
            for (const auto& color_space_cfg : color_spaces_cfg_array) {
                bool format_cfg_ok = true;
                bool color_space_cfg_ok = true;
                const auto format = str_to_VkFormat(format_cfg.get<std::string_view>(), &format_cfg_ok);
                auto color_space = str_to_VkColorSpaceKHR(color_space_cfg.get<std::string_view>(), &color_space_cfg_ok);
                if (!format_cfg_ok || !color_space_cfg_ok) {
                    err("swapchain_t::choose_surface_format: invalid format or color space config");
                    return false;
                }
                for (const auto& surface_format : surface_formats) {
                    if (format == surface_format.format && color_space == surface_format.colorSpace) {
                        surface_format_ = surface_format;
                        return true;
                    }
                }
            }
        }
        warn("failed to find any of the requested surface formats, fallback to the first available");
    } // config_json.contains("formats") && config_json.contains("color_spaces")

    if (config_json.contains("color_spaces")) {
        const auto& color_spaces_cfg_array = config_json["color_spaces"];
        for (const auto& color_space_cfg : color_spaces_cfg_array) {
            bool color_space_cfg_ok = true;
            auto color_space = str_to_VkColorSpaceKHR(color_space_cfg.get<std::string_view>(), &color_space_cfg_ok);
            if (!color_space_cfg_ok) {
                err("swapchain_t::choose_surface_format: invalid color space config");
                return false;
            }
            for (const auto& surface_format : surface_formats) {
                if (color_space == surface_format.colorSpace) {
                    surface_format_ = surface_format;
                    return true;
                }
            }
        }
        warn("failed to find any of the requested surface formats, fallback to the first available");
    }

    if (config_json.contains("formats")) {
        const auto& formats_cfg_array = config_json["formats"];
        for (const auto& format_cfg : formats_cfg_array) {
            bool format_cfg_ok = true;
            const auto format = str_to_VkFormat(format_cfg.get<std::string_view>(), &format_cfg_ok);
            if (!format_cfg_ok) {
                err("swapchain_t::choose_surface_format: invalid format config");
                return false;
            }
            for (const auto& surface_format : surface_formats) {
                if (format == surface_format.format) {
                    surface_format_ = surface_format;
                    return true;
                }
            }
        }
        warn("failed to find any of the requested surface formats, fallback to the first available");
    }
    surface_format_ = surface_formats[0];
    return true;
}


bool spock::swapchain_t::
choose_present_mode()
{
    uint32_t num_present_modes = 0;
    auto ret = vkGetPhysicalDeviceSurfacePresentModesKHR(context_->vk_physical_dev(), context_->vk_surface(), &num_present_modes, nullptr);
    if (ret != VK_SUCCESS) {
        err("swapchain_t::choose_present_mode: vkGetPhysicalDeviceSurfacePresentModesKHR failed: " + std::string(str_from_VkResult(ret)));
        return false;
    }
    if (num_present_modes == 0) {
        err("swapchain_t::choose_present_mode: no present mode supported");
        return false;
    }
    std::vector<VkPresentModeKHR> present_modes(num_present_modes);
    ret = vkGetPhysicalDeviceSurfacePresentModesKHR(context_->vk_physical_dev(), context_->vk_surface(), &num_present_modes, present_modes.data());
    if (ret != VK_SUCCESS) {
        err("swapchain_t::choose_present_mode: vkGetPhysicalDeviceSurfacePresentModesKHR failed: " + std::string(str_from_VkResult(ret)));
        return false;
    }

    const auto& config_json = context_->config_json()["swapchain"];

    if (config_json.contains("present_modes")) {
        const auto& present_modes_cfg_array = config_json["present_modes"];
        for (const auto& present_mode_cfg : present_modes_cfg_array) {
            bool present_mode_cfg_ok = true;
            const auto requested_present_mode = str_to_VkPresentModeKHR(present_mode_cfg.get<std::string_view>(), &present_mode_cfg_ok);
            if (!present_mode_cfg_ok) {
                err("swapchain_t::choose_present_mode: invalid present mode config");
                return false;
            }
            for (const auto& present_mode : present_modes) {
                if (present_mode == requested_present_mode) {
                    present_mode_ = present_mode;
                    return true;
                }
            }
        }
        warn("failed to find any of the requested present modes, fallback to the first available");
    }
    present_mode_ = present_modes[0];
    return true;
}


bool spock::swapchain_t::
choose_image_extent(const VkSurfaceCapabilitiesKHR& a_surface_capabilities, const VkExtent2D& a_extent)
{
    if (a_surface_capabilities.currentExtent.width != UINT32_MAX) {
        image_extent_ = a_surface_capabilities.currentExtent;
    }
    else {
        image_extent_ = VkExtent2D{
            std::clamp(
                static_cast<uint32_t>(a_extent.width),
                a_surface_capabilities.minImageExtent.width,
                a_surface_capabilities.maxImageExtent.width
            ),
            std::clamp(
                static_cast<uint32_t>(a_extent.height),
                a_surface_capabilities.minImageExtent.height,
                a_surface_capabilities.maxImageExtent.height
            )
        };
    }
    if (a_extent.width != image_extent_.width || a_extent.height != image_extent_.height) {
        err("swapchain_t::choose_image_extent: framebuffer and swapchain size mismatch");
        return false;
    }
    return true;
}


spock::swapchain_t::
swapchain_t(swapchain_t&& a_other) noexcept
    : context_{ std::exchange(a_other.context_, nullptr) }
    , vk_swapchain_{ std::exchange(a_other.vk_swapchain_, VK_NULL_HANDLE) }
    , surface_format_{ a_other.surface_format_ }
    , present_mode_{ a_other.present_mode_ }
    , image_count_{ a_other.image_count_ }
    , image_extent_{ a_other.image_extent_ }
    , vk_images_{ std::move(a_other.vk_images_) }
    , vk_image_views_{ std::move(a_other.vk_image_views_) }
{}


spock::swapchain_t& spock::swapchain_t::
operator=(swapchain_t&& a_other) noexcept
{
    if (this != &a_other) {
        if (valid()) {
            verbose("move assign to swapchain...");
            for (uint32_t i = 0; i < image_count_; ++i) {
                vkDestroyImageView(*context_, vk_image_views_[i], nullptr);
            }
            vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
        }
        context_ = std::exchange(a_other.context_, nullptr);
        vk_swapchain_ = std::exchange(a_other.vk_swapchain_, VK_NULL_HANDLE);
        surface_format_ = a_other.surface_format_;
        present_mode_ = a_other.present_mode_;
        image_count_ = a_other.image_count_;
        image_extent_ = a_other.image_extent_;
        vk_images_ = std::move(a_other.vk_images_);
        vk_image_views_ = std::move(a_other.vk_image_views_);
    }
    return *this;
}


spock::swapchain_t::
swapchain_t(const context_t& a_context, const VkExtent2D& a_extent)
    : context_{ &a_context }
    , vk_swapchain_{ VK_NULL_HANDLE }
    , surface_format_{}
    , present_mode_{}
    , image_extent_{}
    , image_count_{}
{
    SPOCK_ENSURE(resize(a_extent), "failed to construct swapchain");
}


spock::swapchain_t::
~swapchain_t()
{
    if (valid()) {
        verbose("destroy swapchain image views...");
        for (uint32_t i = 0; i < image_count_; ++i) {
            vkDestroyImageView(*context_, vk_image_views_[i], nullptr);
        }
        verbose("destroy swapchain...");
        vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
    }
}


bool spock::swapchain_t::
resize(const VkExtent2D& a_extent)
{
    if (!choose_surface_format() || !choose_present_mode()) {
        return false;
    }
    for (uint32_t i = 0; i < image_count_; ++i) {
        vkDestroyImageView(*context_, vk_image_views_[i], nullptr);
    }
    const auto& config_json = context_->config_json()["swapchain"];
    if (config_json.contains("num_images")) {
        image_count_ = config_json["num_images"].get<uint32_t>();
    }

    VkSurfaceCapabilitiesKHR surface_capabilities;
    auto ret = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(context_->vk_physical_dev(), context_->vk_surface(), &surface_capabilities);
    if (ret != VK_SUCCESS) {
        err("swapchain_t::resize: vkGetPhysicalDeviceSurfaceCapabilitiesKHR failed: " + std::string(str_from_VkResult(ret)));
        return false;
    }
    image_count_ = std::max(image_count_, surface_capabilities.minImageCount + 1);

    if (surface_capabilities.maxImageCount > 0) {
        image_count_ = std::min(image_count_, surface_capabilities.maxImageCount);
    }
    if (!choose_image_extent(surface_capabilities, a_extent)) {
        return false;
    }
    // not all devices support alpha opaque
    VkCompositeAlphaFlagBitsKHR composite_alpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    constexpr VkCompositeAlphaFlagBitsKHR composite_alpha_flags[] = {
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
        VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
        VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
    };
    for (const auto flag : composite_alpha_flags) {
        if (surface_capabilities.supportedCompositeAlpha & flag) {
            composite_alpha = flag;
            break;
        }
    }

    VkImageUsageFlags image_usages = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    if (surface_capabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_SRC_BIT) {
        image_usages |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    }
    if (surface_capabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT) {
        image_usages |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    }

    VkSwapchainKHR vk_old_swapchain = vk_swapchain_;
    auto vk_swapchain_create_info = make_VkSwapchainCreateInfoKHR();
    vk_swapchain_create_info.flags = 0;
    vk_swapchain_create_info.surface = context_->vk_surface();
    vk_swapchain_create_info.minImageCount = image_count_;
    vk_swapchain_create_info.imageFormat = surface_format_.format;
    vk_swapchain_create_info.imageColorSpace = surface_format_.colorSpace;
    vk_swapchain_create_info.imageExtent = image_extent_;
    vk_swapchain_create_info.imageArrayLayers = 1;
    vk_swapchain_create_info.imageUsage = image_usages;
    vk_swapchain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; // TODO: relax the assumption that graphics queue can always present
    vk_swapchain_create_info.preTransform = surface_capabilities.currentTransform;
    vk_swapchain_create_info.compositeAlpha = composite_alpha;
    vk_swapchain_create_info.presentMode = present_mode_;
    vk_swapchain_create_info.clipped = VK_TRUE;
    vk_swapchain_create_info.oldSwapchain = vk_old_swapchain;

    ret = vkCreateSwapchainKHR(*context_, &vk_swapchain_create_info, nullptr, &vk_swapchain_);
    if (ret != VK_SUCCESS) {
        err(std::string("swapchain_t::resize: vkCreateSwapchainKHR failed: ") + std::string(str_from_VkResult(ret)));
        return false;
    }
    uint32_t num_vk_images = 0;
    ret = vkGetSwapchainImagesKHR(*context_, vk_swapchain_, &num_vk_images, nullptr);
    if (ret != VK_SUCCESS || num_vk_images == 0) {
        err(std::string("swapchain_t::resize: no available swapchain images or vkGetSwapchainImagesKHR failed: ") + std::string(str_from_VkResult(ret)));
        vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
        return false;
    }
    vk_images_ = std::unique_ptr<VkImage[]>(new(std::nothrow) VkImage[num_vk_images]); // force new to return nullptr if fails instead of throw
    if (vk_images_.get() == nullptr) {
        err(std::string("swapchain_t::resize: failed to allocate storage for vk_images_"));
        vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
        return false;
    }
    ret = vkGetSwapchainImagesKHR(*context_, vk_swapchain_, &num_vk_images, vk_images_.get());
    if (ret != VK_SUCCESS || num_vk_images == 0) {
        err(std::string("swapchain_t::resize: vkGetSwapchainImagesKHR failed: ") + std::string(str_from_VkResult(ret)));
        vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
        return false;
    }
    if (vk_old_swapchain != VK_NULL_HANDLE) {
        ret = vkDeviceWaitIdle(*context_);
        if (ret != VK_SUCCESS) {
            err(std::string("swapchain_t::resize: vkGetSwapchainImagesKHR failed: ") + std::string(str_from_VkResult(ret)));
            vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
            vkDestroySwapchainKHR(*context_, vk_old_swapchain, nullptr); // try to destroy old swapchain anyway
            return false;
        }
        vk_image_views_.reset();
        vkDestroySwapchainKHR(*context_, vk_old_swapchain, nullptr);
    }

    vk_image_views_ = std::unique_ptr<VkImageView[]>(new(std::nothrow) VkImageView[num_vk_images]); // force new to return nullptr if fails instead of throw
    if (vk_image_views_.get() == nullptr) {
        err(std::string("swapchain_t::resize: failed to allocate storage for vk_image_views_"));
        vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr); // as vk_images_ has been completely constructed, dtor will be invoked for vk_images_
        return false;
    }

    uint32_t num_created_image_views = 0;
    while (num_created_image_views < num_vk_images) {
        auto vk_image_view_create_info = make_VkImageViewCreateInfo();
        vk_image_view_create_info.flags = 0;
        vk_image_view_create_info.image = vk_images_[num_created_image_views];
        vk_image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        vk_image_view_create_info.format = surface_format_.format;
        vk_image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        vk_image_view_create_info.subresourceRange.baseMipLevel = 0;
        vk_image_view_create_info.subresourceRange.levelCount = 1;
        vk_image_view_create_info.subresourceRange.baseArrayLayer = 0;
        vk_image_view_create_info.subresourceRange.layerCount = 1;
        ret = vkCreateImageView(*context_, &vk_image_view_create_info, nullptr, vk_image_views_.get() + num_created_image_views);
        if (ret != VK_SUCCESS) {
            err(std::string("swapchain_t::resize: vkCreateImageView failed: ") + std::string(str_from_VkResult(ret)));
            break;
        }
        num_created_image_views += 1;
    }
    if (num_created_image_views < num_vk_images) {
        for (uint32_t i = 0; i < num_created_image_views; ++i) {
            vkDestroyImageView(*context_, vk_image_views_[i], nullptr);
        }
        vkDestroySwapchainKHR(*context_, vk_swapchain_, nullptr);
        return false;
    }
    image_count_ = num_vk_images;
    return true;
}


VkImageView spock::swapchain_t::
image_view(uint32_t a_idx) const 
{
    SPOCK_PARANOID(
        vk_image_views_.get() != nullptr && a_idx < image_count_,
        "swapchain_t::image_view(" + std::to_string(a_idx) + ") failed: index should < " + std::to_string(image_count_)
    );
    return vk_image_views_[a_idx];
}


spock::query_pool_t::query_pool_t(query_pool_t && a_query_pool) noexcept
    : vk_dev_{ std::exchange(a_query_pool.vk_dev_, VK_NULL_HANDLE) }
    , vk_query_pool_{ std::exchange(a_query_pool.vk_query_pool_, VK_NULL_HANDLE) }
{
}


spock::query_pool_t& spock::query_pool_t::
operator=(query_pool_t&& a_query_pool) noexcept
{
    if (this != &a_query_pool) {
        if (valid()) {
            verbose("move assign to query_pool_t...");
            vkDestroyQueryPool(vk_dev_, vk_query_pool_, nullptr);
        }
        vk_dev_ = std::exchange(a_query_pool.vk_dev_, VK_NULL_HANDLE);
        vk_query_pool_ = std::exchange(a_query_pool.vk_query_pool_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::query_pool_t::
query_pool_t(VkDevice a_vk_dev, const VkQueryPoolCreateInfo& a_create_info)
    : vk_dev_{ a_vk_dev }
{
    SPOCK_VK_CALL(vkCreateQueryPool(vk_dev_, &a_create_info, nullptr, &vk_query_pool_));
}


spock::query_pool_t::
query_pool_t(VkDevice a_vk_dev, VkQueryType a_vk_query_type, uint32_t a_query_count, VkQueryPipelineStatisticFlags a_pipeline_stats)
    : vk_dev_{ a_vk_dev }
{
    auto create_info = make_VkQueryPoolCreateInfo();
    create_info.queryType = a_vk_query_type;
    create_info.queryCount = a_query_count;
    create_info.pipelineStatistics = a_pipeline_stats;
    SPOCK_VK_CALL(vkCreateQueryPool(vk_dev_, &create_info, nullptr, &vk_query_pool_));
}


spock::query_pool_t::
~query_pool_t()
{
    if (valid()) {
        vkDestroyQueryPool(vk_dev_, vk_query_pool_, nullptr);
    }
}


spock::dev_timer_array_t::
dev_timer_array_t() 
    : timestamp_period_in_ms_{ 0.f }
    , written_timestamp_count_{ 0 }
    , num_timers_per_frame_{ 0 }
    , moving_average_filter_width_{ 0 }
    , num_in_flight_frames_{ 0 }
    , frame_queue_len_max_{ 0 }
    , frame_queue_tail_idx_{ 0 }
    , frame_queue_len_{ 0 }
    , current_in_flight_idx_{ 0 }
{}


spock::dev_timer_array_t::
dev_timer_array_t
(
    const context_t & a_context, 
    bool a_external_sync, 
    uint32_t a_num_timers_per_frame, 
    uint32_t a_moving_average_filter_width, 
    uint32_t a_num_in_flight_frames, 
    uint32_t a_frame_queue_len_max
)
    : written_timestamp_count_{ 0 }
    , num_timers_per_frame_{ a_num_timers_per_frame }
    , moving_average_filter_width_{ a_moving_average_filter_width }
    , num_in_flight_frames_{ a_num_in_flight_frames }
    , frame_queue_len_max_{ std::max({ a_moving_average_filter_width, a_num_in_flight_frames, a_frame_queue_len_max }) }
    , frame_queue_tail_idx_{ 0 }
    , frame_queue_len_{ 0 }
    , current_in_flight_idx_{ 0 }
{
    SPOCK_PARANOID(num_timers_per_frame_ > 0, "dev_timer_array_t::begin: num_timers_per_frame_ must > 0");
    SPOCK_PARANOID(moving_average_filter_width_ > 0, "dev_timer_array_t::begin: moving_average_filter_width_ must > 0");
    SPOCK_PARANOID(num_in_flight_frames_ > 0, "dev_timer_array_t::begin: num_in_flight_frames_ must > 0");
    SPOCK_PARANOID(frame_queue_len_max_ > 0, "dev_timer_array_t::begin: frame_queue_len_max_ must > 0");

    const auto&  physical_dev_limits = a_context.limits();
    ensure(physical_dev_limits.timestampComputeAndGraphics, "timestampComputeAndGraphics is not supported");
    timestamp_period_in_ms_ = physical_dev_limits.timestampPeriod * 1e-6f;

    const auto num_timestamps_per_frame = num_timers_per_frame_ * 2;
    const auto num_timestampes_in_flight = num_timestamps_per_frame * num_in_flight_frames_;
    const auto num_total_timestamps = num_timestamps_per_frame * frame_queue_len_max_;

    query_pool_ = query_pool_t(a_context, VK_QUERY_TYPE_TIMESTAMP, num_timestampes_in_flight);
    filtered_elapsed_ms_.resize(num_timers_per_frame_, 0.f);
    frame_queue_ = std::unique_ptr<result_type[]>(new result_type[num_total_timestamps]);
    query_reset_.resize(num_in_flight_frames_, false);
    if (!a_external_sync) {
        fences_.reserve(num_in_flight_frames_);
        for (int i = 0; i < num_in_flight_frames_; ++i) {
            fences_.emplace_back(a_context, VkFenceCreateFlags{});
        }
    }
}


void spock::dev_timer_array_t::
begin()
{
    const auto num_timestamps_per_frame = num_timers_per_frame_ * 2;
    query_reset_[current_in_flight_idx_ ] = true;
    const auto first_timestamp_idx = current_in_flight_idx_ * num_timestamps_per_frame;
    vkResetQueryPool(query_pool_.vk_dev(), query_pool_, first_timestamp_idx, num_timestamps_per_frame);
    if (!fences_.empty()) {
        const VkFence* current_fence = &fences_[current_in_flight_idx_]();
        vkResetFences(query_pool_.vk_dev(), 1, current_fence);
    }
    written_timestamp_count_ = 0;
}


const VkFence & spock::dev_timer_array_t::
get_internal_fence() const
{ 
    SPOCK_PARANOID(!fences_.empty(), "dev_timer_array_t::fences_ is empty, external fences should be used");
    return fences_[current_in_flight_idx_](); 
}


void spock::dev_timer_array_t::
end()
{
    if (query_reset_[0]) {
        current_in_flight_idx_ = (current_in_flight_idx_ + 1) % num_in_flight_frames_; // loop has been kicked off
    }
    if (!query_reset_[current_in_flight_idx_]) { // begin(...) hasn't been called yet
        return;
    }
    const auto num_timestamps_per_frame = num_timers_per_frame_ * 2;
    SPOCK_PARANOID(written_timestamp_count_ == num_timestamps_per_frame, "not all timers for one frame are started or stopped");
    const auto prev_filter_width = std::min(moving_average_filter_width_, frame_queue_len_);
    std::for_each(
        filtered_elapsed_ms_.begin(), filtered_elapsed_ms_.end(),
        [prev_filter_width](float& t) { t *= static_cast<float>(prev_filter_width); }
    );
    const float old_frame_period_in_ms = timestamp_period_in_ms_ * static_cast<float>(prev_filter_width == moving_average_filter_width_);
    const auto old_frame_idx = (frame_queue_tail_idx_ - prev_filter_width + frame_queue_len_max_) % frame_queue_len_max_;
    for (size_t dtimer = 0; dtimer < filtered_elapsed_ms_.size(); ++dtimer) {
        const auto old_timestamp_idx_offset = old_frame_idx * num_timestamps_per_frame;
        const auto i = old_timestamp_idx_offset + 2 * dtimer;
        filtered_elapsed_ms_[dtimer] -= old_frame_period_in_ms * (frame_queue_[i + 1] - frame_queue_[i]);
    }

    const auto device = query_pool_.vk_dev();
    if (!fences_.empty()) {
        const VkFence* current_fence = &fences_[current_in_flight_idx_]();
        vkWaitForFences(device, 1, current_fence, VK_TRUE, always<uint64_t>());
    }
    const auto first_timestamp_idx = current_in_flight_idx_ * num_timestamps_per_frame;
    const auto timestamp_offset = frame_queue_tail_idx_ * num_timestamps_per_frame;
    void* data_ptr = frame_queue_.get() + timestamp_offset;

    SPOCK_VK_CALL(
        vkGetQueryPoolResults(
            device, query_pool_, first_timestamp_idx, num_timestamps_per_frame, 
            sizeof(result_type) * num_timestamps_per_frame, data_ptr, 
            sizeof(result_type), VK_QUERY_RESULT_64_BIT
        )
    ); // should not fail if wait properly and ALL timestamps in query have been WRITTEN

    frame_queue_len_ = std::min(frame_queue_len_ + 1, frame_queue_len_max_);
    frame_queue_tail_idx_ = (frame_queue_tail_idx_ + 1) % frame_queue_len_max_;

    const auto avg_filter_width = std::min(moving_average_filter_width_, frame_queue_len_);
    const float avg_scale_factor = 1.f / static_cast<float>(avg_filter_width); // guaranteed: avg_filter_width > 0
    for (size_t dtimer = 0; dtimer < filtered_elapsed_ms_.size(); ++dtimer) {
        const auto i = timestamp_offset + 2 * dtimer;
        filtered_elapsed_ms_[dtimer] = (filtered_elapsed_ms_[dtimer] + timestamp_period_in_ms_ * (frame_queue_[i + 1] - frame_queue_[i])) * avg_scale_factor;
    }
}


float spock::dev_timer_array_t::
elapsed_ms(uint32_t a_timer_idx) const
{ 
    SPOCK_PARANOID(a_timer_idx < num_timers_per_frame_, "dev_timer_array_t::elapsed_ms: a_timer_idx " + std::to_string(a_timer_idx) + " out of bounds");
    return filtered_elapsed_ms_[a_timer_idx]; 
}


void spock::dev_timer_array_t::
cmd_start(VkCommandBuffer a_vk_command_buffer, uint32_t a_timer_idx, VkPipelineStageFlagBits a_pipeline_stage)
{
    SPOCK_PARANOID(a_timer_idx < num_timers_per_frame_, "dev_timer_array_t::cmd_start: a_timer_idx " + std::to_string(a_timer_idx) + " out of bounds");
    const auto timestamp_idx = current_in_flight_idx_ * num_timers_per_frame_ * 2 + a_timer_idx * 2;
    vkCmdWriteTimestamp(a_vk_command_buffer, a_pipeline_stage, query_pool_, timestamp_idx);
    written_timestamp_count_ += 1;
}

void spock::dev_timer_array_t::
cmd_stop(VkCommandBuffer a_vk_command_buffer, uint32_t a_timer_idx, VkPipelineStageFlagBits a_pipeline_stage)
{
    SPOCK_PARANOID(a_timer_idx < num_timers_per_frame_, "dev_timer_array_t::cmd_start: a_timer_idx " + std::to_string(a_timer_idx) + " out of bounds");
    const auto timestamp_idx = current_in_flight_idx_ * num_timers_per_frame_ * 2 + a_timer_idx * 2 + 1;
    vkCmdWriteTimestamp(a_vk_command_buffer, a_pipeline_stage, query_pool_, timestamp_idx);
    written_timestamp_count_ += 1;
}


spock::acceleration_structure_t::
acceleration_structure_t(acceleration_structure_t&& a_other) noexcept
    : vk_dev_{ std::exchange(a_other.vk_dev_, VK_NULL_HANDLE) }
    , buffer_{ std::move(a_other.buffer_) }
    , vk_acceleration_structure_{ std::exchange(a_other.vk_acceleration_structure_, VK_NULL_HANDLE) }
{
}


spock::acceleration_structure_t& spock::acceleration_structure_t::
operator=(acceleration_structure_t&& a_other) noexcept
{
    if (this != &a_other) {
        if (valid()) {
            verbose("move assign to acceleration_structure_t...");
            vkDestroyAccelerationStructureKHR(vk_dev_, vk_acceleration_structure_, nullptr);
        }
        vk_dev_ = std::exchange(a_other.vk_dev_, VK_NULL_HANDLE);
        buffer_ = std::move(a_other.buffer_);
        vk_acceleration_structure_ = std::exchange(a_other.vk_acceleration_structure_, VK_NULL_HANDLE);
    }
    return *this;
}


spock::acceleration_structure_t::
acceleration_structure_t
(
    const context_t& a_context,  
    VkAccelerationStructureTypeKHR a_acceleration_structure_type,
    VkDeviceSize a_acceleration_structure_size
)
    : vk_dev_{ a_context }
{
    auto buffer_create_info = make_VkBufferCreateInfo();
    buffer_create_info.size = a_acceleration_structure_size;
    buffer_create_info.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    VmaAllocationCreateInfo mem_allocation_create_info{};
    mem_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    buffer_ = buffer_t(a_context, buffer_create_info, mem_allocation_create_info);

    auto create_info = make_VkAccelerationStructureCreateInfoKHR();
    create_info.type = a_acceleration_structure_type;
    create_info.size = a_acceleration_structure_size;
    create_info.buffer = buffer_;
    create_info.offset = 0;

    SPOCK_VK_CALL(vkCreateAccelerationStructureKHR(vk_dev_, &create_info, nullptr, &vk_acceleration_structure_));
}


spock::acceleration_structure_t::
~acceleration_structure_t()
{
    if (valid()) {
        verbose("destroy acceleration_structure_t...");
        vkDestroyAccelerationStructureKHR(vk_dev_, vk_acceleration_structure_, nullptr);
    }
}


VkDeviceAddress spock::acceleration_structure_t::
get_device_addr() const
{
    auto addr_info = make_VkAccelerationStructureDeviceAddressInfoKHR();
    addr_info.accelerationStructure = vk_acceleration_structure_;
    return vkGetAccelerationStructureDeviceAddressKHR(vk_dev_, &addr_info);
}


VkAccelerationStructureBuildGeometryInfoKHR spock::blas_builder_array_t::
create_geometry_info(uint32_t a_num_geometries, VkBuildAccelerationStructureFlagsKHR a_flags, bool a_allow_rebuild)
{
    size_t geometry_offset = geometry_pool_.size();
    auto geometry_info = make_VkAccelerationStructureBuildGeometryInfoKHR();
    geometry_info.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    geometry_info.flags = a_flags;
    geometry_info.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    geometry_info.srcAccelerationStructure = VK_NULL_HANDLE;
    geometry_info.dstAccelerationStructure = VK_NULL_HANDLE;
    geometry_info.geometryCount = a_num_geometries;
    geometry_info.pGeometries = reinterpret_cast<VkAccelerationStructureGeometryKHR*>(geometry_offset); // change to real pointer in resolve(...)
    geometry_info.ppGeometries = nullptr;
    geometry_info.scratchData.deviceAddress = static_cast<uint64_t>(a_allow_rebuild);

    return geometry_info;
}

spock::acceleration_structures_build_info_t spock::blas_builder_array_t::
get_partial_build_info(const std::vector<uint32_t>& a_info_indices, VkBuildAccelerationStructureModeKHR a_build_mode, uint64_t a_src_as_mask)
{
    const uint32_t update_info_count = static_cast<uint32_t>(a_info_indices.size());
    const uint32_t total_info_count = static_cast<uint32_t>(build_info_.geometries.size());

    SPOCK_PARANOID(!scratch_buffers_.empty(), "blas_builder_array_t::cmd_rebuild: no scratch buffer is available");
    SPOCK_PARANOID(update_info_count < total_info_count, "blas_builder_array_t::cmd_rebuild: update blas count cannot be larger than initially built blas count");
    SPOCK_PARANOID(
        a_build_mode == VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR  && a_src_as_mask == 0 || 
        a_build_mode == VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR && a_src_as_mask == ~0ull,
        "blas_builder_array_t::cmd_rebuild: bug: mismatch between a_build_mode and a_src_as_mask"
    );

    const VkBuildAccelerationStructureModeKHR rebuild_mode = a_build_mode;
    const uint64_t rebuild_src_as_mask = a_src_as_mask;
    acceleration_structures_build_info_t rebuild_info;

    rebuild_info.geometries.reserve(update_info_count);
    rebuild_info.ranges.reserve(update_info_count);
    for (const auto i : a_info_indices) {
        SPOCK_PARANOID(i < total_info_count, "blas_builder_array_t::cmd_rebuild: index out of bounds");
        SPOCK_PARANOID(
            (a_src_as_mask == 0) || (build_info_.geometries[i].flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR), 
            "blas_builder_array_t::cmd_rebuild: acceleration structure is not allowed to update"
        );
        auto geometry_info = build_info_.geometries[i];
        geometry_info.mode = rebuild_mode;
        geometry_info.srcAccelerationStructure = (VkAccelerationStructureKHR)((uint64_t)geometry_info.dstAccelerationStructure & rebuild_src_as_mask);
        rebuild_info.geometries.push_back(geometry_info);
        rebuild_info.ranges.push_back(build_info_.ranges[i]);
    }
    return rebuild_info;
}


// no explicit guarantee that move constructor of a vector preserves the validity of iterators/pointers to its elements
spock::blas_builder_array_t::
blas_builder_array_t(blas_builder_array_t&& a_other) noexcept
{
    geometry_pool_.swap(a_other.geometry_pool_);
    range_info_pool_.swap(a_other.range_info_pool_);
    scratch_buffers_ = std::move(a_other.scratch_buffers_);
    sizes_infos_ = std::move(a_other.sizes_infos_);
    build_info_ = std::move(a_other.build_info_);
}


// no explicit guarantee that move constructor of a vector preserves the validity of iterators/pointers to its elements
spock::blas_builder_array_t& spock::blas_builder_array_t::
operator=(blas_builder_array_t&& a_other) noexcept
{
    if (this != &a_other) {
        geometry_pool_ = {};
        range_info_pool_ = {};

        geometry_pool_.swap(a_other.geometry_pool_);
        range_info_pool_.swap(a_other.range_info_pool_);
        scratch_buffers_ = std::move(a_other.scratch_buffers_);
        sizes_infos_ = std::move(a_other.sizes_infos_);
        build_info_ = std::move(a_other.build_info_);
    }
    return *this;
}


void spock::blas_builder_array_t::
add_build_info(
    const VkAccelerationStructureGeometryKHR& a_geometry, 
    const VkAccelerationStructureBuildRangeInfoKHR& a_range_info, 
    VkBuildAccelerationStructureFlagsKHR a_flags,
    bool a_allow_rebuild
)
{
    build_info_.geometries.push_back(create_geometry_info(1, a_flags, a_allow_rebuild));
    geometry_pool_.push_back(a_geometry);
    range_info_pool_.push_back(a_range_info);
}


// range info pointers have to be resolved after all build infos have been added, as pointers may be invalidated by range_info_pool_ resize
void spock::blas_builder_array_t::
add_build_infos(
    const std::vector<VkAccelerationStructureGeometryKHR>& a_geometries, 
    const std::vector<VkAccelerationStructureBuildRangeInfoKHR>& a_range_infos, 
    VkBuildAccelerationStructureFlagsKHR a_flags,
    bool a_allow_rebuild
)
{
    SPOCK_PARANOID(a_geometries.size() == a_range_infos.size(), "blas_builder_array_t::add_build_info::geometry count and range info count do not match");

    build_info_.geometries.push_back(create_geometry_info(static_cast<uint32_t>(a_geometries.size()), a_flags, a_allow_rebuild));
    geometry_pool_.insert(geometry_pool_.end(), a_geometries.begin(), a_geometries.end());
    range_info_pool_.insert(range_info_pool_.end(), a_range_infos.begin(), a_range_infos.end());
}


uint32_t spock::blas_builder_array_t::
resolve_infos(VkDevice a_vk_dev)
{
    SPOCK_PARANOID(build_info_.ranges.empty(), "blas_builder_array_t::resolve_infos: build_info_.ranges should be empty");
    SPOCK_PARANOID(sizes_infos_.empty(), "blas_builder_array_t::resolve_infos: sizes_infos_ must remain empty");

    uint32_t num_infos = static_cast<uint32_t>(build_info_.geometries.size());
    build_info_.ranges.reserve(num_infos);
    sizes_infos_.reserve(num_infos);
    for (auto& geometry_info : build_info_.geometries) {
        const auto geometry_offset = *reinterpret_cast<size_t*>(&geometry_info.pGeometries);
        geometry_info.pGeometries = geometry_pool_.data() + geometry_offset;
        build_info_.ranges.push_back(&range_info_pool_[geometry_offset]);
        std::vector<uint32_t> primitive_counts(geometry_info.geometryCount, 0);
        for (size_t i = 0; i < geometry_info.geometryCount; ++i) {
            primitive_counts[i] = range_info_pool_[geometry_offset + i].primitiveCount;
        }
        auto sizes_info = make_VkAccelerationStructureBuildSizesInfoKHR();
        vkGetAccelerationStructureBuildSizesKHR(
            a_vk_dev,
            VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,  // Built on device instead of host
            &geometry_info,                           // Pointer to build info
            primitive_counts.data(),                        // Array of numbers of primitives
            &sizes_info
        );
        sizes_infos_.push_back(sizes_info);
    }
    return num_infos;
}


void spock::blas_builder_array_t::
create_blases(const context_t& a_context, acceleration_structure_t* a_blases, VkAccelerationStructureInstanceKHR* a_instance_begin)
{
    const uint32_t num_infos = static_cast<uint32_t>(build_info_.geometries.size());

    SPOCK_PARANOID(build_info_.ranges.size() == num_infos, "blas_builder_array_t::create_blases: build_info_: mismatch between .ranges .geometries");
    SPOCK_PARANOID(sizes_infos_.size() == num_infos, "blas_builder_array_t::create_blases: size mismatch between sizes_infos_ and build_info_.geometries");
    SPOCK_PARANOID(a_instance_begin != nullptr, "blas_builder_array_t::create_blases: a_instance_begin cannot be nullptr");

    for (uint32_t i = 0; i < num_infos; ++i) {
        auto& blas = a_blases[i];
        blas = acceleration_structure_t{ a_context, build_info_.geometries[i].type, sizes_infos_[i].accelerationStructureSize };
        build_info_.geometries[i].dstAccelerationStructure = blas;
        a_instance_begin[i].accelerationStructureReference = blas.get_device_addr();
    }
}


std::vector<spock::buffer_t> 
spock::blas_builder_array_t::
cmd_initial_build(const context_t & a_context, VkCommandBuffer a_command_buffer)
{
    SPOCK_PARANOID(scratch_buffers_.empty(), "blas_builder_array_t::cmd_initial_build: scratch_buffers_ must be empty");

    const uint32_t num_infos = static_cast<uint32_t>(build_info_.geometries.size());
    scratch_buffers_.reserve(num_infos);

    auto buffer_create_info = make_VkBufferCreateInfo();
    VmaAllocationCreateInfo mem_allocation_create_info{};
    mem_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    buffer_create_info.usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    std::vector<buffer_t> one_time_scratch_buffers;
    //TODO: single scratch buffer with offsets and/or batch scratch buffer size limit
    for (uint32_t i = 0; i < num_infos; ++i) {
        SPOCK_PARANOID(build_info_.geometries[i].dstAccelerationStructure != 0, "blas_builder_array_t::cmd_initial_build: blas has not been created");

        const bool update_allowed = static_cast<bool>(build_info_.geometries[i].flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR);
        const bool rebuild_allowed = static_cast<bool>(build_info_.geometries[i].scratchData.deviceAddress);
        const auto scratch_size_max = std::max(sizes_infos_[i].buildScratchSize, sizes_infos_[i].updateScratchSize);
        const auto scratch_size = update_allowed ? scratch_size_max : sizes_infos_[i].buildScratchSize;
        buffer_create_info.size = sizes_infos_[i].buildScratchSize;
        if (update_allowed || rebuild_allowed) {
            scratch_buffers_.emplace_back(a_context, buffer_create_info, mem_allocation_create_info);
            build_info_.geometries[i].scratchData.deviceAddress = a_context.get_buffer_device_address(scratch_buffers_.back());
        }
        else {
            scratch_buffers_.push_back(buffer_t()); // dummy place holder
            one_time_scratch_buffers.emplace_back(a_context, buffer_create_info, mem_allocation_create_info);
            build_info_.geometries[i].scratchData.deviceAddress = a_context.get_buffer_device_address(one_time_scratch_buffers.back());
        }
    }

    vkCmdBuildAccelerationStructuresKHR(a_command_buffer, num_infos, build_info_.geometries.data(), build_info_.ranges.data());
    return one_time_scratch_buffers;
}


void spock::blas_builder_array_t::
adapt_build_info_for_rebuild_all()
{
    const uint32_t info_count = static_cast<uint32_t>(build_info_.geometries.size());
    for (uint32_t i = 0; i < info_count; ++i) {
        build_info_.geometries[i].mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        build_info_.geometries[i].srcAccelerationStructure = VK_NULL_HANDLE;
    }
}


void spock::blas_builder_array_t::
adapt_build_info_for_refit_all()
{
    const uint32_t info_count = static_cast<uint32_t>(build_info_.geometries.size());
    for (uint32_t i = 0; i < info_count; ++i) {
        SPOCK_PARANOID(
            build_info_.geometries[i].flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR, 
            "blas_builder_array_t::prepare_update_all: acceleration structure is not allowed to update"
        );
        build_info_.geometries[i].mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
        build_info_.geometries[i].srcAccelerationStructure = build_info_.geometries[i].dstAccelerationStructure;
    }
}


void spock::
translate_instance(VkAccelerationStructureInstanceKHR* a_instance, const glm::vec3& a_translate)
{
    const auto transform = glm::translate(glm::identity<glm::mat4>(), a_translate);
    transform_instance(a_instance, transform);
}


void spock::
rotate_instance(VkAccelerationStructureInstanceKHR * a_instance, float a_radians, const glm::vec3 & a_axis)
{
    const auto transform = glm::rotate(glm::identity<glm::mat4>(), a_radians, a_axis);
    transform_instance(a_instance, transform);
}


void spock::
scale_instance(VkAccelerationStructureInstanceKHR* a_instance, const glm::vec3& a_scale_v)
{
    const auto transform = glm::scale(glm::identity<glm::mat4>(), a_scale_v);
    transform_instance(a_instance, transform);
}


spock::acceleration_structure_t spock::tlas_builder_t::
cmd_initial_build(
    const context_t& a_context,
    VkCommandBuffer a_command_buffer, 
    uint32_t a_instance_count,
    const VkAccelerationStructureInstanceKHR* a_instance_begin,
    VkBuildAccelerationStructureFlagsKHR a_flags 
)
{
    SPOCK_PARANOID(
        !instance_buffer_.valid() && !instance_staging_buffer_.valid() && !scratch_buffer_.valid(),
        "tlas_builder_t::cmd_initial_build: buffers must be empty"
    );

    range_info_ = VkAccelerationStructureBuildRangeInfoKHR{};
    range_info_.primitiveOffset = 0;
    range_info_.primitiveCount = a_instance_count;
    range_info_.firstVertex = 0;
    range_info_.transformOffset = 0;

    const VkDeviceSize instance_buffer_bytes = static_cast<VkDeviceSize>(sizeof(VkAccelerationStructureInstanceKHR) * a_instance_count);
    auto buffer_create_info = make_VkBufferCreateInfo();
    buffer_create_info.size        = instance_buffer_bytes;
    buffer_create_info.usage       = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;

    VmaAllocationCreateInfo mem_allocation_create_info{};
    mem_allocation_create_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    instance_buffer_ = buffer_t{ a_context, buffer_create_info, mem_allocation_create_info };
    instance_staging_buffer_ = instance_buffer_.cmd_upload(a_command_buffer, a_instance_begin);

    // vkspec-1.2.187: ch36 Acceleration Structures: vkCmdBuildAccelerationStructuresKHR (p. 1951)
    auto barrier = build_VkMemoryBarrier(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    vkCmdPipelineBarrier(a_command_buffer, 
        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR, 
        0, 1, &barrier, 0, nullptr, 0, nullptr
    );

    auto instance_data = make_VkAccelerationStructureGeometryInstancesDataKHR();
    instance_data.arrayOfPointers = VK_FALSE;
    instance_data.data.deviceAddress = a_context.get_buffer_device_address(instance_buffer_);

    unique_geometry_ = std::make_unique<VkAccelerationStructureGeometryKHR>();
    auto& geometry = *unique_geometry_;
    geometry = make_VkAccelerationStructureGeometryKHR();
    geometry.geometryType       = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    geometry.geometry.instances = instance_data;

    geometry_info_ = make_VkAccelerationStructureBuildGeometryInfoKHR();
    geometry_info_.type                     = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    geometry_info_.flags                    = a_flags;
    geometry_info_.mode                     = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    geometry_info_.srcAccelerationStructure = VK_NULL_HANDLE;
    geometry_info_.geometryCount            = 1;
    geometry_info_.pGeometries              = unique_geometry_.get();

    auto sizes_info = make_VkAccelerationStructureBuildSizesInfoKHR();

    vkGetAccelerationStructureBuildSizesKHR(
        a_context,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,  // Built on device instead of host
        &geometry_info_,                           // Pointer to build info
        &range_info_.primitiveCount,                        // Array of numbers of primitives
        &sizes_info
    );

    acceleration_structure_t tlas{ a_context, VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR, sizes_info.accelerationStructureSize };
    geometry_info_.dstAccelerationStructure = tlas;

    const bool update_allowed = static_cast<bool>(geometry_info_.flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR);
    const auto scratch_buffer_size_max = std::max(sizes_info.buildScratchSize, sizes_info.updateScratchSize);

    buffer_create_info = make_VkBufferCreateInfo();
    buffer_create_info.size  = update_allowed ? scratch_buffer_size_max : sizes_info.buildScratchSize;
    buffer_create_info.usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    mem_allocation_create_info = VmaAllocationCreateInfo{};
    mem_allocation_create_info.requiredFlags  = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    scratch_buffer_ = buffer_t(a_context, buffer_create_info, mem_allocation_create_info);
    geometry_info_.scratchData.deviceAddress = a_context.get_buffer_device_address(scratch_buffer_);

    const auto* range_info_ptr = &range_info_;
    vkCmdBuildAccelerationStructuresKHR(a_command_buffer, 1, &geometry_info_, &range_info_ptr);

    return tlas;
}


void spock::tlas_builder_t::
cmd_update_instance_buffer(
    VkCommandBuffer a_command_buffer, 
    uint32_t a_instance_count, 
    const VkAccelerationStructureInstanceKHR * a_instance_begin, 
    uint32_t a_dst_instance_offset
)
{
    SPOCK_PARANOID(scratch_buffer_.valid(), "tlas_builder_t::cmd_update_instance_buffer: scratch_buffer_ is not valid");
    SPOCK_PARANOID(instance_buffer_.valid(), "tlas_builder_t::cmd_update_instance_buffer: instance_buffer_ is not valid");
    SPOCK_PARANOID(instance_staging_buffer_.valid(), "tlas_builder_t::cmd_update_instance_buffer: instance_staging_buffer_ is not valid");
    SPOCK_PARANOID(a_instance_count <= range_info_.primitiveCount, "tlas_builder_t::cmd_update_instance_buffer: a_instances index out of bounds");
    SPOCK_PARANOID(
        a_dst_instance_offset + a_instance_count <= range_info_.primitiveCount, 
        "tlas_builder_t::cmd_update_instance_buffer: a_instances index out of bounds"
    );

    std::copy(
        a_instance_begin, 
        a_instance_begin + a_instance_count, 
        instance_staging_buffer_.mapped_data<VkAccelerationStructureInstanceKHR>() + a_dst_instance_offset
    );
    VkDeviceSize byte_offset = a_dst_instance_offset * sizeof(VkAccelerationStructureInstanceKHR);
    const VkDeviceSize bytes = a_instance_count * sizeof(VkAccelerationStructureInstanceKHR);
    const auto buffer_copy = build_VkBufferCopy(bytes, byte_offset, byte_offset);
    vkCmdCopyBuffer(a_command_buffer, instance_staging_buffer_, instance_buffer_, 1, &buffer_copy);

}

