#define VOLK_IMPLEMENTATION
#include <volk.h>
#undef VOLK_IMPLEMENTATION

#define SPOCK_GENERATED_IMPLEMENTATION
#include <spock/generated.hpp>
#undef SPOCK_GENERATED_IMPLEMENTATION

#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
#undef VMA_IMPLEMENTATION

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#undef STB_IMAGE_WRITE_IMPLEMENTATION

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#undef STB_IMAGE_IMPLEMENTATION

#define TINYGLTF_IMPLEMENTATION
#include <tiny_gltf.h>
#undef TINYGLTF_IMPLEMENTATION

