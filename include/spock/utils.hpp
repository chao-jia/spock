#pragma once

#if defined(__INTELLISENSE__)
#undef VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#else
#include <volk.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <string>
#include <vector>
#include <utility>
#include <functional>
#include <type_traits>
#include <stdexcept>
#include <algorithm>
#include <thread>
#include <limits>
#include <set>
#include <memory>
#include <filesystem>
#include <chrono>
#include <string_view>
#include <nlohmann/json_fwd.hpp>


// runtime check, should be always on, use macro to delay the evaluation of msg, e.g. to_string(...)
#define SPOCK_ENSURE(expr, msg) do {   \
    if (!(expr)) {                     \
        spock::except(msg);            \
    }                                  \
} while (0)

#define SPOCK_VK_CHECK(expr, msg) do {                                                                           \
    const auto _spock_vk_check_ret__ = (expr);                                                                   \
    if (_spock_vk_check_ret__ != VK_SUCCESS)                                                                     \
        spock::except(msg + std::string(" <") + spock::str_from_VkResult(_spock_vk_check_ret__).data() + '>');   \
} while(0)


#define SPOCK_VK_CALL(expr) do {                                                                                                              \
    const auto _spock_vk_check_ret__ = (expr);                                                                                                \
    if (_spock_vk_check_ret__ != VK_SUCCESS)                                                                                                  \
        spock::except(std::string("error: vk call failed [ " #expr " ] <") + spock::str_from_VkResult(_spock_vk_check_ret__).data() + '>');   \
} while(0)


#define SPOCK_VK_DEBUG_NAME(context, vk_handle, name) do {                             \
    spock::set_vk_handle_debug_name(context, vk_handle, name, context.debug_enabled());\
} while (0)

#ifndef NDEBUG
/// runtime debug assertion
#define SPOCK_PARANOID(a_bool_expr, a_string_msg) do { spock::ensure(a_bool_expr, a_string_msg); } while(0)
#define SPOCK_ALERT(a_bool_expr, a_string_msg) do { if(!(a_bool_expr)) { warn(a_string_msg); } } while(0)
#else
/// runtime debug assertion
#define SPOCK_PARANOID(a_bool_expr, a_string_msg)
#define SPOCK_ALERT(a_bool_expr, a_string_msg)
#endif


namespace spock {

using namespace nlohmann;

void verbose(const std::string& a_msg);
void info(const std::string& a_msg);
void warn(const std::string& a_msg);
void err(const std::string& a_msg);
std::string nested_exception_str(const std::exception& a_exception);
void except(const std::string& a_msg);
void nested_except(const std::string& a_msg);

template<typename T> uint32_t ctz(T x) = delete; // disable implicit conversion
uint32_t ctz(uint32_t x);
template<typename T> uint32_t clz(T x) = delete; // disable implicit conversion
uint32_t clz(uint32_t x);
template<typename T> uint32_t power_of_two_ceil(T x) = delete; // disable implicit conversion
uint32_t power_of_two_ceil(uint32_t x);

std::string to_lower(const std::string& str);
std::string encode_base64(const uint8_t* a_buffer, size_t a_count);
std::vector<uint8_t> decode_base64(const char* a_c_str, size_t a_count);
int put_env(const char* a_name, const char* a_value);
std::string vulkan_api_version_str(uint32_t a_version);
std::string vulkan_driver_version_str(uint32_t a_vendor_id, uint32_t a_version);
std::string device_properties_str(const VkPhysicalDeviceProperties& dev_prop);
std::string file_to_str(const std::string_view a_file_path);
void c_str_to_file(const char* a_c_str, size_t a_bytes, const std::string_view a_file_path);
bool vk_format_feature_filter_linear_supported(VkFormat a_format); // vkspec 1.2.187: 43.3 required format support
void fibonacci_sphere(uint32_t a_sample_count, glm::vec3* a_normals); // a_normals: a buffer with sufficient storage for a_sample_count glm::vec3s

/// runtime check
inline void 
ensure(bool a_bool_expr, const std::string& a_msg)
{
    if (!a_bool_expr) {
        except(a_msg);
    }
}


template<typename T, uint32_t N>
inline constexpr uint32_t
length_of_c_array(T (&)[N]) 
{ 
    return N; 
}


inline uint32_t 
u32_log2(uint32_t x) 
{ 
    return 31u - clz(x); 
}


inline constexpr bool 
is_power_of_two(uint64_t x)
{
    return (x != 0) && ((x & (x - 1)) == 0);
}


template<typename T>
inline constexpr bool 
is_uint()
{
    return std::is_unsigned_v<T> && std::is_integral_v<T>;
}


template<typename T>
inline constexpr 
std::enable_if_t<is_uint<T>(), T> 
div_ceil(T a_dividend, T a_divisor)
{
    SPOCK_PARANOID(a_divisor != 0, "round_up_to_multiple: cannot round up to multiple of zero");
    return (a_dividend + a_divisor - 1) / a_divisor;
}


template<typename T>
inline constexpr 
std::enable_if_t<is_uint<T>(), T> 
round_up_to_multiple(T x, T a_divisor)
{
    SPOCK_PARANOID(a_divisor != 0, "round_up_to_multiple: cannot round up to multiple of zero");
    return div_ceil(x, a_divisor) * a_divisor;
}


template<typename T>
inline constexpr 
std::enable_if_t<is_uint<T>(), T> 
npos()
{
    return std::numeric_limits<T>::max();
}


template<typename T>
inline constexpr 
std::enable_if_t<std::is_integral_v<T>, T>
always()
{
    return std::numeric_limits<T>::max();
}


// N: number of used bits
template<typename T, uint32_t N>
inline constexpr 
std::enable_if_t<is_uint<T>() && (N > 0u), T>
bitset_max()
{
    static_assert(sizeof(T) * 8 >= N, "T should have at least N bits");
    return (static_cast<T>(1) << N) - 1;
}


/**
* test whether `a_flags` has all the bits specified in `a_bits` set to 1
*/
template<typename T, typename U>
inline bool has_flag_bits(T a_flags, U a_bits)
{
    return (a_flags & a_bits) == a_bits;
}


// BinaryFn: void(Type& a, size_t index);
template<typename InputIt, typename BinaryFn> inline BinaryFn
for_indexed_each(InputIt a_first, InputIt a_last, BinaryFn&& a_fn)
{
    const auto dist = static_cast<int64_t>(std::distance(a_first, a_last));
    SPOCK_PARANOID(dist >= 0, "enum_each: first should not be reachable from the last");
    const size_t count = std::abs(dist);
    auto worker = [a_first, a_fn](size_t beg, size_t end){
        for (size_t i = beg; i < end; ++i) {
            a_fn(*(a_first + i), i);
        }
    };
    const auto num_threads = std::clamp(static_cast<size_t>(std::thread::hardware_concurrency()), static_cast<size_t>(2u), count);
    const size_t batch_count = (count + num_threads - 1) / num_threads;
    std::vector<std::thread> thread_pool;
    thread_pool.reserve(num_threads);
    for (size_t i = 0; i < num_threads; ++i) {
        size_t begin_count = i * batch_count;
        size_t end_count = std::min(count, (i + 1) * batch_count);
        thread_pool.emplace_back(worker, begin_count, end_count);
    }
    for (auto& t : thread_pool) t.join();
    return a_fn;
}


/// return true if a_time_point_a is earlier than a_time_point_b
inline bool 
earlier_than(const std::filesystem::file_time_type& a_time_point_a, const std::filesystem::file_time_type& a_time_point_b)
{
#if __cplusplus < 202002L
    return a_time_point_a < a_time_point_b;
#else
    return (a_time_point_a <=> a_time_point_b) < 0;
#endif
}


inline bool
equals_zero(float f)
{
    return std::abs(f) < std::numeric_limits<float>::epsilon();
};

template<class V>
inline bool
equals_zero_vec(const V& v)
{
    static_assert(std::is_same_v<std::remove_reference_t<std::remove_cv_t<decltype(v.x)>>, float>, "equals_one_vec: only takes single float vector");
    return glm::all(glm::lessThan(glm::abs(v), V(glm::epsilon<float>())));
}

inline bool
equals_one(float f) 
{ 
    return std::abs(f - 1.f) < std::numeric_limits<float>::epsilon(); 
};

template<class V>
inline bool
equals_one_vec(const V& v)
{
    static_assert(std::is_same_v<std::remove_reference_t<std::remove_cv_t<decltype(v.x)>>, float>, "equals_one_vec: only takes single float vector");
    return glm::all(glm::lessThan(glm::abs(v - V(1.0f)), V(glm::epsilon<float>())));
}


/// -1.f <= sf <= 1.f
inline float
i8_to_sf32(int8_t i8)
{
    return std::max(static_cast<float>(i8) / 127.f, -1.f); // clamp -128
}

/// -1.f <= sf <= 1.f
inline float
u8_to_sf32(uint8_t u8)
{
    return 2.f * static_cast<float>(u8) / 255.f - 1.f;
}

/// 0.f <= u8 <= 1.f
inline float
u8_to_uf32(uint8_t u8)
{
    return static_cast<float>(u8) / 255.f;
}

/// -1.f <= sf <= 1.f
inline int8_t 
sf32_to_i8(float sf)
{
    return static_cast<int8_t>(std::round(std::clamp(sf, -1.f, 1.f) * 127.f));
}

/// -1.f <= sf <= 1.f
inline uint8_t 
sf32_to_u8(float sf)
{
    return static_cast<uint8_t>(std::round((std::clamp(sf, -1.f, 1.f) + 1.f) * 127.5f));
}

/// 0.f <= f <= 1.f
inline uint8_t 
uf32_to_u8(float f)
{
    return static_cast<uint8_t>(std::round(std::clamp(f, 0.f, 1.f) * 255.f));
}

/// 0.f <= v[i] <= 1.f
inline glm::u8vec2 
uf32_to_u8_vec2(const glm::vec2& v)
{
    return glm::u8vec2{ uf32_to_u8(v.x), uf32_to_u8(v.y) };
}

/// 0.f <= v[i] <= 1.f
inline glm::vec2 
u8_to_uf32_vec2(const glm::u8vec2& v)
{
    return glm::vec2{ u8_to_uf32(v.x), u8_to_uf32(v.y) };
}


/// -1.f <= v[i] <= 1.f
inline glm::u8vec3 
sf32_to_u8_vec3(const glm::vec3& v)
{
    return glm::u8vec3{ sf32_to_u8(v.x), sf32_to_u8(v.y), sf32_to_u8(v.z) };
}

/// 0.f <= v[i] <= 1.f
inline glm::u8vec3 
uf32_to_u8_vec3(const glm::vec3& v)
{
    return glm::u8vec3{ uf32_to_u8(v.x), uf32_to_u8(v.y), uf32_to_u8(v.z) };
}

/// -1.f <= v[i] <= 1.f
inline glm::vec3 
u8_to_sf32_vec3(const glm::u8vec3& v)
{
    return glm::vec3{ u8_to_sf32(v.x), u8_to_sf32(v.y), u8_to_sf32(v.z) };
}

/// 0.f <= v[i] <= 1.f
inline glm::vec3 
u8_to_uf32_vec3(const glm::u8vec3& v)
{
    return glm::vec3{ u8_to_uf32(v.x), u8_to_uf32(v.y), u8_to_uf32(v.z) };
}

/// 0.f <= v[i] <= 1.f
inline glm::u8vec4 
uf32_to_u8_vec4(const glm::vec4& v)
{
    return glm::u8vec4{ uf32_to_u8(v.x), uf32_to_u8(v.y), uf32_to_u8(v.z), uf32_to_u8(v.w) };
}

/// 0.f <= v[i] <= 1.f
inline glm::vec4 
u8_to_uf32_vec4(const glm::u8vec4& v)
{
    return glm::vec4{ u8_to_uf32(v.x), u8_to_uf32(v.y), u8_to_uf32(v.z), u8_to_uf32(v.w)};
}


// octahedron normal helper
inline glm::vec2 
sign_not_zero(const glm::vec2& v)
{
    return glm::vec2{ (v.x >= 0.f) ? 1.f : -1.f, (v.y >= 0.f) ? 1.f : -1.f };
}


// octahedron normal helper
/// reflect over line |x| = |y|, depending on which quadrant v lies in
inline glm::vec2 
diagonal_reflect(const glm::vec2& v)
{
    return (glm::vec2(1.f, 1.f) - glm::abs(glm::vec2(v.y, v.x))) * sign_not_zero(v);
}


inline glm::vec2 
oct_encode_dir(const glm::vec3& n)
{
    glm::vec2 p = glm::vec2(n) * (1.f / glm::l1Norm(n));
    return (n.z <= 0.f) ? diagonal_reflect(p) : p;
}


/// -1.f <= v.x, v.y <= 1.f
inline glm::vec3 
oct_decode_dir(const glm::vec2& v)
{
    glm::vec3 n = glm::vec3(glm::vec2(v), 1.f - std::abs(v.x) - std::abs(v.y));
    if (n.z < 0.f) {
        const auto n_xy = (glm::vec2(1.f) - abs(glm::vec2(n.y, n.x))) * sign_not_zero(glm::vec2(n));
        n.x = n_xy.x;
        n.y = n_xy.y;
    }
    return normalize(n);
}


inline uint8_t 
calc_num_mips(const std::initializer_list<uint32_t>& a_dims)
{
    return static_cast<uint8_t>(std::floor(std::log2(std::max(a_dims)))) + 1u;
}

inline uint8_t
calc_num_mips(const VkExtent3D& a_dims)
{
    const auto dims = { a_dims.width, a_dims.height, a_dims.depth };
    return static_cast<uint8_t>(std::floor(std::log2(std::max(dims)))) + 1u;
}

inline uint8_t
calc_num_mips(const VkExtent2D& a_dims)
{
    const auto dims = { a_dims.width, a_dims.height };
    return static_cast<uint8_t>(std::floor(std::log2(std::max(dims)))) + 1u;
}


inline VkOffset3D
to_offset_3d(const VkExtent3D& a_extent)
{
    return VkOffset3D{ (int32_t)a_extent.width, (int32_t)a_extent.height, (int32_t)a_extent.depth };
}

inline VkOffset3D
to_offset_3d(const VkExtent2D& a_from, uint32_t a_depth)
{
    return VkOffset3D{ (int32_t)a_from.width, (int32_t)a_from.height, (int32_t)a_depth };
}

inline VkOffset3D
to_offset_3d(const VkExtent2D& a_from, int32_t a_depth)
{
    return VkOffset3D{ (int32_t)a_from.width, (int32_t)a_from.height, a_depth };
}

inline VkExtent3D
to_extent_3d(const VkExtent2D& a_from, uint32_t a_depth)
{
    return VkExtent3D{ a_from.width, a_from.height, a_depth };
}

inline VkExtent3D
to_extent_3d(const VkExtent2D& a_from, int32_t a_depth)
{
    return VkExtent3D{ a_from.width, a_from.height, static_cast<uint32_t>(a_depth) };
}


inline VkExtent2D
to_extent_2d(const glm::uvec2& a_from)
{
    return VkExtent2D{ a_from[0], a_from[1] };
}


inline VkExtent3D
to_extent_3d(const glm::uvec3& a_from)
{
    return VkExtent3D{ a_from[0], a_from[1], a_from[2] };
}


inline glm::uvec2
to_uvec2(const VkExtent2D& a_from)
{
    return glm::uvec2(a_from.width, a_from.height);
}


inline glm::uvec3
to_uvec3(const VkExtent3D& a_from)
{
    return glm::uvec3(a_from.width, a_from.height, a_from.depth);
}


template<
    typename t_extent, 
    std::enable_if_t<std::is_same_v<t_extent, glm::uvec2> || std::is_same_v<t_extent, glm::uvec3>, bool> = true
>
inline typename t_extent 
calc_workgroup_count(const t_extent& a_work_dim, const t_extent& a_workgroup_size)
{
    return (a_work_dim + a_workgroup_size - t_extent(1u)) / a_workgroup_size;
}


inline uint32_t 
calc_num_texels(const VkExtent3D& a_dim)
{
    return a_dim.width * a_dim.height * a_dim.depth;
}

inline uint32_t 
calc_num_texels(const VkExtent2D& a_dim)
{
    return a_dim.width * a_dim.height;
}


template<typename T, typename U> inline constexpr std::enable_if_t<std::is_unsigned_v<T>&& std::is_unsigned_v<U>, T>
calc_padding(T a_dim, U a_alignment)
{
    static_assert(sizeof(T) >= sizeof(U), "length type cannot be narrower than alignment type");
    const auto remainder = a_dim % a_alignment;
    return (remainder != 0) * (a_alignment - remainder);
}


/// return: a_dim + padding
template<typename T, typename U> inline constexpr std::enable_if_t<std::is_unsigned_v<T> && std::is_unsigned_v<U>, T> 
calc_padded(T a_dim, U a_alignment)
{
    return a_dim + calc_padding(a_dim, a_alignment);
}


// last element of mip_texel_offsets: number of all mip texels
template<
    typename t_extent, 
    std::enable_if_t<std::is_same_v<t_extent, VkExtent2D> || std::is_same_v<t_extent, VkExtent3D>, bool> = true
>
std::vector<uint32_t> 
calc_mip_texel_offsets(const std::vector<t_extent>& a_mip_dims)
{
    std::vector<uint32_t> mip_texel_offsets(a_mip_dims.size() + 1u, 0u);
    for (size_t i = 0; i < a_mip_dims.size(); ++i) {
        mip_texel_offsets[i + 1] = calc_num_texels(a_mip_dims[i]) + mip_texel_offsets[i];
    }
    return mip_texel_offsets;
}


class host_timer_t {
public:
    using clock_type = std::chrono::high_resolution_clock;
    using ns_type = std::chrono::nanoseconds;
    host_timer_t() : start_{ clock_type::now() } {}
    void start() { start_ = clock_type::now(); }
    double elapsed_ns() const { return static_cast<double>(std::chrono::duration_cast<ns_type>(clock_type::now() - start_).count()); }
    double elapsed_us() const { return static_cast<double>(std::chrono::duration_cast<ns_type>(clock_type::now() - start_).count()) * 1.0e-3f; }
    double elapsed_ms() const { return static_cast<double>(std::chrono::duration_cast<ns_type>(clock_type::now() - start_).count()) * 1.0e-6f; }
    double elapsed_s()  const { return static_cast<double>(std::chrono::duration_cast<ns_type>(clock_type::now() - start_).count()) * 1.0e-9f; }

private:
    std::chrono::time_point<clock_type> start_;

};


class host_perf_timer_t {
public:
    host_perf_timer_t(const host_perf_timer_t&) = delete;
    host_perf_timer_t& operator=(const host_perf_timer_t&) = delete;
    host_perf_timer_t(host_perf_timer_t&&) = default;
    host_perf_timer_t& operator=(host_perf_timer_t&&) = default;

    host_perf_timer_t();
    host_perf_timer_t(uint32_t a_moving_average_filter_width, uint32_t a_frame_queue_len_max = 0);

    float elapsed_ms() const { return filtered_elapsed_ms_; }

    void start() { host_timer_.start(); }
    void stop();

private:
    uint32_t moving_average_filter_width_; // number of frames used to compute average
    uint32_t frame_queue_len_max_; // in terms of frames 

    uint32_t frame_queue_tail_idx_; // in terms of frames 
    uint32_t frame_queue_len_; // in terms of frames 
    float filtered_elapsed_ms_;

    host_timer_t host_timer_;
    std::unique_ptr<float[]> frame_queue_;
};


// initialize exactly once per process, not per thread
struct glslang_initializer_t {
    glslang_initializer_t();
    ~glslang_initializer_t();
};


// TODO: support non-default entry name and source entry name
const std::vector<uint32_t> spirv_from_glsl_src(
    const char* a_glsl_src_c_str,
    VkShaderStageFlagBits a_vk_flag_bits,
    uint32_t a_vulkan_api_version,
    const char* a_glsl_file_path_c_str = {},
    const std::vector<std::string>& a_glsl_inc_dirs = {},
    std::set<std::string>* a_includes = nullptr

);

std::set<std::string> get_glsl_included_files(
    const std::string& a_glsl_path_str, 
    uint32_t a_vulkan_api_version, 
    const std::vector<std::string>& a_glsl_inc_dirs = {}
);

/// return pair (included_paths, spirv_bin)
std::vector<uint32_t> spirv_from_glsl_file(const std::string& a_file_path, uint32_t a_vulkan_api_version);
std::vector<uint32_t> load_spirv_from_file(const std::filesystem::path& a_spirv_file_path);

std::string get_glsl_ext(const std::string& a_glsl_file_name);

VkShaderStageFlagBits vk_shader_stage_from_file_name(const std::string& a_glsl_file_name);

using file_time_t = std::filesystem::file_time_type;
using file_clock_t = file_time_t::clock;


// TODO: release spirv_bin_ after shader module created (move spirv_bin_ to shader module(s) ctor)
class spirv_t
{
private:

    std::vector<uint32_t> spirv_bin_;
    uint32_t vulkan_api_version_;
    file_time_t spirv_bin_timestamp_;
    VkShaderStageFlagBits shader_stage_flag_bits_;

    std::filesystem::path glsl_file_path_;
    std::filesystem::path spirv_file_path_;
    bool glsl_file_valid_;
    bool initialized_;

    bool load_prebuilt_spirv();
    
public:
    static constexpr char* const SPIRV_FILE_EXTENSION = ".spv";
    static constexpr uint32_t SPIRV_MAGIC_NUMBER = 0x07230203;
    static constexpr int CLIENT_INPUT_SEMANTICS_VERSION = 100; // glslang/StandAlone.cpp: Source environment: (source 'Client' is currently the same as target 'Client')
    static constexpr int DEFAULT_VERSION = 110; // TODO: glslang/StandAlone.cpp: use 100 for ES environment, 110 for desktop

    spirv_t(const spirv_t&) = delete;
    spirv_t& operator= (const spirv_t&) = delete;

    spirv_t(spirv_t&& a_spirv) noexcept;
    spirv_t& operator= (spirv_t&& a_spirv) noexcept;

    spirv_t() : vulkan_api_version_{}, shader_stage_flag_bits_{ VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM }, glsl_file_valid_{}, initialized_{} {}
    spirv_t(
        const std::string& a_glsl_file_path_str,
        uint32_t a_vulkan_api_version,
        const std::string& a_spirv_dir_path_str = "",
        const std::vector<std::string>& a_glsl_inc_dirs = {}
    );

    // return true: spirv_bin_ is updated/changed; false: spirv_bin_ is unchanged (already up-to-date or compile failed)
    // a_force_reload: force re-compile, can be set if shader is known to have changed to bypass unnecessary recursive header change detection 
    bool reload(bool a_force_reload = false, const std::vector<std::string>& a_glsl_inc_dirs = {});

    const bool valid() const { return shader_stage_flag_bits_ != VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM; }
    const bool glsl_file_valid() const { return glsl_file_valid_; }
    const std::vector<uint32_t>& spirv_bin() const { return spirv_bin_; }
    VkShaderStageFlagBits shader_stage_flag_bits() const { return shader_stage_flag_bits_; }
};


VKAPI_ATTR VkBool32 VKAPI_CALL debug_utils_messenger_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT,
    VkDebugUtilsMessageTypeFlagsEXT,
    const VkDebugUtilsMessengerCallbackDataEXT*,
    void*
);


} // namespace spock



