#pragma once

#if defined(__INTELLISENSE__)
#undef VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#else
#include <volk.h>
#endif

#include <vk_mem_alloc.h>
#include <glm/glm.hpp>

#include "generated.hpp"
#include "window.hpp"
#include <imgui.h>

#include <nlohmann/json_fwd.hpp>
#include <string>
#include <vector>
#include <array>
#include <memory>
#include <optional>
#include <unordered_map>
#include <unordered_set>

#if __cplusplus < 202002L
#define SPOCK_NODISCARD_REASON(x) 
#else
#define SPOCK_NODISCARD_REASON(x)  (x)
#endif 


inline bool operator== (const VkExtent2D& a, const VkExtent2D& b)
{
    return a.width == b.width && a.height == b.height;
}

inline bool operator== (const VkExtent3D& a, const VkExtent3D& b)
{
    return a.width == b.width && a.height == b.height && a.depth == b.depth;
}


namespace spock {

enum vertex_input_flag_bits_e : int32_t {
    VERTEX_INPUT_POSITION_BIT       = 0x00000001, 
    VERTEX_INPUT_NORMAL_BIT         = 0x00000002,
    VERTEX_INPUT_UV_BIT             = 0x00000004,
    VERTEX_INPUT_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF
};
using vertex_input_flags_t = int32_t;

class  acceleration_structure_t;
class  blas_builder_array_t;
class  buffer_t;
class  buffer_view_t;
class  command_buffer_array_t;
class  command_buffer_t;
class  command_pool_t;
class  context_t;
class  debug_utils_messenger_t;
class  descriptor_pool_t;
class  descriptor_set_layout_t;
class  dev_timer_array_t;
class  device_t;
class  fence_t;
class  framebuffer_t;
struct graphics_pipeline_create_info_helper_t;
class  image_t;
class  image_view_t;
struct index_binding_t;
class  instance_t;
class  physical_device_t;
struct physical_dev_all_features2_t;
struct physical_dev_feature2_wrapper_t;
class  pipeline_layout_t;
class  pipeline_t;
class  query_pool_t;
class  queue_t;
class  render_pass_t;
class  sampler_t;
struct sbt_entry_size_t;
class  semaphore_t;
class  shader_binding_set_t;
struct shader_group_handle_pool_t;
class  shader_module_array_t;
class  shader_module_t;
class  spirv_t;
class  surface_t;
class  swapchain_t;
class  tlas_builder_t;
struct vertex_bindings_t;
struct vertex_input_descriptions_t;


inline bool vk_mem_device_local(VkMemoryPropertyFlags a_flags) { return a_flags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT; }
inline bool vk_mem_host_visible(VkMemoryPropertyFlags a_flags) { return a_flags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT; }
inline bool vk_mem_host_coherent(VkMemoryPropertyFlags a_flags) { return a_flags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT; }
inline bool vk_mem_host_cached(VkMemoryPropertyFlags a_flags) { return a_flags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT; }
inline bool vk_mem_lazily_allocated(VkMemoryPropertyFlags a_flags) { return a_flags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT; }
inline bool vk_mem_protected(VkMemoryPropertyFlags a_flags) { return a_flags & VK_MEMORY_PROPERTY_PROTECTED_BIT; }


VkAccelerationStructureBuildRangeInfoKHR
build_VkAccelerationStructureBuildRangeInfoKHR(
    uint32_t    primitiveCount,
    uint32_t    primitiveOffset,
    uint32_t    firstVertex,
    uint32_t    transformOffset = 0
);

VkAccelerationStructureGeometryKHR
build_VkAccelerationStructureGeometryKHR(
    VkGeometryFlagsKHR                                  flags,
    const VkAccelerationStructureGeometryAabbsDataKHR&  aabbs,
    const void*                                         pNext = nullptr
);

VkAccelerationStructureGeometryKHR
build_VkAccelerationStructureGeometryKHR(
    VkGeometryFlagsKHR                                      flags,
    const VkAccelerationStructureGeometryInstancesDataKHR&  instances,
    const void*                                             pNext     = nullptr
);

VkAccelerationStructureGeometryKHR
build_VkAccelerationStructureGeometryKHR(
    VkGeometryFlagsKHR                                      flags,
    const VkAccelerationStructureGeometryTrianglesDataKHR&  triangles,
    const void*                                             pNext     = nullptr
);

VkAccelerationStructureGeometryAabbsDataKHR
build_VkAccelerationStructureGeometryAabbsDataKHR(
    VkDeviceSize                     stride,
    VkDeviceOrHostAddressConstKHR    data,
    const void*                      pNext = nullptr
);

VkAccelerationStructureGeometryInstancesDataKHR
build_VkAccelerationStructureGeometryInstancesDataKHR(
    VkDeviceOrHostAddressConstKHR    data,
    VkBool32                         arrayOfPointers = VK_FALSE,
    const void*                      pNext = nullptr
);

VkAccelerationStructureGeometryTrianglesDataKHR
build_VkAccelerationStructureGeometryTrianglesDataKHR(
    VkFormat                         vertexFormat,
    VkDeviceOrHostAddressConstKHR    vertexData,
    VkDeviceSize                     vertexStride,
    uint32_t                         maxVertex,
    VkIndexType                      indexType,
    VkDeviceOrHostAddressConstKHR    indexData,
    VkDeviceOrHostAddressConstKHR    transformData = { 0 },
    const void*                      pNext         = nullptr
);


// transform: glm::mt4 in column major
VkAccelerationStructureInstanceKHR
build_VkAccelerationStructureInstanceKHR(
    const glm::mat4&              transform,
    uint32_t                      instanceShaderBindingTableRecordOffset,
    VkGeometryInstanceFlagsKHR    flags                          = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR,
    uint64_t                      accelerationStructureReference = 0,
    uint32_t                      instanceCustomIndex            = 0,
    uint32_t                      mask                           = 0xFF
);


VkAttachmentDescription
build_VkAttachmentDescription(
    VkFormat                     format,
    VkImageLayout                finalLayout,
    VkAttachmentLoadOp           loadOp,
    VkAttachmentStoreOp          storeOp,
    VkSampleCountFlagBits        samples        = VK_SAMPLE_COUNT_1_BIT,
    VkImageLayout                initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED, // don't care about previous content
    VkAttachmentLoadOp           stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
    VkAttachmentStoreOp          stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
    VkAttachmentDescriptionFlags flags          = 0
);


inline VkAttachmentReference 
build_VkAttachmentReference(
    uint32_t        attachment, 
    VkImageLayout   layout
)
{ 
    return VkAttachmentReference{ attachment, layout }; 
}


VkBufferCopy
build_VkBufferCopy(
    VkDeviceSize size,
    VkDeviceSize srcOffset = 0,
    VkDeviceSize dstOffset = 0
);

VkBufferCreateInfo
build_VkBufferCreateInfo(
    VkDeviceSize           size,
    VkBufferUsageFlags     usage,
    VkBufferCreateFlags    flags                 = 0,
    VkSharingMode          sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
    uint32_t               queueFamilyIndexCount = 0,
    const uint32_t*        pQueueFamilyIndices   = nullptr,
    const void*            pNext                 = nullptr
);

VkCommandBufferBeginInfo
build_VkCommandBufferBeginInfo(
    VkCommandBufferUsageFlags             flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    const VkCommandBufferInheritanceInfo* pInheritanceInfo = nullptr,
    const void*                           pNext            = nullptr
);

VkComponentMapping
build_VkComponentMapping(
    VkComponentSwizzle r = VK_COMPONENT_SWIZZLE_IDENTITY,
    VkComponentSwizzle g = VK_COMPONENT_SWIZZLE_IDENTITY,
    VkComponentSwizzle b = VK_COMPONENT_SWIZZLE_IDENTITY,
    VkComponentSwizzle a = VK_COMPONENT_SWIZZLE_IDENTITY
);

VkComputePipelineCreateInfo
build_VkComputePipelineCreateInfo(
    const VkPipelineShaderStageCreateInfo&    stage,
    VkPipelineLayout                          layout,
    VkPipelineCreateFlags                     flags              = 0,
    VkPipeline                                basePipelineHandle = VK_NULL_HANDLE,
    int32_t                                   basePipelineIndex  = -1, /* vkspec-1.2.187: 10.6. Pipeline Derivatives */
    const void*                               pNext              = nullptr
);

VkDescriptorBufferInfo
build_VkDescriptorBufferInfo(
    VkBuffer       buffer,
    VkDeviceSize   offset,
    VkDeviceSize   range
);

VkDescriptorImageInfo
build_VkDescriptorImageInfo(
    VkSampler      sampler,
    VkImageView    imageView,
    VkImageLayout  imageLayout
);

VkDescriptorPoolCreateInfo
build_VkDescriptorPoolCreateInfo(
    uint32_t                       maxSets,
    uint32_t                       poolSizeCount,
    const VkDescriptorPoolSize*    pPoolSizes,
    VkDescriptorPoolCreateFlags    flags            = VkDescriptorPoolCreateFlags(),
    const void*                    pNext            = nullptr
);

inline VkDescriptorPoolSize
build_VkDescriptorPoolSize(
    VkDescriptorType    type,
    uint32_t            descriptorCount
)
{
    return VkDescriptorPoolSize{ type, descriptorCount };
}

VkDescriptorSetAllocateInfo
build_VkDescriptorSetAllocateInfo(
    VkDescriptorPool                descriptorPool,
    uint32_t                        descriptorSetCount,
    const VkDescriptorSetLayout*    pSetLayouts,
    const void*                     pNext                  = nullptr
);

VkDescriptorSetLayoutBinding
build_VkDescriptorSetLayoutBinding(
    uint32_t             binding,
    VkDescriptorType     descriptorType,
    uint32_t             descriptorCount,
    VkShaderStageFlags   stageFlags,
    const VkSampler*     pImmutableSamplers = nullptr
);

VkDescriptorSetLayoutCreateInfo
build_VkDescriptorSetLayoutCreateInfo(
    uint32_t                              bindingCount,
    const VkDescriptorSetLayoutBinding*   pBindings,
    VkDescriptorSetLayoutCreateFlags      flags             = 0
);

VkFramebufferCreateInfo
build_VkFramebufferCreateInfo(
    VkRenderPass                renderPass,
    uint32_t                    attachmentCount,
    const VkImageView*          pAttachments,
    const VkExtent2D&           extent,
    uint32_t                    layers = 1,
    VkFramebufferCreateFlags    flags = 0,
    const void*                 pNext = nullptr
);

VkShaderModuleCreateInfo
build_VkShaderModuleCreateInfo(
    size_t                       codeSize,
    const uint32_t*              pCode,
    VkShaderModuleCreateFlags    flags = 0,
    const void*                  pNext = nullptr
);


VkImageCreateInfo
build_VkImageCreateInfo(
    VkFormat                 format, 
    const VkExtent3D&        extent, 
    VkImageUsageFlags        usage, 
    uint32_t                 mipLevels             = 1, 
    VkSampleCountFlagBits    samples               = VK_SAMPLE_COUNT_1_BIT,
    VkImageTiling            tiling                = VK_IMAGE_TILING_OPTIMAL,
    uint32_t                 arrayLayers           = 1,
    VkSharingMode            sharingMode           = VK_SHARING_MODE_EXCLUSIVE,
    uint32_t                 queueFamilyIndexCount = 0,
    uint32_t*                pQueueFamilyIndices   = NULL,
    VkImageLayout            initialLayout         = VK_IMAGE_LAYOUT_UNDEFINED,
    const void*              pNext                 = nullptr
);


VkImageSubresourceLayers
build_VkImageSubresourceLayers(
    VkImageAspectFlags    aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
    uint32_t              mipLevel       = 0,
    uint32_t              layerCount     = 1,
    uint32_t              baseArrayLayer = 0
);


VkImageSubresourceRange
build_VkImageSubresourceRange(
    VkImageAspectFlags   aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
    uint32_t             levelCount     = 1,
    uint32_t             baseMipLevel   = 0,
    uint32_t             layerCount     = 1,
    uint32_t             baseArrayLayer = 0
);

// a_new_layout: if invalid (i.e. default value), set to be the same as a_old_layout
VkImageMemoryBarrier 
build_VkImageMemoryBarrier(
    VkImage                          image, 
    VkImageLayout                    oldLayout            = VK_IMAGE_LAYOUT_UNDEFINED,
    VkImageLayout                    newLayout            = VK_IMAGE_LAYOUT_MAX_ENUM,
    VkAccessFlags                    srcAccessMask        = VK_ACCESS_NONE_KHR,
    VkAccessFlags                    dstAccessMask        = VK_ACCESS_NONE_KHR,
    const VkImageSubresourceRange&   subresourceRange     = build_VkImageSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT),
    uint32_t                         srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED,
    uint32_t                         dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED,
    const void*                      pNext                = nullptr
);

// set next.oldLayout to prev.newLayout, set next.newLayout to a_new_layout
// won't overwrite if a_range, a_src_queue_family_idx or a_dst_queue_family_idx is invalid (i.e. default value)
void
advance_VkImageMemoryBarrier(
    VkImageMemoryBarrier*             barrier,
    VkImageLayout                     newLayout,
    VkAccessFlags                     srcAccessMask,
    VkAccessFlags                     dstAccessMask,
    const VkImageSubresourceRange&    subresourceRange    = build_VkImageSubresourceRange(VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM),
    uint32_t                          srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
    uint32_t                          dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED
);

VkImageViewCreateInfo
build_VkImageViewCreateInfo(
    VkImage                          image, 
    VkFormat                         format, 
    const VkImageSubresourceRange&   subresourceRange  = build_VkImageSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT),
    VkImageViewType                  viewType          = VK_IMAGE_VIEW_TYPE_2D,
    const VkComponentMapping&        components        = build_VkComponentMapping(),
    const void*                      pNext             = nullptr
);

VkImageViewCreateInfo
build_VkImageViewCreateInfo(
    const image_t&                   a_image, 
    const VkImageSubresourceRange&   subresourceRange  = build_VkImageSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT),
    VkImageViewType                  viewType          = VK_IMAGE_VIEW_TYPE_2D,
    const VkComponentMapping&        components        = build_VkComponentMapping(),
    const void*                      pNext             = nullptr
);

VkMemoryBarrier
build_VkMemoryBarrier(
    VkAccessFlags   srcAccessMask, 
    VkAccessFlags   dstAccessMask,
    const void*     pNext           = nullptr
);

VkPipelineColorBlendAttachmentState
build_VkPipelineColorBlendAttachmentState(
    VkColorComponentFlags    colorWriteMask,
    VkBool32                 blendEnable         =       VK_FALSE,
    VkBlendFactor            srcColorBlendFactor = VK_BLEND_FACTOR_ZERO,
    VkBlendFactor            dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
    VkBlendOp                colorBlendOp        = VK_BLEND_OP_ADD,
    VkBlendFactor            srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
    VkBlendFactor            dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
    VkBlendOp                alphaBlendOp        = VK_BLEND_OP_ADD
);

VkPipelineLayoutCreateInfo
build_VkPipelineLayoutCreateInfo(
    uint32_t                        setLayoutCount,
    const VkDescriptorSetLayout*    pSetLayouts,
    uint32_t                        pushConstantRangeCount     = 0,
    const VkPushConstantRange*      pPushConstantRanges        = nullptr,
    VkPipelineLayoutCreateFlags     flags                      = 0,
    const void*                     pNext                      = nullptr
);

VkPipelineShaderStageCreateInfo
build_VkPipelineShaderStageCreateInfo(
    VkShaderStageFlagBits               stage,
    VkShaderModule                      module,
    const VkSpecializationInfo*         pSpecializationInfo = nullptr,
    VkPipelineShaderStageCreateFlags    flags               = 0,
    const char*                         pName               = "main",
    const void*                         pNext               = nullptr
);

VkPushConstantRange
build_VkPushConstantRange(
    VkShaderStageFlags    stageFlags,
    uint32_t              size,
    uint32_t              offset = 0
);

VkRayTracingPipelineCreateInfoKHR
build_VkRayTracingPipelineCreateInfoKHR(
    VkPipelineLayout                                     layout,
    uint32_t                                             stageCount,
    const VkPipelineShaderStageCreateInfo*               pStages,
    uint32_t                                             groupCount,
    const VkRayTracingShaderGroupCreateInfoKHR*          pGroups,
    uint32_t                                             maxPipelineRayRecursionDepth,
    VkPipelineCreateFlags                                flags              = 0,
    const VkPipelineDynamicStateCreateInfo*              pDynamicState      = nullptr,
    const VkPipelineLibraryCreateInfoKHR*                pLibraryInfo       = nullptr,
    const VkRayTracingPipelineInterfaceCreateInfoKHR*    pLibraryInterface  = nullptr,
    VkPipeline                                           basePipelineHandle = VK_NULL_HANDLE,
    int32_t                                              basePipelineIndex  = -1,
    const void*                                          pNext              = nullptr
);

// VK_SHADER_UNUSED_KHR for unused index
VkRayTracingShaderGroupCreateInfoKHR
build_VkRayTracingShaderGroupCreateInfoKHR(
    VkRayTracingShaderGroupTypeKHR    type,
    uint32_t                          generalShader,
    uint32_t                          closestHitShader                = VK_SHADER_UNUSED_KHR,
    uint32_t                          anyHitShader                    = VK_SHADER_UNUSED_KHR,
    uint32_t                          intersectionShader              = VK_SHADER_UNUSED_KHR,
    const void*                       pShaderGroupCaptureReplayHandle = nullptr,
    const void*                       pNext                           = nullptr
);

// VK_SHADER_UNUSED_KHR for unused index
VkRayTracingShaderGroupCreateInfoKHR
build_VkRayTracingShaderGroupCreateInfoKHR_hit(
    uint32_t                          closestHitShader,
    uint32_t                          anyHitShader                    = VK_SHADER_UNUSED_KHR,
    uint32_t                          intersectionShader              = VK_SHADER_UNUSED_KHR,
    bool                              a_is_triangle_hit               = true, /* VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR if false */
    const void*                       pShaderGroupCaptureReplayHandle = nullptr,
    const void*                       pNext                           = nullptr
);

VkRayTracingShaderGroupCreateInfoKHR
build_VkRayTracingShaderGroupCreateInfoKHR_general(
    uint32_t                          generalShader,
    const void*                       pShaderGroupCaptureReplayHandle = nullptr,
    const void*                       pNext                           = nullptr
);

inline VkRect2D
build_VkRect2D(uint32_t a_width, uint32_t a_height, int32_t offset_x = 0, int32_t offset_y = 0)
{
    return VkRect2D{ {offset_x, offset_y}, {a_width, a_height} };
}

VkRenderPassCreateInfo
build_VkRenderPassCreateInfo(
    uint32_t                          attachmentCount,
    const VkAttachmentDescription*    pAttachments,
    uint32_t                          subpassCount,
    const VkSubpassDescription*       pSubpasses,
    uint32_t                          dependencyCount,
    const VkSubpassDependency*        pDependencies,
    VkRenderPassCreateFlags           flags = 0,
    const void*                       pNext = nullptr
);

inline VkSamplerCreateInfo 
build_VkSamplerCreateInfo(
    float                    maxAnisotropy,
    VkFilter                 magFilter                = VK_FILTER_LINEAR, 
    VkSamplerAddressMode     addressModeU             = VK_SAMPLER_ADDRESS_MODE_REPEAT, 
    VkSamplerMipmapMode      mipmapMode               = VK_SAMPLER_MIPMAP_MODE_LINEAR,
    VkFilter                 minFilter                = VK_FILTER_MAX_ENUM, 
    VkSamplerAddressMode     addressModeV             = VK_SAMPLER_ADDRESS_MODE_MAX_ENUM, 
    VkSamplerAddressMode     addressModeW             = VK_SAMPLER_ADDRESS_MODE_MAX_ENUM, 
    float                    minLod                   = 0.f,
    float                    maxLod                   = VK_LOD_CLAMP_NONE,
    float                    mipLodBias               = 0.f,
    VkBool32                 anisotropyEnable         = VK_TRUE,
    VkBool32                 unnormalizedCoordinates  = VK_FALSE,
    VkBool32                 compareEnable            = VK_FALSE,
    VkCompareOp              compareOp                = VK_COMPARE_OP_ALWAYS,
    VkBorderColor            borderColor              = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
    const void*              pNext                    = nullptr
);

// equivalent to texelFetch with mipLevel = 0
VkSamplerCreateInfo 
dummy_VkSamplerCreateInfo();


inline VkSpecializationInfo
build_VkSpecializationInfo(
    uint32_t                           mapEntryCount,
    const VkSpecializationMapEntry*    pMapEntries,
    size_t                             dataSize,
    const void*                        pData
)
{
    return VkSpecializationInfo{ mapEntryCount, pMapEntries, dataSize, pData };
};


VkStridedDeviceAddressRegionKHR
build_VkStridedDeviceAddressRegionKHR(
    VkDeviceAddress    deviceAddress,
    VkDeviceSize       stride,
    VkDeviceSize       size
);

VkSubmitInfo
build_VkSubmitInfo(
    uint32_t                     commandBufferCount,
    const VkCommandBuffer*       pCommandBuffers,
    uint32_t                     waitSemaphoreCount    = 0,
    const VkSemaphore*           pWaitSemaphores       = nullptr,
    const VkPipelineStageFlags*  pWaitDstStageMask     = nullptr,
    uint32_t                     signalSemaphoreCount  = 0,
    const VkSemaphore*           pSignalSemaphores     = nullptr,
    const void*                  pNext                 = nullptr
);

VkSubpassDependency
build_VkSubpassDependency(
    uint32_t                srcSubpass,
    uint32_t                dstSubpass,
    VkPipelineStageFlags    srcStageMask,
    VkPipelineStageFlags    dstStageMask,
    VkAccessFlags           srcAccessMask,
    VkAccessFlags           dstAccessMask,
    VkDependencyFlags       dependencyFlags = 0
);

VkSubpassDescription
build_VkSubpassDescription(
    uint32_t                        colorAttachmentCount,
    const VkAttachmentReference*    pColorAttachments,
    const VkAttachmentReference*    pDepthStencilAttachment,
    const VkAttachmentReference*    pResolveAttachments     = nullptr,
    uint32_t                        inputAttachmentCount    = 0,
    const VkAttachmentReference*    pInputAttachments       = nullptr,
    uint32_t                        preserveAttachmentCount = 0,
    const uint32_t*                 pPreserveAttachments    = nullptr,
    VkSubpassDescriptionFlags       flags                   = 0,
    VkPipelineBindPoint             pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS
);

VkViewport
build_VkViewport(
    float    width,
    float    height,
    float    x = 0.f,
    float    y = 0.f,
    float    minDepth = 0.f,
    float    maxDepth = 1.f
);

VkWriteDescriptorSet
build_VkWriteDescriptorSet(
    VkDescriptorSet                  dstSet,
    uint32_t                         dstBinding,
    uint32_t                         descriptorCount,
    VkDescriptorType                 descriptorType,
    const VkDescriptorImageInfo*     pImageInfo        = nullptr,
    const VkDescriptorBufferInfo*    pBufferInfo       = nullptr,
    const VkBufferView*              pTexelBufferView  = nullptr,
    uint32_t                         dstArrayElement   = 0,
    const void*                      pNext             = nullptr
);

VkWriteDescriptorSetAccelerationStructureKHR
build_VkWriteDescriptorSetAccelerationStructureKHR(
    uint32_t                             accelerationStructureCount,
    const VkAccelerationStructureKHR*    pAccelerationStructures,
    const void*                          pNext = nullptr
);

VmaAllocationCreateInfo
build_VmaAllocationCreateInfo(
    VkMemoryPropertyFlags       requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    VkMemoryPropertyFlags       preferredFlags = 0,
    VmaAllocationCreateFlags    flags = 0,
    VmaMemoryUsage              usage = VMA_MEMORY_USAGE_UNKNOWN,
    VmaPool                     pool = VK_NULL_HANDLE,
    void*                       pUserData = nullptr,
    float                       priority = 1.f,
    uint32_t                    memoryTypeBits = 0
);


// stage buffer remain being mapped after the function call returns
buffer_t stage(VkDevice a_vk_dev, VmaAllocator a_vma_allocator, const void* a_src, size_t a_bytes);
sbt_entry_size_t calc_sbt_entry_size(const VkPhysicalDeviceRayTracingPipelinePropertiesKHR& a_rt_ppl_properties, uint32_t a_shader_record_buffer_bytes = 0);


class instance_t {
    friend class context_t;

private:
    enum state_e{
        STATE_INSTANCE_LAYER_ON,
        STATE_VALIDATION_LAYER_ON,
        STATE_DEBUG_UTILS_MESSENGER_ON,
        STATE_VALIDATION_FEATURE_ON,
        STATE_DEBUG_PRINTF_ON,
        STATE_COUNT
    };

    VkInstance vk_instance_;
    VkDebugUtilsMessengerCreateInfoEXT debug_utils_messenger_create_info_;
    uint32_t vk_api_version_;
    std::array<bool, STATE_COUNT> states_;

    instance_t(instance_t&&) noexcept;
    instance_t& operator=(instance_t&&) noexcept;

    instance_t() : vk_instance_{ VK_NULL_HANDLE }, vk_api_version_{}, states_{} {}
    instance_t(
        const ordered_json& a_vk_instance_config_json,
        const std::vector<const char*>& a_window_extensions = {},
        PFN_vkDebugUtilsMessengerCallbackEXT a_messenger_callback = nullptr,
        void* a_messenger_user_data = nullptr
    );

public:

    instance_t(const instance_t&) = delete;
    instance_t& operator=(const instance_t&) = delete;

    ~instance_t();
    
    operator VkInstance () const { return vk_instance_; }
    uint32_t vk_api_version() const { return vk_api_version_; }
    const VkDebugUtilsMessengerCreateInfoEXT& debug_utils_messenger_create_info() const { return debug_utils_messenger_create_info_; }

    bool is_validation_layer_on() const { return states_[STATE_VALIDATION_LAYER_ON]; }
    bool is_debug_utils_messenger_on() const { return states_[STATE_DEBUG_UTILS_MESSENGER_ON]; }
    bool is_debug_printf_on() const { return states_[STATE_DEBUG_PRINTF_ON]; }
    bool is_debugging_off() const; 

};


class debug_utils_messenger_t {
    friend class context_t;

private:
    VkInstance vk_instance_;
    VkDebugUtilsMessengerEXT vk_debug_utils_messenger_;

    debug_utils_messenger_t(debug_utils_messenger_t&&) noexcept;
    debug_utils_messenger_t& operator=(debug_utils_messenger_t&&) noexcept;

    debug_utils_messenger_t() : vk_instance_{ VK_NULL_HANDLE }, vk_debug_utils_messenger_{ VK_NULL_HANDLE } {}
    debug_utils_messenger_t(VkInstance a_vk_instance, const VkDebugUtilsMessengerCreateInfoEXT& a_create_info);

public:
    debug_utils_messenger_t(const debug_utils_messenger_t&) = delete;
    debug_utils_messenger_t& operator=(const debug_utils_messenger_t&) = delete;

    ~debug_utils_messenger_t();

};


class surface_t {
    friend class context_t;
private:
    VkSurfaceKHR vk_surface_;
    VkInstance vk_instance_;

    surface_t(surface_t&&) noexcept;
    surface_t& operator=(surface_t&&) noexcept;

    surface_t() : vk_instance_(VK_NULL_HANDLE), vk_surface_(VK_NULL_HANDLE) {}

    surface_t(VkInstance a_vk_instance, VkSurfaceKHR a_vk_surface)
        : vk_instance_(a_vk_instance)
        , vk_surface_(a_vk_surface)
    {}

public:
    surface_t(const surface_t&) = delete;
    surface_t& operator=(const surface_t&) = delete;

    ~surface_t();

    operator VkSurfaceKHR () const { return vk_surface_; }
    bool valid() const { return vk_surface_ != VK_NULL_HANDLE; }
};


class physical_device_t {
private:
    friend class context_t;

    VkPhysicalDevice vk_physical_dev_;
    std::unique_ptr<VkPhysicalDeviceProperties> properties_; // allocate on the heap to reduce stack size
    std::unique_ptr<VkPhysicalDeviceMemoryProperties> memory_properties_; // allocate on the heap to reduce stack size
    std::unordered_set<std::string> available_extensions_;

    physical_device_t(physical_device_t&&) noexcept;
    physical_device_t& operator=(physical_device_t&&) noexcept;

    physical_device_t() : vk_physical_dev_{ VK_NULL_HANDLE } {}
    physical_device_t(const std::vector<VkPhysicalDevice>& a_physical_devices, const ordered_json& a_vk_physical_dev_config_json);

public:
    physical_device_t(const physical_device_t&) = delete;
    physical_device_t& operator=(const physical_device_t&) = delete;
    
    operator VkPhysicalDevice () const { return vk_physical_dev_; }

    const char* name() const { return properties_->deviceName; }
    const std::unordered_set<std::string>& available_extensions() const { return available_extensions_; }

    std::string get_available_extension_list() const;
    bool has_extension(const std::string& a_extension_name) const;

};


/**
* It is the user's responsibility to make sure queues are used for the intended device, 
* and not used concurrently in different threads
*/
class queue_t {
private:
    friend class device_t;

    uint32_t family_index_;
    uint32_t queue_index_;
    VkQueue vk_queue_;

    queue_t(VkDevice a_vk_device, uint32_t a_family_index, uint32_t a_queue_index);

public:
    static const VkQueueFlags FLAGS_GENERAL = VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT;
    static const VkQueueFlags FLAGS_LESS_GENERAL = VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT;

    queue_t() 
        : family_index_{ static_cast<uint32_t>(-1) }
        , queue_index_{ static_cast<uint32_t>(-1) }
        , vk_queue_{ VK_NULL_HANDLE } 
    {}

    queue_t(const queue_t&) = default;
    queue_t& operator=(const queue_t&) = default;

    queue_t(queue_t&&) = default;
    queue_t& operator=(queue_t&&) = default;

    operator VkQueue () const { return vk_queue_; }
    uint32_t family_index() const { return family_index_; }

    bool valid() const { return vk_queue_ != VK_NULL_HANDLE; }

};


class device_t {
    friend class context_t;
private:
    constexpr static uint32_t num_created_queues_default = 1;
    constexpr static float queue_priority_default = 1.f;
    constexpr static const char* general_queue_name = "general";
    constexpr static const char* compute_queue_name = "compute";
    constexpr static const char* transfer_queue_name = "transfer";

    class queue_family_t {
    private:
        friend class device_t;

        VkQueueFamilyProperties properties_;
        uint32_t family_index_;
        uint32_t num_created_queues_;
        uint32_t num_used_queues_;
        VkBool32 surface_supported_;

    public:
        queue_family_t(const queue_family_t&) = delete;
        queue_family_t& operator=(const queue_family_t&) = delete;

        queue_family_t(queue_family_t&&) = default;
        queue_family_t& operator=(queue_family_t&&) = default;

        queue_family_t(
            uint32_t a_family_index, 
            const VkQueueFamilyProperties& a_properties,
            VkBool32 a_surface_supported = VK_FALSE
        );

        uint32_t num_queues_max() const { return properties_.queueCount; }

        /// not thread-safe
        uint32_t next_queue_index();

    }; // class queue_family_t

    VkDevice vk_dev_;
    std::vector<queue_family_t> queue_families_;
    std::optional<uint32_t> queue_family_general_index_; // general queue family must support surface
    std::optional<uint32_t> queue_family_compute_index_;
    std::optional<uint32_t> queue_family_transfer_index_;

    device_t(device_t&&) noexcept;
    device_t& operator=(device_t&&) noexcept;

    device_t() : vk_dev_{ VK_NULL_HANDLE } {}
    device_t(
        const instance_t& a_instance,
        const physical_device_t& a_physical_dev,
        const ordered_json& a_vk_dev_config_json,
        physical_dev_all_features2_t& a_all_features2,
        VkSurfaceKHR a_vk_surface = VK_NULL_HANDLE,
        std::vector<physical_dev_feature2_wrapper_t>* a_features2_chain = nullptr
    );

    /// called after general queue family is found
    void find_dedicated_queue_families(); 

    /// family index, num, priority
    struct queue_family_info_t { uint32_t family_idx; uint32_t queue_count; float priority; };
    using enabled_queue_families_t = std::unordered_map<std::string, queue_family_info_t>;
    
    /// called after find_dedicated_queue_families
    enabled_queue_families_t get_enabled_queue_families(const ordered_json& a_queue_config_json);

public:
    
    device_t(const device_t&) = delete;
    device_t& operator=(const device_t&) = delete;

    ~device_t();

    operator VkDevice () const { return vk_dev_; }

    /// not thread-safe
    queue_t get_queue(VkQueueFlags a_requested_queue_flags = queue_t::FLAGS_GENERAL, VkBool32 a_surface_supported = VK_TRUE);
}; // class device_t


/// The constructors are called in the order in which the members are declared in the class
/// rather than the order in which the members appear in the initializer list
class context_t {
private:
    std::unique_ptr<const ordered_json> config_json_;
    instance_t instance_;
    debug_utils_messenger_t debug_utils_messenger_;
    surface_t surface_;
    physical_device_t physical_dev_;
    device_t device_;

    VmaAllocator alloc_;

public:
    context_t(const context_t&) = delete;
    context_t& operator=(const context_t&) = delete;

    context_t(context_t&& a_context) = delete;
    context_t& operator=(context_t&& a_context) = delete;

    context_t(
        const ordered_json& a_config_json,
        physical_dev_all_features2_t& a_all_features2,
        const window_t* a_window = nullptr,
        std::vector<physical_dev_feature2_wrapper_t>* a_features2_chain = nullptr,
        PFN_vkDebugUtilsMessengerCallbackEXT a_debug_utils_messenger_callback = nullptr,
        void* a_debug_utils_messenger_user_data = nullptr
    );
    ~context_t();

    operator VkDevice () const { return device_; }
    operator VmaAllocator () const { return alloc_; }

    const instance_t& instance() const { return instance_; }
    const ordered_json& config_json() const { return *config_json_; }
    VkInstance vk_instance() const { return instance_; }
    VkSurfaceKHR vk_surface() const { return surface_; }
    VkPhysicalDevice vk_physical_dev() const { return physical_dev_; }
    VkDevice vk_dev() const { return device_; }

    uint32_t vk_api_version() const { return instance_.vk_api_version(); }
    bool is_debugging_off() const { return instance_.is_debugging_off(); }

    const char* physical_dev_name() const { return physical_dev_.name(); }

    const VkPhysicalDeviceLimits& limits() const { return physical_dev_.properties_->limits; }
    float max_sampler_anisotropy() const { return physical_dev_.properties_->limits.maxSamplerAnisotropy; }
    float max_uniform_buffer_range() const { return physical_dev_.properties_->limits.maxUniformBufferRange; }
    VkPhysicalDeviceLimits physical_dev_limits() const { return physical_dev_.properties_->limits; }

    queue_t get_queue(VkQueueFlags a_requested_queue_flags = queue_t::FLAGS_GENERAL, VkBool32 a_surface_supported = VK_TRUE) { return device_.get_queue(a_requested_queue_flags, a_surface_supported); }

    VkSampleCountFlagBits sample_count_flag_bits_max() const;
    VkFormat supported_format(const std::vector<VkFormat>& a_format_cadidates, VkFormatFeatureFlags a_format_features, VkImageTiling a_image_tiling = VK_IMAGE_TILING_MAX_ENUM) const;
    bool format_features_supported(VkFormatFeatureFlags a_format_features, VkFormat a_format, VkImageTiling a_image_tiling) const;
    std::string image_format_features_str(VkFormat a_format, VkImageTiling a_image_tiling) const;
    void get_physical_dev_properties2(void* a_next_ptr) const;
    VkDeviceAddress get_buffer_device_address(VkBuffer a_buffer) const;

    sbt_entry_size_t calc_sbt_entry_size(uint32_t a_shader_record_buffer_bytes = 0);

};


class semaphore_t {
private:
    VkDevice vk_dev_; // does not own the resource
    VkSemaphore vk_semaphore_;

public:
    semaphore_t() : vk_dev_ { VK_NULL_HANDLE }, vk_semaphore_{ VK_NULL_HANDLE }{}

    semaphore_t(const semaphore_t& ) = delete;
    semaphore_t& operator=(const semaphore_t& ) = delete;

    semaphore_t(semaphore_t&& a_semaphore) noexcept;
    semaphore_t& operator= (semaphore_t&& a_semaphore) noexcept;
    
    /// VkSemaphoreCreateFlags: reserved for future use
    semaphore_t(VkDevice a_vk_dev, VkSemaphoreCreateFlags a_vk_semaphore_create_flags = {});
    ~semaphore_t();

    operator VkSemaphore() const { return vk_semaphore_; }
    const VkSemaphore& operator()() const { return vk_semaphore_; }
    bool valid() const { return vk_semaphore_ != VK_NULL_HANDLE; }
};


class fence_t
{
private:
    VkDevice vk_dev_; // does not own the resource
    VkFence  vk_fence_;

public:
    fence_t() : vk_dev_{ VK_NULL_HANDLE }, vk_fence_{ VK_NULL_HANDLE } {}

    fence_t(const fence_t& ) = delete;
    fence_t& operator=(const fence_t& ) = delete;

    fence_t(fence_t&& a_fence) noexcept;
    fence_t& operator= (fence_t&& a_fence) noexcept;
    
    fence_t(VkDevice a_vk_dev, VkFenceCreateFlags a_vk_fence_create_flags);
    ~fence_t();

    operator VkFence () const { return vk_fence_; }
    const VkFence& operator()() const { return vk_fence_; }
    bool valid() const { return vk_fence_ != VK_NULL_HANDLE; }
};


class command_pool_t {
private:
    VkDevice vk_dev_;
    VkCommandPool vk_command_pool_;
    uint32_t queue_family_index_;
    
public:
    command_pool_t() : vk_dev_{ VK_NULL_HANDLE }, vk_command_pool_{ VK_NULL_HANDLE }, queue_family_index_{ ~0u } {}

    command_pool_t(const command_pool_t& a_command_pool) = delete;
    command_pool_t& operator= (const command_pool_t& a_command_pool) = delete;
    
    command_pool_t(command_pool_t&& a_command_pool) noexcept;
    command_pool_t& operator= (command_pool_t&& a_command_pool) noexcept;
    command_pool_t(VkDevice a_vk_dev, const VkCommandPoolCreateInfo& a_command_pool_create_info);
    command_pool_t(VkDevice a_vk_dev, uint32_t a_queue_family_index, VkCommandPoolCreateFlags a_flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
    ~command_pool_t();

    operator VkCommandPool () const { return vk_command_pool_; }
    VkDevice vk_dev() const { return vk_dev_; }
    bool valid() const { return vk_command_pool_ != VK_NULL_HANDLE; }
    uint32_t queue_family_index() const { return queue_family_index_; }

};


class command_buffer_t {
private:
    VkDevice vk_dev_;
    VkCommandPool vk_command_pool_;
    VkCommandBuffer vk_command_buffer_;

public:
    command_buffer_t(const command_buffer_t&) = delete;
    command_buffer_t& operator= (const command_buffer_t&) = delete;

    command_buffer_t(command_buffer_t&&) noexcept;
    command_buffer_t& operator= (command_buffer_t&&) noexcept;

    command_buffer_t() : vk_dev_{ VK_NULL_HANDLE }, vk_command_pool_ { VK_NULL_HANDLE }, vk_command_buffer_{ VK_NULL_HANDLE }{}
    command_buffer_t(const command_pool_t& a_command_pool, VkCommandBufferLevel a_level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
    ~command_buffer_t();

    operator VkCommandBuffer () const { return vk_command_buffer_; }
    const VkCommandBuffer& operator()() const { return vk_command_buffer_; }
    bool valid() const { return vk_command_buffer_ != VK_NULL_HANDLE; }
};


class command_buffer_array_t {
private:
    VkDevice vk_dev_;
    VkCommandPool vk_command_pool_;
    std::vector<VkCommandBuffer> vk_command_buffers_;

public:
    command_buffer_array_t(const command_buffer_array_t&) = delete;
    command_buffer_array_t& operator= (const command_buffer_array_t&) = delete;

    command_buffer_array_t(command_buffer_array_t&&) noexcept;
    command_buffer_array_t& operator= (command_buffer_array_t&&) noexcept;

    command_buffer_array_t() : vk_dev_{ VK_NULL_HANDLE }, vk_command_pool_ { VK_NULL_HANDLE } {}
    command_buffer_array_t(const command_pool_t& a_command_pool, uint32_t a_length, VkCommandBufferLevel a_level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
    ~command_buffer_array_t();

    uint32_t length() const { return static_cast<uint32_t>(vk_command_buffers_.size()); }
    const VkCommandBuffer& operator[] (uint32_t a_command_buffer_idx) const { return vk_command_buffers_[a_command_buffer_idx]; }
    const VkCommandBuffer& at(uint32_t a_command_buffer_idx) const;
};


class descriptor_set_layout_t {
private:
    VkDevice vk_dev_;
    VkDescriptorSetLayout vk_descriptor_set_layout_;

public:
    descriptor_set_layout_t(const descriptor_set_layout_t&) = delete;
    descriptor_set_layout_t& operator= (const descriptor_set_layout_t&) = delete;

    descriptor_set_layout_t(descriptor_set_layout_t&& a_descriptor_set_layout) noexcept;
    descriptor_set_layout_t& operator= (descriptor_set_layout_t&& a_descriptor_set_layout) noexcept;

    descriptor_set_layout_t() : vk_dev_{ VK_NULL_HANDLE }, vk_descriptor_set_layout_{ VK_NULL_HANDLE } {}
    descriptor_set_layout_t(VkDevice, const VkDescriptorSetLayoutCreateInfo&);
    ~descriptor_set_layout_t();

    operator VkDescriptorSetLayout () const { return vk_descriptor_set_layout_; }
    const VkDescriptorSetLayout& operator()() const { return vk_descriptor_set_layout_; }
    bool valid() const {return vk_descriptor_set_layout_ != VK_NULL_HANDLE; }
};


class descriptor_pool_t {
private:
    VkDevice vk_dev_;
    VkDescriptorPool vk_descriptor_pool_;

public:
    descriptor_pool_t(const descriptor_pool_t&) = delete;
    descriptor_pool_t& operator= (const descriptor_pool_t&) = delete;

    descriptor_pool_t(descriptor_pool_t&& a_descriptor_pool) noexcept;
    descriptor_pool_t& operator= (descriptor_pool_t&& a_descriptor_pool) noexcept;

    descriptor_pool_t() : vk_dev_{ VK_NULL_HANDLE }, vk_descriptor_pool_{ VK_NULL_HANDLE } {}
    descriptor_pool_t(VkDevice, const VkDescriptorPoolCreateInfo&);
    ~descriptor_pool_t();

    operator VkDescriptorPool () const { return vk_descriptor_pool_; }
    bool valid() const { return vk_descriptor_pool_ != VK_NULL_HANDLE; }

    void alloc_descriptor_sets(uint32_t a_layout_count, const VkDescriptorSetLayout* a_layouts, VkDescriptorSet* a_descriptor_sets);
    void alloc_descriptor_set(const VkDescriptorSetLayout& a_layout, VkDescriptorSet* a_descriptor_set);
};


class shader_module_t {
private:
    VkDevice vk_dev_;
    VkShaderStageFlagBits vk_shader_stage_;
    VkShaderModule vk_shader_module_;

public:
    shader_module_t(const shader_module_t&) = delete;
    shader_module_t& operator= (const shader_module_t&) = delete;

    shader_module_t(shader_module_t&& a_shader_module) noexcept;
    shader_module_t& operator= (shader_module_t&& a_shader_module) noexcept;
    
    shader_module_t() : vk_dev_{ VK_NULL_HANDLE }, vk_shader_stage_{VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM}, vk_shader_module_{ VK_NULL_HANDLE } {}
    shader_module_t(VkDevice a_vk_dev, VkShaderStageFlagBits a_vk_shader_stage, const std::vector<uint32_t>& a_spirv);
    ~shader_module_t();

    operator VkShaderModule () const { return vk_shader_module_; }
    VkShaderStageFlagBits shader_stage() const { return vk_shader_stage_; }
    bool valid() const { return vk_shader_module_ != VK_NULL_HANDLE; }

};


class shader_module_array_t {
private:
    std::unique_ptr<VkShaderModule[]> vk_shader_modules_;
    std::unique_ptr<VkShaderStageFlagBits[]> vk_shader_stages_;
    VkDevice vk_dev_;
    uint32_t length_;

public:

    shader_module_array_t(const shader_module_array_t&) = delete;
    shader_module_array_t& operator= (const shader_module_array_t&) = delete;

    shader_module_array_t(shader_module_array_t&&) noexcept;
    shader_module_array_t& operator= (shader_module_array_t&&) noexcept;
    
    shader_module_array_t() : vk_dev_{ VK_NULL_HANDLE }, length_{} {}
    shader_module_array_t(
        VkDevice a_vk_dev, 
        const spirv_t* a_spirv_begin, 
        uint32_t a_spirv_count,
        VkShaderModuleCreateFlags a_flags = 0,
        void const* const* a_next_pptr = nullptr
    );
    ~shader_module_array_t();

    uint32_t length() const { return length_; }
    const VkShaderModule* vk_shader_modules() const { return vk_shader_modules_.get(); }
    const VkShaderStageFlagBits* vk_shader_stages() const { return vk_shader_stages_.get(); }

};


std::unique_ptr<VkPipelineShaderStageCreateInfo[]> build_shader_stage_create_infos(
    const shader_module_array_t&         a_shader_modules, 
    const VkSpecializationInfo*          a_specialization_info  = nullptr,
    VkPipelineShaderStageCreateFlags     a_flags                = 0,
    const char*                          a_name                 = "main",
    const void* const *                  a_next_pptr            = nullptr
);


class pipeline_layout_t {
private:
    VkDevice vk_dev_;
    VkPipelineLayout vk_pipeline_layout_;

public:
    pipeline_layout_t(const pipeline_layout_t&) = delete;
    pipeline_layout_t& operator= (const pipeline_layout_t&) = delete;

    pipeline_layout_t(pipeline_layout_t&& a_pipeline_layout) noexcept;
    pipeline_layout_t& operator= (pipeline_layout_t&& a_pipeline_layout) noexcept;

    pipeline_layout_t() : vk_dev_{ VK_NULL_HANDLE }, vk_pipeline_layout_{ VK_NULL_HANDLE } {}
    pipeline_layout_t(VkDevice, const VkPipelineLayoutCreateInfo&);
    ~pipeline_layout_t();

    operator VkPipelineLayout() const { return vk_pipeline_layout_; }
    bool valid() const { return vk_pipeline_layout_ != VK_NULL_HANDLE; }
};


class pipeline_t {
private:
    VkDevice vk_dev_;
    VkPipeline vk_pipeline_;
public:
    pipeline_t(const pipeline_t&) = delete;
    pipeline_t& operator= (const pipeline_t&) = delete;

    pipeline_t(pipeline_t&& a_pipeline) noexcept;
    pipeline_t& operator= (pipeline_t&& a_pipeline) noexcept;

    pipeline_t() : vk_dev_{ VK_NULL_HANDLE }, vk_pipeline_{ VK_NULL_HANDLE } {}
    pipeline_t(VkDevice a_vk_dev, VkPipeline a_vk_pipeline) : vk_dev_{ a_vk_dev }, vk_pipeline_{ a_vk_pipeline } {}
    pipeline_t(VkDevice a_vk_dev, const VkComputePipelineCreateInfo& a_vk_create_info);
    pipeline_t(VkDevice a_vk_dev, const VkGraphicsPipelineCreateInfo& a_vk_create_info);
    ~pipeline_t();

    operator VkPipeline() const { return vk_pipeline_; }
    bool valid() const { return vk_pipeline_ != VK_NULL_HANDLE; }

};


struct vertex_input_descriptions_t {
    std::vector<VkVertexInputBindingDescription> bindings; 
    std::vector<VkVertexInputAttributeDescription> attributes;
};


struct graphics_pipeline_create_info_helper_t {
    VkPipelineCreateFlags create_flags;

    vertex_input_descriptions_t vertex_input_descriptions;
    VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info;

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info;
    VkPipelineTessellationStateCreateInfo tessellation_state_create_info;

    std::vector<VkViewport> viewports;
    std::vector<VkRect2D> scissors;
    VkPipelineViewportStateCreateInfo viewport_state_create_info;

    VkPipelineRasterizationStateCreateInfo rasterization_state_create_info;

    VkPipelineMultisampleStateCreateInfo multisample_state_create_info;

    VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info;

    std::vector<VkPipelineColorBlendAttachmentState> color_blend_attachments;
    VkPipelineColorBlendStateCreateInfo color_blend_state_create_info;

    std::vector<VkDynamicState> dynamic_states;
    VkPipelineDynamicStateCreateInfo dynamic_state_create_info;

    VkPipeline base_pipeline_handle;
    int32_t base_pipeline_index;

    /// add viewport and scissor state to dynamic states if viewport width and/or view height is/are 0
    graphics_pipeline_create_info_helper_t(uint32_t a_viewport_width = 0, uint32_t a_viewport_height = 0);

    VkGraphicsPipelineCreateInfo resolve(
        VkDevice a_vk_dev, VkRenderPass a_render_pass, uint32_t a_subpass, const VkPipelineLayout a_vk_pipeline_layout, 
        uint32_t a_num_shader_stages, const VkPipelineShaderStageCreateInfo* a_shader_stage_create_info_begin
    );
};


/*
default graphics pipeline state:

    input_assembly_state_create_info.topology               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    viewport.x        = 0.f;
    viewport.y        = 0.f;
    viewport.width    = static_cast<float>(a_viewport_width);
    viewport.height   = static_cast<float>(a_viewport_height);
    viewport.minDepth = 0.f;
    viewport.maxDepth = 1.f;

    scissor.offset = { 0, 0 };
    scissor.extent = VkExtent2D{ a_viewport_width, a_viewport_height };

    rasterization_state_create_info.depthClampEnable        = VK_FALSE;
    rasterization_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    rasterization_state_create_info.polygonMode             = VK_POLYGON_MODE_FILL;
    rasterization_state_create_info.cullMode                = VK_CULL_MODE_BACK_BIT;
    rasterization_state_create_info.frontFace               = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterization_state_create_info.depthBiasEnable         = VK_FALSE;

    multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisample_state_create_info.sampleShadingEnable  = VK_FALSE;

    depth_stencil_state_create_info.depthTestEnable       = VK_TRUE;
    depth_stencil_state_create_info.depthWriteEnable      = VK_TRUE;
    depth_stencil_state_create_info.depthCompareOp        = VK_COMPARE_OP_LESS;
    depth_stencil_state_create_info.depthBoundsTestEnable = VK_FALSE;
    depth_stencil_state_create_info.stencilTestEnable     = VK_FALSE;

    color_blend_attachment.colorWriteMask       = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    color_blend_attachment.blendEnable          = VK_FALSE;
    float* blend_constants                      = color_blend_state_create_info.blendConstants;
    std::fill(blend_constants, blend_constants + 4, 0.f);

    if (a_viewport_width == 0 || a_viewport_height == 0) {
        dynamic_states.push_back(VK_DYNAMIC_STATE_VIEWPORT);
        dynamic_states.push_back(VK_DYNAMIC_STATE_SCISSOR);
    }

*/


void 
create_compute_pipelines(
    VkDevice a_vk_dev, 
    uint32_t a_num_create_infos, 
    const VkComputePipelineCreateInfo* a_vk_create_infos, 
    pipeline_t* a_pipeline_begin,
    VkPipelineCache a_pipeline_cache = VK_NULL_HANDLE
);

void 
create_graphics_pipelines(
    VkDevice a_vk_dev, 
    uint32_t a_num_create_infos, 
    const VkGraphicsPipelineCreateInfo* a_vk_create_infos, 
    pipeline_t* a_pipeline_begin,
    VkPipelineCache a_pipeline_cache = VK_NULL_HANDLE
);


struct sbt_entry_size_t {
    uint32_t shader_group_handle_size; // in bytes
    uint32_t stride; // sbt stride in bytes
};


struct shader_group_handle_pool_t {
    std::unique_ptr<uint8_t[]> data;
    std::unique_ptr<uint32_t[]> byte_offsets; // array length: pipeline_count + 1; for each ray tracing pipeline, last element: total bytes
    uint32_t pipeline_count{};

    // a_shader_group_idx_offsets[a_pipeline_count] should also be valid
    shader_group_handle_pool_t() {}
    shader_group_handle_pool_t(uint32_t a_pipeline_count, const uint32_t* a_shader_group_idx_offsets, uint32_t a_shader_group_handle_size);
    uint32_t data_size_in_bytes() const { return byte_offsets[pipeline_count]; }
};


// a_group_handle_data_byte_offsets[a_create_info_count] should also be valid
void
create_raytracing_pipelines(
    VkDevice a_vk_dev,
    uint32_t a_create_info_count,
    const VkRayTracingPipelineCreateInfoKHR* a_vk_create_infos, 
    const uint32_t* a_group_handle_data_byte_offsets, 
    pipeline_t* a_pipeline_begin,
    uint8_t* a_group_handle_data_begin,
    VkDeferredOperationKHR a_deferred_operation = VK_NULL_HANDLE,
    VkPipelineCache a_pipeline_cache = VK_NULL_HANDLE
);


struct vertex_bindings_t {
    uint32_t first{};
    uint32_t count{};
    std::vector<VkBuffer> buffers;
    std::vector<VkDeviceSize> buffer_offsets;
};


struct index_binding_t {
    VkBuffer buffer{};
    VkDeviceSize offset{};
    VkIndexType index_type{ VK_INDEX_TYPE_MAX_ENUM };
    VkDeviceSize count{};
};


class framebuffer_t {
private:
    VkDevice vk_dev_;
    VkFramebuffer vk_framebuffer_;

public:
    framebuffer_t(const framebuffer_t&) = delete;
    framebuffer_t& operator= (const framebuffer_t&) = delete;

    framebuffer_t(framebuffer_t&& a_framebuffer) noexcept;
    framebuffer_t& operator= (framebuffer_t&& a_framebuffer) noexcept;

    framebuffer_t() : vk_dev_{ VK_NULL_HANDLE }, vk_framebuffer_{ VK_NULL_HANDLE } {}
    framebuffer_t(VkDevice, const VkFramebufferCreateInfo&);
    ~framebuffer_t();

    operator VkFramebuffer() const { return vk_framebuffer_; }
    bool valid() const { return vk_framebuffer_ != VK_NULL_HANDLE; }
};


class render_pass_t {
private:
    VkDevice vk_dev_;
    VkRenderPass vk_render_pass_;

public:
    render_pass_t(const render_pass_t&) = delete;
    render_pass_t& operator= (const render_pass_t&) = delete;

    render_pass_t(render_pass_t&& a_render_pass) noexcept;
    render_pass_t& operator= (render_pass_t&& a_render_pass) noexcept;

    render_pass_t() : vk_dev_{ VK_NULL_HANDLE }, vk_render_pass_{ VK_NULL_HANDLE } {}
    render_pass_t(VkDevice, const VkRenderPassCreateInfo&);
    ~render_pass_t();

    operator VkRenderPass() const { return vk_render_pass_; }
    bool valid() const { return vk_render_pass_ != VK_NULL_HANDLE; }
};


namespace impl {

class alloc_t {
protected:
    VkDevice vk_dev_;
    VmaAllocator vma_allocator_;
    VmaAllocation vma_allocation_;
    VkMemoryPropertyFlags flags_;
    void* mapped_ptr_;

public:

    alloc_t(alloc_t&& a_alloc) noexcept;
    alloc_t& operator= (alloc_t&& a_alloc) noexcept;

    alloc_t(const alloc_t& a_alloc) = delete;
    alloc_t& operator= (const alloc_t& a_alloc) = delete;

    alloc_t();
    alloc_t(const context_t& a_context);
    alloc_t(VkDevice a_vk_dev, VmaAllocator a_vma_allocator);

    bool mappable() const { return vk_mem_host_visible(flags_); }
    bool device_local() const { return vk_mem_device_local(flags_); }
    bool host_visible() const { return vk_mem_host_visible(flags_); }
    bool host_coherent() const { return vk_mem_host_coherent(flags_); }
    bool host_cached() const { return vk_mem_host_cached(flags_); }
    VkMemoryPropertyFlags flags() const { return flags_; }

    bool mapped() const { return mapped_ptr_ != nullptr; }
    VkDevice vk_dev() const { return vk_dev_; }
    VmaAllocator vma_allocator() const { return vma_allocator_; }

    void map();
    void unmap();
    void flush_alloc(VkDeviceSize a_offset, VkDeviceSize a_size);
    void invalidate_alloc(VkDeviceSize a_offset, VkDeviceSize a_size);

    template<typename T = void> T* mapped_data() const { return reinterpret_cast<T*>(mapped_ptr_); }
};

} // namespace spock::impl


class buffer_t : public impl::alloc_t
{
private:
    VkBuffer vk_buffer_;
    size_t bytes_;

public:
    buffer_t() : alloc_t{}, vk_buffer_{ VK_NULL_HANDLE }, bytes_{ 0 }  {}

    buffer_t(const buffer_t& a_buffer) = delete;
    buffer_t& operator=(const buffer_t& a_buffer) = delete;

    buffer_t(buffer_t&& a_buffer) noexcept;
    buffer_t& operator=(buffer_t&& a_buffer) noexcept;

    buffer_t(const context_t& a_context, const VkBufferCreateInfo&, const VmaAllocationCreateInfo&);
    buffer_t(VkDevice a_vk_dev, VmaAllocator a_vma_allocator, const VkBufferCreateInfo&, const VmaAllocationCreateInfo&);
    ~buffer_t();

    std::pair<VmaAllocation, VkBuffer> release();
    [[nodiscard]] buffer_t cmd_upload(VkCommandBuffer a_command_buffer, const void* a_src);

    operator VkBuffer () const { return vk_buffer_; }
    bool valid() const { return vk_buffer_ != VK_NULL_HANDLE; }
    size_t bytes() const { return bytes_; }

};

inline buffer_t stage(const context_t& a_context, const void* a_src, size_t a_bytes) { return stage(a_context, a_context, a_src, a_bytes); }


class buffer_view_t
{
private:
    VkDevice vk_dev_;
    VkBufferView vk_buffer_view_;

public:
    buffer_view_t(const buffer_view_t& ) = delete;
    buffer_view_t& operator=(const buffer_view_t& ) = delete;

    buffer_view_t(buffer_view_t&& a_buffer_view) noexcept;
    buffer_view_t& operator= (buffer_view_t&& a_buffer_view) noexcept;
   
    buffer_view_t() : vk_dev_{ VK_NULL_HANDLE }, vk_buffer_view_{ VK_NULL_HANDLE } {}
    buffer_view_t(VkDevice a_vk_dev, const VkBufferViewCreateInfo& a_create_info);
    buffer_view_t(VkDevice a_vk_dev, buffer_t const* a_buffer, VkFormat a_vk_format);
    ~buffer_view_t();

    VkBufferView release();
    operator VkBufferView () const { return vk_buffer_view_; }
    const VkBufferView* handle() const { return &vk_buffer_view_; }
    bool valid() const { return vk_buffer_view_ != VK_NULL_HANDLE; }
};


class image_t : public impl::alloc_t
{
private:
    VkImage vk_image_;
    
    VkFormat format_;
    VkExtent3D extent_;
    uint32_t num_mip_levels_;

public:
    image_t() : alloc_t{}, vk_image_{ VK_NULL_HANDLE }, format_{VK_FORMAT_UNDEFINED}, extent_{}, num_mip_levels_{} {}

    image_t(const image_t& a_image) = delete;
    image_t& operator=(const image_t& a_image) = delete;

    image_t(image_t&& a_image) noexcept;
    image_t& operator=(image_t&& a_image) noexcept;

    image_t(const context_t& a_context, const VkImageCreateInfo& a_image_create_info, const VmaAllocationCreateInfo& a_alloc_create_info = build_VmaAllocationCreateInfo());
    image_t(VkDevice a_vk_dev, VmaAllocator a_vma_allocator, const VkImageCreateInfo& a_image_create_info, const VmaAllocationCreateInfo& a_alloc_create_info = build_VmaAllocationCreateInfo());
    ~image_t();

    std::pair<VmaAllocation, VkImage> release();

    /// For mip level 0: before uploading, layouts of all mip levels are transformed to VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    /// but only layout of mip level 0 will be transformed to `a_final_image_layout` after the upload.
    /// upload one mip level
    [[nodiscard SPOCK_NODISCARD_REASON("returned staging buffer must not be destroyed until upload is done")]] buffer_t
    cmd_upload(
        VkCommandBuffer a_command_buffer,
        const void* a_src, size_t a_bytes,
        const VkImageSubresourceLayers& a_image_subresource_layers,
        VkImageLayout a_final_image_layout,
        VkPipelineStageFlags a_next_stages,
        VkAccessFlags a_next_accesses
    ) const;

    /// aspect: VK_IMAGE_ASPECT_COLOR_BIT
    /// assume mipmap 0 is in VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, the rest in VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    /// return extent of all mip levels 
    std::vector<VkExtent3D>
    cmd_gen_mipmaps_from_mip0(
        VkCommandBuffer a_command_buffer, 
        VkImageLayout a_final_image_layout,
        VkPipelineStageFlags a_next_stages,
        VkAccessFlags a_next_accesses
    ) const;

    /// blit mip0 to another image, format conversion is performed by vulkan when necessary 
    /// src image:
    ///     usage:  VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT 
    ///     layout: VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    ///     aspect: VK_IMAGE_ASPECT_COLOR_BIT
    /// dst image:
    ///     usage:  VK_IMAGE_USAGE_TRANSFER_DST_BIT 
    ///     init layout: VK_IMAGE_LAYOUT_UNDEFINED
    ///     final layout: VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL for mip0, the rest: VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    void cmd_blit_mip0_to_image(
        VkCommandBuffer a_command_buffer, 
        image_t& a_dst_image,
        VkImageLayout a_dst_final_image_layout,
        VkPipelineStageFlags a_dst_next_stages,
        VkAccessFlags a_dst_next_accesses
    ) const;

    /// uncompressed format only
    [[nodiscard SPOCK_NODISCARD_REASON("returned buffer must live beyond the point of queue submission in order to be useful")]] buffer_t
    cmd_copy_to_host_visible_buffer( VkCommandBuffer a_command_buffer, VkDeviceSize a_texel_block_bytes, bool a_mip_0_only = false) const;

    bool valid() const { return vk_image_ != VK_NULL_HANDLE && num_mip_levels_ > 0u; }
    VkFormat format() const { return format_; }
    VkExtent3D extent() const { return extent_; }
    uint32_t num_mip_levels() const { return num_mip_levels_; }
    operator VkImage () const { return vk_image_; }

};


class image_view_t
{
private:
    VkDevice vk_dev_;
    VkImageView vk_image_view_;
    VkImageSubresourceRange vk_image_subresource_range_;

public:
    image_view_t() : vk_dev_ { VK_NULL_HANDLE }, vk_image_view_{ VK_NULL_HANDLE }, vk_image_subresource_range_{} {}
    image_view_t(const image_view_t& ) = delete;
    image_view_t& operator=(const image_view_t& ) = delete;

    image_view_t(image_view_t&& a_image_view) noexcept;
    image_view_t& operator= (image_view_t&& a_image_view) noexcept;
   
    image_view_t(VkDevice a_vk_dev, const VkImageViewCreateInfo& a_create_info);
    ~image_view_t();

    VkImageView release();

    operator VkImageView () const { return vk_image_view_; }
    bool valid() const { return vk_image_view_ != VK_NULL_HANDLE; }
    const VkImageSubresourceRange& range() const { return vk_image_subresource_range_; }

};


class sampler_t
{
private:
    VkDevice vk_dev_; // does not own the resource
    VkSampler vk_sampler_;

public:
    sampler_t() : vk_dev_ { VK_NULL_HANDLE }, vk_sampler_{ VK_NULL_HANDLE }{}
    sampler_t(const sampler_t& ) = delete;
    sampler_t& operator=(const sampler_t& ) = delete;

    sampler_t(sampler_t&& a_sampler) noexcept;
    sampler_t& operator= (sampler_t&& a_sampler) noexcept;
    
    sampler_t(VkDevice a_vk_dev); // dummy sampler
    sampler_t(VkDevice a_vk_dev, const VkSamplerCreateInfo& a_vk_sampler_create_info);
    sampler_t(const context_t& a_context, VkFilter a_filter, VkSamplerAddressMode a_addr_mode);
    ~sampler_t();

    operator VkSampler () const { return vk_sampler_; }
    bool valid() const { return vk_sampler_ != VK_NULL_HANDLE; }
};


class swapchain_t {
private:
    const context_t* context_;

    VkSwapchainKHR vk_swapchain_;

    VkSurfaceFormatKHR surface_format_;
    VkPresentModeKHR present_mode_;
    uint32_t image_count_;
    VkExtent2D image_extent_;

    std::unique_ptr<VkImage[]> vk_images_;
    std::unique_ptr<VkImageView[]> vk_image_views_;

    bool choose_surface_format();
    bool choose_present_mode();
    bool choose_image_extent(const VkSurfaceCapabilitiesKHR& a_surface_capabilities, const VkExtent2D& a_extent);

public:
    swapchain_t(const swapchain_t&) = delete;
    swapchain_t& operator=(const swapchain_t&) = delete;

    swapchain_t(swapchain_t&& a_swapchain) noexcept;
    swapchain_t& operator=(swapchain_t&& a_swapchain) noexcept;

    swapchain_t() : context_{ nullptr } , vk_swapchain_{ VK_NULL_HANDLE } , surface_format_{} , present_mode_{} , image_count_{} , image_extent_{} {}
    swapchain_t(const context_t& a_context, const VkExtent2D& a_extent);
    ~swapchain_t();

    // make sure a_width != 0 && a_height != 0
    bool resize(const VkExtent2D& a_extent);

    operator VkSwapchainKHR () const { return vk_swapchain_; }
    const VkSwapchainKHR* ptr() const { return &vk_swapchain_; }
    bool valid() const { return vk_swapchain_ != VK_NULL_HANDLE; }

    uint32_t image_count() const { return image_count_; }
    VkFormat image_format() const { return surface_format_.format; }
    const VkExtent2D& image_extent() const { return image_extent_; }
    float image_aspect_ratio() const { return static_cast<float>(image_extent_.width) / static_cast<float>(image_extent_.height); }
    VkPresentModeKHR present_mode() const { return present_mode_; }
    VkSurfaceFormatKHR surface_format() const { return surface_format_; }
    VkImageView image_view(uint32_t a_idx) const;
    const VkImageView* vk_image_views() const { return vk_image_views_.get(); }

};


class query_pool_t {
private:
    VkDevice vk_dev_;
    VkQueryPool vk_query_pool_;

public:
    query_pool_t(const query_pool_t&) = delete;
    query_pool_t& operator=(const query_pool_t&) = delete;

    query_pool_t(query_pool_t&&) noexcept;
    query_pool_t& operator=(query_pool_t&&) noexcept;

    query_pool_t() : vk_dev_{ VK_NULL_HANDLE }, vk_query_pool_{ VK_NULL_HANDLE } {}
    query_pool_t(VkDevice a_vk_dev, const VkQueryPoolCreateInfo& a_create_info);
    query_pool_t(VkDevice a_vk_dev, VkQueryType a_vk_query_type, uint32_t a_query_count, VkQueryPipelineStatisticFlags a_pipeline_stats = {});
    ~query_pool_t();

    operator VkQueryPool() const { return vk_query_pool_; }
    VkDevice vk_dev() const { return vk_dev_; }
    bool valid() const { return vk_query_pool_ != VK_NULL_HANDLE; }
};


// only for vkCmd* submitted to the same queue; not thread safe
class dev_timer_array_t {
public:
    using result_type = uint64_t;

    dev_timer_array_t(const dev_timer_array_t&) = delete;
    dev_timer_array_t& operator=(const dev_timer_array_t&) = delete;
    dev_timer_array_t(dev_timer_array_t&&) = default;
    dev_timer_array_t& operator=(dev_timer_array_t&&) = default;

    dev_timer_array_t();
    // if a_external_sync == true, user must explicitly synchronize before call dev_timer_array_t::end()
    dev_timer_array_t(const context_t& a_context, bool a_external_sync, uint32_t a_num_timers_per_frame, uint32_t a_moving_average_filter_width, uint32_t a_num_in_flight_frames, uint32_t a_frame_queue_len_max = 0);

    void begin();
    const VkFence& get_internal_fence() const; 
    void end();
    float elapsed_ms(uint32_t a_timer_idx) const;

    void cmd_start(VkCommandBuffer a_vk_command_buffer, uint32_t a_timer_idx, VkPipelineStageFlagBits a_pipeline_stage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
    void cmd_stop(VkCommandBuffer a_vk_command_buffer, uint32_t a_timer_idx, VkPipelineStageFlagBits a_pipeline_stage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);

private:
    float timestamp_period_in_ms_;
    uint32_t written_timestamp_count_;

    uint32_t num_timers_per_frame_; // num_of_timestamps / 2
    uint32_t moving_average_filter_width_; // number of frames used to compute average
    uint32_t num_in_flight_frames_; // max number of in-flight frames
    uint32_t frame_queue_len_max_; // in terms of number of frames 

    uint32_t frame_queue_tail_idx_; // in terms of number of frames
    uint32_t frame_queue_len_; // in terms of number of frames
    uint32_t current_in_flight_idx_; 
    std::vector<bool> query_reset_; // query_reset_[i]: if query_pool is reset for i_th in-flight frame;

    query_pool_t query_pool_;
    std::vector<float> filtered_elapsed_ms_;
    std::unique_ptr<result_type[]> frame_queue_;
    std::vector<fence_t> fences_;

};


// TODO: allow buffer to be suballocated from a pool
class acceleration_structure_t {
private:
    VkDevice vk_dev_;
    buffer_t buffer_;
    VkAccelerationStructureKHR vk_acceleration_structure_;

public:
    acceleration_structure_t(const acceleration_structure_t&) = delete;
    acceleration_structure_t& operator=(const acceleration_structure_t&) = delete;

    acceleration_structure_t(acceleration_structure_t&&) noexcept;
    acceleration_structure_t& operator=(acceleration_structure_t&&) noexcept;

    acceleration_structure_t() : vk_dev_{ VK_NULL_HANDLE }, vk_acceleration_structure_{ VK_NULL_HANDLE } {}

    // a_build_geometry_info.scratchData.deviceAddress will be filled with build scratch buffer size if vkCreateAccelerationStructureKHR succeeds
    acceleration_structure_t(
        const context_t& a_context, 
        VkAccelerationStructureTypeKHR a_acceleration_structure_type,
        VkDeviceSize a_acceleration_structure_size
    );

    ~acceleration_structure_t();

    VkDeviceAddress get_device_addr() const; // the AccelerationStructureDeviceAddress, not device addr of buffer_
    operator VkAccelerationStructureKHR() const { return vk_acceleration_structure_; }
    const VkAccelerationStructureKHR& operator()() const { return vk_acceleration_structure_; }
    VkDevice vk_dev() const { return vk_dev_; }
    bool valid() const { return vk_acceleration_structure_ != VK_NULL_HANDLE; }

};

// cache blas rebuild infos if known beforehand, typically before the main loop
struct acceleration_structures_build_info_t {
    std::vector<VkAccelerationStructureBuildGeometryInfoKHR> geometries;
    std::vector<const VkAccelerationStructureBuildRangeInfoKHR*> ranges; // pointers to elements of blas_builder_array_t::range_info_pool_
};


class blas_builder_array_t {
private:
    std::vector<buffer_t> scratch_buffers_; // pointed to by geometry_info_ (device addr)
    std::vector<VkAccelerationStructureGeometryKHR> geometry_pool_; // pointed to by build_info_.geometries
    std::vector<VkAccelerationStructureBuildRangeInfoKHR> range_info_pool_; // pointed to by build_info_.ranges
    std::vector<VkAccelerationStructureBuildSizesInfoKHR> sizes_infos_;
    acceleration_structures_build_info_t build_info_;

    VkAccelerationStructureBuildGeometryInfoKHR create_geometry_info(uint32_t a_num_geometries, VkBuildAccelerationStructureFlagsKHR a_flags, bool a_allow_rebuild);
    acceleration_structures_build_info_t get_partial_build_info(const std::vector<uint32_t>& a_info_indices, VkBuildAccelerationStructureModeKHR a_build_mode, uint64_t a_src_as_mask);

public:
    blas_builder_array_t(const blas_builder_array_t&) = delete;
    blas_builder_array_t& operator= (const blas_builder_array_t&) = delete;
    blas_builder_array_t(blas_builder_array_t&&) noexcept; // no explicit guarantee that move constructor of a vector preserves the validity of iterators/pointers to its elements
    blas_builder_array_t& operator= (blas_builder_array_t&&) noexcept; // no explicit guarantee that move assignment of a vector preserves the validity of iterators/pointers to its elements
    blas_builder_array_t() {}

    // a blas consists of only one geometry
    void add_build_info(
        const VkAccelerationStructureGeometryKHR& a_geometry, 
        const VkAccelerationStructureBuildRangeInfoKHR& a_range_info,
        VkBuildAccelerationStructureFlagsKHR a_flags,
        bool a_allow_rebuild
    );
    // a blas consists of multiple geometries
    void add_build_infos(
        const std::vector<VkAccelerationStructureGeometryKHR>& a_geometries, 
        const std::vector<VkAccelerationStructureBuildRangeInfoKHR>& a_range_infos,
        VkBuildAccelerationStructureFlagsKHR a_flags,
        bool a_allow_rebuild
    );

    // update the pointers in build_info_.geometries and build_info_.ranges after the last add_build_info(...) call and calc BuildSizes
    // return blas count
    uint32_t resolve_infos(VkDevice a_vk_dev); 

    // need sizes_infos from resolve_infos;
    // create blases, and store them in pre-allocated a_blases
    // update every geometry_infos_[i].dstAccelerationStructure and a_instances[i].accelerationStructureReference
    void create_blases(const context_t& a_context, acceleration_structure_t* a_blases, VkAccelerationStructureInstanceKHR* a_instance_begin);

    // return the scratch buffers that can be released after cmd_initial_build finishes
    [[nodiscard SPOCK_NODISCARD_REASON("returned one-time scratch buffers must not be destroyed until cmd_initial_build(...) is done")]] 
    std::vector<buffer_t> cmd_initial_build(const context_t& a_context, VkCommandBuffer a_command_buffer);

    void adapt_build_info_for_rebuild_all();
    void adapt_build_info_for_refit_all();

    const acceleration_structures_build_info_t& build_info() const { return build_info_; }

    // cmd_rebuild(...) cannot be used for rebuild if the geometry change does not meet the requirement for update/refit (except for ALLOW_UPDATE_BIT flag); 
    // in that case, recreate blas and blas_builder_t
    acceleration_structures_build_info_t get_partial_rebuild_info(const std::vector<uint32_t>& a_info_indices)
    {
        return get_partial_build_info(a_info_indices, VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR, 0);
    }
    
    acceleration_structures_build_info_t get_partial_refit_info(const std::vector<uint32_t>& a_info_indices)
    {
        return get_partial_build_info(a_info_indices, VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR, ~0ull); // ull: at least 64-bit
    }

};


void translate_instance(VkAccelerationStructureInstanceKHR* a_instance, const glm::vec3& a_translate);
void rotate_instance(VkAccelerationStructureInstanceKHR* a_instance, float a_radians, const glm::vec3& a_axis);
void scale_instance(VkAccelerationStructureInstanceKHR* a_instance, const glm::vec3& a_scale_v);

inline void
transform_instance(VkAccelerationStructureInstanceKHR* a_instance, const glm::mat4& a_column_major_transform)
{
    // access in shader: gl_ObjectToWorldEXT
    *reinterpret_cast<glm::mat3x4*>(a_instance->transform.matrix) = glm::transpose(glm::mat4x3(a_column_major_transform)); 
}


class tlas_builder_t {
private:
    buffer_t instance_buffer_; // pointed to by unique_geometry_ (device addr)
    buffer_t instance_staging_buffer_;
    buffer_t scratch_buffer_; // pointed to by geometry_info_ (device addr)
    std::unique_ptr<VkAccelerationStructureGeometryKHR> unique_geometry_; // pointed to by geometry_info_, unique_ptr allows for defaulted move ctor
    VkAccelerationStructureBuildRangeInfoKHR range_info_;
    VkAccelerationStructureBuildGeometryInfoKHR geometry_info_;

public:
    tlas_builder_t(const tlas_builder_t&) = delete;
    tlas_builder_t& operator= (const tlas_builder_t&) = delete;
    tlas_builder_t(tlas_builder_t&&) = default;
    tlas_builder_t& operator= (tlas_builder_t&&) = default;
    tlas_builder_t() : range_info_{}, geometry_info_{}{}

    acceleration_structure_t cmd_initial_build(
        const context_t& a_context,
        VkCommandBuffer a_command_buffer, 
        uint32_t a_instance_count,
        const VkAccelerationStructureInstanceKHR* a_instance_begin,
        VkBuildAccelerationStructureFlagsKHR a_flags
    );

    // vkspec-1.2.187: ch36 Acceleration Structures: vkCmdBuildAccelerationStructuresKHR (p. 1951)
    // sync before cmd_rebuild or cmd_refit (cmd_update_barrier):
    //      srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT
    //      dstAccessMask = VK_ACCESS_SHADER_READ_BIT
    //      srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT
    //      dstStage = VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR
    void cmd_update_instance_buffer(
        VkCommandBuffer a_command_buffer, 
        uint32_t a_instance_count,
        const VkAccelerationStructureInstanceKHR* a_instance_begin,
        uint32_t a_dst_instance_offset
    );


    void cmd_update_barrier(VkCommandBuffer a_command_buffer)
    {
        const auto barrier = build_VkMemoryBarrier(VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
        vkCmdPipelineBarrier(a_command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR, 0, 1, &barrier, 0, nullptr, 0, nullptr);
    }


    // sync after last cmd_instance_buffer before cmd_rebuild or cmd_refit (cmd_update_barrier):
    void cmd_rebuild(VkCommandBuffer a_command_buffer)
    {
        geometry_info_.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        geometry_info_.srcAccelerationStructure = VK_NULL_HANDLE;
        const auto* range_info_ptr = &range_info_;
        vkCmdBuildAccelerationStructuresKHR(a_command_buffer, 1, &geometry_info_, &range_info_ptr);
    }

    // sync after last cmd_instance_buffer before cmd_rebuild or cmd_refit (cmd_update_barrier):
    void cmd_refit(VkCommandBuffer a_command_buffer)
    {
        geometry_info_.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
        geometry_info_.srcAccelerationStructure = geometry_info_.dstAccelerationStructure;
        const auto* range_info_ptr = &range_info_;
        vkCmdBuildAccelerationStructuresKHR(a_command_buffer, 1, &geometry_info_, &range_info_ptr);
    }

};


// a set of shader binding tables used in one ray tracing dispatch, e.g. vkCmdTraceRaysKHR
class shader_binding_set_t {
private:
    VkStridedDeviceAddressRegionKHR raygen_;
    VkStridedDeviceAddressRegionKHR miss_;
    VkStridedDeviceAddressRegionKHR hit_;
    VkStridedDeviceAddressRegionKHR callable_;

public:
    shader_binding_set_t(shader_binding_set_t&&) = default;
    shader_binding_set_t& operator=(shader_binding_set_t&&) = default;
    shader_binding_set_t(const shader_binding_set_t&) = default;
    shader_binding_set_t& operator=(const shader_binding_set_t&) = default;

    shader_binding_set_t() : raygen_{}, miss_{}, hit_{}, callable_{}{}

    // if unused, set to zero (i.e. VkStridedDeviceAddressRegionKHR{})
    shader_binding_set_t
    (
        const VkStridedDeviceAddressRegionKHR& a_raygen,
        const VkStridedDeviceAddressRegionKHR& a_miss = VkStridedDeviceAddressRegionKHR{},
        const VkStridedDeviceAddressRegionKHR& a_hit = VkStridedDeviceAddressRegionKHR{},
        const VkStridedDeviceAddressRegionKHR& a_callable = VkStridedDeviceAddressRegionKHR{}
    ) 
        : raygen_{ a_raygen }, miss_{ a_miss }, hit_{ a_hit }, callable_{ a_callable }
    {}

    VkStridedDeviceAddressRegionKHR const* raygen() const { return &raygen_; }
    VkStridedDeviceAddressRegionKHR const* miss() const { return &miss_; }
    VkStridedDeviceAddressRegionKHR const* hit() const { return &hit_; }
    VkStridedDeviceAddressRegionKHR const* callable() const { return &callable_; }
};


inline void cmd_trace_rays(VkCommandBuffer a_cmd_buf, const shader_binding_set_t& a_sb_set, const glm::uvec3& a_dim)
{
    vkCmdTraceRaysKHR(a_cmd_buf, a_sb_set.raygen(), a_sb_set.miss(), a_sb_set.hit(), a_sb_set.callable(), a_dim.x, a_dim.y, a_dim.z);
}


} // namespace spock


