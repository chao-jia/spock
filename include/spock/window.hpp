#pragma once

#if defined(__INTELLISENSE__)
#undef VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#else
#include <volk.h>
#endif

#if defined(VK_USE_PLATFORM_ANDROID_KHR)
#include <android/native_activity.h>
#include <android/asset_manager.h>
#include <android_native_app_glue.h>
#include <sys/system_properties.h>
#else
#include <glfw/glfw3.h>
#endif

#include <nlohmann/json_fwd.hpp>
#include <imgui.h>

namespace spock {

using namespace nlohmann;

class window_t {
private:
    GLFWwindow* window_;
    int width_;
    int height_;
    int windowed_width_;
    int windowed_height_;
    int window_pos_x_;
    int window_pos_y_;
    bool full_screen_;
    float monitor_scale_x_;
    float monitor_scale_y_;

public:
    enum class state_e {
        UNCHANGED = 0,
        RESIZED,
        MINIMIZED
    };

    window_t(const window_t&) = delete;
    window_t& operator=(const window_t&) = delete;

    window_t(window_t&& a_window) = delete;
    window_t& operator=(window_t&& a_window) = delete;

    window_t(const json& a_window_config_json);
    ~window_t();
    operator GLFWwindow* () const { return window_; }

    int width() const { return width_; }
    int height() const { return height_; }
    float aspect_ratio() const { return static_cast<float>(width_) / static_cast<float>(height_); }
    float get_scale_factor() const { return monitor_scale_x_ > monitor_scale_y_ ? monitor_scale_x_ : monitor_scale_y_; }
    state_e check_size();
    void toggle_full_screen();

    bool should_close() { return glfwWindowShouldClose(window_); }
    void poll_event() { glfwPollEvents(); }
    void set_title(const char* a_title) { glfwSetWindowTitle(window_, a_title); }

    VkSurfaceKHR create_surface(VkInstance a_vk_instance) const;
    static std::vector<const char*> instance_extensions();

};


class context_t;
class command_pool_t;
class queue_t;

// depends on glfw
class imgui_t {
private:
    ImGuiIO* imgui_io_;
    ImDrawData* im_draw_data_;
    VkDevice vk_dev_;
    VkDescriptorPool vk_descriptor_pool_;
    float current_scale_factor_;
    std::vector<std::pair<std::string, float>> font_path_size_pairs_;
    std::vector<ImFont*> fonts_; // fonts_.back() is the default font

    float ini_scale_factor_;
    std::string ini_path_str_;
    std::string ini_data_; 

    void set_style();
    void deinit();
    void read_imgui_ini_file();
    void write_imgui_ini_file();
    void init_imgui_fonts();

public:
    imgui_t();
    imgui_t(const std::vector< std::pair<std::string, float> >& a_font_path_size_pairs, const std::string& a_imgui_ini_path_str = "");
    void init(
        const window_t& a_window,
        const context_t& a_context,
        VkCommandBuffer a_vk_cmd_buf,
        const queue_t& a_queue,
        VkRenderPass a_vk_render_pass,
        uint32_t a_subpass,
        uint32_t a_image_count,
        VkSampleCountFlagBits a_sample_count_flag_bits
    );
    imgui_t(const imgui_t&) = delete;
    imgui_t& operator= (const imgui_t&) = delete;
    imgui_t(imgui_t&&) noexcept;
    imgui_t& operator= (imgui_t&&) noexcept;

    ~imgui_t();

    void push_font(size_t i);
    void pop_font() { ImGui::PopFont(); }

    bool want_keys() const { return imgui_io_->WantCaptureKeyboard; }
    bool want_mouse() const { return imgui_io_->WantCaptureMouse; }
    void scroll(float xoffset, float yoffset) const { imgui_io_->MouseWheel += yoffset; imgui_io_->MouseWheelH += xoffset; }

    void new_frame();
    void begin_render(const char* a_name, bool* a_open = NULL, ImGuiWindowFlags a_flags = 0);
    void end_render();

    /// record command buffer only if !is_minimized
    void record_command_buffer(VkCommandBuffer a_vk_command_buffer);

    static VkDescriptorPoolSize imgui_descriptor_pool_size();

};






} // namespace spock

