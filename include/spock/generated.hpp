// generated from vulkan header version: 1.2.198 (variant: 0)

#pragma once
#if defined(_MSC_VER)
#pragma warning(disable : 26812)
#endif
#if defined(__INTELLISENSE__)
#undef VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#else
#include <volk.h>
#endif

#include <map>
#include <unordered_set>
#include <string>
#include <string_view>

namespace spock {
std::string_view get_extension_name(std::string_view a_feature_name, bool* a_ok_ptr = nullptr);

struct physical_dev_feature2_wrapper_t {
    void* feature_ptr;
    void** feature_pNext_ptr;
    std::string_view feature_name;
    std::string_view extension_name;
    bool available;
    bool required;
    bool versioned;

    physical_dev_feature2_wrapper_t() : feature_ptr(nullptr), feature_pNext_ptr(nullptr), available(false), required(false), versioned(false) {}
    physical_dev_feature2_wrapper_t(void* a_feature_ptr, void** a_feature_pNext_ptr, std::string_view a_feature_name, std::string_view a_extension_name) 
        : feature_ptr(a_feature_ptr)
        , feature_pNext_ptr(a_feature_pNext_ptr)
        , feature_name(a_feature_name)
        , extension_name(a_extension_name)
        , available(false)
        , required(false)
        , versioned(false) 
    {
        if (extension_name.length() > 10 && extension_name.substr(0, 11) == std::string_view{ "VK_VERSION_" }) {
            versioned = true;
        }
    }
};

struct physical_dev_all_features2_t {
    physical_dev_all_features2_t(bool a_requested_only = false) : requested_only{ a_requested_only } {}
    std::vector<physical_dev_feature2_wrapper_t> link(const std::map<std::string_view, bool>& a_feature_requests, const std::unordered_set<std::string>& a_available_device_extensions, bool& a_ok, std::string& a_error_msg);
    physical_dev_feature2_wrapper_t wrap_feature(std::string_view a_feature_name, bool* a_ok_ptr = nullptr);

    bool requested_only;
#if VK_VERSION_1_1
    VkPhysicalDevice16BitStorageFeatures PhysicalDevice16BitStorageFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES };
    VkPhysicalDeviceFeatures2 PhysicalDeviceFeatures2{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 };
    VkPhysicalDeviceMultiviewFeatures PhysicalDeviceMultiviewFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES };
    VkPhysicalDeviceVariablePointersFeatures PhysicalDeviceVariablePointersFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES };
    VkPhysicalDeviceProtectedMemoryFeatures PhysicalDeviceProtectedMemoryFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES };
    VkPhysicalDeviceSamplerYcbcrConversionFeatures PhysicalDeviceSamplerYcbcrConversionFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES };
    VkPhysicalDeviceShaderDrawParametersFeatures PhysicalDeviceShaderDrawParametersFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES };
#endif
#if VK_VERSION_1_2
    VkPhysicalDeviceVulkan11Features PhysicalDeviceVulkan11Features{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES };
    VkPhysicalDeviceVulkan12Features PhysicalDeviceVulkan12Features{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES };
    VkPhysicalDevice8BitStorageFeatures PhysicalDevice8BitStorageFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES };
    VkPhysicalDeviceShaderAtomicInt64Features PhysicalDeviceShaderAtomicInt64Features{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES };
    VkPhysicalDeviceShaderFloat16Int8Features PhysicalDeviceShaderFloat16Int8Features{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES };
    VkPhysicalDeviceDescriptorIndexingFeatures PhysicalDeviceDescriptorIndexingFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES };
    VkPhysicalDeviceScalarBlockLayoutFeatures PhysicalDeviceScalarBlockLayoutFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES };
    VkPhysicalDeviceVulkanMemoryModelFeatures PhysicalDeviceVulkanMemoryModelFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES };
    VkPhysicalDeviceImagelessFramebufferFeatures PhysicalDeviceImagelessFramebufferFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES };
    VkPhysicalDeviceUniformBufferStandardLayoutFeatures PhysicalDeviceUniformBufferStandardLayoutFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES };
    VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures PhysicalDeviceShaderSubgroupExtendedTypesFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES };
    VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures PhysicalDeviceSeparateDepthStencilLayoutsFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES };
    VkPhysicalDeviceHostQueryResetFeatures PhysicalDeviceHostQueryResetFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES };
    VkPhysicalDeviceTimelineSemaphoreFeatures PhysicalDeviceTimelineSemaphoreFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES };
    VkPhysicalDeviceBufferDeviceAddressFeatures PhysicalDeviceBufferDeviceAddressFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES };
#endif
#if VK_KHR_dynamic_rendering
    VkPhysicalDeviceDynamicRenderingFeaturesKHR PhysicalDeviceDynamicRenderingFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR };
#endif
#if VK_KHR_performance_query
    VkPhysicalDevicePerformanceQueryFeaturesKHR PhysicalDevicePerformanceQueryFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR };
#endif
#if VK_KHR_shader_clock
    VkPhysicalDeviceShaderClockFeaturesKHR PhysicalDeviceShaderClockFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR };
#endif
#if VK_KHR_shader_terminate_invocation
    VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR PhysicalDeviceShaderTerminateInvocationFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR };
#endif
#if VK_KHR_fragment_shading_rate
    VkPhysicalDeviceFragmentShadingRateFeaturesKHR PhysicalDeviceFragmentShadingRateFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR };
#endif
#if VK_KHR_present_wait
    VkPhysicalDevicePresentWaitFeaturesKHR PhysicalDevicePresentWaitFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR };
#endif
#if VK_KHR_pipeline_executable_properties
    VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR PhysicalDevicePipelineExecutablePropertiesFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR };
#endif
#if VK_KHR_shader_integer_dot_product
    VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR PhysicalDeviceShaderIntegerDotProductFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_FEATURES_KHR };
#endif
#if VK_KHR_present_id
    VkPhysicalDevicePresentIdFeaturesKHR PhysicalDevicePresentIdFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR };
#endif
#if VK_KHR_synchronization2
    VkPhysicalDeviceSynchronization2FeaturesKHR PhysicalDeviceSynchronization2FeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR };
#endif
#if VK_KHR_shader_subgroup_uniform_control_flow
    VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR PhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_UNIFORM_CONTROL_FLOW_FEATURES_KHR };
#endif
#if VK_KHR_zero_initialize_workgroup_memory
    VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR };
#endif
#if VK_KHR_workgroup_memory_explicit_layout
    VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR };
#endif
#if VK_KHR_maintenance4
    VkPhysicalDeviceMaintenance4FeaturesKHR PhysicalDeviceMaintenance4FeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES_KHR };
#endif
#if VK_EXT_transform_feedback
    VkPhysicalDeviceTransformFeedbackFeaturesEXT PhysicalDeviceTransformFeedbackFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT };
#endif
#if VK_NV_corner_sampled_image
    VkPhysicalDeviceCornerSampledImageFeaturesNV PhysicalDeviceCornerSampledImageFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV };
#endif
#if VK_EXT_texture_compression_astc_hdr
    VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT PhysicalDeviceTextureCompressionASTCHDRFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT };
#endif
#if VK_EXT_astc_decode_mode
    VkPhysicalDeviceASTCDecodeFeaturesEXT PhysicalDeviceASTCDecodeFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT };
#endif
#if VK_EXT_conditional_rendering
    VkPhysicalDeviceConditionalRenderingFeaturesEXT PhysicalDeviceConditionalRenderingFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT };
#endif
#if VK_EXT_depth_clip_enable
    VkPhysicalDeviceDepthClipEnableFeaturesEXT PhysicalDeviceDepthClipEnableFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT };
#endif
#if VK_EXT_inline_uniform_block
    VkPhysicalDeviceInlineUniformBlockFeaturesEXT PhysicalDeviceInlineUniformBlockFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT };
#endif
#if VK_EXT_blend_operation_advanced
    VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT PhysicalDeviceBlendOperationAdvancedFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT };
#endif
#if VK_NV_shader_sm_builtins
    VkPhysicalDeviceShaderSMBuiltinsFeaturesNV PhysicalDeviceShaderSMBuiltinsFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV };
#endif
#if VK_NV_shading_rate_image
    VkPhysicalDeviceShadingRateImageFeaturesNV PhysicalDeviceShadingRateImageFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV };
#endif
#if VK_NV_representative_fragment_test
    VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV PhysicalDeviceRepresentativeFragmentTestFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV };
#endif
#if VK_EXT_vertex_attribute_divisor
    VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT PhysicalDeviceVertexAttributeDivisorFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT };
#endif
#if VK_NV_compute_shader_derivatives
    VkPhysicalDeviceComputeShaderDerivativesFeaturesNV PhysicalDeviceComputeShaderDerivativesFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV };
#endif
#if VK_NV_mesh_shader
    VkPhysicalDeviceMeshShaderFeaturesNV PhysicalDeviceMeshShaderFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV };
#endif
#if VK_NV_fragment_shader_barycentric
    VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV PhysicalDeviceFragmentShaderBarycentricFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV };
#endif
#if VK_NV_shader_image_footprint
    VkPhysicalDeviceShaderImageFootprintFeaturesNV PhysicalDeviceShaderImageFootprintFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV };
#endif
#if VK_NV_scissor_exclusive
    VkPhysicalDeviceExclusiveScissorFeaturesNV PhysicalDeviceExclusiveScissorFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV };
#endif
#if VK_INTEL_shader_integer_functions2
    VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL };
#endif
#if VK_EXT_fragment_density_map
    VkPhysicalDeviceFragmentDensityMapFeaturesEXT PhysicalDeviceFragmentDensityMapFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT };
#endif
#if VK_EXT_subgroup_size_control
    VkPhysicalDeviceSubgroupSizeControlFeaturesEXT PhysicalDeviceSubgroupSizeControlFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT };
#endif
#if VK_AMD_device_coherent_memory
    VkPhysicalDeviceCoherentMemoryFeaturesAMD PhysicalDeviceCoherentMemoryFeaturesAMD{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD };
#endif
#if VK_EXT_shader_image_atomic_int64
    VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT PhysicalDeviceShaderImageAtomicInt64FeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_ATOMIC_INT64_FEATURES_EXT };
#endif
#if VK_EXT_memory_priority
    VkPhysicalDeviceMemoryPriorityFeaturesEXT PhysicalDeviceMemoryPriorityFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT };
#endif
#if VK_NV_dedicated_allocation_image_aliasing
    VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV };
#endif
#if VK_EXT_buffer_device_address
    VkPhysicalDeviceBufferDeviceAddressFeaturesEXT PhysicalDeviceBufferDeviceAddressFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT };
#endif
#if VK_NV_cooperative_matrix
    VkPhysicalDeviceCooperativeMatrixFeaturesNV PhysicalDeviceCooperativeMatrixFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV };
#endif
#if VK_NV_coverage_reduction_mode
    VkPhysicalDeviceCoverageReductionModeFeaturesNV PhysicalDeviceCoverageReductionModeFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV };
#endif
#if VK_EXT_fragment_shader_interlock
    VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT PhysicalDeviceFragmentShaderInterlockFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT };
#endif
#if VK_EXT_ycbcr_image_arrays
    VkPhysicalDeviceYcbcrImageArraysFeaturesEXT PhysicalDeviceYcbcrImageArraysFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT };
#endif
#if VK_EXT_provoking_vertex
    VkPhysicalDeviceProvokingVertexFeaturesEXT PhysicalDeviceProvokingVertexFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT };
#endif
#if VK_EXT_line_rasterization
    VkPhysicalDeviceLineRasterizationFeaturesEXT PhysicalDeviceLineRasterizationFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT };
#endif
#if VK_EXT_shader_atomic_float
    VkPhysicalDeviceShaderAtomicFloatFeaturesEXT PhysicalDeviceShaderAtomicFloatFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT };
#endif
#if VK_EXT_index_type_uint8
    VkPhysicalDeviceIndexTypeUint8FeaturesEXT PhysicalDeviceIndexTypeUint8FeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT };
#endif
#if VK_EXT_extended_dynamic_state
    VkPhysicalDeviceExtendedDynamicStateFeaturesEXT PhysicalDeviceExtendedDynamicStateFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT };
#endif
#if VK_EXT_shader_atomic_float2
    VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT PhysicalDeviceShaderAtomicFloat2FeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT };
#endif
#if VK_EXT_shader_demote_to_helper_invocation
    VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT };
#endif
#if VK_NV_device_generated_commands
    VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV PhysicalDeviceDeviceGeneratedCommandsFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV };
#endif
#if VK_NV_inherited_viewport_scissor
    VkPhysicalDeviceInheritedViewportScissorFeaturesNV PhysicalDeviceInheritedViewportScissorFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV };
#endif
#if VK_EXT_texel_buffer_alignment
    VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT PhysicalDeviceTexelBufferAlignmentFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT };
#endif
#if VK_EXT_device_memory_report
    VkPhysicalDeviceDeviceMemoryReportFeaturesEXT PhysicalDeviceDeviceMemoryReportFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT };
#endif
#if VK_EXT_robustness2
    VkPhysicalDeviceRobustness2FeaturesEXT PhysicalDeviceRobustness2FeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT };
#endif
#if VK_EXT_custom_border_color
    VkPhysicalDeviceCustomBorderColorFeaturesEXT PhysicalDeviceCustomBorderColorFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT };
#endif
#if VK_EXT_private_data
    VkPhysicalDevicePrivateDataFeaturesEXT PhysicalDevicePrivateDataFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT };
#endif
#if VK_EXT_pipeline_creation_cache_control
    VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT PhysicalDevicePipelineCreationCacheControlFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_CREATION_CACHE_CONTROL_FEATURES_EXT };
#endif
#if VK_NV_device_diagnostics_config
    VkPhysicalDeviceDiagnosticsConfigFeaturesNV PhysicalDeviceDiagnosticsConfigFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DIAGNOSTICS_CONFIG_FEATURES_NV };
#endif
#if VK_NV_fragment_shading_rate_enums
    VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV PhysicalDeviceFragmentShadingRateEnumsFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV };
#endif
#if VK_NV_ray_tracing_motion_blur
    VkPhysicalDeviceRayTracingMotionBlurFeaturesNV PhysicalDeviceRayTracingMotionBlurFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV };
#endif
#if VK_EXT_ycbcr_2plane_444_formats
    VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT PhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_2_PLANE_444_FORMATS_FEATURES_EXT };
#endif
#if VK_EXT_fragment_density_map2
    VkPhysicalDeviceFragmentDensityMap2FeaturesEXT PhysicalDeviceFragmentDensityMap2FeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT };
#endif
#if VK_EXT_image_robustness
    VkPhysicalDeviceImageRobustnessFeaturesEXT PhysicalDeviceImageRobustnessFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_ROBUSTNESS_FEATURES_EXT };
#endif
#if VK_EXT_4444_formats
    VkPhysicalDevice4444FormatsFeaturesEXT PhysicalDevice4444FormatsFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT };
#endif
#if VK_EXT_rgba10x6_formats
    VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT PhysicalDeviceRGBA10X6FormatsFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RGBA10X6_FORMATS_FEATURES_EXT };
#endif
#if VK_VALVE_mutable_descriptor_type
    VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE PhysicalDeviceMutableDescriptorTypeFeaturesVALVE{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE };
#endif
#if VK_EXT_vertex_input_dynamic_state
    VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT PhysicalDeviceVertexInputDynamicStateFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT };
#endif
#if VK_EXT_primitive_topology_list_restart
    VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT PhysicalDevicePrimitiveTopologyListRestartFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIMITIVE_TOPOLOGY_LIST_RESTART_FEATURES_EXT };
#endif
#if VK_HUAWEI_subpass_shading
    VkPhysicalDeviceSubpassShadingFeaturesHUAWEI PhysicalDeviceSubpassShadingFeaturesHUAWEI{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_FEATURES_HUAWEI };
#endif
#if VK_HUAWEI_invocation_mask
    VkPhysicalDeviceInvocationMaskFeaturesHUAWEI PhysicalDeviceInvocationMaskFeaturesHUAWEI{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI };
#endif
#if VK_NV_external_memory_rdma
    VkPhysicalDeviceExternalMemoryRDMAFeaturesNV PhysicalDeviceExternalMemoryRDMAFeaturesNV{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV };
#endif
#if VK_EXT_extended_dynamic_state2
    VkPhysicalDeviceExtendedDynamicState2FeaturesEXT PhysicalDeviceExtendedDynamicState2FeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_2_FEATURES_EXT };
#endif
#if VK_EXT_color_write_enable
    VkPhysicalDeviceColorWriteEnableFeaturesEXT PhysicalDeviceColorWriteEnableFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT };
#endif
#if VK_EXT_global_priority_query
    VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT PhysicalDeviceGlobalPriorityQueryFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT };
#endif
#if VK_EXT_multi_draw
    VkPhysicalDeviceMultiDrawFeaturesEXT PhysicalDeviceMultiDrawFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_FEATURES_EXT };
#endif
#if VK_EXT_border_color_swizzle
    VkPhysicalDeviceBorderColorSwizzleFeaturesEXT PhysicalDeviceBorderColorSwizzleFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BORDER_COLOR_SWIZZLE_FEATURES_EXT };
#endif
#if VK_EXT_pageable_device_local_memory
    VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PAGEABLE_DEVICE_LOCAL_MEMORY_FEATURES_EXT };
#endif
#if VK_KHR_acceleration_structure
    VkPhysicalDeviceAccelerationStructureFeaturesKHR PhysicalDeviceAccelerationStructureFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR };
#endif
#if VK_KHR_ray_tracing_pipeline
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR PhysicalDeviceRayTracingPipelineFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR };
#endif
#if VK_KHR_ray_query
    VkPhysicalDeviceRayQueryFeaturesKHR PhysicalDeviceRayQueryFeaturesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR };
#endif


}; // struct physical_dev_all_features2_t

#if VK_VERSION_1_0
VkResult str_to_VkResult(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkResult(VkResult e);
VkStructureType str_to_VkStructureType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkStructureType(VkStructureType e);
VkImageLayout str_to_VkImageLayout(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageLayout(VkImageLayout e);
VkObjectType str_to_VkObjectType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkObjectType(VkObjectType e);
VkPipelineCacheHeaderVersion str_to_VkPipelineCacheHeaderVersion(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineCacheHeaderVersion(VkPipelineCacheHeaderVersion e);
VkVendorId str_to_VkVendorId(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkVendorId(VkVendorId e);
VkSystemAllocationScope str_to_VkSystemAllocationScope(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSystemAllocationScope(VkSystemAllocationScope e);
VkInternalAllocationType str_to_VkInternalAllocationType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkInternalAllocationType(VkInternalAllocationType e);
VkFormat str_to_VkFormat(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFormat(VkFormat e);
VkImageTiling str_to_VkImageTiling(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageTiling(VkImageTiling e);
VkImageType str_to_VkImageType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageType(VkImageType e);
VkPhysicalDeviceType str_to_VkPhysicalDeviceType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPhysicalDeviceType(VkPhysicalDeviceType e);
VkQueryType str_to_VkQueryType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueryType(VkQueryType e);
VkSharingMode str_to_VkSharingMode(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSharingMode(VkSharingMode e);
VkComponentSwizzle str_to_VkComponentSwizzle(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkComponentSwizzle(VkComponentSwizzle e);
VkImageViewType str_to_VkImageViewType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageViewType(VkImageViewType e);
VkBlendFactor str_to_VkBlendFactor(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBlendFactor(VkBlendFactor e);
VkBlendOp str_to_VkBlendOp(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBlendOp(VkBlendOp e);
VkCompareOp str_to_VkCompareOp(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCompareOp(VkCompareOp e);
VkDynamicState str_to_VkDynamicState(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDynamicState(VkDynamicState e);
VkFrontFace str_to_VkFrontFace(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFrontFace(VkFrontFace e);
VkVertexInputRate str_to_VkVertexInputRate(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkVertexInputRate(VkVertexInputRate e);
VkPrimitiveTopology str_to_VkPrimitiveTopology(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPrimitiveTopology(VkPrimitiveTopology e);
VkPolygonMode str_to_VkPolygonMode(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPolygonMode(VkPolygonMode e);
VkStencilOp str_to_VkStencilOp(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkStencilOp(VkStencilOp e);
VkLogicOp str_to_VkLogicOp(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkLogicOp(VkLogicOp e);
VkBorderColor str_to_VkBorderColor(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBorderColor(VkBorderColor e);
VkFilter str_to_VkFilter(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFilter(VkFilter e);
VkSamplerAddressMode str_to_VkSamplerAddressMode(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSamplerAddressMode(VkSamplerAddressMode e);
VkSamplerMipmapMode str_to_VkSamplerMipmapMode(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSamplerMipmapMode(VkSamplerMipmapMode e);
VkDescriptorType str_to_VkDescriptorType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDescriptorType(VkDescriptorType e);
VkAttachmentLoadOp str_to_VkAttachmentLoadOp(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAttachmentLoadOp(VkAttachmentLoadOp e);
VkAttachmentStoreOp str_to_VkAttachmentStoreOp(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAttachmentStoreOp(VkAttachmentStoreOp e);
VkPipelineBindPoint str_to_VkPipelineBindPoint(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineBindPoint(VkPipelineBindPoint e);
VkCommandBufferLevel str_to_VkCommandBufferLevel(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCommandBufferLevel(VkCommandBufferLevel e);
VkIndexType str_to_VkIndexType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkIndexType(VkIndexType e);
VkSubpassContents str_to_VkSubpassContents(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSubpassContents(VkSubpassContents e);
VkAccessFlagBits str_to_VkAccessFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccessFlagBits(VkAccessFlagBits e);
std::string str_from_VkAccessFlags(VkAccessFlags f);
VkImageAspectFlagBits str_to_VkImageAspectFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageAspectFlagBits(VkImageAspectFlagBits e);
std::string str_from_VkImageAspectFlags(VkImageAspectFlags f);
VkFormatFeatureFlagBits str_to_VkFormatFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFormatFeatureFlagBits(VkFormatFeatureFlagBits e);
std::string str_from_VkFormatFeatureFlags(VkFormatFeatureFlags f);
VkImageCreateFlagBits str_to_VkImageCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageCreateFlagBits(VkImageCreateFlagBits e);
std::string str_from_VkImageCreateFlags(VkImageCreateFlags f);
VkSampleCountFlagBits str_to_VkSampleCountFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSampleCountFlagBits(VkSampleCountFlagBits e);
std::string str_from_VkSampleCountFlags(VkSampleCountFlags f);
VkImageUsageFlagBits str_to_VkImageUsageFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageUsageFlagBits(VkImageUsageFlagBits e);
std::string str_from_VkImageUsageFlags(VkImageUsageFlags f);
VkMemoryHeapFlagBits str_to_VkMemoryHeapFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkMemoryHeapFlagBits(VkMemoryHeapFlagBits e);
std::string str_from_VkMemoryHeapFlags(VkMemoryHeapFlags f);
VkMemoryPropertyFlagBits str_to_VkMemoryPropertyFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkMemoryPropertyFlagBits(VkMemoryPropertyFlagBits e);
std::string str_from_VkMemoryPropertyFlags(VkMemoryPropertyFlags f);
VkQueueFlagBits str_to_VkQueueFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueueFlagBits(VkQueueFlagBits e);
std::string str_from_VkQueueFlags(VkQueueFlags f);
VkDeviceQueueCreateFlagBits str_to_VkDeviceQueueCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDeviceQueueCreateFlagBits(VkDeviceQueueCreateFlagBits e);
std::string str_from_VkDeviceQueueCreateFlags(VkDeviceQueueCreateFlags f);
VkPipelineStageFlagBits str_to_VkPipelineStageFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineStageFlagBits(VkPipelineStageFlagBits e);
std::string str_from_VkPipelineStageFlags(VkPipelineStageFlags f);
VkSparseMemoryBindFlagBits str_to_VkSparseMemoryBindFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSparseMemoryBindFlagBits(VkSparseMemoryBindFlagBits e);
std::string str_from_VkSparseMemoryBindFlags(VkSparseMemoryBindFlags f);
VkSparseImageFormatFlagBits str_to_VkSparseImageFormatFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSparseImageFormatFlagBits(VkSparseImageFormatFlagBits e);
std::string str_from_VkSparseImageFormatFlags(VkSparseImageFormatFlags f);
VkFenceCreateFlagBits str_to_VkFenceCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFenceCreateFlagBits(VkFenceCreateFlagBits e);
std::string str_from_VkFenceCreateFlags(VkFenceCreateFlags f);
VkEventCreateFlagBits str_to_VkEventCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkEventCreateFlagBits(VkEventCreateFlagBits e);
std::string str_from_VkEventCreateFlags(VkEventCreateFlags f);
VkQueryPipelineStatisticFlagBits str_to_VkQueryPipelineStatisticFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueryPipelineStatisticFlagBits(VkQueryPipelineStatisticFlagBits e);
std::string str_from_VkQueryPipelineStatisticFlags(VkQueryPipelineStatisticFlags f);
VkQueryResultFlagBits str_to_VkQueryResultFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueryResultFlagBits(VkQueryResultFlagBits e);
std::string str_from_VkQueryResultFlags(VkQueryResultFlags f);
VkBufferCreateFlagBits str_to_VkBufferCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBufferCreateFlagBits(VkBufferCreateFlagBits e);
std::string str_from_VkBufferCreateFlags(VkBufferCreateFlags f);
VkBufferUsageFlagBits str_to_VkBufferUsageFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBufferUsageFlagBits(VkBufferUsageFlagBits e);
std::string str_from_VkBufferUsageFlags(VkBufferUsageFlags f);
VkImageViewCreateFlagBits str_to_VkImageViewCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkImageViewCreateFlagBits(VkImageViewCreateFlagBits e);
std::string str_from_VkImageViewCreateFlags(VkImageViewCreateFlags f);
VkPipelineCacheCreateFlagBits str_to_VkPipelineCacheCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineCacheCreateFlagBits(VkPipelineCacheCreateFlagBits e);
std::string str_from_VkPipelineCacheCreateFlags(VkPipelineCacheCreateFlags f);
VkColorComponentFlagBits str_to_VkColorComponentFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkColorComponentFlagBits(VkColorComponentFlagBits e);
std::string str_from_VkColorComponentFlags(VkColorComponentFlags f);
VkPipelineCreateFlagBits str_to_VkPipelineCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineCreateFlagBits(VkPipelineCreateFlagBits e);
std::string str_from_VkPipelineCreateFlags(VkPipelineCreateFlags f);
VkPipelineShaderStageCreateFlagBits str_to_VkPipelineShaderStageCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineShaderStageCreateFlagBits(VkPipelineShaderStageCreateFlagBits e);
std::string str_from_VkPipelineShaderStageCreateFlags(VkPipelineShaderStageCreateFlags f);
VkShaderStageFlagBits str_to_VkShaderStageFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkShaderStageFlagBits(VkShaderStageFlagBits e);
std::string str_from_VkShaderStageFlags(VkShaderStageFlags f);
VkCullModeFlagBits str_to_VkCullModeFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCullModeFlagBits(VkCullModeFlagBits e);
std::string str_from_VkCullModeFlags(VkCullModeFlags f);
VkSamplerCreateFlagBits str_to_VkSamplerCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSamplerCreateFlagBits(VkSamplerCreateFlagBits e);
std::string str_from_VkSamplerCreateFlags(VkSamplerCreateFlags f);
VkDescriptorPoolCreateFlagBits str_to_VkDescriptorPoolCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDescriptorPoolCreateFlagBits(VkDescriptorPoolCreateFlagBits e);
std::string str_from_VkDescriptorPoolCreateFlags(VkDescriptorPoolCreateFlags f);
VkDescriptorSetLayoutCreateFlagBits str_to_VkDescriptorSetLayoutCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDescriptorSetLayoutCreateFlagBits(VkDescriptorSetLayoutCreateFlagBits e);
std::string str_from_VkDescriptorSetLayoutCreateFlags(VkDescriptorSetLayoutCreateFlags f);
VkAttachmentDescriptionFlagBits str_to_VkAttachmentDescriptionFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAttachmentDescriptionFlagBits(VkAttachmentDescriptionFlagBits e);
std::string str_from_VkAttachmentDescriptionFlags(VkAttachmentDescriptionFlags f);
VkDependencyFlagBits str_to_VkDependencyFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDependencyFlagBits(VkDependencyFlagBits e);
std::string str_from_VkDependencyFlags(VkDependencyFlags f);
VkFramebufferCreateFlagBits str_to_VkFramebufferCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFramebufferCreateFlagBits(VkFramebufferCreateFlagBits e);
std::string str_from_VkFramebufferCreateFlags(VkFramebufferCreateFlags f);
VkRenderPassCreateFlagBits str_to_VkRenderPassCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkRenderPassCreateFlagBits(VkRenderPassCreateFlagBits e);
std::string str_from_VkRenderPassCreateFlags(VkRenderPassCreateFlags f);
VkSubpassDescriptionFlagBits str_to_VkSubpassDescriptionFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSubpassDescriptionFlagBits(VkSubpassDescriptionFlagBits e);
std::string str_from_VkSubpassDescriptionFlags(VkSubpassDescriptionFlags f);
VkCommandPoolCreateFlagBits str_to_VkCommandPoolCreateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCommandPoolCreateFlagBits(VkCommandPoolCreateFlagBits e);
std::string str_from_VkCommandPoolCreateFlags(VkCommandPoolCreateFlags f);
VkCommandPoolResetFlagBits str_to_VkCommandPoolResetFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCommandPoolResetFlagBits(VkCommandPoolResetFlagBits e);
std::string str_from_VkCommandPoolResetFlags(VkCommandPoolResetFlags f);
VkCommandBufferUsageFlagBits str_to_VkCommandBufferUsageFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCommandBufferUsageFlagBits(VkCommandBufferUsageFlagBits e);
std::string str_from_VkCommandBufferUsageFlags(VkCommandBufferUsageFlags f);
VkQueryControlFlagBits str_to_VkQueryControlFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueryControlFlagBits(VkQueryControlFlagBits e);
std::string str_from_VkQueryControlFlags(VkQueryControlFlags f);
VkCommandBufferResetFlagBits str_to_VkCommandBufferResetFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCommandBufferResetFlagBits(VkCommandBufferResetFlagBits e);
std::string str_from_VkCommandBufferResetFlags(VkCommandBufferResetFlags f);
VkStencilFaceFlagBits str_to_VkStencilFaceFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkStencilFaceFlagBits(VkStencilFaceFlagBits e);
std::string str_from_VkStencilFaceFlags(VkStencilFaceFlags f);
#endif
#if VK_VERSION_1_1
VkPointClippingBehavior str_to_VkPointClippingBehavior(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPointClippingBehavior(VkPointClippingBehavior e);
VkTessellationDomainOrigin str_to_VkTessellationDomainOrigin(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkTessellationDomainOrigin(VkTessellationDomainOrigin e);
VkSamplerYcbcrModelConversion str_to_VkSamplerYcbcrModelConversion(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSamplerYcbcrModelConversion(VkSamplerYcbcrModelConversion e);
VkSamplerYcbcrRange str_to_VkSamplerYcbcrRange(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSamplerYcbcrRange(VkSamplerYcbcrRange e);
VkChromaLocation str_to_VkChromaLocation(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkChromaLocation(VkChromaLocation e);
VkDescriptorUpdateTemplateType str_to_VkDescriptorUpdateTemplateType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDescriptorUpdateTemplateType(VkDescriptorUpdateTemplateType e);
VkSubgroupFeatureFlagBits str_to_VkSubgroupFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSubgroupFeatureFlagBits(VkSubgroupFeatureFlagBits e);
std::string str_from_VkSubgroupFeatureFlags(VkSubgroupFeatureFlags f);
VkPeerMemoryFeatureFlagBits str_to_VkPeerMemoryFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPeerMemoryFeatureFlagBits(VkPeerMemoryFeatureFlagBits e);
std::string str_from_VkPeerMemoryFeatureFlags(VkPeerMemoryFeatureFlags f);
VkMemoryAllocateFlagBits str_to_VkMemoryAllocateFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkMemoryAllocateFlagBits(VkMemoryAllocateFlagBits e);
std::string str_from_VkMemoryAllocateFlags(VkMemoryAllocateFlags f);
VkExternalMemoryHandleTypeFlagBits str_to_VkExternalMemoryHandleTypeFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalMemoryHandleTypeFlagBits(VkExternalMemoryHandleTypeFlagBits e);
std::string str_from_VkExternalMemoryHandleTypeFlags(VkExternalMemoryHandleTypeFlags f);
VkExternalMemoryFeatureFlagBits str_to_VkExternalMemoryFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalMemoryFeatureFlagBits(VkExternalMemoryFeatureFlagBits e);
std::string str_from_VkExternalMemoryFeatureFlags(VkExternalMemoryFeatureFlags f);
VkExternalFenceHandleTypeFlagBits str_to_VkExternalFenceHandleTypeFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalFenceHandleTypeFlagBits(VkExternalFenceHandleTypeFlagBits e);
std::string str_from_VkExternalFenceHandleTypeFlags(VkExternalFenceHandleTypeFlags f);
VkExternalFenceFeatureFlagBits str_to_VkExternalFenceFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalFenceFeatureFlagBits(VkExternalFenceFeatureFlagBits e);
std::string str_from_VkExternalFenceFeatureFlags(VkExternalFenceFeatureFlags f);
VkFenceImportFlagBits str_to_VkFenceImportFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFenceImportFlagBits(VkFenceImportFlagBits e);
std::string str_from_VkFenceImportFlags(VkFenceImportFlags f);
VkSemaphoreImportFlagBits str_to_VkSemaphoreImportFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSemaphoreImportFlagBits(VkSemaphoreImportFlagBits e);
std::string str_from_VkSemaphoreImportFlags(VkSemaphoreImportFlags f);
VkExternalSemaphoreHandleTypeFlagBits str_to_VkExternalSemaphoreHandleTypeFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalSemaphoreHandleTypeFlagBits(VkExternalSemaphoreHandleTypeFlagBits e);
std::string str_from_VkExternalSemaphoreHandleTypeFlags(VkExternalSemaphoreHandleTypeFlags f);
VkExternalSemaphoreFeatureFlagBits str_to_VkExternalSemaphoreFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalSemaphoreFeatureFlagBits(VkExternalSemaphoreFeatureFlagBits e);
std::string str_from_VkExternalSemaphoreFeatureFlags(VkExternalSemaphoreFeatureFlags f);
#endif
#if VK_VERSION_1_2
VkDriverId str_to_VkDriverId(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDriverId(VkDriverId e);
VkShaderFloatControlsIndependence str_to_VkShaderFloatControlsIndependence(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkShaderFloatControlsIndependence(VkShaderFloatControlsIndependence e);
VkSamplerReductionMode str_to_VkSamplerReductionMode(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSamplerReductionMode(VkSamplerReductionMode e);
VkSemaphoreType str_to_VkSemaphoreType(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSemaphoreType(VkSemaphoreType e);
VkResolveModeFlagBits str_to_VkResolveModeFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkResolveModeFlagBits(VkResolveModeFlagBits e);
std::string str_from_VkResolveModeFlags(VkResolveModeFlags f);
VkDescriptorBindingFlagBits str_to_VkDescriptorBindingFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDescriptorBindingFlagBits(VkDescriptorBindingFlagBits e);
std::string str_from_VkDescriptorBindingFlags(VkDescriptorBindingFlags f);
VkSemaphoreWaitFlagBits str_to_VkSemaphoreWaitFlagBits(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSemaphoreWaitFlagBits(VkSemaphoreWaitFlagBits e);
std::string str_from_VkSemaphoreWaitFlags(VkSemaphoreWaitFlags f);
#endif
#if VK_KHR_surface
VkPresentModeKHR str_to_VkPresentModeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPresentModeKHR(VkPresentModeKHR e);
VkColorSpaceKHR str_to_VkColorSpaceKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkColorSpaceKHR(VkColorSpaceKHR e);
VkSurfaceTransformFlagBitsKHR str_to_VkSurfaceTransformFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSurfaceTransformFlagBitsKHR(VkSurfaceTransformFlagBitsKHR e);
std::string str_from_VkSurfaceTransformFlagsKHR(VkSurfaceTransformFlagsKHR f);
VkCompositeAlphaFlagBitsKHR str_to_VkCompositeAlphaFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCompositeAlphaFlagBitsKHR(VkCompositeAlphaFlagBitsKHR e);
std::string str_from_VkCompositeAlphaFlagsKHR(VkCompositeAlphaFlagsKHR f);
#endif
#if VK_KHR_swapchain
VkSwapchainCreateFlagBitsKHR str_to_VkSwapchainCreateFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSwapchainCreateFlagBitsKHR(VkSwapchainCreateFlagBitsKHR e);
std::string str_from_VkSwapchainCreateFlagsKHR(VkSwapchainCreateFlagsKHR f);
VkDeviceGroupPresentModeFlagBitsKHR str_to_VkDeviceGroupPresentModeFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDeviceGroupPresentModeFlagBitsKHR(VkDeviceGroupPresentModeFlagBitsKHR e);
std::string str_from_VkDeviceGroupPresentModeFlagsKHR(VkDeviceGroupPresentModeFlagsKHR f);
#endif
#if VK_KHR_display
VkDisplayPlaneAlphaFlagBitsKHR str_to_VkDisplayPlaneAlphaFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDisplayPlaneAlphaFlagBitsKHR(VkDisplayPlaneAlphaFlagBitsKHR e);
std::string str_from_VkDisplayPlaneAlphaFlagsKHR(VkDisplayPlaneAlphaFlagsKHR f);
#endif
#if VK_KHR_dynamic_rendering
VkRenderingFlagBitsKHR str_to_VkRenderingFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkRenderingFlagBitsKHR(VkRenderingFlagBitsKHR e);
std::string str_from_VkRenderingFlagsKHR(VkRenderingFlagsKHR f);
#endif
#if VK_KHR_performance_query
VkPerformanceCounterUnitKHR str_to_VkPerformanceCounterUnitKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceCounterUnitKHR(VkPerformanceCounterUnitKHR e);
VkPerformanceCounterScopeKHR str_to_VkPerformanceCounterScopeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceCounterScopeKHR(VkPerformanceCounterScopeKHR e);
VkPerformanceCounterStorageKHR str_to_VkPerformanceCounterStorageKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceCounterStorageKHR(VkPerformanceCounterStorageKHR e);
VkPerformanceCounterDescriptionFlagBitsKHR str_to_VkPerformanceCounterDescriptionFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceCounterDescriptionFlagBitsKHR(VkPerformanceCounterDescriptionFlagBitsKHR e);
std::string str_from_VkPerformanceCounterDescriptionFlagsKHR(VkPerformanceCounterDescriptionFlagsKHR f);
VkAcquireProfilingLockFlagBitsKHR str_to_VkAcquireProfilingLockFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAcquireProfilingLockFlagBitsKHR(VkAcquireProfilingLockFlagBitsKHR e);
std::string str_from_VkAcquireProfilingLockFlagsKHR(VkAcquireProfilingLockFlagsKHR f);
#endif
#if VK_KHR_fragment_shading_rate
VkFragmentShadingRateCombinerOpKHR str_to_VkFragmentShadingRateCombinerOpKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFragmentShadingRateCombinerOpKHR(VkFragmentShadingRateCombinerOpKHR e);
#endif
#if VK_KHR_pipeline_executable_properties
VkPipelineExecutableStatisticFormatKHR str_to_VkPipelineExecutableStatisticFormatKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineExecutableStatisticFormatKHR(VkPipelineExecutableStatisticFormatKHR e);
#endif
#if VK_KHR_synchronization2
VkSubmitFlagBitsKHR str_to_VkSubmitFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSubmitFlagBitsKHR(VkSubmitFlagBitsKHR e);
std::string str_from_VkSubmitFlagsKHR(VkSubmitFlagsKHR f);
#endif
#if VK_EXT_debug_report
VkDebugReportObjectTypeEXT str_to_VkDebugReportObjectTypeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDebugReportObjectTypeEXT(VkDebugReportObjectTypeEXT e);
VkDebugReportFlagBitsEXT str_to_VkDebugReportFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDebugReportFlagBitsEXT(VkDebugReportFlagBitsEXT e);
std::string str_from_VkDebugReportFlagsEXT(VkDebugReportFlagsEXT f);
#endif
#if VK_AMD_rasterization_order
VkRasterizationOrderAMD str_to_VkRasterizationOrderAMD(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkRasterizationOrderAMD(VkRasterizationOrderAMD e);
#endif
#if VK_AMD_shader_info
VkShaderInfoTypeAMD str_to_VkShaderInfoTypeAMD(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkShaderInfoTypeAMD(VkShaderInfoTypeAMD e);
#endif
#if VK_NV_external_memory_capabilities
VkExternalMemoryHandleTypeFlagBitsNV str_to_VkExternalMemoryHandleTypeFlagBitsNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalMemoryHandleTypeFlagBitsNV(VkExternalMemoryHandleTypeFlagBitsNV e);
std::string str_from_VkExternalMemoryHandleTypeFlagsNV(VkExternalMemoryHandleTypeFlagsNV f);
VkExternalMemoryFeatureFlagBitsNV str_to_VkExternalMemoryFeatureFlagBitsNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkExternalMemoryFeatureFlagBitsNV(VkExternalMemoryFeatureFlagBitsNV e);
std::string str_from_VkExternalMemoryFeatureFlagsNV(VkExternalMemoryFeatureFlagsNV f);
#endif
#if VK_EXT_validation_flags
VkValidationCheckEXT str_to_VkValidationCheckEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkValidationCheckEXT(VkValidationCheckEXT e);
#endif
#if VK_EXT_conditional_rendering
VkConditionalRenderingFlagBitsEXT str_to_VkConditionalRenderingFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkConditionalRenderingFlagBitsEXT(VkConditionalRenderingFlagBitsEXT e);
std::string str_from_VkConditionalRenderingFlagsEXT(VkConditionalRenderingFlagsEXT f);
#endif
#if VK_EXT_display_surface_counter
VkSurfaceCounterFlagBitsEXT str_to_VkSurfaceCounterFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkSurfaceCounterFlagBitsEXT(VkSurfaceCounterFlagBitsEXT e);
std::string str_from_VkSurfaceCounterFlagsEXT(VkSurfaceCounterFlagsEXT f);
#endif
#if VK_EXT_display_control
VkDisplayPowerStateEXT str_to_VkDisplayPowerStateEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDisplayPowerStateEXT(VkDisplayPowerStateEXT e);
VkDeviceEventTypeEXT str_to_VkDeviceEventTypeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDeviceEventTypeEXT(VkDeviceEventTypeEXT e);
VkDisplayEventTypeEXT str_to_VkDisplayEventTypeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDisplayEventTypeEXT(VkDisplayEventTypeEXT e);
#endif
#if VK_NV_viewport_swizzle
VkViewportCoordinateSwizzleNV str_to_VkViewportCoordinateSwizzleNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkViewportCoordinateSwizzleNV(VkViewportCoordinateSwizzleNV e);
#endif
#if VK_EXT_discard_rectangles
VkDiscardRectangleModeEXT str_to_VkDiscardRectangleModeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDiscardRectangleModeEXT(VkDiscardRectangleModeEXT e);
#endif
#if VK_EXT_conservative_rasterization
VkConservativeRasterizationModeEXT str_to_VkConservativeRasterizationModeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkConservativeRasterizationModeEXT(VkConservativeRasterizationModeEXT e);
#endif
#if VK_EXT_debug_utils
VkDebugUtilsMessageSeverityFlagBitsEXT str_to_VkDebugUtilsMessageSeverityFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDebugUtilsMessageSeverityFlagBitsEXT(VkDebugUtilsMessageSeverityFlagBitsEXT e);
std::string str_from_VkDebugUtilsMessageSeverityFlagsEXT(VkDebugUtilsMessageSeverityFlagsEXT f);
VkDebugUtilsMessageTypeFlagBitsEXT str_to_VkDebugUtilsMessageTypeFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDebugUtilsMessageTypeFlagBitsEXT(VkDebugUtilsMessageTypeFlagBitsEXT e);
std::string str_from_VkDebugUtilsMessageTypeFlagsEXT(VkDebugUtilsMessageTypeFlagsEXT f);
#endif
#if VK_EXT_blend_operation_advanced
VkBlendOverlapEXT str_to_VkBlendOverlapEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBlendOverlapEXT(VkBlendOverlapEXT e);
#endif
#if VK_NV_framebuffer_mixed_samples
VkCoverageModulationModeNV str_to_VkCoverageModulationModeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCoverageModulationModeNV(VkCoverageModulationModeNV e);
#endif
#if VK_EXT_validation_cache
VkValidationCacheHeaderVersionEXT str_to_VkValidationCacheHeaderVersionEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkValidationCacheHeaderVersionEXT(VkValidationCacheHeaderVersionEXT e);
#endif
#if VK_NV_shading_rate_image
VkShadingRatePaletteEntryNV str_to_VkShadingRatePaletteEntryNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkShadingRatePaletteEntryNV(VkShadingRatePaletteEntryNV e);
VkCoarseSampleOrderTypeNV str_to_VkCoarseSampleOrderTypeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCoarseSampleOrderTypeNV(VkCoarseSampleOrderTypeNV e);
#endif
#if VK_NV_ray_tracing
VkRayTracingShaderGroupTypeKHR str_to_VkRayTracingShaderGroupTypeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkRayTracingShaderGroupTypeKHR(VkRayTracingShaderGroupTypeKHR e);
VkGeometryTypeKHR str_to_VkGeometryTypeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkGeometryTypeKHR(VkGeometryTypeKHR e);
VkAccelerationStructureTypeKHR str_to_VkAccelerationStructureTypeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccelerationStructureTypeKHR(VkAccelerationStructureTypeKHR e);
VkCopyAccelerationStructureModeKHR str_to_VkCopyAccelerationStructureModeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCopyAccelerationStructureModeKHR(VkCopyAccelerationStructureModeKHR e);
VkAccelerationStructureMemoryRequirementsTypeNV str_to_VkAccelerationStructureMemoryRequirementsTypeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccelerationStructureMemoryRequirementsTypeNV(VkAccelerationStructureMemoryRequirementsTypeNV e);
VkGeometryFlagBitsKHR str_to_VkGeometryFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkGeometryFlagBitsKHR(VkGeometryFlagBitsKHR e);
std::string str_from_VkGeometryFlagsKHR(VkGeometryFlagsKHR f);
VkGeometryInstanceFlagBitsKHR str_to_VkGeometryInstanceFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkGeometryInstanceFlagBitsKHR(VkGeometryInstanceFlagBitsKHR e);
std::string str_from_VkGeometryInstanceFlagsKHR(VkGeometryInstanceFlagsKHR f);
VkBuildAccelerationStructureFlagBitsKHR str_to_VkBuildAccelerationStructureFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBuildAccelerationStructureFlagBitsKHR(VkBuildAccelerationStructureFlagBitsKHR e);
std::string str_from_VkBuildAccelerationStructureFlagsKHR(VkBuildAccelerationStructureFlagsKHR f);
#endif
#if VK_EXT_global_priority
VkQueueGlobalPriorityEXT str_to_VkQueueGlobalPriorityEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueueGlobalPriorityEXT(VkQueueGlobalPriorityEXT e);
#endif
#if VK_AMD_pipeline_compiler_control
VkPipelineCompilerControlFlagBitsAMD str_to_VkPipelineCompilerControlFlagBitsAMD(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineCompilerControlFlagBitsAMD(VkPipelineCompilerControlFlagBitsAMD e);
std::string str_from_VkPipelineCompilerControlFlagsAMD(VkPipelineCompilerControlFlagsAMD f);
#endif
#if VK_EXT_calibrated_timestamps
VkTimeDomainEXT str_to_VkTimeDomainEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkTimeDomainEXT(VkTimeDomainEXT e);
#endif
#if VK_AMD_memory_overallocation_behavior
VkMemoryOverallocationBehaviorAMD str_to_VkMemoryOverallocationBehaviorAMD(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkMemoryOverallocationBehaviorAMD(VkMemoryOverallocationBehaviorAMD e);
#endif
#if VK_EXT_pipeline_creation_feedback
VkPipelineCreationFeedbackFlagBitsEXT str_to_VkPipelineCreationFeedbackFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPipelineCreationFeedbackFlagBitsEXT(VkPipelineCreationFeedbackFlagBitsEXT e);
std::string str_from_VkPipelineCreationFeedbackFlagsEXT(VkPipelineCreationFeedbackFlagsEXT f);
#endif
#if VK_INTEL_performance_query
VkPerformanceConfigurationTypeINTEL str_to_VkPerformanceConfigurationTypeINTEL(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceConfigurationTypeINTEL(VkPerformanceConfigurationTypeINTEL e);
VkQueryPoolSamplingModeINTEL str_to_VkQueryPoolSamplingModeINTEL(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkQueryPoolSamplingModeINTEL(VkQueryPoolSamplingModeINTEL e);
VkPerformanceOverrideTypeINTEL str_to_VkPerformanceOverrideTypeINTEL(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceOverrideTypeINTEL(VkPerformanceOverrideTypeINTEL e);
VkPerformanceParameterTypeINTEL str_to_VkPerformanceParameterTypeINTEL(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceParameterTypeINTEL(VkPerformanceParameterTypeINTEL e);
VkPerformanceValueTypeINTEL str_to_VkPerformanceValueTypeINTEL(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPerformanceValueTypeINTEL(VkPerformanceValueTypeINTEL e);
#endif
#if VK_AMD_shader_core_properties2
VkShaderCorePropertiesFlagBitsAMD str_to_VkShaderCorePropertiesFlagBitsAMD(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkShaderCorePropertiesFlagBitsAMD(VkShaderCorePropertiesFlagBitsAMD e);
std::string str_from_VkShaderCorePropertiesFlagsAMD(VkShaderCorePropertiesFlagsAMD f);
#endif
#if VK_EXT_tooling_info
VkToolPurposeFlagBitsEXT str_to_VkToolPurposeFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkToolPurposeFlagBitsEXT(VkToolPurposeFlagBitsEXT e);
std::string str_from_VkToolPurposeFlagsEXT(VkToolPurposeFlagsEXT f);
#endif
#if VK_EXT_validation_features
VkValidationFeatureEnableEXT str_to_VkValidationFeatureEnableEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkValidationFeatureEnableEXT(VkValidationFeatureEnableEXT e);
VkValidationFeatureDisableEXT str_to_VkValidationFeatureDisableEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkValidationFeatureDisableEXT(VkValidationFeatureDisableEXT e);
#endif
#if VK_NV_cooperative_matrix
VkComponentTypeNV str_to_VkComponentTypeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkComponentTypeNV(VkComponentTypeNV e);
VkScopeNV str_to_VkScopeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkScopeNV(VkScopeNV e);
#endif
#if VK_NV_coverage_reduction_mode
VkCoverageReductionModeNV str_to_VkCoverageReductionModeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkCoverageReductionModeNV(VkCoverageReductionModeNV e);
#endif
#if VK_EXT_provoking_vertex
VkProvokingVertexModeEXT str_to_VkProvokingVertexModeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkProvokingVertexModeEXT(VkProvokingVertexModeEXT e);
#endif
#if VK_EXT_line_rasterization
VkLineRasterizationModeEXT str_to_VkLineRasterizationModeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkLineRasterizationModeEXT(VkLineRasterizationModeEXT e);
#endif
#if VK_NV_device_generated_commands
VkIndirectCommandsTokenTypeNV str_to_VkIndirectCommandsTokenTypeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkIndirectCommandsTokenTypeNV(VkIndirectCommandsTokenTypeNV e);
VkIndirectStateFlagBitsNV str_to_VkIndirectStateFlagBitsNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkIndirectStateFlagBitsNV(VkIndirectStateFlagBitsNV e);
std::string str_from_VkIndirectStateFlagsNV(VkIndirectStateFlagsNV f);
VkIndirectCommandsLayoutUsageFlagBitsNV str_to_VkIndirectCommandsLayoutUsageFlagBitsNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkIndirectCommandsLayoutUsageFlagBitsNV(VkIndirectCommandsLayoutUsageFlagBitsNV e);
std::string str_from_VkIndirectCommandsLayoutUsageFlagsNV(VkIndirectCommandsLayoutUsageFlagsNV f);
#endif
#if VK_EXT_device_memory_report
VkDeviceMemoryReportEventTypeEXT str_to_VkDeviceMemoryReportEventTypeEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDeviceMemoryReportEventTypeEXT(VkDeviceMemoryReportEventTypeEXT e);
#endif
#if VK_EXT_private_data
VkPrivateDataSlotCreateFlagBitsEXT str_to_VkPrivateDataSlotCreateFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkPrivateDataSlotCreateFlagBitsEXT(VkPrivateDataSlotCreateFlagBitsEXT e);
std::string str_from_VkPrivateDataSlotCreateFlagsEXT(VkPrivateDataSlotCreateFlagsEXT f);
#endif
#if VK_NV_device_diagnostics_config
VkDeviceDiagnosticsConfigFlagBitsNV str_to_VkDeviceDiagnosticsConfigFlagBitsNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkDeviceDiagnosticsConfigFlagBitsNV(VkDeviceDiagnosticsConfigFlagBitsNV e);
std::string str_from_VkDeviceDiagnosticsConfigFlagsNV(VkDeviceDiagnosticsConfigFlagsNV f);
#endif
#if VK_NV_fragment_shading_rate_enums
VkFragmentShadingRateTypeNV str_to_VkFragmentShadingRateTypeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFragmentShadingRateTypeNV(VkFragmentShadingRateTypeNV e);
VkFragmentShadingRateNV str_to_VkFragmentShadingRateNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkFragmentShadingRateNV(VkFragmentShadingRateNV e);
#endif
#if VK_NV_ray_tracing_motion_blur
VkAccelerationStructureMotionInstanceTypeNV str_to_VkAccelerationStructureMotionInstanceTypeNV(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccelerationStructureMotionInstanceTypeNV(VkAccelerationStructureMotionInstanceTypeNV e);
#endif
#if VK_KHR_acceleration_structure
VkBuildAccelerationStructureModeKHR str_to_VkBuildAccelerationStructureModeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkBuildAccelerationStructureModeKHR(VkBuildAccelerationStructureModeKHR e);
VkAccelerationStructureBuildTypeKHR str_to_VkAccelerationStructureBuildTypeKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccelerationStructureBuildTypeKHR(VkAccelerationStructureBuildTypeKHR e);
VkAccelerationStructureCompatibilityKHR str_to_VkAccelerationStructureCompatibilityKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccelerationStructureCompatibilityKHR(VkAccelerationStructureCompatibilityKHR e);
VkAccelerationStructureCreateFlagBitsKHR str_to_VkAccelerationStructureCreateFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkAccelerationStructureCreateFlagBitsKHR(VkAccelerationStructureCreateFlagBitsKHR e);
std::string str_from_VkAccelerationStructureCreateFlagsKHR(VkAccelerationStructureCreateFlagsKHR f);
#endif
#if VK_KHR_ray_tracing_pipeline
VkShaderGroupShaderKHR str_to_VkShaderGroupShaderKHR(std::string_view a_str, bool* a_ok_ptr = nullptr);
std::string_view str_from_VkShaderGroupShaderKHR(VkShaderGroupShaderKHR e);
#endif
#if VK_VERSION_1_0
VkBufferMemoryBarrier make_VkBufferMemoryBarrier();
VkImageMemoryBarrier make_VkImageMemoryBarrier();
VkMemoryBarrier make_VkMemoryBarrier();
VkApplicationInfo make_VkApplicationInfo();
VkInstanceCreateInfo make_VkInstanceCreateInfo();
VkDeviceQueueCreateInfo make_VkDeviceQueueCreateInfo();
VkDeviceCreateInfo make_VkDeviceCreateInfo();
VkSubmitInfo make_VkSubmitInfo();
VkMappedMemoryRange make_VkMappedMemoryRange();
VkMemoryAllocateInfo make_VkMemoryAllocateInfo();
VkBindSparseInfo make_VkBindSparseInfo();
VkFenceCreateInfo make_VkFenceCreateInfo();
VkSemaphoreCreateInfo make_VkSemaphoreCreateInfo();
VkEventCreateInfo make_VkEventCreateInfo();
VkQueryPoolCreateInfo make_VkQueryPoolCreateInfo();
VkBufferCreateInfo make_VkBufferCreateInfo();
VkBufferViewCreateInfo make_VkBufferViewCreateInfo();
VkImageCreateInfo make_VkImageCreateInfo();
VkImageViewCreateInfo make_VkImageViewCreateInfo();
VkShaderModuleCreateInfo make_VkShaderModuleCreateInfo();
VkPipelineCacheCreateInfo make_VkPipelineCacheCreateInfo();
VkPipelineShaderStageCreateInfo make_VkPipelineShaderStageCreateInfo();
VkComputePipelineCreateInfo make_VkComputePipelineCreateInfo();
VkPipelineVertexInputStateCreateInfo make_VkPipelineVertexInputStateCreateInfo();
VkPipelineInputAssemblyStateCreateInfo make_VkPipelineInputAssemblyStateCreateInfo();
VkPipelineTessellationStateCreateInfo make_VkPipelineTessellationStateCreateInfo();
VkPipelineViewportStateCreateInfo make_VkPipelineViewportStateCreateInfo();
VkPipelineRasterizationStateCreateInfo make_VkPipelineRasterizationStateCreateInfo();
VkPipelineMultisampleStateCreateInfo make_VkPipelineMultisampleStateCreateInfo();
VkPipelineDepthStencilStateCreateInfo make_VkPipelineDepthStencilStateCreateInfo();
VkPipelineColorBlendStateCreateInfo make_VkPipelineColorBlendStateCreateInfo();
VkPipelineDynamicStateCreateInfo make_VkPipelineDynamicStateCreateInfo();
VkGraphicsPipelineCreateInfo make_VkGraphicsPipelineCreateInfo();
VkPipelineLayoutCreateInfo make_VkPipelineLayoutCreateInfo();
VkSamplerCreateInfo make_VkSamplerCreateInfo();
VkCopyDescriptorSet make_VkCopyDescriptorSet();
VkDescriptorPoolCreateInfo make_VkDescriptorPoolCreateInfo();
VkDescriptorSetAllocateInfo make_VkDescriptorSetAllocateInfo();
VkDescriptorSetLayoutCreateInfo make_VkDescriptorSetLayoutCreateInfo();
VkWriteDescriptorSet make_VkWriteDescriptorSet();
VkFramebufferCreateInfo make_VkFramebufferCreateInfo();
VkRenderPassCreateInfo make_VkRenderPassCreateInfo();
VkCommandPoolCreateInfo make_VkCommandPoolCreateInfo();
VkCommandBufferAllocateInfo make_VkCommandBufferAllocateInfo();
VkCommandBufferInheritanceInfo make_VkCommandBufferInheritanceInfo();
VkCommandBufferBeginInfo make_VkCommandBufferBeginInfo();
VkRenderPassBeginInfo make_VkRenderPassBeginInfo();
#endif

#if VK_VERSION_1_1
VkPhysicalDeviceSubgroupProperties make_VkPhysicalDeviceSubgroupProperties();
VkBindBufferMemoryInfo make_VkBindBufferMemoryInfo();
VkBindImageMemoryInfo make_VkBindImageMemoryInfo();
VkPhysicalDevice16BitStorageFeatures make_VkPhysicalDevice16BitStorageFeatures();
VkMemoryDedicatedRequirements make_VkMemoryDedicatedRequirements();
VkMemoryDedicatedAllocateInfo make_VkMemoryDedicatedAllocateInfo();
VkMemoryAllocateFlagsInfo make_VkMemoryAllocateFlagsInfo();
VkDeviceGroupRenderPassBeginInfo make_VkDeviceGroupRenderPassBeginInfo();
VkDeviceGroupCommandBufferBeginInfo make_VkDeviceGroupCommandBufferBeginInfo();
VkDeviceGroupSubmitInfo make_VkDeviceGroupSubmitInfo();
VkDeviceGroupBindSparseInfo make_VkDeviceGroupBindSparseInfo();
VkBindBufferMemoryDeviceGroupInfo make_VkBindBufferMemoryDeviceGroupInfo();
VkBindImageMemoryDeviceGroupInfo make_VkBindImageMemoryDeviceGroupInfo();
VkPhysicalDeviceGroupProperties make_VkPhysicalDeviceGroupProperties();
VkDeviceGroupDeviceCreateInfo make_VkDeviceGroupDeviceCreateInfo();
VkBufferMemoryRequirementsInfo2 make_VkBufferMemoryRequirementsInfo2();
VkImageMemoryRequirementsInfo2 make_VkImageMemoryRequirementsInfo2();
VkImageSparseMemoryRequirementsInfo2 make_VkImageSparseMemoryRequirementsInfo2();
VkMemoryRequirements2 make_VkMemoryRequirements2();
VkSparseImageMemoryRequirements2 make_VkSparseImageMemoryRequirements2();
VkPhysicalDeviceFeatures2 make_VkPhysicalDeviceFeatures2();
VkPhysicalDeviceProperties2 make_VkPhysicalDeviceProperties2();
VkFormatProperties2 make_VkFormatProperties2();
VkImageFormatProperties2 make_VkImageFormatProperties2();
VkPhysicalDeviceImageFormatInfo2 make_VkPhysicalDeviceImageFormatInfo2();
VkQueueFamilyProperties2 make_VkQueueFamilyProperties2();
VkPhysicalDeviceMemoryProperties2 make_VkPhysicalDeviceMemoryProperties2();
VkSparseImageFormatProperties2 make_VkSparseImageFormatProperties2();
VkPhysicalDeviceSparseImageFormatInfo2 make_VkPhysicalDeviceSparseImageFormatInfo2();
VkPhysicalDevicePointClippingProperties make_VkPhysicalDevicePointClippingProperties();
VkRenderPassInputAttachmentAspectCreateInfo make_VkRenderPassInputAttachmentAspectCreateInfo();
VkImageViewUsageCreateInfo make_VkImageViewUsageCreateInfo();
VkPipelineTessellationDomainOriginStateCreateInfo make_VkPipelineTessellationDomainOriginStateCreateInfo();
VkRenderPassMultiviewCreateInfo make_VkRenderPassMultiviewCreateInfo();
VkPhysicalDeviceMultiviewFeatures make_VkPhysicalDeviceMultiviewFeatures();
VkPhysicalDeviceMultiviewProperties make_VkPhysicalDeviceMultiviewProperties();
VkPhysicalDeviceVariablePointersFeatures make_VkPhysicalDeviceVariablePointersFeatures();
VkPhysicalDeviceProtectedMemoryFeatures make_VkPhysicalDeviceProtectedMemoryFeatures();
VkPhysicalDeviceProtectedMemoryProperties make_VkPhysicalDeviceProtectedMemoryProperties();
VkDeviceQueueInfo2 make_VkDeviceQueueInfo2();
VkProtectedSubmitInfo make_VkProtectedSubmitInfo();
VkSamplerYcbcrConversionCreateInfo make_VkSamplerYcbcrConversionCreateInfo();
VkSamplerYcbcrConversionInfo make_VkSamplerYcbcrConversionInfo();
VkBindImagePlaneMemoryInfo make_VkBindImagePlaneMemoryInfo();
VkImagePlaneMemoryRequirementsInfo make_VkImagePlaneMemoryRequirementsInfo();
VkPhysicalDeviceSamplerYcbcrConversionFeatures make_VkPhysicalDeviceSamplerYcbcrConversionFeatures();
VkSamplerYcbcrConversionImageFormatProperties make_VkSamplerYcbcrConversionImageFormatProperties();
VkDescriptorUpdateTemplateCreateInfo make_VkDescriptorUpdateTemplateCreateInfo();
VkPhysicalDeviceExternalImageFormatInfo make_VkPhysicalDeviceExternalImageFormatInfo();
VkExternalImageFormatProperties make_VkExternalImageFormatProperties();
VkPhysicalDeviceExternalBufferInfo make_VkPhysicalDeviceExternalBufferInfo();
VkExternalBufferProperties make_VkExternalBufferProperties();
VkPhysicalDeviceIDProperties make_VkPhysicalDeviceIDProperties();
VkExternalMemoryImageCreateInfo make_VkExternalMemoryImageCreateInfo();
VkExternalMemoryBufferCreateInfo make_VkExternalMemoryBufferCreateInfo();
VkExportMemoryAllocateInfo make_VkExportMemoryAllocateInfo();
VkPhysicalDeviceExternalFenceInfo make_VkPhysicalDeviceExternalFenceInfo();
VkExternalFenceProperties make_VkExternalFenceProperties();
VkExportFenceCreateInfo make_VkExportFenceCreateInfo();
VkExportSemaphoreCreateInfo make_VkExportSemaphoreCreateInfo();
VkPhysicalDeviceExternalSemaphoreInfo make_VkPhysicalDeviceExternalSemaphoreInfo();
VkExternalSemaphoreProperties make_VkExternalSemaphoreProperties();
VkPhysicalDeviceMaintenance3Properties make_VkPhysicalDeviceMaintenance3Properties();
VkDescriptorSetLayoutSupport make_VkDescriptorSetLayoutSupport();
VkPhysicalDeviceShaderDrawParametersFeatures make_VkPhysicalDeviceShaderDrawParametersFeatures();
#endif

#if VK_VERSION_1_2
VkPhysicalDeviceVulkan11Features make_VkPhysicalDeviceVulkan11Features();
VkPhysicalDeviceVulkan11Properties make_VkPhysicalDeviceVulkan11Properties();
VkPhysicalDeviceVulkan12Features make_VkPhysicalDeviceVulkan12Features();
VkPhysicalDeviceVulkan12Properties make_VkPhysicalDeviceVulkan12Properties();
VkImageFormatListCreateInfo make_VkImageFormatListCreateInfo();
VkAttachmentDescription2 make_VkAttachmentDescription2();
VkAttachmentReference2 make_VkAttachmentReference2();
VkSubpassDescription2 make_VkSubpassDescription2();
VkSubpassDependency2 make_VkSubpassDependency2();
VkRenderPassCreateInfo2 make_VkRenderPassCreateInfo2();
VkSubpassBeginInfo make_VkSubpassBeginInfo();
VkSubpassEndInfo make_VkSubpassEndInfo();
VkPhysicalDevice8BitStorageFeatures make_VkPhysicalDevice8BitStorageFeatures();
VkPhysicalDeviceDriverProperties make_VkPhysicalDeviceDriverProperties();
VkPhysicalDeviceShaderAtomicInt64Features make_VkPhysicalDeviceShaderAtomicInt64Features();
VkPhysicalDeviceShaderFloat16Int8Features make_VkPhysicalDeviceShaderFloat16Int8Features();
VkPhysicalDeviceFloatControlsProperties make_VkPhysicalDeviceFloatControlsProperties();
VkDescriptorSetLayoutBindingFlagsCreateInfo make_VkDescriptorSetLayoutBindingFlagsCreateInfo();
VkPhysicalDeviceDescriptorIndexingFeatures make_VkPhysicalDeviceDescriptorIndexingFeatures();
VkPhysicalDeviceDescriptorIndexingProperties make_VkPhysicalDeviceDescriptorIndexingProperties();
VkDescriptorSetVariableDescriptorCountAllocateInfo make_VkDescriptorSetVariableDescriptorCountAllocateInfo();
VkDescriptorSetVariableDescriptorCountLayoutSupport make_VkDescriptorSetVariableDescriptorCountLayoutSupport();
VkSubpassDescriptionDepthStencilResolve make_VkSubpassDescriptionDepthStencilResolve();
VkPhysicalDeviceDepthStencilResolveProperties make_VkPhysicalDeviceDepthStencilResolveProperties();
VkPhysicalDeviceScalarBlockLayoutFeatures make_VkPhysicalDeviceScalarBlockLayoutFeatures();
VkImageStencilUsageCreateInfo make_VkImageStencilUsageCreateInfo();
VkSamplerReductionModeCreateInfo make_VkSamplerReductionModeCreateInfo();
VkPhysicalDeviceSamplerFilterMinmaxProperties make_VkPhysicalDeviceSamplerFilterMinmaxProperties();
VkPhysicalDeviceVulkanMemoryModelFeatures make_VkPhysicalDeviceVulkanMemoryModelFeatures();
VkPhysicalDeviceImagelessFramebufferFeatures make_VkPhysicalDeviceImagelessFramebufferFeatures();
VkFramebufferAttachmentImageInfo make_VkFramebufferAttachmentImageInfo();
VkFramebufferAttachmentsCreateInfo make_VkFramebufferAttachmentsCreateInfo();
VkRenderPassAttachmentBeginInfo make_VkRenderPassAttachmentBeginInfo();
VkPhysicalDeviceUniformBufferStandardLayoutFeatures make_VkPhysicalDeviceUniformBufferStandardLayoutFeatures();
VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures make_VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures();
VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures make_VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures();
VkAttachmentReferenceStencilLayout make_VkAttachmentReferenceStencilLayout();
VkAttachmentDescriptionStencilLayout make_VkAttachmentDescriptionStencilLayout();
VkPhysicalDeviceHostQueryResetFeatures make_VkPhysicalDeviceHostQueryResetFeatures();
VkPhysicalDeviceTimelineSemaphoreFeatures make_VkPhysicalDeviceTimelineSemaphoreFeatures();
VkPhysicalDeviceTimelineSemaphoreProperties make_VkPhysicalDeviceTimelineSemaphoreProperties();
VkSemaphoreTypeCreateInfo make_VkSemaphoreTypeCreateInfo();
VkTimelineSemaphoreSubmitInfo make_VkTimelineSemaphoreSubmitInfo();
VkSemaphoreWaitInfo make_VkSemaphoreWaitInfo();
VkSemaphoreSignalInfo make_VkSemaphoreSignalInfo();
VkPhysicalDeviceBufferDeviceAddressFeatures make_VkPhysicalDeviceBufferDeviceAddressFeatures();
VkBufferDeviceAddressInfo make_VkBufferDeviceAddressInfo();
VkBufferOpaqueCaptureAddressCreateInfo make_VkBufferOpaqueCaptureAddressCreateInfo();
VkMemoryOpaqueCaptureAddressAllocateInfo make_VkMemoryOpaqueCaptureAddressAllocateInfo();
VkDeviceMemoryOpaqueCaptureAddressInfo make_VkDeviceMemoryOpaqueCaptureAddressInfo();
#endif

#if VK_KHR_swapchain
VkSwapchainCreateInfoKHR make_VkSwapchainCreateInfoKHR();
VkPresentInfoKHR make_VkPresentInfoKHR();
VkImageSwapchainCreateInfoKHR make_VkImageSwapchainCreateInfoKHR();
VkBindImageMemorySwapchainInfoKHR make_VkBindImageMemorySwapchainInfoKHR();
VkAcquireNextImageInfoKHR make_VkAcquireNextImageInfoKHR();
VkDeviceGroupPresentCapabilitiesKHR make_VkDeviceGroupPresentCapabilitiesKHR();
VkDeviceGroupPresentInfoKHR make_VkDeviceGroupPresentInfoKHR();
VkDeviceGroupSwapchainCreateInfoKHR make_VkDeviceGroupSwapchainCreateInfoKHR();
#endif

#if VK_KHR_display
VkDisplayModeCreateInfoKHR make_VkDisplayModeCreateInfoKHR();
VkDisplaySurfaceCreateInfoKHR make_VkDisplaySurfaceCreateInfoKHR();
#endif

#if VK_KHR_display_swapchain
VkDisplayPresentInfoKHR make_VkDisplayPresentInfoKHR();
#endif

#if VK_KHR_dynamic_rendering
VkRenderingAttachmentInfoKHR make_VkRenderingAttachmentInfoKHR();
VkRenderingInfoKHR make_VkRenderingInfoKHR();
VkPipelineRenderingCreateInfoKHR make_VkPipelineRenderingCreateInfoKHR();
VkPhysicalDeviceDynamicRenderingFeaturesKHR make_VkPhysicalDeviceDynamicRenderingFeaturesKHR();
VkCommandBufferInheritanceRenderingInfoKHR make_VkCommandBufferInheritanceRenderingInfoKHR();
VkRenderingFragmentShadingRateAttachmentInfoKHR make_VkRenderingFragmentShadingRateAttachmentInfoKHR();
VkRenderingFragmentDensityMapAttachmentInfoEXT make_VkRenderingFragmentDensityMapAttachmentInfoEXT();
VkAttachmentSampleCountInfoAMD make_VkAttachmentSampleCountInfoAMD();
VkMultiviewPerViewAttributesInfoNVX make_VkMultiviewPerViewAttributesInfoNVX();
#endif

#if VK_KHR_external_memory_fd
VkImportMemoryFdInfoKHR make_VkImportMemoryFdInfoKHR();
VkMemoryFdPropertiesKHR make_VkMemoryFdPropertiesKHR();
VkMemoryGetFdInfoKHR make_VkMemoryGetFdInfoKHR();
#endif

#if VK_KHR_external_semaphore_fd
VkImportSemaphoreFdInfoKHR make_VkImportSemaphoreFdInfoKHR();
VkSemaphoreGetFdInfoKHR make_VkSemaphoreGetFdInfoKHR();
#endif

#if VK_KHR_push_descriptor
VkPhysicalDevicePushDescriptorPropertiesKHR make_VkPhysicalDevicePushDescriptorPropertiesKHR();
#endif

#if VK_KHR_incremental_present
VkPresentRegionsKHR make_VkPresentRegionsKHR();
#endif

#if VK_KHR_shared_presentable_image
VkSharedPresentSurfaceCapabilitiesKHR make_VkSharedPresentSurfaceCapabilitiesKHR();
#endif

#if VK_KHR_external_fence_fd
VkImportFenceFdInfoKHR make_VkImportFenceFdInfoKHR();
VkFenceGetFdInfoKHR make_VkFenceGetFdInfoKHR();
#endif

#if VK_KHR_performance_query
VkPhysicalDevicePerformanceQueryFeaturesKHR make_VkPhysicalDevicePerformanceQueryFeaturesKHR();
VkPhysicalDevicePerformanceQueryPropertiesKHR make_VkPhysicalDevicePerformanceQueryPropertiesKHR();
VkPerformanceCounterKHR make_VkPerformanceCounterKHR();
VkPerformanceCounterDescriptionKHR make_VkPerformanceCounterDescriptionKHR();
VkQueryPoolPerformanceCreateInfoKHR make_VkQueryPoolPerformanceCreateInfoKHR();
VkAcquireProfilingLockInfoKHR make_VkAcquireProfilingLockInfoKHR();
VkPerformanceQuerySubmitInfoKHR make_VkPerformanceQuerySubmitInfoKHR();
#endif

#if VK_KHR_get_surface_capabilities2
VkPhysicalDeviceSurfaceInfo2KHR make_VkPhysicalDeviceSurfaceInfo2KHR();
VkSurfaceCapabilities2KHR make_VkSurfaceCapabilities2KHR();
VkSurfaceFormat2KHR make_VkSurfaceFormat2KHR();
#endif

#if VK_KHR_get_display_properties2
VkDisplayProperties2KHR make_VkDisplayProperties2KHR();
VkDisplayPlaneProperties2KHR make_VkDisplayPlaneProperties2KHR();
VkDisplayModeProperties2KHR make_VkDisplayModeProperties2KHR();
VkDisplayPlaneInfo2KHR make_VkDisplayPlaneInfo2KHR();
VkDisplayPlaneCapabilities2KHR make_VkDisplayPlaneCapabilities2KHR();
#endif

#if VK_KHR_shader_clock
VkPhysicalDeviceShaderClockFeaturesKHR make_VkPhysicalDeviceShaderClockFeaturesKHR();
#endif

#if VK_KHR_shader_terminate_invocation
VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR make_VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR();
#endif

#if VK_KHR_fragment_shading_rate
VkFragmentShadingRateAttachmentInfoKHR make_VkFragmentShadingRateAttachmentInfoKHR();
VkPipelineFragmentShadingRateStateCreateInfoKHR make_VkPipelineFragmentShadingRateStateCreateInfoKHR();
VkPhysicalDeviceFragmentShadingRateFeaturesKHR make_VkPhysicalDeviceFragmentShadingRateFeaturesKHR();
VkPhysicalDeviceFragmentShadingRatePropertiesKHR make_VkPhysicalDeviceFragmentShadingRatePropertiesKHR();
VkPhysicalDeviceFragmentShadingRateKHR make_VkPhysicalDeviceFragmentShadingRateKHR();
#endif

#if VK_KHR_surface_protected_capabilities
VkSurfaceProtectedCapabilitiesKHR make_VkSurfaceProtectedCapabilitiesKHR();
#endif

#if VK_KHR_present_wait
VkPhysicalDevicePresentWaitFeaturesKHR make_VkPhysicalDevicePresentWaitFeaturesKHR();
#endif

#if VK_KHR_pipeline_executable_properties
VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR make_VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR();
VkPipelineInfoKHR make_VkPipelineInfoKHR();
VkPipelineExecutablePropertiesKHR make_VkPipelineExecutablePropertiesKHR();
VkPipelineExecutableInfoKHR make_VkPipelineExecutableInfoKHR();
VkPipelineExecutableStatisticKHR make_VkPipelineExecutableStatisticKHR();
VkPipelineExecutableInternalRepresentationKHR make_VkPipelineExecutableInternalRepresentationKHR();
#endif

#if VK_KHR_shader_integer_dot_product
VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR make_VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR();
VkPhysicalDeviceShaderIntegerDotProductPropertiesKHR make_VkPhysicalDeviceShaderIntegerDotProductPropertiesKHR();
#endif

#if VK_KHR_pipeline_library
VkPipelineLibraryCreateInfoKHR make_VkPipelineLibraryCreateInfoKHR();
#endif

#if VK_KHR_present_id
VkPresentIdKHR make_VkPresentIdKHR();
VkPhysicalDevicePresentIdFeaturesKHR make_VkPhysicalDevicePresentIdFeaturesKHR();
#endif

#if VK_KHR_synchronization2
VkMemoryBarrier2KHR make_VkMemoryBarrier2KHR();
VkBufferMemoryBarrier2KHR make_VkBufferMemoryBarrier2KHR();
VkImageMemoryBarrier2KHR make_VkImageMemoryBarrier2KHR();
VkDependencyInfoKHR make_VkDependencyInfoKHR();
VkSemaphoreSubmitInfoKHR make_VkSemaphoreSubmitInfoKHR();
VkCommandBufferSubmitInfoKHR make_VkCommandBufferSubmitInfoKHR();
VkSubmitInfo2KHR make_VkSubmitInfo2KHR();
VkPhysicalDeviceSynchronization2FeaturesKHR make_VkPhysicalDeviceSynchronization2FeaturesKHR();
VkQueueFamilyCheckpointProperties2NV make_VkQueueFamilyCheckpointProperties2NV();
VkCheckpointData2NV make_VkCheckpointData2NV();
#endif

#if VK_KHR_shader_subgroup_uniform_control_flow
VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR make_VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR();
#endif

#if VK_KHR_zero_initialize_workgroup_memory
VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR make_VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR();
#endif

#if VK_KHR_workgroup_memory_explicit_layout
VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR make_VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR();
#endif

#if VK_KHR_copy_commands2
VkBufferCopy2KHR make_VkBufferCopy2KHR();
VkCopyBufferInfo2KHR make_VkCopyBufferInfo2KHR();
VkImageCopy2KHR make_VkImageCopy2KHR();
VkCopyImageInfo2KHR make_VkCopyImageInfo2KHR();
VkBufferImageCopy2KHR make_VkBufferImageCopy2KHR();
VkCopyBufferToImageInfo2KHR make_VkCopyBufferToImageInfo2KHR();
VkCopyImageToBufferInfo2KHR make_VkCopyImageToBufferInfo2KHR();
VkImageBlit2KHR make_VkImageBlit2KHR();
VkBlitImageInfo2KHR make_VkBlitImageInfo2KHR();
VkImageResolve2KHR make_VkImageResolve2KHR();
VkResolveImageInfo2KHR make_VkResolveImageInfo2KHR();
#endif

#if VK_KHR_format_feature_flags2
VkFormatProperties3KHR make_VkFormatProperties3KHR();
#endif

#if VK_KHR_maintenance4
VkPhysicalDeviceMaintenance4FeaturesKHR make_VkPhysicalDeviceMaintenance4FeaturesKHR();
VkPhysicalDeviceMaintenance4PropertiesKHR make_VkPhysicalDeviceMaintenance4PropertiesKHR();
VkDeviceBufferMemoryRequirementsKHR make_VkDeviceBufferMemoryRequirementsKHR();
VkDeviceImageMemoryRequirementsKHR make_VkDeviceImageMemoryRequirementsKHR();
#endif

#if VK_EXT_debug_report
VkDebugReportCallbackCreateInfoEXT make_VkDebugReportCallbackCreateInfoEXT();
#endif

#if VK_AMD_rasterization_order
VkPipelineRasterizationStateRasterizationOrderAMD make_VkPipelineRasterizationStateRasterizationOrderAMD();
#endif

#if VK_EXT_debug_marker
VkDebugMarkerObjectNameInfoEXT make_VkDebugMarkerObjectNameInfoEXT();
VkDebugMarkerObjectTagInfoEXT make_VkDebugMarkerObjectTagInfoEXT();
VkDebugMarkerMarkerInfoEXT make_VkDebugMarkerMarkerInfoEXT();
#endif

#if VK_NV_dedicated_allocation
VkDedicatedAllocationImageCreateInfoNV make_VkDedicatedAllocationImageCreateInfoNV();
VkDedicatedAllocationBufferCreateInfoNV make_VkDedicatedAllocationBufferCreateInfoNV();
VkDedicatedAllocationMemoryAllocateInfoNV make_VkDedicatedAllocationMemoryAllocateInfoNV();
#endif

#if VK_EXT_transform_feedback
VkPhysicalDeviceTransformFeedbackFeaturesEXT make_VkPhysicalDeviceTransformFeedbackFeaturesEXT();
VkPhysicalDeviceTransformFeedbackPropertiesEXT make_VkPhysicalDeviceTransformFeedbackPropertiesEXT();
VkPipelineRasterizationStateStreamCreateInfoEXT make_VkPipelineRasterizationStateStreamCreateInfoEXT();
#endif

#if VK_NVX_binary_import
VkCuModuleCreateInfoNVX make_VkCuModuleCreateInfoNVX();
VkCuFunctionCreateInfoNVX make_VkCuFunctionCreateInfoNVX();
VkCuLaunchInfoNVX make_VkCuLaunchInfoNVX();
#endif

#if VK_NVX_image_view_handle
VkImageViewHandleInfoNVX make_VkImageViewHandleInfoNVX();
VkImageViewAddressPropertiesNVX make_VkImageViewAddressPropertiesNVX();
#endif

#if VK_AMD_texture_gather_bias_lod
VkTextureLODGatherFormatPropertiesAMD make_VkTextureLODGatherFormatPropertiesAMD();
#endif

#if VK_NV_corner_sampled_image
VkPhysicalDeviceCornerSampledImageFeaturesNV make_VkPhysicalDeviceCornerSampledImageFeaturesNV();
#endif

#if VK_NV_external_memory
VkExternalMemoryImageCreateInfoNV make_VkExternalMemoryImageCreateInfoNV();
VkExportMemoryAllocateInfoNV make_VkExportMemoryAllocateInfoNV();
#endif

#if VK_EXT_validation_flags
VkValidationFlagsEXT make_VkValidationFlagsEXT();
#endif

#if VK_EXT_texture_compression_astc_hdr
VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT make_VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT();
#endif

#if VK_EXT_astc_decode_mode
VkImageViewASTCDecodeModeEXT make_VkImageViewASTCDecodeModeEXT();
VkPhysicalDeviceASTCDecodeFeaturesEXT make_VkPhysicalDeviceASTCDecodeFeaturesEXT();
#endif

#if VK_EXT_conditional_rendering
VkConditionalRenderingBeginInfoEXT make_VkConditionalRenderingBeginInfoEXT();
VkPhysicalDeviceConditionalRenderingFeaturesEXT make_VkPhysicalDeviceConditionalRenderingFeaturesEXT();
VkCommandBufferInheritanceConditionalRenderingInfoEXT make_VkCommandBufferInheritanceConditionalRenderingInfoEXT();
#endif

#if VK_NV_clip_space_w_scaling
VkPipelineViewportWScalingStateCreateInfoNV make_VkPipelineViewportWScalingStateCreateInfoNV();
#endif

#if VK_EXT_display_surface_counter
VkSurfaceCapabilities2EXT make_VkSurfaceCapabilities2EXT();
#endif

#if VK_EXT_display_control
VkDisplayPowerInfoEXT make_VkDisplayPowerInfoEXT();
VkDeviceEventInfoEXT make_VkDeviceEventInfoEXT();
VkDisplayEventInfoEXT make_VkDisplayEventInfoEXT();
VkSwapchainCounterCreateInfoEXT make_VkSwapchainCounterCreateInfoEXT();
#endif

#if VK_GOOGLE_display_timing
VkPresentTimesInfoGOOGLE make_VkPresentTimesInfoGOOGLE();
#endif

#if VK_NVX_multiview_per_view_attributes
VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX make_VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX();
#endif

#if VK_NV_viewport_swizzle
VkPipelineViewportSwizzleStateCreateInfoNV make_VkPipelineViewportSwizzleStateCreateInfoNV();
#endif

#if VK_EXT_discard_rectangles
VkPhysicalDeviceDiscardRectanglePropertiesEXT make_VkPhysicalDeviceDiscardRectanglePropertiesEXT();
VkPipelineDiscardRectangleStateCreateInfoEXT make_VkPipelineDiscardRectangleStateCreateInfoEXT();
#endif

#if VK_EXT_conservative_rasterization
VkPhysicalDeviceConservativeRasterizationPropertiesEXT make_VkPhysicalDeviceConservativeRasterizationPropertiesEXT();
VkPipelineRasterizationConservativeStateCreateInfoEXT make_VkPipelineRasterizationConservativeStateCreateInfoEXT();
#endif

#if VK_EXT_depth_clip_enable
VkPhysicalDeviceDepthClipEnableFeaturesEXT make_VkPhysicalDeviceDepthClipEnableFeaturesEXT();
VkPipelineRasterizationDepthClipStateCreateInfoEXT make_VkPipelineRasterizationDepthClipStateCreateInfoEXT();
#endif

#if VK_EXT_hdr_metadata
VkHdrMetadataEXT make_VkHdrMetadataEXT();
#endif

#if VK_EXT_debug_utils
VkDebugUtilsLabelEXT make_VkDebugUtilsLabelEXT();
VkDebugUtilsObjectNameInfoEXT make_VkDebugUtilsObjectNameInfoEXT();
VkDebugUtilsMessengerCallbackDataEXT make_VkDebugUtilsMessengerCallbackDataEXT();
VkDebugUtilsMessengerCreateInfoEXT make_VkDebugUtilsMessengerCreateInfoEXT();
VkDebugUtilsObjectTagInfoEXT make_VkDebugUtilsObjectTagInfoEXT();
#endif

#if VK_EXT_inline_uniform_block
VkPhysicalDeviceInlineUniformBlockFeaturesEXT make_VkPhysicalDeviceInlineUniformBlockFeaturesEXT();
VkPhysicalDeviceInlineUniformBlockPropertiesEXT make_VkPhysicalDeviceInlineUniformBlockPropertiesEXT();
VkWriteDescriptorSetInlineUniformBlockEXT make_VkWriteDescriptorSetInlineUniformBlockEXT();
VkDescriptorPoolInlineUniformBlockCreateInfoEXT make_VkDescriptorPoolInlineUniformBlockCreateInfoEXT();
#endif

#if VK_EXT_sample_locations
VkSampleLocationsInfoEXT make_VkSampleLocationsInfoEXT();
VkRenderPassSampleLocationsBeginInfoEXT make_VkRenderPassSampleLocationsBeginInfoEXT();
VkPipelineSampleLocationsStateCreateInfoEXT make_VkPipelineSampleLocationsStateCreateInfoEXT();
VkPhysicalDeviceSampleLocationsPropertiesEXT make_VkPhysicalDeviceSampleLocationsPropertiesEXT();
VkMultisamplePropertiesEXT make_VkMultisamplePropertiesEXT();
#endif

#if VK_EXT_blend_operation_advanced
VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT make_VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT();
VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT make_VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT();
VkPipelineColorBlendAdvancedStateCreateInfoEXT make_VkPipelineColorBlendAdvancedStateCreateInfoEXT();
#endif

#if VK_NV_fragment_coverage_to_color
VkPipelineCoverageToColorStateCreateInfoNV make_VkPipelineCoverageToColorStateCreateInfoNV();
#endif

#if VK_NV_framebuffer_mixed_samples
VkPipelineCoverageModulationStateCreateInfoNV make_VkPipelineCoverageModulationStateCreateInfoNV();
#endif

#if VK_NV_shader_sm_builtins
VkPhysicalDeviceShaderSMBuiltinsPropertiesNV make_VkPhysicalDeviceShaderSMBuiltinsPropertiesNV();
VkPhysicalDeviceShaderSMBuiltinsFeaturesNV make_VkPhysicalDeviceShaderSMBuiltinsFeaturesNV();
#endif

#if VK_EXT_image_drm_format_modifier
VkDrmFormatModifierPropertiesListEXT make_VkDrmFormatModifierPropertiesListEXT();
VkPhysicalDeviceImageDrmFormatModifierInfoEXT make_VkPhysicalDeviceImageDrmFormatModifierInfoEXT();
VkImageDrmFormatModifierListCreateInfoEXT make_VkImageDrmFormatModifierListCreateInfoEXT();
VkImageDrmFormatModifierExplicitCreateInfoEXT make_VkImageDrmFormatModifierExplicitCreateInfoEXT();
VkImageDrmFormatModifierPropertiesEXT make_VkImageDrmFormatModifierPropertiesEXT();
VkDrmFormatModifierPropertiesList2EXT make_VkDrmFormatModifierPropertiesList2EXT();
#endif

#if VK_EXT_validation_cache
VkValidationCacheCreateInfoEXT make_VkValidationCacheCreateInfoEXT();
VkShaderModuleValidationCacheCreateInfoEXT make_VkShaderModuleValidationCacheCreateInfoEXT();
#endif

#if VK_NV_shading_rate_image
VkPipelineViewportShadingRateImageStateCreateInfoNV make_VkPipelineViewportShadingRateImageStateCreateInfoNV();
VkPhysicalDeviceShadingRateImageFeaturesNV make_VkPhysicalDeviceShadingRateImageFeaturesNV();
VkPhysicalDeviceShadingRateImagePropertiesNV make_VkPhysicalDeviceShadingRateImagePropertiesNV();
VkPipelineViewportCoarseSampleOrderStateCreateInfoNV make_VkPipelineViewportCoarseSampleOrderStateCreateInfoNV();
#endif

#if VK_NV_ray_tracing
VkRayTracingShaderGroupCreateInfoNV make_VkRayTracingShaderGroupCreateInfoNV();
VkRayTracingPipelineCreateInfoNV make_VkRayTracingPipelineCreateInfoNV();
VkGeometryTrianglesNV make_VkGeometryTrianglesNV();
VkGeometryAABBNV make_VkGeometryAABBNV();
VkGeometryNV make_VkGeometryNV();
VkAccelerationStructureInfoNV make_VkAccelerationStructureInfoNV();
VkAccelerationStructureCreateInfoNV make_VkAccelerationStructureCreateInfoNV();
VkBindAccelerationStructureMemoryInfoNV make_VkBindAccelerationStructureMemoryInfoNV();
VkWriteDescriptorSetAccelerationStructureNV make_VkWriteDescriptorSetAccelerationStructureNV();
VkAccelerationStructureMemoryRequirementsInfoNV make_VkAccelerationStructureMemoryRequirementsInfoNV();
VkPhysicalDeviceRayTracingPropertiesNV make_VkPhysicalDeviceRayTracingPropertiesNV();
#endif

#if VK_NV_representative_fragment_test
VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV make_VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV();
VkPipelineRepresentativeFragmentTestStateCreateInfoNV make_VkPipelineRepresentativeFragmentTestStateCreateInfoNV();
#endif

#if VK_EXT_filter_cubic
VkPhysicalDeviceImageViewImageFormatInfoEXT make_VkPhysicalDeviceImageViewImageFormatInfoEXT();
VkFilterCubicImageViewImageFormatPropertiesEXT make_VkFilterCubicImageViewImageFormatPropertiesEXT();
#endif

#if VK_EXT_global_priority
VkDeviceQueueGlobalPriorityCreateInfoEXT make_VkDeviceQueueGlobalPriorityCreateInfoEXT();
#endif

#if VK_EXT_external_memory_host
VkImportMemoryHostPointerInfoEXT make_VkImportMemoryHostPointerInfoEXT();
VkMemoryHostPointerPropertiesEXT make_VkMemoryHostPointerPropertiesEXT();
VkPhysicalDeviceExternalMemoryHostPropertiesEXT make_VkPhysicalDeviceExternalMemoryHostPropertiesEXT();
#endif

#if VK_AMD_pipeline_compiler_control
VkPipelineCompilerControlCreateInfoAMD make_VkPipelineCompilerControlCreateInfoAMD();
#endif

#if VK_EXT_calibrated_timestamps
VkCalibratedTimestampInfoEXT make_VkCalibratedTimestampInfoEXT();
#endif

#if VK_AMD_shader_core_properties
VkPhysicalDeviceShaderCorePropertiesAMD make_VkPhysicalDeviceShaderCorePropertiesAMD();
#endif

#if VK_AMD_memory_overallocation_behavior
VkDeviceMemoryOverallocationCreateInfoAMD make_VkDeviceMemoryOverallocationCreateInfoAMD();
#endif

#if VK_EXT_vertex_attribute_divisor
VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT make_VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT();
VkPipelineVertexInputDivisorStateCreateInfoEXT make_VkPipelineVertexInputDivisorStateCreateInfoEXT();
VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT make_VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT();
#endif

#if VK_EXT_pipeline_creation_feedback
VkPipelineCreationFeedbackCreateInfoEXT make_VkPipelineCreationFeedbackCreateInfoEXT();
#endif

#if VK_NV_compute_shader_derivatives
VkPhysicalDeviceComputeShaderDerivativesFeaturesNV make_VkPhysicalDeviceComputeShaderDerivativesFeaturesNV();
#endif

#if VK_NV_mesh_shader
VkPhysicalDeviceMeshShaderFeaturesNV make_VkPhysicalDeviceMeshShaderFeaturesNV();
VkPhysicalDeviceMeshShaderPropertiesNV make_VkPhysicalDeviceMeshShaderPropertiesNV();
#endif

#if VK_NV_fragment_shader_barycentric
VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV make_VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV();
#endif

#if VK_NV_shader_image_footprint
VkPhysicalDeviceShaderImageFootprintFeaturesNV make_VkPhysicalDeviceShaderImageFootprintFeaturesNV();
#endif

#if VK_NV_scissor_exclusive
VkPipelineViewportExclusiveScissorStateCreateInfoNV make_VkPipelineViewportExclusiveScissorStateCreateInfoNV();
VkPhysicalDeviceExclusiveScissorFeaturesNV make_VkPhysicalDeviceExclusiveScissorFeaturesNV();
#endif

#if VK_NV_device_diagnostic_checkpoints
VkQueueFamilyCheckpointPropertiesNV make_VkQueueFamilyCheckpointPropertiesNV();
VkCheckpointDataNV make_VkCheckpointDataNV();
#endif

#if VK_INTEL_shader_integer_functions2
VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL make_VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL();
#endif

#if VK_INTEL_performance_query
VkInitializePerformanceApiInfoINTEL make_VkInitializePerformanceApiInfoINTEL();
VkQueryPoolPerformanceQueryCreateInfoINTEL make_VkQueryPoolPerformanceQueryCreateInfoINTEL();
VkPerformanceMarkerInfoINTEL make_VkPerformanceMarkerInfoINTEL();
VkPerformanceStreamMarkerInfoINTEL make_VkPerformanceStreamMarkerInfoINTEL();
VkPerformanceOverrideInfoINTEL make_VkPerformanceOverrideInfoINTEL();
VkPerformanceConfigurationAcquireInfoINTEL make_VkPerformanceConfigurationAcquireInfoINTEL();
#endif

#if VK_EXT_pci_bus_info
VkPhysicalDevicePCIBusInfoPropertiesEXT make_VkPhysicalDevicePCIBusInfoPropertiesEXT();
#endif

#if VK_AMD_display_native_hdr
VkDisplayNativeHdrSurfaceCapabilitiesAMD make_VkDisplayNativeHdrSurfaceCapabilitiesAMD();
VkSwapchainDisplayNativeHdrCreateInfoAMD make_VkSwapchainDisplayNativeHdrCreateInfoAMD();
#endif

#if VK_EXT_fragment_density_map
VkPhysicalDeviceFragmentDensityMapFeaturesEXT make_VkPhysicalDeviceFragmentDensityMapFeaturesEXT();
VkPhysicalDeviceFragmentDensityMapPropertiesEXT make_VkPhysicalDeviceFragmentDensityMapPropertiesEXT();
VkRenderPassFragmentDensityMapCreateInfoEXT make_VkRenderPassFragmentDensityMapCreateInfoEXT();
#endif

#if VK_EXT_subgroup_size_control
VkPhysicalDeviceSubgroupSizeControlFeaturesEXT make_VkPhysicalDeviceSubgroupSizeControlFeaturesEXT();
VkPhysicalDeviceSubgroupSizeControlPropertiesEXT make_VkPhysicalDeviceSubgroupSizeControlPropertiesEXT();
VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT make_VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT();
#endif

#if VK_AMD_shader_core_properties2
VkPhysicalDeviceShaderCoreProperties2AMD make_VkPhysicalDeviceShaderCoreProperties2AMD();
#endif

#if VK_AMD_device_coherent_memory
VkPhysicalDeviceCoherentMemoryFeaturesAMD make_VkPhysicalDeviceCoherentMemoryFeaturesAMD();
#endif

#if VK_EXT_shader_image_atomic_int64
VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT make_VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT();
#endif

#if VK_EXT_memory_budget
VkPhysicalDeviceMemoryBudgetPropertiesEXT make_VkPhysicalDeviceMemoryBudgetPropertiesEXT();
#endif

#if VK_EXT_memory_priority
VkPhysicalDeviceMemoryPriorityFeaturesEXT make_VkPhysicalDeviceMemoryPriorityFeaturesEXT();
VkMemoryPriorityAllocateInfoEXT make_VkMemoryPriorityAllocateInfoEXT();
#endif

#if VK_NV_dedicated_allocation_image_aliasing
VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV make_VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV();
#endif

#if VK_EXT_buffer_device_address
VkPhysicalDeviceBufferDeviceAddressFeaturesEXT make_VkPhysicalDeviceBufferDeviceAddressFeaturesEXT();
VkBufferDeviceAddressCreateInfoEXT make_VkBufferDeviceAddressCreateInfoEXT();
#endif

#if VK_EXT_tooling_info
VkPhysicalDeviceToolPropertiesEXT make_VkPhysicalDeviceToolPropertiesEXT();
#endif

#if VK_EXT_validation_features
VkValidationFeaturesEXT make_VkValidationFeaturesEXT();
#endif

#if VK_NV_cooperative_matrix
VkCooperativeMatrixPropertiesNV make_VkCooperativeMatrixPropertiesNV();
VkPhysicalDeviceCooperativeMatrixFeaturesNV make_VkPhysicalDeviceCooperativeMatrixFeaturesNV();
VkPhysicalDeviceCooperativeMatrixPropertiesNV make_VkPhysicalDeviceCooperativeMatrixPropertiesNV();
#endif

#if VK_NV_coverage_reduction_mode
VkPhysicalDeviceCoverageReductionModeFeaturesNV make_VkPhysicalDeviceCoverageReductionModeFeaturesNV();
VkPipelineCoverageReductionStateCreateInfoNV make_VkPipelineCoverageReductionStateCreateInfoNV();
VkFramebufferMixedSamplesCombinationNV make_VkFramebufferMixedSamplesCombinationNV();
#endif

#if VK_EXT_fragment_shader_interlock
VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT make_VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT();
#endif

#if VK_EXT_ycbcr_image_arrays
VkPhysicalDeviceYcbcrImageArraysFeaturesEXT make_VkPhysicalDeviceYcbcrImageArraysFeaturesEXT();
#endif

#if VK_EXT_provoking_vertex
VkPhysicalDeviceProvokingVertexFeaturesEXT make_VkPhysicalDeviceProvokingVertexFeaturesEXT();
VkPhysicalDeviceProvokingVertexPropertiesEXT make_VkPhysicalDeviceProvokingVertexPropertiesEXT();
VkPipelineRasterizationProvokingVertexStateCreateInfoEXT make_VkPipelineRasterizationProvokingVertexStateCreateInfoEXT();
#endif

#if VK_EXT_headless_surface
VkHeadlessSurfaceCreateInfoEXT make_VkHeadlessSurfaceCreateInfoEXT();
#endif

#if VK_EXT_line_rasterization
VkPhysicalDeviceLineRasterizationFeaturesEXT make_VkPhysicalDeviceLineRasterizationFeaturesEXT();
VkPhysicalDeviceLineRasterizationPropertiesEXT make_VkPhysicalDeviceLineRasterizationPropertiesEXT();
VkPipelineRasterizationLineStateCreateInfoEXT make_VkPipelineRasterizationLineStateCreateInfoEXT();
#endif

#if VK_EXT_shader_atomic_float
VkPhysicalDeviceShaderAtomicFloatFeaturesEXT make_VkPhysicalDeviceShaderAtomicFloatFeaturesEXT();
#endif

#if VK_EXT_index_type_uint8
VkPhysicalDeviceIndexTypeUint8FeaturesEXT make_VkPhysicalDeviceIndexTypeUint8FeaturesEXT();
#endif

#if VK_EXT_extended_dynamic_state
VkPhysicalDeviceExtendedDynamicStateFeaturesEXT make_VkPhysicalDeviceExtendedDynamicStateFeaturesEXT();
#endif

#if VK_EXT_shader_atomic_float2
VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT make_VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT();
#endif

#if VK_EXT_shader_demote_to_helper_invocation
VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT make_VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT();
#endif

#if VK_NV_device_generated_commands
VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV make_VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV();
VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV make_VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV();
VkGraphicsShaderGroupCreateInfoNV make_VkGraphicsShaderGroupCreateInfoNV();
VkGraphicsPipelineShaderGroupsCreateInfoNV make_VkGraphicsPipelineShaderGroupsCreateInfoNV();
VkIndirectCommandsLayoutTokenNV make_VkIndirectCommandsLayoutTokenNV();
VkIndirectCommandsLayoutCreateInfoNV make_VkIndirectCommandsLayoutCreateInfoNV();
VkGeneratedCommandsInfoNV make_VkGeneratedCommandsInfoNV();
VkGeneratedCommandsMemoryRequirementsInfoNV make_VkGeneratedCommandsMemoryRequirementsInfoNV();
#endif

#if VK_NV_inherited_viewport_scissor
VkPhysicalDeviceInheritedViewportScissorFeaturesNV make_VkPhysicalDeviceInheritedViewportScissorFeaturesNV();
VkCommandBufferInheritanceViewportScissorInfoNV make_VkCommandBufferInheritanceViewportScissorInfoNV();
#endif

#if VK_EXT_texel_buffer_alignment
VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT make_VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT();
VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT make_VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT();
#endif

#if VK_QCOM_render_pass_transform
VkRenderPassTransformBeginInfoQCOM make_VkRenderPassTransformBeginInfoQCOM();
VkCommandBufferInheritanceRenderPassTransformInfoQCOM make_VkCommandBufferInheritanceRenderPassTransformInfoQCOM();
#endif

#if VK_EXT_device_memory_report
VkPhysicalDeviceDeviceMemoryReportFeaturesEXT make_VkPhysicalDeviceDeviceMemoryReportFeaturesEXT();
VkDeviceMemoryReportCallbackDataEXT make_VkDeviceMemoryReportCallbackDataEXT();
VkDeviceDeviceMemoryReportCreateInfoEXT make_VkDeviceDeviceMemoryReportCreateInfoEXT();
#endif

#if VK_EXT_robustness2
VkPhysicalDeviceRobustness2FeaturesEXT make_VkPhysicalDeviceRobustness2FeaturesEXT();
VkPhysicalDeviceRobustness2PropertiesEXT make_VkPhysicalDeviceRobustness2PropertiesEXT();
#endif

#if VK_EXT_custom_border_color
VkSamplerCustomBorderColorCreateInfoEXT make_VkSamplerCustomBorderColorCreateInfoEXT();
VkPhysicalDeviceCustomBorderColorPropertiesEXT make_VkPhysicalDeviceCustomBorderColorPropertiesEXT();
VkPhysicalDeviceCustomBorderColorFeaturesEXT make_VkPhysicalDeviceCustomBorderColorFeaturesEXT();
#endif

#if VK_EXT_private_data
VkPhysicalDevicePrivateDataFeaturesEXT make_VkPhysicalDevicePrivateDataFeaturesEXT();
VkDevicePrivateDataCreateInfoEXT make_VkDevicePrivateDataCreateInfoEXT();
VkPrivateDataSlotCreateInfoEXT make_VkPrivateDataSlotCreateInfoEXT();
#endif

#if VK_EXT_pipeline_creation_cache_control
VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT make_VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT();
#endif

#if VK_NV_device_diagnostics_config
VkPhysicalDeviceDiagnosticsConfigFeaturesNV make_VkPhysicalDeviceDiagnosticsConfigFeaturesNV();
VkDeviceDiagnosticsConfigCreateInfoNV make_VkDeviceDiagnosticsConfigCreateInfoNV();
#endif

#if VK_NV_fragment_shading_rate_enums
VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV make_VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV();
VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV make_VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV();
VkPipelineFragmentShadingRateEnumStateCreateInfoNV make_VkPipelineFragmentShadingRateEnumStateCreateInfoNV();
#endif

#if VK_NV_ray_tracing_motion_blur
VkAccelerationStructureGeometryMotionTrianglesDataNV make_VkAccelerationStructureGeometryMotionTrianglesDataNV();
VkAccelerationStructureMotionInfoNV make_VkAccelerationStructureMotionInfoNV();
VkPhysicalDeviceRayTracingMotionBlurFeaturesNV make_VkPhysicalDeviceRayTracingMotionBlurFeaturesNV();
#endif

#if VK_EXT_ycbcr_2plane_444_formats
VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT make_VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT();
#endif

#if VK_EXT_fragment_density_map2
VkPhysicalDeviceFragmentDensityMap2FeaturesEXT make_VkPhysicalDeviceFragmentDensityMap2FeaturesEXT();
VkPhysicalDeviceFragmentDensityMap2PropertiesEXT make_VkPhysicalDeviceFragmentDensityMap2PropertiesEXT();
#endif

#if VK_QCOM_rotated_copy_commands
VkCopyCommandTransformInfoQCOM make_VkCopyCommandTransformInfoQCOM();
#endif

#if VK_EXT_image_robustness
VkPhysicalDeviceImageRobustnessFeaturesEXT make_VkPhysicalDeviceImageRobustnessFeaturesEXT();
#endif

#if VK_EXT_4444_formats
VkPhysicalDevice4444FormatsFeaturesEXT make_VkPhysicalDevice4444FormatsFeaturesEXT();
#endif

#if VK_EXT_rgba10x6_formats
VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT make_VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT();
#endif

#if VK_VALVE_mutable_descriptor_type
VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE make_VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE();
VkMutableDescriptorTypeCreateInfoVALVE make_VkMutableDescriptorTypeCreateInfoVALVE();
#endif

#if VK_EXT_vertex_input_dynamic_state
VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT make_VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT();
VkVertexInputBindingDescription2EXT make_VkVertexInputBindingDescription2EXT();
VkVertexInputAttributeDescription2EXT make_VkVertexInputAttributeDescription2EXT();
#endif

#if VK_EXT_physical_device_drm
VkPhysicalDeviceDrmPropertiesEXT make_VkPhysicalDeviceDrmPropertiesEXT();
#endif

#if VK_EXT_primitive_topology_list_restart
VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT make_VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT();
#endif

#if VK_HUAWEI_subpass_shading
VkSubpassShadingPipelineCreateInfoHUAWEI make_VkSubpassShadingPipelineCreateInfoHUAWEI();
VkPhysicalDeviceSubpassShadingFeaturesHUAWEI make_VkPhysicalDeviceSubpassShadingFeaturesHUAWEI();
VkPhysicalDeviceSubpassShadingPropertiesHUAWEI make_VkPhysicalDeviceSubpassShadingPropertiesHUAWEI();
#endif

#if VK_HUAWEI_invocation_mask
VkPhysicalDeviceInvocationMaskFeaturesHUAWEI make_VkPhysicalDeviceInvocationMaskFeaturesHUAWEI();
#endif

#if VK_NV_external_memory_rdma
VkMemoryGetRemoteAddressInfoNV make_VkMemoryGetRemoteAddressInfoNV();
VkPhysicalDeviceExternalMemoryRDMAFeaturesNV make_VkPhysicalDeviceExternalMemoryRDMAFeaturesNV();
#endif

#if VK_EXT_extended_dynamic_state2
VkPhysicalDeviceExtendedDynamicState2FeaturesEXT make_VkPhysicalDeviceExtendedDynamicState2FeaturesEXT();
#endif

#if VK_EXT_color_write_enable
VkPhysicalDeviceColorWriteEnableFeaturesEXT make_VkPhysicalDeviceColorWriteEnableFeaturesEXT();
VkPipelineColorWriteCreateInfoEXT make_VkPipelineColorWriteCreateInfoEXT();
#endif

#if VK_EXT_global_priority_query
VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT make_VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT();
VkQueueFamilyGlobalPriorityPropertiesEXT make_VkQueueFamilyGlobalPriorityPropertiesEXT();
#endif

#if VK_EXT_multi_draw
VkPhysicalDeviceMultiDrawFeaturesEXT make_VkPhysicalDeviceMultiDrawFeaturesEXT();
VkPhysicalDeviceMultiDrawPropertiesEXT make_VkPhysicalDeviceMultiDrawPropertiesEXT();
#endif

#if VK_EXT_border_color_swizzle
VkPhysicalDeviceBorderColorSwizzleFeaturesEXT make_VkPhysicalDeviceBorderColorSwizzleFeaturesEXT();
VkSamplerBorderColorComponentMappingCreateInfoEXT make_VkSamplerBorderColorComponentMappingCreateInfoEXT();
#endif

#if VK_EXT_pageable_device_local_memory
VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT make_VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT();
#endif

#if VK_KHR_acceleration_structure
VkAccelerationStructureGeometryTrianglesDataKHR make_VkAccelerationStructureGeometryTrianglesDataKHR();
VkAccelerationStructureGeometryAabbsDataKHR make_VkAccelerationStructureGeometryAabbsDataKHR();
VkAccelerationStructureGeometryInstancesDataKHR make_VkAccelerationStructureGeometryInstancesDataKHR();
VkAccelerationStructureGeometryKHR make_VkAccelerationStructureGeometryKHR();
VkAccelerationStructureBuildGeometryInfoKHR make_VkAccelerationStructureBuildGeometryInfoKHR();
VkAccelerationStructureCreateInfoKHR make_VkAccelerationStructureCreateInfoKHR();
VkWriteDescriptorSetAccelerationStructureKHR make_VkWriteDescriptorSetAccelerationStructureKHR();
VkPhysicalDeviceAccelerationStructureFeaturesKHR make_VkPhysicalDeviceAccelerationStructureFeaturesKHR();
VkPhysicalDeviceAccelerationStructurePropertiesKHR make_VkPhysicalDeviceAccelerationStructurePropertiesKHR();
VkAccelerationStructureDeviceAddressInfoKHR make_VkAccelerationStructureDeviceAddressInfoKHR();
VkAccelerationStructureVersionInfoKHR make_VkAccelerationStructureVersionInfoKHR();
VkCopyAccelerationStructureToMemoryInfoKHR make_VkCopyAccelerationStructureToMemoryInfoKHR();
VkCopyMemoryToAccelerationStructureInfoKHR make_VkCopyMemoryToAccelerationStructureInfoKHR();
VkCopyAccelerationStructureInfoKHR make_VkCopyAccelerationStructureInfoKHR();
VkAccelerationStructureBuildSizesInfoKHR make_VkAccelerationStructureBuildSizesInfoKHR();
#endif

#if VK_KHR_ray_tracing_pipeline
VkRayTracingShaderGroupCreateInfoKHR make_VkRayTracingShaderGroupCreateInfoKHR();
VkRayTracingPipelineInterfaceCreateInfoKHR make_VkRayTracingPipelineInterfaceCreateInfoKHR();
VkRayTracingPipelineCreateInfoKHR make_VkRayTracingPipelineCreateInfoKHR();
VkPhysicalDeviceRayTracingPipelineFeaturesKHR make_VkPhysicalDeviceRayTracingPipelineFeaturesKHR();
VkPhysicalDeviceRayTracingPipelinePropertiesKHR make_VkPhysicalDeviceRayTracingPipelinePropertiesKHR();
#endif

#if VK_KHR_ray_query
VkPhysicalDeviceRayQueryFeaturesKHR make_VkPhysicalDeviceRayQueryFeaturesKHR();
#endif



#if VK_EXT_debug_utils
#if VK_VERSION_1_0
VkObjectType get_vk_object_type(VkBuffer a_vk_handle);
VkObjectType get_vk_object_type(VkImage a_vk_handle);
VkObjectType get_vk_object_type(VkInstance a_vk_handle);
VkObjectType get_vk_object_type(VkPhysicalDevice a_vk_handle);
VkObjectType get_vk_object_type(VkDevice a_vk_handle);
VkObjectType get_vk_object_type(VkQueue a_vk_handle);
VkObjectType get_vk_object_type(VkSemaphore a_vk_handle);
VkObjectType get_vk_object_type(VkCommandBuffer a_vk_handle);
VkObjectType get_vk_object_type(VkFence a_vk_handle);
VkObjectType get_vk_object_type(VkDeviceMemory a_vk_handle);
VkObjectType get_vk_object_type(VkEvent a_vk_handle);
VkObjectType get_vk_object_type(VkQueryPool a_vk_handle);
VkObjectType get_vk_object_type(VkBufferView a_vk_handle);
VkObjectType get_vk_object_type(VkImageView a_vk_handle);
VkObjectType get_vk_object_type(VkShaderModule a_vk_handle);
VkObjectType get_vk_object_type(VkPipelineCache a_vk_handle);
VkObjectType get_vk_object_type(VkPipelineLayout a_vk_handle);
VkObjectType get_vk_object_type(VkPipeline a_vk_handle);
VkObjectType get_vk_object_type(VkRenderPass a_vk_handle);
VkObjectType get_vk_object_type(VkDescriptorSetLayout a_vk_handle);
VkObjectType get_vk_object_type(VkSampler a_vk_handle);
VkObjectType get_vk_object_type(VkDescriptorSet a_vk_handle);
VkObjectType get_vk_object_type(VkDescriptorPool a_vk_handle);
VkObjectType get_vk_object_type(VkFramebuffer a_vk_handle);
VkObjectType get_vk_object_type(VkCommandPool a_vk_handle);
#endif

#if VK_VERSION_1_1
VkObjectType get_vk_object_type(VkSamplerYcbcrConversion a_vk_handle);
VkObjectType get_vk_object_type(VkDescriptorUpdateTemplate a_vk_handle);
#endif

#if VK_KHR_surface
VkObjectType get_vk_object_type(VkSurfaceKHR a_vk_handle);
#endif

#if VK_KHR_swapchain
VkObjectType get_vk_object_type(VkSwapchainKHR a_vk_handle);
#endif

#if VK_KHR_display
VkObjectType get_vk_object_type(VkDisplayKHR a_vk_handle);
VkObjectType get_vk_object_type(VkDisplayModeKHR a_vk_handle);
#endif

#if VK_KHR_deferred_host_operations
VkObjectType get_vk_object_type(VkDeferredOperationKHR a_vk_handle);
#endif

#if VK_EXT_debug_report
VkObjectType get_vk_object_type(VkDebugReportCallbackEXT a_vk_handle);
#endif

#if VK_NVX_binary_import
VkObjectType get_vk_object_type(VkCuModuleNVX a_vk_handle);
VkObjectType get_vk_object_type(VkCuFunctionNVX a_vk_handle);
#endif

#if VK_EXT_debug_utils
VkObjectType get_vk_object_type(VkDebugUtilsMessengerEXT a_vk_handle);
#endif

#if VK_EXT_validation_cache
VkObjectType get_vk_object_type(VkValidationCacheEXT a_vk_handle);
#endif

#if VK_NV_ray_tracing
VkObjectType get_vk_object_type(VkAccelerationStructureNV a_vk_handle);
#endif

#if VK_INTEL_performance_query
VkObjectType get_vk_object_type(VkPerformanceConfigurationINTEL a_vk_handle);
#endif

#if VK_NV_device_generated_commands
VkObjectType get_vk_object_type(VkIndirectCommandsLayoutNV a_vk_handle);
#endif

#if VK_EXT_private_data
VkObjectType get_vk_object_type(VkPrivateDataSlotEXT a_vk_handle);
#endif

#if VK_KHR_acceleration_structure
VkObjectType get_vk_object_type(VkAccelerationStructureKHR a_vk_handle);
#endif

#endif // VK_EXT_debug_utils

} // namespace spock

#if defined(SPOCK_GENERATED_IMPLEMENTATION)

namespace {
} // anonymous namespace

namespace spock{

std::vector<physical_dev_feature2_wrapper_t> physical_dev_all_features2_t::
link
(
    const std::map<std::string_view, bool>& a_feature_requests,
    const std::unordered_set<std::string>& a_available_device_extensions,
    bool& a_ok,
    std::string& a_error_msg
)
{
    std::vector<physical_dev_feature2_wrapper_t> feature_wrappers;
    a_ok = false;
    feature_wrappers.reserve(a_feature_requests.size());
    for (const auto& request : a_feature_requests) {
        const std::string_view feature_name_sv{ request.first };
        const bool is_feature_required = request.second;
        bool extension_name_found = false;
        const auto extension_name = get_extension_name(feature_name_sv, &extension_name_found);
        if (extension_name_found) {
            auto feature_wrapper = wrap_feature(feature_name_sv);
            bool ext_available = feature_wrapper.versioned || a_available_device_extensions.find(extension_name.data()) != a_available_device_extensions.end();
            if (is_feature_required && !ext_available) {
                a_error_msg = std::string(feature_name_sv) + " is not available";
                return feature_wrappers;
            }
            else {
                a_ok = true;
                feature_wrapper.required = is_feature_required;
                feature_wrapper.available = ext_available;
                if (ext_available) {
                    feature_wrappers.push_back(feature_wrapper);
                }
                else {
                    a_error_msg = std::string(feature_name_sv) + " is not available";
                }
            }
        }
        else {
            const std::string header_version_str = std::to_string(VK_API_VERSION_MAJOR(VK_HEADER_VERSION_COMPLETE)) + '.' + std::to_string(VK_API_VERSION_MINOR(VK_HEADER_VERSION_COMPLETE)) + '.' + std::to_string(VK_API_VERSION_PATCH(VK_HEADER_VERSION_COMPLETE));
            a_error_msg = std::string(feature_name_sv) + " is not available in Vulkan " + header_version_str;
            if (!is_feature_required) {
                a_ok = true;
            }
        }
    }
    const size_t num_enabled_features = feature_wrappers.size();
    if (num_enabled_features == 0) {
        return feature_wrappers;
    }
    for (size_t i = 0; i < num_enabled_features - 1; ++i) {
        *feature_wrappers[i].feature_pNext_ptr = feature_wrappers[i + 1].feature_ptr;
    }
    *feature_wrappers.back().feature_pNext_ptr = nullptr;

    return feature_wrappers;
}

physical_dev_feature2_wrapper_t physical_dev_all_features2_t::wrap_feature(std::string_view a_feature_name, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
#if VK_VERSION_1_1
    if (a_feature_name == "VkPhysicalDevice16BitStorageFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevice16BitStorageFeatures, &PhysicalDevice16BitStorageFeatures.pNext, "VkPhysicalDevice16BitStorageFeatures", "VK_VERSION_1_1"};
    if (a_feature_name == "VkPhysicalDeviceFeatures2"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFeatures2, &PhysicalDeviceFeatures2.pNext, "VkPhysicalDeviceFeatures2", "VK_VERSION_1_1"};
    if (a_feature_name == "VkPhysicalDeviceMultiviewFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceMultiviewFeatures, &PhysicalDeviceMultiviewFeatures.pNext, "VkPhysicalDeviceMultiviewFeatures", "VK_VERSION_1_1"};
    if (a_feature_name == "VkPhysicalDeviceVariablePointersFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceVariablePointersFeatures, &PhysicalDeviceVariablePointersFeatures.pNext, "VkPhysicalDeviceVariablePointersFeatures", "VK_VERSION_1_1"};
    if (a_feature_name == "VkPhysicalDeviceProtectedMemoryFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceProtectedMemoryFeatures, &PhysicalDeviceProtectedMemoryFeatures.pNext, "VkPhysicalDeviceProtectedMemoryFeatures", "VK_VERSION_1_1"};
    if (a_feature_name == "VkPhysicalDeviceSamplerYcbcrConversionFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceSamplerYcbcrConversionFeatures, &PhysicalDeviceSamplerYcbcrConversionFeatures.pNext, "VkPhysicalDeviceSamplerYcbcrConversionFeatures", "VK_VERSION_1_1"};
    if (a_feature_name == "VkPhysicalDeviceShaderDrawParametersFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderDrawParametersFeatures, &PhysicalDeviceShaderDrawParametersFeatures.pNext, "VkPhysicalDeviceShaderDrawParametersFeatures", "VK_VERSION_1_1"};
#endif

#if VK_VERSION_1_2
    if (a_feature_name == "VkPhysicalDeviceVulkan11Features"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceVulkan11Features, &PhysicalDeviceVulkan11Features.pNext, "VkPhysicalDeviceVulkan11Features", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceVulkan12Features"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceVulkan12Features, &PhysicalDeviceVulkan12Features.pNext, "VkPhysicalDeviceVulkan12Features", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDevice8BitStorageFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevice8BitStorageFeatures, &PhysicalDevice8BitStorageFeatures.pNext, "VkPhysicalDevice8BitStorageFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceShaderAtomicInt64Features"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderAtomicInt64Features, &PhysicalDeviceShaderAtomicInt64Features.pNext, "VkPhysicalDeviceShaderAtomicInt64Features", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceShaderFloat16Int8Features"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderFloat16Int8Features, &PhysicalDeviceShaderFloat16Int8Features.pNext, "VkPhysicalDeviceShaderFloat16Int8Features", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceDescriptorIndexingFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDescriptorIndexingFeatures, &PhysicalDeviceDescriptorIndexingFeatures.pNext, "VkPhysicalDeviceDescriptorIndexingFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceScalarBlockLayoutFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceScalarBlockLayoutFeatures, &PhysicalDeviceScalarBlockLayoutFeatures.pNext, "VkPhysicalDeviceScalarBlockLayoutFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceVulkanMemoryModelFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceVulkanMemoryModelFeatures, &PhysicalDeviceVulkanMemoryModelFeatures.pNext, "VkPhysicalDeviceVulkanMemoryModelFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceImagelessFramebufferFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceImagelessFramebufferFeatures, &PhysicalDeviceImagelessFramebufferFeatures.pNext, "VkPhysicalDeviceImagelessFramebufferFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceUniformBufferStandardLayoutFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceUniformBufferStandardLayoutFeatures, &PhysicalDeviceUniformBufferStandardLayoutFeatures.pNext, "VkPhysicalDeviceUniformBufferStandardLayoutFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderSubgroupExtendedTypesFeatures, &PhysicalDeviceShaderSubgroupExtendedTypesFeatures.pNext, "VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceSeparateDepthStencilLayoutsFeatures, &PhysicalDeviceSeparateDepthStencilLayoutsFeatures.pNext, "VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceHostQueryResetFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceHostQueryResetFeatures, &PhysicalDeviceHostQueryResetFeatures.pNext, "VkPhysicalDeviceHostQueryResetFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceTimelineSemaphoreFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceTimelineSemaphoreFeatures, &PhysicalDeviceTimelineSemaphoreFeatures.pNext, "VkPhysicalDeviceTimelineSemaphoreFeatures", "VK_VERSION_1_2"};
    if (a_feature_name == "VkPhysicalDeviceBufferDeviceAddressFeatures"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceBufferDeviceAddressFeatures, &PhysicalDeviceBufferDeviceAddressFeatures.pNext, "VkPhysicalDeviceBufferDeviceAddressFeatures", "VK_VERSION_1_2"};
#endif

#if VK_KHR_dynamic_rendering
    if (a_feature_name == "VkPhysicalDeviceDynamicRenderingFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDynamicRenderingFeaturesKHR, &PhysicalDeviceDynamicRenderingFeaturesKHR.pNext, "VkPhysicalDeviceDynamicRenderingFeaturesKHR", "VK_KHR_dynamic_rendering"};
#endif

#if VK_KHR_performance_query
    if (a_feature_name == "VkPhysicalDevicePerformanceQueryFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePerformanceQueryFeaturesKHR, &PhysicalDevicePerformanceQueryFeaturesKHR.pNext, "VkPhysicalDevicePerformanceQueryFeaturesKHR", "VK_KHR_performance_query"};
#endif

#if VK_KHR_shader_clock
    if (a_feature_name == "VkPhysicalDeviceShaderClockFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderClockFeaturesKHR, &PhysicalDeviceShaderClockFeaturesKHR.pNext, "VkPhysicalDeviceShaderClockFeaturesKHR", "VK_KHR_shader_clock"};
#endif

#if VK_KHR_shader_terminate_invocation
    if (a_feature_name == "VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderTerminateInvocationFeaturesKHR, &PhysicalDeviceShaderTerminateInvocationFeaturesKHR.pNext, "VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR", "VK_KHR_shader_terminate_invocation"};
#endif

#if VK_KHR_fragment_shading_rate
    if (a_feature_name == "VkPhysicalDeviceFragmentShadingRateFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFragmentShadingRateFeaturesKHR, &PhysicalDeviceFragmentShadingRateFeaturesKHR.pNext, "VkPhysicalDeviceFragmentShadingRateFeaturesKHR", "VK_KHR_fragment_shading_rate"};
#endif

#if VK_KHR_present_wait
    if (a_feature_name == "VkPhysicalDevicePresentWaitFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePresentWaitFeaturesKHR, &PhysicalDevicePresentWaitFeaturesKHR.pNext, "VkPhysicalDevicePresentWaitFeaturesKHR", "VK_KHR_present_wait"};
#endif

#if VK_KHR_pipeline_executable_properties
    if (a_feature_name == "VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePipelineExecutablePropertiesFeaturesKHR, &PhysicalDevicePipelineExecutablePropertiesFeaturesKHR.pNext, "VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR", "VK_KHR_pipeline_executable_properties"};
#endif

#if VK_KHR_shader_integer_dot_product
    if (a_feature_name == "VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderIntegerDotProductFeaturesKHR, &PhysicalDeviceShaderIntegerDotProductFeaturesKHR.pNext, "VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR", "VK_KHR_shader_integer_dot_product"};
#endif

#if VK_KHR_present_id
    if (a_feature_name == "VkPhysicalDevicePresentIdFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePresentIdFeaturesKHR, &PhysicalDevicePresentIdFeaturesKHR.pNext, "VkPhysicalDevicePresentIdFeaturesKHR", "VK_KHR_present_id"};
#endif

#if VK_KHR_synchronization2
    if (a_feature_name == "VkPhysicalDeviceSynchronization2FeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceSynchronization2FeaturesKHR, &PhysicalDeviceSynchronization2FeaturesKHR.pNext, "VkPhysicalDeviceSynchronization2FeaturesKHR", "VK_KHR_synchronization2"};
#endif

#if VK_KHR_shader_subgroup_uniform_control_flow
    if (a_feature_name == "VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR, &PhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR.pNext, "VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR", "VK_KHR_shader_subgroup_uniform_control_flow"};
#endif

#if VK_KHR_zero_initialize_workgroup_memory
    if (a_feature_name == "VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR, &PhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR.pNext, "VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR", "VK_KHR_zero_initialize_workgroup_memory"};
#endif

#if VK_KHR_workgroup_memory_explicit_layout
    if (a_feature_name == "VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR, &PhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR.pNext, "VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR", "VK_KHR_workgroup_memory_explicit_layout"};
#endif

#if VK_KHR_maintenance4
    if (a_feature_name == "VkPhysicalDeviceMaintenance4FeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceMaintenance4FeaturesKHR, &PhysicalDeviceMaintenance4FeaturesKHR.pNext, "VkPhysicalDeviceMaintenance4FeaturesKHR", "VK_KHR_maintenance4"};
#endif

#if VK_EXT_transform_feedback
    if (a_feature_name == "VkPhysicalDeviceTransformFeedbackFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceTransformFeedbackFeaturesEXT, &PhysicalDeviceTransformFeedbackFeaturesEXT.pNext, "VkPhysicalDeviceTransformFeedbackFeaturesEXT", "VK_EXT_transform_feedback"};
#endif

#if VK_NV_corner_sampled_image
    if (a_feature_name == "VkPhysicalDeviceCornerSampledImageFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceCornerSampledImageFeaturesNV, &PhysicalDeviceCornerSampledImageFeaturesNV.pNext, "VkPhysicalDeviceCornerSampledImageFeaturesNV", "VK_NV_corner_sampled_image"};
#endif

#if VK_EXT_texture_compression_astc_hdr
    if (a_feature_name == "VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceTextureCompressionASTCHDRFeaturesEXT, &PhysicalDeviceTextureCompressionASTCHDRFeaturesEXT.pNext, "VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT", "VK_EXT_texture_compression_astc_hdr"};
#endif

#if VK_EXT_astc_decode_mode
    if (a_feature_name == "VkPhysicalDeviceASTCDecodeFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceASTCDecodeFeaturesEXT, &PhysicalDeviceASTCDecodeFeaturesEXT.pNext, "VkPhysicalDeviceASTCDecodeFeaturesEXT", "VK_EXT_astc_decode_mode"};
#endif

#if VK_EXT_conditional_rendering
    if (a_feature_name == "VkPhysicalDeviceConditionalRenderingFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceConditionalRenderingFeaturesEXT, &PhysicalDeviceConditionalRenderingFeaturesEXT.pNext, "VkPhysicalDeviceConditionalRenderingFeaturesEXT", "VK_EXT_conditional_rendering"};
#endif

#if VK_EXT_depth_clip_enable
    if (a_feature_name == "VkPhysicalDeviceDepthClipEnableFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDepthClipEnableFeaturesEXT, &PhysicalDeviceDepthClipEnableFeaturesEXT.pNext, "VkPhysicalDeviceDepthClipEnableFeaturesEXT", "VK_EXT_depth_clip_enable"};
#endif

#if VK_EXT_inline_uniform_block
    if (a_feature_name == "VkPhysicalDeviceInlineUniformBlockFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceInlineUniformBlockFeaturesEXT, &PhysicalDeviceInlineUniformBlockFeaturesEXT.pNext, "VkPhysicalDeviceInlineUniformBlockFeaturesEXT", "VK_EXT_inline_uniform_block"};
#endif

#if VK_EXT_blend_operation_advanced
    if (a_feature_name == "VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceBlendOperationAdvancedFeaturesEXT, &PhysicalDeviceBlendOperationAdvancedFeaturesEXT.pNext, "VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT", "VK_EXT_blend_operation_advanced"};
#endif

#if VK_NV_shader_sm_builtins
    if (a_feature_name == "VkPhysicalDeviceShaderSMBuiltinsFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderSMBuiltinsFeaturesNV, &PhysicalDeviceShaderSMBuiltinsFeaturesNV.pNext, "VkPhysicalDeviceShaderSMBuiltinsFeaturesNV", "VK_NV_shader_sm_builtins"};
#endif

#if VK_NV_shading_rate_image
    if (a_feature_name == "VkPhysicalDeviceShadingRateImageFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShadingRateImageFeaturesNV, &PhysicalDeviceShadingRateImageFeaturesNV.pNext, "VkPhysicalDeviceShadingRateImageFeaturesNV", "VK_NV_shading_rate_image"};
#endif

#if VK_NV_representative_fragment_test
    if (a_feature_name == "VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceRepresentativeFragmentTestFeaturesNV, &PhysicalDeviceRepresentativeFragmentTestFeaturesNV.pNext, "VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV", "VK_NV_representative_fragment_test"};
#endif

#if VK_EXT_vertex_attribute_divisor
    if (a_feature_name == "VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceVertexAttributeDivisorFeaturesEXT, &PhysicalDeviceVertexAttributeDivisorFeaturesEXT.pNext, "VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT", "VK_EXT_vertex_attribute_divisor"};
#endif

#if VK_NV_compute_shader_derivatives
    if (a_feature_name == "VkPhysicalDeviceComputeShaderDerivativesFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceComputeShaderDerivativesFeaturesNV, &PhysicalDeviceComputeShaderDerivativesFeaturesNV.pNext, "VkPhysicalDeviceComputeShaderDerivativesFeaturesNV", "VK_NV_compute_shader_derivatives"};
#endif

#if VK_NV_mesh_shader
    if (a_feature_name == "VkPhysicalDeviceMeshShaderFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceMeshShaderFeaturesNV, &PhysicalDeviceMeshShaderFeaturesNV.pNext, "VkPhysicalDeviceMeshShaderFeaturesNV", "VK_NV_mesh_shader"};
#endif

#if VK_NV_fragment_shader_barycentric
    if (a_feature_name == "VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFragmentShaderBarycentricFeaturesNV, &PhysicalDeviceFragmentShaderBarycentricFeaturesNV.pNext, "VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV", "VK_NV_fragment_shader_barycentric"};
#endif

#if VK_NV_shader_image_footprint
    if (a_feature_name == "VkPhysicalDeviceShaderImageFootprintFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderImageFootprintFeaturesNV, &PhysicalDeviceShaderImageFootprintFeaturesNV.pNext, "VkPhysicalDeviceShaderImageFootprintFeaturesNV", "VK_NV_shader_image_footprint"};
#endif

#if VK_NV_scissor_exclusive
    if (a_feature_name == "VkPhysicalDeviceExclusiveScissorFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceExclusiveScissorFeaturesNV, &PhysicalDeviceExclusiveScissorFeaturesNV.pNext, "VkPhysicalDeviceExclusiveScissorFeaturesNV", "VK_NV_scissor_exclusive"};
#endif

#if VK_INTEL_shader_integer_functions2
    if (a_feature_name == "VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL, &PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL.pNext, "VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL", "VK_INTEL_shader_integer_functions2"};
#endif

#if VK_EXT_fragment_density_map
    if (a_feature_name == "VkPhysicalDeviceFragmentDensityMapFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFragmentDensityMapFeaturesEXT, &PhysicalDeviceFragmentDensityMapFeaturesEXT.pNext, "VkPhysicalDeviceFragmentDensityMapFeaturesEXT", "VK_EXT_fragment_density_map"};
#endif

#if VK_EXT_subgroup_size_control
    if (a_feature_name == "VkPhysicalDeviceSubgroupSizeControlFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceSubgroupSizeControlFeaturesEXT, &PhysicalDeviceSubgroupSizeControlFeaturesEXT.pNext, "VkPhysicalDeviceSubgroupSizeControlFeaturesEXT", "VK_EXT_subgroup_size_control"};
#endif

#if VK_AMD_device_coherent_memory
    if (a_feature_name == "VkPhysicalDeviceCoherentMemoryFeaturesAMD"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceCoherentMemoryFeaturesAMD, &PhysicalDeviceCoherentMemoryFeaturesAMD.pNext, "VkPhysicalDeviceCoherentMemoryFeaturesAMD", "VK_AMD_device_coherent_memory"};
#endif

#if VK_EXT_shader_image_atomic_int64
    if (a_feature_name == "VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderImageAtomicInt64FeaturesEXT, &PhysicalDeviceShaderImageAtomicInt64FeaturesEXT.pNext, "VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT", "VK_EXT_shader_image_atomic_int64"};
#endif

#if VK_EXT_memory_priority
    if (a_feature_name == "VkPhysicalDeviceMemoryPriorityFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceMemoryPriorityFeaturesEXT, &PhysicalDeviceMemoryPriorityFeaturesEXT.pNext, "VkPhysicalDeviceMemoryPriorityFeaturesEXT", "VK_EXT_memory_priority"};
#endif

#if VK_NV_dedicated_allocation_image_aliasing
    if (a_feature_name == "VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV, &PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV.pNext, "VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV", "VK_NV_dedicated_allocation_image_aliasing"};
#endif

#if VK_EXT_buffer_device_address
    if (a_feature_name == "VkPhysicalDeviceBufferDeviceAddressFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceBufferDeviceAddressFeaturesEXT, &PhysicalDeviceBufferDeviceAddressFeaturesEXT.pNext, "VkPhysicalDeviceBufferDeviceAddressFeaturesEXT", "VK_EXT_buffer_device_address"};
#endif

#if VK_NV_cooperative_matrix
    if (a_feature_name == "VkPhysicalDeviceCooperativeMatrixFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceCooperativeMatrixFeaturesNV, &PhysicalDeviceCooperativeMatrixFeaturesNV.pNext, "VkPhysicalDeviceCooperativeMatrixFeaturesNV", "VK_NV_cooperative_matrix"};
#endif

#if VK_NV_coverage_reduction_mode
    if (a_feature_name == "VkPhysicalDeviceCoverageReductionModeFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceCoverageReductionModeFeaturesNV, &PhysicalDeviceCoverageReductionModeFeaturesNV.pNext, "VkPhysicalDeviceCoverageReductionModeFeaturesNV", "VK_NV_coverage_reduction_mode"};
#endif

#if VK_EXT_fragment_shader_interlock
    if (a_feature_name == "VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFragmentShaderInterlockFeaturesEXT, &PhysicalDeviceFragmentShaderInterlockFeaturesEXT.pNext, "VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT", "VK_EXT_fragment_shader_interlock"};
#endif

#if VK_EXT_ycbcr_image_arrays
    if (a_feature_name == "VkPhysicalDeviceYcbcrImageArraysFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceYcbcrImageArraysFeaturesEXT, &PhysicalDeviceYcbcrImageArraysFeaturesEXT.pNext, "VkPhysicalDeviceYcbcrImageArraysFeaturesEXT", "VK_EXT_ycbcr_image_arrays"};
#endif

#if VK_EXT_provoking_vertex
    if (a_feature_name == "VkPhysicalDeviceProvokingVertexFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceProvokingVertexFeaturesEXT, &PhysicalDeviceProvokingVertexFeaturesEXT.pNext, "VkPhysicalDeviceProvokingVertexFeaturesEXT", "VK_EXT_provoking_vertex"};
#endif

#if VK_EXT_line_rasterization
    if (a_feature_name == "VkPhysicalDeviceLineRasterizationFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceLineRasterizationFeaturesEXT, &PhysicalDeviceLineRasterizationFeaturesEXT.pNext, "VkPhysicalDeviceLineRasterizationFeaturesEXT", "VK_EXT_line_rasterization"};
#endif

#if VK_EXT_shader_atomic_float
    if (a_feature_name == "VkPhysicalDeviceShaderAtomicFloatFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderAtomicFloatFeaturesEXT, &PhysicalDeviceShaderAtomicFloatFeaturesEXT.pNext, "VkPhysicalDeviceShaderAtomicFloatFeaturesEXT", "VK_EXT_shader_atomic_float"};
#endif

#if VK_EXT_index_type_uint8
    if (a_feature_name == "VkPhysicalDeviceIndexTypeUint8FeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceIndexTypeUint8FeaturesEXT, &PhysicalDeviceIndexTypeUint8FeaturesEXT.pNext, "VkPhysicalDeviceIndexTypeUint8FeaturesEXT", "VK_EXT_index_type_uint8"};
#endif

#if VK_EXT_extended_dynamic_state
    if (a_feature_name == "VkPhysicalDeviceExtendedDynamicStateFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceExtendedDynamicStateFeaturesEXT, &PhysicalDeviceExtendedDynamicStateFeaturesEXT.pNext, "VkPhysicalDeviceExtendedDynamicStateFeaturesEXT", "VK_EXT_extended_dynamic_state"};
#endif

#if VK_EXT_shader_atomic_float2
    if (a_feature_name == "VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderAtomicFloat2FeaturesEXT, &PhysicalDeviceShaderAtomicFloat2FeaturesEXT.pNext, "VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT", "VK_EXT_shader_atomic_float2"};
#endif

#if VK_EXT_shader_demote_to_helper_invocation
    if (a_feature_name == "VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT, &PhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT.pNext, "VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT", "VK_EXT_shader_demote_to_helper_invocation"};
#endif

#if VK_NV_device_generated_commands
    if (a_feature_name == "VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDeviceGeneratedCommandsFeaturesNV, &PhysicalDeviceDeviceGeneratedCommandsFeaturesNV.pNext, "VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV", "VK_NV_device_generated_commands"};
#endif

#if VK_NV_inherited_viewport_scissor
    if (a_feature_name == "VkPhysicalDeviceInheritedViewportScissorFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceInheritedViewportScissorFeaturesNV, &PhysicalDeviceInheritedViewportScissorFeaturesNV.pNext, "VkPhysicalDeviceInheritedViewportScissorFeaturesNV", "VK_NV_inherited_viewport_scissor"};
#endif

#if VK_EXT_texel_buffer_alignment
    if (a_feature_name == "VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceTexelBufferAlignmentFeaturesEXT, &PhysicalDeviceTexelBufferAlignmentFeaturesEXT.pNext, "VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT", "VK_EXT_texel_buffer_alignment"};
#endif

#if VK_EXT_device_memory_report
    if (a_feature_name == "VkPhysicalDeviceDeviceMemoryReportFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDeviceMemoryReportFeaturesEXT, &PhysicalDeviceDeviceMemoryReportFeaturesEXT.pNext, "VkPhysicalDeviceDeviceMemoryReportFeaturesEXT", "VK_EXT_device_memory_report"};
#endif

#if VK_EXT_robustness2
    if (a_feature_name == "VkPhysicalDeviceRobustness2FeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceRobustness2FeaturesEXT, &PhysicalDeviceRobustness2FeaturesEXT.pNext, "VkPhysicalDeviceRobustness2FeaturesEXT", "VK_EXT_robustness2"};
#endif

#if VK_EXT_custom_border_color
    if (a_feature_name == "VkPhysicalDeviceCustomBorderColorFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceCustomBorderColorFeaturesEXT, &PhysicalDeviceCustomBorderColorFeaturesEXT.pNext, "VkPhysicalDeviceCustomBorderColorFeaturesEXT", "VK_EXT_custom_border_color"};
#endif

#if VK_EXT_private_data
    if (a_feature_name == "VkPhysicalDevicePrivateDataFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePrivateDataFeaturesEXT, &PhysicalDevicePrivateDataFeaturesEXT.pNext, "VkPhysicalDevicePrivateDataFeaturesEXT", "VK_EXT_private_data"};
#endif

#if VK_EXT_pipeline_creation_cache_control
    if (a_feature_name == "VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePipelineCreationCacheControlFeaturesEXT, &PhysicalDevicePipelineCreationCacheControlFeaturesEXT.pNext, "VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT", "VK_EXT_pipeline_creation_cache_control"};
#endif

#if VK_NV_device_diagnostics_config
    if (a_feature_name == "VkPhysicalDeviceDiagnosticsConfigFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceDiagnosticsConfigFeaturesNV, &PhysicalDeviceDiagnosticsConfigFeaturesNV.pNext, "VkPhysicalDeviceDiagnosticsConfigFeaturesNV", "VK_NV_device_diagnostics_config"};
#endif

#if VK_NV_fragment_shading_rate_enums
    if (a_feature_name == "VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFragmentShadingRateEnumsFeaturesNV, &PhysicalDeviceFragmentShadingRateEnumsFeaturesNV.pNext, "VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV", "VK_NV_fragment_shading_rate_enums"};
#endif

#if VK_NV_ray_tracing_motion_blur
    if (a_feature_name == "VkPhysicalDeviceRayTracingMotionBlurFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceRayTracingMotionBlurFeaturesNV, &PhysicalDeviceRayTracingMotionBlurFeaturesNV.pNext, "VkPhysicalDeviceRayTracingMotionBlurFeaturesNV", "VK_NV_ray_tracing_motion_blur"};
#endif

#if VK_EXT_ycbcr_2plane_444_formats
    if (a_feature_name == "VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT, &PhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT.pNext, "VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT", "VK_EXT_ycbcr_2plane_444_formats"};
#endif

#if VK_EXT_fragment_density_map2
    if (a_feature_name == "VkPhysicalDeviceFragmentDensityMap2FeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceFragmentDensityMap2FeaturesEXT, &PhysicalDeviceFragmentDensityMap2FeaturesEXT.pNext, "VkPhysicalDeviceFragmentDensityMap2FeaturesEXT", "VK_EXT_fragment_density_map2"};
#endif

#if VK_EXT_image_robustness
    if (a_feature_name == "VkPhysicalDeviceImageRobustnessFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceImageRobustnessFeaturesEXT, &PhysicalDeviceImageRobustnessFeaturesEXT.pNext, "VkPhysicalDeviceImageRobustnessFeaturesEXT", "VK_EXT_image_robustness"};
#endif

#if VK_EXT_4444_formats
    if (a_feature_name == "VkPhysicalDevice4444FormatsFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevice4444FormatsFeaturesEXT, &PhysicalDevice4444FormatsFeaturesEXT.pNext, "VkPhysicalDevice4444FormatsFeaturesEXT", "VK_EXT_4444_formats"};
#endif

#if VK_EXT_rgba10x6_formats
    if (a_feature_name == "VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceRGBA10X6FormatsFeaturesEXT, &PhysicalDeviceRGBA10X6FormatsFeaturesEXT.pNext, "VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT", "VK_EXT_rgba10x6_formats"};
#endif

#if VK_VALVE_mutable_descriptor_type
    if (a_feature_name == "VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceMutableDescriptorTypeFeaturesVALVE, &PhysicalDeviceMutableDescriptorTypeFeaturesVALVE.pNext, "VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE", "VK_VALVE_mutable_descriptor_type"};
#endif

#if VK_EXT_vertex_input_dynamic_state
    if (a_feature_name == "VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceVertexInputDynamicStateFeaturesEXT, &PhysicalDeviceVertexInputDynamicStateFeaturesEXT.pNext, "VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT", "VK_EXT_vertex_input_dynamic_state"};
#endif

#if VK_EXT_primitive_topology_list_restart
    if (a_feature_name == "VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePrimitiveTopologyListRestartFeaturesEXT, &PhysicalDevicePrimitiveTopologyListRestartFeaturesEXT.pNext, "VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT", "VK_EXT_primitive_topology_list_restart"};
#endif

#if VK_HUAWEI_subpass_shading
    if (a_feature_name == "VkPhysicalDeviceSubpassShadingFeaturesHUAWEI"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceSubpassShadingFeaturesHUAWEI, &PhysicalDeviceSubpassShadingFeaturesHUAWEI.pNext, "VkPhysicalDeviceSubpassShadingFeaturesHUAWEI", "VK_HUAWEI_subpass_shading"};
#endif

#if VK_HUAWEI_invocation_mask
    if (a_feature_name == "VkPhysicalDeviceInvocationMaskFeaturesHUAWEI"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceInvocationMaskFeaturesHUAWEI, &PhysicalDeviceInvocationMaskFeaturesHUAWEI.pNext, "VkPhysicalDeviceInvocationMaskFeaturesHUAWEI", "VK_HUAWEI_invocation_mask"};
#endif

#if VK_NV_external_memory_rdma
    if (a_feature_name == "VkPhysicalDeviceExternalMemoryRDMAFeaturesNV"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceExternalMemoryRDMAFeaturesNV, &PhysicalDeviceExternalMemoryRDMAFeaturesNV.pNext, "VkPhysicalDeviceExternalMemoryRDMAFeaturesNV", "VK_NV_external_memory_rdma"};
#endif

#if VK_EXT_extended_dynamic_state2
    if (a_feature_name == "VkPhysicalDeviceExtendedDynamicState2FeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceExtendedDynamicState2FeaturesEXT, &PhysicalDeviceExtendedDynamicState2FeaturesEXT.pNext, "VkPhysicalDeviceExtendedDynamicState2FeaturesEXT", "VK_EXT_extended_dynamic_state2"};
#endif

#if VK_EXT_color_write_enable
    if (a_feature_name == "VkPhysicalDeviceColorWriteEnableFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceColorWriteEnableFeaturesEXT, &PhysicalDeviceColorWriteEnableFeaturesEXT.pNext, "VkPhysicalDeviceColorWriteEnableFeaturesEXT", "VK_EXT_color_write_enable"};
#endif

#if VK_EXT_global_priority_query
    if (a_feature_name == "VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceGlobalPriorityQueryFeaturesEXT, &PhysicalDeviceGlobalPriorityQueryFeaturesEXT.pNext, "VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT", "VK_EXT_global_priority_query"};
#endif

#if VK_EXT_multi_draw
    if (a_feature_name == "VkPhysicalDeviceMultiDrawFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceMultiDrawFeaturesEXT, &PhysicalDeviceMultiDrawFeaturesEXT.pNext, "VkPhysicalDeviceMultiDrawFeaturesEXT", "VK_EXT_multi_draw"};
#endif

#if VK_EXT_border_color_swizzle
    if (a_feature_name == "VkPhysicalDeviceBorderColorSwizzleFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceBorderColorSwizzleFeaturesEXT, &PhysicalDeviceBorderColorSwizzleFeaturesEXT.pNext, "VkPhysicalDeviceBorderColorSwizzleFeaturesEXT", "VK_EXT_border_color_swizzle"};
#endif

#if VK_EXT_pageable_device_local_memory
    if (a_feature_name == "VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT"sv) return physical_dev_feature2_wrapper_t{&PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT, &PhysicalDevicePageableDeviceLocalMemoryFeaturesEXT.pNext, "VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT", "VK_EXT_pageable_device_local_memory"};
#endif

#if VK_KHR_acceleration_structure
    if (a_feature_name == "VkPhysicalDeviceAccelerationStructureFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceAccelerationStructureFeaturesKHR, &PhysicalDeviceAccelerationStructureFeaturesKHR.pNext, "VkPhysicalDeviceAccelerationStructureFeaturesKHR", "VK_KHR_acceleration_structure"};
#endif

#if VK_KHR_ray_tracing_pipeline
    if (a_feature_name == "VkPhysicalDeviceRayTracingPipelineFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceRayTracingPipelineFeaturesKHR, &PhysicalDeviceRayTracingPipelineFeaturesKHR.pNext, "VkPhysicalDeviceRayTracingPipelineFeaturesKHR", "VK_KHR_ray_tracing_pipeline"};
#endif

#if VK_KHR_ray_query
    if (a_feature_name == "VkPhysicalDeviceRayQueryFeaturesKHR"sv) return physical_dev_feature2_wrapper_t{&PhysicalDeviceRayQueryFeaturesKHR, &PhysicalDeviceRayQueryFeaturesKHR.pNext, "VkPhysicalDeviceRayQueryFeaturesKHR", "VK_KHR_ray_query"};
#endif

    if (a_ok_ptr) *a_ok_ptr = false;
    return physical_dev_feature2_wrapper_t();
}

std::string_view get_extension_name(std::string_view a_feature_name, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
#if VK_VERSION_1_1
    if(a_feature_name == "VkPhysicalDevice16BitStorageFeatures"sv) return "VK_VERSION_1_1";
    if(a_feature_name == "VkPhysicalDeviceFeatures2"sv) return "VK_VERSION_1_1";
    if(a_feature_name == "VkPhysicalDeviceMultiviewFeatures"sv) return "VK_VERSION_1_1";
    if(a_feature_name == "VkPhysicalDeviceVariablePointersFeatures"sv) return "VK_VERSION_1_1";
    if(a_feature_name == "VkPhysicalDeviceProtectedMemoryFeatures"sv) return "VK_VERSION_1_1";
    if(a_feature_name == "VkPhysicalDeviceSamplerYcbcrConversionFeatures"sv) return "VK_VERSION_1_1";
    if(a_feature_name == "VkPhysicalDeviceShaderDrawParametersFeatures"sv) return "VK_VERSION_1_1";
#endif
#if VK_VERSION_1_2
    if(a_feature_name == "VkPhysicalDeviceVulkan11Features"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceVulkan12Features"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDevice8BitStorageFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceShaderAtomicInt64Features"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceShaderFloat16Int8Features"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceDescriptorIndexingFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceScalarBlockLayoutFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceVulkanMemoryModelFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceImagelessFramebufferFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceUniformBufferStandardLayoutFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceHostQueryResetFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceTimelineSemaphoreFeatures"sv) return "VK_VERSION_1_2";
    if(a_feature_name == "VkPhysicalDeviceBufferDeviceAddressFeatures"sv) return "VK_VERSION_1_2";
#endif
#if VK_KHR_dynamic_rendering
    if(a_feature_name == "VkPhysicalDeviceDynamicRenderingFeaturesKHR"sv) return "VK_KHR_dynamic_rendering";
#endif
#if VK_KHR_performance_query
    if(a_feature_name == "VkPhysicalDevicePerformanceQueryFeaturesKHR"sv) return "VK_KHR_performance_query";
#endif
#if VK_KHR_shader_clock
    if(a_feature_name == "VkPhysicalDeviceShaderClockFeaturesKHR"sv) return "VK_KHR_shader_clock";
#endif
#if VK_KHR_shader_terminate_invocation
    if(a_feature_name == "VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR"sv) return "VK_KHR_shader_terminate_invocation";
#endif
#if VK_KHR_fragment_shading_rate
    if(a_feature_name == "VkPhysicalDeviceFragmentShadingRateFeaturesKHR"sv) return "VK_KHR_fragment_shading_rate";
#endif
#if VK_KHR_present_wait
    if(a_feature_name == "VkPhysicalDevicePresentWaitFeaturesKHR"sv) return "VK_KHR_present_wait";
#endif
#if VK_KHR_pipeline_executable_properties
    if(a_feature_name == "VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR"sv) return "VK_KHR_pipeline_executable_properties";
#endif
#if VK_KHR_shader_integer_dot_product
    if(a_feature_name == "VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR"sv) return "VK_KHR_shader_integer_dot_product";
#endif
#if VK_KHR_present_id
    if(a_feature_name == "VkPhysicalDevicePresentIdFeaturesKHR"sv) return "VK_KHR_present_id";
#endif
#if VK_KHR_synchronization2
    if(a_feature_name == "VkPhysicalDeviceSynchronization2FeaturesKHR"sv) return "VK_KHR_synchronization2";
#endif
#if VK_KHR_shader_subgroup_uniform_control_flow
    if(a_feature_name == "VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR"sv) return "VK_KHR_shader_subgroup_uniform_control_flow";
#endif
#if VK_KHR_zero_initialize_workgroup_memory
    if(a_feature_name == "VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR"sv) return "VK_KHR_zero_initialize_workgroup_memory";
#endif
#if VK_KHR_workgroup_memory_explicit_layout
    if(a_feature_name == "VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR"sv) return "VK_KHR_workgroup_memory_explicit_layout";
#endif
#if VK_KHR_maintenance4
    if(a_feature_name == "VkPhysicalDeviceMaintenance4FeaturesKHR"sv) return "VK_KHR_maintenance4";
#endif
#if VK_EXT_transform_feedback
    if(a_feature_name == "VkPhysicalDeviceTransformFeedbackFeaturesEXT"sv) return "VK_EXT_transform_feedback";
#endif
#if VK_NV_corner_sampled_image
    if(a_feature_name == "VkPhysicalDeviceCornerSampledImageFeaturesNV"sv) return "VK_NV_corner_sampled_image";
#endif
#if VK_EXT_texture_compression_astc_hdr
    if(a_feature_name == "VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT"sv) return "VK_EXT_texture_compression_astc_hdr";
#endif
#if VK_EXT_astc_decode_mode
    if(a_feature_name == "VkPhysicalDeviceASTCDecodeFeaturesEXT"sv) return "VK_EXT_astc_decode_mode";
#endif
#if VK_EXT_conditional_rendering
    if(a_feature_name == "VkPhysicalDeviceConditionalRenderingFeaturesEXT"sv) return "VK_EXT_conditional_rendering";
#endif
#if VK_EXT_depth_clip_enable
    if(a_feature_name == "VkPhysicalDeviceDepthClipEnableFeaturesEXT"sv) return "VK_EXT_depth_clip_enable";
#endif
#if VK_EXT_inline_uniform_block
    if(a_feature_name == "VkPhysicalDeviceInlineUniformBlockFeaturesEXT"sv) return "VK_EXT_inline_uniform_block";
#endif
#if VK_EXT_blend_operation_advanced
    if(a_feature_name == "VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT"sv) return "VK_EXT_blend_operation_advanced";
#endif
#if VK_NV_shader_sm_builtins
    if(a_feature_name == "VkPhysicalDeviceShaderSMBuiltinsFeaturesNV"sv) return "VK_NV_shader_sm_builtins";
#endif
#if VK_NV_shading_rate_image
    if(a_feature_name == "VkPhysicalDeviceShadingRateImageFeaturesNV"sv) return "VK_NV_shading_rate_image";
#endif
#if VK_NV_representative_fragment_test
    if(a_feature_name == "VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV"sv) return "VK_NV_representative_fragment_test";
#endif
#if VK_EXT_vertex_attribute_divisor
    if(a_feature_name == "VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT"sv) return "VK_EXT_vertex_attribute_divisor";
#endif
#if VK_NV_compute_shader_derivatives
    if(a_feature_name == "VkPhysicalDeviceComputeShaderDerivativesFeaturesNV"sv) return "VK_NV_compute_shader_derivatives";
#endif
#if VK_NV_mesh_shader
    if(a_feature_name == "VkPhysicalDeviceMeshShaderFeaturesNV"sv) return "VK_NV_mesh_shader";
#endif
#if VK_NV_fragment_shader_barycentric
    if(a_feature_name == "VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV"sv) return "VK_NV_fragment_shader_barycentric";
#endif
#if VK_NV_shader_image_footprint
    if(a_feature_name == "VkPhysicalDeviceShaderImageFootprintFeaturesNV"sv) return "VK_NV_shader_image_footprint";
#endif
#if VK_NV_scissor_exclusive
    if(a_feature_name == "VkPhysicalDeviceExclusiveScissorFeaturesNV"sv) return "VK_NV_scissor_exclusive";
#endif
#if VK_INTEL_shader_integer_functions2
    if(a_feature_name == "VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL"sv) return "VK_INTEL_shader_integer_functions2";
#endif
#if VK_EXT_fragment_density_map
    if(a_feature_name == "VkPhysicalDeviceFragmentDensityMapFeaturesEXT"sv) return "VK_EXT_fragment_density_map";
#endif
#if VK_EXT_subgroup_size_control
    if(a_feature_name == "VkPhysicalDeviceSubgroupSizeControlFeaturesEXT"sv) return "VK_EXT_subgroup_size_control";
#endif
#if VK_AMD_device_coherent_memory
    if(a_feature_name == "VkPhysicalDeviceCoherentMemoryFeaturesAMD"sv) return "VK_AMD_device_coherent_memory";
#endif
#if VK_EXT_shader_image_atomic_int64
    if(a_feature_name == "VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT"sv) return "VK_EXT_shader_image_atomic_int64";
#endif
#if VK_EXT_memory_priority
    if(a_feature_name == "VkPhysicalDeviceMemoryPriorityFeaturesEXT"sv) return "VK_EXT_memory_priority";
#endif
#if VK_NV_dedicated_allocation_image_aliasing
    if(a_feature_name == "VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV"sv) return "VK_NV_dedicated_allocation_image_aliasing";
#endif
#if VK_EXT_buffer_device_address
    if(a_feature_name == "VkPhysicalDeviceBufferDeviceAddressFeaturesEXT"sv) return "VK_EXT_buffer_device_address";
#endif
#if VK_NV_cooperative_matrix
    if(a_feature_name == "VkPhysicalDeviceCooperativeMatrixFeaturesNV"sv) return "VK_NV_cooperative_matrix";
#endif
#if VK_NV_coverage_reduction_mode
    if(a_feature_name == "VkPhysicalDeviceCoverageReductionModeFeaturesNV"sv) return "VK_NV_coverage_reduction_mode";
#endif
#if VK_EXT_fragment_shader_interlock
    if(a_feature_name == "VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT"sv) return "VK_EXT_fragment_shader_interlock";
#endif
#if VK_EXT_ycbcr_image_arrays
    if(a_feature_name == "VkPhysicalDeviceYcbcrImageArraysFeaturesEXT"sv) return "VK_EXT_ycbcr_image_arrays";
#endif
#if VK_EXT_provoking_vertex
    if(a_feature_name == "VkPhysicalDeviceProvokingVertexFeaturesEXT"sv) return "VK_EXT_provoking_vertex";
#endif
#if VK_EXT_line_rasterization
    if(a_feature_name == "VkPhysicalDeviceLineRasterizationFeaturesEXT"sv) return "VK_EXT_line_rasterization";
#endif
#if VK_EXT_shader_atomic_float
    if(a_feature_name == "VkPhysicalDeviceShaderAtomicFloatFeaturesEXT"sv) return "VK_EXT_shader_atomic_float";
#endif
#if VK_EXT_index_type_uint8
    if(a_feature_name == "VkPhysicalDeviceIndexTypeUint8FeaturesEXT"sv) return "VK_EXT_index_type_uint8";
#endif
#if VK_EXT_extended_dynamic_state
    if(a_feature_name == "VkPhysicalDeviceExtendedDynamicStateFeaturesEXT"sv) return "VK_EXT_extended_dynamic_state";
#endif
#if VK_EXT_shader_atomic_float2
    if(a_feature_name == "VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT"sv) return "VK_EXT_shader_atomic_float2";
#endif
#if VK_EXT_shader_demote_to_helper_invocation
    if(a_feature_name == "VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT"sv) return "VK_EXT_shader_demote_to_helper_invocation";
#endif
#if VK_NV_device_generated_commands
    if(a_feature_name == "VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV"sv) return "VK_NV_device_generated_commands";
#endif
#if VK_NV_inherited_viewport_scissor
    if(a_feature_name == "VkPhysicalDeviceInheritedViewportScissorFeaturesNV"sv) return "VK_NV_inherited_viewport_scissor";
#endif
#if VK_EXT_texel_buffer_alignment
    if(a_feature_name == "VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT"sv) return "VK_EXT_texel_buffer_alignment";
#endif
#if VK_EXT_device_memory_report
    if(a_feature_name == "VkPhysicalDeviceDeviceMemoryReportFeaturesEXT"sv) return "VK_EXT_device_memory_report";
#endif
#if VK_EXT_robustness2
    if(a_feature_name == "VkPhysicalDeviceRobustness2FeaturesEXT"sv) return "VK_EXT_robustness2";
#endif
#if VK_EXT_custom_border_color
    if(a_feature_name == "VkPhysicalDeviceCustomBorderColorFeaturesEXT"sv) return "VK_EXT_custom_border_color";
#endif
#if VK_EXT_private_data
    if(a_feature_name == "VkPhysicalDevicePrivateDataFeaturesEXT"sv) return "VK_EXT_private_data";
#endif
#if VK_EXT_pipeline_creation_cache_control
    if(a_feature_name == "VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT"sv) return "VK_EXT_pipeline_creation_cache_control";
#endif
#if VK_NV_device_diagnostics_config
    if(a_feature_name == "VkPhysicalDeviceDiagnosticsConfigFeaturesNV"sv) return "VK_NV_device_diagnostics_config";
#endif
#if VK_NV_fragment_shading_rate_enums
    if(a_feature_name == "VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV"sv) return "VK_NV_fragment_shading_rate_enums";
#endif
#if VK_NV_ray_tracing_motion_blur
    if(a_feature_name == "VkPhysicalDeviceRayTracingMotionBlurFeaturesNV"sv) return "VK_NV_ray_tracing_motion_blur";
#endif
#if VK_EXT_ycbcr_2plane_444_formats
    if(a_feature_name == "VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT"sv) return "VK_EXT_ycbcr_2plane_444_formats";
#endif
#if VK_EXT_fragment_density_map2
    if(a_feature_name == "VkPhysicalDeviceFragmentDensityMap2FeaturesEXT"sv) return "VK_EXT_fragment_density_map2";
#endif
#if VK_EXT_image_robustness
    if(a_feature_name == "VkPhysicalDeviceImageRobustnessFeaturesEXT"sv) return "VK_EXT_image_robustness";
#endif
#if VK_EXT_4444_formats
    if(a_feature_name == "VkPhysicalDevice4444FormatsFeaturesEXT"sv) return "VK_EXT_4444_formats";
#endif
#if VK_EXT_rgba10x6_formats
    if(a_feature_name == "VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT"sv) return "VK_EXT_rgba10x6_formats";
#endif
#if VK_VALVE_mutable_descriptor_type
    if(a_feature_name == "VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE"sv) return "VK_VALVE_mutable_descriptor_type";
#endif
#if VK_EXT_vertex_input_dynamic_state
    if(a_feature_name == "VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT"sv) return "VK_EXT_vertex_input_dynamic_state";
#endif
#if VK_EXT_primitive_topology_list_restart
    if(a_feature_name == "VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT"sv) return "VK_EXT_primitive_topology_list_restart";
#endif
#if VK_HUAWEI_subpass_shading
    if(a_feature_name == "VkPhysicalDeviceSubpassShadingFeaturesHUAWEI"sv) return "VK_HUAWEI_subpass_shading";
#endif
#if VK_HUAWEI_invocation_mask
    if(a_feature_name == "VkPhysicalDeviceInvocationMaskFeaturesHUAWEI"sv) return "VK_HUAWEI_invocation_mask";
#endif
#if VK_NV_external_memory_rdma
    if(a_feature_name == "VkPhysicalDeviceExternalMemoryRDMAFeaturesNV"sv) return "VK_NV_external_memory_rdma";
#endif
#if VK_EXT_extended_dynamic_state2
    if(a_feature_name == "VkPhysicalDeviceExtendedDynamicState2FeaturesEXT"sv) return "VK_EXT_extended_dynamic_state2";
#endif
#if VK_EXT_color_write_enable
    if(a_feature_name == "VkPhysicalDeviceColorWriteEnableFeaturesEXT"sv) return "VK_EXT_color_write_enable";
#endif
#if VK_EXT_global_priority_query
    if(a_feature_name == "VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT"sv) return "VK_EXT_global_priority_query";
#endif
#if VK_EXT_multi_draw
    if(a_feature_name == "VkPhysicalDeviceMultiDrawFeaturesEXT"sv) return "VK_EXT_multi_draw";
#endif
#if VK_EXT_border_color_swizzle
    if(a_feature_name == "VkPhysicalDeviceBorderColorSwizzleFeaturesEXT"sv) return "VK_EXT_border_color_swizzle";
#endif
#if VK_EXT_pageable_device_local_memory
    if(a_feature_name == "VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT"sv) return "VK_EXT_pageable_device_local_memory";
#endif
#if VK_KHR_acceleration_structure
    if(a_feature_name == "VkPhysicalDeviceAccelerationStructureFeaturesKHR"sv) return "VK_KHR_acceleration_structure";
#endif
#if VK_KHR_ray_tracing_pipeline
    if(a_feature_name == "VkPhysicalDeviceRayTracingPipelineFeaturesKHR"sv) return "VK_KHR_ray_tracing_pipeline";
#endif
#if VK_KHR_ray_query
    if(a_feature_name == "VkPhysicalDeviceRayQueryFeaturesKHR"sv) return "VK_KHR_ray_query";
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return "Unknown_extension_name";
}

#if VK_VERSION_1_0
VkResult str_to_VkResult(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SUCCESS"sv) return VK_SUCCESS;
    if (a_str == "VK_NOT_READY"sv) return VK_NOT_READY;
    if (a_str == "VK_TIMEOUT"sv) return VK_TIMEOUT;
    if (a_str == "VK_EVENT_SET"sv) return VK_EVENT_SET;
    if (a_str == "VK_EVENT_RESET"sv) return VK_EVENT_RESET;
    if (a_str == "VK_INCOMPLETE"sv) return VK_INCOMPLETE;
    if (a_str == "VK_ERROR_OUT_OF_HOST_MEMORY"sv) return VK_ERROR_OUT_OF_HOST_MEMORY;
    if (a_str == "VK_ERROR_OUT_OF_DEVICE_MEMORY"sv) return VK_ERROR_OUT_OF_DEVICE_MEMORY;
    if (a_str == "VK_ERROR_INITIALIZATION_FAILED"sv) return VK_ERROR_INITIALIZATION_FAILED;
    if (a_str == "VK_ERROR_DEVICE_LOST"sv) return VK_ERROR_DEVICE_LOST;
    if (a_str == "VK_ERROR_MEMORY_MAP_FAILED"sv) return VK_ERROR_MEMORY_MAP_FAILED;
    if (a_str == "VK_ERROR_LAYER_NOT_PRESENT"sv) return VK_ERROR_LAYER_NOT_PRESENT;
    if (a_str == "VK_ERROR_EXTENSION_NOT_PRESENT"sv) return VK_ERROR_EXTENSION_NOT_PRESENT;
    if (a_str == "VK_ERROR_FEATURE_NOT_PRESENT"sv) return VK_ERROR_FEATURE_NOT_PRESENT;
    if (a_str == "VK_ERROR_INCOMPATIBLE_DRIVER"sv) return VK_ERROR_INCOMPATIBLE_DRIVER;
    if (a_str == "VK_ERROR_TOO_MANY_OBJECTS"sv) return VK_ERROR_TOO_MANY_OBJECTS;
    if (a_str == "VK_ERROR_FORMAT_NOT_SUPPORTED"sv) return VK_ERROR_FORMAT_NOT_SUPPORTED;
    if (a_str == "VK_ERROR_FRAGMENTED_POOL"sv) return VK_ERROR_FRAGMENTED_POOL;
    if (a_str == "VK_ERROR_UNKNOWN"sv) return VK_ERROR_UNKNOWN;
    if (a_str == "VK_ERROR_OUT_OF_POOL_MEMORY"sv) return VK_ERROR_OUT_OF_POOL_MEMORY;
    if (a_str == "VK_ERROR_INVALID_EXTERNAL_HANDLE"sv) return VK_ERROR_INVALID_EXTERNAL_HANDLE;
    if (a_str == "VK_ERROR_FRAGMENTATION"sv) return VK_ERROR_FRAGMENTATION;
    if (a_str == "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS"sv) return VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS;
    if (a_str == "VK_ERROR_SURFACE_LOST_KHR"sv) return VK_ERROR_SURFACE_LOST_KHR;
    if (a_str == "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR"sv) return VK_ERROR_NATIVE_WINDOW_IN_USE_KHR;
    if (a_str == "VK_SUBOPTIMAL_KHR"sv) return VK_SUBOPTIMAL_KHR;
    if (a_str == "VK_ERROR_OUT_OF_DATE_KHR"sv) return VK_ERROR_OUT_OF_DATE_KHR;
    if (a_str == "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR"sv) return VK_ERROR_INCOMPATIBLE_DISPLAY_KHR;
    if (a_str == "VK_ERROR_VALIDATION_FAILED_EXT"sv) return VK_ERROR_VALIDATION_FAILED_EXT;
    if (a_str == "VK_ERROR_INVALID_SHADER_NV"sv) return VK_ERROR_INVALID_SHADER_NV;
    if (a_str == "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT"sv) return VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT;
    if (a_str == "VK_ERROR_NOT_PERMITTED_EXT"sv) return VK_ERROR_NOT_PERMITTED_EXT;
    if (a_str == "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT"sv) return VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT;
    if (a_str == "VK_THREAD_IDLE_KHR"sv) return VK_THREAD_IDLE_KHR;
    if (a_str == "VK_THREAD_DONE_KHR"sv) return VK_THREAD_DONE_KHR;
    if (a_str == "VK_OPERATION_DEFERRED_KHR"sv) return VK_OPERATION_DEFERRED_KHR;
    if (a_str == "VK_OPERATION_NOT_DEFERRED_KHR"sv) return VK_OPERATION_NOT_DEFERRED_KHR;
    if (a_str == "VK_PIPELINE_COMPILE_REQUIRED_EXT"sv) return VK_PIPELINE_COMPILE_REQUIRED_EXT;
    if (a_str == "VK_ERROR_OUT_OF_POOL_MEMORY_KHR"sv) return VK_ERROR_OUT_OF_POOL_MEMORY_KHR;
    if (a_str == "VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR"sv) return VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR;
    if (a_str == "VK_ERROR_FRAGMENTATION_EXT"sv) return VK_ERROR_FRAGMENTATION_EXT;
    if (a_str == "VK_ERROR_INVALID_DEVICE_ADDRESS_EXT"sv) return VK_ERROR_INVALID_DEVICE_ADDRESS_EXT;
    if (a_str == "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR"sv) return VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR;
    if (a_str == "VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT"sv) return VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT;
    if (a_str == "VK_RESULT_MAX_ENUM"sv) return VK_RESULT_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkResult>(0x7FFFFFFF);
}

std::string_view str_from_VkResult(VkResult e)
{
    switch (e) {
        case VK_SUCCESS: return "VK_SUCCESS";
        case VK_NOT_READY: return "VK_NOT_READY";
        case VK_TIMEOUT: return "VK_TIMEOUT";
        case VK_EVENT_SET: return "VK_EVENT_SET";
        case VK_EVENT_RESET: return "VK_EVENT_RESET";
        case VK_INCOMPLETE: return "VK_INCOMPLETE";
        case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
        case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";
        case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";
        case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
        case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
        case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
        case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
        case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
        case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
        case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
        case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
        case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
        case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
        case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
        case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";
        case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
        case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
        case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
        case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
        case VK_SUBOPTIMAL_KHR: return "VK_SUBOPTIMAL_KHR";
        case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
        case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
        case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
        case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
        case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
        case VK_ERROR_NOT_PERMITTED_EXT: return "VK_ERROR_NOT_PERMITTED_EXT";
        case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";
        case VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
        case VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
        case VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
        case VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
        case VK_PIPELINE_COMPILE_REQUIRED_EXT: return "VK_PIPELINE_COMPILE_REQUIRED_EXT";
        case VK_RESULT_MAX_ENUM: return "VK_RESULT_MAX_ENUM";
        default: break;
    }
    return "Unknown VkResult";
}

VkStructureType str_to_VkStructureType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_STRUCTURE_TYPE_APPLICATION_INFO"sv) return VK_STRUCTURE_TYPE_APPLICATION_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SUBMIT_INFO"sv) return VK_STRUCTURE_TYPE_SUBMIT_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE"sv) return VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_SPARSE_INFO"sv) return VK_STRUCTURE_TYPE_BIND_SPARSE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_FENCE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EVENT_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET"sv) return VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET"sv) return VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET;
    if (a_str == "VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER"sv) return VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER"sv) return VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_BARRIER"sv) return VK_STRUCTURE_TYPE_MEMORY_BARRIER;
    if (a_str == "VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO"sv) return VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS"sv) return VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO"sv) return VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO"sv) return VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2"sv) return VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2"sv) return VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2"sv) return VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2"sv) return VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
    if (a_str == "VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2"sv) return VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    if (a_str == "VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2"sv) return VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2"sv) return VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2"sv) return VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
    if (a_str == "VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2"sv) return VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO"sv) return VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2"sv) return VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO"sv) return VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO"sv) return VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES"sv) return VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES"sv) return VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES"sv) return VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES"sv) return VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES"sv) return VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2"sv) return VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2"sv) return VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO"sv) return VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_END_INFO"sv) return VK_STRUCTURE_TYPE_SUBPASS_END_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE"sv) return VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO"sv) return VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO"sv) return VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO"sv) return VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO"sv) return VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO"sv) return VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO"sv) return VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO;
    if (a_str == "VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PRESENT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_STREAM_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_STREAM_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_CU_MODULE_CREATE_INFO_NVX"sv) return VK_STRUCTURE_TYPE_CU_MODULE_CREATE_INFO_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_CU_FUNCTION_CREATE_INFO_NVX"sv) return VK_STRUCTURE_TYPE_CU_FUNCTION_CREATE_INFO_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_CU_LAUNCH_INFO_NVX"sv) return VK_STRUCTURE_TYPE_CU_LAUNCH_INFO_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_VIEW_HANDLE_INFO_NVX"sv) return VK_STRUCTURE_TYPE_IMAGE_VIEW_HANDLE_INFO_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_VIEW_ADDRESS_PROPERTIES_NVX"sv) return VK_STRUCTURE_TYPE_IMAGE_VIEW_ADDRESS_PROPERTIES_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD"sv) return VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_RENDERING_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RENDERING_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDERING_INFO_KHR"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDERING_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_AMD"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_MULTIVIEW_PER_VIEW_ATTRIBUTES_INFO_NVX"sv) return VK_STRUCTURE_TYPE_MULTIVIEW_PER_VIEW_ATTRIBUTES_INFO_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP"sv) return VK_STRUCTURE_TYPE_STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_NV"sv) return VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_NV"sv) return VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_NV"sv) return VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT"sv) return VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VI_SURFACE_CREATE_INFO_NN"sv) return VK_STRUCTURE_TYPE_VI_SURFACE_CREATE_INFO_NN;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_VIEW_ASTC_DECODE_MODE_EXT"sv) return VK_STRUCTURE_TYPE_IMAGE_VIEW_ASTC_DECODE_MODE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_WIN32_HANDLE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_WIN32_HANDLE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_D3D12_FENCE_SUBMIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_D3D12_FENCE_SUBMIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_CONDITIONAL_RENDERING_BEGIN_INFO_EXT"sv) return VK_STRUCTURE_TYPE_CONDITIONAL_RENDERING_BEGIN_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR"sv) return VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT"sv) return VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE"sv) return VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_DEPTH_CLIP_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_DEPTH_CLIP_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_HDR_METADATA_EXT"sv) return VK_STRUCTURE_TYPE_HDR_METADATA_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR"sv) return VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_FENCE_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMPORT_FENCE_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_FENCE_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXPORT_FENCE_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR"sv) return VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_QUERY_SUBMIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_QUERY_SUBMIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACQUIRE_PROFILING_LOCK_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACQUIRE_PROFILING_LOCK_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR"sv) return VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR"sv) return VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_PLANE_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_PLANE_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_MODE_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_MODE_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_PLANE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_PLANE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_PLANE_CAPABILITIES_2_KHR"sv) return VK_STRUCTURE_TYPE_DISPLAY_PLANE_CAPABILITIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK"sv) return VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK;
    if (a_str == "VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK"sv) return VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_USAGE_ANDROID"sv) return VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_USAGE_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID"sv) return VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID"sv) return VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID"sv) return VK_STRUCTURE_TYPE_IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID"sv) return VK_STRUCTURE_TYPE_MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_FORMAT_ANDROID"sv) return VK_STRUCTURE_TYPE_EXTERNAL_FORMAT_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_2_ANDROID"sv) return VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_2_ANDROID;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK_EXT"sv) return VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_INLINE_UNIFORM_BLOCK_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_INLINE_UNIFORM_BLOCK_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR"sv) return VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_VERSION_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_VERSION_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_TO_MEMORY_INFO_KHR"sv) return VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_TO_MEMORY_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_MEMORY_TO_ACCELERATION_STRUCTURE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_COPY_MEMORY_TO_ACCELERATION_STRUCTURE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_INTERFACE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_INTERFACE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT"sv) return VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_2_EXT"sv) return VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_2_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GEOMETRY_NV"sv) return VK_STRUCTURE_TYPE_GEOMETRY_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV"sv) return VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV"sv) return VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV"sv) return VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV"sv) return VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_REPRESENTATIVE_FRAGMENT_TEST_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_REPRESENTATIVE_FRAGMENT_TEST_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT"sv) return VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD"sv) return VK_STRUCTURE_TYPE_PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT"sv) return VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_AMD"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_MEMORY_OVERALLOCATION_CREATE_INFO_AMD"sv) return VK_STRUCTURE_TYPE_DEVICE_MEMORY_OVERALLOCATION_CREATE_INFO_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PRESENT_FRAME_TOKEN_GGP"sv) return VK_STRUCTURE_TYPE_PRESENT_FRAME_TOKEN_GGP;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV"sv) return VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV;
    if (a_str == "VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_INITIALIZE_PERFORMANCE_API_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_INITIALIZE_PERFORMANCE_API_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_MARKER_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_MARKER_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_STREAM_MARKER_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_STREAM_MARKER_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_OVERRIDE_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_OVERRIDE_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DISPLAY_NATIVE_HDR_SURFACE_CAPABILITIES_AMD"sv) return VK_STRUCTURE_TYPE_DISPLAY_NATIVE_HDR_SURFACE_CAPABILITIES_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_SWAPCHAIN_DISPLAY_NATIVE_HDR_CREATE_INFO_AMD"sv) return VK_STRUCTURE_TYPE_SWAPCHAIN_DISPLAY_NATIVE_HDR_CREATE_INFO_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGEPIPE_SURFACE_CREATE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_IMAGEPIPE_SURFACE_CREATE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_FRAGMENT_DENSITY_MAP_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_FRAGMENT_DENSITY_MAP_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_STATE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_STATE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_2_AMD"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_2_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_ATOMIC_INT64_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_ATOMIC_INT64_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_PRIORITY_ALLOCATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_MEMORY_PRIORITY_ALLOCATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_PROTECTED_CAPABILITIES_KHR"sv) return VK_STRUCTURE_TYPE_SURFACE_PROTECTED_CAPABILITIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV"sv) return VK_STRUCTURE_TYPE_FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT"sv) return VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_HEADLESS_SURFACE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_HEADLESS_SURFACE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GRAPHICS_SHADER_GROUP_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_GRAPHICS_SHADER_GROUP_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_TOKEN_NV"sv) return VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_TOKEN_NV;
    if (a_str == "VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GENERATED_COMMANDS_INFO_NV"sv) return VK_STRUCTURE_TYPE_GENERATED_COMMANDS_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV"sv) return VK_STRUCTURE_TYPE_GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT"sv) return VK_STRUCTURE_TYPE_DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PRESENT_ID_KHR"sv) return VK_STRUCTURE_TYPE_PRESENT_ID_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_PRIVATE_DATA_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEVICE_PRIVATE_DATA_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PRIVATE_DATA_SLOT_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PRIVATE_DATA_SLOT_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_CREATION_CACHE_CONTROL_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_CREATION_CACHE_CONTROL_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DIAGNOSTICS_CONFIG_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DIAGNOSTICS_CONFIG_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_DIAGNOSTICS_CONFIG_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_DEVICE_DIAGNOSTICS_CONFIG_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2_KHR"sv) return VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_2_NV"sv) return VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_2_NV;
    if (a_str == "VK_STRUCTURE_TYPE_CHECKPOINT_DATA_2_NV"sv) return VK_STRUCTURE_TYPE_CHECKPOINT_DATA_2_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_UNIFORM_CONTROL_FLOW_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_UNIFORM_CONTROL_FLOW_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV"sv) return VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MOTION_INFO_NV"sv) return VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MOTION_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_2_PLANE_444_FORMATS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_2_PLANE_444_FORMATS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_COMMAND_TRANSFORM_INFO_QCOM"sv) return VK_STRUCTURE_TYPE_COPY_COMMAND_TRANSFORM_INFO_QCOM;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_ROBUSTNESS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_ROBUSTNESS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_BUFFER_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_COPY_BUFFER_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_IMAGE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_COPY_IMAGE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_COPY_IMAGE_TO_BUFFER_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_COPY_IMAGE_TO_BUFFER_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RESOLVE_IMAGE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_RESOLVE_IMAGE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_COPY_2_KHR"sv) return VK_STRUCTURE_TYPE_BUFFER_COPY_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_COPY_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_COPY_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_BLIT_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_BLIT_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2_KHR"sv) return VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_RESOLVE_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_RESOLVE_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RGBA10X6_FORMATS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RGBA10X6_FORMATS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DIRECTFB_SURFACE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DIRECTFB_SURFACE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE;
    if (a_str == "VK_STRUCTURE_TYPE_MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE"sv) return VK_STRUCTURE_TYPE_MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT"sv) return VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT"sv) return VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRM_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRM_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIMITIVE_TOPOLOGY_LIST_RESTART_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIMITIVE_TOPOLOGY_LIST_RESTART_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_3_KHR"sv) return VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_3_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_MEMORY_ZIRCON_HANDLE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_IMPORT_MEMORY_ZIRCON_HANDLE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_ZIRCON_HANDLE_PROPERTIES_FUCHSIA"sv) return VK_STRUCTURE_TYPE_MEMORY_ZIRCON_HANDLE_PROPERTIES_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_GET_ZIRCON_HANDLE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_MEMORY_GET_ZIRCON_HANDLE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CREATE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CREATE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_IMPORT_MEMORY_BUFFER_COLLECTION_FUCHSIA"sv) return VK_STRUCTURE_TYPE_IMPORT_MEMORY_BUFFER_COLLECTION_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_IMAGE_CREATE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_BUFFER_COLLECTION_IMAGE_CREATE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_PROPERTIES_FUCHSIA"sv) return VK_STRUCTURE_TYPE_BUFFER_COLLECTION_PROPERTIES_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_CONSTRAINTS_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_BUFFER_CONSTRAINTS_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_BUFFER_CREATE_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_BUFFER_COLLECTION_BUFFER_CREATE_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_CONSTRAINTS_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_IMAGE_CONSTRAINTS_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_FORMAT_CONSTRAINTS_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_IMAGE_FORMAT_CONSTRAINTS_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_SYSMEM_COLOR_SPACE_FUCHSIA"sv) return VK_STRUCTURE_TYPE_SYSMEM_COLOR_SPACE_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CONSTRAINTS_INFO_FUCHSIA"sv) return VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CONSTRAINTS_INFO_FUCHSIA;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_SHADING_PIPELINE_CREATE_INFO_HUAWEI"sv) return VK_STRUCTURE_TYPE_SUBPASS_SHADING_PIPELINE_CREATE_INFO_HUAWEI;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_FEATURES_HUAWEI"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_FEATURES_HUAWEI;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_PROPERTIES_HUAWEI"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_PROPERTIES_HUAWEI;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_GET_REMOTE_ADDRESS_INFO_NV"sv) return VK_STRUCTURE_TYPE_MEMORY_GET_REMOTE_ADDRESS_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_2_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_2_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SCREEN_SURFACE_CREATE_INFO_QNX"sv) return VK_STRUCTURE_TYPE_SCREEN_SURFACE_CREATE_INFO_QNX;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_COLOR_WRITE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_PIPELINE_COLOR_WRITE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BORDER_COLOR_SWIZZLE_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BORDER_COLOR_SWIZZLE_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_BORDER_COLOR_COMPONENT_MAPPING_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SAMPLER_BORDER_COLOR_COMPONENT_MAPPING_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PAGEABLE_DEVICE_LOCAL_MEMORY_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PAGEABLE_DEVICE_LOCAL_MEMORY_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_BUFFER_MEMORY_REQUIREMENTS_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_BUFFER_MEMORY_REQUIREMENTS_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_IMAGE_MEMORY_REQUIREMENTS_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_IMAGE_MEMORY_REQUIREMENTS_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETER_FEATURES"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETER_FEATURES;
    if (a_str == "VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_NV"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_NV;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT16_INT8_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT16_INT8_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES2_EXT"sv) return VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES2_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2_KHR"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2_KHR"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2_KHR"sv) return VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2_KHR"sv) return VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_END_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SUBPASS_END_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2_KHR"sv) return VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO_KHR"sv) return VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT_EXT"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT_KHR"sv) return VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE_KHR"sv) return VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO_KHR"sv) return VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO_INTEL"sv) return VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO_INTEL;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT_KHR"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT_KHR"sv) return VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_ADDRESS_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_ADDRESS_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_EXT"sv) return VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO_KHR"sv) return VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES_EXT"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_MAX_ENUM"sv) return VK_STRUCTURE_TYPE_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_PROFILE_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_PROFILE_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_CAPABILITIES_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_CAPABILITIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_GET_MEMORY_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_GET_MEMORY_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_BIND_MEMORY_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_BIND_MEMORY_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_SESSION_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_SESSION_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_CODING_CONTROL_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_CODING_CONTROL_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_PROFILES_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_PROFILES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_FORMAT_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_FORMAT_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_CAPABILITIES_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_CAPABILITIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_VCL_FRAME_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_VCL_FRAME_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_DPB_SLOT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_DPB_SLOT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_NALU_SLICE_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_NALU_SLICE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_EMIT_PICTURE_PARAMETERS_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_EMIT_PICTURE_PARAMETERS_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_PROFILE_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_PROFILE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_CAPABILITIES_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_CAPABILITIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_VCL_FRAME_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_VCL_FRAME_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_DPB_SLOT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_DPB_SLOT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_NALU_SLICE_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_NALU_SLICE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_EMIT_PICTURE_PARAMETERS_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_EMIT_PICTURE_PARAMETERS_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_PROFILE_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_PROFILE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_REFERENCE_LISTS_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_REFERENCE_LISTS_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_CAPABILITIES_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_CAPABILITIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PICTURE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PICTURE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_MVC_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_MVC_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PROFILE_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PROFILE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_FEATURES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_FEATURES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_PROPERTIES_KHR"sv) return VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_PROPERTIES_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_CAPABILITIES_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_CAPABILITIES_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PROFILE_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PROFILE_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PICTURE_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PICTURE_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_DPB_SLOT_INFO_EXT"sv) return VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_DPB_SLOT_INFO_EXT;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_INFO_KHR;
    if (a_str == "VK_STRUCTURE_TYPE_VIDEO_ENCODE_RATE_CONTROL_INFO_KHR"sv) return VK_STRUCTURE_TYPE_VIDEO_ENCODE_RATE_CONTROL_INFO_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkStructureType>(0x7FFFFFFF);
}

std::string_view str_from_VkStructureType(VkStructureType e)
{
    switch (e) {
        case VK_STRUCTURE_TYPE_APPLICATION_INFO: return "VK_STRUCTURE_TYPE_APPLICATION_INFO";
        case VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO: return "VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO: return "VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO: return "VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_SUBMIT_INFO: return "VK_STRUCTURE_TYPE_SUBMIT_INFO";
        case VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE: return "VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE";
        case VK_STRUCTURE_TYPE_BIND_SPARSE_INFO: return "VK_STRUCTURE_TYPE_BIND_SPARSE_INFO";
        case VK_STRUCTURE_TYPE_FENCE_CREATE_INFO: return "VK_STRUCTURE_TYPE_FENCE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO: return "VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_EVENT_CREATE_INFO: return "VK_STRUCTURE_TYPE_EVENT_CREATE_INFO";
        case VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO: return "VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO";
        case VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO: return "VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO";
        case VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO: return "VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO";
        case VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO: return "VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO: return "VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO";
        case VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO: return "VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO: return "VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO: return "VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO";
        case VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO: return "VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO: return "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO: return "VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET: return "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET";
        case VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET: return "VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET";
        case VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO: return "VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO";
        case VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO: return "VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO";
        case VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO: return "VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO";
        case VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO: return "VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO";
        case VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER: return "VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER";
        case VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER: return "VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER";
        case VK_STRUCTURE_TYPE_MEMORY_BARRIER: return "VK_STRUCTURE_TYPE_MEMORY_BARRIER";
        case VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO: return "VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO: return "VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES";
        case VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO: return "VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO";
        case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO: return "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES";
        case VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS: return "VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS";
        case VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO: return "VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO";
        case VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO: return "VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO";
        case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO: return "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2: return "VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2";
        case VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2: return "VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2";
        case VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2: return "VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2";
        case VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2: return "VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2";
        case VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2: return "VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2";
        case VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2: return "VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2";
        case VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2: return "VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2";
        case VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2: return "VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2";
        case VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2: return "VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES";
        case VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO: return "VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO";
        case VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO: return "VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO: return "VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES";
        case VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO: return "VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES";
        case VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2: return "VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2";
        case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO: return "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO";
        case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO: return "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO";
        case VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO: return "VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO";
        case VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO: return "VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES";
        case VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES: return "VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO: return "VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO";
        case VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES: return "VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO";
        case VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES: return "VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES";
        case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO: return "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO";
        case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO: return "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO";
        case VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES: return "VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES";
        case VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO: return "VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO: return "VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO";
        case VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES: return "VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT: return "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES";
        case VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO: return "VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO";
        case VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2: return "VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2";
        case VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2: return "VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2";
        case VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2: return "VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2";
        case VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2: return "VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2";
        case VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2: return "VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2";
        case VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO: return "VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO";
        case VK_STRUCTURE_TYPE_SUBPASS_END_INFO: return "VK_STRUCTURE_TYPE_SUBPASS_END_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO: return "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT: return "VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES";
        case VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE: return "VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES";
        case VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO: return "VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES";
        case VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO: return "VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES";
        case VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO: return "VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO";
        case VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO: return "VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO";
        case VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO: return "VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES";
        case VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT: return "VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT";
        case VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT: return "VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES";
        case VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO: return "VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO";
        case VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO: return "VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO";
        case VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO: return "VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO";
        case VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO: return "VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES";
        case VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO: return "VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO";
        case VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO: return "VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO";
        case VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO: return "VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO";
        case VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO: return "VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO";
        case VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PRESENT_INFO_KHR: return "VK_STRUCTURE_TYPE_PRESENT_INFO_KHR";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR";
        case VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR: return "VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR";
        case VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR: return "VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR";
        case VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR";
        case VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD";
        case VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV: return "VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_STREAM_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_STREAM_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_CU_MODULE_CREATE_INFO_NVX: return "VK_STRUCTURE_TYPE_CU_MODULE_CREATE_INFO_NVX";
        case VK_STRUCTURE_TYPE_CU_FUNCTION_CREATE_INFO_NVX: return "VK_STRUCTURE_TYPE_CU_FUNCTION_CREATE_INFO_NVX";
        case VK_STRUCTURE_TYPE_CU_LAUNCH_INFO_NVX: return "VK_STRUCTURE_TYPE_CU_LAUNCH_INFO_NVX";
        case VK_STRUCTURE_TYPE_IMAGE_VIEW_HANDLE_INFO_NVX: return "VK_STRUCTURE_TYPE_IMAGE_VIEW_HANDLE_INFO_NVX";
        case VK_STRUCTURE_TYPE_IMAGE_VIEW_ADDRESS_PROPERTIES_NVX: return "VK_STRUCTURE_TYPE_IMAGE_VIEW_ADDRESS_PROPERTIES_NVX";
        case VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD: return "VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD";
        case VK_STRUCTURE_TYPE_RENDERING_INFO_KHR: return "VK_STRUCTURE_TYPE_RENDERING_INFO_KHR";
        case VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR: return "VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDERING_INFO_KHR: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDERING_INFO_KHR";
        case VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR: return "VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR";
        case VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_INFO_EXT: return "VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_INFO_EXT";
        case VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_AMD: return "VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_AMD";
        case VK_STRUCTURE_TYPE_MULTIVIEW_PER_VIEW_ATTRIBUTES_INFO_NVX: return "VK_STRUCTURE_TYPE_MULTIVIEW_PER_VIEW_ATTRIBUTES_INFO_NVX";
        case VK_STRUCTURE_TYPE_STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP: return "VK_STRUCTURE_TYPE_STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV";
        case VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV: return "VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV";
        case VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_NV: return "VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_NV";
        case VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_NV: return "VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_NV";
        case VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_NV: return "VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_NV";
        case VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT: return "VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT";
        case VK_STRUCTURE_TYPE_VI_SURFACE_CREATE_INFO_NN: return "VK_STRUCTURE_TYPE_VI_SURFACE_CREATE_INFO_NN";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_IMAGE_VIEW_ASTC_DECODE_MODE_EXT: return "VK_STRUCTURE_TYPE_IMAGE_VIEW_ASTC_DECODE_MODE_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_MEMORY_WIN32_HANDLE_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_MEMORY_WIN32_HANDLE_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR: return "VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR";
        case VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR: return "VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR";
        case VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR: return "VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR";
        case VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_D3D12_FENCE_SUBMIT_INFO_KHR: return "VK_STRUCTURE_TYPE_D3D12_FENCE_SUBMIT_INFO_KHR";
        case VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR: return "VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR";
        case VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR: return "VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_CONDITIONAL_RENDERING_BEGIN_INFO_EXT: return "VK_STRUCTURE_TYPE_CONDITIONAL_RENDERING_BEGIN_INFO_EXT";
        case VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR: return "VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT: return "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT";
        case VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT: return "VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT: return "VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT";
        case VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT: return "VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT";
        case VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE: return "VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX";
        case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_DEPTH_CLIP_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_DEPTH_CLIP_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_HDR_METADATA_EXT: return "VK_STRUCTURE_TYPE_HDR_METADATA_EXT";
        case VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR: return "VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR";
        case VK_STRUCTURE_TYPE_IMPORT_FENCE_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_IMPORT_FENCE_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_EXPORT_FENCE_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_EXPORT_FENCE_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR: return "VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR: return "VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR";
        case VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR: return "VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PERFORMANCE_QUERY_SUBMIT_INFO_KHR: return "VK_STRUCTURE_TYPE_PERFORMANCE_QUERY_SUBMIT_INFO_KHR";
        case VK_STRUCTURE_TYPE_ACQUIRE_PROFILING_LOCK_INFO_KHR: return "VK_STRUCTURE_TYPE_ACQUIRE_PROFILING_LOCK_INFO_KHR";
        case VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR: return "VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR";
        case VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR: return "VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR: return "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR";
        case VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR: return "VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_PROPERTIES_2_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_PROPERTIES_2_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_PLANE_PROPERTIES_2_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_PLANE_PROPERTIES_2_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_MODE_PROPERTIES_2_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_MODE_PROPERTIES_2_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_PLANE_INFO_2_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_PLANE_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_DISPLAY_PLANE_CAPABILITIES_2_KHR: return "VK_STRUCTURE_TYPE_DISPLAY_PLANE_CAPABILITIES_2_KHR";
        case VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK: return "VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK";
        case VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK: return "VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK";
        case VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT: return "VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT";
        case VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT: return "VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT";
        case VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_USAGE_ANDROID: return "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_USAGE_ANDROID";
        case VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID: return "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID";
        case VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID: return "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID";
        case VK_STRUCTURE_TYPE_IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID: return "VK_STRUCTURE_TYPE_IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID";
        case VK_STRUCTURE_TYPE_MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID: return "VK_STRUCTURE_TYPE_MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID";
        case VK_STRUCTURE_TYPE_EXTERNAL_FORMAT_ANDROID: return "VK_STRUCTURE_TYPE_EXTERNAL_FORMAT_ANDROID";
        case VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_2_ANDROID: return "VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_2_ANDROID";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK_EXT: return "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK_EXT";
        case VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_INLINE_UNIFORM_BLOCK_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_INLINE_UNIFORM_BLOCK_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT: return "VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT";
        case VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT: return "VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR: return "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_VERSION_INFO_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_VERSION_INFO_KHR";
        case VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_INFO_KHR: return "VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_INFO_KHR";
        case VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_TO_MEMORY_INFO_KHR: return "VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_TO_MEMORY_INFO_KHR";
        case VK_STRUCTURE_TYPE_COPY_MEMORY_TO_ACCELERATION_STRUCTURE_INFO_KHR: return "VK_STRUCTURE_TYPE_COPY_MEMORY_TO_ACCELERATION_STRUCTURE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_INTERFACE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_INTERFACE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT: return "VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT";
        case VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_2_EXT: return "VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_2_EXT";
        case VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_GEOMETRY_NV: return "VK_STRUCTURE_TYPE_GEOMETRY_NV";
        case VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV: return "VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV";
        case VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV: return "VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV";
        case VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV: return "VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV";
        case VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV: return "VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PIPELINE_REPRESENTATIVE_FRAGMENT_TEST_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_REPRESENTATIVE_FRAGMENT_TEST_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT";
        case VK_STRUCTURE_TYPE_FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT: return "VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT";
        case VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD: return "VK_STRUCTURE_TYPE_PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD";
        case VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT: return "VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_AMD: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_AMD";
        case VK_STRUCTURE_TYPE_DEVICE_MEMORY_OVERALLOCATION_CREATE_INFO_AMD: return "VK_STRUCTURE_TYPE_DEVICE_MEMORY_OVERALLOCATION_CREATE_INFO_AMD";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PRESENT_FRAME_TOKEN_GGP: return "VK_STRUCTURE_TYPE_PRESENT_FRAME_TOKEN_GGP";
        case VK_STRUCTURE_TYPE_PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV";
        case VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV: return "VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV";
        case VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL";
        case VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL: return "VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL";
        case VK_STRUCTURE_TYPE_INITIALIZE_PERFORMANCE_API_INFO_INTEL: return "VK_STRUCTURE_TYPE_INITIALIZE_PERFORMANCE_API_INFO_INTEL";
        case VK_STRUCTURE_TYPE_PERFORMANCE_MARKER_INFO_INTEL: return "VK_STRUCTURE_TYPE_PERFORMANCE_MARKER_INFO_INTEL";
        case VK_STRUCTURE_TYPE_PERFORMANCE_STREAM_MARKER_INFO_INTEL: return "VK_STRUCTURE_TYPE_PERFORMANCE_STREAM_MARKER_INFO_INTEL";
        case VK_STRUCTURE_TYPE_PERFORMANCE_OVERRIDE_INFO_INTEL: return "VK_STRUCTURE_TYPE_PERFORMANCE_OVERRIDE_INFO_INTEL";
        case VK_STRUCTURE_TYPE_PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL: return "VK_STRUCTURE_TYPE_PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_DISPLAY_NATIVE_HDR_SURFACE_CAPABILITIES_AMD: return "VK_STRUCTURE_TYPE_DISPLAY_NATIVE_HDR_SURFACE_CAPABILITIES_AMD";
        case VK_STRUCTURE_TYPE_SWAPCHAIN_DISPLAY_NATIVE_HDR_CREATE_INFO_AMD: return "VK_STRUCTURE_TYPE_SWAPCHAIN_DISPLAY_NATIVE_HDR_CREATE_INFO_AMD";
        case VK_STRUCTURE_TYPE_IMAGEPIPE_SURFACE_CREATE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_IMAGEPIPE_SURFACE_CREATE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_RENDER_PASS_FRAGMENT_DENSITY_MAP_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_RENDER_PASS_FRAGMENT_DENSITY_MAP_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR: return "VK_STRUCTURE_TYPE_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_STATE_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_STATE_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_2_AMD: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_2_AMD";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_ATOMIC_INT64_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_ATOMIC_INT64_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_MEMORY_PRIORITY_ALLOCATE_INFO_EXT: return "VK_STRUCTURE_TYPE_MEMORY_PRIORITY_ALLOCATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_SURFACE_PROTECTED_CAPABILITIES_KHR: return "VK_STRUCTURE_TYPE_SURFACE_PROTECTED_CAPABILITIES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT: return "VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV";
        case VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV: return "VK_STRUCTURE_TYPE_FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT: return "VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT";
        case VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT: return "VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT";
        case VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT: return "VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT";
        case VK_STRUCTURE_TYPE_HEADLESS_SURFACE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_HEADLESS_SURFACE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR";
        case VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_GRAPHICS_SHADER_GROUP_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_GRAPHICS_SHADER_GROUP_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_TOKEN_NV: return "VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_TOKEN_NV";
        case VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_GENERATED_COMMANDS_INFO_NV: return "VK_STRUCTURE_TYPE_GENERATED_COMMANDS_INFO_NV";
        case VK_STRUCTURE_TYPE_GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV: return "VK_STRUCTURE_TYPE_GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM";
        case VK_STRUCTURE_TYPE_RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM: return "VK_STRUCTURE_TYPE_RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT: return "VK_STRUCTURE_TYPE_DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_PRESENT_ID_KHR: return "VK_STRUCTURE_TYPE_PRESENT_ID_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_DEVICE_PRIVATE_DATA_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DEVICE_PRIVATE_DATA_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PRIVATE_DATA_SLOT_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PRIVATE_DATA_SLOT_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_CREATION_CACHE_CONTROL_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_CREATION_CACHE_CONTROL_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DIAGNOSTICS_CONFIG_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DIAGNOSTICS_CONFIG_FEATURES_NV";
        case VK_STRUCTURE_TYPE_DEVICE_DIAGNOSTICS_CONFIG_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_DEVICE_DIAGNOSTICS_CONFIG_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR: return "VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR";
        case VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2_KHR: return "VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2_KHR";
        case VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR: return "VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR";
        case VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR: return "VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR";
        case VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR: return "VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR: return "VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR";
        case VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR: return "VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_2_NV: return "VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_2_NV";
        case VK_STRUCTURE_TYPE_CHECKPOINT_DATA_2_NV: return "VK_STRUCTURE_TYPE_CHECKPOINT_DATA_2_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_UNIFORM_CONTROL_FLOW_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_UNIFORM_CONTROL_FLOW_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV: return "VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV";
        case VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MOTION_INFO_NV: return "VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MOTION_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_2_PLANE_444_FORMATS_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_2_PLANE_444_FORMATS_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_COPY_COMMAND_TRANSFORM_INFO_QCOM: return "VK_STRUCTURE_TYPE_COPY_COMMAND_TRANSFORM_INFO_QCOM";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_ROBUSTNESS_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_ROBUSTNESS_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_COPY_BUFFER_INFO_2_KHR: return "VK_STRUCTURE_TYPE_COPY_BUFFER_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_COPY_IMAGE_INFO_2_KHR: return "VK_STRUCTURE_TYPE_COPY_IMAGE_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2_KHR: return "VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_COPY_IMAGE_TO_BUFFER_INFO_2_KHR: return "VK_STRUCTURE_TYPE_COPY_IMAGE_TO_BUFFER_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2_KHR: return "VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_RESOLVE_IMAGE_INFO_2_KHR: return "VK_STRUCTURE_TYPE_RESOLVE_IMAGE_INFO_2_KHR";
        case VK_STRUCTURE_TYPE_BUFFER_COPY_2_KHR: return "VK_STRUCTURE_TYPE_BUFFER_COPY_2_KHR";
        case VK_STRUCTURE_TYPE_IMAGE_COPY_2_KHR: return "VK_STRUCTURE_TYPE_IMAGE_COPY_2_KHR";
        case VK_STRUCTURE_TYPE_IMAGE_BLIT_2_KHR: return "VK_STRUCTURE_TYPE_IMAGE_BLIT_2_KHR";
        case VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2_KHR: return "VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2_KHR";
        case VK_STRUCTURE_TYPE_IMAGE_RESOLVE_2_KHR: return "VK_STRUCTURE_TYPE_IMAGE_RESOLVE_2_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RGBA10X6_FORMATS_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RGBA10X6_FORMATS_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_DIRECTFB_SURFACE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_DIRECTFB_SURFACE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE";
        case VK_STRUCTURE_TYPE_MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE: return "VK_STRUCTURE_TYPE_MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT: return "VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT";
        case VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT: return "VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRM_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRM_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIMITIVE_TOPOLOGY_LIST_RESTART_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIMITIVE_TOPOLOGY_LIST_RESTART_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_3_KHR: return "VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_3_KHR";
        case VK_STRUCTURE_TYPE_IMPORT_MEMORY_ZIRCON_HANDLE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_IMPORT_MEMORY_ZIRCON_HANDLE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_MEMORY_ZIRCON_HANDLE_PROPERTIES_FUCHSIA: return "VK_STRUCTURE_TYPE_MEMORY_ZIRCON_HANDLE_PROPERTIES_FUCHSIA";
        case VK_STRUCTURE_TYPE_MEMORY_GET_ZIRCON_HANDLE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_MEMORY_GET_ZIRCON_HANDLE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_ZIRCON_HANDLE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_SEMAPHORE_GET_ZIRCON_HANDLE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CREATE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CREATE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_IMPORT_MEMORY_BUFFER_COLLECTION_FUCHSIA: return "VK_STRUCTURE_TYPE_IMPORT_MEMORY_BUFFER_COLLECTION_FUCHSIA";
        case VK_STRUCTURE_TYPE_BUFFER_COLLECTION_IMAGE_CREATE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_IMAGE_CREATE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_BUFFER_COLLECTION_PROPERTIES_FUCHSIA: return "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_PROPERTIES_FUCHSIA";
        case VK_STRUCTURE_TYPE_BUFFER_CONSTRAINTS_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_BUFFER_CONSTRAINTS_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_BUFFER_COLLECTION_BUFFER_CREATE_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_BUFFER_CREATE_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_IMAGE_CONSTRAINTS_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_IMAGE_CONSTRAINTS_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_IMAGE_FORMAT_CONSTRAINTS_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_IMAGE_FORMAT_CONSTRAINTS_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_SYSMEM_COLOR_SPACE_FUCHSIA: return "VK_STRUCTURE_TYPE_SYSMEM_COLOR_SPACE_FUCHSIA";
        case VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CONSTRAINTS_INFO_FUCHSIA: return "VK_STRUCTURE_TYPE_BUFFER_COLLECTION_CONSTRAINTS_INFO_FUCHSIA";
        case VK_STRUCTURE_TYPE_SUBPASS_SHADING_PIPELINE_CREATE_INFO_HUAWEI: return "VK_STRUCTURE_TYPE_SUBPASS_SHADING_PIPELINE_CREATE_INFO_HUAWEI";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_FEATURES_HUAWEI: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_FEATURES_HUAWEI";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_PROPERTIES_HUAWEI: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_PROPERTIES_HUAWEI";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI";
        case VK_STRUCTURE_TYPE_MEMORY_GET_REMOTE_ADDRESS_INFO_NV: return "VK_STRUCTURE_TYPE_MEMORY_GET_REMOTE_ADDRESS_INFO_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_2_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_2_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_SCREEN_SURFACE_CREATE_INFO_QNX: return "VK_STRUCTURE_TYPE_SCREEN_SURFACE_CREATE_INFO_QNX";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PIPELINE_COLOR_WRITE_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_PIPELINE_COLOR_WRITE_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_PROPERTIES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_PROPERTIES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BORDER_COLOR_SWIZZLE_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BORDER_COLOR_SWIZZLE_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_SAMPLER_BORDER_COLOR_COMPONENT_MAPPING_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_SAMPLER_BORDER_COLOR_COMPONENT_MAPPING_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PAGEABLE_DEVICE_LOCAL_MEMORY_FEATURES_EXT: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PAGEABLE_DEVICE_LOCAL_MEMORY_FEATURES_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_DEVICE_BUFFER_MEMORY_REQUIREMENTS_KHR: return "VK_STRUCTURE_TYPE_DEVICE_BUFFER_MEMORY_REQUIREMENTS_KHR";
        case VK_STRUCTURE_TYPE_DEVICE_IMAGE_MEMORY_REQUIREMENTS_KHR: return "VK_STRUCTURE_TYPE_DEVICE_IMAGE_MEMORY_REQUIREMENTS_KHR";
        case VK_STRUCTURE_TYPE_MAX_ENUM: return "VK_STRUCTURE_TYPE_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_STRUCTURE_TYPE_VIDEO_PROFILE_KHR: return "VK_STRUCTURE_TYPE_VIDEO_PROFILE_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_CAPABILITIES_KHR: return "VK_STRUCTURE_TYPE_VIDEO_CAPABILITIES_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_KHR: return "VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_GET_MEMORY_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_VIDEO_GET_MEMORY_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_BIND_MEMORY_KHR: return "VK_STRUCTURE_TYPE_VIDEO_BIND_MEMORY_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_SESSION_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_SESSION_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_UPDATE_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_CODING_CONTROL_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_CODING_CONTROL_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_KHR: return "VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR: return "VK_STRUCTURE_TYPE_VIDEO_QUEUE_FAMILY_PROPERTIES_2_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_PROFILES_KHR: return "VK_STRUCTURE_TYPE_VIDEO_PROFILES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_FORMAT_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_VIDEO_FORMAT_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_CAPABILITIES_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_CAPABILITIES_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_VCL_FRAME_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_VCL_FRAME_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_DPB_SLOT_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_DPB_SLOT_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_NALU_SLICE_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_NALU_SLICE_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_EMIT_PICTURE_PARAMETERS_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_EMIT_PICTURE_PARAMETERS_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_PROFILE_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_PROFILE_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_CAPABILITIES_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_CAPABILITIES_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_VCL_FRAME_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_VCL_FRAME_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_DPB_SLOT_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_DPB_SLOT_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_NALU_SLICE_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_NALU_SLICE_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_EMIT_PICTURE_PARAMETERS_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_EMIT_PICTURE_PARAMETERS_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_PROFILE_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_PROFILE_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_REFERENCE_LISTS_EXT: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_H265_REFERENCE_LISTS_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_CAPABILITIES_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_CAPABILITIES_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PICTURE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PICTURE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_MVC_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_MVC_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PROFILE_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PROFILE_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_DPB_SLOT_INFO_EXT";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_FEATURES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_FEATURES_KHR";
        case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_PROPERTIES_KHR: return "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PORTABILITY_SUBSET_PROPERTIES_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_CAPABILITIES_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_CAPABILITIES_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_CREATE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_SESSION_PARAMETERS_ADD_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PROFILE_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PROFILE_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PICTURE_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_PICTURE_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_DPB_SLOT_INFO_EXT: return "VK_STRUCTURE_TYPE_VIDEO_DECODE_H265_DPB_SLOT_INFO_EXT";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_INFO_KHR";
        case VK_STRUCTURE_TYPE_VIDEO_ENCODE_RATE_CONTROL_INFO_KHR: return "VK_STRUCTURE_TYPE_VIDEO_ENCODE_RATE_CONTROL_INFO_KHR";
#endif
        default: break;
    }
    return "Unknown VkStructureType";
}

VkImageLayout str_to_VkImageLayout(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_LAYOUT_UNDEFINED"sv) return VK_IMAGE_LAYOUT_UNDEFINED;
    if (a_str == "VK_IMAGE_LAYOUT_GENERAL"sv) return VK_IMAGE_LAYOUT_GENERAL;
    if (a_str == "VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL"sv) return VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL"sv) return VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL"sv) return VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL"sv) return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL"sv) return VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL"sv) return VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_PREINITIALIZED"sv) return VK_IMAGE_LAYOUT_PREINITIALIZED;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL"sv) return VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL"sv) return VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL"sv) return VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL"sv) return VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL"sv) return VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL"sv) return VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL;
    if (a_str == "VK_IMAGE_LAYOUT_PRESENT_SRC_KHR"sv) return VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR"sv) return VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT"sv) return VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT;
    if (a_str == "VK_IMAGE_LAYOUT_FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_READ_ONLY_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_READ_ONLY_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_SHADING_RATE_OPTIMAL_NV"sv) return VK_IMAGE_LAYOUT_SHADING_RATE_OPTIMAL_NV;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL_KHR"sv) return VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_MAX_ENUM"sv) return VK_IMAGE_LAYOUT_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_IMAGE_LAYOUT_VIDEO_DECODE_DST_KHR"sv) return VK_IMAGE_LAYOUT_VIDEO_DECODE_DST_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_VIDEO_DECODE_SRC_KHR"sv) return VK_IMAGE_LAYOUT_VIDEO_DECODE_SRC_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_VIDEO_DECODE_DPB_KHR"sv) return VK_IMAGE_LAYOUT_VIDEO_DECODE_DPB_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_VIDEO_ENCODE_DST_KHR"sv) return VK_IMAGE_LAYOUT_VIDEO_ENCODE_DST_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_VIDEO_ENCODE_SRC_KHR"sv) return VK_IMAGE_LAYOUT_VIDEO_ENCODE_SRC_KHR;
    if (a_str == "VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR"sv) return VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageLayout>(0x7FFFFFFF);
}

std::string_view str_from_VkImageLayout(VkImageLayout e)
{
    switch (e) {
        case VK_IMAGE_LAYOUT_UNDEFINED: return "VK_IMAGE_LAYOUT_UNDEFINED";
        case VK_IMAGE_LAYOUT_GENERAL: return "VK_IMAGE_LAYOUT_GENERAL";
        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL: return "VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL";
        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL: return "VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL";
        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL: return "VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL";
        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: return "VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL";
        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL: return "VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL";
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL: return "VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL";
        case VK_IMAGE_LAYOUT_PREINITIALIZED: return "VK_IMAGE_LAYOUT_PREINITIALIZED";
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL: return "VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL";
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL: return "VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL";
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL: return "VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL";
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL: return "VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL";
        case VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL: return "VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL";
        case VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL: return "VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL";
        case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR: return "VK_IMAGE_LAYOUT_PRESENT_SRC_KHR";
        case VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR: return "VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR";
        case VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT: return "VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT";
        case VK_IMAGE_LAYOUT_FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR: return "VK_IMAGE_LAYOUT_FRAGMENT_SHADING_RATE_ATTACHMENT_OPTIMAL_KHR";
        case VK_IMAGE_LAYOUT_READ_ONLY_OPTIMAL_KHR: return "VK_IMAGE_LAYOUT_READ_ONLY_OPTIMAL_KHR";
        case VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR: return "VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR";
        case VK_IMAGE_LAYOUT_MAX_ENUM: return "VK_IMAGE_LAYOUT_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_IMAGE_LAYOUT_VIDEO_DECODE_DST_KHR: return "VK_IMAGE_LAYOUT_VIDEO_DECODE_DST_KHR";
        case VK_IMAGE_LAYOUT_VIDEO_DECODE_SRC_KHR: return "VK_IMAGE_LAYOUT_VIDEO_DECODE_SRC_KHR";
        case VK_IMAGE_LAYOUT_VIDEO_DECODE_DPB_KHR: return "VK_IMAGE_LAYOUT_VIDEO_DECODE_DPB_KHR";
        case VK_IMAGE_LAYOUT_VIDEO_ENCODE_DST_KHR: return "VK_IMAGE_LAYOUT_VIDEO_ENCODE_DST_KHR";
        case VK_IMAGE_LAYOUT_VIDEO_ENCODE_SRC_KHR: return "VK_IMAGE_LAYOUT_VIDEO_ENCODE_SRC_KHR";
        case VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR: return "VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR";
#endif
        default: break;
    }
    return "Unknown VkImageLayout";
}

VkObjectType str_to_VkObjectType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_OBJECT_TYPE_UNKNOWN"sv) return VK_OBJECT_TYPE_UNKNOWN;
    if (a_str == "VK_OBJECT_TYPE_INSTANCE"sv) return VK_OBJECT_TYPE_INSTANCE;
    if (a_str == "VK_OBJECT_TYPE_PHYSICAL_DEVICE"sv) return VK_OBJECT_TYPE_PHYSICAL_DEVICE;
    if (a_str == "VK_OBJECT_TYPE_DEVICE"sv) return VK_OBJECT_TYPE_DEVICE;
    if (a_str == "VK_OBJECT_TYPE_QUEUE"sv) return VK_OBJECT_TYPE_QUEUE;
    if (a_str == "VK_OBJECT_TYPE_SEMAPHORE"sv) return VK_OBJECT_TYPE_SEMAPHORE;
    if (a_str == "VK_OBJECT_TYPE_COMMAND_BUFFER"sv) return VK_OBJECT_TYPE_COMMAND_BUFFER;
    if (a_str == "VK_OBJECT_TYPE_FENCE"sv) return VK_OBJECT_TYPE_FENCE;
    if (a_str == "VK_OBJECT_TYPE_DEVICE_MEMORY"sv) return VK_OBJECT_TYPE_DEVICE_MEMORY;
    if (a_str == "VK_OBJECT_TYPE_BUFFER"sv) return VK_OBJECT_TYPE_BUFFER;
    if (a_str == "VK_OBJECT_TYPE_IMAGE"sv) return VK_OBJECT_TYPE_IMAGE;
    if (a_str == "VK_OBJECT_TYPE_EVENT"sv) return VK_OBJECT_TYPE_EVENT;
    if (a_str == "VK_OBJECT_TYPE_QUERY_POOL"sv) return VK_OBJECT_TYPE_QUERY_POOL;
    if (a_str == "VK_OBJECT_TYPE_BUFFER_VIEW"sv) return VK_OBJECT_TYPE_BUFFER_VIEW;
    if (a_str == "VK_OBJECT_TYPE_IMAGE_VIEW"sv) return VK_OBJECT_TYPE_IMAGE_VIEW;
    if (a_str == "VK_OBJECT_TYPE_SHADER_MODULE"sv) return VK_OBJECT_TYPE_SHADER_MODULE;
    if (a_str == "VK_OBJECT_TYPE_PIPELINE_CACHE"sv) return VK_OBJECT_TYPE_PIPELINE_CACHE;
    if (a_str == "VK_OBJECT_TYPE_PIPELINE_LAYOUT"sv) return VK_OBJECT_TYPE_PIPELINE_LAYOUT;
    if (a_str == "VK_OBJECT_TYPE_RENDER_PASS"sv) return VK_OBJECT_TYPE_RENDER_PASS;
    if (a_str == "VK_OBJECT_TYPE_PIPELINE"sv) return VK_OBJECT_TYPE_PIPELINE;
    if (a_str == "VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT"sv) return VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT;
    if (a_str == "VK_OBJECT_TYPE_SAMPLER"sv) return VK_OBJECT_TYPE_SAMPLER;
    if (a_str == "VK_OBJECT_TYPE_DESCRIPTOR_POOL"sv) return VK_OBJECT_TYPE_DESCRIPTOR_POOL;
    if (a_str == "VK_OBJECT_TYPE_DESCRIPTOR_SET"sv) return VK_OBJECT_TYPE_DESCRIPTOR_SET;
    if (a_str == "VK_OBJECT_TYPE_FRAMEBUFFER"sv) return VK_OBJECT_TYPE_FRAMEBUFFER;
    if (a_str == "VK_OBJECT_TYPE_COMMAND_POOL"sv) return VK_OBJECT_TYPE_COMMAND_POOL;
    if (a_str == "VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION"sv) return VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION;
    if (a_str == "VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE"sv) return VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE;
    if (a_str == "VK_OBJECT_TYPE_SURFACE_KHR"sv) return VK_OBJECT_TYPE_SURFACE_KHR;
    if (a_str == "VK_OBJECT_TYPE_SWAPCHAIN_KHR"sv) return VK_OBJECT_TYPE_SWAPCHAIN_KHR;
    if (a_str == "VK_OBJECT_TYPE_DISPLAY_KHR"sv) return VK_OBJECT_TYPE_DISPLAY_KHR;
    if (a_str == "VK_OBJECT_TYPE_DISPLAY_MODE_KHR"sv) return VK_OBJECT_TYPE_DISPLAY_MODE_KHR;
    if (a_str == "VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT"sv) return VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT;
    if (a_str == "VK_OBJECT_TYPE_CU_MODULE_NVX"sv) return VK_OBJECT_TYPE_CU_MODULE_NVX;
    if (a_str == "VK_OBJECT_TYPE_CU_FUNCTION_NVX"sv) return VK_OBJECT_TYPE_CU_FUNCTION_NVX;
    if (a_str == "VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT"sv) return VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT;
    if (a_str == "VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR"sv) return VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR;
    if (a_str == "VK_OBJECT_TYPE_VALIDATION_CACHE_EXT"sv) return VK_OBJECT_TYPE_VALIDATION_CACHE_EXT;
    if (a_str == "VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV"sv) return VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV;
    if (a_str == "VK_OBJECT_TYPE_PERFORMANCE_CONFIGURATION_INTEL"sv) return VK_OBJECT_TYPE_PERFORMANCE_CONFIGURATION_INTEL;
    if (a_str == "VK_OBJECT_TYPE_DEFERRED_OPERATION_KHR"sv) return VK_OBJECT_TYPE_DEFERRED_OPERATION_KHR;
    if (a_str == "VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NV"sv) return VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NV;
    if (a_str == "VK_OBJECT_TYPE_PRIVATE_DATA_SLOT_EXT"sv) return VK_OBJECT_TYPE_PRIVATE_DATA_SLOT_EXT;
    if (a_str == "VK_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA"sv) return VK_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA;
    if (a_str == "VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR"sv) return VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR;
    if (a_str == "VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR"sv) return VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR;
    if (a_str == "VK_OBJECT_TYPE_MAX_ENUM"sv) return VK_OBJECT_TYPE_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_OBJECT_TYPE_VIDEO_SESSION_KHR"sv) return VK_OBJECT_TYPE_VIDEO_SESSION_KHR;
    if (a_str == "VK_OBJECT_TYPE_VIDEO_SESSION_PARAMETERS_KHR"sv) return VK_OBJECT_TYPE_VIDEO_SESSION_PARAMETERS_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkObjectType>(0x7FFFFFFF);
}

std::string_view str_from_VkObjectType(VkObjectType e)
{
    switch (e) {
        case VK_OBJECT_TYPE_UNKNOWN: return "VK_OBJECT_TYPE_UNKNOWN";
        case VK_OBJECT_TYPE_INSTANCE: return "VK_OBJECT_TYPE_INSTANCE";
        case VK_OBJECT_TYPE_PHYSICAL_DEVICE: return "VK_OBJECT_TYPE_PHYSICAL_DEVICE";
        case VK_OBJECT_TYPE_DEVICE: return "VK_OBJECT_TYPE_DEVICE";
        case VK_OBJECT_TYPE_QUEUE: return "VK_OBJECT_TYPE_QUEUE";
        case VK_OBJECT_TYPE_SEMAPHORE: return "VK_OBJECT_TYPE_SEMAPHORE";
        case VK_OBJECT_TYPE_COMMAND_BUFFER: return "VK_OBJECT_TYPE_COMMAND_BUFFER";
        case VK_OBJECT_TYPE_FENCE: return "VK_OBJECT_TYPE_FENCE";
        case VK_OBJECT_TYPE_DEVICE_MEMORY: return "VK_OBJECT_TYPE_DEVICE_MEMORY";
        case VK_OBJECT_TYPE_BUFFER: return "VK_OBJECT_TYPE_BUFFER";
        case VK_OBJECT_TYPE_IMAGE: return "VK_OBJECT_TYPE_IMAGE";
        case VK_OBJECT_TYPE_EVENT: return "VK_OBJECT_TYPE_EVENT";
        case VK_OBJECT_TYPE_QUERY_POOL: return "VK_OBJECT_TYPE_QUERY_POOL";
        case VK_OBJECT_TYPE_BUFFER_VIEW: return "VK_OBJECT_TYPE_BUFFER_VIEW";
        case VK_OBJECT_TYPE_IMAGE_VIEW: return "VK_OBJECT_TYPE_IMAGE_VIEW";
        case VK_OBJECT_TYPE_SHADER_MODULE: return "VK_OBJECT_TYPE_SHADER_MODULE";
        case VK_OBJECT_TYPE_PIPELINE_CACHE: return "VK_OBJECT_TYPE_PIPELINE_CACHE";
        case VK_OBJECT_TYPE_PIPELINE_LAYOUT: return "VK_OBJECT_TYPE_PIPELINE_LAYOUT";
        case VK_OBJECT_TYPE_RENDER_PASS: return "VK_OBJECT_TYPE_RENDER_PASS";
        case VK_OBJECT_TYPE_PIPELINE: return "VK_OBJECT_TYPE_PIPELINE";
        case VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT: return "VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT";
        case VK_OBJECT_TYPE_SAMPLER: return "VK_OBJECT_TYPE_SAMPLER";
        case VK_OBJECT_TYPE_DESCRIPTOR_POOL: return "VK_OBJECT_TYPE_DESCRIPTOR_POOL";
        case VK_OBJECT_TYPE_DESCRIPTOR_SET: return "VK_OBJECT_TYPE_DESCRIPTOR_SET";
        case VK_OBJECT_TYPE_FRAMEBUFFER: return "VK_OBJECT_TYPE_FRAMEBUFFER";
        case VK_OBJECT_TYPE_COMMAND_POOL: return "VK_OBJECT_TYPE_COMMAND_POOL";
        case VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION: return "VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION";
        case VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE: return "VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE";
        case VK_OBJECT_TYPE_SURFACE_KHR: return "VK_OBJECT_TYPE_SURFACE_KHR";
        case VK_OBJECT_TYPE_SWAPCHAIN_KHR: return "VK_OBJECT_TYPE_SWAPCHAIN_KHR";
        case VK_OBJECT_TYPE_DISPLAY_KHR: return "VK_OBJECT_TYPE_DISPLAY_KHR";
        case VK_OBJECT_TYPE_DISPLAY_MODE_KHR: return "VK_OBJECT_TYPE_DISPLAY_MODE_KHR";
        case VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT: return "VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT";
        case VK_OBJECT_TYPE_CU_MODULE_NVX: return "VK_OBJECT_TYPE_CU_MODULE_NVX";
        case VK_OBJECT_TYPE_CU_FUNCTION_NVX: return "VK_OBJECT_TYPE_CU_FUNCTION_NVX";
        case VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT: return "VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT";
        case VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR: return "VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR";
        case VK_OBJECT_TYPE_VALIDATION_CACHE_EXT: return "VK_OBJECT_TYPE_VALIDATION_CACHE_EXT";
        case VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV: return "VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV";
        case VK_OBJECT_TYPE_PERFORMANCE_CONFIGURATION_INTEL: return "VK_OBJECT_TYPE_PERFORMANCE_CONFIGURATION_INTEL";
        case VK_OBJECT_TYPE_DEFERRED_OPERATION_KHR: return "VK_OBJECT_TYPE_DEFERRED_OPERATION_KHR";
        case VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NV: return "VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NV";
        case VK_OBJECT_TYPE_PRIVATE_DATA_SLOT_EXT: return "VK_OBJECT_TYPE_PRIVATE_DATA_SLOT_EXT";
        case VK_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA: return "VK_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA";
        case VK_OBJECT_TYPE_MAX_ENUM: return "VK_OBJECT_TYPE_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_OBJECT_TYPE_VIDEO_SESSION_KHR: return "VK_OBJECT_TYPE_VIDEO_SESSION_KHR";
        case VK_OBJECT_TYPE_VIDEO_SESSION_PARAMETERS_KHR: return "VK_OBJECT_TYPE_VIDEO_SESSION_PARAMETERS_KHR";
#endif
        default: break;
    }
    return "Unknown VkObjectType";
}

VkPipelineCacheHeaderVersion str_to_VkPipelineCacheHeaderVersion(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_CACHE_HEADER_VERSION_ONE"sv) return VK_PIPELINE_CACHE_HEADER_VERSION_ONE;
    if (a_str == "VK_PIPELINE_CACHE_HEADER_VERSION_MAX_ENUM"sv) return VK_PIPELINE_CACHE_HEADER_VERSION_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineCacheHeaderVersion>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineCacheHeaderVersion(VkPipelineCacheHeaderVersion e)
{
    switch (e) {
        case VK_PIPELINE_CACHE_HEADER_VERSION_ONE: return "VK_PIPELINE_CACHE_HEADER_VERSION_ONE";
        case VK_PIPELINE_CACHE_HEADER_VERSION_MAX_ENUM: return "VK_PIPELINE_CACHE_HEADER_VERSION_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPipelineCacheHeaderVersion";
}

VkVendorId str_to_VkVendorId(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VENDOR_ID_VIV"sv) return VK_VENDOR_ID_VIV;
    if (a_str == "VK_VENDOR_ID_VSI"sv) return VK_VENDOR_ID_VSI;
    if (a_str == "VK_VENDOR_ID_KAZAN"sv) return VK_VENDOR_ID_KAZAN;
    if (a_str == "VK_VENDOR_ID_CODEPLAY"sv) return VK_VENDOR_ID_CODEPLAY;
    if (a_str == "VK_VENDOR_ID_MESA"sv) return VK_VENDOR_ID_MESA;
    if (a_str == "VK_VENDOR_ID_POCL"sv) return VK_VENDOR_ID_POCL;
    if (a_str == "VK_VENDOR_ID_MAX_ENUM"sv) return VK_VENDOR_ID_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkVendorId>(0x7FFFFFFF);
}

std::string_view str_from_VkVendorId(VkVendorId e)
{
    switch (e) {
        case VK_VENDOR_ID_VIV: return "VK_VENDOR_ID_VIV";
        case VK_VENDOR_ID_VSI: return "VK_VENDOR_ID_VSI";
        case VK_VENDOR_ID_KAZAN: return "VK_VENDOR_ID_KAZAN";
        case VK_VENDOR_ID_CODEPLAY: return "VK_VENDOR_ID_CODEPLAY";
        case VK_VENDOR_ID_MESA: return "VK_VENDOR_ID_MESA";
        case VK_VENDOR_ID_POCL: return "VK_VENDOR_ID_POCL";
        case VK_VENDOR_ID_MAX_ENUM: return "VK_VENDOR_ID_MAX_ENUM";
        default: break;
    }
    return "Unknown VkVendorId";
}

VkSystemAllocationScope str_to_VkSystemAllocationScope(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SYSTEM_ALLOCATION_SCOPE_COMMAND"sv) return VK_SYSTEM_ALLOCATION_SCOPE_COMMAND;
    if (a_str == "VK_SYSTEM_ALLOCATION_SCOPE_OBJECT"sv) return VK_SYSTEM_ALLOCATION_SCOPE_OBJECT;
    if (a_str == "VK_SYSTEM_ALLOCATION_SCOPE_CACHE"sv) return VK_SYSTEM_ALLOCATION_SCOPE_CACHE;
    if (a_str == "VK_SYSTEM_ALLOCATION_SCOPE_DEVICE"sv) return VK_SYSTEM_ALLOCATION_SCOPE_DEVICE;
    if (a_str == "VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE"sv) return VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE;
    if (a_str == "VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM"sv) return VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSystemAllocationScope>(0x7FFFFFFF);
}

std::string_view str_from_VkSystemAllocationScope(VkSystemAllocationScope e)
{
    switch (e) {
        case VK_SYSTEM_ALLOCATION_SCOPE_COMMAND: return "VK_SYSTEM_ALLOCATION_SCOPE_COMMAND";
        case VK_SYSTEM_ALLOCATION_SCOPE_OBJECT: return "VK_SYSTEM_ALLOCATION_SCOPE_OBJECT";
        case VK_SYSTEM_ALLOCATION_SCOPE_CACHE: return "VK_SYSTEM_ALLOCATION_SCOPE_CACHE";
        case VK_SYSTEM_ALLOCATION_SCOPE_DEVICE: return "VK_SYSTEM_ALLOCATION_SCOPE_DEVICE";
        case VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE: return "VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE";
        case VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM: return "VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSystemAllocationScope";
}

VkInternalAllocationType str_to_VkInternalAllocationType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE"sv) return VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE;
    if (a_str == "VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM"sv) return VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkInternalAllocationType>(0x7FFFFFFF);
}

std::string_view str_from_VkInternalAllocationType(VkInternalAllocationType e)
{
    switch (e) {
        case VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE: return "VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE";
        case VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM: return "VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkInternalAllocationType";
}

VkFormat str_to_VkFormat(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FORMAT_UNDEFINED"sv) return VK_FORMAT_UNDEFINED;
    if (a_str == "VK_FORMAT_R4G4_UNORM_PACK8"sv) return VK_FORMAT_R4G4_UNORM_PACK8;
    if (a_str == "VK_FORMAT_R4G4B4A4_UNORM_PACK16"sv) return VK_FORMAT_R4G4B4A4_UNORM_PACK16;
    if (a_str == "VK_FORMAT_B4G4R4A4_UNORM_PACK16"sv) return VK_FORMAT_B4G4R4A4_UNORM_PACK16;
    if (a_str == "VK_FORMAT_R5G6B5_UNORM_PACK16"sv) return VK_FORMAT_R5G6B5_UNORM_PACK16;
    if (a_str == "VK_FORMAT_B5G6R5_UNORM_PACK16"sv) return VK_FORMAT_B5G6R5_UNORM_PACK16;
    if (a_str == "VK_FORMAT_R5G5B5A1_UNORM_PACK16"sv) return VK_FORMAT_R5G5B5A1_UNORM_PACK16;
    if (a_str == "VK_FORMAT_B5G5R5A1_UNORM_PACK16"sv) return VK_FORMAT_B5G5R5A1_UNORM_PACK16;
    if (a_str == "VK_FORMAT_A1R5G5B5_UNORM_PACK16"sv) return VK_FORMAT_A1R5G5B5_UNORM_PACK16;
    if (a_str == "VK_FORMAT_R8_UNORM"sv) return VK_FORMAT_R8_UNORM;
    if (a_str == "VK_FORMAT_R8_SNORM"sv) return VK_FORMAT_R8_SNORM;
    if (a_str == "VK_FORMAT_R8_USCALED"sv) return VK_FORMAT_R8_USCALED;
    if (a_str == "VK_FORMAT_R8_SSCALED"sv) return VK_FORMAT_R8_SSCALED;
    if (a_str == "VK_FORMAT_R8_UINT"sv) return VK_FORMAT_R8_UINT;
    if (a_str == "VK_FORMAT_R8_SINT"sv) return VK_FORMAT_R8_SINT;
    if (a_str == "VK_FORMAT_R8_SRGB"sv) return VK_FORMAT_R8_SRGB;
    if (a_str == "VK_FORMAT_R8G8_UNORM"sv) return VK_FORMAT_R8G8_UNORM;
    if (a_str == "VK_FORMAT_R8G8_SNORM"sv) return VK_FORMAT_R8G8_SNORM;
    if (a_str == "VK_FORMAT_R8G8_USCALED"sv) return VK_FORMAT_R8G8_USCALED;
    if (a_str == "VK_FORMAT_R8G8_SSCALED"sv) return VK_FORMAT_R8G8_SSCALED;
    if (a_str == "VK_FORMAT_R8G8_UINT"sv) return VK_FORMAT_R8G8_UINT;
    if (a_str == "VK_FORMAT_R8G8_SINT"sv) return VK_FORMAT_R8G8_SINT;
    if (a_str == "VK_FORMAT_R8G8_SRGB"sv) return VK_FORMAT_R8G8_SRGB;
    if (a_str == "VK_FORMAT_R8G8B8_UNORM"sv) return VK_FORMAT_R8G8B8_UNORM;
    if (a_str == "VK_FORMAT_R8G8B8_SNORM"sv) return VK_FORMAT_R8G8B8_SNORM;
    if (a_str == "VK_FORMAT_R8G8B8_USCALED"sv) return VK_FORMAT_R8G8B8_USCALED;
    if (a_str == "VK_FORMAT_R8G8B8_SSCALED"sv) return VK_FORMAT_R8G8B8_SSCALED;
    if (a_str == "VK_FORMAT_R8G8B8_UINT"sv) return VK_FORMAT_R8G8B8_UINT;
    if (a_str == "VK_FORMAT_R8G8B8_SINT"sv) return VK_FORMAT_R8G8B8_SINT;
    if (a_str == "VK_FORMAT_R8G8B8_SRGB"sv) return VK_FORMAT_R8G8B8_SRGB;
    if (a_str == "VK_FORMAT_B8G8R8_UNORM"sv) return VK_FORMAT_B8G8R8_UNORM;
    if (a_str == "VK_FORMAT_B8G8R8_SNORM"sv) return VK_FORMAT_B8G8R8_SNORM;
    if (a_str == "VK_FORMAT_B8G8R8_USCALED"sv) return VK_FORMAT_B8G8R8_USCALED;
    if (a_str == "VK_FORMAT_B8G8R8_SSCALED"sv) return VK_FORMAT_B8G8R8_SSCALED;
    if (a_str == "VK_FORMAT_B8G8R8_UINT"sv) return VK_FORMAT_B8G8R8_UINT;
    if (a_str == "VK_FORMAT_B8G8R8_SINT"sv) return VK_FORMAT_B8G8R8_SINT;
    if (a_str == "VK_FORMAT_B8G8R8_SRGB"sv) return VK_FORMAT_B8G8R8_SRGB;
    if (a_str == "VK_FORMAT_R8G8B8A8_UNORM"sv) return VK_FORMAT_R8G8B8A8_UNORM;
    if (a_str == "VK_FORMAT_R8G8B8A8_SNORM"sv) return VK_FORMAT_R8G8B8A8_SNORM;
    if (a_str == "VK_FORMAT_R8G8B8A8_USCALED"sv) return VK_FORMAT_R8G8B8A8_USCALED;
    if (a_str == "VK_FORMAT_R8G8B8A8_SSCALED"sv) return VK_FORMAT_R8G8B8A8_SSCALED;
    if (a_str == "VK_FORMAT_R8G8B8A8_UINT"sv) return VK_FORMAT_R8G8B8A8_UINT;
    if (a_str == "VK_FORMAT_R8G8B8A8_SINT"sv) return VK_FORMAT_R8G8B8A8_SINT;
    if (a_str == "VK_FORMAT_R8G8B8A8_SRGB"sv) return VK_FORMAT_R8G8B8A8_SRGB;
    if (a_str == "VK_FORMAT_B8G8R8A8_UNORM"sv) return VK_FORMAT_B8G8R8A8_UNORM;
    if (a_str == "VK_FORMAT_B8G8R8A8_SNORM"sv) return VK_FORMAT_B8G8R8A8_SNORM;
    if (a_str == "VK_FORMAT_B8G8R8A8_USCALED"sv) return VK_FORMAT_B8G8R8A8_USCALED;
    if (a_str == "VK_FORMAT_B8G8R8A8_SSCALED"sv) return VK_FORMAT_B8G8R8A8_SSCALED;
    if (a_str == "VK_FORMAT_B8G8R8A8_UINT"sv) return VK_FORMAT_B8G8R8A8_UINT;
    if (a_str == "VK_FORMAT_B8G8R8A8_SINT"sv) return VK_FORMAT_B8G8R8A8_SINT;
    if (a_str == "VK_FORMAT_B8G8R8A8_SRGB"sv) return VK_FORMAT_B8G8R8A8_SRGB;
    if (a_str == "VK_FORMAT_A8B8G8R8_UNORM_PACK32"sv) return VK_FORMAT_A8B8G8R8_UNORM_PACK32;
    if (a_str == "VK_FORMAT_A8B8G8R8_SNORM_PACK32"sv) return VK_FORMAT_A8B8G8R8_SNORM_PACK32;
    if (a_str == "VK_FORMAT_A8B8G8R8_USCALED_PACK32"sv) return VK_FORMAT_A8B8G8R8_USCALED_PACK32;
    if (a_str == "VK_FORMAT_A8B8G8R8_SSCALED_PACK32"sv) return VK_FORMAT_A8B8G8R8_SSCALED_PACK32;
    if (a_str == "VK_FORMAT_A8B8G8R8_UINT_PACK32"sv) return VK_FORMAT_A8B8G8R8_UINT_PACK32;
    if (a_str == "VK_FORMAT_A8B8G8R8_SINT_PACK32"sv) return VK_FORMAT_A8B8G8R8_SINT_PACK32;
    if (a_str == "VK_FORMAT_A8B8G8R8_SRGB_PACK32"sv) return VK_FORMAT_A8B8G8R8_SRGB_PACK32;
    if (a_str == "VK_FORMAT_A2R10G10B10_UNORM_PACK32"sv) return VK_FORMAT_A2R10G10B10_UNORM_PACK32;
    if (a_str == "VK_FORMAT_A2R10G10B10_SNORM_PACK32"sv) return VK_FORMAT_A2R10G10B10_SNORM_PACK32;
    if (a_str == "VK_FORMAT_A2R10G10B10_USCALED_PACK32"sv) return VK_FORMAT_A2R10G10B10_USCALED_PACK32;
    if (a_str == "VK_FORMAT_A2R10G10B10_SSCALED_PACK32"sv) return VK_FORMAT_A2R10G10B10_SSCALED_PACK32;
    if (a_str == "VK_FORMAT_A2R10G10B10_UINT_PACK32"sv) return VK_FORMAT_A2R10G10B10_UINT_PACK32;
    if (a_str == "VK_FORMAT_A2R10G10B10_SINT_PACK32"sv) return VK_FORMAT_A2R10G10B10_SINT_PACK32;
    if (a_str == "VK_FORMAT_A2B10G10R10_UNORM_PACK32"sv) return VK_FORMAT_A2B10G10R10_UNORM_PACK32;
    if (a_str == "VK_FORMAT_A2B10G10R10_SNORM_PACK32"sv) return VK_FORMAT_A2B10G10R10_SNORM_PACK32;
    if (a_str == "VK_FORMAT_A2B10G10R10_USCALED_PACK32"sv) return VK_FORMAT_A2B10G10R10_USCALED_PACK32;
    if (a_str == "VK_FORMAT_A2B10G10R10_SSCALED_PACK32"sv) return VK_FORMAT_A2B10G10R10_SSCALED_PACK32;
    if (a_str == "VK_FORMAT_A2B10G10R10_UINT_PACK32"sv) return VK_FORMAT_A2B10G10R10_UINT_PACK32;
    if (a_str == "VK_FORMAT_A2B10G10R10_SINT_PACK32"sv) return VK_FORMAT_A2B10G10R10_SINT_PACK32;
    if (a_str == "VK_FORMAT_R16_UNORM"sv) return VK_FORMAT_R16_UNORM;
    if (a_str == "VK_FORMAT_R16_SNORM"sv) return VK_FORMAT_R16_SNORM;
    if (a_str == "VK_FORMAT_R16_USCALED"sv) return VK_FORMAT_R16_USCALED;
    if (a_str == "VK_FORMAT_R16_SSCALED"sv) return VK_FORMAT_R16_SSCALED;
    if (a_str == "VK_FORMAT_R16_UINT"sv) return VK_FORMAT_R16_UINT;
    if (a_str == "VK_FORMAT_R16_SINT"sv) return VK_FORMAT_R16_SINT;
    if (a_str == "VK_FORMAT_R16_SFLOAT"sv) return VK_FORMAT_R16_SFLOAT;
    if (a_str == "VK_FORMAT_R16G16_UNORM"sv) return VK_FORMAT_R16G16_UNORM;
    if (a_str == "VK_FORMAT_R16G16_SNORM"sv) return VK_FORMAT_R16G16_SNORM;
    if (a_str == "VK_FORMAT_R16G16_USCALED"sv) return VK_FORMAT_R16G16_USCALED;
    if (a_str == "VK_FORMAT_R16G16_SSCALED"sv) return VK_FORMAT_R16G16_SSCALED;
    if (a_str == "VK_FORMAT_R16G16_UINT"sv) return VK_FORMAT_R16G16_UINT;
    if (a_str == "VK_FORMAT_R16G16_SINT"sv) return VK_FORMAT_R16G16_SINT;
    if (a_str == "VK_FORMAT_R16G16_SFLOAT"sv) return VK_FORMAT_R16G16_SFLOAT;
    if (a_str == "VK_FORMAT_R16G16B16_UNORM"sv) return VK_FORMAT_R16G16B16_UNORM;
    if (a_str == "VK_FORMAT_R16G16B16_SNORM"sv) return VK_FORMAT_R16G16B16_SNORM;
    if (a_str == "VK_FORMAT_R16G16B16_USCALED"sv) return VK_FORMAT_R16G16B16_USCALED;
    if (a_str == "VK_FORMAT_R16G16B16_SSCALED"sv) return VK_FORMAT_R16G16B16_SSCALED;
    if (a_str == "VK_FORMAT_R16G16B16_UINT"sv) return VK_FORMAT_R16G16B16_UINT;
    if (a_str == "VK_FORMAT_R16G16B16_SINT"sv) return VK_FORMAT_R16G16B16_SINT;
    if (a_str == "VK_FORMAT_R16G16B16_SFLOAT"sv) return VK_FORMAT_R16G16B16_SFLOAT;
    if (a_str == "VK_FORMAT_R16G16B16A16_UNORM"sv) return VK_FORMAT_R16G16B16A16_UNORM;
    if (a_str == "VK_FORMAT_R16G16B16A16_SNORM"sv) return VK_FORMAT_R16G16B16A16_SNORM;
    if (a_str == "VK_FORMAT_R16G16B16A16_USCALED"sv) return VK_FORMAT_R16G16B16A16_USCALED;
    if (a_str == "VK_FORMAT_R16G16B16A16_SSCALED"sv) return VK_FORMAT_R16G16B16A16_SSCALED;
    if (a_str == "VK_FORMAT_R16G16B16A16_UINT"sv) return VK_FORMAT_R16G16B16A16_UINT;
    if (a_str == "VK_FORMAT_R16G16B16A16_SINT"sv) return VK_FORMAT_R16G16B16A16_SINT;
    if (a_str == "VK_FORMAT_R16G16B16A16_SFLOAT"sv) return VK_FORMAT_R16G16B16A16_SFLOAT;
    if (a_str == "VK_FORMAT_R32_UINT"sv) return VK_FORMAT_R32_UINT;
    if (a_str == "VK_FORMAT_R32_SINT"sv) return VK_FORMAT_R32_SINT;
    if (a_str == "VK_FORMAT_R32_SFLOAT"sv) return VK_FORMAT_R32_SFLOAT;
    if (a_str == "VK_FORMAT_R32G32_UINT"sv) return VK_FORMAT_R32G32_UINT;
    if (a_str == "VK_FORMAT_R32G32_SINT"sv) return VK_FORMAT_R32G32_SINT;
    if (a_str == "VK_FORMAT_R32G32_SFLOAT"sv) return VK_FORMAT_R32G32_SFLOAT;
    if (a_str == "VK_FORMAT_R32G32B32_UINT"sv) return VK_FORMAT_R32G32B32_UINT;
    if (a_str == "VK_FORMAT_R32G32B32_SINT"sv) return VK_FORMAT_R32G32B32_SINT;
    if (a_str == "VK_FORMAT_R32G32B32_SFLOAT"sv) return VK_FORMAT_R32G32B32_SFLOAT;
    if (a_str == "VK_FORMAT_R32G32B32A32_UINT"sv) return VK_FORMAT_R32G32B32A32_UINT;
    if (a_str == "VK_FORMAT_R32G32B32A32_SINT"sv) return VK_FORMAT_R32G32B32A32_SINT;
    if (a_str == "VK_FORMAT_R32G32B32A32_SFLOAT"sv) return VK_FORMAT_R32G32B32A32_SFLOAT;
    if (a_str == "VK_FORMAT_R64_UINT"sv) return VK_FORMAT_R64_UINT;
    if (a_str == "VK_FORMAT_R64_SINT"sv) return VK_FORMAT_R64_SINT;
    if (a_str == "VK_FORMAT_R64_SFLOAT"sv) return VK_FORMAT_R64_SFLOAT;
    if (a_str == "VK_FORMAT_R64G64_UINT"sv) return VK_FORMAT_R64G64_UINT;
    if (a_str == "VK_FORMAT_R64G64_SINT"sv) return VK_FORMAT_R64G64_SINT;
    if (a_str == "VK_FORMAT_R64G64_SFLOAT"sv) return VK_FORMAT_R64G64_SFLOAT;
    if (a_str == "VK_FORMAT_R64G64B64_UINT"sv) return VK_FORMAT_R64G64B64_UINT;
    if (a_str == "VK_FORMAT_R64G64B64_SINT"sv) return VK_FORMAT_R64G64B64_SINT;
    if (a_str == "VK_FORMAT_R64G64B64_SFLOAT"sv) return VK_FORMAT_R64G64B64_SFLOAT;
    if (a_str == "VK_FORMAT_R64G64B64A64_UINT"sv) return VK_FORMAT_R64G64B64A64_UINT;
    if (a_str == "VK_FORMAT_R64G64B64A64_SINT"sv) return VK_FORMAT_R64G64B64A64_SINT;
    if (a_str == "VK_FORMAT_R64G64B64A64_SFLOAT"sv) return VK_FORMAT_R64G64B64A64_SFLOAT;
    if (a_str == "VK_FORMAT_B10G11R11_UFLOAT_PACK32"sv) return VK_FORMAT_B10G11R11_UFLOAT_PACK32;
    if (a_str == "VK_FORMAT_E5B9G9R9_UFLOAT_PACK32"sv) return VK_FORMAT_E5B9G9R9_UFLOAT_PACK32;
    if (a_str == "VK_FORMAT_D16_UNORM"sv) return VK_FORMAT_D16_UNORM;
    if (a_str == "VK_FORMAT_X8_D24_UNORM_PACK32"sv) return VK_FORMAT_X8_D24_UNORM_PACK32;
    if (a_str == "VK_FORMAT_D32_SFLOAT"sv) return VK_FORMAT_D32_SFLOAT;
    if (a_str == "VK_FORMAT_S8_UINT"sv) return VK_FORMAT_S8_UINT;
    if (a_str == "VK_FORMAT_D16_UNORM_S8_UINT"sv) return VK_FORMAT_D16_UNORM_S8_UINT;
    if (a_str == "VK_FORMAT_D24_UNORM_S8_UINT"sv) return VK_FORMAT_D24_UNORM_S8_UINT;
    if (a_str == "VK_FORMAT_D32_SFLOAT_S8_UINT"sv) return VK_FORMAT_D32_SFLOAT_S8_UINT;
    if (a_str == "VK_FORMAT_BC1_RGB_UNORM_BLOCK"sv) return VK_FORMAT_BC1_RGB_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC1_RGB_SRGB_BLOCK"sv) return VK_FORMAT_BC1_RGB_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_BC1_RGBA_UNORM_BLOCK"sv) return VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC1_RGBA_SRGB_BLOCK"sv) return VK_FORMAT_BC1_RGBA_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_BC2_UNORM_BLOCK"sv) return VK_FORMAT_BC2_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC2_SRGB_BLOCK"sv) return VK_FORMAT_BC2_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_BC3_UNORM_BLOCK"sv) return VK_FORMAT_BC3_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC3_SRGB_BLOCK"sv) return VK_FORMAT_BC3_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_BC4_UNORM_BLOCK"sv) return VK_FORMAT_BC4_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC4_SNORM_BLOCK"sv) return VK_FORMAT_BC4_SNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC5_UNORM_BLOCK"sv) return VK_FORMAT_BC5_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC5_SNORM_BLOCK"sv) return VK_FORMAT_BC5_SNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC6H_UFLOAT_BLOCK"sv) return VK_FORMAT_BC6H_UFLOAT_BLOCK;
    if (a_str == "VK_FORMAT_BC6H_SFLOAT_BLOCK"sv) return VK_FORMAT_BC6H_SFLOAT_BLOCK;
    if (a_str == "VK_FORMAT_BC7_UNORM_BLOCK"sv) return VK_FORMAT_BC7_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_BC7_SRGB_BLOCK"sv) return VK_FORMAT_BC7_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK"sv) return VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK"sv) return VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK"sv) return VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK"sv) return VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK"sv) return VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK"sv) return VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_EAC_R11_UNORM_BLOCK"sv) return VK_FORMAT_EAC_R11_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_EAC_R11_SNORM_BLOCK"sv) return VK_FORMAT_EAC_R11_SNORM_BLOCK;
    if (a_str == "VK_FORMAT_EAC_R11G11_UNORM_BLOCK"sv) return VK_FORMAT_EAC_R11G11_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_EAC_R11G11_SNORM_BLOCK"sv) return VK_FORMAT_EAC_R11G11_SNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_4x4_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_4x4_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_4x4_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_4x4_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_5x4_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_5x4_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_5x4_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_5x4_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_5x5_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_5x5_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_5x5_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_5x5_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_6x5_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_6x5_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_6x5_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_6x5_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_6x6_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_6x6_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_6x6_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_6x6_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_8x5_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_8x5_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_8x5_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_8x5_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_8x6_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_8x6_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_8x6_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_8x6_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_8x8_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_8x8_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_8x8_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_8x8_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x5_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_10x5_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x5_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_10x5_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x6_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_10x6_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x6_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_10x6_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x8_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_10x8_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x8_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_10x8_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x10_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_10x10_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_10x10_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_10x10_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_12x10_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_12x10_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_12x10_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_12x10_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_12x12_UNORM_BLOCK"sv) return VK_FORMAT_ASTC_12x12_UNORM_BLOCK;
    if (a_str == "VK_FORMAT_ASTC_12x12_SRGB_BLOCK"sv) return VK_FORMAT_ASTC_12x12_SRGB_BLOCK;
    if (a_str == "VK_FORMAT_G8B8G8R8_422_UNORM"sv) return VK_FORMAT_G8B8G8R8_422_UNORM;
    if (a_str == "VK_FORMAT_B8G8R8G8_422_UNORM"sv) return VK_FORMAT_B8G8R8G8_422_UNORM;
    if (a_str == "VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM"sv) return VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM;
    if (a_str == "VK_FORMAT_G8_B8R8_2PLANE_420_UNORM"sv) return VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
    if (a_str == "VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM"sv) return VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM;
    if (a_str == "VK_FORMAT_G8_B8R8_2PLANE_422_UNORM"sv) return VK_FORMAT_G8_B8R8_2PLANE_422_UNORM;
    if (a_str == "VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM"sv) return VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM;
    if (a_str == "VK_FORMAT_R10X6_UNORM_PACK16"sv) return VK_FORMAT_R10X6_UNORM_PACK16;
    if (a_str == "VK_FORMAT_R10X6G10X6_UNORM_2PACK16"sv) return VK_FORMAT_R10X6G10X6_UNORM_2PACK16;
    if (a_str == "VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16"sv) return VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16;
    if (a_str == "VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16"sv) return VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16;
    if (a_str == "VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16"sv) return VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16;
    if (a_str == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16"sv) return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16"sv) return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16"sv) return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16"sv) return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16"sv) return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_R12X4_UNORM_PACK16"sv) return VK_FORMAT_R12X4_UNORM_PACK16;
    if (a_str == "VK_FORMAT_R12X4G12X4_UNORM_2PACK16"sv) return VK_FORMAT_R12X4G12X4_UNORM_2PACK16;
    if (a_str == "VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16"sv) return VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16;
    if (a_str == "VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16"sv) return VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16;
    if (a_str == "VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16"sv) return VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16;
    if (a_str == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16"sv) return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16"sv) return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16"sv) return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16"sv) return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16"sv) return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16;
    if (a_str == "VK_FORMAT_G16B16G16R16_422_UNORM"sv) return VK_FORMAT_G16B16G16R16_422_UNORM;
    if (a_str == "VK_FORMAT_B16G16R16G16_422_UNORM"sv) return VK_FORMAT_B16G16R16G16_422_UNORM;
    if (a_str == "VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM"sv) return VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM;
    if (a_str == "VK_FORMAT_G16_B16R16_2PLANE_420_UNORM"sv) return VK_FORMAT_G16_B16R16_2PLANE_420_UNORM;
    if (a_str == "VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM"sv) return VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM;
    if (a_str == "VK_FORMAT_G16_B16R16_2PLANE_422_UNORM"sv) return VK_FORMAT_G16_B16R16_2PLANE_422_UNORM;
    if (a_str == "VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM"sv) return VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM;
    if (a_str == "VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG"sv) return VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG"sv) return VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG"sv) return VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG"sv) return VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG"sv) return VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG"sv) return VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG"sv) return VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG;
    if (a_str == "VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG"sv) return VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG;
    if (a_str == "VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT"sv) return VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT;
    if (a_str == "VK_FORMAT_G8_B8R8_2PLANE_444_UNORM_EXT"sv) return VK_FORMAT_G8_B8R8_2PLANE_444_UNORM_EXT;
    if (a_str == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16_EXT"sv) return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16_EXT;
    if (a_str == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16_EXT"sv) return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16_EXT;
    if (a_str == "VK_FORMAT_G16_B16R16_2PLANE_444_UNORM_EXT"sv) return VK_FORMAT_G16_B16R16_2PLANE_444_UNORM_EXT;
    if (a_str == "VK_FORMAT_A4R4G4B4_UNORM_PACK16_EXT"sv) return VK_FORMAT_A4R4G4B4_UNORM_PACK16_EXT;
    if (a_str == "VK_FORMAT_A4B4G4R4_UNORM_PACK16_EXT"sv) return VK_FORMAT_A4B4G4R4_UNORM_PACK16_EXT;
    if (a_str == "VK_FORMAT_G8B8G8R8_422_UNORM_KHR"sv) return VK_FORMAT_G8B8G8R8_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_B8G8R8G8_422_UNORM_KHR"sv) return VK_FORMAT_B8G8R8G8_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM_KHR"sv) return VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM_KHR;
    if (a_str == "VK_FORMAT_G8_B8R8_2PLANE_420_UNORM_KHR"sv) return VK_FORMAT_G8_B8R8_2PLANE_420_UNORM_KHR;
    if (a_str == "VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM_KHR"sv) return VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_G8_B8R8_2PLANE_422_UNORM_KHR"sv) return VK_FORMAT_G8_B8R8_2PLANE_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM_KHR"sv) return VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM_KHR;
    if (a_str == "VK_FORMAT_R10X6_UNORM_PACK16_KHR"sv) return VK_FORMAT_R10X6_UNORM_PACK16_KHR;
    if (a_str == "VK_FORMAT_R10X6G10X6_UNORM_2PACK16_KHR"sv) return VK_FORMAT_R10X6G10X6_UNORM_2PACK16_KHR;
    if (a_str == "VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16_KHR"sv) return VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16_KHR;
    if (a_str == "VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16_KHR"sv) return VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16_KHR;
    if (a_str == "VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16_KHR"sv) return VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16_KHR;
    if (a_str == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_R12X4_UNORM_PACK16_KHR"sv) return VK_FORMAT_R12X4_UNORM_PACK16_KHR;
    if (a_str == "VK_FORMAT_R12X4G12X4_UNORM_2PACK16_KHR"sv) return VK_FORMAT_R12X4G12X4_UNORM_2PACK16_KHR;
    if (a_str == "VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16_KHR"sv) return VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16_KHR;
    if (a_str == "VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16_KHR"sv) return VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16_KHR;
    if (a_str == "VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16_KHR"sv) return VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16_KHR;
    if (a_str == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16_KHR"sv) return VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16_KHR;
    if (a_str == "VK_FORMAT_G16B16G16R16_422_UNORM_KHR"sv) return VK_FORMAT_G16B16G16R16_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_B16G16R16G16_422_UNORM_KHR"sv) return VK_FORMAT_B16G16R16G16_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM_KHR"sv) return VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM_KHR;
    if (a_str == "VK_FORMAT_G16_B16R16_2PLANE_420_UNORM_KHR"sv) return VK_FORMAT_G16_B16R16_2PLANE_420_UNORM_KHR;
    if (a_str == "VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM_KHR"sv) return VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_G16_B16R16_2PLANE_422_UNORM_KHR"sv) return VK_FORMAT_G16_B16R16_2PLANE_422_UNORM_KHR;
    if (a_str == "VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM_KHR"sv) return VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM_KHR;
    if (a_str == "VK_FORMAT_MAX_ENUM"sv) return VK_FORMAT_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFormat>(0x7FFFFFFF);
}

std::string_view str_from_VkFormat(VkFormat e)
{
    switch (e) {
        case VK_FORMAT_UNDEFINED: return "VK_FORMAT_UNDEFINED";
        case VK_FORMAT_R4G4_UNORM_PACK8: return "VK_FORMAT_R4G4_UNORM_PACK8";
        case VK_FORMAT_R4G4B4A4_UNORM_PACK16: return "VK_FORMAT_R4G4B4A4_UNORM_PACK16";
        case VK_FORMAT_B4G4R4A4_UNORM_PACK16: return "VK_FORMAT_B4G4R4A4_UNORM_PACK16";
        case VK_FORMAT_R5G6B5_UNORM_PACK16: return "VK_FORMAT_R5G6B5_UNORM_PACK16";
        case VK_FORMAT_B5G6R5_UNORM_PACK16: return "VK_FORMAT_B5G6R5_UNORM_PACK16";
        case VK_FORMAT_R5G5B5A1_UNORM_PACK16: return "VK_FORMAT_R5G5B5A1_UNORM_PACK16";
        case VK_FORMAT_B5G5R5A1_UNORM_PACK16: return "VK_FORMAT_B5G5R5A1_UNORM_PACK16";
        case VK_FORMAT_A1R5G5B5_UNORM_PACK16: return "VK_FORMAT_A1R5G5B5_UNORM_PACK16";
        case VK_FORMAT_R8_UNORM: return "VK_FORMAT_R8_UNORM";
        case VK_FORMAT_R8_SNORM: return "VK_FORMAT_R8_SNORM";
        case VK_FORMAT_R8_USCALED: return "VK_FORMAT_R8_USCALED";
        case VK_FORMAT_R8_SSCALED: return "VK_FORMAT_R8_SSCALED";
        case VK_FORMAT_R8_UINT: return "VK_FORMAT_R8_UINT";
        case VK_FORMAT_R8_SINT: return "VK_FORMAT_R8_SINT";
        case VK_FORMAT_R8_SRGB: return "VK_FORMAT_R8_SRGB";
        case VK_FORMAT_R8G8_UNORM: return "VK_FORMAT_R8G8_UNORM";
        case VK_FORMAT_R8G8_SNORM: return "VK_FORMAT_R8G8_SNORM";
        case VK_FORMAT_R8G8_USCALED: return "VK_FORMAT_R8G8_USCALED";
        case VK_FORMAT_R8G8_SSCALED: return "VK_FORMAT_R8G8_SSCALED";
        case VK_FORMAT_R8G8_UINT: return "VK_FORMAT_R8G8_UINT";
        case VK_FORMAT_R8G8_SINT: return "VK_FORMAT_R8G8_SINT";
        case VK_FORMAT_R8G8_SRGB: return "VK_FORMAT_R8G8_SRGB";
        case VK_FORMAT_R8G8B8_UNORM: return "VK_FORMAT_R8G8B8_UNORM";
        case VK_FORMAT_R8G8B8_SNORM: return "VK_FORMAT_R8G8B8_SNORM";
        case VK_FORMAT_R8G8B8_USCALED: return "VK_FORMAT_R8G8B8_USCALED";
        case VK_FORMAT_R8G8B8_SSCALED: return "VK_FORMAT_R8G8B8_SSCALED";
        case VK_FORMAT_R8G8B8_UINT: return "VK_FORMAT_R8G8B8_UINT";
        case VK_FORMAT_R8G8B8_SINT: return "VK_FORMAT_R8G8B8_SINT";
        case VK_FORMAT_R8G8B8_SRGB: return "VK_FORMAT_R8G8B8_SRGB";
        case VK_FORMAT_B8G8R8_UNORM: return "VK_FORMAT_B8G8R8_UNORM";
        case VK_FORMAT_B8G8R8_SNORM: return "VK_FORMAT_B8G8R8_SNORM";
        case VK_FORMAT_B8G8R8_USCALED: return "VK_FORMAT_B8G8R8_USCALED";
        case VK_FORMAT_B8G8R8_SSCALED: return "VK_FORMAT_B8G8R8_SSCALED";
        case VK_FORMAT_B8G8R8_UINT: return "VK_FORMAT_B8G8R8_UINT";
        case VK_FORMAT_B8G8R8_SINT: return "VK_FORMAT_B8G8R8_SINT";
        case VK_FORMAT_B8G8R8_SRGB: return "VK_FORMAT_B8G8R8_SRGB";
        case VK_FORMAT_R8G8B8A8_UNORM: return "VK_FORMAT_R8G8B8A8_UNORM";
        case VK_FORMAT_R8G8B8A8_SNORM: return "VK_FORMAT_R8G8B8A8_SNORM";
        case VK_FORMAT_R8G8B8A8_USCALED: return "VK_FORMAT_R8G8B8A8_USCALED";
        case VK_FORMAT_R8G8B8A8_SSCALED: return "VK_FORMAT_R8G8B8A8_SSCALED";
        case VK_FORMAT_R8G8B8A8_UINT: return "VK_FORMAT_R8G8B8A8_UINT";
        case VK_FORMAT_R8G8B8A8_SINT: return "VK_FORMAT_R8G8B8A8_SINT";
        case VK_FORMAT_R8G8B8A8_SRGB: return "VK_FORMAT_R8G8B8A8_SRGB";
        case VK_FORMAT_B8G8R8A8_UNORM: return "VK_FORMAT_B8G8R8A8_UNORM";
        case VK_FORMAT_B8G8R8A8_SNORM: return "VK_FORMAT_B8G8R8A8_SNORM";
        case VK_FORMAT_B8G8R8A8_USCALED: return "VK_FORMAT_B8G8R8A8_USCALED";
        case VK_FORMAT_B8G8R8A8_SSCALED: return "VK_FORMAT_B8G8R8A8_SSCALED";
        case VK_FORMAT_B8G8R8A8_UINT: return "VK_FORMAT_B8G8R8A8_UINT";
        case VK_FORMAT_B8G8R8A8_SINT: return "VK_FORMAT_B8G8R8A8_SINT";
        case VK_FORMAT_B8G8R8A8_SRGB: return "VK_FORMAT_B8G8R8A8_SRGB";
        case VK_FORMAT_A8B8G8R8_UNORM_PACK32: return "VK_FORMAT_A8B8G8R8_UNORM_PACK32";
        case VK_FORMAT_A8B8G8R8_SNORM_PACK32: return "VK_FORMAT_A8B8G8R8_SNORM_PACK32";
        case VK_FORMAT_A8B8G8R8_USCALED_PACK32: return "VK_FORMAT_A8B8G8R8_USCALED_PACK32";
        case VK_FORMAT_A8B8G8R8_SSCALED_PACK32: return "VK_FORMAT_A8B8G8R8_SSCALED_PACK32";
        case VK_FORMAT_A8B8G8R8_UINT_PACK32: return "VK_FORMAT_A8B8G8R8_UINT_PACK32";
        case VK_FORMAT_A8B8G8R8_SINT_PACK32: return "VK_FORMAT_A8B8G8R8_SINT_PACK32";
        case VK_FORMAT_A8B8G8R8_SRGB_PACK32: return "VK_FORMAT_A8B8G8R8_SRGB_PACK32";
        case VK_FORMAT_A2R10G10B10_UNORM_PACK32: return "VK_FORMAT_A2R10G10B10_UNORM_PACK32";
        case VK_FORMAT_A2R10G10B10_SNORM_PACK32: return "VK_FORMAT_A2R10G10B10_SNORM_PACK32";
        case VK_FORMAT_A2R10G10B10_USCALED_PACK32: return "VK_FORMAT_A2R10G10B10_USCALED_PACK32";
        case VK_FORMAT_A2R10G10B10_SSCALED_PACK32: return "VK_FORMAT_A2R10G10B10_SSCALED_PACK32";
        case VK_FORMAT_A2R10G10B10_UINT_PACK32: return "VK_FORMAT_A2R10G10B10_UINT_PACK32";
        case VK_FORMAT_A2R10G10B10_SINT_PACK32: return "VK_FORMAT_A2R10G10B10_SINT_PACK32";
        case VK_FORMAT_A2B10G10R10_UNORM_PACK32: return "VK_FORMAT_A2B10G10R10_UNORM_PACK32";
        case VK_FORMAT_A2B10G10R10_SNORM_PACK32: return "VK_FORMAT_A2B10G10R10_SNORM_PACK32";
        case VK_FORMAT_A2B10G10R10_USCALED_PACK32: return "VK_FORMAT_A2B10G10R10_USCALED_PACK32";
        case VK_FORMAT_A2B10G10R10_SSCALED_PACK32: return "VK_FORMAT_A2B10G10R10_SSCALED_PACK32";
        case VK_FORMAT_A2B10G10R10_UINT_PACK32: return "VK_FORMAT_A2B10G10R10_UINT_PACK32";
        case VK_FORMAT_A2B10G10R10_SINT_PACK32: return "VK_FORMAT_A2B10G10R10_SINT_PACK32";
        case VK_FORMAT_R16_UNORM: return "VK_FORMAT_R16_UNORM";
        case VK_FORMAT_R16_SNORM: return "VK_FORMAT_R16_SNORM";
        case VK_FORMAT_R16_USCALED: return "VK_FORMAT_R16_USCALED";
        case VK_FORMAT_R16_SSCALED: return "VK_FORMAT_R16_SSCALED";
        case VK_FORMAT_R16_UINT: return "VK_FORMAT_R16_UINT";
        case VK_FORMAT_R16_SINT: return "VK_FORMAT_R16_SINT";
        case VK_FORMAT_R16_SFLOAT: return "VK_FORMAT_R16_SFLOAT";
        case VK_FORMAT_R16G16_UNORM: return "VK_FORMAT_R16G16_UNORM";
        case VK_FORMAT_R16G16_SNORM: return "VK_FORMAT_R16G16_SNORM";
        case VK_FORMAT_R16G16_USCALED: return "VK_FORMAT_R16G16_USCALED";
        case VK_FORMAT_R16G16_SSCALED: return "VK_FORMAT_R16G16_SSCALED";
        case VK_FORMAT_R16G16_UINT: return "VK_FORMAT_R16G16_UINT";
        case VK_FORMAT_R16G16_SINT: return "VK_FORMAT_R16G16_SINT";
        case VK_FORMAT_R16G16_SFLOAT: return "VK_FORMAT_R16G16_SFLOAT";
        case VK_FORMAT_R16G16B16_UNORM: return "VK_FORMAT_R16G16B16_UNORM";
        case VK_FORMAT_R16G16B16_SNORM: return "VK_FORMAT_R16G16B16_SNORM";
        case VK_FORMAT_R16G16B16_USCALED: return "VK_FORMAT_R16G16B16_USCALED";
        case VK_FORMAT_R16G16B16_SSCALED: return "VK_FORMAT_R16G16B16_SSCALED";
        case VK_FORMAT_R16G16B16_UINT: return "VK_FORMAT_R16G16B16_UINT";
        case VK_FORMAT_R16G16B16_SINT: return "VK_FORMAT_R16G16B16_SINT";
        case VK_FORMAT_R16G16B16_SFLOAT: return "VK_FORMAT_R16G16B16_SFLOAT";
        case VK_FORMAT_R16G16B16A16_UNORM: return "VK_FORMAT_R16G16B16A16_UNORM";
        case VK_FORMAT_R16G16B16A16_SNORM: return "VK_FORMAT_R16G16B16A16_SNORM";
        case VK_FORMAT_R16G16B16A16_USCALED: return "VK_FORMAT_R16G16B16A16_USCALED";
        case VK_FORMAT_R16G16B16A16_SSCALED: return "VK_FORMAT_R16G16B16A16_SSCALED";
        case VK_FORMAT_R16G16B16A16_UINT: return "VK_FORMAT_R16G16B16A16_UINT";
        case VK_FORMAT_R16G16B16A16_SINT: return "VK_FORMAT_R16G16B16A16_SINT";
        case VK_FORMAT_R16G16B16A16_SFLOAT: return "VK_FORMAT_R16G16B16A16_SFLOAT";
        case VK_FORMAT_R32_UINT: return "VK_FORMAT_R32_UINT";
        case VK_FORMAT_R32_SINT: return "VK_FORMAT_R32_SINT";
        case VK_FORMAT_R32_SFLOAT: return "VK_FORMAT_R32_SFLOAT";
        case VK_FORMAT_R32G32_UINT: return "VK_FORMAT_R32G32_UINT";
        case VK_FORMAT_R32G32_SINT: return "VK_FORMAT_R32G32_SINT";
        case VK_FORMAT_R32G32_SFLOAT: return "VK_FORMAT_R32G32_SFLOAT";
        case VK_FORMAT_R32G32B32_UINT: return "VK_FORMAT_R32G32B32_UINT";
        case VK_FORMAT_R32G32B32_SINT: return "VK_FORMAT_R32G32B32_SINT";
        case VK_FORMAT_R32G32B32_SFLOAT: return "VK_FORMAT_R32G32B32_SFLOAT";
        case VK_FORMAT_R32G32B32A32_UINT: return "VK_FORMAT_R32G32B32A32_UINT";
        case VK_FORMAT_R32G32B32A32_SINT: return "VK_FORMAT_R32G32B32A32_SINT";
        case VK_FORMAT_R32G32B32A32_SFLOAT: return "VK_FORMAT_R32G32B32A32_SFLOAT";
        case VK_FORMAT_R64_UINT: return "VK_FORMAT_R64_UINT";
        case VK_FORMAT_R64_SINT: return "VK_FORMAT_R64_SINT";
        case VK_FORMAT_R64_SFLOAT: return "VK_FORMAT_R64_SFLOAT";
        case VK_FORMAT_R64G64_UINT: return "VK_FORMAT_R64G64_UINT";
        case VK_FORMAT_R64G64_SINT: return "VK_FORMAT_R64G64_SINT";
        case VK_FORMAT_R64G64_SFLOAT: return "VK_FORMAT_R64G64_SFLOAT";
        case VK_FORMAT_R64G64B64_UINT: return "VK_FORMAT_R64G64B64_UINT";
        case VK_FORMAT_R64G64B64_SINT: return "VK_FORMAT_R64G64B64_SINT";
        case VK_FORMAT_R64G64B64_SFLOAT: return "VK_FORMAT_R64G64B64_SFLOAT";
        case VK_FORMAT_R64G64B64A64_UINT: return "VK_FORMAT_R64G64B64A64_UINT";
        case VK_FORMAT_R64G64B64A64_SINT: return "VK_FORMAT_R64G64B64A64_SINT";
        case VK_FORMAT_R64G64B64A64_SFLOAT: return "VK_FORMAT_R64G64B64A64_SFLOAT";
        case VK_FORMAT_B10G11R11_UFLOAT_PACK32: return "VK_FORMAT_B10G11R11_UFLOAT_PACK32";
        case VK_FORMAT_E5B9G9R9_UFLOAT_PACK32: return "VK_FORMAT_E5B9G9R9_UFLOAT_PACK32";
        case VK_FORMAT_D16_UNORM: return "VK_FORMAT_D16_UNORM";
        case VK_FORMAT_X8_D24_UNORM_PACK32: return "VK_FORMAT_X8_D24_UNORM_PACK32";
        case VK_FORMAT_D32_SFLOAT: return "VK_FORMAT_D32_SFLOAT";
        case VK_FORMAT_S8_UINT: return "VK_FORMAT_S8_UINT";
        case VK_FORMAT_D16_UNORM_S8_UINT: return "VK_FORMAT_D16_UNORM_S8_UINT";
        case VK_FORMAT_D24_UNORM_S8_UINT: return "VK_FORMAT_D24_UNORM_S8_UINT";
        case VK_FORMAT_D32_SFLOAT_S8_UINT: return "VK_FORMAT_D32_SFLOAT_S8_UINT";
        case VK_FORMAT_BC1_RGB_UNORM_BLOCK: return "VK_FORMAT_BC1_RGB_UNORM_BLOCK";
        case VK_FORMAT_BC1_RGB_SRGB_BLOCK: return "VK_FORMAT_BC1_RGB_SRGB_BLOCK";
        case VK_FORMAT_BC1_RGBA_UNORM_BLOCK: return "VK_FORMAT_BC1_RGBA_UNORM_BLOCK";
        case VK_FORMAT_BC1_RGBA_SRGB_BLOCK: return "VK_FORMAT_BC1_RGBA_SRGB_BLOCK";
        case VK_FORMAT_BC2_UNORM_BLOCK: return "VK_FORMAT_BC2_UNORM_BLOCK";
        case VK_FORMAT_BC2_SRGB_BLOCK: return "VK_FORMAT_BC2_SRGB_BLOCK";
        case VK_FORMAT_BC3_UNORM_BLOCK: return "VK_FORMAT_BC3_UNORM_BLOCK";
        case VK_FORMAT_BC3_SRGB_BLOCK: return "VK_FORMAT_BC3_SRGB_BLOCK";
        case VK_FORMAT_BC4_UNORM_BLOCK: return "VK_FORMAT_BC4_UNORM_BLOCK";
        case VK_FORMAT_BC4_SNORM_BLOCK: return "VK_FORMAT_BC4_SNORM_BLOCK";
        case VK_FORMAT_BC5_UNORM_BLOCK: return "VK_FORMAT_BC5_UNORM_BLOCK";
        case VK_FORMAT_BC5_SNORM_BLOCK: return "VK_FORMAT_BC5_SNORM_BLOCK";
        case VK_FORMAT_BC6H_UFLOAT_BLOCK: return "VK_FORMAT_BC6H_UFLOAT_BLOCK";
        case VK_FORMAT_BC6H_SFLOAT_BLOCK: return "VK_FORMAT_BC6H_SFLOAT_BLOCK";
        case VK_FORMAT_BC7_UNORM_BLOCK: return "VK_FORMAT_BC7_UNORM_BLOCK";
        case VK_FORMAT_BC7_SRGB_BLOCK: return "VK_FORMAT_BC7_SRGB_BLOCK";
        case VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK: return "VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK";
        case VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK: return "VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK";
        case VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK";
        case VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK";
        case VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK";
        case VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK: return "VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK";
        case VK_FORMAT_EAC_R11_UNORM_BLOCK: return "VK_FORMAT_EAC_R11_UNORM_BLOCK";
        case VK_FORMAT_EAC_R11_SNORM_BLOCK: return "VK_FORMAT_EAC_R11_SNORM_BLOCK";
        case VK_FORMAT_EAC_R11G11_UNORM_BLOCK: return "VK_FORMAT_EAC_R11G11_UNORM_BLOCK";
        case VK_FORMAT_EAC_R11G11_SNORM_BLOCK: return "VK_FORMAT_EAC_R11G11_SNORM_BLOCK";
        case VK_FORMAT_ASTC_4x4_UNORM_BLOCK: return "VK_FORMAT_ASTC_4x4_UNORM_BLOCK";
        case VK_FORMAT_ASTC_4x4_SRGB_BLOCK: return "VK_FORMAT_ASTC_4x4_SRGB_BLOCK";
        case VK_FORMAT_ASTC_5x4_UNORM_BLOCK: return "VK_FORMAT_ASTC_5x4_UNORM_BLOCK";
        case VK_FORMAT_ASTC_5x4_SRGB_BLOCK: return "VK_FORMAT_ASTC_5x4_SRGB_BLOCK";
        case VK_FORMAT_ASTC_5x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_5x5_UNORM_BLOCK";
        case VK_FORMAT_ASTC_5x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_5x5_SRGB_BLOCK";
        case VK_FORMAT_ASTC_6x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_6x5_UNORM_BLOCK";
        case VK_FORMAT_ASTC_6x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_6x5_SRGB_BLOCK";
        case VK_FORMAT_ASTC_6x6_UNORM_BLOCK: return "VK_FORMAT_ASTC_6x6_UNORM_BLOCK";
        case VK_FORMAT_ASTC_6x6_SRGB_BLOCK: return "VK_FORMAT_ASTC_6x6_SRGB_BLOCK";
        case VK_FORMAT_ASTC_8x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_8x5_UNORM_BLOCK";
        case VK_FORMAT_ASTC_8x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_8x5_SRGB_BLOCK";
        case VK_FORMAT_ASTC_8x6_UNORM_BLOCK: return "VK_FORMAT_ASTC_8x6_UNORM_BLOCK";
        case VK_FORMAT_ASTC_8x6_SRGB_BLOCK: return "VK_FORMAT_ASTC_8x6_SRGB_BLOCK";
        case VK_FORMAT_ASTC_8x8_UNORM_BLOCK: return "VK_FORMAT_ASTC_8x8_UNORM_BLOCK";
        case VK_FORMAT_ASTC_8x8_SRGB_BLOCK: return "VK_FORMAT_ASTC_8x8_SRGB_BLOCK";
        case VK_FORMAT_ASTC_10x5_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x5_UNORM_BLOCK";
        case VK_FORMAT_ASTC_10x5_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x5_SRGB_BLOCK";
        case VK_FORMAT_ASTC_10x6_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x6_UNORM_BLOCK";
        case VK_FORMAT_ASTC_10x6_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x6_SRGB_BLOCK";
        case VK_FORMAT_ASTC_10x8_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x8_UNORM_BLOCK";
        case VK_FORMAT_ASTC_10x8_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x8_SRGB_BLOCK";
        case VK_FORMAT_ASTC_10x10_UNORM_BLOCK: return "VK_FORMAT_ASTC_10x10_UNORM_BLOCK";
        case VK_FORMAT_ASTC_10x10_SRGB_BLOCK: return "VK_FORMAT_ASTC_10x10_SRGB_BLOCK";
        case VK_FORMAT_ASTC_12x10_UNORM_BLOCK: return "VK_FORMAT_ASTC_12x10_UNORM_BLOCK";
        case VK_FORMAT_ASTC_12x10_SRGB_BLOCK: return "VK_FORMAT_ASTC_12x10_SRGB_BLOCK";
        case VK_FORMAT_ASTC_12x12_UNORM_BLOCK: return "VK_FORMAT_ASTC_12x12_UNORM_BLOCK";
        case VK_FORMAT_ASTC_12x12_SRGB_BLOCK: return "VK_FORMAT_ASTC_12x12_SRGB_BLOCK";
        case VK_FORMAT_G8B8G8R8_422_UNORM: return "VK_FORMAT_G8B8G8R8_422_UNORM";
        case VK_FORMAT_B8G8R8G8_422_UNORM: return "VK_FORMAT_B8G8R8G8_422_UNORM";
        case VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM: return "VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM";
        case VK_FORMAT_G8_B8R8_2PLANE_420_UNORM: return "VK_FORMAT_G8_B8R8_2PLANE_420_UNORM";
        case VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM: return "VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM";
        case VK_FORMAT_G8_B8R8_2PLANE_422_UNORM: return "VK_FORMAT_G8_B8R8_2PLANE_422_UNORM";
        case VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM: return "VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM";
        case VK_FORMAT_R10X6_UNORM_PACK16: return "VK_FORMAT_R10X6_UNORM_PACK16";
        case VK_FORMAT_R10X6G10X6_UNORM_2PACK16: return "VK_FORMAT_R10X6G10X6_UNORM_2PACK16";
        case VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16: return "VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16";
        case VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16: return "VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16";
        case VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16: return "VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16";
        case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16";
        case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16";
        case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16";
        case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16";
        case VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16: return "VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16";
        case VK_FORMAT_R12X4_UNORM_PACK16: return "VK_FORMAT_R12X4_UNORM_PACK16";
        case VK_FORMAT_R12X4G12X4_UNORM_2PACK16: return "VK_FORMAT_R12X4G12X4_UNORM_2PACK16";
        case VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16: return "VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16";
        case VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16: return "VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16";
        case VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16: return "VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16";
        case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16";
        case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16";
        case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16";
        case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16";
        case VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16: return "VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16";
        case VK_FORMAT_G16B16G16R16_422_UNORM: return "VK_FORMAT_G16B16G16R16_422_UNORM";
        case VK_FORMAT_B16G16R16G16_422_UNORM: return "VK_FORMAT_B16G16R16G16_422_UNORM";
        case VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM: return "VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM";
        case VK_FORMAT_G16_B16R16_2PLANE_420_UNORM: return "VK_FORMAT_G16_B16R16_2PLANE_420_UNORM";
        case VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM: return "VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM";
        case VK_FORMAT_G16_B16R16_2PLANE_422_UNORM: return "VK_FORMAT_G16_B16R16_2PLANE_422_UNORM";
        case VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM: return "VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM";
        case VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG";
        case VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG";
        case VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG";
        case VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG: return "VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG";
        case VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG";
        case VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG";
        case VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG";
        case VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG: return "VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG";
        case VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT: return "VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT";
        case VK_FORMAT_G8_B8R8_2PLANE_444_UNORM_EXT: return "VK_FORMAT_G8_B8R8_2PLANE_444_UNORM_EXT";
        case VK_FORMAT_G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16_EXT: return "VK_FORMAT_G10X6_B10X6R10X6_2PLANE_444_UNORM_3PACK16_EXT";
        case VK_FORMAT_G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16_EXT: return "VK_FORMAT_G12X4_B12X4R12X4_2PLANE_444_UNORM_3PACK16_EXT";
        case VK_FORMAT_G16_B16R16_2PLANE_444_UNORM_EXT: return "VK_FORMAT_G16_B16R16_2PLANE_444_UNORM_EXT";
        case VK_FORMAT_A4R4G4B4_UNORM_PACK16_EXT: return "VK_FORMAT_A4R4G4B4_UNORM_PACK16_EXT";
        case VK_FORMAT_A4B4G4R4_UNORM_PACK16_EXT: return "VK_FORMAT_A4B4G4R4_UNORM_PACK16_EXT";
        case VK_FORMAT_MAX_ENUM: return "VK_FORMAT_MAX_ENUM";
        default: break;
    }
    return "Unknown VkFormat";
}

VkImageTiling str_to_VkImageTiling(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_TILING_OPTIMAL"sv) return VK_IMAGE_TILING_OPTIMAL;
    if (a_str == "VK_IMAGE_TILING_LINEAR"sv) return VK_IMAGE_TILING_LINEAR;
    if (a_str == "VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT"sv) return VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT;
    if (a_str == "VK_IMAGE_TILING_MAX_ENUM"sv) return VK_IMAGE_TILING_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageTiling>(0x7FFFFFFF);
}

std::string_view str_from_VkImageTiling(VkImageTiling e)
{
    switch (e) {
        case VK_IMAGE_TILING_OPTIMAL: return "VK_IMAGE_TILING_OPTIMAL";
        case VK_IMAGE_TILING_LINEAR: return "VK_IMAGE_TILING_LINEAR";
        case VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT: return "VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT";
        case VK_IMAGE_TILING_MAX_ENUM: return "VK_IMAGE_TILING_MAX_ENUM";
        default: break;
    }
    return "Unknown VkImageTiling";
}

VkImageType str_to_VkImageType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_TYPE_1D"sv) return VK_IMAGE_TYPE_1D;
    if (a_str == "VK_IMAGE_TYPE_2D"sv) return VK_IMAGE_TYPE_2D;
    if (a_str == "VK_IMAGE_TYPE_3D"sv) return VK_IMAGE_TYPE_3D;
    if (a_str == "VK_IMAGE_TYPE_MAX_ENUM"sv) return VK_IMAGE_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageType>(0x7FFFFFFF);
}

std::string_view str_from_VkImageType(VkImageType e)
{
    switch (e) {
        case VK_IMAGE_TYPE_1D: return "VK_IMAGE_TYPE_1D";
        case VK_IMAGE_TYPE_2D: return "VK_IMAGE_TYPE_2D";
        case VK_IMAGE_TYPE_3D: return "VK_IMAGE_TYPE_3D";
        case VK_IMAGE_TYPE_MAX_ENUM: return "VK_IMAGE_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkImageType";
}

VkPhysicalDeviceType str_to_VkPhysicalDeviceType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PHYSICAL_DEVICE_TYPE_OTHER"sv) return VK_PHYSICAL_DEVICE_TYPE_OTHER;
    if (a_str == "VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU"sv) return VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;
    if (a_str == "VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU"sv) return VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
    if (a_str == "VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU"sv) return VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU;
    if (a_str == "VK_PHYSICAL_DEVICE_TYPE_CPU"sv) return VK_PHYSICAL_DEVICE_TYPE_CPU;
    if (a_str == "VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM"sv) return VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPhysicalDeviceType>(0x7FFFFFFF);
}

std::string_view str_from_VkPhysicalDeviceType(VkPhysicalDeviceType e)
{
    switch (e) {
        case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "VK_PHYSICAL_DEVICE_TYPE_OTHER";
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU";
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU";
        case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU";
        case VK_PHYSICAL_DEVICE_TYPE_CPU: return "VK_PHYSICAL_DEVICE_TYPE_CPU";
        case VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM: return "VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPhysicalDeviceType";
}

VkQueryType str_to_VkQueryType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUERY_TYPE_OCCLUSION"sv) return VK_QUERY_TYPE_OCCLUSION;
    if (a_str == "VK_QUERY_TYPE_PIPELINE_STATISTICS"sv) return VK_QUERY_TYPE_PIPELINE_STATISTICS;
    if (a_str == "VK_QUERY_TYPE_TIMESTAMP"sv) return VK_QUERY_TYPE_TIMESTAMP;
    if (a_str == "VK_QUERY_TYPE_TRANSFORM_FEEDBACK_STREAM_EXT"sv) return VK_QUERY_TYPE_TRANSFORM_FEEDBACK_STREAM_EXT;
    if (a_str == "VK_QUERY_TYPE_PERFORMANCE_QUERY_KHR"sv) return VK_QUERY_TYPE_PERFORMANCE_QUERY_KHR;
    if (a_str == "VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR"sv) return VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR;
    if (a_str == "VK_QUERY_TYPE_ACCELERATION_STRUCTURE_SERIALIZATION_SIZE_KHR"sv) return VK_QUERY_TYPE_ACCELERATION_STRUCTURE_SERIALIZATION_SIZE_KHR;
    if (a_str == "VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_NV"sv) return VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_NV;
    if (a_str == "VK_QUERY_TYPE_PERFORMANCE_QUERY_INTEL"sv) return VK_QUERY_TYPE_PERFORMANCE_QUERY_INTEL;
    if (a_str == "VK_QUERY_TYPE_MAX_ENUM"sv) return VK_QUERY_TYPE_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_QUERY_TYPE_RESULT_STATUS_ONLY_KHR"sv) return VK_QUERY_TYPE_RESULT_STATUS_ONLY_KHR;
    if (a_str == "VK_QUERY_TYPE_VIDEO_ENCODE_BITSTREAM_BUFFER_RANGE_KHR"sv) return VK_QUERY_TYPE_VIDEO_ENCODE_BITSTREAM_BUFFER_RANGE_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueryType>(0x7FFFFFFF);
}

std::string_view str_from_VkQueryType(VkQueryType e)
{
    switch (e) {
        case VK_QUERY_TYPE_OCCLUSION: return "VK_QUERY_TYPE_OCCLUSION";
        case VK_QUERY_TYPE_PIPELINE_STATISTICS: return "VK_QUERY_TYPE_PIPELINE_STATISTICS";
        case VK_QUERY_TYPE_TIMESTAMP: return "VK_QUERY_TYPE_TIMESTAMP";
        case VK_QUERY_TYPE_TRANSFORM_FEEDBACK_STREAM_EXT: return "VK_QUERY_TYPE_TRANSFORM_FEEDBACK_STREAM_EXT";
        case VK_QUERY_TYPE_PERFORMANCE_QUERY_KHR: return "VK_QUERY_TYPE_PERFORMANCE_QUERY_KHR";
        case VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR: return "VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR";
        case VK_QUERY_TYPE_ACCELERATION_STRUCTURE_SERIALIZATION_SIZE_KHR: return "VK_QUERY_TYPE_ACCELERATION_STRUCTURE_SERIALIZATION_SIZE_KHR";
        case VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_NV: return "VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_NV";
        case VK_QUERY_TYPE_PERFORMANCE_QUERY_INTEL: return "VK_QUERY_TYPE_PERFORMANCE_QUERY_INTEL";
        case VK_QUERY_TYPE_MAX_ENUM: return "VK_QUERY_TYPE_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_QUERY_TYPE_RESULT_STATUS_ONLY_KHR: return "VK_QUERY_TYPE_RESULT_STATUS_ONLY_KHR";
        case VK_QUERY_TYPE_VIDEO_ENCODE_BITSTREAM_BUFFER_RANGE_KHR: return "VK_QUERY_TYPE_VIDEO_ENCODE_BITSTREAM_BUFFER_RANGE_KHR";
#endif
        default: break;
    }
    return "Unknown VkQueryType";
}

VkSharingMode str_to_VkSharingMode(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHARING_MODE_EXCLUSIVE"sv) return VK_SHARING_MODE_EXCLUSIVE;
    if (a_str == "VK_SHARING_MODE_CONCURRENT"sv) return VK_SHARING_MODE_CONCURRENT;
    if (a_str == "VK_SHARING_MODE_MAX_ENUM"sv) return VK_SHARING_MODE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSharingMode>(0x7FFFFFFF);
}

std::string_view str_from_VkSharingMode(VkSharingMode e)
{
    switch (e) {
        case VK_SHARING_MODE_EXCLUSIVE: return "VK_SHARING_MODE_EXCLUSIVE";
        case VK_SHARING_MODE_CONCURRENT: return "VK_SHARING_MODE_CONCURRENT";
        case VK_SHARING_MODE_MAX_ENUM: return "VK_SHARING_MODE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSharingMode";
}

VkComponentSwizzle str_to_VkComponentSwizzle(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMPONENT_SWIZZLE_IDENTITY"sv) return VK_COMPONENT_SWIZZLE_IDENTITY;
    if (a_str == "VK_COMPONENT_SWIZZLE_ZERO"sv) return VK_COMPONENT_SWIZZLE_ZERO;
    if (a_str == "VK_COMPONENT_SWIZZLE_ONE"sv) return VK_COMPONENT_SWIZZLE_ONE;
    if (a_str == "VK_COMPONENT_SWIZZLE_R"sv) return VK_COMPONENT_SWIZZLE_R;
    if (a_str == "VK_COMPONENT_SWIZZLE_G"sv) return VK_COMPONENT_SWIZZLE_G;
    if (a_str == "VK_COMPONENT_SWIZZLE_B"sv) return VK_COMPONENT_SWIZZLE_B;
    if (a_str == "VK_COMPONENT_SWIZZLE_A"sv) return VK_COMPONENT_SWIZZLE_A;
    if (a_str == "VK_COMPONENT_SWIZZLE_MAX_ENUM"sv) return VK_COMPONENT_SWIZZLE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkComponentSwizzle>(0x7FFFFFFF);
}

std::string_view str_from_VkComponentSwizzle(VkComponentSwizzle e)
{
    switch (e) {
        case VK_COMPONENT_SWIZZLE_IDENTITY: return "VK_COMPONENT_SWIZZLE_IDENTITY";
        case VK_COMPONENT_SWIZZLE_ZERO: return "VK_COMPONENT_SWIZZLE_ZERO";
        case VK_COMPONENT_SWIZZLE_ONE: return "VK_COMPONENT_SWIZZLE_ONE";
        case VK_COMPONENT_SWIZZLE_R: return "VK_COMPONENT_SWIZZLE_R";
        case VK_COMPONENT_SWIZZLE_G: return "VK_COMPONENT_SWIZZLE_G";
        case VK_COMPONENT_SWIZZLE_B: return "VK_COMPONENT_SWIZZLE_B";
        case VK_COMPONENT_SWIZZLE_A: return "VK_COMPONENT_SWIZZLE_A";
        case VK_COMPONENT_SWIZZLE_MAX_ENUM: return "VK_COMPONENT_SWIZZLE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkComponentSwizzle";
}

VkImageViewType str_to_VkImageViewType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_VIEW_TYPE_1D"sv) return VK_IMAGE_VIEW_TYPE_1D;
    if (a_str == "VK_IMAGE_VIEW_TYPE_2D"sv) return VK_IMAGE_VIEW_TYPE_2D;
    if (a_str == "VK_IMAGE_VIEW_TYPE_3D"sv) return VK_IMAGE_VIEW_TYPE_3D;
    if (a_str == "VK_IMAGE_VIEW_TYPE_CUBE"sv) return VK_IMAGE_VIEW_TYPE_CUBE;
    if (a_str == "VK_IMAGE_VIEW_TYPE_1D_ARRAY"sv) return VK_IMAGE_VIEW_TYPE_1D_ARRAY;
    if (a_str == "VK_IMAGE_VIEW_TYPE_2D_ARRAY"sv) return VK_IMAGE_VIEW_TYPE_2D_ARRAY;
    if (a_str == "VK_IMAGE_VIEW_TYPE_CUBE_ARRAY"sv) return VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
    if (a_str == "VK_IMAGE_VIEW_TYPE_MAX_ENUM"sv) return VK_IMAGE_VIEW_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageViewType>(0x7FFFFFFF);
}

std::string_view str_from_VkImageViewType(VkImageViewType e)
{
    switch (e) {
        case VK_IMAGE_VIEW_TYPE_1D: return "VK_IMAGE_VIEW_TYPE_1D";
        case VK_IMAGE_VIEW_TYPE_2D: return "VK_IMAGE_VIEW_TYPE_2D";
        case VK_IMAGE_VIEW_TYPE_3D: return "VK_IMAGE_VIEW_TYPE_3D";
        case VK_IMAGE_VIEW_TYPE_CUBE: return "VK_IMAGE_VIEW_TYPE_CUBE";
        case VK_IMAGE_VIEW_TYPE_1D_ARRAY: return "VK_IMAGE_VIEW_TYPE_1D_ARRAY";
        case VK_IMAGE_VIEW_TYPE_2D_ARRAY: return "VK_IMAGE_VIEW_TYPE_2D_ARRAY";
        case VK_IMAGE_VIEW_TYPE_CUBE_ARRAY: return "VK_IMAGE_VIEW_TYPE_CUBE_ARRAY";
        case VK_IMAGE_VIEW_TYPE_MAX_ENUM: return "VK_IMAGE_VIEW_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkImageViewType";
}

VkBlendFactor str_to_VkBlendFactor(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BLEND_FACTOR_ZERO"sv) return VK_BLEND_FACTOR_ZERO;
    if (a_str == "VK_BLEND_FACTOR_ONE"sv) return VK_BLEND_FACTOR_ONE;
    if (a_str == "VK_BLEND_FACTOR_SRC_COLOR"sv) return VK_BLEND_FACTOR_SRC_COLOR;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR"sv) return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
    if (a_str == "VK_BLEND_FACTOR_DST_COLOR"sv) return VK_BLEND_FACTOR_DST_COLOR;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR"sv) return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
    if (a_str == "VK_BLEND_FACTOR_SRC_ALPHA"sv) return VK_BLEND_FACTOR_SRC_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA"sv) return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_DST_ALPHA"sv) return VK_BLEND_FACTOR_DST_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA"sv) return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_CONSTANT_COLOR"sv) return VK_BLEND_FACTOR_CONSTANT_COLOR;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR"sv) return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
    if (a_str == "VK_BLEND_FACTOR_CONSTANT_ALPHA"sv) return VK_BLEND_FACTOR_CONSTANT_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA"sv) return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_SRC_ALPHA_SATURATE"sv) return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
    if (a_str == "VK_BLEND_FACTOR_SRC1_COLOR"sv) return VK_BLEND_FACTOR_SRC1_COLOR;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR"sv) return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
    if (a_str == "VK_BLEND_FACTOR_SRC1_ALPHA"sv) return VK_BLEND_FACTOR_SRC1_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA"sv) return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
    if (a_str == "VK_BLEND_FACTOR_MAX_ENUM"sv) return VK_BLEND_FACTOR_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBlendFactor>(0x7FFFFFFF);
}

std::string_view str_from_VkBlendFactor(VkBlendFactor e)
{
    switch (e) {
        case VK_BLEND_FACTOR_ZERO: return "VK_BLEND_FACTOR_ZERO";
        case VK_BLEND_FACTOR_ONE: return "VK_BLEND_FACTOR_ONE";
        case VK_BLEND_FACTOR_SRC_COLOR: return "VK_BLEND_FACTOR_SRC_COLOR";
        case VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR: return "VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR";
        case VK_BLEND_FACTOR_DST_COLOR: return "VK_BLEND_FACTOR_DST_COLOR";
        case VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR: return "VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR";
        case VK_BLEND_FACTOR_SRC_ALPHA: return "VK_BLEND_FACTOR_SRC_ALPHA";
        case VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA: return "VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA";
        case VK_BLEND_FACTOR_DST_ALPHA: return "VK_BLEND_FACTOR_DST_ALPHA";
        case VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA: return "VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA";
        case VK_BLEND_FACTOR_CONSTANT_COLOR: return "VK_BLEND_FACTOR_CONSTANT_COLOR";
        case VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR: return "VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR";
        case VK_BLEND_FACTOR_CONSTANT_ALPHA: return "VK_BLEND_FACTOR_CONSTANT_ALPHA";
        case VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA: return "VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA";
        case VK_BLEND_FACTOR_SRC_ALPHA_SATURATE: return "VK_BLEND_FACTOR_SRC_ALPHA_SATURATE";
        case VK_BLEND_FACTOR_SRC1_COLOR: return "VK_BLEND_FACTOR_SRC1_COLOR";
        case VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR: return "VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR";
        case VK_BLEND_FACTOR_SRC1_ALPHA: return "VK_BLEND_FACTOR_SRC1_ALPHA";
        case VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA: return "VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA";
        case VK_BLEND_FACTOR_MAX_ENUM: return "VK_BLEND_FACTOR_MAX_ENUM";
        default: break;
    }
    return "Unknown VkBlendFactor";
}

VkBlendOp str_to_VkBlendOp(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BLEND_OP_ADD"sv) return VK_BLEND_OP_ADD;
    if (a_str == "VK_BLEND_OP_SUBTRACT"sv) return VK_BLEND_OP_SUBTRACT;
    if (a_str == "VK_BLEND_OP_REVERSE_SUBTRACT"sv) return VK_BLEND_OP_REVERSE_SUBTRACT;
    if (a_str == "VK_BLEND_OP_MIN"sv) return VK_BLEND_OP_MIN;
    if (a_str == "VK_BLEND_OP_MAX"sv) return VK_BLEND_OP_MAX;
    if (a_str == "VK_BLEND_OP_ZERO_EXT"sv) return VK_BLEND_OP_ZERO_EXT;
    if (a_str == "VK_BLEND_OP_SRC_EXT"sv) return VK_BLEND_OP_SRC_EXT;
    if (a_str == "VK_BLEND_OP_DST_EXT"sv) return VK_BLEND_OP_DST_EXT;
    if (a_str == "VK_BLEND_OP_SRC_OVER_EXT"sv) return VK_BLEND_OP_SRC_OVER_EXT;
    if (a_str == "VK_BLEND_OP_DST_OVER_EXT"sv) return VK_BLEND_OP_DST_OVER_EXT;
    if (a_str == "VK_BLEND_OP_SRC_IN_EXT"sv) return VK_BLEND_OP_SRC_IN_EXT;
    if (a_str == "VK_BLEND_OP_DST_IN_EXT"sv) return VK_BLEND_OP_DST_IN_EXT;
    if (a_str == "VK_BLEND_OP_SRC_OUT_EXT"sv) return VK_BLEND_OP_SRC_OUT_EXT;
    if (a_str == "VK_BLEND_OP_DST_OUT_EXT"sv) return VK_BLEND_OP_DST_OUT_EXT;
    if (a_str == "VK_BLEND_OP_SRC_ATOP_EXT"sv) return VK_BLEND_OP_SRC_ATOP_EXT;
    if (a_str == "VK_BLEND_OP_DST_ATOP_EXT"sv) return VK_BLEND_OP_DST_ATOP_EXT;
    if (a_str == "VK_BLEND_OP_XOR_EXT"sv) return VK_BLEND_OP_XOR_EXT;
    if (a_str == "VK_BLEND_OP_MULTIPLY_EXT"sv) return VK_BLEND_OP_MULTIPLY_EXT;
    if (a_str == "VK_BLEND_OP_SCREEN_EXT"sv) return VK_BLEND_OP_SCREEN_EXT;
    if (a_str == "VK_BLEND_OP_OVERLAY_EXT"sv) return VK_BLEND_OP_OVERLAY_EXT;
    if (a_str == "VK_BLEND_OP_DARKEN_EXT"sv) return VK_BLEND_OP_DARKEN_EXT;
    if (a_str == "VK_BLEND_OP_LIGHTEN_EXT"sv) return VK_BLEND_OP_LIGHTEN_EXT;
    if (a_str == "VK_BLEND_OP_COLORDODGE_EXT"sv) return VK_BLEND_OP_COLORDODGE_EXT;
    if (a_str == "VK_BLEND_OP_COLORBURN_EXT"sv) return VK_BLEND_OP_COLORBURN_EXT;
    if (a_str == "VK_BLEND_OP_HARDLIGHT_EXT"sv) return VK_BLEND_OP_HARDLIGHT_EXT;
    if (a_str == "VK_BLEND_OP_SOFTLIGHT_EXT"sv) return VK_BLEND_OP_SOFTLIGHT_EXT;
    if (a_str == "VK_BLEND_OP_DIFFERENCE_EXT"sv) return VK_BLEND_OP_DIFFERENCE_EXT;
    if (a_str == "VK_BLEND_OP_EXCLUSION_EXT"sv) return VK_BLEND_OP_EXCLUSION_EXT;
    if (a_str == "VK_BLEND_OP_INVERT_EXT"sv) return VK_BLEND_OP_INVERT_EXT;
    if (a_str == "VK_BLEND_OP_INVERT_RGB_EXT"sv) return VK_BLEND_OP_INVERT_RGB_EXT;
    if (a_str == "VK_BLEND_OP_LINEARDODGE_EXT"sv) return VK_BLEND_OP_LINEARDODGE_EXT;
    if (a_str == "VK_BLEND_OP_LINEARBURN_EXT"sv) return VK_BLEND_OP_LINEARBURN_EXT;
    if (a_str == "VK_BLEND_OP_VIVIDLIGHT_EXT"sv) return VK_BLEND_OP_VIVIDLIGHT_EXT;
    if (a_str == "VK_BLEND_OP_LINEARLIGHT_EXT"sv) return VK_BLEND_OP_LINEARLIGHT_EXT;
    if (a_str == "VK_BLEND_OP_PINLIGHT_EXT"sv) return VK_BLEND_OP_PINLIGHT_EXT;
    if (a_str == "VK_BLEND_OP_HARDMIX_EXT"sv) return VK_BLEND_OP_HARDMIX_EXT;
    if (a_str == "VK_BLEND_OP_HSL_HUE_EXT"sv) return VK_BLEND_OP_HSL_HUE_EXT;
    if (a_str == "VK_BLEND_OP_HSL_SATURATION_EXT"sv) return VK_BLEND_OP_HSL_SATURATION_EXT;
    if (a_str == "VK_BLEND_OP_HSL_COLOR_EXT"sv) return VK_BLEND_OP_HSL_COLOR_EXT;
    if (a_str == "VK_BLEND_OP_HSL_LUMINOSITY_EXT"sv) return VK_BLEND_OP_HSL_LUMINOSITY_EXT;
    if (a_str == "VK_BLEND_OP_PLUS_EXT"sv) return VK_BLEND_OP_PLUS_EXT;
    if (a_str == "VK_BLEND_OP_PLUS_CLAMPED_EXT"sv) return VK_BLEND_OP_PLUS_CLAMPED_EXT;
    if (a_str == "VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT"sv) return VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT;
    if (a_str == "VK_BLEND_OP_PLUS_DARKER_EXT"sv) return VK_BLEND_OP_PLUS_DARKER_EXT;
    if (a_str == "VK_BLEND_OP_MINUS_EXT"sv) return VK_BLEND_OP_MINUS_EXT;
    if (a_str == "VK_BLEND_OP_MINUS_CLAMPED_EXT"sv) return VK_BLEND_OP_MINUS_CLAMPED_EXT;
    if (a_str == "VK_BLEND_OP_CONTRAST_EXT"sv) return VK_BLEND_OP_CONTRAST_EXT;
    if (a_str == "VK_BLEND_OP_INVERT_OVG_EXT"sv) return VK_BLEND_OP_INVERT_OVG_EXT;
    if (a_str == "VK_BLEND_OP_RED_EXT"sv) return VK_BLEND_OP_RED_EXT;
    if (a_str == "VK_BLEND_OP_GREEN_EXT"sv) return VK_BLEND_OP_GREEN_EXT;
    if (a_str == "VK_BLEND_OP_BLUE_EXT"sv) return VK_BLEND_OP_BLUE_EXT;
    if (a_str == "VK_BLEND_OP_MAX_ENUM"sv) return VK_BLEND_OP_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBlendOp>(0x7FFFFFFF);
}

std::string_view str_from_VkBlendOp(VkBlendOp e)
{
    switch (e) {
        case VK_BLEND_OP_ADD: return "VK_BLEND_OP_ADD";
        case VK_BLEND_OP_SUBTRACT: return "VK_BLEND_OP_SUBTRACT";
        case VK_BLEND_OP_REVERSE_SUBTRACT: return "VK_BLEND_OP_REVERSE_SUBTRACT";
        case VK_BLEND_OP_MIN: return "VK_BLEND_OP_MIN";
        case VK_BLEND_OP_MAX: return "VK_BLEND_OP_MAX";
        case VK_BLEND_OP_ZERO_EXT: return "VK_BLEND_OP_ZERO_EXT";
        case VK_BLEND_OP_SRC_EXT: return "VK_BLEND_OP_SRC_EXT";
        case VK_BLEND_OP_DST_EXT: return "VK_BLEND_OP_DST_EXT";
        case VK_BLEND_OP_SRC_OVER_EXT: return "VK_BLEND_OP_SRC_OVER_EXT";
        case VK_BLEND_OP_DST_OVER_EXT: return "VK_BLEND_OP_DST_OVER_EXT";
        case VK_BLEND_OP_SRC_IN_EXT: return "VK_BLEND_OP_SRC_IN_EXT";
        case VK_BLEND_OP_DST_IN_EXT: return "VK_BLEND_OP_DST_IN_EXT";
        case VK_BLEND_OP_SRC_OUT_EXT: return "VK_BLEND_OP_SRC_OUT_EXT";
        case VK_BLEND_OP_DST_OUT_EXT: return "VK_BLEND_OP_DST_OUT_EXT";
        case VK_BLEND_OP_SRC_ATOP_EXT: return "VK_BLEND_OP_SRC_ATOP_EXT";
        case VK_BLEND_OP_DST_ATOP_EXT: return "VK_BLEND_OP_DST_ATOP_EXT";
        case VK_BLEND_OP_XOR_EXT: return "VK_BLEND_OP_XOR_EXT";
        case VK_BLEND_OP_MULTIPLY_EXT: return "VK_BLEND_OP_MULTIPLY_EXT";
        case VK_BLEND_OP_SCREEN_EXT: return "VK_BLEND_OP_SCREEN_EXT";
        case VK_BLEND_OP_OVERLAY_EXT: return "VK_BLEND_OP_OVERLAY_EXT";
        case VK_BLEND_OP_DARKEN_EXT: return "VK_BLEND_OP_DARKEN_EXT";
        case VK_BLEND_OP_LIGHTEN_EXT: return "VK_BLEND_OP_LIGHTEN_EXT";
        case VK_BLEND_OP_COLORDODGE_EXT: return "VK_BLEND_OP_COLORDODGE_EXT";
        case VK_BLEND_OP_COLORBURN_EXT: return "VK_BLEND_OP_COLORBURN_EXT";
        case VK_BLEND_OP_HARDLIGHT_EXT: return "VK_BLEND_OP_HARDLIGHT_EXT";
        case VK_BLEND_OP_SOFTLIGHT_EXT: return "VK_BLEND_OP_SOFTLIGHT_EXT";
        case VK_BLEND_OP_DIFFERENCE_EXT: return "VK_BLEND_OP_DIFFERENCE_EXT";
        case VK_BLEND_OP_EXCLUSION_EXT: return "VK_BLEND_OP_EXCLUSION_EXT";
        case VK_BLEND_OP_INVERT_EXT: return "VK_BLEND_OP_INVERT_EXT";
        case VK_BLEND_OP_INVERT_RGB_EXT: return "VK_BLEND_OP_INVERT_RGB_EXT";
        case VK_BLEND_OP_LINEARDODGE_EXT: return "VK_BLEND_OP_LINEARDODGE_EXT";
        case VK_BLEND_OP_LINEARBURN_EXT: return "VK_BLEND_OP_LINEARBURN_EXT";
        case VK_BLEND_OP_VIVIDLIGHT_EXT: return "VK_BLEND_OP_VIVIDLIGHT_EXT";
        case VK_BLEND_OP_LINEARLIGHT_EXT: return "VK_BLEND_OP_LINEARLIGHT_EXT";
        case VK_BLEND_OP_PINLIGHT_EXT: return "VK_BLEND_OP_PINLIGHT_EXT";
        case VK_BLEND_OP_HARDMIX_EXT: return "VK_BLEND_OP_HARDMIX_EXT";
        case VK_BLEND_OP_HSL_HUE_EXT: return "VK_BLEND_OP_HSL_HUE_EXT";
        case VK_BLEND_OP_HSL_SATURATION_EXT: return "VK_BLEND_OP_HSL_SATURATION_EXT";
        case VK_BLEND_OP_HSL_COLOR_EXT: return "VK_BLEND_OP_HSL_COLOR_EXT";
        case VK_BLEND_OP_HSL_LUMINOSITY_EXT: return "VK_BLEND_OP_HSL_LUMINOSITY_EXT";
        case VK_BLEND_OP_PLUS_EXT: return "VK_BLEND_OP_PLUS_EXT";
        case VK_BLEND_OP_PLUS_CLAMPED_EXT: return "VK_BLEND_OP_PLUS_CLAMPED_EXT";
        case VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT: return "VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT";
        case VK_BLEND_OP_PLUS_DARKER_EXT: return "VK_BLEND_OP_PLUS_DARKER_EXT";
        case VK_BLEND_OP_MINUS_EXT: return "VK_BLEND_OP_MINUS_EXT";
        case VK_BLEND_OP_MINUS_CLAMPED_EXT: return "VK_BLEND_OP_MINUS_CLAMPED_EXT";
        case VK_BLEND_OP_CONTRAST_EXT: return "VK_BLEND_OP_CONTRAST_EXT";
        case VK_BLEND_OP_INVERT_OVG_EXT: return "VK_BLEND_OP_INVERT_OVG_EXT";
        case VK_BLEND_OP_RED_EXT: return "VK_BLEND_OP_RED_EXT";
        case VK_BLEND_OP_GREEN_EXT: return "VK_BLEND_OP_GREEN_EXT";
        case VK_BLEND_OP_BLUE_EXT: return "VK_BLEND_OP_BLUE_EXT";
        case VK_BLEND_OP_MAX_ENUM: return "VK_BLEND_OP_MAX_ENUM";
        default: break;
    }
    return "Unknown VkBlendOp";
}

VkCompareOp str_to_VkCompareOp(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMPARE_OP_NEVER"sv) return VK_COMPARE_OP_NEVER;
    if (a_str == "VK_COMPARE_OP_LESS"sv) return VK_COMPARE_OP_LESS;
    if (a_str == "VK_COMPARE_OP_EQUAL"sv) return VK_COMPARE_OP_EQUAL;
    if (a_str == "VK_COMPARE_OP_LESS_OR_EQUAL"sv) return VK_COMPARE_OP_LESS_OR_EQUAL;
    if (a_str == "VK_COMPARE_OP_GREATER"sv) return VK_COMPARE_OP_GREATER;
    if (a_str == "VK_COMPARE_OP_NOT_EQUAL"sv) return VK_COMPARE_OP_NOT_EQUAL;
    if (a_str == "VK_COMPARE_OP_GREATER_OR_EQUAL"sv) return VK_COMPARE_OP_GREATER_OR_EQUAL;
    if (a_str == "VK_COMPARE_OP_ALWAYS"sv) return VK_COMPARE_OP_ALWAYS;
    if (a_str == "VK_COMPARE_OP_MAX_ENUM"sv) return VK_COMPARE_OP_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCompareOp>(0x7FFFFFFF);
}

std::string_view str_from_VkCompareOp(VkCompareOp e)
{
    switch (e) {
        case VK_COMPARE_OP_NEVER: return "VK_COMPARE_OP_NEVER";
        case VK_COMPARE_OP_LESS: return "VK_COMPARE_OP_LESS";
        case VK_COMPARE_OP_EQUAL: return "VK_COMPARE_OP_EQUAL";
        case VK_COMPARE_OP_LESS_OR_EQUAL: return "VK_COMPARE_OP_LESS_OR_EQUAL";
        case VK_COMPARE_OP_GREATER: return "VK_COMPARE_OP_GREATER";
        case VK_COMPARE_OP_NOT_EQUAL: return "VK_COMPARE_OP_NOT_EQUAL";
        case VK_COMPARE_OP_GREATER_OR_EQUAL: return "VK_COMPARE_OP_GREATER_OR_EQUAL";
        case VK_COMPARE_OP_ALWAYS: return "VK_COMPARE_OP_ALWAYS";
        case VK_COMPARE_OP_MAX_ENUM: return "VK_COMPARE_OP_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCompareOp";
}

VkDynamicState str_to_VkDynamicState(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DYNAMIC_STATE_VIEWPORT"sv) return VK_DYNAMIC_STATE_VIEWPORT;
    if (a_str == "VK_DYNAMIC_STATE_SCISSOR"sv) return VK_DYNAMIC_STATE_SCISSOR;
    if (a_str == "VK_DYNAMIC_STATE_LINE_WIDTH"sv) return VK_DYNAMIC_STATE_LINE_WIDTH;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_BIAS"sv) return VK_DYNAMIC_STATE_DEPTH_BIAS;
    if (a_str == "VK_DYNAMIC_STATE_BLEND_CONSTANTS"sv) return VK_DYNAMIC_STATE_BLEND_CONSTANTS;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_BOUNDS"sv) return VK_DYNAMIC_STATE_DEPTH_BOUNDS;
    if (a_str == "VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK"sv) return VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK;
    if (a_str == "VK_DYNAMIC_STATE_STENCIL_WRITE_MASK"sv) return VK_DYNAMIC_STATE_STENCIL_WRITE_MASK;
    if (a_str == "VK_DYNAMIC_STATE_STENCIL_REFERENCE"sv) return VK_DYNAMIC_STATE_STENCIL_REFERENCE;
    if (a_str == "VK_DYNAMIC_STATE_VIEWPORT_W_SCALING_NV"sv) return VK_DYNAMIC_STATE_VIEWPORT_W_SCALING_NV;
    if (a_str == "VK_DYNAMIC_STATE_DISCARD_RECTANGLE_EXT"sv) return VK_DYNAMIC_STATE_DISCARD_RECTANGLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_SAMPLE_LOCATIONS_EXT"sv) return VK_DYNAMIC_STATE_SAMPLE_LOCATIONS_EXT;
    if (a_str == "VK_DYNAMIC_STATE_RAY_TRACING_PIPELINE_STACK_SIZE_KHR"sv) return VK_DYNAMIC_STATE_RAY_TRACING_PIPELINE_STACK_SIZE_KHR;
    if (a_str == "VK_DYNAMIC_STATE_VIEWPORT_SHADING_RATE_PALETTE_NV"sv) return VK_DYNAMIC_STATE_VIEWPORT_SHADING_RATE_PALETTE_NV;
    if (a_str == "VK_DYNAMIC_STATE_VIEWPORT_COARSE_SAMPLE_ORDER_NV"sv) return VK_DYNAMIC_STATE_VIEWPORT_COARSE_SAMPLE_ORDER_NV;
    if (a_str == "VK_DYNAMIC_STATE_EXCLUSIVE_SCISSOR_NV"sv) return VK_DYNAMIC_STATE_EXCLUSIVE_SCISSOR_NV;
    if (a_str == "VK_DYNAMIC_STATE_FRAGMENT_SHADING_RATE_KHR"sv) return VK_DYNAMIC_STATE_FRAGMENT_SHADING_RATE_KHR;
    if (a_str == "VK_DYNAMIC_STATE_LINE_STIPPLE_EXT"sv) return VK_DYNAMIC_STATE_LINE_STIPPLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_CULL_MODE_EXT"sv) return VK_DYNAMIC_STATE_CULL_MODE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_FRONT_FACE_EXT"sv) return VK_DYNAMIC_STATE_FRONT_FACE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_PRIMITIVE_TOPOLOGY_EXT"sv) return VK_DYNAMIC_STATE_PRIMITIVE_TOPOLOGY_EXT;
    if (a_str == "VK_DYNAMIC_STATE_VIEWPORT_WITH_COUNT_EXT"sv) return VK_DYNAMIC_STATE_VIEWPORT_WITH_COUNT_EXT;
    if (a_str == "VK_DYNAMIC_STATE_SCISSOR_WITH_COUNT_EXT"sv) return VK_DYNAMIC_STATE_SCISSOR_WITH_COUNT_EXT;
    if (a_str == "VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE_EXT"sv) return VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_TEST_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_DEPTH_TEST_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_WRITE_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_DEPTH_WRITE_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT"sv) return VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_BOUNDS_TEST_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_DEPTH_BOUNDS_TEST_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_STENCIL_TEST_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_STENCIL_TEST_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_STENCIL_OP_EXT"sv) return VK_DYNAMIC_STATE_STENCIL_OP_EXT;
    if (a_str == "VK_DYNAMIC_STATE_VERTEX_INPUT_EXT"sv) return VK_DYNAMIC_STATE_VERTEX_INPUT_EXT;
    if (a_str == "VK_DYNAMIC_STATE_PATCH_CONTROL_POINTS_EXT"sv) return VK_DYNAMIC_STATE_PATCH_CONTROL_POINTS_EXT;
    if (a_str == "VK_DYNAMIC_STATE_RASTERIZER_DISCARD_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_RASTERIZER_DISCARD_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_DEPTH_BIAS_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_DEPTH_BIAS_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_LOGIC_OP_EXT"sv) return VK_DYNAMIC_STATE_LOGIC_OP_EXT;
    if (a_str == "VK_DYNAMIC_STATE_PRIMITIVE_RESTART_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_PRIMITIVE_RESTART_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_COLOR_WRITE_ENABLE_EXT"sv) return VK_DYNAMIC_STATE_COLOR_WRITE_ENABLE_EXT;
    if (a_str == "VK_DYNAMIC_STATE_MAX_ENUM"sv) return VK_DYNAMIC_STATE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDynamicState>(0x7FFFFFFF);
}

std::string_view str_from_VkDynamicState(VkDynamicState e)
{
    switch (e) {
        case VK_DYNAMIC_STATE_VIEWPORT: return "VK_DYNAMIC_STATE_VIEWPORT";
        case VK_DYNAMIC_STATE_SCISSOR: return "VK_DYNAMIC_STATE_SCISSOR";
        case VK_DYNAMIC_STATE_LINE_WIDTH: return "VK_DYNAMIC_STATE_LINE_WIDTH";
        case VK_DYNAMIC_STATE_DEPTH_BIAS: return "VK_DYNAMIC_STATE_DEPTH_BIAS";
        case VK_DYNAMIC_STATE_BLEND_CONSTANTS: return "VK_DYNAMIC_STATE_BLEND_CONSTANTS";
        case VK_DYNAMIC_STATE_DEPTH_BOUNDS: return "VK_DYNAMIC_STATE_DEPTH_BOUNDS";
        case VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK: return "VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK";
        case VK_DYNAMIC_STATE_STENCIL_WRITE_MASK: return "VK_DYNAMIC_STATE_STENCIL_WRITE_MASK";
        case VK_DYNAMIC_STATE_STENCIL_REFERENCE: return "VK_DYNAMIC_STATE_STENCIL_REFERENCE";
        case VK_DYNAMIC_STATE_VIEWPORT_W_SCALING_NV: return "VK_DYNAMIC_STATE_VIEWPORT_W_SCALING_NV";
        case VK_DYNAMIC_STATE_DISCARD_RECTANGLE_EXT: return "VK_DYNAMIC_STATE_DISCARD_RECTANGLE_EXT";
        case VK_DYNAMIC_STATE_SAMPLE_LOCATIONS_EXT: return "VK_DYNAMIC_STATE_SAMPLE_LOCATIONS_EXT";
        case VK_DYNAMIC_STATE_RAY_TRACING_PIPELINE_STACK_SIZE_KHR: return "VK_DYNAMIC_STATE_RAY_TRACING_PIPELINE_STACK_SIZE_KHR";
        case VK_DYNAMIC_STATE_VIEWPORT_SHADING_RATE_PALETTE_NV: return "VK_DYNAMIC_STATE_VIEWPORT_SHADING_RATE_PALETTE_NV";
        case VK_DYNAMIC_STATE_VIEWPORT_COARSE_SAMPLE_ORDER_NV: return "VK_DYNAMIC_STATE_VIEWPORT_COARSE_SAMPLE_ORDER_NV";
        case VK_DYNAMIC_STATE_EXCLUSIVE_SCISSOR_NV: return "VK_DYNAMIC_STATE_EXCLUSIVE_SCISSOR_NV";
        case VK_DYNAMIC_STATE_FRAGMENT_SHADING_RATE_KHR: return "VK_DYNAMIC_STATE_FRAGMENT_SHADING_RATE_KHR";
        case VK_DYNAMIC_STATE_LINE_STIPPLE_EXT: return "VK_DYNAMIC_STATE_LINE_STIPPLE_EXT";
        case VK_DYNAMIC_STATE_CULL_MODE_EXT: return "VK_DYNAMIC_STATE_CULL_MODE_EXT";
        case VK_DYNAMIC_STATE_FRONT_FACE_EXT: return "VK_DYNAMIC_STATE_FRONT_FACE_EXT";
        case VK_DYNAMIC_STATE_PRIMITIVE_TOPOLOGY_EXT: return "VK_DYNAMIC_STATE_PRIMITIVE_TOPOLOGY_EXT";
        case VK_DYNAMIC_STATE_VIEWPORT_WITH_COUNT_EXT: return "VK_DYNAMIC_STATE_VIEWPORT_WITH_COUNT_EXT";
        case VK_DYNAMIC_STATE_SCISSOR_WITH_COUNT_EXT: return "VK_DYNAMIC_STATE_SCISSOR_WITH_COUNT_EXT";
        case VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE_EXT: return "VK_DYNAMIC_STATE_VERTEX_INPUT_BINDING_STRIDE_EXT";
        case VK_DYNAMIC_STATE_DEPTH_TEST_ENABLE_EXT: return "VK_DYNAMIC_STATE_DEPTH_TEST_ENABLE_EXT";
        case VK_DYNAMIC_STATE_DEPTH_WRITE_ENABLE_EXT: return "VK_DYNAMIC_STATE_DEPTH_WRITE_ENABLE_EXT";
        case VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT: return "VK_DYNAMIC_STATE_DEPTH_COMPARE_OP_EXT";
        case VK_DYNAMIC_STATE_DEPTH_BOUNDS_TEST_ENABLE_EXT: return "VK_DYNAMIC_STATE_DEPTH_BOUNDS_TEST_ENABLE_EXT";
        case VK_DYNAMIC_STATE_STENCIL_TEST_ENABLE_EXT: return "VK_DYNAMIC_STATE_STENCIL_TEST_ENABLE_EXT";
        case VK_DYNAMIC_STATE_STENCIL_OP_EXT: return "VK_DYNAMIC_STATE_STENCIL_OP_EXT";
        case VK_DYNAMIC_STATE_VERTEX_INPUT_EXT: return "VK_DYNAMIC_STATE_VERTEX_INPUT_EXT";
        case VK_DYNAMIC_STATE_PATCH_CONTROL_POINTS_EXT: return "VK_DYNAMIC_STATE_PATCH_CONTROL_POINTS_EXT";
        case VK_DYNAMIC_STATE_RASTERIZER_DISCARD_ENABLE_EXT: return "VK_DYNAMIC_STATE_RASTERIZER_DISCARD_ENABLE_EXT";
        case VK_DYNAMIC_STATE_DEPTH_BIAS_ENABLE_EXT: return "VK_DYNAMIC_STATE_DEPTH_BIAS_ENABLE_EXT";
        case VK_DYNAMIC_STATE_LOGIC_OP_EXT: return "VK_DYNAMIC_STATE_LOGIC_OP_EXT";
        case VK_DYNAMIC_STATE_PRIMITIVE_RESTART_ENABLE_EXT: return "VK_DYNAMIC_STATE_PRIMITIVE_RESTART_ENABLE_EXT";
        case VK_DYNAMIC_STATE_COLOR_WRITE_ENABLE_EXT: return "VK_DYNAMIC_STATE_COLOR_WRITE_ENABLE_EXT";
        case VK_DYNAMIC_STATE_MAX_ENUM: return "VK_DYNAMIC_STATE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDynamicState";
}

VkFrontFace str_to_VkFrontFace(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FRONT_FACE_COUNTER_CLOCKWISE"sv) return VK_FRONT_FACE_COUNTER_CLOCKWISE;
    if (a_str == "VK_FRONT_FACE_CLOCKWISE"sv) return VK_FRONT_FACE_CLOCKWISE;
    if (a_str == "VK_FRONT_FACE_MAX_ENUM"sv) return VK_FRONT_FACE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFrontFace>(0x7FFFFFFF);
}

std::string_view str_from_VkFrontFace(VkFrontFace e)
{
    switch (e) {
        case VK_FRONT_FACE_COUNTER_CLOCKWISE: return "VK_FRONT_FACE_COUNTER_CLOCKWISE";
        case VK_FRONT_FACE_CLOCKWISE: return "VK_FRONT_FACE_CLOCKWISE";
        case VK_FRONT_FACE_MAX_ENUM: return "VK_FRONT_FACE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkFrontFace";
}

VkVertexInputRate str_to_VkVertexInputRate(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VERTEX_INPUT_RATE_VERTEX"sv) return VK_VERTEX_INPUT_RATE_VERTEX;
    if (a_str == "VK_VERTEX_INPUT_RATE_INSTANCE"sv) return VK_VERTEX_INPUT_RATE_INSTANCE;
    if (a_str == "VK_VERTEX_INPUT_RATE_MAX_ENUM"sv) return VK_VERTEX_INPUT_RATE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkVertexInputRate>(0x7FFFFFFF);
}

std::string_view str_from_VkVertexInputRate(VkVertexInputRate e)
{
    switch (e) {
        case VK_VERTEX_INPUT_RATE_VERTEX: return "VK_VERTEX_INPUT_RATE_VERTEX";
        case VK_VERTEX_INPUT_RATE_INSTANCE: return "VK_VERTEX_INPUT_RATE_INSTANCE";
        case VK_VERTEX_INPUT_RATE_MAX_ENUM: return "VK_VERTEX_INPUT_RATE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkVertexInputRate";
}

VkPrimitiveTopology str_to_VkPrimitiveTopology(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_POINT_LIST"sv) return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_LINE_LIST"sv) return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_LINE_STRIP"sv) return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST"sv) return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP"sv) return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN"sv) return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY"sv) return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY"sv) return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY"sv) return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY"sv) return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_PATCH_LIST"sv) return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
    if (a_str == "VK_PRIMITIVE_TOPOLOGY_MAX_ENUM"sv) return VK_PRIMITIVE_TOPOLOGY_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPrimitiveTopology>(0x7FFFFFFF);
}

std::string_view str_from_VkPrimitiveTopology(VkPrimitiveTopology e)
{
    switch (e) {
        case VK_PRIMITIVE_TOPOLOGY_POINT_LIST: return "VK_PRIMITIVE_TOPOLOGY_POINT_LIST";
        case VK_PRIMITIVE_TOPOLOGY_LINE_LIST: return "VK_PRIMITIVE_TOPOLOGY_LINE_LIST";
        case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP: return "VK_PRIMITIVE_TOPOLOGY_LINE_STRIP";
        case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST: return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST";
        case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP: return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP";
        case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN: return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN";
        case VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY: return "VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY";
        case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY: return "VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY";
        case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY: return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY";
        case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY: return "VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY";
        case VK_PRIMITIVE_TOPOLOGY_PATCH_LIST: return "VK_PRIMITIVE_TOPOLOGY_PATCH_LIST";
        case VK_PRIMITIVE_TOPOLOGY_MAX_ENUM: return "VK_PRIMITIVE_TOPOLOGY_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPrimitiveTopology";
}

VkPolygonMode str_to_VkPolygonMode(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_POLYGON_MODE_FILL"sv) return VK_POLYGON_MODE_FILL;
    if (a_str == "VK_POLYGON_MODE_LINE"sv) return VK_POLYGON_MODE_LINE;
    if (a_str == "VK_POLYGON_MODE_POINT"sv) return VK_POLYGON_MODE_POINT;
    if (a_str == "VK_POLYGON_MODE_FILL_RECTANGLE_NV"sv) return VK_POLYGON_MODE_FILL_RECTANGLE_NV;
    if (a_str == "VK_POLYGON_MODE_MAX_ENUM"sv) return VK_POLYGON_MODE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPolygonMode>(0x7FFFFFFF);
}

std::string_view str_from_VkPolygonMode(VkPolygonMode e)
{
    switch (e) {
        case VK_POLYGON_MODE_FILL: return "VK_POLYGON_MODE_FILL";
        case VK_POLYGON_MODE_LINE: return "VK_POLYGON_MODE_LINE";
        case VK_POLYGON_MODE_POINT: return "VK_POLYGON_MODE_POINT";
        case VK_POLYGON_MODE_FILL_RECTANGLE_NV: return "VK_POLYGON_MODE_FILL_RECTANGLE_NV";
        case VK_POLYGON_MODE_MAX_ENUM: return "VK_POLYGON_MODE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPolygonMode";
}

VkStencilOp str_to_VkStencilOp(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_STENCIL_OP_KEEP"sv) return VK_STENCIL_OP_KEEP;
    if (a_str == "VK_STENCIL_OP_ZERO"sv) return VK_STENCIL_OP_ZERO;
    if (a_str == "VK_STENCIL_OP_REPLACE"sv) return VK_STENCIL_OP_REPLACE;
    if (a_str == "VK_STENCIL_OP_INCREMENT_AND_CLAMP"sv) return VK_STENCIL_OP_INCREMENT_AND_CLAMP;
    if (a_str == "VK_STENCIL_OP_DECREMENT_AND_CLAMP"sv) return VK_STENCIL_OP_DECREMENT_AND_CLAMP;
    if (a_str == "VK_STENCIL_OP_INVERT"sv) return VK_STENCIL_OP_INVERT;
    if (a_str == "VK_STENCIL_OP_INCREMENT_AND_WRAP"sv) return VK_STENCIL_OP_INCREMENT_AND_WRAP;
    if (a_str == "VK_STENCIL_OP_DECREMENT_AND_WRAP"sv) return VK_STENCIL_OP_DECREMENT_AND_WRAP;
    if (a_str == "VK_STENCIL_OP_MAX_ENUM"sv) return VK_STENCIL_OP_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkStencilOp>(0x7FFFFFFF);
}

std::string_view str_from_VkStencilOp(VkStencilOp e)
{
    switch (e) {
        case VK_STENCIL_OP_KEEP: return "VK_STENCIL_OP_KEEP";
        case VK_STENCIL_OP_ZERO: return "VK_STENCIL_OP_ZERO";
        case VK_STENCIL_OP_REPLACE: return "VK_STENCIL_OP_REPLACE";
        case VK_STENCIL_OP_INCREMENT_AND_CLAMP: return "VK_STENCIL_OP_INCREMENT_AND_CLAMP";
        case VK_STENCIL_OP_DECREMENT_AND_CLAMP: return "VK_STENCIL_OP_DECREMENT_AND_CLAMP";
        case VK_STENCIL_OP_INVERT: return "VK_STENCIL_OP_INVERT";
        case VK_STENCIL_OP_INCREMENT_AND_WRAP: return "VK_STENCIL_OP_INCREMENT_AND_WRAP";
        case VK_STENCIL_OP_DECREMENT_AND_WRAP: return "VK_STENCIL_OP_DECREMENT_AND_WRAP";
        case VK_STENCIL_OP_MAX_ENUM: return "VK_STENCIL_OP_MAX_ENUM";
        default: break;
    }
    return "Unknown VkStencilOp";
}

VkLogicOp str_to_VkLogicOp(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_LOGIC_OP_CLEAR"sv) return VK_LOGIC_OP_CLEAR;
    if (a_str == "VK_LOGIC_OP_AND"sv) return VK_LOGIC_OP_AND;
    if (a_str == "VK_LOGIC_OP_AND_REVERSE"sv) return VK_LOGIC_OP_AND_REVERSE;
    if (a_str == "VK_LOGIC_OP_COPY"sv) return VK_LOGIC_OP_COPY;
    if (a_str == "VK_LOGIC_OP_AND_INVERTED"sv) return VK_LOGIC_OP_AND_INVERTED;
    if (a_str == "VK_LOGIC_OP_NO_OP"sv) return VK_LOGIC_OP_NO_OP;
    if (a_str == "VK_LOGIC_OP_XOR"sv) return VK_LOGIC_OP_XOR;
    if (a_str == "VK_LOGIC_OP_OR"sv) return VK_LOGIC_OP_OR;
    if (a_str == "VK_LOGIC_OP_NOR"sv) return VK_LOGIC_OP_NOR;
    if (a_str == "VK_LOGIC_OP_EQUIVALENT"sv) return VK_LOGIC_OP_EQUIVALENT;
    if (a_str == "VK_LOGIC_OP_INVERT"sv) return VK_LOGIC_OP_INVERT;
    if (a_str == "VK_LOGIC_OP_OR_REVERSE"sv) return VK_LOGIC_OP_OR_REVERSE;
    if (a_str == "VK_LOGIC_OP_COPY_INVERTED"sv) return VK_LOGIC_OP_COPY_INVERTED;
    if (a_str == "VK_LOGIC_OP_OR_INVERTED"sv) return VK_LOGIC_OP_OR_INVERTED;
    if (a_str == "VK_LOGIC_OP_NAND"sv) return VK_LOGIC_OP_NAND;
    if (a_str == "VK_LOGIC_OP_SET"sv) return VK_LOGIC_OP_SET;
    if (a_str == "VK_LOGIC_OP_MAX_ENUM"sv) return VK_LOGIC_OP_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkLogicOp>(0x7FFFFFFF);
}

std::string_view str_from_VkLogicOp(VkLogicOp e)
{
    switch (e) {
        case VK_LOGIC_OP_CLEAR: return "VK_LOGIC_OP_CLEAR";
        case VK_LOGIC_OP_AND: return "VK_LOGIC_OP_AND";
        case VK_LOGIC_OP_AND_REVERSE: return "VK_LOGIC_OP_AND_REVERSE";
        case VK_LOGIC_OP_COPY: return "VK_LOGIC_OP_COPY";
        case VK_LOGIC_OP_AND_INVERTED: return "VK_LOGIC_OP_AND_INVERTED";
        case VK_LOGIC_OP_NO_OP: return "VK_LOGIC_OP_NO_OP";
        case VK_LOGIC_OP_XOR: return "VK_LOGIC_OP_XOR";
        case VK_LOGIC_OP_OR: return "VK_LOGIC_OP_OR";
        case VK_LOGIC_OP_NOR: return "VK_LOGIC_OP_NOR";
        case VK_LOGIC_OP_EQUIVALENT: return "VK_LOGIC_OP_EQUIVALENT";
        case VK_LOGIC_OP_INVERT: return "VK_LOGIC_OP_INVERT";
        case VK_LOGIC_OP_OR_REVERSE: return "VK_LOGIC_OP_OR_REVERSE";
        case VK_LOGIC_OP_COPY_INVERTED: return "VK_LOGIC_OP_COPY_INVERTED";
        case VK_LOGIC_OP_OR_INVERTED: return "VK_LOGIC_OP_OR_INVERTED";
        case VK_LOGIC_OP_NAND: return "VK_LOGIC_OP_NAND";
        case VK_LOGIC_OP_SET: return "VK_LOGIC_OP_SET";
        case VK_LOGIC_OP_MAX_ENUM: return "VK_LOGIC_OP_MAX_ENUM";
        default: break;
    }
    return "Unknown VkLogicOp";
}

VkBorderColor str_to_VkBorderColor(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK"sv) return VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
    if (a_str == "VK_BORDER_COLOR_INT_TRANSPARENT_BLACK"sv) return VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
    if (a_str == "VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK"sv) return VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
    if (a_str == "VK_BORDER_COLOR_INT_OPAQUE_BLACK"sv) return VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    if (a_str == "VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE"sv) return VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    if (a_str == "VK_BORDER_COLOR_INT_OPAQUE_WHITE"sv) return VK_BORDER_COLOR_INT_OPAQUE_WHITE;
    if (a_str == "VK_BORDER_COLOR_FLOAT_CUSTOM_EXT"sv) return VK_BORDER_COLOR_FLOAT_CUSTOM_EXT;
    if (a_str == "VK_BORDER_COLOR_INT_CUSTOM_EXT"sv) return VK_BORDER_COLOR_INT_CUSTOM_EXT;
    if (a_str == "VK_BORDER_COLOR_MAX_ENUM"sv) return VK_BORDER_COLOR_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBorderColor>(0x7FFFFFFF);
}

std::string_view str_from_VkBorderColor(VkBorderColor e)
{
    switch (e) {
        case VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK: return "VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK";
        case VK_BORDER_COLOR_INT_TRANSPARENT_BLACK: return "VK_BORDER_COLOR_INT_TRANSPARENT_BLACK";
        case VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK: return "VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK";
        case VK_BORDER_COLOR_INT_OPAQUE_BLACK: return "VK_BORDER_COLOR_INT_OPAQUE_BLACK";
        case VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE: return "VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE";
        case VK_BORDER_COLOR_INT_OPAQUE_WHITE: return "VK_BORDER_COLOR_INT_OPAQUE_WHITE";
        case VK_BORDER_COLOR_FLOAT_CUSTOM_EXT: return "VK_BORDER_COLOR_FLOAT_CUSTOM_EXT";
        case VK_BORDER_COLOR_INT_CUSTOM_EXT: return "VK_BORDER_COLOR_INT_CUSTOM_EXT";
        case VK_BORDER_COLOR_MAX_ENUM: return "VK_BORDER_COLOR_MAX_ENUM";
        default: break;
    }
    return "Unknown VkBorderColor";
}

VkFilter str_to_VkFilter(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FILTER_NEAREST"sv) return VK_FILTER_NEAREST;
    if (a_str == "VK_FILTER_LINEAR"sv) return VK_FILTER_LINEAR;
    if (a_str == "VK_FILTER_CUBIC_IMG"sv) return VK_FILTER_CUBIC_IMG;
    if (a_str == "VK_FILTER_CUBIC_EXT"sv) return VK_FILTER_CUBIC_EXT;
    if (a_str == "VK_FILTER_MAX_ENUM"sv) return VK_FILTER_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFilter>(0x7FFFFFFF);
}

std::string_view str_from_VkFilter(VkFilter e)
{
    switch (e) {
        case VK_FILTER_NEAREST: return "VK_FILTER_NEAREST";
        case VK_FILTER_LINEAR: return "VK_FILTER_LINEAR";
        case VK_FILTER_CUBIC_IMG: return "VK_FILTER_CUBIC_IMG";
        case VK_FILTER_MAX_ENUM: return "VK_FILTER_MAX_ENUM";
        default: break;
    }
    return "Unknown VkFilter";
}

VkSamplerAddressMode str_to_VkSamplerAddressMode(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_REPEAT"sv) return VK_SAMPLER_ADDRESS_MODE_REPEAT;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT"sv) return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE"sv) return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER"sv) return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE"sv) return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE_KHR"sv) return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE_KHR;
    if (a_str == "VK_SAMPLER_ADDRESS_MODE_MAX_ENUM"sv) return VK_SAMPLER_ADDRESS_MODE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSamplerAddressMode>(0x7FFFFFFF);
}

std::string_view str_from_VkSamplerAddressMode(VkSamplerAddressMode e)
{
    switch (e) {
        case VK_SAMPLER_ADDRESS_MODE_REPEAT: return "VK_SAMPLER_ADDRESS_MODE_REPEAT";
        case VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT: return "VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT";
        case VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE: return "VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE";
        case VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER: return "VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER";
        case VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE: return "VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE";
        case VK_SAMPLER_ADDRESS_MODE_MAX_ENUM: return "VK_SAMPLER_ADDRESS_MODE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSamplerAddressMode";
}

VkSamplerMipmapMode str_to_VkSamplerMipmapMode(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLER_MIPMAP_MODE_NEAREST"sv) return VK_SAMPLER_MIPMAP_MODE_NEAREST;
    if (a_str == "VK_SAMPLER_MIPMAP_MODE_LINEAR"sv) return VK_SAMPLER_MIPMAP_MODE_LINEAR;
    if (a_str == "VK_SAMPLER_MIPMAP_MODE_MAX_ENUM"sv) return VK_SAMPLER_MIPMAP_MODE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSamplerMipmapMode>(0x7FFFFFFF);
}

std::string_view str_from_VkSamplerMipmapMode(VkSamplerMipmapMode e)
{
    switch (e) {
        case VK_SAMPLER_MIPMAP_MODE_NEAREST: return "VK_SAMPLER_MIPMAP_MODE_NEAREST";
        case VK_SAMPLER_MIPMAP_MODE_LINEAR: return "VK_SAMPLER_MIPMAP_MODE_LINEAR";
        case VK_SAMPLER_MIPMAP_MODE_MAX_ENUM: return "VK_SAMPLER_MIPMAP_MODE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSamplerMipmapMode";
}

VkDescriptorType str_to_VkDescriptorType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DESCRIPTOR_TYPE_SAMPLER"sv) return VK_DESCRIPTOR_TYPE_SAMPLER;
    if (a_str == "VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER"sv) return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    if (a_str == "VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE"sv) return VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    if (a_str == "VK_DESCRIPTOR_TYPE_STORAGE_IMAGE"sv) return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    if (a_str == "VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER"sv) return VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
    if (a_str == "VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER"sv) return VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
    if (a_str == "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER"sv) return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    if (a_str == "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER"sv) return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    if (a_str == "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC"sv) return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    if (a_str == "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC"sv) return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
    if (a_str == "VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT"sv) return VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
    if (a_str == "VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT"sv) return VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT;
    if (a_str == "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR"sv) return VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    if (a_str == "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV"sv) return VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;
    if (a_str == "VK_DESCRIPTOR_TYPE_MUTABLE_VALVE"sv) return VK_DESCRIPTOR_TYPE_MUTABLE_VALVE;
    if (a_str == "VK_DESCRIPTOR_TYPE_MAX_ENUM"sv) return VK_DESCRIPTOR_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDescriptorType>(0x7FFFFFFF);
}

std::string_view str_from_VkDescriptorType(VkDescriptorType e)
{
    switch (e) {
        case VK_DESCRIPTOR_TYPE_SAMPLER: return "VK_DESCRIPTOR_TYPE_SAMPLER";
        case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER: return "VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER";
        case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE: return "VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE";
        case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE: return "VK_DESCRIPTOR_TYPE_STORAGE_IMAGE";
        case VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER: return "VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER";
        case VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER: return "VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER";
        case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER: return "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER";
        case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER: return "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER";
        case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC: return "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC";
        case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC: return "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC";
        case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT: return "VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT";
        case VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT: return "VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT";
        case VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR: return "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR";
        case VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV: return "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV";
        case VK_DESCRIPTOR_TYPE_MUTABLE_VALVE: return "VK_DESCRIPTOR_TYPE_MUTABLE_VALVE";
        case VK_DESCRIPTOR_TYPE_MAX_ENUM: return "VK_DESCRIPTOR_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDescriptorType";
}

VkAttachmentLoadOp str_to_VkAttachmentLoadOp(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ATTACHMENT_LOAD_OP_LOAD"sv) return VK_ATTACHMENT_LOAD_OP_LOAD;
    if (a_str == "VK_ATTACHMENT_LOAD_OP_CLEAR"sv) return VK_ATTACHMENT_LOAD_OP_CLEAR;
    if (a_str == "VK_ATTACHMENT_LOAD_OP_DONT_CARE"sv) return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    if (a_str == "VK_ATTACHMENT_LOAD_OP_NONE_EXT"sv) return VK_ATTACHMENT_LOAD_OP_NONE_EXT;
    if (a_str == "VK_ATTACHMENT_LOAD_OP_MAX_ENUM"sv) return VK_ATTACHMENT_LOAD_OP_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAttachmentLoadOp>(0x7FFFFFFF);
}

std::string_view str_from_VkAttachmentLoadOp(VkAttachmentLoadOp e)
{
    switch (e) {
        case VK_ATTACHMENT_LOAD_OP_LOAD: return "VK_ATTACHMENT_LOAD_OP_LOAD";
        case VK_ATTACHMENT_LOAD_OP_CLEAR: return "VK_ATTACHMENT_LOAD_OP_CLEAR";
        case VK_ATTACHMENT_LOAD_OP_DONT_CARE: return "VK_ATTACHMENT_LOAD_OP_DONT_CARE";
        case VK_ATTACHMENT_LOAD_OP_NONE_EXT: return "VK_ATTACHMENT_LOAD_OP_NONE_EXT";
        case VK_ATTACHMENT_LOAD_OP_MAX_ENUM: return "VK_ATTACHMENT_LOAD_OP_MAX_ENUM";
        default: break;
    }
    return "Unknown VkAttachmentLoadOp";
}

VkAttachmentStoreOp str_to_VkAttachmentStoreOp(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ATTACHMENT_STORE_OP_STORE"sv) return VK_ATTACHMENT_STORE_OP_STORE;
    if (a_str == "VK_ATTACHMENT_STORE_OP_DONT_CARE"sv) return VK_ATTACHMENT_STORE_OP_DONT_CARE;
    if (a_str == "VK_ATTACHMENT_STORE_OP_NONE_KHR"sv) return VK_ATTACHMENT_STORE_OP_NONE_KHR;
    if (a_str == "VK_ATTACHMENT_STORE_OP_NONE_QCOM"sv) return VK_ATTACHMENT_STORE_OP_NONE_QCOM;
    if (a_str == "VK_ATTACHMENT_STORE_OP_NONE_EXT"sv) return VK_ATTACHMENT_STORE_OP_NONE_EXT;
    if (a_str == "VK_ATTACHMENT_STORE_OP_MAX_ENUM"sv) return VK_ATTACHMENT_STORE_OP_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAttachmentStoreOp>(0x7FFFFFFF);
}

std::string_view str_from_VkAttachmentStoreOp(VkAttachmentStoreOp e)
{
    switch (e) {
        case VK_ATTACHMENT_STORE_OP_STORE: return "VK_ATTACHMENT_STORE_OP_STORE";
        case VK_ATTACHMENT_STORE_OP_DONT_CARE: return "VK_ATTACHMENT_STORE_OP_DONT_CARE";
        case VK_ATTACHMENT_STORE_OP_NONE_KHR: return "VK_ATTACHMENT_STORE_OP_NONE_KHR";
        case VK_ATTACHMENT_STORE_OP_MAX_ENUM: return "VK_ATTACHMENT_STORE_OP_MAX_ENUM";
        default: break;
    }
    return "Unknown VkAttachmentStoreOp";
}

VkPipelineBindPoint str_to_VkPipelineBindPoint(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_BIND_POINT_GRAPHICS"sv) return VK_PIPELINE_BIND_POINT_GRAPHICS;
    if (a_str == "VK_PIPELINE_BIND_POINT_COMPUTE"sv) return VK_PIPELINE_BIND_POINT_COMPUTE;
    if (a_str == "VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR"sv) return VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR;
    if (a_str == "VK_PIPELINE_BIND_POINT_SUBPASS_SHADING_HUAWEI"sv) return VK_PIPELINE_BIND_POINT_SUBPASS_SHADING_HUAWEI;
    if (a_str == "VK_PIPELINE_BIND_POINT_RAY_TRACING_NV"sv) return VK_PIPELINE_BIND_POINT_RAY_TRACING_NV;
    if (a_str == "VK_PIPELINE_BIND_POINT_MAX_ENUM"sv) return VK_PIPELINE_BIND_POINT_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineBindPoint>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineBindPoint(VkPipelineBindPoint e)
{
    switch (e) {
        case VK_PIPELINE_BIND_POINT_GRAPHICS: return "VK_PIPELINE_BIND_POINT_GRAPHICS";
        case VK_PIPELINE_BIND_POINT_COMPUTE: return "VK_PIPELINE_BIND_POINT_COMPUTE";
        case VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR: return "VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR";
        case VK_PIPELINE_BIND_POINT_SUBPASS_SHADING_HUAWEI: return "VK_PIPELINE_BIND_POINT_SUBPASS_SHADING_HUAWEI";
        case VK_PIPELINE_BIND_POINT_MAX_ENUM: return "VK_PIPELINE_BIND_POINT_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPipelineBindPoint";
}

VkCommandBufferLevel str_to_VkCommandBufferLevel(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMMAND_BUFFER_LEVEL_PRIMARY"sv) return VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    if (a_str == "VK_COMMAND_BUFFER_LEVEL_SECONDARY"sv) return VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    if (a_str == "VK_COMMAND_BUFFER_LEVEL_MAX_ENUM"sv) return VK_COMMAND_BUFFER_LEVEL_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCommandBufferLevel>(0x7FFFFFFF);
}

std::string_view str_from_VkCommandBufferLevel(VkCommandBufferLevel e)
{
    switch (e) {
        case VK_COMMAND_BUFFER_LEVEL_PRIMARY: return "VK_COMMAND_BUFFER_LEVEL_PRIMARY";
        case VK_COMMAND_BUFFER_LEVEL_SECONDARY: return "VK_COMMAND_BUFFER_LEVEL_SECONDARY";
        case VK_COMMAND_BUFFER_LEVEL_MAX_ENUM: return "VK_COMMAND_BUFFER_LEVEL_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCommandBufferLevel";
}

VkIndexType str_to_VkIndexType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_INDEX_TYPE_UINT16"sv) return VK_INDEX_TYPE_UINT16;
    if (a_str == "VK_INDEX_TYPE_UINT32"sv) return VK_INDEX_TYPE_UINT32;
    if (a_str == "VK_INDEX_TYPE_NONE_KHR"sv) return VK_INDEX_TYPE_NONE_KHR;
    if (a_str == "VK_INDEX_TYPE_UINT8_EXT"sv) return VK_INDEX_TYPE_UINT8_EXT;
    if (a_str == "VK_INDEX_TYPE_NONE_NV"sv) return VK_INDEX_TYPE_NONE_NV;
    if (a_str == "VK_INDEX_TYPE_MAX_ENUM"sv) return VK_INDEX_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkIndexType>(0x7FFFFFFF);
}

std::string_view str_from_VkIndexType(VkIndexType e)
{
    switch (e) {
        case VK_INDEX_TYPE_UINT16: return "VK_INDEX_TYPE_UINT16";
        case VK_INDEX_TYPE_UINT32: return "VK_INDEX_TYPE_UINT32";
        case VK_INDEX_TYPE_NONE_KHR: return "VK_INDEX_TYPE_NONE_KHR";
        case VK_INDEX_TYPE_UINT8_EXT: return "VK_INDEX_TYPE_UINT8_EXT";
        case VK_INDEX_TYPE_MAX_ENUM: return "VK_INDEX_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkIndexType";
}

VkSubpassContents str_to_VkSubpassContents(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SUBPASS_CONTENTS_INLINE"sv) return VK_SUBPASS_CONTENTS_INLINE;
    if (a_str == "VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS"sv) return VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS;
    if (a_str == "VK_SUBPASS_CONTENTS_MAX_ENUM"sv) return VK_SUBPASS_CONTENTS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSubpassContents>(0x7FFFFFFF);
}

std::string_view str_from_VkSubpassContents(VkSubpassContents e)
{
    switch (e) {
        case VK_SUBPASS_CONTENTS_INLINE: return "VK_SUBPASS_CONTENTS_INLINE";
        case VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS: return "VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS";
        case VK_SUBPASS_CONTENTS_MAX_ENUM: return "VK_SUBPASS_CONTENTS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSubpassContents";
}

VkAccessFlagBits str_to_VkAccessFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCESS_INDIRECT_COMMAND_READ_BIT"sv) return VK_ACCESS_INDIRECT_COMMAND_READ_BIT;
    if (a_str == "VK_ACCESS_INDEX_READ_BIT"sv) return VK_ACCESS_INDEX_READ_BIT;
    if (a_str == "VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT"sv) return VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    if (a_str == "VK_ACCESS_UNIFORM_READ_BIT"sv) return VK_ACCESS_UNIFORM_READ_BIT;
    if (a_str == "VK_ACCESS_INPUT_ATTACHMENT_READ_BIT"sv) return VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
    if (a_str == "VK_ACCESS_SHADER_READ_BIT"sv) return VK_ACCESS_SHADER_READ_BIT;
    if (a_str == "VK_ACCESS_SHADER_WRITE_BIT"sv) return VK_ACCESS_SHADER_WRITE_BIT;
    if (a_str == "VK_ACCESS_COLOR_ATTACHMENT_READ_BIT"sv) return VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
    if (a_str == "VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT"sv) return VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    if (a_str == "VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT"sv) return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
    if (a_str == "VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT"sv) return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
    if (a_str == "VK_ACCESS_TRANSFER_READ_BIT"sv) return VK_ACCESS_TRANSFER_READ_BIT;
    if (a_str == "VK_ACCESS_TRANSFER_WRITE_BIT"sv) return VK_ACCESS_TRANSFER_WRITE_BIT;
    if (a_str == "VK_ACCESS_HOST_READ_BIT"sv) return VK_ACCESS_HOST_READ_BIT;
    if (a_str == "VK_ACCESS_HOST_WRITE_BIT"sv) return VK_ACCESS_HOST_WRITE_BIT;
    if (a_str == "VK_ACCESS_MEMORY_READ_BIT"sv) return VK_ACCESS_MEMORY_READ_BIT;
    if (a_str == "VK_ACCESS_MEMORY_WRITE_BIT"sv) return VK_ACCESS_MEMORY_WRITE_BIT;
    if (a_str == "VK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT"sv) return VK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT;
    if (a_str == "VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT"sv) return VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT;
    if (a_str == "VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT"sv) return VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT;
    if (a_str == "VK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT"sv) return VK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT;
    if (a_str == "VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT"sv) return VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT;
    if (a_str == "VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR"sv) return VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR;
    if (a_str == "VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR"sv) return VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR;
    if (a_str == "VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT"sv) return VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT;
    if (a_str == "VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR"sv) return VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR;
    if (a_str == "VK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV"sv) return VK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV;
    if (a_str == "VK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV"sv) return VK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV;
    if (a_str == "VK_ACCESS_NONE_KHR"sv) return VK_ACCESS_NONE_KHR;
    if (a_str == "VK_ACCESS_SHADING_RATE_IMAGE_READ_BIT_NV"sv) return VK_ACCESS_SHADING_RATE_IMAGE_READ_BIT_NV;
    if (a_str == "VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV"sv) return VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV;
    if (a_str == "VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV"sv) return VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV;
    if (a_str == "VK_ACCESS_FLAG_BITS_MAX_ENUM"sv) return VK_ACCESS_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccessFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkAccessFlagBits(VkAccessFlagBits e)
{
    switch (e) {
        case VK_ACCESS_INDIRECT_COMMAND_READ_BIT: return "VK_ACCESS_INDIRECT_COMMAND_READ_BIT";
        case VK_ACCESS_INDEX_READ_BIT: return "VK_ACCESS_INDEX_READ_BIT";
        case VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT: return "VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT";
        case VK_ACCESS_UNIFORM_READ_BIT: return "VK_ACCESS_UNIFORM_READ_BIT";
        case VK_ACCESS_INPUT_ATTACHMENT_READ_BIT: return "VK_ACCESS_INPUT_ATTACHMENT_READ_BIT";
        case VK_ACCESS_SHADER_READ_BIT: return "VK_ACCESS_SHADER_READ_BIT";
        case VK_ACCESS_SHADER_WRITE_BIT: return "VK_ACCESS_SHADER_WRITE_BIT";
        case VK_ACCESS_COLOR_ATTACHMENT_READ_BIT: return "VK_ACCESS_COLOR_ATTACHMENT_READ_BIT";
        case VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT: return "VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT";
        case VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT: return "VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT";
        case VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT: return "VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT";
        case VK_ACCESS_TRANSFER_READ_BIT: return "VK_ACCESS_TRANSFER_READ_BIT";
        case VK_ACCESS_TRANSFER_WRITE_BIT: return "VK_ACCESS_TRANSFER_WRITE_BIT";
        case VK_ACCESS_HOST_READ_BIT: return "VK_ACCESS_HOST_READ_BIT";
        case VK_ACCESS_HOST_WRITE_BIT: return "VK_ACCESS_HOST_WRITE_BIT";
        case VK_ACCESS_MEMORY_READ_BIT: return "VK_ACCESS_MEMORY_READ_BIT";
        case VK_ACCESS_MEMORY_WRITE_BIT: return "VK_ACCESS_MEMORY_WRITE_BIT";
        case VK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT: return "VK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT";
        case VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT: return "VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT";
        case VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT: return "VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT";
        case VK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT: return "VK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT";
        case VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT: return "VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT";
        case VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR: return "VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_KHR";
        case VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR: return "VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_KHR";
        case VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT: return "VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT";
        case VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR: return "VK_ACCESS_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR";
        case VK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV: return "VK_ACCESS_COMMAND_PREPROCESS_READ_BIT_NV";
        case VK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV: return "VK_ACCESS_COMMAND_PREPROCESS_WRITE_BIT_NV";
        case VK_ACCESS_NONE_KHR: return "VK_ACCESS_NONE_KHR";
        case VK_ACCESS_FLAG_BITS_MAX_ENUM: return "VK_ACCESS_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkAccessFlagBits";
}

std::string str_from_VkAccessFlags(VkAccessFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkAccessFlagBits(static_cast<VkAccessFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkAccessFlagBits(static_cast<VkAccessFlagBits>(0)));
    return result;
}

 VkImageAspectFlagBits str_to_VkImageAspectFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_ASPECT_COLOR_BIT"sv) return VK_IMAGE_ASPECT_COLOR_BIT;
    if (a_str == "VK_IMAGE_ASPECT_DEPTH_BIT"sv) return VK_IMAGE_ASPECT_DEPTH_BIT;
    if (a_str == "VK_IMAGE_ASPECT_STENCIL_BIT"sv) return VK_IMAGE_ASPECT_STENCIL_BIT;
    if (a_str == "VK_IMAGE_ASPECT_METADATA_BIT"sv) return VK_IMAGE_ASPECT_METADATA_BIT;
    if (a_str == "VK_IMAGE_ASPECT_PLANE_0_BIT"sv) return VK_IMAGE_ASPECT_PLANE_0_BIT;
    if (a_str == "VK_IMAGE_ASPECT_PLANE_1_BIT"sv) return VK_IMAGE_ASPECT_PLANE_1_BIT;
    if (a_str == "VK_IMAGE_ASPECT_PLANE_2_BIT"sv) return VK_IMAGE_ASPECT_PLANE_2_BIT;
    if (a_str == "VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT"sv) return VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT;
    if (a_str == "VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT"sv) return VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT;
    if (a_str == "VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT"sv) return VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT;
    if (a_str == "VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT"sv) return VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT;
    if (a_str == "VK_IMAGE_ASPECT_PLANE_0_BIT_KHR"sv) return VK_IMAGE_ASPECT_PLANE_0_BIT_KHR;
    if (a_str == "VK_IMAGE_ASPECT_PLANE_1_BIT_KHR"sv) return VK_IMAGE_ASPECT_PLANE_1_BIT_KHR;
    if (a_str == "VK_IMAGE_ASPECT_PLANE_2_BIT_KHR"sv) return VK_IMAGE_ASPECT_PLANE_2_BIT_KHR;
    if (a_str == "VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM"sv) return VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageAspectFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkImageAspectFlagBits(VkImageAspectFlagBits e)
{
    switch (e) {
        case VK_IMAGE_ASPECT_COLOR_BIT: return "VK_IMAGE_ASPECT_COLOR_BIT";
        case VK_IMAGE_ASPECT_DEPTH_BIT: return "VK_IMAGE_ASPECT_DEPTH_BIT";
        case VK_IMAGE_ASPECT_STENCIL_BIT: return "VK_IMAGE_ASPECT_STENCIL_BIT";
        case VK_IMAGE_ASPECT_METADATA_BIT: return "VK_IMAGE_ASPECT_METADATA_BIT";
        case VK_IMAGE_ASPECT_PLANE_0_BIT: return "VK_IMAGE_ASPECT_PLANE_0_BIT";
        case VK_IMAGE_ASPECT_PLANE_1_BIT: return "VK_IMAGE_ASPECT_PLANE_1_BIT";
        case VK_IMAGE_ASPECT_PLANE_2_BIT: return "VK_IMAGE_ASPECT_PLANE_2_BIT";
        case VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT: return "VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT";
        case VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT: return "VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT";
        case VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT: return "VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT";
        case VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT: return "VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT";
        case VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM: return "VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkImageAspectFlagBits";
}

std::string str_from_VkImageAspectFlags(VkImageAspectFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkImageAspectFlagBits(static_cast<VkImageAspectFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkImageAspectFlagBits(static_cast<VkImageAspectFlagBits>(0)));
    return result;
}

 VkFormatFeatureFlagBits str_to_VkFormatFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT;
    if (a_str == "VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT"sv) return VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT;
    if (a_str == "VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT"sv) return VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT;
    if (a_str == "VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT"sv) return VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT;
    if (a_str == "VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT"sv) return VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT;
    if (a_str == "VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT"sv) return VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT;
    if (a_str == "VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT"sv) return VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT;
    if (a_str == "VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT"sv) return VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT;
    if (a_str == "VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT"sv) return VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT;
    if (a_str == "VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT"sv) return VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;
    if (a_str == "VK_FORMAT_FEATURE_BLIT_SRC_BIT"sv) return VK_FORMAT_FEATURE_BLIT_SRC_BIT;
    if (a_str == "VK_FORMAT_FEATURE_BLIT_DST_BIT"sv) return VK_FORMAT_FEATURE_BLIT_DST_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT;
    if (a_str == "VK_FORMAT_FEATURE_TRANSFER_SRC_BIT"sv) return VK_FORMAT_FEATURE_TRANSFER_SRC_BIT;
    if (a_str == "VK_FORMAT_FEATURE_TRANSFER_DST_BIT"sv) return VK_FORMAT_FEATURE_TRANSFER_DST_BIT;
    if (a_str == "VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT"sv) return VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT;
    if (a_str == "VK_FORMAT_FEATURE_DISJOINT_BIT"sv) return VK_FORMAT_FEATURE_DISJOINT_BIT;
    if (a_str == "VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT"sv) return VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG;
    if (a_str == "VK_FORMAT_FEATURE_ACCELERATION_STRUCTURE_VERTEX_BUFFER_BIT_KHR"sv) return VK_FORMAT_FEATURE_ACCELERATION_STRUCTURE_VERTEX_BUFFER_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_FRAGMENT_DENSITY_MAP_BIT_EXT"sv) return VK_FORMAT_FEATURE_FRAGMENT_DENSITY_MAP_BIT_EXT;
    if (a_str == "VK_FORMAT_FEATURE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR"sv) return VK_FORMAT_FEATURE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_TRANSFER_SRC_BIT_KHR"sv) return VK_FORMAT_FEATURE_TRANSFER_SRC_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_TRANSFER_DST_BIT_KHR"sv) return VK_FORMAT_FEATURE_TRANSFER_DST_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT_EXT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT_EXT;
    if (a_str == "VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT_KHR"sv) return VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT_KHR"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT_KHR"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT_KHR"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT_KHR"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_DISJOINT_BIT_KHR"sv) return VK_FORMAT_FEATURE_DISJOINT_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT_KHR"sv) return VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_EXT"sv) return VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_EXT;
    if (a_str == "VK_FORMAT_FEATURE_FLAG_BITS_MAX_ENUM"sv) return VK_FORMAT_FEATURE_FLAG_BITS_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_FORMAT_FEATURE_VIDEO_DECODE_OUTPUT_BIT_KHR"sv) return VK_FORMAT_FEATURE_VIDEO_DECODE_OUTPUT_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_VIDEO_DECODE_DPB_BIT_KHR"sv) return VK_FORMAT_FEATURE_VIDEO_DECODE_DPB_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_VIDEO_ENCODE_INPUT_BIT_KHR"sv) return VK_FORMAT_FEATURE_VIDEO_ENCODE_INPUT_BIT_KHR;
    if (a_str == "VK_FORMAT_FEATURE_VIDEO_ENCODE_DPB_BIT_KHR"sv) return VK_FORMAT_FEATURE_VIDEO_ENCODE_DPB_BIT_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFormatFeatureFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkFormatFeatureFlagBits(VkFormatFeatureFlagBits e)
{
    switch (e) {
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT";
        case VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT: return "VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT";
        case VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT: return "VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT";
        case VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT: return "VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT";
        case VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT: return "VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT";
        case VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT: return "VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT";
        case VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT: return "VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT";
        case VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT: return "VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT";
        case VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT: return "VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT";
        case VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT: return "VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT";
        case VK_FORMAT_FEATURE_BLIT_SRC_BIT: return "VK_FORMAT_FEATURE_BLIT_SRC_BIT";
        case VK_FORMAT_FEATURE_BLIT_DST_BIT: return "VK_FORMAT_FEATURE_BLIT_DST_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT";
        case VK_FORMAT_FEATURE_TRANSFER_SRC_BIT: return "VK_FORMAT_FEATURE_TRANSFER_SRC_BIT";
        case VK_FORMAT_FEATURE_TRANSFER_DST_BIT: return "VK_FORMAT_FEATURE_TRANSFER_DST_BIT";
        case VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT: return "VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT";
        case VK_FORMAT_FEATURE_DISJOINT_BIT: return "VK_FORMAT_FEATURE_DISJOINT_BIT";
        case VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT: return "VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT";
        case VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG: return "VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG";
        case VK_FORMAT_FEATURE_ACCELERATION_STRUCTURE_VERTEX_BUFFER_BIT_KHR: return "VK_FORMAT_FEATURE_ACCELERATION_STRUCTURE_VERTEX_BUFFER_BIT_KHR";
        case VK_FORMAT_FEATURE_FRAGMENT_DENSITY_MAP_BIT_EXT: return "VK_FORMAT_FEATURE_FRAGMENT_DENSITY_MAP_BIT_EXT";
        case VK_FORMAT_FEATURE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR: return "VK_FORMAT_FEATURE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR";
        case VK_FORMAT_FEATURE_FLAG_BITS_MAX_ENUM: return "VK_FORMAT_FEATURE_FLAG_BITS_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_FORMAT_FEATURE_VIDEO_DECODE_OUTPUT_BIT_KHR: return "VK_FORMAT_FEATURE_VIDEO_DECODE_OUTPUT_BIT_KHR";
        case VK_FORMAT_FEATURE_VIDEO_DECODE_DPB_BIT_KHR: return "VK_FORMAT_FEATURE_VIDEO_DECODE_DPB_BIT_KHR";
        case VK_FORMAT_FEATURE_VIDEO_ENCODE_INPUT_BIT_KHR: return "VK_FORMAT_FEATURE_VIDEO_ENCODE_INPUT_BIT_KHR";
        case VK_FORMAT_FEATURE_VIDEO_ENCODE_DPB_BIT_KHR: return "VK_FORMAT_FEATURE_VIDEO_ENCODE_DPB_BIT_KHR";
#endif
        default: break;
    }
    return "Unknown VkFormatFeatureFlagBits";
}

std::string str_from_VkFormatFeatureFlags(VkFormatFeatureFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkFormatFeatureFlagBits(static_cast<VkFormatFeatureFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkFormatFeatureFlagBits(static_cast<VkFormatFeatureFlagBits>(0)));
    return result;
}

 VkImageCreateFlagBits str_to_VkImageCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_CREATE_SPARSE_BINDING_BIT"sv) return VK_IMAGE_CREATE_SPARSE_BINDING_BIT;
    if (a_str == "VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT"sv) return VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT;
    if (a_str == "VK_IMAGE_CREATE_SPARSE_ALIASED_BIT"sv) return VK_IMAGE_CREATE_SPARSE_ALIASED_BIT;
    if (a_str == "VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT"sv) return VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT;
    if (a_str == "VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT"sv) return VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    if (a_str == "VK_IMAGE_CREATE_ALIAS_BIT"sv) return VK_IMAGE_CREATE_ALIAS_BIT;
    if (a_str == "VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT"sv) return VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT;
    if (a_str == "VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT"sv) return VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
    if (a_str == "VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT"sv) return VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT;
    if (a_str == "VK_IMAGE_CREATE_EXTENDED_USAGE_BIT"sv) return VK_IMAGE_CREATE_EXTENDED_USAGE_BIT;
    if (a_str == "VK_IMAGE_CREATE_PROTECTED_BIT"sv) return VK_IMAGE_CREATE_PROTECTED_BIT;
    if (a_str == "VK_IMAGE_CREATE_DISJOINT_BIT"sv) return VK_IMAGE_CREATE_DISJOINT_BIT;
    if (a_str == "VK_IMAGE_CREATE_CORNER_SAMPLED_BIT_NV"sv) return VK_IMAGE_CREATE_CORNER_SAMPLED_BIT_NV;
    if (a_str == "VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT"sv) return VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT;
    if (a_str == "VK_IMAGE_CREATE_SUBSAMPLED_BIT_EXT"sv) return VK_IMAGE_CREATE_SUBSAMPLED_BIT_EXT;
    if (a_str == "VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR"sv) return VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR;
    if (a_str == "VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT_KHR"sv) return VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT_KHR;
    if (a_str == "VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT_KHR"sv) return VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT_KHR;
    if (a_str == "VK_IMAGE_CREATE_EXTENDED_USAGE_BIT_KHR"sv) return VK_IMAGE_CREATE_EXTENDED_USAGE_BIT_KHR;
    if (a_str == "VK_IMAGE_CREATE_DISJOINT_BIT_KHR"sv) return VK_IMAGE_CREATE_DISJOINT_BIT_KHR;
    if (a_str == "VK_IMAGE_CREATE_ALIAS_BIT_KHR"sv) return VK_IMAGE_CREATE_ALIAS_BIT_KHR;
    if (a_str == "VK_IMAGE_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_IMAGE_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkImageCreateFlagBits(VkImageCreateFlagBits e)
{
    switch (e) {
        case VK_IMAGE_CREATE_SPARSE_BINDING_BIT: return "VK_IMAGE_CREATE_SPARSE_BINDING_BIT";
        case VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT: return "VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT";
        case VK_IMAGE_CREATE_SPARSE_ALIASED_BIT: return "VK_IMAGE_CREATE_SPARSE_ALIASED_BIT";
        case VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT: return "VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT";
        case VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT: return "VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT";
        case VK_IMAGE_CREATE_ALIAS_BIT: return "VK_IMAGE_CREATE_ALIAS_BIT";
        case VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT: return "VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT";
        case VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT: return "VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT";
        case VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT: return "VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT";
        case VK_IMAGE_CREATE_EXTENDED_USAGE_BIT: return "VK_IMAGE_CREATE_EXTENDED_USAGE_BIT";
        case VK_IMAGE_CREATE_PROTECTED_BIT: return "VK_IMAGE_CREATE_PROTECTED_BIT";
        case VK_IMAGE_CREATE_DISJOINT_BIT: return "VK_IMAGE_CREATE_DISJOINT_BIT";
        case VK_IMAGE_CREATE_CORNER_SAMPLED_BIT_NV: return "VK_IMAGE_CREATE_CORNER_SAMPLED_BIT_NV";
        case VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT: return "VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT";
        case VK_IMAGE_CREATE_SUBSAMPLED_BIT_EXT: return "VK_IMAGE_CREATE_SUBSAMPLED_BIT_EXT";
        case VK_IMAGE_CREATE_FLAG_BITS_MAX_ENUM: return "VK_IMAGE_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkImageCreateFlagBits";
}

std::string str_from_VkImageCreateFlags(VkImageCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkImageCreateFlagBits(static_cast<VkImageCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkImageCreateFlagBits(static_cast<VkImageCreateFlagBits>(0)));
    return result;
}

 VkSampleCountFlagBits str_to_VkSampleCountFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLE_COUNT_1_BIT"sv) return VK_SAMPLE_COUNT_1_BIT;
    if (a_str == "VK_SAMPLE_COUNT_2_BIT"sv) return VK_SAMPLE_COUNT_2_BIT;
    if (a_str == "VK_SAMPLE_COUNT_4_BIT"sv) return VK_SAMPLE_COUNT_4_BIT;
    if (a_str == "VK_SAMPLE_COUNT_8_BIT"sv) return VK_SAMPLE_COUNT_8_BIT;
    if (a_str == "VK_SAMPLE_COUNT_16_BIT"sv) return VK_SAMPLE_COUNT_16_BIT;
    if (a_str == "VK_SAMPLE_COUNT_32_BIT"sv) return VK_SAMPLE_COUNT_32_BIT;
    if (a_str == "VK_SAMPLE_COUNT_64_BIT"sv) return VK_SAMPLE_COUNT_64_BIT;
    if (a_str == "VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM"sv) return VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSampleCountFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSampleCountFlagBits(VkSampleCountFlagBits e)
{
    switch (e) {
        case VK_SAMPLE_COUNT_1_BIT: return "VK_SAMPLE_COUNT_1_BIT";
        case VK_SAMPLE_COUNT_2_BIT: return "VK_SAMPLE_COUNT_2_BIT";
        case VK_SAMPLE_COUNT_4_BIT: return "VK_SAMPLE_COUNT_4_BIT";
        case VK_SAMPLE_COUNT_8_BIT: return "VK_SAMPLE_COUNT_8_BIT";
        case VK_SAMPLE_COUNT_16_BIT: return "VK_SAMPLE_COUNT_16_BIT";
        case VK_SAMPLE_COUNT_32_BIT: return "VK_SAMPLE_COUNT_32_BIT";
        case VK_SAMPLE_COUNT_64_BIT: return "VK_SAMPLE_COUNT_64_BIT";
        case VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM: return "VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSampleCountFlagBits";
}

std::string str_from_VkSampleCountFlags(VkSampleCountFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSampleCountFlagBits(static_cast<VkSampleCountFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSampleCountFlagBits(static_cast<VkSampleCountFlagBits>(0)));
    return result;
}

 VkImageUsageFlagBits str_to_VkImageUsageFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_USAGE_TRANSFER_SRC_BIT"sv) return VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    if (a_str == "VK_IMAGE_USAGE_TRANSFER_DST_BIT"sv) return VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    if (a_str == "VK_IMAGE_USAGE_SAMPLED_BIT"sv) return VK_IMAGE_USAGE_SAMPLED_BIT;
    if (a_str == "VK_IMAGE_USAGE_STORAGE_BIT"sv) return VK_IMAGE_USAGE_STORAGE_BIT;
    if (a_str == "VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT"sv) return VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    if (a_str == "VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT"sv) return VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    if (a_str == "VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT"sv) return VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT;
    if (a_str == "VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT"sv) return VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
    if (a_str == "VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT"sv) return VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT;
    if (a_str == "VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR"sv) return VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR;
    if (a_str == "VK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI"sv) return VK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI;
    if (a_str == "VK_IMAGE_USAGE_SHADING_RATE_IMAGE_BIT_NV"sv) return VK_IMAGE_USAGE_SHADING_RATE_IMAGE_BIT_NV;
    if (a_str == "VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM"sv) return VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR"sv) return VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR;
    if (a_str == "VK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR"sv) return VK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR;
    if (a_str == "VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR"sv) return VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR;
    if (a_str == "VK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR"sv) return VK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR;
    if (a_str == "VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR"sv) return VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR;
    if (a_str == "VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR"sv) return VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageUsageFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkImageUsageFlagBits(VkImageUsageFlagBits e)
{
    switch (e) {
        case VK_IMAGE_USAGE_TRANSFER_SRC_BIT: return "VK_IMAGE_USAGE_TRANSFER_SRC_BIT";
        case VK_IMAGE_USAGE_TRANSFER_DST_BIT: return "VK_IMAGE_USAGE_TRANSFER_DST_BIT";
        case VK_IMAGE_USAGE_SAMPLED_BIT: return "VK_IMAGE_USAGE_SAMPLED_BIT";
        case VK_IMAGE_USAGE_STORAGE_BIT: return "VK_IMAGE_USAGE_STORAGE_BIT";
        case VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT: return "VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT";
        case VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT: return "VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT";
        case VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT: return "VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT";
        case VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT: return "VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT";
        case VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT: return "VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT";
        case VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR: return "VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR";
        case VK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI: return "VK_IMAGE_USAGE_INVOCATION_MASK_BIT_HUAWEI";
        case VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM: return "VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR: return "VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR";
        case VK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR: return "VK_IMAGE_USAGE_VIDEO_DECODE_SRC_BIT_KHR";
        case VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR: return "VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR";
        case VK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR: return "VK_IMAGE_USAGE_VIDEO_ENCODE_DST_BIT_KHR";
        case VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR: return "VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR";
        case VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR: return "VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR";
#endif
        default: break;
    }
    return "Unknown VkImageUsageFlagBits";
}

std::string str_from_VkImageUsageFlags(VkImageUsageFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkImageUsageFlagBits(static_cast<VkImageUsageFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkImageUsageFlagBits(static_cast<VkImageUsageFlagBits>(0)));
    return result;
}

 VkMemoryHeapFlagBits str_to_VkMemoryHeapFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT"sv) return VK_MEMORY_HEAP_DEVICE_LOCAL_BIT;
    if (a_str == "VK_MEMORY_HEAP_MULTI_INSTANCE_BIT"sv) return VK_MEMORY_HEAP_MULTI_INSTANCE_BIT;
    if (a_str == "VK_MEMORY_HEAP_MULTI_INSTANCE_BIT_KHR"sv) return VK_MEMORY_HEAP_MULTI_INSTANCE_BIT_KHR;
    if (a_str == "VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM"sv) return VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkMemoryHeapFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkMemoryHeapFlagBits(VkMemoryHeapFlagBits e)
{
    switch (e) {
        case VK_MEMORY_HEAP_DEVICE_LOCAL_BIT: return "VK_MEMORY_HEAP_DEVICE_LOCAL_BIT";
        case VK_MEMORY_HEAP_MULTI_INSTANCE_BIT: return "VK_MEMORY_HEAP_MULTI_INSTANCE_BIT";
        case VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM: return "VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkMemoryHeapFlagBits";
}

std::string str_from_VkMemoryHeapFlags(VkMemoryHeapFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkMemoryHeapFlagBits(static_cast<VkMemoryHeapFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkMemoryHeapFlagBits(static_cast<VkMemoryHeapFlagBits>(0)));
    return result;
}

 VkMemoryPropertyFlagBits str_to_VkMemoryPropertyFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT"sv) return VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    if (a_str == "VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT"sv) return VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    if (a_str == "VK_MEMORY_PROPERTY_HOST_COHERENT_BIT"sv) return VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    if (a_str == "VK_MEMORY_PROPERTY_HOST_CACHED_BIT"sv) return VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
    if (a_str == "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT"sv) return VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
    if (a_str == "VK_MEMORY_PROPERTY_PROTECTED_BIT"sv) return VK_MEMORY_PROPERTY_PROTECTED_BIT;
    if (a_str == "VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD"sv) return VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD;
    if (a_str == "VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD"sv) return VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD;
    if (a_str == "VK_MEMORY_PROPERTY_RDMA_CAPABLE_BIT_NV"sv) return VK_MEMORY_PROPERTY_RDMA_CAPABLE_BIT_NV;
    if (a_str == "VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM"sv) return VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkMemoryPropertyFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkMemoryPropertyFlagBits(VkMemoryPropertyFlagBits e)
{
    switch (e) {
        case VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT: return "VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT";
        case VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT: return "VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT";
        case VK_MEMORY_PROPERTY_HOST_COHERENT_BIT: return "VK_MEMORY_PROPERTY_HOST_COHERENT_BIT";
        case VK_MEMORY_PROPERTY_HOST_CACHED_BIT: return "VK_MEMORY_PROPERTY_HOST_CACHED_BIT";
        case VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT: return "VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT";
        case VK_MEMORY_PROPERTY_PROTECTED_BIT: return "VK_MEMORY_PROPERTY_PROTECTED_BIT";
        case VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD: return "VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD";
        case VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD: return "VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD";
        case VK_MEMORY_PROPERTY_RDMA_CAPABLE_BIT_NV: return "VK_MEMORY_PROPERTY_RDMA_CAPABLE_BIT_NV";
        case VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM: return "VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkMemoryPropertyFlagBits";
}

std::string str_from_VkMemoryPropertyFlags(VkMemoryPropertyFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkMemoryPropertyFlagBits(static_cast<VkMemoryPropertyFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkMemoryPropertyFlagBits(static_cast<VkMemoryPropertyFlagBits>(0)));
    return result;
}

 VkQueueFlagBits str_to_VkQueueFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUEUE_GRAPHICS_BIT"sv) return VK_QUEUE_GRAPHICS_BIT;
    if (a_str == "VK_QUEUE_COMPUTE_BIT"sv) return VK_QUEUE_COMPUTE_BIT;
    if (a_str == "VK_QUEUE_TRANSFER_BIT"sv) return VK_QUEUE_TRANSFER_BIT;
    if (a_str == "VK_QUEUE_SPARSE_BINDING_BIT"sv) return VK_QUEUE_SPARSE_BINDING_BIT;
    if (a_str == "VK_QUEUE_PROTECTED_BIT"sv) return VK_QUEUE_PROTECTED_BIT;
    if (a_str == "VK_QUEUE_FLAG_BITS_MAX_ENUM"sv) return VK_QUEUE_FLAG_BITS_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_QUEUE_VIDEO_DECODE_BIT_KHR"sv) return VK_QUEUE_VIDEO_DECODE_BIT_KHR;
    if (a_str == "VK_QUEUE_VIDEO_ENCODE_BIT_KHR"sv) return VK_QUEUE_VIDEO_ENCODE_BIT_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueueFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkQueueFlagBits(VkQueueFlagBits e)
{
    switch (e) {
        case VK_QUEUE_GRAPHICS_BIT: return "VK_QUEUE_GRAPHICS_BIT";
        case VK_QUEUE_COMPUTE_BIT: return "VK_QUEUE_COMPUTE_BIT";
        case VK_QUEUE_TRANSFER_BIT: return "VK_QUEUE_TRANSFER_BIT";
        case VK_QUEUE_SPARSE_BINDING_BIT: return "VK_QUEUE_SPARSE_BINDING_BIT";
        case VK_QUEUE_PROTECTED_BIT: return "VK_QUEUE_PROTECTED_BIT";
        case VK_QUEUE_FLAG_BITS_MAX_ENUM: return "VK_QUEUE_FLAG_BITS_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_QUEUE_VIDEO_DECODE_BIT_KHR: return "VK_QUEUE_VIDEO_DECODE_BIT_KHR";
        case VK_QUEUE_VIDEO_ENCODE_BIT_KHR: return "VK_QUEUE_VIDEO_ENCODE_BIT_KHR";
#endif
        default: break;
    }
    return "Unknown VkQueueFlagBits";
}

std::string str_from_VkQueueFlags(VkQueueFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkQueueFlagBits(static_cast<VkQueueFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkQueueFlagBits(static_cast<VkQueueFlagBits>(0)));
    return result;
}

 VkDeviceQueueCreateFlagBits str_to_VkDeviceQueueCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEVICE_QUEUE_CREATE_PROTECTED_BIT"sv) return VK_DEVICE_QUEUE_CREATE_PROTECTED_BIT;
    if (a_str == "VK_DEVICE_QUEUE_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_DEVICE_QUEUE_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDeviceQueueCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkDeviceQueueCreateFlagBits(VkDeviceQueueCreateFlagBits e)
{
    switch (e) {
        case VK_DEVICE_QUEUE_CREATE_PROTECTED_BIT: return "VK_DEVICE_QUEUE_CREATE_PROTECTED_BIT";
        case VK_DEVICE_QUEUE_CREATE_FLAG_BITS_MAX_ENUM: return "VK_DEVICE_QUEUE_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDeviceQueueCreateFlagBits";
}

std::string str_from_VkDeviceQueueCreateFlags(VkDeviceQueueCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDeviceQueueCreateFlagBits(static_cast<VkDeviceQueueCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDeviceQueueCreateFlagBits(static_cast<VkDeviceQueueCreateFlagBits>(0)));
    return result;
}

 VkPipelineStageFlagBits str_to_VkPipelineStageFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT"sv) return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    if (a_str == "VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT"sv) return VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT;
    if (a_str == "VK_PIPELINE_STAGE_VERTEX_INPUT_BIT"sv) return VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
    if (a_str == "VK_PIPELINE_STAGE_VERTEX_SHADER_BIT"sv) return VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT"sv) return VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT"sv) return VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT"sv) return VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT"sv) return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT"sv) return VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    if (a_str == "VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT"sv) return VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
    if (a_str == "VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT"sv) return VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    if (a_str == "VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT"sv) return VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_TRANSFER_BIT"sv) return VK_PIPELINE_STAGE_TRANSFER_BIT;
    if (a_str == "VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT"sv) return VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    if (a_str == "VK_PIPELINE_STAGE_HOST_BIT"sv) return VK_PIPELINE_STAGE_HOST_BIT;
    if (a_str == "VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT"sv) return VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
    if (a_str == "VK_PIPELINE_STAGE_ALL_COMMANDS_BIT"sv) return VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    if (a_str == "VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT"sv) return VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT;
    if (a_str == "VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT"sv) return VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT;
    if (a_str == "VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR"sv) return VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR;
    if (a_str == "VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR"sv) return VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR;
    if (a_str == "VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV"sv) return VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV;
    if (a_str == "VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV"sv) return VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV;
    if (a_str == "VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT"sv) return VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT;
    if (a_str == "VK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR"sv) return VK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR;
    if (a_str == "VK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV"sv) return VK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV;
    if (a_str == "VK_PIPELINE_STAGE_NONE_KHR"sv) return VK_PIPELINE_STAGE_NONE_KHR;
    if (a_str == "VK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV"sv) return VK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV;
    if (a_str == "VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV"sv) return VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV;
    if (a_str == "VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV"sv) return VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV;
    if (a_str == "VK_PIPELINE_STAGE_FLAG_BITS_MAX_ENUM"sv) return VK_PIPELINE_STAGE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineStageFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineStageFlagBits(VkPipelineStageFlagBits e)
{
    switch (e) {
        case VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT: return "VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT";
        case VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT: return "VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT";
        case VK_PIPELINE_STAGE_VERTEX_INPUT_BIT: return "VK_PIPELINE_STAGE_VERTEX_INPUT_BIT";
        case VK_PIPELINE_STAGE_VERTEX_SHADER_BIT: return "VK_PIPELINE_STAGE_VERTEX_SHADER_BIT";
        case VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT: return "VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT";
        case VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT: return "VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT";
        case VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT: return "VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT";
        case VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT: return "VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT";
        case VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT: return "VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT";
        case VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT: return "VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT";
        case VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT: return "VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT";
        case VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT: return "VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT";
        case VK_PIPELINE_STAGE_TRANSFER_BIT: return "VK_PIPELINE_STAGE_TRANSFER_BIT";
        case VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT: return "VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT";
        case VK_PIPELINE_STAGE_HOST_BIT: return "VK_PIPELINE_STAGE_HOST_BIT";
        case VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT: return "VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT";
        case VK_PIPELINE_STAGE_ALL_COMMANDS_BIT: return "VK_PIPELINE_STAGE_ALL_COMMANDS_BIT";
        case VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT: return "VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT";
        case VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT: return "VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT";
        case VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR: return "VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR";
        case VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR: return "VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR";
        case VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV: return "VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV";
        case VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV: return "VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV";
        case VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT: return "VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT";
        case VK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR: return "VK_PIPELINE_STAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR";
        case VK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV: return "VK_PIPELINE_STAGE_COMMAND_PREPROCESS_BIT_NV";
        case VK_PIPELINE_STAGE_NONE_KHR: return "VK_PIPELINE_STAGE_NONE_KHR";
        case VK_PIPELINE_STAGE_FLAG_BITS_MAX_ENUM: return "VK_PIPELINE_STAGE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPipelineStageFlagBits";
}

std::string str_from_VkPipelineStageFlags(VkPipelineStageFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPipelineStageFlagBits(static_cast<VkPipelineStageFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPipelineStageFlagBits(static_cast<VkPipelineStageFlagBits>(0)));
    return result;
}

 VkSparseMemoryBindFlagBits str_to_VkSparseMemoryBindFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SPARSE_MEMORY_BIND_METADATA_BIT"sv) return VK_SPARSE_MEMORY_BIND_METADATA_BIT;
    if (a_str == "VK_SPARSE_MEMORY_BIND_FLAG_BITS_MAX_ENUM"sv) return VK_SPARSE_MEMORY_BIND_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSparseMemoryBindFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSparseMemoryBindFlagBits(VkSparseMemoryBindFlagBits e)
{
    switch (e) {
        case VK_SPARSE_MEMORY_BIND_METADATA_BIT: return "VK_SPARSE_MEMORY_BIND_METADATA_BIT";
        case VK_SPARSE_MEMORY_BIND_FLAG_BITS_MAX_ENUM: return "VK_SPARSE_MEMORY_BIND_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSparseMemoryBindFlagBits";
}

std::string str_from_VkSparseMemoryBindFlags(VkSparseMemoryBindFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSparseMemoryBindFlagBits(static_cast<VkSparseMemoryBindFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSparseMemoryBindFlagBits(static_cast<VkSparseMemoryBindFlagBits>(0)));
    return result;
}

 VkSparseImageFormatFlagBits str_to_VkSparseImageFormatFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SPARSE_IMAGE_FORMAT_SINGLE_MIPTAIL_BIT"sv) return VK_SPARSE_IMAGE_FORMAT_SINGLE_MIPTAIL_BIT;
    if (a_str == "VK_SPARSE_IMAGE_FORMAT_ALIGNED_MIP_SIZE_BIT"sv) return VK_SPARSE_IMAGE_FORMAT_ALIGNED_MIP_SIZE_BIT;
    if (a_str == "VK_SPARSE_IMAGE_FORMAT_NONSTANDARD_BLOCK_SIZE_BIT"sv) return VK_SPARSE_IMAGE_FORMAT_NONSTANDARD_BLOCK_SIZE_BIT;
    if (a_str == "VK_SPARSE_IMAGE_FORMAT_FLAG_BITS_MAX_ENUM"sv) return VK_SPARSE_IMAGE_FORMAT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSparseImageFormatFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSparseImageFormatFlagBits(VkSparseImageFormatFlagBits e)
{
    switch (e) {
        case VK_SPARSE_IMAGE_FORMAT_SINGLE_MIPTAIL_BIT: return "VK_SPARSE_IMAGE_FORMAT_SINGLE_MIPTAIL_BIT";
        case VK_SPARSE_IMAGE_FORMAT_ALIGNED_MIP_SIZE_BIT: return "VK_SPARSE_IMAGE_FORMAT_ALIGNED_MIP_SIZE_BIT";
        case VK_SPARSE_IMAGE_FORMAT_NONSTANDARD_BLOCK_SIZE_BIT: return "VK_SPARSE_IMAGE_FORMAT_NONSTANDARD_BLOCK_SIZE_BIT";
        case VK_SPARSE_IMAGE_FORMAT_FLAG_BITS_MAX_ENUM: return "VK_SPARSE_IMAGE_FORMAT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSparseImageFormatFlagBits";
}

std::string str_from_VkSparseImageFormatFlags(VkSparseImageFormatFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSparseImageFormatFlagBits(static_cast<VkSparseImageFormatFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSparseImageFormatFlagBits(static_cast<VkSparseImageFormatFlagBits>(0)));
    return result;
}

 VkFenceCreateFlagBits str_to_VkFenceCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FENCE_CREATE_SIGNALED_BIT"sv) return VK_FENCE_CREATE_SIGNALED_BIT;
    if (a_str == "VK_FENCE_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_FENCE_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFenceCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkFenceCreateFlagBits(VkFenceCreateFlagBits e)
{
    switch (e) {
        case VK_FENCE_CREATE_SIGNALED_BIT: return "VK_FENCE_CREATE_SIGNALED_BIT";
        case VK_FENCE_CREATE_FLAG_BITS_MAX_ENUM: return "VK_FENCE_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkFenceCreateFlagBits";
}

std::string str_from_VkFenceCreateFlags(VkFenceCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkFenceCreateFlagBits(static_cast<VkFenceCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkFenceCreateFlagBits(static_cast<VkFenceCreateFlagBits>(0)));
    return result;
}

 VkEventCreateFlagBits str_to_VkEventCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EVENT_CREATE_DEVICE_ONLY_BIT_KHR"sv) return VK_EVENT_CREATE_DEVICE_ONLY_BIT_KHR;
    if (a_str == "VK_EVENT_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_EVENT_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkEventCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkEventCreateFlagBits(VkEventCreateFlagBits e)
{
    switch (e) {
        case VK_EVENT_CREATE_DEVICE_ONLY_BIT_KHR: return "VK_EVENT_CREATE_DEVICE_ONLY_BIT_KHR";
        case VK_EVENT_CREATE_FLAG_BITS_MAX_ENUM: return "VK_EVENT_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkEventCreateFlagBits";
}

std::string str_from_VkEventCreateFlags(VkEventCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkEventCreateFlagBits(static_cast<VkEventCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkEventCreateFlagBits(static_cast<VkEventCreateFlagBits>(0)));
    return result;
}

 VkQueryPipelineStatisticFlagBits str_to_VkQueryPipelineStatisticFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT"sv) return VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT;
    if (a_str == "VK_QUERY_PIPELINE_STATISTIC_FLAG_BITS_MAX_ENUM"sv) return VK_QUERY_PIPELINE_STATISTIC_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueryPipelineStatisticFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkQueryPipelineStatisticFlagBits(VkQueryPipelineStatisticFlagBits e)
{
    switch (e) {
        case VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT: return "VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT: return "VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT: return "VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT: return "VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT: return "VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT: return "VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT: return "VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT: return "VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT: return "VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT: return "VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT: return "VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT";
        case VK_QUERY_PIPELINE_STATISTIC_FLAG_BITS_MAX_ENUM: return "VK_QUERY_PIPELINE_STATISTIC_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkQueryPipelineStatisticFlagBits";
}

std::string str_from_VkQueryPipelineStatisticFlags(VkQueryPipelineStatisticFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkQueryPipelineStatisticFlagBits(static_cast<VkQueryPipelineStatisticFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkQueryPipelineStatisticFlagBits(static_cast<VkQueryPipelineStatisticFlagBits>(0)));
    return result;
}

 VkQueryResultFlagBits str_to_VkQueryResultFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUERY_RESULT_64_BIT"sv) return VK_QUERY_RESULT_64_BIT;
    if (a_str == "VK_QUERY_RESULT_WAIT_BIT"sv) return VK_QUERY_RESULT_WAIT_BIT;
    if (a_str == "VK_QUERY_RESULT_WITH_AVAILABILITY_BIT"sv) return VK_QUERY_RESULT_WITH_AVAILABILITY_BIT;
    if (a_str == "VK_QUERY_RESULT_PARTIAL_BIT"sv) return VK_QUERY_RESULT_PARTIAL_BIT;
    if (a_str == "VK_QUERY_RESULT_FLAG_BITS_MAX_ENUM"sv) return VK_QUERY_RESULT_FLAG_BITS_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_QUERY_RESULT_WITH_STATUS_BIT_KHR"sv) return VK_QUERY_RESULT_WITH_STATUS_BIT_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueryResultFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkQueryResultFlagBits(VkQueryResultFlagBits e)
{
    switch (e) {
        case VK_QUERY_RESULT_64_BIT: return "VK_QUERY_RESULT_64_BIT";
        case VK_QUERY_RESULT_WAIT_BIT: return "VK_QUERY_RESULT_WAIT_BIT";
        case VK_QUERY_RESULT_WITH_AVAILABILITY_BIT: return "VK_QUERY_RESULT_WITH_AVAILABILITY_BIT";
        case VK_QUERY_RESULT_PARTIAL_BIT: return "VK_QUERY_RESULT_PARTIAL_BIT";
        case VK_QUERY_RESULT_FLAG_BITS_MAX_ENUM: return "VK_QUERY_RESULT_FLAG_BITS_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_QUERY_RESULT_WITH_STATUS_BIT_KHR: return "VK_QUERY_RESULT_WITH_STATUS_BIT_KHR";
#endif
        default: break;
    }
    return "Unknown VkQueryResultFlagBits";
}

std::string str_from_VkQueryResultFlags(VkQueryResultFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkQueryResultFlagBits(static_cast<VkQueryResultFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkQueryResultFlagBits(static_cast<VkQueryResultFlagBits>(0)));
    return result;
}

 VkBufferCreateFlagBits str_to_VkBufferCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BUFFER_CREATE_SPARSE_BINDING_BIT"sv) return VK_BUFFER_CREATE_SPARSE_BINDING_BIT;
    if (a_str == "VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT"sv) return VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT;
    if (a_str == "VK_BUFFER_CREATE_SPARSE_ALIASED_BIT"sv) return VK_BUFFER_CREATE_SPARSE_ALIASED_BIT;
    if (a_str == "VK_BUFFER_CREATE_PROTECTED_BIT"sv) return VK_BUFFER_CREATE_PROTECTED_BIT;
    if (a_str == "VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT"sv) return VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT;
    if (a_str == "VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_EXT"sv) return VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_EXT;
    if (a_str == "VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR"sv) return VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR;
    if (a_str == "VK_BUFFER_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_BUFFER_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBufferCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkBufferCreateFlagBits(VkBufferCreateFlagBits e)
{
    switch (e) {
        case VK_BUFFER_CREATE_SPARSE_BINDING_BIT: return "VK_BUFFER_CREATE_SPARSE_BINDING_BIT";
        case VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT: return "VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT";
        case VK_BUFFER_CREATE_SPARSE_ALIASED_BIT: return "VK_BUFFER_CREATE_SPARSE_ALIASED_BIT";
        case VK_BUFFER_CREATE_PROTECTED_BIT: return "VK_BUFFER_CREATE_PROTECTED_BIT";
        case VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT: return "VK_BUFFER_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT";
        case VK_BUFFER_CREATE_FLAG_BITS_MAX_ENUM: return "VK_BUFFER_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkBufferCreateFlagBits";
}

std::string str_from_VkBufferCreateFlags(VkBufferCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkBufferCreateFlagBits(static_cast<VkBufferCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkBufferCreateFlagBits(static_cast<VkBufferCreateFlagBits>(0)));
    return result;
}

 VkBufferUsageFlagBits str_to_VkBufferUsageFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BUFFER_USAGE_TRANSFER_SRC_BIT"sv) return VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    if (a_str == "VK_BUFFER_USAGE_TRANSFER_DST_BIT"sv) return VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    if (a_str == "VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT"sv) return VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT"sv) return VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT"sv) return VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_STORAGE_BUFFER_BIT"sv) return VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_INDEX_BUFFER_BIT"sv) return VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_VERTEX_BUFFER_BIT"sv) return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT"sv) return VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
    if (a_str == "VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT"sv) return VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    if (a_str == "VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT"sv) return VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT;
    if (a_str == "VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT"sv) return VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT;
    if (a_str == "VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT"sv) return VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT;
    if (a_str == "VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR"sv) return VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR"sv) return VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR"sv) return VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_RAY_TRACING_BIT_NV"sv) return VK_BUFFER_USAGE_RAY_TRACING_BIT_NV;
    if (a_str == "VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT_EXT"sv) return VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT_EXT;
    if (a_str == "VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT_KHR"sv) return VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM"sv) return VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM;
#ifdef VK_ENABLE_BETA_EXTENSIONS
    if (a_str == "VK_BUFFER_USAGE_VIDEO_DECODE_SRC_BIT_KHR"sv) return VK_BUFFER_USAGE_VIDEO_DECODE_SRC_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_VIDEO_DECODE_DST_BIT_KHR"sv) return VK_BUFFER_USAGE_VIDEO_DECODE_DST_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_VIDEO_ENCODE_DST_BIT_KHR"sv) return VK_BUFFER_USAGE_VIDEO_ENCODE_DST_BIT_KHR;
    if (a_str == "VK_BUFFER_USAGE_VIDEO_ENCODE_SRC_BIT_KHR"sv) return VK_BUFFER_USAGE_VIDEO_ENCODE_SRC_BIT_KHR;
#endif
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBufferUsageFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkBufferUsageFlagBits(VkBufferUsageFlagBits e)
{
    switch (e) {
        case VK_BUFFER_USAGE_TRANSFER_SRC_BIT: return "VK_BUFFER_USAGE_TRANSFER_SRC_BIT";
        case VK_BUFFER_USAGE_TRANSFER_DST_BIT: return "VK_BUFFER_USAGE_TRANSFER_DST_BIT";
        case VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT: return "VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT";
        case VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT: return "VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT";
        case VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT: return "VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT";
        case VK_BUFFER_USAGE_STORAGE_BUFFER_BIT: return "VK_BUFFER_USAGE_STORAGE_BUFFER_BIT";
        case VK_BUFFER_USAGE_INDEX_BUFFER_BIT: return "VK_BUFFER_USAGE_INDEX_BUFFER_BIT";
        case VK_BUFFER_USAGE_VERTEX_BUFFER_BIT: return "VK_BUFFER_USAGE_VERTEX_BUFFER_BIT";
        case VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT: return "VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT";
        case VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT: return "VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT";
        case VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT: return "VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT";
        case VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT: return "VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT";
        case VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT: return "VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT";
        case VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR: return "VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR";
        case VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR: return "VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR";
        case VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR: return "VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR";
        case VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM: return "VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM";
#ifdef VK_ENABLE_BETA_EXTENSIONS
        case VK_BUFFER_USAGE_VIDEO_DECODE_SRC_BIT_KHR: return "VK_BUFFER_USAGE_VIDEO_DECODE_SRC_BIT_KHR";
        case VK_BUFFER_USAGE_VIDEO_DECODE_DST_BIT_KHR: return "VK_BUFFER_USAGE_VIDEO_DECODE_DST_BIT_KHR";
        case VK_BUFFER_USAGE_VIDEO_ENCODE_DST_BIT_KHR: return "VK_BUFFER_USAGE_VIDEO_ENCODE_DST_BIT_KHR";
        case VK_BUFFER_USAGE_VIDEO_ENCODE_SRC_BIT_KHR: return "VK_BUFFER_USAGE_VIDEO_ENCODE_SRC_BIT_KHR";
#endif
        default: break;
    }
    return "Unknown VkBufferUsageFlagBits";
}

std::string str_from_VkBufferUsageFlags(VkBufferUsageFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkBufferUsageFlagBits(static_cast<VkBufferUsageFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkBufferUsageFlagBits(static_cast<VkBufferUsageFlagBits>(0)));
    return result;
}

 VkImageViewCreateFlagBits str_to_VkImageViewCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT"sv) return VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT;
    if (a_str == "VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DEFERRED_BIT_EXT"sv) return VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DEFERRED_BIT_EXT;
    if (a_str == "VK_IMAGE_VIEW_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_IMAGE_VIEW_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkImageViewCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkImageViewCreateFlagBits(VkImageViewCreateFlagBits e)
{
    switch (e) {
        case VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT: return "VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DYNAMIC_BIT_EXT";
        case VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DEFERRED_BIT_EXT: return "VK_IMAGE_VIEW_CREATE_FRAGMENT_DENSITY_MAP_DEFERRED_BIT_EXT";
        case VK_IMAGE_VIEW_CREATE_FLAG_BITS_MAX_ENUM: return "VK_IMAGE_VIEW_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkImageViewCreateFlagBits";
}

std::string str_from_VkImageViewCreateFlags(VkImageViewCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkImageViewCreateFlagBits(static_cast<VkImageViewCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkImageViewCreateFlagBits(static_cast<VkImageViewCreateFlagBits>(0)));
    return result;
}

 VkPipelineCacheCreateFlagBits str_to_VkPipelineCacheCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_CACHE_CREATE_EXTERNALLY_SYNCHRONIZED_BIT_EXT"sv) return VK_PIPELINE_CACHE_CREATE_EXTERNALLY_SYNCHRONIZED_BIT_EXT;
    if (a_str == "VK_PIPELINE_CACHE_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_PIPELINE_CACHE_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineCacheCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineCacheCreateFlagBits(VkPipelineCacheCreateFlagBits e)
{
    switch (e) {
        case VK_PIPELINE_CACHE_CREATE_EXTERNALLY_SYNCHRONIZED_BIT_EXT: return "VK_PIPELINE_CACHE_CREATE_EXTERNALLY_SYNCHRONIZED_BIT_EXT";
        case VK_PIPELINE_CACHE_CREATE_FLAG_BITS_MAX_ENUM: return "VK_PIPELINE_CACHE_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPipelineCacheCreateFlagBits";
}

std::string str_from_VkPipelineCacheCreateFlags(VkPipelineCacheCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPipelineCacheCreateFlagBits(static_cast<VkPipelineCacheCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPipelineCacheCreateFlagBits(static_cast<VkPipelineCacheCreateFlagBits>(0)));
    return result;
}

 VkColorComponentFlagBits str_to_VkColorComponentFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COLOR_COMPONENT_R_BIT"sv) return VK_COLOR_COMPONENT_R_BIT;
    if (a_str == "VK_COLOR_COMPONENT_G_BIT"sv) return VK_COLOR_COMPONENT_G_BIT;
    if (a_str == "VK_COLOR_COMPONENT_B_BIT"sv) return VK_COLOR_COMPONENT_B_BIT;
    if (a_str == "VK_COLOR_COMPONENT_A_BIT"sv) return VK_COLOR_COMPONENT_A_BIT;
    if (a_str == "VK_COLOR_COMPONENT_FLAG_BITS_MAX_ENUM"sv) return VK_COLOR_COMPONENT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkColorComponentFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkColorComponentFlagBits(VkColorComponentFlagBits e)
{
    switch (e) {
        case VK_COLOR_COMPONENT_R_BIT: return "VK_COLOR_COMPONENT_R_BIT";
        case VK_COLOR_COMPONENT_G_BIT: return "VK_COLOR_COMPONENT_G_BIT";
        case VK_COLOR_COMPONENT_B_BIT: return "VK_COLOR_COMPONENT_B_BIT";
        case VK_COLOR_COMPONENT_A_BIT: return "VK_COLOR_COMPONENT_A_BIT";
        case VK_COLOR_COMPONENT_FLAG_BITS_MAX_ENUM: return "VK_COLOR_COMPONENT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkColorComponentFlagBits";
}

std::string str_from_VkColorComponentFlags(VkColorComponentFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkColorComponentFlagBits(static_cast<VkColorComponentFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkColorComponentFlagBits(static_cast<VkColorComponentFlagBits>(0)));
    return result;
}

 VkPipelineCreateFlagBits str_to_VkPipelineCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT"sv) return VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
    if (a_str == "VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT"sv) return VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
    if (a_str == "VK_PIPELINE_CREATE_DERIVATIVE_BIT"sv) return VK_PIPELINE_CREATE_DERIVATIVE_BIT;
    if (a_str == "VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT"sv) return VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT;
    if (a_str == "VK_PIPELINE_CREATE_DISPATCH_BASE_BIT"sv) return VK_PIPELINE_CREATE_DISPATCH_BASE_BIT;
    if (a_str == "VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR"sv) return VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR;
    if (a_str == "VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_DENSITY_MAP_ATTACHMENT_BIT_EXT"sv) return VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_DENSITY_MAP_ATTACHMENT_BIT_EXT;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR"sv) return VK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV"sv) return VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV;
    if (a_str == "VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR"sv) return VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR"sv) return VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV"sv) return VK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV;
    if (a_str == "VK_PIPELINE_CREATE_LIBRARY_BIT_KHR"sv) return VK_PIPELINE_CREATE_LIBRARY_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT_EXT"sv) return VK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT_EXT;
    if (a_str == "VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT_EXT"sv) return VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT_EXT;
    if (a_str == "VK_PIPELINE_CREATE_RAY_TRACING_ALLOW_MOTION_BIT_NV"sv) return VK_PIPELINE_CREATE_RAY_TRACING_ALLOW_MOTION_BIT_NV;
    if (a_str == "VK_PIPELINE_CREATE_DISPATCH_BASE"sv) return VK_PIPELINE_CREATE_DISPATCH_BASE;
    if (a_str == "VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT_KHR"sv) return VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT_KHR;
    if (a_str == "VK_PIPELINE_CREATE_DISPATCH_BASE_KHR"sv) return VK_PIPELINE_CREATE_DISPATCH_BASE_KHR;
    if (a_str == "VK_PIPELINE_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_PIPELINE_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineCreateFlagBits(VkPipelineCreateFlagBits e)
{
    switch (e) {
        case VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT: return "VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT";
        case VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT: return "VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT";
        case VK_PIPELINE_CREATE_DERIVATIVE_BIT: return "VK_PIPELINE_CREATE_DERIVATIVE_BIT";
        case VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT: return "VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT";
        case VK_PIPELINE_CREATE_DISPATCH_BASE_BIT: return "VK_PIPELINE_CREATE_DISPATCH_BASE_BIT";
        case VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR: return "VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR";
        case VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_DENSITY_MAP_ATTACHMENT_BIT_EXT: return "VK_PIPELINE_RASTERIZATION_STATE_CREATE_FRAGMENT_DENSITY_MAP_ATTACHMENT_BIT_EXT";
        case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_ANY_HIT_SHADERS_BIT_KHR";
        case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_CLOSEST_HIT_SHADERS_BIT_KHR";
        case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_MISS_SHADERS_BIT_KHR";
        case VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_NO_NULL_INTERSECTION_SHADERS_BIT_KHR";
        case VK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_SKIP_TRIANGLES_BIT_KHR";
        case VK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_SKIP_AABBS_BIT_KHR";
        case VK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR: return "VK_PIPELINE_CREATE_RAY_TRACING_SHADER_GROUP_HANDLE_CAPTURE_REPLAY_BIT_KHR";
        case VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV: return "VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV";
        case VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR: return "VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR";
        case VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR: return "VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR";
        case VK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV: return "VK_PIPELINE_CREATE_INDIRECT_BINDABLE_BIT_NV";
        case VK_PIPELINE_CREATE_LIBRARY_BIT_KHR: return "VK_PIPELINE_CREATE_LIBRARY_BIT_KHR";
        case VK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT_EXT: return "VK_PIPELINE_CREATE_FAIL_ON_PIPELINE_COMPILE_REQUIRED_BIT_EXT";
        case VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT_EXT: return "VK_PIPELINE_CREATE_EARLY_RETURN_ON_FAILURE_BIT_EXT";
        case VK_PIPELINE_CREATE_RAY_TRACING_ALLOW_MOTION_BIT_NV: return "VK_PIPELINE_CREATE_RAY_TRACING_ALLOW_MOTION_BIT_NV";
        case VK_PIPELINE_CREATE_FLAG_BITS_MAX_ENUM: return "VK_PIPELINE_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPipelineCreateFlagBits";
}

std::string str_from_VkPipelineCreateFlags(VkPipelineCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPipelineCreateFlagBits(static_cast<VkPipelineCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPipelineCreateFlagBits(static_cast<VkPipelineCreateFlagBits>(0)));
    return result;
}

 VkPipelineShaderStageCreateFlagBits str_to_VkPipelineShaderStageCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT_EXT"sv) return VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT_EXT;
    if (a_str == "VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT_EXT"sv) return VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT_EXT;
    if (a_str == "VK_PIPELINE_SHADER_STAGE_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_PIPELINE_SHADER_STAGE_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineShaderStageCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineShaderStageCreateFlagBits(VkPipelineShaderStageCreateFlagBits e)
{
    switch (e) {
        case VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT_EXT: return "VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT_EXT";
        case VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT_EXT: return "VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT_EXT";
        case VK_PIPELINE_SHADER_STAGE_CREATE_FLAG_BITS_MAX_ENUM: return "VK_PIPELINE_SHADER_STAGE_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPipelineShaderStageCreateFlagBits";
}

std::string str_from_VkPipelineShaderStageCreateFlags(VkPipelineShaderStageCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPipelineShaderStageCreateFlagBits(static_cast<VkPipelineShaderStageCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPipelineShaderStageCreateFlagBits(static_cast<VkPipelineShaderStageCreateFlagBits>(0)));
    return result;
}

 VkShaderStageFlagBits str_to_VkShaderStageFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHADER_STAGE_VERTEX_BIT"sv) return VK_SHADER_STAGE_VERTEX_BIT;
    if (a_str == "VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT"sv) return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    if (a_str == "VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT"sv) return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    if (a_str == "VK_SHADER_STAGE_GEOMETRY_BIT"sv) return VK_SHADER_STAGE_GEOMETRY_BIT;
    if (a_str == "VK_SHADER_STAGE_FRAGMENT_BIT"sv) return VK_SHADER_STAGE_FRAGMENT_BIT;
    if (a_str == "VK_SHADER_STAGE_COMPUTE_BIT"sv) return VK_SHADER_STAGE_COMPUTE_BIT;
    if (a_str == "VK_SHADER_STAGE_ALL_GRAPHICS"sv) return VK_SHADER_STAGE_ALL_GRAPHICS;
    if (a_str == "VK_SHADER_STAGE_ALL"sv) return VK_SHADER_STAGE_ALL;
    if (a_str == "VK_SHADER_STAGE_RAYGEN_BIT_KHR"sv) return VK_SHADER_STAGE_RAYGEN_BIT_KHR;
    if (a_str == "VK_SHADER_STAGE_ANY_HIT_BIT_KHR"sv) return VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
    if (a_str == "VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR"sv) return VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
    if (a_str == "VK_SHADER_STAGE_MISS_BIT_KHR"sv) return VK_SHADER_STAGE_MISS_BIT_KHR;
    if (a_str == "VK_SHADER_STAGE_INTERSECTION_BIT_KHR"sv) return VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
    if (a_str == "VK_SHADER_STAGE_CALLABLE_BIT_KHR"sv) return VK_SHADER_STAGE_CALLABLE_BIT_KHR;
    if (a_str == "VK_SHADER_STAGE_TASK_BIT_NV"sv) return VK_SHADER_STAGE_TASK_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_MESH_BIT_NV"sv) return VK_SHADER_STAGE_MESH_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_SUBPASS_SHADING_BIT_HUAWEI"sv) return VK_SHADER_STAGE_SUBPASS_SHADING_BIT_HUAWEI;
    if (a_str == "VK_SHADER_STAGE_RAYGEN_BIT_NV"sv) return VK_SHADER_STAGE_RAYGEN_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_ANY_HIT_BIT_NV"sv) return VK_SHADER_STAGE_ANY_HIT_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV"sv) return VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_MISS_BIT_NV"sv) return VK_SHADER_STAGE_MISS_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_INTERSECTION_BIT_NV"sv) return VK_SHADER_STAGE_INTERSECTION_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_CALLABLE_BIT_NV"sv) return VK_SHADER_STAGE_CALLABLE_BIT_NV;
    if (a_str == "VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM"sv) return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkShaderStageFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkShaderStageFlagBits(VkShaderStageFlagBits e)
{
    switch (e) {
        case VK_SHADER_STAGE_VERTEX_BIT: return "VK_SHADER_STAGE_VERTEX_BIT";
        case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT: return "VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT";
        case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT: return "VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT";
        case VK_SHADER_STAGE_GEOMETRY_BIT: return "VK_SHADER_STAGE_GEOMETRY_BIT";
        case VK_SHADER_STAGE_FRAGMENT_BIT: return "VK_SHADER_STAGE_FRAGMENT_BIT";
        case VK_SHADER_STAGE_COMPUTE_BIT: return "VK_SHADER_STAGE_COMPUTE_BIT";
        case VK_SHADER_STAGE_ALL_GRAPHICS: return "VK_SHADER_STAGE_ALL_GRAPHICS";
        case VK_SHADER_STAGE_ALL: return "VK_SHADER_STAGE_ALL";
        case VK_SHADER_STAGE_RAYGEN_BIT_KHR: return "VK_SHADER_STAGE_RAYGEN_BIT_KHR";
        case VK_SHADER_STAGE_ANY_HIT_BIT_KHR: return "VK_SHADER_STAGE_ANY_HIT_BIT_KHR";
        case VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR: return "VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR";
        case VK_SHADER_STAGE_MISS_BIT_KHR: return "VK_SHADER_STAGE_MISS_BIT_KHR";
        case VK_SHADER_STAGE_INTERSECTION_BIT_KHR: return "VK_SHADER_STAGE_INTERSECTION_BIT_KHR";
        case VK_SHADER_STAGE_CALLABLE_BIT_KHR: return "VK_SHADER_STAGE_CALLABLE_BIT_KHR";
        case VK_SHADER_STAGE_TASK_BIT_NV: return "VK_SHADER_STAGE_TASK_BIT_NV";
        case VK_SHADER_STAGE_MESH_BIT_NV: return "VK_SHADER_STAGE_MESH_BIT_NV";
        case VK_SHADER_STAGE_SUBPASS_SHADING_BIT_HUAWEI: return "VK_SHADER_STAGE_SUBPASS_SHADING_BIT_HUAWEI";
        default: break;
    }
    return "Unknown VkShaderStageFlagBits";
}

std::string str_from_VkShaderStageFlags(VkShaderStageFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkShaderStageFlagBits(static_cast<VkShaderStageFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkShaderStageFlagBits(static_cast<VkShaderStageFlagBits>(0)));
    return result;
}

 VkCullModeFlagBits str_to_VkCullModeFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_CULL_MODE_NONE"sv) return VK_CULL_MODE_NONE;
    if (a_str == "VK_CULL_MODE_FRONT_BIT"sv) return VK_CULL_MODE_FRONT_BIT;
    if (a_str == "VK_CULL_MODE_BACK_BIT"sv) return VK_CULL_MODE_BACK_BIT;
    if (a_str == "VK_CULL_MODE_FRONT_AND_BACK"sv) return VK_CULL_MODE_FRONT_AND_BACK;
    if (a_str == "VK_CULL_MODE_FLAG_BITS_MAX_ENUM"sv) return VK_CULL_MODE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCullModeFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkCullModeFlagBits(VkCullModeFlagBits e)
{
    switch (e) {
        case VK_CULL_MODE_NONE: return "VK_CULL_MODE_NONE";
        case VK_CULL_MODE_FRONT_BIT: return "VK_CULL_MODE_FRONT_BIT";
        case VK_CULL_MODE_BACK_BIT: return "VK_CULL_MODE_BACK_BIT";
        case VK_CULL_MODE_FRONT_AND_BACK: return "VK_CULL_MODE_FRONT_AND_BACK";
        case VK_CULL_MODE_FLAG_BITS_MAX_ENUM: return "VK_CULL_MODE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCullModeFlagBits";
}

std::string str_from_VkCullModeFlags(VkCullModeFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkCullModeFlagBits(static_cast<VkCullModeFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkCullModeFlagBits(static_cast<VkCullModeFlagBits>(0)));
    return result;
}

 VkSamplerCreateFlagBits str_to_VkSamplerCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLER_CREATE_SUBSAMPLED_BIT_EXT"sv) return VK_SAMPLER_CREATE_SUBSAMPLED_BIT_EXT;
    if (a_str == "VK_SAMPLER_CREATE_SUBSAMPLED_COARSE_RECONSTRUCTION_BIT_EXT"sv) return VK_SAMPLER_CREATE_SUBSAMPLED_COARSE_RECONSTRUCTION_BIT_EXT;
    if (a_str == "VK_SAMPLER_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_SAMPLER_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSamplerCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSamplerCreateFlagBits(VkSamplerCreateFlagBits e)
{
    switch (e) {
        case VK_SAMPLER_CREATE_SUBSAMPLED_BIT_EXT: return "VK_SAMPLER_CREATE_SUBSAMPLED_BIT_EXT";
        case VK_SAMPLER_CREATE_SUBSAMPLED_COARSE_RECONSTRUCTION_BIT_EXT: return "VK_SAMPLER_CREATE_SUBSAMPLED_COARSE_RECONSTRUCTION_BIT_EXT";
        case VK_SAMPLER_CREATE_FLAG_BITS_MAX_ENUM: return "VK_SAMPLER_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSamplerCreateFlagBits";
}

std::string str_from_VkSamplerCreateFlags(VkSamplerCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSamplerCreateFlagBits(static_cast<VkSamplerCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSamplerCreateFlagBits(static_cast<VkSamplerCreateFlagBits>(0)));
    return result;
}

 VkDescriptorPoolCreateFlagBits str_to_VkDescriptorPoolCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT"sv) return VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    if (a_str == "VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT"sv) return VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
    if (a_str == "VK_DESCRIPTOR_POOL_CREATE_HOST_ONLY_BIT_VALVE"sv) return VK_DESCRIPTOR_POOL_CREATE_HOST_ONLY_BIT_VALVE;
    if (a_str == "VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT_EXT"sv) return VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT_EXT;
    if (a_str == "VK_DESCRIPTOR_POOL_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_DESCRIPTOR_POOL_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDescriptorPoolCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkDescriptorPoolCreateFlagBits(VkDescriptorPoolCreateFlagBits e)
{
    switch (e) {
        case VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT: return "VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT";
        case VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT: return "VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT";
        case VK_DESCRIPTOR_POOL_CREATE_HOST_ONLY_BIT_VALVE: return "VK_DESCRIPTOR_POOL_CREATE_HOST_ONLY_BIT_VALVE";
        case VK_DESCRIPTOR_POOL_CREATE_FLAG_BITS_MAX_ENUM: return "VK_DESCRIPTOR_POOL_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDescriptorPoolCreateFlagBits";
}

std::string str_from_VkDescriptorPoolCreateFlags(VkDescriptorPoolCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDescriptorPoolCreateFlagBits(static_cast<VkDescriptorPoolCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDescriptorPoolCreateFlagBits(static_cast<VkDescriptorPoolCreateFlagBits>(0)));
    return result;
}

 VkDescriptorSetLayoutCreateFlagBits str_to_VkDescriptorSetLayoutCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT"sv) return VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
    if (a_str == "VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR"sv) return VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR;
    if (a_str == "VK_DESCRIPTOR_SET_LAYOUT_CREATE_HOST_ONLY_POOL_BIT_VALVE"sv) return VK_DESCRIPTOR_SET_LAYOUT_CREATE_HOST_ONLY_POOL_BIT_VALVE;
    if (a_str == "VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT"sv) return VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT;
    if (a_str == "VK_DESCRIPTOR_SET_LAYOUT_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_DESCRIPTOR_SET_LAYOUT_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDescriptorSetLayoutCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkDescriptorSetLayoutCreateFlagBits(VkDescriptorSetLayoutCreateFlagBits e)
{
    switch (e) {
        case VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT: return "VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT";
        case VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR: return "VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR";
        case VK_DESCRIPTOR_SET_LAYOUT_CREATE_HOST_ONLY_POOL_BIT_VALVE: return "VK_DESCRIPTOR_SET_LAYOUT_CREATE_HOST_ONLY_POOL_BIT_VALVE";
        case VK_DESCRIPTOR_SET_LAYOUT_CREATE_FLAG_BITS_MAX_ENUM: return "VK_DESCRIPTOR_SET_LAYOUT_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDescriptorSetLayoutCreateFlagBits";
}

std::string str_from_VkDescriptorSetLayoutCreateFlags(VkDescriptorSetLayoutCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDescriptorSetLayoutCreateFlagBits(static_cast<VkDescriptorSetLayoutCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDescriptorSetLayoutCreateFlagBits(static_cast<VkDescriptorSetLayoutCreateFlagBits>(0)));
    return result;
}

 VkAttachmentDescriptionFlagBits str_to_VkAttachmentDescriptionFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT"sv) return VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT;
    if (a_str == "VK_ATTACHMENT_DESCRIPTION_FLAG_BITS_MAX_ENUM"sv) return VK_ATTACHMENT_DESCRIPTION_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAttachmentDescriptionFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkAttachmentDescriptionFlagBits(VkAttachmentDescriptionFlagBits e)
{
    switch (e) {
        case VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT: return "VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT";
        case VK_ATTACHMENT_DESCRIPTION_FLAG_BITS_MAX_ENUM: return "VK_ATTACHMENT_DESCRIPTION_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkAttachmentDescriptionFlagBits";
}

std::string str_from_VkAttachmentDescriptionFlags(VkAttachmentDescriptionFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkAttachmentDescriptionFlagBits(static_cast<VkAttachmentDescriptionFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkAttachmentDescriptionFlagBits(static_cast<VkAttachmentDescriptionFlagBits>(0)));
    return result;
}

 VkDependencyFlagBits str_to_VkDependencyFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEPENDENCY_BY_REGION_BIT"sv) return VK_DEPENDENCY_BY_REGION_BIT;
    if (a_str == "VK_DEPENDENCY_DEVICE_GROUP_BIT"sv) return VK_DEPENDENCY_DEVICE_GROUP_BIT;
    if (a_str == "VK_DEPENDENCY_VIEW_LOCAL_BIT"sv) return VK_DEPENDENCY_VIEW_LOCAL_BIT;
    if (a_str == "VK_DEPENDENCY_VIEW_LOCAL_BIT_KHR"sv) return VK_DEPENDENCY_VIEW_LOCAL_BIT_KHR;
    if (a_str == "VK_DEPENDENCY_DEVICE_GROUP_BIT_KHR"sv) return VK_DEPENDENCY_DEVICE_GROUP_BIT_KHR;
    if (a_str == "VK_DEPENDENCY_FLAG_BITS_MAX_ENUM"sv) return VK_DEPENDENCY_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDependencyFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkDependencyFlagBits(VkDependencyFlagBits e)
{
    switch (e) {
        case VK_DEPENDENCY_BY_REGION_BIT: return "VK_DEPENDENCY_BY_REGION_BIT";
        case VK_DEPENDENCY_DEVICE_GROUP_BIT: return "VK_DEPENDENCY_DEVICE_GROUP_BIT";
        case VK_DEPENDENCY_VIEW_LOCAL_BIT: return "VK_DEPENDENCY_VIEW_LOCAL_BIT";
        case VK_DEPENDENCY_FLAG_BITS_MAX_ENUM: return "VK_DEPENDENCY_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDependencyFlagBits";
}

std::string str_from_VkDependencyFlags(VkDependencyFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDependencyFlagBits(static_cast<VkDependencyFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDependencyFlagBits(static_cast<VkDependencyFlagBits>(0)));
    return result;
}

 VkFramebufferCreateFlagBits str_to_VkFramebufferCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT"sv) return VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT;
    if (a_str == "VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT_KHR"sv) return VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT_KHR;
    if (a_str == "VK_FRAMEBUFFER_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_FRAMEBUFFER_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFramebufferCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkFramebufferCreateFlagBits(VkFramebufferCreateFlagBits e)
{
    switch (e) {
        case VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT: return "VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT";
        case VK_FRAMEBUFFER_CREATE_FLAG_BITS_MAX_ENUM: return "VK_FRAMEBUFFER_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkFramebufferCreateFlagBits";
}

std::string str_from_VkFramebufferCreateFlags(VkFramebufferCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkFramebufferCreateFlagBits(static_cast<VkFramebufferCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkFramebufferCreateFlagBits(static_cast<VkFramebufferCreateFlagBits>(0)));
    return result;
}

 VkRenderPassCreateFlagBits str_to_VkRenderPassCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_RENDER_PASS_CREATE_TRANSFORM_BIT_QCOM"sv) return VK_RENDER_PASS_CREATE_TRANSFORM_BIT_QCOM;
    if (a_str == "VK_RENDER_PASS_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_RENDER_PASS_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkRenderPassCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkRenderPassCreateFlagBits(VkRenderPassCreateFlagBits e)
{
    switch (e) {
        case VK_RENDER_PASS_CREATE_TRANSFORM_BIT_QCOM: return "VK_RENDER_PASS_CREATE_TRANSFORM_BIT_QCOM";
        case VK_RENDER_PASS_CREATE_FLAG_BITS_MAX_ENUM: return "VK_RENDER_PASS_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkRenderPassCreateFlagBits";
}

std::string str_from_VkRenderPassCreateFlags(VkRenderPassCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkRenderPassCreateFlagBits(static_cast<VkRenderPassCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkRenderPassCreateFlagBits(static_cast<VkRenderPassCreateFlagBits>(0)));
    return result;
}

 VkSubpassDescriptionFlagBits str_to_VkSubpassDescriptionFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX"sv) return VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX;
    if (a_str == "VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX"sv) return VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX;
    if (a_str == "VK_SUBPASS_DESCRIPTION_FRAGMENT_REGION_BIT_QCOM"sv) return VK_SUBPASS_DESCRIPTION_FRAGMENT_REGION_BIT_QCOM;
    if (a_str == "VK_SUBPASS_DESCRIPTION_SHADER_RESOLVE_BIT_QCOM"sv) return VK_SUBPASS_DESCRIPTION_SHADER_RESOLVE_BIT_QCOM;
    if (a_str == "VK_SUBPASS_DESCRIPTION_FLAG_BITS_MAX_ENUM"sv) return VK_SUBPASS_DESCRIPTION_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSubpassDescriptionFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSubpassDescriptionFlagBits(VkSubpassDescriptionFlagBits e)
{
    switch (e) {
        case VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX: return "VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX";
        case VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX: return "VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX";
        case VK_SUBPASS_DESCRIPTION_FRAGMENT_REGION_BIT_QCOM: return "VK_SUBPASS_DESCRIPTION_FRAGMENT_REGION_BIT_QCOM";
        case VK_SUBPASS_DESCRIPTION_SHADER_RESOLVE_BIT_QCOM: return "VK_SUBPASS_DESCRIPTION_SHADER_RESOLVE_BIT_QCOM";
        case VK_SUBPASS_DESCRIPTION_FLAG_BITS_MAX_ENUM: return "VK_SUBPASS_DESCRIPTION_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSubpassDescriptionFlagBits";
}

std::string str_from_VkSubpassDescriptionFlags(VkSubpassDescriptionFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSubpassDescriptionFlagBits(static_cast<VkSubpassDescriptionFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSubpassDescriptionFlagBits(static_cast<VkSubpassDescriptionFlagBits>(0)));
    return result;
}

 VkCommandPoolCreateFlagBits str_to_VkCommandPoolCreateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMMAND_POOL_CREATE_TRANSIENT_BIT"sv) return VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    if (a_str == "VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT"sv) return VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    if (a_str == "VK_COMMAND_POOL_CREATE_PROTECTED_BIT"sv) return VK_COMMAND_POOL_CREATE_PROTECTED_BIT;
    if (a_str == "VK_COMMAND_POOL_CREATE_FLAG_BITS_MAX_ENUM"sv) return VK_COMMAND_POOL_CREATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCommandPoolCreateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkCommandPoolCreateFlagBits(VkCommandPoolCreateFlagBits e)
{
    switch (e) {
        case VK_COMMAND_POOL_CREATE_TRANSIENT_BIT: return "VK_COMMAND_POOL_CREATE_TRANSIENT_BIT";
        case VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT: return "VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT";
        case VK_COMMAND_POOL_CREATE_PROTECTED_BIT: return "VK_COMMAND_POOL_CREATE_PROTECTED_BIT";
        case VK_COMMAND_POOL_CREATE_FLAG_BITS_MAX_ENUM: return "VK_COMMAND_POOL_CREATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCommandPoolCreateFlagBits";
}

std::string str_from_VkCommandPoolCreateFlags(VkCommandPoolCreateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkCommandPoolCreateFlagBits(static_cast<VkCommandPoolCreateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkCommandPoolCreateFlagBits(static_cast<VkCommandPoolCreateFlagBits>(0)));
    return result;
}

 VkCommandPoolResetFlagBits str_to_VkCommandPoolResetFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT"sv) return VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT;
    if (a_str == "VK_COMMAND_POOL_RESET_FLAG_BITS_MAX_ENUM"sv) return VK_COMMAND_POOL_RESET_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCommandPoolResetFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkCommandPoolResetFlagBits(VkCommandPoolResetFlagBits e)
{
    switch (e) {
        case VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT: return "VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT";
        case VK_COMMAND_POOL_RESET_FLAG_BITS_MAX_ENUM: return "VK_COMMAND_POOL_RESET_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCommandPoolResetFlagBits";
}

std::string str_from_VkCommandPoolResetFlags(VkCommandPoolResetFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkCommandPoolResetFlagBits(static_cast<VkCommandPoolResetFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkCommandPoolResetFlagBits(static_cast<VkCommandPoolResetFlagBits>(0)));
    return result;
}

 VkCommandBufferUsageFlagBits str_to_VkCommandBufferUsageFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT"sv) return VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    if (a_str == "VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT"sv) return VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
    if (a_str == "VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT"sv) return VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
    if (a_str == "VK_COMMAND_BUFFER_USAGE_FLAG_BITS_MAX_ENUM"sv) return VK_COMMAND_BUFFER_USAGE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCommandBufferUsageFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkCommandBufferUsageFlagBits(VkCommandBufferUsageFlagBits e)
{
    switch (e) {
        case VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT: return "VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT";
        case VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT: return "VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT";
        case VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT: return "VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT";
        case VK_COMMAND_BUFFER_USAGE_FLAG_BITS_MAX_ENUM: return "VK_COMMAND_BUFFER_USAGE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCommandBufferUsageFlagBits";
}

std::string str_from_VkCommandBufferUsageFlags(VkCommandBufferUsageFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkCommandBufferUsageFlagBits(static_cast<VkCommandBufferUsageFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkCommandBufferUsageFlagBits(static_cast<VkCommandBufferUsageFlagBits>(0)));
    return result;
}

 VkQueryControlFlagBits str_to_VkQueryControlFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUERY_CONTROL_PRECISE_BIT"sv) return VK_QUERY_CONTROL_PRECISE_BIT;
    if (a_str == "VK_QUERY_CONTROL_FLAG_BITS_MAX_ENUM"sv) return VK_QUERY_CONTROL_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueryControlFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkQueryControlFlagBits(VkQueryControlFlagBits e)
{
    switch (e) {
        case VK_QUERY_CONTROL_PRECISE_BIT: return "VK_QUERY_CONTROL_PRECISE_BIT";
        case VK_QUERY_CONTROL_FLAG_BITS_MAX_ENUM: return "VK_QUERY_CONTROL_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkQueryControlFlagBits";
}

std::string str_from_VkQueryControlFlags(VkQueryControlFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkQueryControlFlagBits(static_cast<VkQueryControlFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkQueryControlFlagBits(static_cast<VkQueryControlFlagBits>(0)));
    return result;
}

 VkCommandBufferResetFlagBits str_to_VkCommandBufferResetFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT"sv) return VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT;
    if (a_str == "VK_COMMAND_BUFFER_RESET_FLAG_BITS_MAX_ENUM"sv) return VK_COMMAND_BUFFER_RESET_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCommandBufferResetFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkCommandBufferResetFlagBits(VkCommandBufferResetFlagBits e)
{
    switch (e) {
        case VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT: return "VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT";
        case VK_COMMAND_BUFFER_RESET_FLAG_BITS_MAX_ENUM: return "VK_COMMAND_BUFFER_RESET_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkCommandBufferResetFlagBits";
}

std::string str_from_VkCommandBufferResetFlags(VkCommandBufferResetFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkCommandBufferResetFlagBits(static_cast<VkCommandBufferResetFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkCommandBufferResetFlagBits(static_cast<VkCommandBufferResetFlagBits>(0)));
    return result;
}

 VkStencilFaceFlagBits str_to_VkStencilFaceFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_STENCIL_FACE_FRONT_BIT"sv) return VK_STENCIL_FACE_FRONT_BIT;
    if (a_str == "VK_STENCIL_FACE_BACK_BIT"sv) return VK_STENCIL_FACE_BACK_BIT;
    if (a_str == "VK_STENCIL_FACE_FRONT_AND_BACK"sv) return VK_STENCIL_FACE_FRONT_AND_BACK;
    if (a_str == "VK_STENCIL_FRONT_AND_BACK"sv) return VK_STENCIL_FRONT_AND_BACK;
    if (a_str == "VK_STENCIL_FACE_FLAG_BITS_MAX_ENUM"sv) return VK_STENCIL_FACE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkStencilFaceFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkStencilFaceFlagBits(VkStencilFaceFlagBits e)
{
    switch (e) {
        case VK_STENCIL_FACE_FRONT_BIT: return "VK_STENCIL_FACE_FRONT_BIT";
        case VK_STENCIL_FACE_BACK_BIT: return "VK_STENCIL_FACE_BACK_BIT";
        case VK_STENCIL_FACE_FRONT_AND_BACK: return "VK_STENCIL_FACE_FRONT_AND_BACK";
        case VK_STENCIL_FACE_FLAG_BITS_MAX_ENUM: return "VK_STENCIL_FACE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkStencilFaceFlagBits";
}

std::string str_from_VkStencilFaceFlags(VkStencilFaceFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkStencilFaceFlagBits(static_cast<VkStencilFaceFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkStencilFaceFlagBits(static_cast<VkStencilFaceFlagBits>(0)));
    return result;
}

 #endif
#if VK_VERSION_1_1
VkPointClippingBehavior str_to_VkPointClippingBehavior(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES"sv) return VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES;
    if (a_str == "VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY"sv) return VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY;
    if (a_str == "VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES_KHR"sv) return VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES_KHR;
    if (a_str == "VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY_KHR"sv) return VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY_KHR;
    if (a_str == "VK_POINT_CLIPPING_BEHAVIOR_MAX_ENUM"sv) return VK_POINT_CLIPPING_BEHAVIOR_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPointClippingBehavior>(0x7FFFFFFF);
}

std::string_view str_from_VkPointClippingBehavior(VkPointClippingBehavior e)
{
    switch (e) {
        case VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES: return "VK_POINT_CLIPPING_BEHAVIOR_ALL_CLIP_PLANES";
        case VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY: return "VK_POINT_CLIPPING_BEHAVIOR_USER_CLIP_PLANES_ONLY";
        case VK_POINT_CLIPPING_BEHAVIOR_MAX_ENUM: return "VK_POINT_CLIPPING_BEHAVIOR_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPointClippingBehavior";
}

VkTessellationDomainOrigin str_to_VkTessellationDomainOrigin(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT"sv) return VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT;
    if (a_str == "VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT"sv) return VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT;
    if (a_str == "VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT_KHR"sv) return VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT_KHR;
    if (a_str == "VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT_KHR"sv) return VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT_KHR;
    if (a_str == "VK_TESSELLATION_DOMAIN_ORIGIN_MAX_ENUM"sv) return VK_TESSELLATION_DOMAIN_ORIGIN_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkTessellationDomainOrigin>(0x7FFFFFFF);
}

std::string_view str_from_VkTessellationDomainOrigin(VkTessellationDomainOrigin e)
{
    switch (e) {
        case VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT: return "VK_TESSELLATION_DOMAIN_ORIGIN_UPPER_LEFT";
        case VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT: return "VK_TESSELLATION_DOMAIN_ORIGIN_LOWER_LEFT";
        case VK_TESSELLATION_DOMAIN_ORIGIN_MAX_ENUM: return "VK_TESSELLATION_DOMAIN_ORIGIN_MAX_ENUM";
        default: break;
    }
    return "Unknown VkTessellationDomainOrigin";
}

VkSamplerYcbcrModelConversion str_to_VkSamplerYcbcrModelConversion(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY_KHR"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY_KHR"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709_KHR"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601_KHR"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020_KHR"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_MODEL_CONVERSION_MAX_ENUM"sv) return VK_SAMPLER_YCBCR_MODEL_CONVERSION_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSamplerYcbcrModelConversion>(0x7FFFFFFF);
}

std::string_view str_from_VkSamplerYcbcrModelConversion(VkSamplerYcbcrModelConversion e)
{
    switch (e) {
        case VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY: return "VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY";
        case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY: return "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY";
        case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709: return "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709";
        case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601: return "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601";
        case VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020: return "VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020";
        case VK_SAMPLER_YCBCR_MODEL_CONVERSION_MAX_ENUM: return "VK_SAMPLER_YCBCR_MODEL_CONVERSION_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSamplerYcbcrModelConversion";
}

VkSamplerYcbcrRange str_to_VkSamplerYcbcrRange(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLER_YCBCR_RANGE_ITU_FULL"sv) return VK_SAMPLER_YCBCR_RANGE_ITU_FULL;
    if (a_str == "VK_SAMPLER_YCBCR_RANGE_ITU_NARROW"sv) return VK_SAMPLER_YCBCR_RANGE_ITU_NARROW;
    if (a_str == "VK_SAMPLER_YCBCR_RANGE_ITU_FULL_KHR"sv) return VK_SAMPLER_YCBCR_RANGE_ITU_FULL_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_RANGE_ITU_NARROW_KHR"sv) return VK_SAMPLER_YCBCR_RANGE_ITU_NARROW_KHR;
    if (a_str == "VK_SAMPLER_YCBCR_RANGE_MAX_ENUM"sv) return VK_SAMPLER_YCBCR_RANGE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSamplerYcbcrRange>(0x7FFFFFFF);
}

std::string_view str_from_VkSamplerYcbcrRange(VkSamplerYcbcrRange e)
{
    switch (e) {
        case VK_SAMPLER_YCBCR_RANGE_ITU_FULL: return "VK_SAMPLER_YCBCR_RANGE_ITU_FULL";
        case VK_SAMPLER_YCBCR_RANGE_ITU_NARROW: return "VK_SAMPLER_YCBCR_RANGE_ITU_NARROW";
        case VK_SAMPLER_YCBCR_RANGE_MAX_ENUM: return "VK_SAMPLER_YCBCR_RANGE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSamplerYcbcrRange";
}

VkChromaLocation str_to_VkChromaLocation(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_CHROMA_LOCATION_COSITED_EVEN"sv) return VK_CHROMA_LOCATION_COSITED_EVEN;
    if (a_str == "VK_CHROMA_LOCATION_MIDPOINT"sv) return VK_CHROMA_LOCATION_MIDPOINT;
    if (a_str == "VK_CHROMA_LOCATION_COSITED_EVEN_KHR"sv) return VK_CHROMA_LOCATION_COSITED_EVEN_KHR;
    if (a_str == "VK_CHROMA_LOCATION_MIDPOINT_KHR"sv) return VK_CHROMA_LOCATION_MIDPOINT_KHR;
    if (a_str == "VK_CHROMA_LOCATION_MAX_ENUM"sv) return VK_CHROMA_LOCATION_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkChromaLocation>(0x7FFFFFFF);
}

std::string_view str_from_VkChromaLocation(VkChromaLocation e)
{
    switch (e) {
        case VK_CHROMA_LOCATION_COSITED_EVEN: return "VK_CHROMA_LOCATION_COSITED_EVEN";
        case VK_CHROMA_LOCATION_MIDPOINT: return "VK_CHROMA_LOCATION_MIDPOINT";
        case VK_CHROMA_LOCATION_MAX_ENUM: return "VK_CHROMA_LOCATION_MAX_ENUM";
        default: break;
    }
    return "Unknown VkChromaLocation";
}

VkDescriptorUpdateTemplateType str_to_VkDescriptorUpdateTemplateType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET"sv) return VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET;
    if (a_str == "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_PUSH_DESCRIPTORS_KHR"sv) return VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_PUSH_DESCRIPTORS_KHR;
    if (a_str == "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET_KHR"sv) return VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET_KHR;
    if (a_str == "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_MAX_ENUM"sv) return VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDescriptorUpdateTemplateType>(0x7FFFFFFF);
}

std::string_view str_from_VkDescriptorUpdateTemplateType(VkDescriptorUpdateTemplateType e)
{
    switch (e) {
        case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET: return "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_DESCRIPTOR_SET";
        case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_PUSH_DESCRIPTORS_KHR: return "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_PUSH_DESCRIPTORS_KHR";
        case VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_MAX_ENUM: return "VK_DESCRIPTOR_UPDATE_TEMPLATE_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDescriptorUpdateTemplateType";
}

VkSubgroupFeatureFlagBits str_to_VkSubgroupFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SUBGROUP_FEATURE_BASIC_BIT"sv) return VK_SUBGROUP_FEATURE_BASIC_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_VOTE_BIT"sv) return VK_SUBGROUP_FEATURE_VOTE_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_ARITHMETIC_BIT"sv) return VK_SUBGROUP_FEATURE_ARITHMETIC_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_BALLOT_BIT"sv) return VK_SUBGROUP_FEATURE_BALLOT_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_SHUFFLE_BIT"sv) return VK_SUBGROUP_FEATURE_SHUFFLE_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_SHUFFLE_RELATIVE_BIT"sv) return VK_SUBGROUP_FEATURE_SHUFFLE_RELATIVE_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_CLUSTERED_BIT"sv) return VK_SUBGROUP_FEATURE_CLUSTERED_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_QUAD_BIT"sv) return VK_SUBGROUP_FEATURE_QUAD_BIT;
    if (a_str == "VK_SUBGROUP_FEATURE_PARTITIONED_BIT_NV"sv) return VK_SUBGROUP_FEATURE_PARTITIONED_BIT_NV;
    if (a_str == "VK_SUBGROUP_FEATURE_FLAG_BITS_MAX_ENUM"sv) return VK_SUBGROUP_FEATURE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSubgroupFeatureFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSubgroupFeatureFlagBits(VkSubgroupFeatureFlagBits e)
{
    switch (e) {
        case VK_SUBGROUP_FEATURE_BASIC_BIT: return "VK_SUBGROUP_FEATURE_BASIC_BIT";
        case VK_SUBGROUP_FEATURE_VOTE_BIT: return "VK_SUBGROUP_FEATURE_VOTE_BIT";
        case VK_SUBGROUP_FEATURE_ARITHMETIC_BIT: return "VK_SUBGROUP_FEATURE_ARITHMETIC_BIT";
        case VK_SUBGROUP_FEATURE_BALLOT_BIT: return "VK_SUBGROUP_FEATURE_BALLOT_BIT";
        case VK_SUBGROUP_FEATURE_SHUFFLE_BIT: return "VK_SUBGROUP_FEATURE_SHUFFLE_BIT";
        case VK_SUBGROUP_FEATURE_SHUFFLE_RELATIVE_BIT: return "VK_SUBGROUP_FEATURE_SHUFFLE_RELATIVE_BIT";
        case VK_SUBGROUP_FEATURE_CLUSTERED_BIT: return "VK_SUBGROUP_FEATURE_CLUSTERED_BIT";
        case VK_SUBGROUP_FEATURE_QUAD_BIT: return "VK_SUBGROUP_FEATURE_QUAD_BIT";
        case VK_SUBGROUP_FEATURE_PARTITIONED_BIT_NV: return "VK_SUBGROUP_FEATURE_PARTITIONED_BIT_NV";
        case VK_SUBGROUP_FEATURE_FLAG_BITS_MAX_ENUM: return "VK_SUBGROUP_FEATURE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSubgroupFeatureFlagBits";
}

std::string str_from_VkSubgroupFeatureFlags(VkSubgroupFeatureFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSubgroupFeatureFlagBits(static_cast<VkSubgroupFeatureFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSubgroupFeatureFlagBits(static_cast<VkSubgroupFeatureFlagBits>(0)));
    return result;
}

 VkPeerMemoryFeatureFlagBits str_to_VkPeerMemoryFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT"sv) return VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT;
    if (a_str == "VK_PEER_MEMORY_FEATURE_COPY_DST_BIT"sv) return VK_PEER_MEMORY_FEATURE_COPY_DST_BIT;
    if (a_str == "VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT"sv) return VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT;
    if (a_str == "VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT"sv) return VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT;
    if (a_str == "VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT_KHR"sv) return VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT_KHR;
    if (a_str == "VK_PEER_MEMORY_FEATURE_COPY_DST_BIT_KHR"sv) return VK_PEER_MEMORY_FEATURE_COPY_DST_BIT_KHR;
    if (a_str == "VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT_KHR"sv) return VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT_KHR;
    if (a_str == "VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT_KHR"sv) return VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT_KHR;
    if (a_str == "VK_PEER_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM"sv) return VK_PEER_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPeerMemoryFeatureFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkPeerMemoryFeatureFlagBits(VkPeerMemoryFeatureFlagBits e)
{
    switch (e) {
        case VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT: return "VK_PEER_MEMORY_FEATURE_COPY_SRC_BIT";
        case VK_PEER_MEMORY_FEATURE_COPY_DST_BIT: return "VK_PEER_MEMORY_FEATURE_COPY_DST_BIT";
        case VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT: return "VK_PEER_MEMORY_FEATURE_GENERIC_SRC_BIT";
        case VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT: return "VK_PEER_MEMORY_FEATURE_GENERIC_DST_BIT";
        case VK_PEER_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM: return "VK_PEER_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkPeerMemoryFeatureFlagBits";
}

std::string str_from_VkPeerMemoryFeatureFlags(VkPeerMemoryFeatureFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPeerMemoryFeatureFlagBits(static_cast<VkPeerMemoryFeatureFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPeerMemoryFeatureFlagBits(static_cast<VkPeerMemoryFeatureFlagBits>(0)));
    return result;
}

 VkMemoryAllocateFlagBits str_to_VkMemoryAllocateFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT"sv) return VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT;
    if (a_str == "VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT"sv) return VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;
    if (a_str == "VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT"sv) return VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT;
    if (a_str == "VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT_KHR"sv) return VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT_KHR;
    if (a_str == "VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR"sv) return VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR;
    if (a_str == "VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR"sv) return VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR;
    if (a_str == "VK_MEMORY_ALLOCATE_FLAG_BITS_MAX_ENUM"sv) return VK_MEMORY_ALLOCATE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkMemoryAllocateFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkMemoryAllocateFlagBits(VkMemoryAllocateFlagBits e)
{
    switch (e) {
        case VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT: return "VK_MEMORY_ALLOCATE_DEVICE_MASK_BIT";
        case VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT: return "VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT";
        case VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT: return "VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT";
        case VK_MEMORY_ALLOCATE_FLAG_BITS_MAX_ENUM: return "VK_MEMORY_ALLOCATE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkMemoryAllocateFlagBits";
}

std::string str_from_VkMemoryAllocateFlags(VkMemoryAllocateFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkMemoryAllocateFlagBits(static_cast<VkMemoryAllocateFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkMemoryAllocateFlagBits(static_cast<VkMemoryAllocateFlagBits>(0)));
    return result;
}

 VkExternalMemoryHandleTypeFlagBits str_to_VkExternalMemoryHandleTypeFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_MAPPED_FOREIGN_MEMORY_BIT_EXT"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_MAPPED_FOREIGN_MEMORY_BIT_EXT;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_ZIRCON_VMO_BIT_FUCHSIA"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_ZIRCON_VMO_BIT_FUCHSIA;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_RDMA_ADDRESS_BIT_NV"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_RDMA_ADDRESS_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalMemoryHandleTypeFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalMemoryHandleTypeFlagBits(VkExternalMemoryHandleTypeFlagBits e)
{
    switch (e) {
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_TEXTURE_KMT_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE_BIT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_MAPPED_FOREIGN_MEMORY_BIT_EXT: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_MAPPED_FOREIGN_MEMORY_BIT_EXT";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_ZIRCON_VMO_BIT_FUCHSIA: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_ZIRCON_VMO_BIT_FUCHSIA";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_RDMA_ADDRESS_BIT_NV: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_RDMA_ADDRESS_BIT_NV";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkExternalMemoryHandleTypeFlagBits";
}

std::string str_from_VkExternalMemoryHandleTypeFlags(VkExternalMemoryHandleTypeFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalMemoryHandleTypeFlagBits(static_cast<VkExternalMemoryHandleTypeFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalMemoryHandleTypeFlagBits(static_cast<VkExternalMemoryHandleTypeFlagBits>(0)));
    return result;
}

 VkExternalMemoryFeatureFlagBits str_to_VkExternalMemoryFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT"sv) return VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT"sv) return VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT"sv) return VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_KHR"sv) return VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM"sv) return VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalMemoryFeatureFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalMemoryFeatureFlagBits(VkExternalMemoryFeatureFlagBits e)
{
    switch (e) {
        case VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT: return "VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT";
        case VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT: return "VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT";
        case VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT: return "VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT";
        case VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM: return "VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkExternalMemoryFeatureFlagBits";
}

std::string str_from_VkExternalMemoryFeatureFlags(VkExternalMemoryFeatureFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalMemoryFeatureFlagBits(static_cast<VkExternalMemoryFeatureFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalMemoryFeatureFlagBits(static_cast<VkExternalMemoryFeatureFlagBits>(0)));
    return result;
}

 VkExternalFenceHandleTypeFlagBits str_to_VkExternalFenceHandleTypeFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT_KHR"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT_KHR;
    if (a_str == "VK_EXTERNAL_FENCE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM"sv) return VK_EXTERNAL_FENCE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalFenceHandleTypeFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalFenceHandleTypeFlagBits(VkExternalFenceHandleTypeFlagBits e)
{
    switch (e) {
        case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT: return "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT";
        case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT: return "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT";
        case VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT: return "VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT";
        case VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT: return "VK_EXTERNAL_FENCE_HANDLE_TYPE_SYNC_FD_BIT";
        case VK_EXTERNAL_FENCE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM: return "VK_EXTERNAL_FENCE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkExternalFenceHandleTypeFlagBits";
}

std::string str_from_VkExternalFenceHandleTypeFlags(VkExternalFenceHandleTypeFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalFenceHandleTypeFlagBits(static_cast<VkExternalFenceHandleTypeFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalFenceHandleTypeFlagBits(static_cast<VkExternalFenceHandleTypeFlagBits>(0)));
    return result;
}

 VkExternalFenceFeatureFlagBits str_to_VkExternalFenceFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT"sv) return VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT;
    if (a_str == "VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT"sv) return VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT;
    if (a_str == "VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT_KHR"sv) return VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT_KHR"sv) return VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_FENCE_FEATURE_FLAG_BITS_MAX_ENUM"sv) return VK_EXTERNAL_FENCE_FEATURE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalFenceFeatureFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalFenceFeatureFlagBits(VkExternalFenceFeatureFlagBits e)
{
    switch (e) {
        case VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT: return "VK_EXTERNAL_FENCE_FEATURE_EXPORTABLE_BIT";
        case VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT: return "VK_EXTERNAL_FENCE_FEATURE_IMPORTABLE_BIT";
        case VK_EXTERNAL_FENCE_FEATURE_FLAG_BITS_MAX_ENUM: return "VK_EXTERNAL_FENCE_FEATURE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkExternalFenceFeatureFlagBits";
}

std::string str_from_VkExternalFenceFeatureFlags(VkExternalFenceFeatureFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalFenceFeatureFlagBits(static_cast<VkExternalFenceFeatureFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalFenceFeatureFlagBits(static_cast<VkExternalFenceFeatureFlagBits>(0)));
    return result;
}

 VkFenceImportFlagBits str_to_VkFenceImportFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FENCE_IMPORT_TEMPORARY_BIT"sv) return VK_FENCE_IMPORT_TEMPORARY_BIT;
    if (a_str == "VK_FENCE_IMPORT_TEMPORARY_BIT_KHR"sv) return VK_FENCE_IMPORT_TEMPORARY_BIT_KHR;
    if (a_str == "VK_FENCE_IMPORT_FLAG_BITS_MAX_ENUM"sv) return VK_FENCE_IMPORT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFenceImportFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkFenceImportFlagBits(VkFenceImportFlagBits e)
{
    switch (e) {
        case VK_FENCE_IMPORT_TEMPORARY_BIT: return "VK_FENCE_IMPORT_TEMPORARY_BIT";
        case VK_FENCE_IMPORT_FLAG_BITS_MAX_ENUM: return "VK_FENCE_IMPORT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkFenceImportFlagBits";
}

std::string str_from_VkFenceImportFlags(VkFenceImportFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkFenceImportFlagBits(static_cast<VkFenceImportFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkFenceImportFlagBits(static_cast<VkFenceImportFlagBits>(0)));
    return result;
}

 VkSemaphoreImportFlagBits str_to_VkSemaphoreImportFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SEMAPHORE_IMPORT_TEMPORARY_BIT"sv) return VK_SEMAPHORE_IMPORT_TEMPORARY_BIT;
    if (a_str == "VK_SEMAPHORE_IMPORT_TEMPORARY_BIT_KHR"sv) return VK_SEMAPHORE_IMPORT_TEMPORARY_BIT_KHR;
    if (a_str == "VK_SEMAPHORE_IMPORT_FLAG_BITS_MAX_ENUM"sv) return VK_SEMAPHORE_IMPORT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSemaphoreImportFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSemaphoreImportFlagBits(VkSemaphoreImportFlagBits e)
{
    switch (e) {
        case VK_SEMAPHORE_IMPORT_TEMPORARY_BIT: return "VK_SEMAPHORE_IMPORT_TEMPORARY_BIT";
        case VK_SEMAPHORE_IMPORT_FLAG_BITS_MAX_ENUM: return "VK_SEMAPHORE_IMPORT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSemaphoreImportFlagBits";
}

std::string str_from_VkSemaphoreImportFlags(VkSemaphoreImportFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSemaphoreImportFlagBits(static_cast<VkSemaphoreImportFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSemaphoreImportFlagBits(static_cast<VkSemaphoreImportFlagBits>(0)));
    return result;
}

 VkExternalSemaphoreHandleTypeFlagBits str_to_VkExternalSemaphoreHandleTypeFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_ZIRCON_EVENT_BIT_FUCHSIA"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_ZIRCON_EVENT_BIT_FUCHSIA;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_FENCE_BIT"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_FENCE_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM"sv) return VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalSemaphoreHandleTypeFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalSemaphoreHandleTypeFlagBits(VkExternalSemaphoreHandleTypeFlagBits e)
{
    switch (e) {
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT";
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT";
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT";
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE_BIT";
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_SYNC_FD_BIT";
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_ZIRCON_EVENT_BIT_FUCHSIA: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_ZIRCON_EVENT_BIT_FUCHSIA";
        case VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM: return "VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkExternalSemaphoreHandleTypeFlagBits";
}

std::string str_from_VkExternalSemaphoreHandleTypeFlags(VkExternalSemaphoreHandleTypeFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalSemaphoreHandleTypeFlagBits(static_cast<VkExternalSemaphoreHandleTypeFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalSemaphoreHandleTypeFlagBits(static_cast<VkExternalSemaphoreHandleTypeFlagBits>(0)));
    return result;
}

 VkExternalSemaphoreFeatureFlagBits str_to_VkExternalSemaphoreFeatureFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT"sv) return VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT"sv) return VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT_KHR"sv) return VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT_KHR;
    if (a_str == "VK_EXTERNAL_SEMAPHORE_FEATURE_FLAG_BITS_MAX_ENUM"sv) return VK_EXTERNAL_SEMAPHORE_FEATURE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalSemaphoreFeatureFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalSemaphoreFeatureFlagBits(VkExternalSemaphoreFeatureFlagBits e)
{
    switch (e) {
        case VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT: return "VK_EXTERNAL_SEMAPHORE_FEATURE_EXPORTABLE_BIT";
        case VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT: return "VK_EXTERNAL_SEMAPHORE_FEATURE_IMPORTABLE_BIT";
        case VK_EXTERNAL_SEMAPHORE_FEATURE_FLAG_BITS_MAX_ENUM: return "VK_EXTERNAL_SEMAPHORE_FEATURE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkExternalSemaphoreFeatureFlagBits";
}

std::string str_from_VkExternalSemaphoreFeatureFlags(VkExternalSemaphoreFeatureFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalSemaphoreFeatureFlagBits(static_cast<VkExternalSemaphoreFeatureFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalSemaphoreFeatureFlagBits(static_cast<VkExternalSemaphoreFeatureFlagBits>(0)));
    return result;
}

 #endif
#if VK_VERSION_1_2
VkDriverId str_to_VkDriverId(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DRIVER_ID_AMD_PROPRIETARY"sv) return VK_DRIVER_ID_AMD_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_AMD_OPEN_SOURCE"sv) return VK_DRIVER_ID_AMD_OPEN_SOURCE;
    if (a_str == "VK_DRIVER_ID_MESA_RADV"sv) return VK_DRIVER_ID_MESA_RADV;
    if (a_str == "VK_DRIVER_ID_NVIDIA_PROPRIETARY"sv) return VK_DRIVER_ID_NVIDIA_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS"sv) return VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS;
    if (a_str == "VK_DRIVER_ID_INTEL_OPEN_SOURCE_MESA"sv) return VK_DRIVER_ID_INTEL_OPEN_SOURCE_MESA;
    if (a_str == "VK_DRIVER_ID_IMAGINATION_PROPRIETARY"sv) return VK_DRIVER_ID_IMAGINATION_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_QUALCOMM_PROPRIETARY"sv) return VK_DRIVER_ID_QUALCOMM_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_ARM_PROPRIETARY"sv) return VK_DRIVER_ID_ARM_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_GOOGLE_SWIFTSHADER"sv) return VK_DRIVER_ID_GOOGLE_SWIFTSHADER;
    if (a_str == "VK_DRIVER_ID_GGP_PROPRIETARY"sv) return VK_DRIVER_ID_GGP_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_BROADCOM_PROPRIETARY"sv) return VK_DRIVER_ID_BROADCOM_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_MESA_LLVMPIPE"sv) return VK_DRIVER_ID_MESA_LLVMPIPE;
    if (a_str == "VK_DRIVER_ID_MOLTENVK"sv) return VK_DRIVER_ID_MOLTENVK;
    if (a_str == "VK_DRIVER_ID_COREAVI_PROPRIETARY"sv) return VK_DRIVER_ID_COREAVI_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_JUICE_PROPRIETARY"sv) return VK_DRIVER_ID_JUICE_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_VERISILICON_PROPRIETARY"sv) return VK_DRIVER_ID_VERISILICON_PROPRIETARY;
    if (a_str == "VK_DRIVER_ID_MESA_TURNIP"sv) return VK_DRIVER_ID_MESA_TURNIP;
    if (a_str == "VK_DRIVER_ID_MESA_V3DV"sv) return VK_DRIVER_ID_MESA_V3DV;
    if (a_str == "VK_DRIVER_ID_MESA_PANVK"sv) return VK_DRIVER_ID_MESA_PANVK;
    if (a_str == "VK_DRIVER_ID_AMD_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_AMD_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_AMD_OPEN_SOURCE_KHR"sv) return VK_DRIVER_ID_AMD_OPEN_SOURCE_KHR;
    if (a_str == "VK_DRIVER_ID_MESA_RADV_KHR"sv) return VK_DRIVER_ID_MESA_RADV_KHR;
    if (a_str == "VK_DRIVER_ID_NVIDIA_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_NVIDIA_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS_KHR"sv) return VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS_KHR;
    if (a_str == "VK_DRIVER_ID_INTEL_OPEN_SOURCE_MESA_KHR"sv) return VK_DRIVER_ID_INTEL_OPEN_SOURCE_MESA_KHR;
    if (a_str == "VK_DRIVER_ID_IMAGINATION_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_IMAGINATION_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_QUALCOMM_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_QUALCOMM_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_ARM_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_ARM_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_GOOGLE_SWIFTSHADER_KHR"sv) return VK_DRIVER_ID_GOOGLE_SWIFTSHADER_KHR;
    if (a_str == "VK_DRIVER_ID_GGP_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_GGP_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_BROADCOM_PROPRIETARY_KHR"sv) return VK_DRIVER_ID_BROADCOM_PROPRIETARY_KHR;
    if (a_str == "VK_DRIVER_ID_MAX_ENUM"sv) return VK_DRIVER_ID_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDriverId>(0x7FFFFFFF);
}

std::string_view str_from_VkDriverId(VkDriverId e)
{
    switch (e) {
        case VK_DRIVER_ID_AMD_PROPRIETARY: return "VK_DRIVER_ID_AMD_PROPRIETARY";
        case VK_DRIVER_ID_AMD_OPEN_SOURCE: return "VK_DRIVER_ID_AMD_OPEN_SOURCE";
        case VK_DRIVER_ID_MESA_RADV: return "VK_DRIVER_ID_MESA_RADV";
        case VK_DRIVER_ID_NVIDIA_PROPRIETARY: return "VK_DRIVER_ID_NVIDIA_PROPRIETARY";
        case VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS: return "VK_DRIVER_ID_INTEL_PROPRIETARY_WINDOWS";
        case VK_DRIVER_ID_INTEL_OPEN_SOURCE_MESA: return "VK_DRIVER_ID_INTEL_OPEN_SOURCE_MESA";
        case VK_DRIVER_ID_IMAGINATION_PROPRIETARY: return "VK_DRIVER_ID_IMAGINATION_PROPRIETARY";
        case VK_DRIVER_ID_QUALCOMM_PROPRIETARY: return "VK_DRIVER_ID_QUALCOMM_PROPRIETARY";
        case VK_DRIVER_ID_ARM_PROPRIETARY: return "VK_DRIVER_ID_ARM_PROPRIETARY";
        case VK_DRIVER_ID_GOOGLE_SWIFTSHADER: return "VK_DRIVER_ID_GOOGLE_SWIFTSHADER";
        case VK_DRIVER_ID_GGP_PROPRIETARY: return "VK_DRIVER_ID_GGP_PROPRIETARY";
        case VK_DRIVER_ID_BROADCOM_PROPRIETARY: return "VK_DRIVER_ID_BROADCOM_PROPRIETARY";
        case VK_DRIVER_ID_MESA_LLVMPIPE: return "VK_DRIVER_ID_MESA_LLVMPIPE";
        case VK_DRIVER_ID_MOLTENVK: return "VK_DRIVER_ID_MOLTENVK";
        case VK_DRIVER_ID_COREAVI_PROPRIETARY: return "VK_DRIVER_ID_COREAVI_PROPRIETARY";
        case VK_DRIVER_ID_JUICE_PROPRIETARY: return "VK_DRIVER_ID_JUICE_PROPRIETARY";
        case VK_DRIVER_ID_VERISILICON_PROPRIETARY: return "VK_DRIVER_ID_VERISILICON_PROPRIETARY";
        case VK_DRIVER_ID_MESA_TURNIP: return "VK_DRIVER_ID_MESA_TURNIP";
        case VK_DRIVER_ID_MESA_V3DV: return "VK_DRIVER_ID_MESA_V3DV";
        case VK_DRIVER_ID_MESA_PANVK: return "VK_DRIVER_ID_MESA_PANVK";
        case VK_DRIVER_ID_MAX_ENUM: return "VK_DRIVER_ID_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDriverId";
}

VkShaderFloatControlsIndependence str_to_VkShaderFloatControlsIndependence(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_32_BIT_ONLY"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_32_BIT_ONLY;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_ALL"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_ALL;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_NONE"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_NONE;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_32_BIT_ONLY_KHR"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_32_BIT_ONLY_KHR;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_ALL_KHR"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_ALL_KHR;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_NONE_KHR"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_NONE_KHR;
    if (a_str == "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_MAX_ENUM"sv) return VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkShaderFloatControlsIndependence>(0x7FFFFFFF);
}

std::string_view str_from_VkShaderFloatControlsIndependence(VkShaderFloatControlsIndependence e)
{
    switch (e) {
        case VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_32_BIT_ONLY: return "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_32_BIT_ONLY";
        case VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_ALL: return "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_ALL";
        case VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_NONE: return "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_NONE";
        case VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_MAX_ENUM: return "VK_SHADER_FLOAT_CONTROLS_INDEPENDENCE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkShaderFloatControlsIndependence";
}

VkSamplerReductionMode str_to_VkSamplerReductionMode(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE"sv) return VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_MIN"sv) return VK_SAMPLER_REDUCTION_MODE_MIN;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_MAX"sv) return VK_SAMPLER_REDUCTION_MODE_MAX;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE_EXT"sv) return VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE_EXT;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_MIN_EXT"sv) return VK_SAMPLER_REDUCTION_MODE_MIN_EXT;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_MAX_EXT"sv) return VK_SAMPLER_REDUCTION_MODE_MAX_EXT;
    if (a_str == "VK_SAMPLER_REDUCTION_MODE_MAX_ENUM"sv) return VK_SAMPLER_REDUCTION_MODE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSamplerReductionMode>(0x7FFFFFFF);
}

std::string_view str_from_VkSamplerReductionMode(VkSamplerReductionMode e)
{
    switch (e) {
        case VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE: return "VK_SAMPLER_REDUCTION_MODE_WEIGHTED_AVERAGE";
        case VK_SAMPLER_REDUCTION_MODE_MIN: return "VK_SAMPLER_REDUCTION_MODE_MIN";
        case VK_SAMPLER_REDUCTION_MODE_MAX: return "VK_SAMPLER_REDUCTION_MODE_MAX";
        case VK_SAMPLER_REDUCTION_MODE_MAX_ENUM: return "VK_SAMPLER_REDUCTION_MODE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSamplerReductionMode";
}

VkSemaphoreType str_to_VkSemaphoreType(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SEMAPHORE_TYPE_BINARY"sv) return VK_SEMAPHORE_TYPE_BINARY;
    if (a_str == "VK_SEMAPHORE_TYPE_TIMELINE"sv) return VK_SEMAPHORE_TYPE_TIMELINE;
    if (a_str == "VK_SEMAPHORE_TYPE_BINARY_KHR"sv) return VK_SEMAPHORE_TYPE_BINARY_KHR;
    if (a_str == "VK_SEMAPHORE_TYPE_TIMELINE_KHR"sv) return VK_SEMAPHORE_TYPE_TIMELINE_KHR;
    if (a_str == "VK_SEMAPHORE_TYPE_MAX_ENUM"sv) return VK_SEMAPHORE_TYPE_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSemaphoreType>(0x7FFFFFFF);
}

std::string_view str_from_VkSemaphoreType(VkSemaphoreType e)
{
    switch (e) {
        case VK_SEMAPHORE_TYPE_BINARY: return "VK_SEMAPHORE_TYPE_BINARY";
        case VK_SEMAPHORE_TYPE_TIMELINE: return "VK_SEMAPHORE_TYPE_TIMELINE";
        case VK_SEMAPHORE_TYPE_MAX_ENUM: return "VK_SEMAPHORE_TYPE_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSemaphoreType";
}

VkResolveModeFlagBits str_to_VkResolveModeFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_RESOLVE_MODE_NONE"sv) return VK_RESOLVE_MODE_NONE;
    if (a_str == "VK_RESOLVE_MODE_SAMPLE_ZERO_BIT"sv) return VK_RESOLVE_MODE_SAMPLE_ZERO_BIT;
    if (a_str == "VK_RESOLVE_MODE_AVERAGE_BIT"sv) return VK_RESOLVE_MODE_AVERAGE_BIT;
    if (a_str == "VK_RESOLVE_MODE_MIN_BIT"sv) return VK_RESOLVE_MODE_MIN_BIT;
    if (a_str == "VK_RESOLVE_MODE_MAX_BIT"sv) return VK_RESOLVE_MODE_MAX_BIT;
    if (a_str == "VK_RESOLVE_MODE_NONE_KHR"sv) return VK_RESOLVE_MODE_NONE_KHR;
    if (a_str == "VK_RESOLVE_MODE_SAMPLE_ZERO_BIT_KHR"sv) return VK_RESOLVE_MODE_SAMPLE_ZERO_BIT_KHR;
    if (a_str == "VK_RESOLVE_MODE_AVERAGE_BIT_KHR"sv) return VK_RESOLVE_MODE_AVERAGE_BIT_KHR;
    if (a_str == "VK_RESOLVE_MODE_MIN_BIT_KHR"sv) return VK_RESOLVE_MODE_MIN_BIT_KHR;
    if (a_str == "VK_RESOLVE_MODE_MAX_BIT_KHR"sv) return VK_RESOLVE_MODE_MAX_BIT_KHR;
    if (a_str == "VK_RESOLVE_MODE_FLAG_BITS_MAX_ENUM"sv) return VK_RESOLVE_MODE_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkResolveModeFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkResolveModeFlagBits(VkResolveModeFlagBits e)
{
    switch (e) {
        case VK_RESOLVE_MODE_NONE: return "VK_RESOLVE_MODE_NONE";
        case VK_RESOLVE_MODE_SAMPLE_ZERO_BIT: return "VK_RESOLVE_MODE_SAMPLE_ZERO_BIT";
        case VK_RESOLVE_MODE_AVERAGE_BIT: return "VK_RESOLVE_MODE_AVERAGE_BIT";
        case VK_RESOLVE_MODE_MIN_BIT: return "VK_RESOLVE_MODE_MIN_BIT";
        case VK_RESOLVE_MODE_MAX_BIT: return "VK_RESOLVE_MODE_MAX_BIT";
        case VK_RESOLVE_MODE_FLAG_BITS_MAX_ENUM: return "VK_RESOLVE_MODE_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkResolveModeFlagBits";
}

std::string str_from_VkResolveModeFlags(VkResolveModeFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkResolveModeFlagBits(static_cast<VkResolveModeFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkResolveModeFlagBits(static_cast<VkResolveModeFlagBits>(0)));
    return result;
}

 VkDescriptorBindingFlagBits str_to_VkDescriptorBindingFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT"sv) return VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT;
    if (a_str == "VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT"sv) return VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT;
    if (a_str == "VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT"sv) return VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT;
    if (a_str == "VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT"sv) return VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT;
    if (a_str == "VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT_EXT"sv) return VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT_EXT;
    if (a_str == "VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT_EXT"sv) return VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT_EXT;
    if (a_str == "VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT_EXT"sv) return VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT_EXT;
    if (a_str == "VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT"sv) return VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT;
    if (a_str == "VK_DESCRIPTOR_BINDING_FLAG_BITS_MAX_ENUM"sv) return VK_DESCRIPTOR_BINDING_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDescriptorBindingFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkDescriptorBindingFlagBits(VkDescriptorBindingFlagBits e)
{
    switch (e) {
        case VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT: return "VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT";
        case VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT: return "VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT";
        case VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT: return "VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT";
        case VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT: return "VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT";
        case VK_DESCRIPTOR_BINDING_FLAG_BITS_MAX_ENUM: return "VK_DESCRIPTOR_BINDING_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkDescriptorBindingFlagBits";
}

std::string str_from_VkDescriptorBindingFlags(VkDescriptorBindingFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDescriptorBindingFlagBits(static_cast<VkDescriptorBindingFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDescriptorBindingFlagBits(static_cast<VkDescriptorBindingFlagBits>(0)));
    return result;
}

 VkSemaphoreWaitFlagBits str_to_VkSemaphoreWaitFlagBits(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SEMAPHORE_WAIT_ANY_BIT"sv) return VK_SEMAPHORE_WAIT_ANY_BIT;
    if (a_str == "VK_SEMAPHORE_WAIT_ANY_BIT_KHR"sv) return VK_SEMAPHORE_WAIT_ANY_BIT_KHR;
    if (a_str == "VK_SEMAPHORE_WAIT_FLAG_BITS_MAX_ENUM"sv) return VK_SEMAPHORE_WAIT_FLAG_BITS_MAX_ENUM;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSemaphoreWaitFlagBits>(0x7FFFFFFF);
}

std::string_view str_from_VkSemaphoreWaitFlagBits(VkSemaphoreWaitFlagBits e)
{
    switch (e) {
        case VK_SEMAPHORE_WAIT_ANY_BIT: return "VK_SEMAPHORE_WAIT_ANY_BIT";
        case VK_SEMAPHORE_WAIT_FLAG_BITS_MAX_ENUM: return "VK_SEMAPHORE_WAIT_FLAG_BITS_MAX_ENUM";
        default: break;
    }
    return "Unknown VkSemaphoreWaitFlagBits";
}

std::string str_from_VkSemaphoreWaitFlags(VkSemaphoreWaitFlags f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSemaphoreWaitFlagBits(static_cast<VkSemaphoreWaitFlagBits>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSemaphoreWaitFlagBits(static_cast<VkSemaphoreWaitFlagBits>(0)));
    return result;
}

 #endif
#if VK_KHR_surface
VkPresentModeKHR str_to_VkPresentModeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PRESENT_MODE_IMMEDIATE_KHR"sv) return VK_PRESENT_MODE_IMMEDIATE_KHR;
    if (a_str == "VK_PRESENT_MODE_MAILBOX_KHR"sv) return VK_PRESENT_MODE_MAILBOX_KHR;
    if (a_str == "VK_PRESENT_MODE_FIFO_KHR"sv) return VK_PRESENT_MODE_FIFO_KHR;
    if (a_str == "VK_PRESENT_MODE_FIFO_RELAXED_KHR"sv) return VK_PRESENT_MODE_FIFO_RELAXED_KHR;
    if (a_str == "VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR"sv) return VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR;
    if (a_str == "VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR"sv) return VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR;
    if (a_str == "VK_PRESENT_MODE_MAX_ENUM_KHR"sv) return VK_PRESENT_MODE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPresentModeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkPresentModeKHR(VkPresentModeKHR e)
{
    switch (e) {
        case VK_PRESENT_MODE_IMMEDIATE_KHR: return "VK_PRESENT_MODE_IMMEDIATE_KHR";
        case VK_PRESENT_MODE_MAILBOX_KHR: return "VK_PRESENT_MODE_MAILBOX_KHR";
        case VK_PRESENT_MODE_FIFO_KHR: return "VK_PRESENT_MODE_FIFO_KHR";
        case VK_PRESENT_MODE_FIFO_RELAXED_KHR: return "VK_PRESENT_MODE_FIFO_RELAXED_KHR";
        case VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR: return "VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR";
        case VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR: return "VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR";
        case VK_PRESENT_MODE_MAX_ENUM_KHR: return "VK_PRESENT_MODE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkPresentModeKHR";
}

VkColorSpaceKHR str_to_VkColorSpaceKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COLOR_SPACE_SRGB_NONLINEAR_KHR"sv) return VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    if (a_str == "VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT"sv) return VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT"sv) return VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_DISPLAY_P3_LINEAR_EXT"sv) return VK_COLOR_SPACE_DISPLAY_P3_LINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT"sv) return VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_BT709_LINEAR_EXT"sv) return VK_COLOR_SPACE_BT709_LINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_BT709_NONLINEAR_EXT"sv) return VK_COLOR_SPACE_BT709_NONLINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_BT2020_LINEAR_EXT"sv) return VK_COLOR_SPACE_BT2020_LINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_HDR10_ST2084_EXT"sv) return VK_COLOR_SPACE_HDR10_ST2084_EXT;
    if (a_str == "VK_COLOR_SPACE_DOLBYVISION_EXT"sv) return VK_COLOR_SPACE_DOLBYVISION_EXT;
    if (a_str == "VK_COLOR_SPACE_HDR10_HLG_EXT"sv) return VK_COLOR_SPACE_HDR10_HLG_EXT;
    if (a_str == "VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT"sv) return VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT"sv) return VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_PASS_THROUGH_EXT"sv) return VK_COLOR_SPACE_PASS_THROUGH_EXT;
    if (a_str == "VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT"sv) return VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_DISPLAY_NATIVE_AMD"sv) return VK_COLOR_SPACE_DISPLAY_NATIVE_AMD;
    if (a_str == "VK_COLORSPACE_SRGB_NONLINEAR_KHR"sv) return VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    if (a_str == "VK_COLOR_SPACE_DCI_P3_LINEAR_EXT"sv) return VK_COLOR_SPACE_DCI_P3_LINEAR_EXT;
    if (a_str == "VK_COLOR_SPACE_MAX_ENUM_KHR"sv) return VK_COLOR_SPACE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkColorSpaceKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkColorSpaceKHR(VkColorSpaceKHR e)
{
    switch (e) {
        case VK_COLOR_SPACE_SRGB_NONLINEAR_KHR: return "VK_COLOR_SPACE_SRGB_NONLINEAR_KHR";
        case VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT: return "VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT";
        case VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT: return "VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT";
        case VK_COLOR_SPACE_DISPLAY_P3_LINEAR_EXT: return "VK_COLOR_SPACE_DISPLAY_P3_LINEAR_EXT";
        case VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT: return "VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT";
        case VK_COLOR_SPACE_BT709_LINEAR_EXT: return "VK_COLOR_SPACE_BT709_LINEAR_EXT";
        case VK_COLOR_SPACE_BT709_NONLINEAR_EXT: return "VK_COLOR_SPACE_BT709_NONLINEAR_EXT";
        case VK_COLOR_SPACE_BT2020_LINEAR_EXT: return "VK_COLOR_SPACE_BT2020_LINEAR_EXT";
        case VK_COLOR_SPACE_HDR10_ST2084_EXT: return "VK_COLOR_SPACE_HDR10_ST2084_EXT";
        case VK_COLOR_SPACE_DOLBYVISION_EXT: return "VK_COLOR_SPACE_DOLBYVISION_EXT";
        case VK_COLOR_SPACE_HDR10_HLG_EXT: return "VK_COLOR_SPACE_HDR10_HLG_EXT";
        case VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT: return "VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT";
        case VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT: return "VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT";
        case VK_COLOR_SPACE_PASS_THROUGH_EXT: return "VK_COLOR_SPACE_PASS_THROUGH_EXT";
        case VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT: return "VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT";
        case VK_COLOR_SPACE_DISPLAY_NATIVE_AMD: return "VK_COLOR_SPACE_DISPLAY_NATIVE_AMD";
        case VK_COLOR_SPACE_MAX_ENUM_KHR: return "VK_COLOR_SPACE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkColorSpaceKHR";
}

VkSurfaceTransformFlagBitsKHR str_to_VkSurfaceTransformFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR"sv) return VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR;
    if (a_str == "VK_SURFACE_TRANSFORM_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_SURFACE_TRANSFORM_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSurfaceTransformFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkSurfaceTransformFlagBitsKHR(VkSurfaceTransformFlagBitsKHR e)
{
    switch (e) {
        case VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR: return "VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR";
        case VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR: return "VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR";
        case VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR: return "VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR";
        case VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR: return "VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR";
        case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR: return "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR";
        case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR: return "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR";
        case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR: return "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR";
        case VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR: return "VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR";
        case VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR: return "VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR";
        case VK_SURFACE_TRANSFORM_FLAG_BITS_MAX_ENUM_KHR: return "VK_SURFACE_TRANSFORM_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkSurfaceTransformFlagBitsKHR";
}

std::string str_from_VkSurfaceTransformFlagsKHR(VkSurfaceTransformFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSurfaceTransformFlagBitsKHR(static_cast<VkSurfaceTransformFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSurfaceTransformFlagBitsKHR(static_cast<VkSurfaceTransformFlagBitsKHR>(0)));
    return result;
}

 VkCompositeAlphaFlagBitsKHR str_to_VkCompositeAlphaFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR"sv) return VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    if (a_str == "VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR"sv) return VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR;
    if (a_str == "VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR"sv) return VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR;
    if (a_str == "VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR"sv) return VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
    if (a_str == "VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCompositeAlphaFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkCompositeAlphaFlagBitsKHR(VkCompositeAlphaFlagBitsKHR e)
{
    switch (e) {
        case VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR: return "VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR";
        case VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR: return "VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR";
        case VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR: return "VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR";
        case VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR: return "VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR";
        case VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR: return "VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkCompositeAlphaFlagBitsKHR";
}

std::string str_from_VkCompositeAlphaFlagsKHR(VkCompositeAlphaFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkCompositeAlphaFlagBitsKHR(static_cast<VkCompositeAlphaFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkCompositeAlphaFlagBitsKHR(static_cast<VkCompositeAlphaFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_KHR_swapchain
VkSwapchainCreateFlagBitsKHR str_to_VkSwapchainCreateFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR"sv) return VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR;
    if (a_str == "VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR"sv) return VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR;
    if (a_str == "VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR"sv) return VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR;
    if (a_str == "VK_SWAPCHAIN_CREATE_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_SWAPCHAIN_CREATE_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSwapchainCreateFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkSwapchainCreateFlagBitsKHR(VkSwapchainCreateFlagBitsKHR e)
{
    switch (e) {
        case VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR: return "VK_SWAPCHAIN_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR";
        case VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR: return "VK_SWAPCHAIN_CREATE_PROTECTED_BIT_KHR";
        case VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR: return "VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR";
        case VK_SWAPCHAIN_CREATE_FLAG_BITS_MAX_ENUM_KHR: return "VK_SWAPCHAIN_CREATE_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkSwapchainCreateFlagBitsKHR";
}

std::string str_from_VkSwapchainCreateFlagsKHR(VkSwapchainCreateFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSwapchainCreateFlagBitsKHR(static_cast<VkSwapchainCreateFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSwapchainCreateFlagBitsKHR(static_cast<VkSwapchainCreateFlagBitsKHR>(0)));
    return result;
}

 VkDeviceGroupPresentModeFlagBitsKHR str_to_VkDeviceGroupPresentModeFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR"sv) return VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR;
    if (a_str == "VK_DEVICE_GROUP_PRESENT_MODE_REMOTE_BIT_KHR"sv) return VK_DEVICE_GROUP_PRESENT_MODE_REMOTE_BIT_KHR;
    if (a_str == "VK_DEVICE_GROUP_PRESENT_MODE_SUM_BIT_KHR"sv) return VK_DEVICE_GROUP_PRESENT_MODE_SUM_BIT_KHR;
    if (a_str == "VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_MULTI_DEVICE_BIT_KHR"sv) return VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_MULTI_DEVICE_BIT_KHR;
    if (a_str == "VK_DEVICE_GROUP_PRESENT_MODE_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_DEVICE_GROUP_PRESENT_MODE_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDeviceGroupPresentModeFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkDeviceGroupPresentModeFlagBitsKHR(VkDeviceGroupPresentModeFlagBitsKHR e)
{
    switch (e) {
        case VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR: return "VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_BIT_KHR";
        case VK_DEVICE_GROUP_PRESENT_MODE_REMOTE_BIT_KHR: return "VK_DEVICE_GROUP_PRESENT_MODE_REMOTE_BIT_KHR";
        case VK_DEVICE_GROUP_PRESENT_MODE_SUM_BIT_KHR: return "VK_DEVICE_GROUP_PRESENT_MODE_SUM_BIT_KHR";
        case VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_MULTI_DEVICE_BIT_KHR: return "VK_DEVICE_GROUP_PRESENT_MODE_LOCAL_MULTI_DEVICE_BIT_KHR";
        case VK_DEVICE_GROUP_PRESENT_MODE_FLAG_BITS_MAX_ENUM_KHR: return "VK_DEVICE_GROUP_PRESENT_MODE_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkDeviceGroupPresentModeFlagBitsKHR";
}

std::string str_from_VkDeviceGroupPresentModeFlagsKHR(VkDeviceGroupPresentModeFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDeviceGroupPresentModeFlagBitsKHR(static_cast<VkDeviceGroupPresentModeFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDeviceGroupPresentModeFlagBitsKHR(static_cast<VkDeviceGroupPresentModeFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_KHR_display
VkDisplayPlaneAlphaFlagBitsKHR str_to_VkDisplayPlaneAlphaFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR"sv) return VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR;
    if (a_str == "VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR"sv) return VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR;
    if (a_str == "VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR"sv) return VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR;
    if (a_str == "VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR"sv) return VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR;
    if (a_str == "VK_DISPLAY_PLANE_ALPHA_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_DISPLAY_PLANE_ALPHA_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDisplayPlaneAlphaFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkDisplayPlaneAlphaFlagBitsKHR(VkDisplayPlaneAlphaFlagBitsKHR e)
{
    switch (e) {
        case VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR: return "VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR";
        case VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR: return "VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR";
        case VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR: return "VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR";
        case VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR: return "VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR";
        case VK_DISPLAY_PLANE_ALPHA_FLAG_BITS_MAX_ENUM_KHR: return "VK_DISPLAY_PLANE_ALPHA_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkDisplayPlaneAlphaFlagBitsKHR";
}

std::string str_from_VkDisplayPlaneAlphaFlagsKHR(VkDisplayPlaneAlphaFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDisplayPlaneAlphaFlagBitsKHR(static_cast<VkDisplayPlaneAlphaFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDisplayPlaneAlphaFlagBitsKHR(static_cast<VkDisplayPlaneAlphaFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_KHR_dynamic_rendering
VkRenderingFlagBitsKHR str_to_VkRenderingFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_RENDERING_CONTENTS_SECONDARY_COMMAND_BUFFERS_BIT_KHR"sv) return VK_RENDERING_CONTENTS_SECONDARY_COMMAND_BUFFERS_BIT_KHR;
    if (a_str == "VK_RENDERING_SUSPENDING_BIT_KHR"sv) return VK_RENDERING_SUSPENDING_BIT_KHR;
    if (a_str == "VK_RENDERING_RESUMING_BIT_KHR"sv) return VK_RENDERING_RESUMING_BIT_KHR;
    if (a_str == "VK_RENDERING_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_RENDERING_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkRenderingFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkRenderingFlagBitsKHR(VkRenderingFlagBitsKHR e)
{
    switch (e) {
        case VK_RENDERING_CONTENTS_SECONDARY_COMMAND_BUFFERS_BIT_KHR: return "VK_RENDERING_CONTENTS_SECONDARY_COMMAND_BUFFERS_BIT_KHR";
        case VK_RENDERING_SUSPENDING_BIT_KHR: return "VK_RENDERING_SUSPENDING_BIT_KHR";
        case VK_RENDERING_RESUMING_BIT_KHR: return "VK_RENDERING_RESUMING_BIT_KHR";
        case VK_RENDERING_FLAG_BITS_MAX_ENUM_KHR: return "VK_RENDERING_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkRenderingFlagBitsKHR";
}

std::string str_from_VkRenderingFlagsKHR(VkRenderingFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkRenderingFlagBitsKHR(static_cast<VkRenderingFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkRenderingFlagBitsKHR(static_cast<VkRenderingFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_KHR_performance_query
VkPerformanceCounterUnitKHR str_to_VkPerformanceCounterUnitKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_GENERIC_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_GENERIC_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_PERCENTAGE_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_PERCENTAGE_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_NANOSECONDS_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_NANOSECONDS_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_BYTES_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_BYTES_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_BYTES_PER_SECOND_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_BYTES_PER_SECOND_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_KELVIN_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_KELVIN_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_WATTS_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_WATTS_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_VOLTS_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_VOLTS_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_AMPS_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_AMPS_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_HERTZ_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_HERTZ_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_CYCLES_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_CYCLES_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_UNIT_MAX_ENUM_KHR"sv) return VK_PERFORMANCE_COUNTER_UNIT_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceCounterUnitKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceCounterUnitKHR(VkPerformanceCounterUnitKHR e)
{
    switch (e) {
        case VK_PERFORMANCE_COUNTER_UNIT_GENERIC_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_GENERIC_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_PERCENTAGE_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_PERCENTAGE_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_NANOSECONDS_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_NANOSECONDS_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_BYTES_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_BYTES_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_BYTES_PER_SECOND_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_BYTES_PER_SECOND_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_KELVIN_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_KELVIN_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_WATTS_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_WATTS_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_VOLTS_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_VOLTS_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_AMPS_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_AMPS_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_HERTZ_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_HERTZ_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_CYCLES_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_CYCLES_KHR";
        case VK_PERFORMANCE_COUNTER_UNIT_MAX_ENUM_KHR: return "VK_PERFORMANCE_COUNTER_UNIT_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkPerformanceCounterUnitKHR";
}

VkPerformanceCounterScopeKHR str_to_VkPerformanceCounterScopeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_BUFFER_KHR"sv) return VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_BUFFER_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_SCOPE_RENDER_PASS_KHR"sv) return VK_PERFORMANCE_COUNTER_SCOPE_RENDER_PASS_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_KHR"sv) return VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_KHR;
    if (a_str == "VK_QUERY_SCOPE_COMMAND_BUFFER_KHR"sv) return VK_QUERY_SCOPE_COMMAND_BUFFER_KHR;
    if (a_str == "VK_QUERY_SCOPE_RENDER_PASS_KHR"sv) return VK_QUERY_SCOPE_RENDER_PASS_KHR;
    if (a_str == "VK_QUERY_SCOPE_COMMAND_KHR"sv) return VK_QUERY_SCOPE_COMMAND_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_SCOPE_MAX_ENUM_KHR"sv) return VK_PERFORMANCE_COUNTER_SCOPE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceCounterScopeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceCounterScopeKHR(VkPerformanceCounterScopeKHR e)
{
    switch (e) {
        case VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_BUFFER_KHR: return "VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_BUFFER_KHR";
        case VK_PERFORMANCE_COUNTER_SCOPE_RENDER_PASS_KHR: return "VK_PERFORMANCE_COUNTER_SCOPE_RENDER_PASS_KHR";
        case VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_KHR: return "VK_PERFORMANCE_COUNTER_SCOPE_COMMAND_KHR";
        case VK_PERFORMANCE_COUNTER_SCOPE_MAX_ENUM_KHR: return "VK_PERFORMANCE_COUNTER_SCOPE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkPerformanceCounterScopeKHR";
}

VkPerformanceCounterStorageKHR str_to_VkPerformanceCounterStorageKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_INT32_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_INT32_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_INT64_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_INT64_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_UINT32_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_UINT32_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_UINT64_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_UINT64_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_FLOAT32_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_FLOAT32_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_FLOAT64_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_FLOAT64_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_STORAGE_MAX_ENUM_KHR"sv) return VK_PERFORMANCE_COUNTER_STORAGE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceCounterStorageKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceCounterStorageKHR(VkPerformanceCounterStorageKHR e)
{
    switch (e) {
        case VK_PERFORMANCE_COUNTER_STORAGE_INT32_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_INT32_KHR";
        case VK_PERFORMANCE_COUNTER_STORAGE_INT64_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_INT64_KHR";
        case VK_PERFORMANCE_COUNTER_STORAGE_UINT32_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_UINT32_KHR";
        case VK_PERFORMANCE_COUNTER_STORAGE_UINT64_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_UINT64_KHR";
        case VK_PERFORMANCE_COUNTER_STORAGE_FLOAT32_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_FLOAT32_KHR";
        case VK_PERFORMANCE_COUNTER_STORAGE_FLOAT64_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_FLOAT64_KHR";
        case VK_PERFORMANCE_COUNTER_STORAGE_MAX_ENUM_KHR: return "VK_PERFORMANCE_COUNTER_STORAGE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkPerformanceCounterStorageKHR";
}

VkPerformanceCounterDescriptionFlagBitsKHR str_to_VkPerformanceCounterDescriptionFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_BIT_KHR"sv) return VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_BIT_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_BIT_KHR"sv) return VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_BIT_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_KHR"sv) return VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_KHR"sv) return VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_KHR;
    if (a_str == "VK_PERFORMANCE_COUNTER_DESCRIPTION_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_PERFORMANCE_COUNTER_DESCRIPTION_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceCounterDescriptionFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceCounterDescriptionFlagBitsKHR(VkPerformanceCounterDescriptionFlagBitsKHR e)
{
    switch (e) {
        case VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_BIT_KHR: return "VK_PERFORMANCE_COUNTER_DESCRIPTION_PERFORMANCE_IMPACTING_BIT_KHR";
        case VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_BIT_KHR: return "VK_PERFORMANCE_COUNTER_DESCRIPTION_CONCURRENTLY_IMPACTED_BIT_KHR";
        case VK_PERFORMANCE_COUNTER_DESCRIPTION_FLAG_BITS_MAX_ENUM_KHR: return "VK_PERFORMANCE_COUNTER_DESCRIPTION_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkPerformanceCounterDescriptionFlagBitsKHR";
}

std::string str_from_VkPerformanceCounterDescriptionFlagsKHR(VkPerformanceCounterDescriptionFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPerformanceCounterDescriptionFlagBitsKHR(static_cast<VkPerformanceCounterDescriptionFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPerformanceCounterDescriptionFlagBitsKHR(static_cast<VkPerformanceCounterDescriptionFlagBitsKHR>(0)));
    return result;
}

 VkAcquireProfilingLockFlagBitsKHR str_to_VkAcquireProfilingLockFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACQUIRE_PROFILING_LOCK_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_ACQUIRE_PROFILING_LOCK_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAcquireProfilingLockFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkAcquireProfilingLockFlagBitsKHR(VkAcquireProfilingLockFlagBitsKHR e)
{
    switch (e) {
        case VK_ACQUIRE_PROFILING_LOCK_FLAG_BITS_MAX_ENUM_KHR: return "VK_ACQUIRE_PROFILING_LOCK_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkAcquireProfilingLockFlagBitsKHR";
}

std::string str_from_VkAcquireProfilingLockFlagsKHR(VkAcquireProfilingLockFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkAcquireProfilingLockFlagBitsKHR(static_cast<VkAcquireProfilingLockFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkAcquireProfilingLockFlagBitsKHR(static_cast<VkAcquireProfilingLockFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_KHR_fragment_shading_rate
VkFragmentShadingRateCombinerOpKHR str_to_VkFragmentShadingRateCombinerOpKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR"sv) return VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_REPLACE_KHR"sv) return VK_FRAGMENT_SHADING_RATE_COMBINER_OP_REPLACE_KHR;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MIN_KHR"sv) return VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MIN_KHR;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_KHR"sv) return VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_KHR;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MUL_KHR"sv) return VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MUL_KHR;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_ENUM_KHR"sv) return VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFragmentShadingRateCombinerOpKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkFragmentShadingRateCombinerOpKHR(VkFragmentShadingRateCombinerOpKHR e)
{
    switch (e) {
        case VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR: return "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_KEEP_KHR";
        case VK_FRAGMENT_SHADING_RATE_COMBINER_OP_REPLACE_KHR: return "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_REPLACE_KHR";
        case VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MIN_KHR: return "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MIN_KHR";
        case VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_KHR: return "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_KHR";
        case VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MUL_KHR: return "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MUL_KHR";
        case VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_ENUM_KHR: return "VK_FRAGMENT_SHADING_RATE_COMBINER_OP_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkFragmentShadingRateCombinerOpKHR";
}

#endif
#if VK_KHR_pipeline_executable_properties
VkPipelineExecutableStatisticFormatKHR str_to_VkPipelineExecutableStatisticFormatKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR"sv) return VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR;
    if (a_str == "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR"sv) return VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR;
    if (a_str == "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR"sv) return VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR;
    if (a_str == "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR"sv) return VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR;
    if (a_str == "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_MAX_ENUM_KHR"sv) return VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineExecutableStatisticFormatKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineExecutableStatisticFormatKHR(VkPipelineExecutableStatisticFormatKHR e)
{
    switch (e) {
        case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR: return "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR";
        case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR: return "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR";
        case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR: return "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR";
        case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR: return "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR";
        case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_MAX_ENUM_KHR: return "VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkPipelineExecutableStatisticFormatKHR";
}

#endif
#if VK_KHR_synchronization2
VkSubmitFlagBitsKHR str_to_VkSubmitFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SUBMIT_PROTECTED_BIT_KHR"sv) return VK_SUBMIT_PROTECTED_BIT_KHR;
    if (a_str == "VK_SUBMIT_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_SUBMIT_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSubmitFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkSubmitFlagBitsKHR(VkSubmitFlagBitsKHR e)
{
    switch (e) {
        case VK_SUBMIT_PROTECTED_BIT_KHR: return "VK_SUBMIT_PROTECTED_BIT_KHR";
        case VK_SUBMIT_FLAG_BITS_MAX_ENUM_KHR: return "VK_SUBMIT_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkSubmitFlagBitsKHR";
}

std::string str_from_VkSubmitFlagsKHR(VkSubmitFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSubmitFlagBitsKHR(static_cast<VkSubmitFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSubmitFlagBitsKHR(static_cast<VkSubmitFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_EXT_debug_report
VkDebugReportObjectTypeEXT str_to_VkDebugReportObjectTypeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_CU_MODULE_NVX_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_CU_MODULE_NVX_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_CU_FUNCTION_NVX_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_CU_FUNCTION_NVX_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR_EXT;
    if (a_str == "VK_DEBUG_REPORT_OBJECT_TYPE_MAX_ENUM_EXT"sv) return VK_DEBUG_REPORT_OBJECT_TYPE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDebugReportObjectTypeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDebugReportObjectTypeEXT(VkDebugReportObjectTypeEXT e)
{
    switch (e) {
        case VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_CU_MODULE_NVX_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_CU_MODULE_NVX_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_CU_FUNCTION_NVX_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_CU_FUNCTION_NVX_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_COLLECTION_FUCHSIA_EXT";
        case VK_DEBUG_REPORT_OBJECT_TYPE_MAX_ENUM_EXT: return "VK_DEBUG_REPORT_OBJECT_TYPE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDebugReportObjectTypeEXT";
}

VkDebugReportFlagBitsEXT str_to_VkDebugReportFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEBUG_REPORT_INFORMATION_BIT_EXT"sv) return VK_DEBUG_REPORT_INFORMATION_BIT_EXT;
    if (a_str == "VK_DEBUG_REPORT_WARNING_BIT_EXT"sv) return VK_DEBUG_REPORT_WARNING_BIT_EXT;
    if (a_str == "VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT"sv) return VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
    if (a_str == "VK_DEBUG_REPORT_ERROR_BIT_EXT"sv) return VK_DEBUG_REPORT_ERROR_BIT_EXT;
    if (a_str == "VK_DEBUG_REPORT_DEBUG_BIT_EXT"sv) return VK_DEBUG_REPORT_DEBUG_BIT_EXT;
    if (a_str == "VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDebugReportFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDebugReportFlagBitsEXT(VkDebugReportFlagBitsEXT e)
{
    switch (e) {
        case VK_DEBUG_REPORT_INFORMATION_BIT_EXT: return "VK_DEBUG_REPORT_INFORMATION_BIT_EXT";
        case VK_DEBUG_REPORT_WARNING_BIT_EXT: return "VK_DEBUG_REPORT_WARNING_BIT_EXT";
        case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT: return "VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT";
        case VK_DEBUG_REPORT_ERROR_BIT_EXT: return "VK_DEBUG_REPORT_ERROR_BIT_EXT";
        case VK_DEBUG_REPORT_DEBUG_BIT_EXT: return "VK_DEBUG_REPORT_DEBUG_BIT_EXT";
        case VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT: return "VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDebugReportFlagBitsEXT";
}

std::string str_from_VkDebugReportFlagsEXT(VkDebugReportFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDebugReportFlagBitsEXT(static_cast<VkDebugReportFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDebugReportFlagBitsEXT(static_cast<VkDebugReportFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_AMD_rasterization_order
VkRasterizationOrderAMD str_to_VkRasterizationOrderAMD(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_RASTERIZATION_ORDER_STRICT_AMD"sv) return VK_RASTERIZATION_ORDER_STRICT_AMD;
    if (a_str == "VK_RASTERIZATION_ORDER_RELAXED_AMD"sv) return VK_RASTERIZATION_ORDER_RELAXED_AMD;
    if (a_str == "VK_RASTERIZATION_ORDER_MAX_ENUM_AMD"sv) return VK_RASTERIZATION_ORDER_MAX_ENUM_AMD;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkRasterizationOrderAMD>(0x7FFFFFFF);
}

std::string_view str_from_VkRasterizationOrderAMD(VkRasterizationOrderAMD e)
{
    switch (e) {
        case VK_RASTERIZATION_ORDER_STRICT_AMD: return "VK_RASTERIZATION_ORDER_STRICT_AMD";
        case VK_RASTERIZATION_ORDER_RELAXED_AMD: return "VK_RASTERIZATION_ORDER_RELAXED_AMD";
        case VK_RASTERIZATION_ORDER_MAX_ENUM_AMD: return "VK_RASTERIZATION_ORDER_MAX_ENUM_AMD";
        default: break;
    }
    return "Unknown VkRasterizationOrderAMD";
}

#endif
#if VK_AMD_shader_info
VkShaderInfoTypeAMD str_to_VkShaderInfoTypeAMD(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHADER_INFO_TYPE_STATISTICS_AMD"sv) return VK_SHADER_INFO_TYPE_STATISTICS_AMD;
    if (a_str == "VK_SHADER_INFO_TYPE_BINARY_AMD"sv) return VK_SHADER_INFO_TYPE_BINARY_AMD;
    if (a_str == "VK_SHADER_INFO_TYPE_DISASSEMBLY_AMD"sv) return VK_SHADER_INFO_TYPE_DISASSEMBLY_AMD;
    if (a_str == "VK_SHADER_INFO_TYPE_MAX_ENUM_AMD"sv) return VK_SHADER_INFO_TYPE_MAX_ENUM_AMD;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkShaderInfoTypeAMD>(0x7FFFFFFF);
}

std::string_view str_from_VkShaderInfoTypeAMD(VkShaderInfoTypeAMD e)
{
    switch (e) {
        case VK_SHADER_INFO_TYPE_STATISTICS_AMD: return "VK_SHADER_INFO_TYPE_STATISTICS_AMD";
        case VK_SHADER_INFO_TYPE_BINARY_AMD: return "VK_SHADER_INFO_TYPE_BINARY_AMD";
        case VK_SHADER_INFO_TYPE_DISASSEMBLY_AMD: return "VK_SHADER_INFO_TYPE_DISASSEMBLY_AMD";
        case VK_SHADER_INFO_TYPE_MAX_ENUM_AMD: return "VK_SHADER_INFO_TYPE_MAX_ENUM_AMD";
        default: break;
    }
    return "Unknown VkShaderInfoTypeAMD";
}

#endif
#if VK_NV_external_memory_capabilities
VkExternalMemoryHandleTypeFlagBitsNV str_to_VkExternalMemoryHandleTypeFlagBitsNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_NV"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_NV"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_BIT_NV"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_KMT_BIT_NV"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_KMT_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM_NV"sv) return VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalMemoryHandleTypeFlagBitsNV>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalMemoryHandleTypeFlagBitsNV(VkExternalMemoryHandleTypeFlagBitsNV e)
{
    switch (e) {
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_NV: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT_NV";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_NV: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT_NV";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_BIT_NV: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_BIT_NV";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_KMT_BIT_NV: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_IMAGE_KMT_BIT_NV";
        case VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM_NV: return "VK_EXTERNAL_MEMORY_HANDLE_TYPE_FLAG_BITS_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkExternalMemoryHandleTypeFlagBitsNV";
}

std::string str_from_VkExternalMemoryHandleTypeFlagsNV(VkExternalMemoryHandleTypeFlagsNV f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalMemoryHandleTypeFlagBitsNV(static_cast<VkExternalMemoryHandleTypeFlagBitsNV>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalMemoryHandleTypeFlagBitsNV(static_cast<VkExternalMemoryHandleTypeFlagBitsNV>(0)));
    return result;
}

 VkExternalMemoryFeatureFlagBitsNV str_to_VkExternalMemoryFeatureFlagBitsNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_NV"sv) return VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_NV"sv) return VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_NV"sv) return VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_NV;
    if (a_str == "VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM_NV"sv) return VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkExternalMemoryFeatureFlagBitsNV>(0x7FFFFFFF);
}

std::string_view str_from_VkExternalMemoryFeatureFlagBitsNV(VkExternalMemoryFeatureFlagBitsNV e)
{
    switch (e) {
        case VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_NV: return "VK_EXTERNAL_MEMORY_FEATURE_DEDICATED_ONLY_BIT_NV";
        case VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_NV: return "VK_EXTERNAL_MEMORY_FEATURE_EXPORTABLE_BIT_NV";
        case VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_NV: return "VK_EXTERNAL_MEMORY_FEATURE_IMPORTABLE_BIT_NV";
        case VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM_NV: return "VK_EXTERNAL_MEMORY_FEATURE_FLAG_BITS_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkExternalMemoryFeatureFlagBitsNV";
}

std::string str_from_VkExternalMemoryFeatureFlagsNV(VkExternalMemoryFeatureFlagsNV f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkExternalMemoryFeatureFlagBitsNV(static_cast<VkExternalMemoryFeatureFlagBitsNV>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkExternalMemoryFeatureFlagBitsNV(static_cast<VkExternalMemoryFeatureFlagBitsNV>(0)));
    return result;
}

 #endif
#if VK_EXT_validation_flags
VkValidationCheckEXT str_to_VkValidationCheckEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VALIDATION_CHECK_ALL_EXT"sv) return VK_VALIDATION_CHECK_ALL_EXT;
    if (a_str == "VK_VALIDATION_CHECK_SHADERS_EXT"sv) return VK_VALIDATION_CHECK_SHADERS_EXT;
    if (a_str == "VK_VALIDATION_CHECK_MAX_ENUM_EXT"sv) return VK_VALIDATION_CHECK_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkValidationCheckEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkValidationCheckEXT(VkValidationCheckEXT e)
{
    switch (e) {
        case VK_VALIDATION_CHECK_ALL_EXT: return "VK_VALIDATION_CHECK_ALL_EXT";
        case VK_VALIDATION_CHECK_SHADERS_EXT: return "VK_VALIDATION_CHECK_SHADERS_EXT";
        case VK_VALIDATION_CHECK_MAX_ENUM_EXT: return "VK_VALIDATION_CHECK_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkValidationCheckEXT";
}

#endif
#if VK_EXT_conditional_rendering
VkConditionalRenderingFlagBitsEXT str_to_VkConditionalRenderingFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_CONDITIONAL_RENDERING_INVERTED_BIT_EXT"sv) return VK_CONDITIONAL_RENDERING_INVERTED_BIT_EXT;
    if (a_str == "VK_CONDITIONAL_RENDERING_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_CONDITIONAL_RENDERING_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkConditionalRenderingFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkConditionalRenderingFlagBitsEXT(VkConditionalRenderingFlagBitsEXT e)
{
    switch (e) {
        case VK_CONDITIONAL_RENDERING_INVERTED_BIT_EXT: return "VK_CONDITIONAL_RENDERING_INVERTED_BIT_EXT";
        case VK_CONDITIONAL_RENDERING_FLAG_BITS_MAX_ENUM_EXT: return "VK_CONDITIONAL_RENDERING_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkConditionalRenderingFlagBitsEXT";
}

std::string str_from_VkConditionalRenderingFlagsEXT(VkConditionalRenderingFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkConditionalRenderingFlagBitsEXT(static_cast<VkConditionalRenderingFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkConditionalRenderingFlagBitsEXT(static_cast<VkConditionalRenderingFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_EXT_display_surface_counter
VkSurfaceCounterFlagBitsEXT str_to_VkSurfaceCounterFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SURFACE_COUNTER_VBLANK_BIT_EXT"sv) return VK_SURFACE_COUNTER_VBLANK_BIT_EXT;
    if (a_str == "VK_SURFACE_COUNTER_VBLANK_EXT"sv) return VK_SURFACE_COUNTER_VBLANK_EXT;
    if (a_str == "VK_SURFACE_COUNTER_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_SURFACE_COUNTER_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkSurfaceCounterFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkSurfaceCounterFlagBitsEXT(VkSurfaceCounterFlagBitsEXT e)
{
    switch (e) {
        case VK_SURFACE_COUNTER_VBLANK_BIT_EXT: return "VK_SURFACE_COUNTER_VBLANK_BIT_EXT";
        case VK_SURFACE_COUNTER_FLAG_BITS_MAX_ENUM_EXT: return "VK_SURFACE_COUNTER_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkSurfaceCounterFlagBitsEXT";
}

std::string str_from_VkSurfaceCounterFlagsEXT(VkSurfaceCounterFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkSurfaceCounterFlagBitsEXT(static_cast<VkSurfaceCounterFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkSurfaceCounterFlagBitsEXT(static_cast<VkSurfaceCounterFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_EXT_display_control
VkDisplayPowerStateEXT str_to_VkDisplayPowerStateEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DISPLAY_POWER_STATE_OFF_EXT"sv) return VK_DISPLAY_POWER_STATE_OFF_EXT;
    if (a_str == "VK_DISPLAY_POWER_STATE_SUSPEND_EXT"sv) return VK_DISPLAY_POWER_STATE_SUSPEND_EXT;
    if (a_str == "VK_DISPLAY_POWER_STATE_ON_EXT"sv) return VK_DISPLAY_POWER_STATE_ON_EXT;
    if (a_str == "VK_DISPLAY_POWER_STATE_MAX_ENUM_EXT"sv) return VK_DISPLAY_POWER_STATE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDisplayPowerStateEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDisplayPowerStateEXT(VkDisplayPowerStateEXT e)
{
    switch (e) {
        case VK_DISPLAY_POWER_STATE_OFF_EXT: return "VK_DISPLAY_POWER_STATE_OFF_EXT";
        case VK_DISPLAY_POWER_STATE_SUSPEND_EXT: return "VK_DISPLAY_POWER_STATE_SUSPEND_EXT";
        case VK_DISPLAY_POWER_STATE_ON_EXT: return "VK_DISPLAY_POWER_STATE_ON_EXT";
        case VK_DISPLAY_POWER_STATE_MAX_ENUM_EXT: return "VK_DISPLAY_POWER_STATE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDisplayPowerStateEXT";
}

VkDeviceEventTypeEXT str_to_VkDeviceEventTypeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEVICE_EVENT_TYPE_DISPLAY_HOTPLUG_EXT"sv) return VK_DEVICE_EVENT_TYPE_DISPLAY_HOTPLUG_EXT;
    if (a_str == "VK_DEVICE_EVENT_TYPE_MAX_ENUM_EXT"sv) return VK_DEVICE_EVENT_TYPE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDeviceEventTypeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDeviceEventTypeEXT(VkDeviceEventTypeEXT e)
{
    switch (e) {
        case VK_DEVICE_EVENT_TYPE_DISPLAY_HOTPLUG_EXT: return "VK_DEVICE_EVENT_TYPE_DISPLAY_HOTPLUG_EXT";
        case VK_DEVICE_EVENT_TYPE_MAX_ENUM_EXT: return "VK_DEVICE_EVENT_TYPE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDeviceEventTypeEXT";
}

VkDisplayEventTypeEXT str_to_VkDisplayEventTypeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DISPLAY_EVENT_TYPE_FIRST_PIXEL_OUT_EXT"sv) return VK_DISPLAY_EVENT_TYPE_FIRST_PIXEL_OUT_EXT;
    if (a_str == "VK_DISPLAY_EVENT_TYPE_MAX_ENUM_EXT"sv) return VK_DISPLAY_EVENT_TYPE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDisplayEventTypeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDisplayEventTypeEXT(VkDisplayEventTypeEXT e)
{
    switch (e) {
        case VK_DISPLAY_EVENT_TYPE_FIRST_PIXEL_OUT_EXT: return "VK_DISPLAY_EVENT_TYPE_FIRST_PIXEL_OUT_EXT";
        case VK_DISPLAY_EVENT_TYPE_MAX_ENUM_EXT: return "VK_DISPLAY_EVENT_TYPE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDisplayEventTypeEXT";
}

#endif
#if VK_NV_viewport_swizzle
VkViewportCoordinateSwizzleNV str_to_VkViewportCoordinateSwizzleNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_X_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_X_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_X_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_X_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Y_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Y_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Y_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Y_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Z_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Z_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Z_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Z_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_W_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_W_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_W_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_W_NV;
    if (a_str == "VK_VIEWPORT_COORDINATE_SWIZZLE_MAX_ENUM_NV"sv) return VK_VIEWPORT_COORDINATE_SWIZZLE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkViewportCoordinateSwizzleNV>(0x7FFFFFFF);
}

std::string_view str_from_VkViewportCoordinateSwizzleNV(VkViewportCoordinateSwizzleNV e)
{
    switch (e) {
        case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_X_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_X_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_X_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_X_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Y_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Y_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Y_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Y_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Z_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_Z_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Z_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_Z_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_W_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_POSITIVE_W_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_W_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_NEGATIVE_W_NV";
        case VK_VIEWPORT_COORDINATE_SWIZZLE_MAX_ENUM_NV: return "VK_VIEWPORT_COORDINATE_SWIZZLE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkViewportCoordinateSwizzleNV";
}

#endif
#if VK_EXT_discard_rectangles
VkDiscardRectangleModeEXT str_to_VkDiscardRectangleModeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DISCARD_RECTANGLE_MODE_INCLUSIVE_EXT"sv) return VK_DISCARD_RECTANGLE_MODE_INCLUSIVE_EXT;
    if (a_str == "VK_DISCARD_RECTANGLE_MODE_EXCLUSIVE_EXT"sv) return VK_DISCARD_RECTANGLE_MODE_EXCLUSIVE_EXT;
    if (a_str == "VK_DISCARD_RECTANGLE_MODE_MAX_ENUM_EXT"sv) return VK_DISCARD_RECTANGLE_MODE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDiscardRectangleModeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDiscardRectangleModeEXT(VkDiscardRectangleModeEXT e)
{
    switch (e) {
        case VK_DISCARD_RECTANGLE_MODE_INCLUSIVE_EXT: return "VK_DISCARD_RECTANGLE_MODE_INCLUSIVE_EXT";
        case VK_DISCARD_RECTANGLE_MODE_EXCLUSIVE_EXT: return "VK_DISCARD_RECTANGLE_MODE_EXCLUSIVE_EXT";
        case VK_DISCARD_RECTANGLE_MODE_MAX_ENUM_EXT: return "VK_DISCARD_RECTANGLE_MODE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDiscardRectangleModeEXT";
}

#endif
#if VK_EXT_conservative_rasterization
VkConservativeRasterizationModeEXT str_to_VkConservativeRasterizationModeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_CONSERVATIVE_RASTERIZATION_MODE_DISABLED_EXT"sv) return VK_CONSERVATIVE_RASTERIZATION_MODE_DISABLED_EXT;
    if (a_str == "VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT"sv) return VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT;
    if (a_str == "VK_CONSERVATIVE_RASTERIZATION_MODE_UNDERESTIMATE_EXT"sv) return VK_CONSERVATIVE_RASTERIZATION_MODE_UNDERESTIMATE_EXT;
    if (a_str == "VK_CONSERVATIVE_RASTERIZATION_MODE_MAX_ENUM_EXT"sv) return VK_CONSERVATIVE_RASTERIZATION_MODE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkConservativeRasterizationModeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkConservativeRasterizationModeEXT(VkConservativeRasterizationModeEXT e)
{
    switch (e) {
        case VK_CONSERVATIVE_RASTERIZATION_MODE_DISABLED_EXT: return "VK_CONSERVATIVE_RASTERIZATION_MODE_DISABLED_EXT";
        case VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT: return "VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT";
        case VK_CONSERVATIVE_RASTERIZATION_MODE_UNDERESTIMATE_EXT: return "VK_CONSERVATIVE_RASTERIZATION_MODE_UNDERESTIMATE_EXT";
        case VK_CONSERVATIVE_RASTERIZATION_MODE_MAX_ENUM_EXT: return "VK_CONSERVATIVE_RASTERIZATION_MODE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkConservativeRasterizationModeEXT";
}

#endif
#if VK_EXT_debug_utils
VkDebugUtilsMessageSeverityFlagBitsEXT str_to_VkDebugUtilsMessageSeverityFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDebugUtilsMessageSeverityFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDebugUtilsMessageSeverityFlagBitsEXT(VkDebugUtilsMessageSeverityFlagBitsEXT e)
{
    switch (e) {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT: return "VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDebugUtilsMessageSeverityFlagBitsEXT";
}

std::string str_from_VkDebugUtilsMessageSeverityFlagsEXT(VkDebugUtilsMessageSeverityFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDebugUtilsMessageSeverityFlagBitsEXT(static_cast<VkDebugUtilsMessageSeverityFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDebugUtilsMessageSeverityFlagBitsEXT(static_cast<VkDebugUtilsMessageSeverityFlagBitsEXT>(0)));
    return result;
}

 VkDebugUtilsMessageTypeFlagBitsEXT str_to_VkDebugUtilsMessageTypeFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    if (a_str == "VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDebugUtilsMessageTypeFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDebugUtilsMessageTypeFlagBitsEXT(VkDebugUtilsMessageTypeFlagBitsEXT e)
{
    switch (e) {
        case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT: return "VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT";
        case VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT: return "VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDebugUtilsMessageTypeFlagBitsEXT";
}

std::string str_from_VkDebugUtilsMessageTypeFlagsEXT(VkDebugUtilsMessageTypeFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDebugUtilsMessageTypeFlagBitsEXT(static_cast<VkDebugUtilsMessageTypeFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDebugUtilsMessageTypeFlagBitsEXT(static_cast<VkDebugUtilsMessageTypeFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_EXT_blend_operation_advanced
VkBlendOverlapEXT str_to_VkBlendOverlapEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BLEND_OVERLAP_UNCORRELATED_EXT"sv) return VK_BLEND_OVERLAP_UNCORRELATED_EXT;
    if (a_str == "VK_BLEND_OVERLAP_DISJOINT_EXT"sv) return VK_BLEND_OVERLAP_DISJOINT_EXT;
    if (a_str == "VK_BLEND_OVERLAP_CONJOINT_EXT"sv) return VK_BLEND_OVERLAP_CONJOINT_EXT;
    if (a_str == "VK_BLEND_OVERLAP_MAX_ENUM_EXT"sv) return VK_BLEND_OVERLAP_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBlendOverlapEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkBlendOverlapEXT(VkBlendOverlapEXT e)
{
    switch (e) {
        case VK_BLEND_OVERLAP_UNCORRELATED_EXT: return "VK_BLEND_OVERLAP_UNCORRELATED_EXT";
        case VK_BLEND_OVERLAP_DISJOINT_EXT: return "VK_BLEND_OVERLAP_DISJOINT_EXT";
        case VK_BLEND_OVERLAP_CONJOINT_EXT: return "VK_BLEND_OVERLAP_CONJOINT_EXT";
        case VK_BLEND_OVERLAP_MAX_ENUM_EXT: return "VK_BLEND_OVERLAP_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkBlendOverlapEXT";
}

#endif
#if VK_NV_framebuffer_mixed_samples
VkCoverageModulationModeNV str_to_VkCoverageModulationModeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COVERAGE_MODULATION_MODE_NONE_NV"sv) return VK_COVERAGE_MODULATION_MODE_NONE_NV;
    if (a_str == "VK_COVERAGE_MODULATION_MODE_RGB_NV"sv) return VK_COVERAGE_MODULATION_MODE_RGB_NV;
    if (a_str == "VK_COVERAGE_MODULATION_MODE_ALPHA_NV"sv) return VK_COVERAGE_MODULATION_MODE_ALPHA_NV;
    if (a_str == "VK_COVERAGE_MODULATION_MODE_RGBA_NV"sv) return VK_COVERAGE_MODULATION_MODE_RGBA_NV;
    if (a_str == "VK_COVERAGE_MODULATION_MODE_MAX_ENUM_NV"sv) return VK_COVERAGE_MODULATION_MODE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCoverageModulationModeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkCoverageModulationModeNV(VkCoverageModulationModeNV e)
{
    switch (e) {
        case VK_COVERAGE_MODULATION_MODE_NONE_NV: return "VK_COVERAGE_MODULATION_MODE_NONE_NV";
        case VK_COVERAGE_MODULATION_MODE_RGB_NV: return "VK_COVERAGE_MODULATION_MODE_RGB_NV";
        case VK_COVERAGE_MODULATION_MODE_ALPHA_NV: return "VK_COVERAGE_MODULATION_MODE_ALPHA_NV";
        case VK_COVERAGE_MODULATION_MODE_RGBA_NV: return "VK_COVERAGE_MODULATION_MODE_RGBA_NV";
        case VK_COVERAGE_MODULATION_MODE_MAX_ENUM_NV: return "VK_COVERAGE_MODULATION_MODE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkCoverageModulationModeNV";
}

#endif
#if VK_EXT_validation_cache
VkValidationCacheHeaderVersionEXT str_to_VkValidationCacheHeaderVersionEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VALIDATION_CACHE_HEADER_VERSION_ONE_EXT"sv) return VK_VALIDATION_CACHE_HEADER_VERSION_ONE_EXT;
    if (a_str == "VK_VALIDATION_CACHE_HEADER_VERSION_MAX_ENUM_EXT"sv) return VK_VALIDATION_CACHE_HEADER_VERSION_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkValidationCacheHeaderVersionEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkValidationCacheHeaderVersionEXT(VkValidationCacheHeaderVersionEXT e)
{
    switch (e) {
        case VK_VALIDATION_CACHE_HEADER_VERSION_ONE_EXT: return "VK_VALIDATION_CACHE_HEADER_VERSION_ONE_EXT";
        case VK_VALIDATION_CACHE_HEADER_VERSION_MAX_ENUM_EXT: return "VK_VALIDATION_CACHE_HEADER_VERSION_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkValidationCacheHeaderVersionEXT";
}

#endif
#if VK_NV_shading_rate_image
VkShadingRatePaletteEntryNV str_to_VkShadingRatePaletteEntryNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_NO_INVOCATIONS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_NO_INVOCATIONS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_16_INVOCATIONS_PER_PIXEL_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_16_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_8_INVOCATIONS_PER_PIXEL_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_8_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_4_INVOCATIONS_PER_PIXEL_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_4_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_2_INVOCATIONS_PER_PIXEL_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_2_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_PIXEL_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_PIXEL_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X1_PIXELS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X1_PIXELS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_1X2_PIXELS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_1X2_PIXELS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X2_PIXELS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X2_PIXELS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X2_PIXELS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X2_PIXELS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X4_PIXELS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X4_PIXELS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X4_PIXELS_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X4_PIXELS_NV;
    if (a_str == "VK_SHADING_RATE_PALETTE_ENTRY_MAX_ENUM_NV"sv) return VK_SHADING_RATE_PALETTE_ENTRY_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkShadingRatePaletteEntryNV>(0x7FFFFFFF);
}

std::string_view str_from_VkShadingRatePaletteEntryNV(VkShadingRatePaletteEntryNV e)
{
    switch (e) {
        case VK_SHADING_RATE_PALETTE_ENTRY_NO_INVOCATIONS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_NO_INVOCATIONS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_16_INVOCATIONS_PER_PIXEL_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_16_INVOCATIONS_PER_PIXEL_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_8_INVOCATIONS_PER_PIXEL_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_8_INVOCATIONS_PER_PIXEL_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_4_INVOCATIONS_PER_PIXEL_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_4_INVOCATIONS_PER_PIXEL_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_2_INVOCATIONS_PER_PIXEL_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_2_INVOCATIONS_PER_PIXEL_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_PIXEL_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_PIXEL_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X1_PIXELS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X1_PIXELS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_1X2_PIXELS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_1X2_PIXELS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X2_PIXELS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X2_PIXELS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X2_PIXELS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X2_PIXELS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X4_PIXELS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_2X4_PIXELS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X4_PIXELS_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_1_INVOCATION_PER_4X4_PIXELS_NV";
        case VK_SHADING_RATE_PALETTE_ENTRY_MAX_ENUM_NV: return "VK_SHADING_RATE_PALETTE_ENTRY_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkShadingRatePaletteEntryNV";
}

VkCoarseSampleOrderTypeNV str_to_VkCoarseSampleOrderTypeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COARSE_SAMPLE_ORDER_TYPE_DEFAULT_NV"sv) return VK_COARSE_SAMPLE_ORDER_TYPE_DEFAULT_NV;
    if (a_str == "VK_COARSE_SAMPLE_ORDER_TYPE_CUSTOM_NV"sv) return VK_COARSE_SAMPLE_ORDER_TYPE_CUSTOM_NV;
    if (a_str == "VK_COARSE_SAMPLE_ORDER_TYPE_PIXEL_MAJOR_NV"sv) return VK_COARSE_SAMPLE_ORDER_TYPE_PIXEL_MAJOR_NV;
    if (a_str == "VK_COARSE_SAMPLE_ORDER_TYPE_SAMPLE_MAJOR_NV"sv) return VK_COARSE_SAMPLE_ORDER_TYPE_SAMPLE_MAJOR_NV;
    if (a_str == "VK_COARSE_SAMPLE_ORDER_TYPE_MAX_ENUM_NV"sv) return VK_COARSE_SAMPLE_ORDER_TYPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCoarseSampleOrderTypeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkCoarseSampleOrderTypeNV(VkCoarseSampleOrderTypeNV e)
{
    switch (e) {
        case VK_COARSE_SAMPLE_ORDER_TYPE_DEFAULT_NV: return "VK_COARSE_SAMPLE_ORDER_TYPE_DEFAULT_NV";
        case VK_COARSE_SAMPLE_ORDER_TYPE_CUSTOM_NV: return "VK_COARSE_SAMPLE_ORDER_TYPE_CUSTOM_NV";
        case VK_COARSE_SAMPLE_ORDER_TYPE_PIXEL_MAJOR_NV: return "VK_COARSE_SAMPLE_ORDER_TYPE_PIXEL_MAJOR_NV";
        case VK_COARSE_SAMPLE_ORDER_TYPE_SAMPLE_MAJOR_NV: return "VK_COARSE_SAMPLE_ORDER_TYPE_SAMPLE_MAJOR_NV";
        case VK_COARSE_SAMPLE_ORDER_TYPE_MAX_ENUM_NV: return "VK_COARSE_SAMPLE_ORDER_TYPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkCoarseSampleOrderTypeNV";
}

#endif
#if VK_NV_ray_tracing
VkRayTracingShaderGroupTypeKHR str_to_VkRayTracingShaderGroupTypeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_NV"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_NV;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_NV"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_NV;
    if (a_str == "VK_RAY_TRACING_SHADER_GROUP_TYPE_MAX_ENUM_KHR"sv) return VK_RAY_TRACING_SHADER_GROUP_TYPE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkRayTracingShaderGroupTypeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkRayTracingShaderGroupTypeKHR(VkRayTracingShaderGroupTypeKHR e)
{
    switch (e) {
        case VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR: return "VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR";
        case VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR: return "VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR";
        case VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR: return "VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR";
        case VK_RAY_TRACING_SHADER_GROUP_TYPE_MAX_ENUM_KHR: return "VK_RAY_TRACING_SHADER_GROUP_TYPE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkRayTracingShaderGroupTypeKHR";
}

VkGeometryTypeKHR str_to_VkGeometryTypeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_GEOMETRY_TYPE_TRIANGLES_KHR"sv) return VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    if (a_str == "VK_GEOMETRY_TYPE_AABBS_KHR"sv) return VK_GEOMETRY_TYPE_AABBS_KHR;
    if (a_str == "VK_GEOMETRY_TYPE_INSTANCES_KHR"sv) return VK_GEOMETRY_TYPE_INSTANCES_KHR;
    if (a_str == "VK_GEOMETRY_TYPE_TRIANGLES_NV"sv) return VK_GEOMETRY_TYPE_TRIANGLES_NV;
    if (a_str == "VK_GEOMETRY_TYPE_AABBS_NV"sv) return VK_GEOMETRY_TYPE_AABBS_NV;
    if (a_str == "VK_GEOMETRY_TYPE_MAX_ENUM_KHR"sv) return VK_GEOMETRY_TYPE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkGeometryTypeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkGeometryTypeKHR(VkGeometryTypeKHR e)
{
    switch (e) {
        case VK_GEOMETRY_TYPE_TRIANGLES_KHR: return "VK_GEOMETRY_TYPE_TRIANGLES_KHR";
        case VK_GEOMETRY_TYPE_AABBS_KHR: return "VK_GEOMETRY_TYPE_AABBS_KHR";
        case VK_GEOMETRY_TYPE_INSTANCES_KHR: return "VK_GEOMETRY_TYPE_INSTANCES_KHR";
        case VK_GEOMETRY_TYPE_MAX_ENUM_KHR: return "VK_GEOMETRY_TYPE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkGeometryTypeKHR";
}

VkAccelerationStructureTypeKHR str_to_VkAccelerationStructureTypeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR"sv) return VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR"sv) return VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_TYPE_GENERIC_KHR"sv) return VK_ACCELERATION_STRUCTURE_TYPE_GENERIC_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV"sv) return VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV"sv) return VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_TYPE_MAX_ENUM_KHR"sv) return VK_ACCELERATION_STRUCTURE_TYPE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccelerationStructureTypeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkAccelerationStructureTypeKHR(VkAccelerationStructureTypeKHR e)
{
    switch (e) {
        case VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR: return "VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR";
        case VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR: return "VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR";
        case VK_ACCELERATION_STRUCTURE_TYPE_GENERIC_KHR: return "VK_ACCELERATION_STRUCTURE_TYPE_GENERIC_KHR";
        case VK_ACCELERATION_STRUCTURE_TYPE_MAX_ENUM_KHR: return "VK_ACCELERATION_STRUCTURE_TYPE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkAccelerationStructureTypeKHR";
}

VkCopyAccelerationStructureModeKHR str_to_VkCopyAccelerationStructureModeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_CLONE_KHR"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_CLONE_KHR;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_COMPACT_KHR"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_COMPACT_KHR;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_SERIALIZE_KHR"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_SERIALIZE_KHR;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_DESERIALIZE_KHR"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_DESERIALIZE_KHR;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_CLONE_NV"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_CLONE_NV;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_COMPACT_NV"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_COMPACT_NV;
    if (a_str == "VK_COPY_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR"sv) return VK_COPY_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCopyAccelerationStructureModeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkCopyAccelerationStructureModeKHR(VkCopyAccelerationStructureModeKHR e)
{
    switch (e) {
        case VK_COPY_ACCELERATION_STRUCTURE_MODE_CLONE_KHR: return "VK_COPY_ACCELERATION_STRUCTURE_MODE_CLONE_KHR";
        case VK_COPY_ACCELERATION_STRUCTURE_MODE_COMPACT_KHR: return "VK_COPY_ACCELERATION_STRUCTURE_MODE_COMPACT_KHR";
        case VK_COPY_ACCELERATION_STRUCTURE_MODE_SERIALIZE_KHR: return "VK_COPY_ACCELERATION_STRUCTURE_MODE_SERIALIZE_KHR";
        case VK_COPY_ACCELERATION_STRUCTURE_MODE_DESERIALIZE_KHR: return "VK_COPY_ACCELERATION_STRUCTURE_MODE_DESERIALIZE_KHR";
        case VK_COPY_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR: return "VK_COPY_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkCopyAccelerationStructureModeKHR";
}

VkAccelerationStructureMemoryRequirementsTypeNV str_to_VkAccelerationStructureMemoryRequirementsTypeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV"sv) return VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_BUILD_SCRATCH_NV"sv) return VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_BUILD_SCRATCH_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_UPDATE_SCRATCH_NV"sv) return VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_UPDATE_SCRATCH_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_MAX_ENUM_NV"sv) return VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccelerationStructureMemoryRequirementsTypeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkAccelerationStructureMemoryRequirementsTypeNV(VkAccelerationStructureMemoryRequirementsTypeNV e)
{
    switch (e) {
        case VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV: return "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV";
        case VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_BUILD_SCRATCH_NV: return "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_BUILD_SCRATCH_NV";
        case VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_UPDATE_SCRATCH_NV: return "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_UPDATE_SCRATCH_NV";
        case VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_MAX_ENUM_NV: return "VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkAccelerationStructureMemoryRequirementsTypeNV";
}

VkGeometryFlagBitsKHR str_to_VkGeometryFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_GEOMETRY_OPAQUE_BIT_KHR"sv) return VK_GEOMETRY_OPAQUE_BIT_KHR;
    if (a_str == "VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_KHR"sv) return VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_KHR;
    if (a_str == "VK_GEOMETRY_OPAQUE_BIT_NV"sv) return VK_GEOMETRY_OPAQUE_BIT_NV;
    if (a_str == "VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_NV"sv) return VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_NV;
    if (a_str == "VK_GEOMETRY_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_GEOMETRY_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkGeometryFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkGeometryFlagBitsKHR(VkGeometryFlagBitsKHR e)
{
    switch (e) {
        case VK_GEOMETRY_OPAQUE_BIT_KHR: return "VK_GEOMETRY_OPAQUE_BIT_KHR";
        case VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_KHR: return "VK_GEOMETRY_NO_DUPLICATE_ANY_HIT_INVOCATION_BIT_KHR";
        case VK_GEOMETRY_FLAG_BITS_MAX_ENUM_KHR: return "VK_GEOMETRY_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkGeometryFlagBitsKHR";
}

std::string str_from_VkGeometryFlagsKHR(VkGeometryFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkGeometryFlagBitsKHR(static_cast<VkGeometryFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkGeometryFlagBitsKHR(static_cast<VkGeometryFlagBitsKHR>(0)));
    return result;
}

 VkGeometryInstanceFlagBitsKHR str_to_VkGeometryInstanceFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR"sv) return VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
    if (a_str == "VK_GEOMETRY_INSTANCE_TRIANGLE_FLIP_FACING_BIT_KHR"sv) return VK_GEOMETRY_INSTANCE_TRIANGLE_FLIP_FACING_BIT_KHR;
    if (a_str == "VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_KHR"sv) return VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_KHR;
    if (a_str == "VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_KHR"sv) return VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_KHR;
    if (a_str == "VK_GEOMETRY_INSTANCE_TRIANGLE_FRONT_COUNTERCLOCKWISE_BIT_KHR"sv) return VK_GEOMETRY_INSTANCE_TRIANGLE_FRONT_COUNTERCLOCKWISE_BIT_KHR;
    if (a_str == "VK_GEOMETRY_INSTANCE_TRIANGLE_CULL_DISABLE_BIT_NV"sv) return VK_GEOMETRY_INSTANCE_TRIANGLE_CULL_DISABLE_BIT_NV;
    if (a_str == "VK_GEOMETRY_INSTANCE_TRIANGLE_FRONT_COUNTERCLOCKWISE_BIT_NV"sv) return VK_GEOMETRY_INSTANCE_TRIANGLE_FRONT_COUNTERCLOCKWISE_BIT_NV;
    if (a_str == "VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_NV"sv) return VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_NV;
    if (a_str == "VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_NV"sv) return VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_NV;
    if (a_str == "VK_GEOMETRY_INSTANCE_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_GEOMETRY_INSTANCE_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkGeometryInstanceFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkGeometryInstanceFlagBitsKHR(VkGeometryInstanceFlagBitsKHR e)
{
    switch (e) {
        case VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR: return "VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR";
        case VK_GEOMETRY_INSTANCE_TRIANGLE_FLIP_FACING_BIT_KHR: return "VK_GEOMETRY_INSTANCE_TRIANGLE_FLIP_FACING_BIT_KHR";
        case VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_KHR: return "VK_GEOMETRY_INSTANCE_FORCE_OPAQUE_BIT_KHR";
        case VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_KHR: return "VK_GEOMETRY_INSTANCE_FORCE_NO_OPAQUE_BIT_KHR";
        case VK_GEOMETRY_INSTANCE_FLAG_BITS_MAX_ENUM_KHR: return "VK_GEOMETRY_INSTANCE_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkGeometryInstanceFlagBitsKHR";
}

std::string str_from_VkGeometryInstanceFlagsKHR(VkGeometryInstanceFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkGeometryInstanceFlagBitsKHR(static_cast<VkGeometryInstanceFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkGeometryInstanceFlagBitsKHR(static_cast<VkGeometryInstanceFlagBitsKHR>(0)));
    return result;
}

 VkBuildAccelerationStructureFlagBitsKHR str_to_VkBuildAccelerationStructureFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_MOTION_BIT_NV"sv) return VK_BUILD_ACCELERATION_STRUCTURE_MOTION_BIT_NV;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_NV"sv) return VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_NV;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_NV"sv) return VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_NV;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_NV"sv) return VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_NV;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_NV"sv) return VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_NV;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_NV"sv) return VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_NV;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBuildAccelerationStructureFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkBuildAccelerationStructureFlagBitsKHR(VkBuildAccelerationStructureFlagBitsKHR e)
{
    switch (e) {
        case VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_LOW_MEMORY_BIT_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_MOTION_BIT_NV: return "VK_BUILD_ACCELERATION_STRUCTURE_MOTION_BIT_NV";
        case VK_BUILD_ACCELERATION_STRUCTURE_FLAG_BITS_MAX_ENUM_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkBuildAccelerationStructureFlagBitsKHR";
}

std::string str_from_VkBuildAccelerationStructureFlagsKHR(VkBuildAccelerationStructureFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkBuildAccelerationStructureFlagBitsKHR(static_cast<VkBuildAccelerationStructureFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkBuildAccelerationStructureFlagBitsKHR(static_cast<VkBuildAccelerationStructureFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_EXT_global_priority
VkQueueGlobalPriorityEXT str_to_VkQueueGlobalPriorityEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUEUE_GLOBAL_PRIORITY_LOW_EXT"sv) return VK_QUEUE_GLOBAL_PRIORITY_LOW_EXT;
    if (a_str == "VK_QUEUE_GLOBAL_PRIORITY_MEDIUM_EXT"sv) return VK_QUEUE_GLOBAL_PRIORITY_MEDIUM_EXT;
    if (a_str == "VK_QUEUE_GLOBAL_PRIORITY_HIGH_EXT"sv) return VK_QUEUE_GLOBAL_PRIORITY_HIGH_EXT;
    if (a_str == "VK_QUEUE_GLOBAL_PRIORITY_REALTIME_EXT"sv) return VK_QUEUE_GLOBAL_PRIORITY_REALTIME_EXT;
    if (a_str == "VK_QUEUE_GLOBAL_PRIORITY_MAX_ENUM_EXT"sv) return VK_QUEUE_GLOBAL_PRIORITY_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueueGlobalPriorityEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkQueueGlobalPriorityEXT(VkQueueGlobalPriorityEXT e)
{
    switch (e) {
        case VK_QUEUE_GLOBAL_PRIORITY_LOW_EXT: return "VK_QUEUE_GLOBAL_PRIORITY_LOW_EXT";
        case VK_QUEUE_GLOBAL_PRIORITY_MEDIUM_EXT: return "VK_QUEUE_GLOBAL_PRIORITY_MEDIUM_EXT";
        case VK_QUEUE_GLOBAL_PRIORITY_HIGH_EXT: return "VK_QUEUE_GLOBAL_PRIORITY_HIGH_EXT";
        case VK_QUEUE_GLOBAL_PRIORITY_REALTIME_EXT: return "VK_QUEUE_GLOBAL_PRIORITY_REALTIME_EXT";
        case VK_QUEUE_GLOBAL_PRIORITY_MAX_ENUM_EXT: return "VK_QUEUE_GLOBAL_PRIORITY_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkQueueGlobalPriorityEXT";
}

#endif
#if VK_AMD_pipeline_compiler_control
VkPipelineCompilerControlFlagBitsAMD str_to_VkPipelineCompilerControlFlagBitsAMD(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_COMPILER_CONTROL_FLAG_BITS_MAX_ENUM_AMD"sv) return VK_PIPELINE_COMPILER_CONTROL_FLAG_BITS_MAX_ENUM_AMD;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineCompilerControlFlagBitsAMD>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineCompilerControlFlagBitsAMD(VkPipelineCompilerControlFlagBitsAMD e)
{
    switch (e) {
        case VK_PIPELINE_COMPILER_CONTROL_FLAG_BITS_MAX_ENUM_AMD: return "VK_PIPELINE_COMPILER_CONTROL_FLAG_BITS_MAX_ENUM_AMD";
        default: break;
    }
    return "Unknown VkPipelineCompilerControlFlagBitsAMD";
}

std::string str_from_VkPipelineCompilerControlFlagsAMD(VkPipelineCompilerControlFlagsAMD f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPipelineCompilerControlFlagBitsAMD(static_cast<VkPipelineCompilerControlFlagBitsAMD>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPipelineCompilerControlFlagBitsAMD(static_cast<VkPipelineCompilerControlFlagBitsAMD>(0)));
    return result;
}

 #endif
#if VK_EXT_calibrated_timestamps
VkTimeDomainEXT str_to_VkTimeDomainEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_TIME_DOMAIN_DEVICE_EXT"sv) return VK_TIME_DOMAIN_DEVICE_EXT;
    if (a_str == "VK_TIME_DOMAIN_CLOCK_MONOTONIC_EXT"sv) return VK_TIME_DOMAIN_CLOCK_MONOTONIC_EXT;
    if (a_str == "VK_TIME_DOMAIN_CLOCK_MONOTONIC_RAW_EXT"sv) return VK_TIME_DOMAIN_CLOCK_MONOTONIC_RAW_EXT;
    if (a_str == "VK_TIME_DOMAIN_QUERY_PERFORMANCE_COUNTER_EXT"sv) return VK_TIME_DOMAIN_QUERY_PERFORMANCE_COUNTER_EXT;
    if (a_str == "VK_TIME_DOMAIN_MAX_ENUM_EXT"sv) return VK_TIME_DOMAIN_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkTimeDomainEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkTimeDomainEXT(VkTimeDomainEXT e)
{
    switch (e) {
        case VK_TIME_DOMAIN_DEVICE_EXT: return "VK_TIME_DOMAIN_DEVICE_EXT";
        case VK_TIME_DOMAIN_CLOCK_MONOTONIC_EXT: return "VK_TIME_DOMAIN_CLOCK_MONOTONIC_EXT";
        case VK_TIME_DOMAIN_CLOCK_MONOTONIC_RAW_EXT: return "VK_TIME_DOMAIN_CLOCK_MONOTONIC_RAW_EXT";
        case VK_TIME_DOMAIN_QUERY_PERFORMANCE_COUNTER_EXT: return "VK_TIME_DOMAIN_QUERY_PERFORMANCE_COUNTER_EXT";
        case VK_TIME_DOMAIN_MAX_ENUM_EXT: return "VK_TIME_DOMAIN_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkTimeDomainEXT";
}

#endif
#if VK_AMD_memory_overallocation_behavior
VkMemoryOverallocationBehaviorAMD str_to_VkMemoryOverallocationBehaviorAMD(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_MEMORY_OVERALLOCATION_BEHAVIOR_DEFAULT_AMD"sv) return VK_MEMORY_OVERALLOCATION_BEHAVIOR_DEFAULT_AMD;
    if (a_str == "VK_MEMORY_OVERALLOCATION_BEHAVIOR_ALLOWED_AMD"sv) return VK_MEMORY_OVERALLOCATION_BEHAVIOR_ALLOWED_AMD;
    if (a_str == "VK_MEMORY_OVERALLOCATION_BEHAVIOR_DISALLOWED_AMD"sv) return VK_MEMORY_OVERALLOCATION_BEHAVIOR_DISALLOWED_AMD;
    if (a_str == "VK_MEMORY_OVERALLOCATION_BEHAVIOR_MAX_ENUM_AMD"sv) return VK_MEMORY_OVERALLOCATION_BEHAVIOR_MAX_ENUM_AMD;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkMemoryOverallocationBehaviorAMD>(0x7FFFFFFF);
}

std::string_view str_from_VkMemoryOverallocationBehaviorAMD(VkMemoryOverallocationBehaviorAMD e)
{
    switch (e) {
        case VK_MEMORY_OVERALLOCATION_BEHAVIOR_DEFAULT_AMD: return "VK_MEMORY_OVERALLOCATION_BEHAVIOR_DEFAULT_AMD";
        case VK_MEMORY_OVERALLOCATION_BEHAVIOR_ALLOWED_AMD: return "VK_MEMORY_OVERALLOCATION_BEHAVIOR_ALLOWED_AMD";
        case VK_MEMORY_OVERALLOCATION_BEHAVIOR_DISALLOWED_AMD: return "VK_MEMORY_OVERALLOCATION_BEHAVIOR_DISALLOWED_AMD";
        case VK_MEMORY_OVERALLOCATION_BEHAVIOR_MAX_ENUM_AMD: return "VK_MEMORY_OVERALLOCATION_BEHAVIOR_MAX_ENUM_AMD";
        default: break;
    }
    return "Unknown VkMemoryOverallocationBehaviorAMD";
}

#endif
#if VK_EXT_pipeline_creation_feedback
VkPipelineCreationFeedbackFlagBitsEXT str_to_VkPipelineCreationFeedbackFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PIPELINE_CREATION_FEEDBACK_VALID_BIT_EXT"sv) return VK_PIPELINE_CREATION_FEEDBACK_VALID_BIT_EXT;
    if (a_str == "VK_PIPELINE_CREATION_FEEDBACK_APPLICATION_PIPELINE_CACHE_HIT_BIT_EXT"sv) return VK_PIPELINE_CREATION_FEEDBACK_APPLICATION_PIPELINE_CACHE_HIT_BIT_EXT;
    if (a_str == "VK_PIPELINE_CREATION_FEEDBACK_BASE_PIPELINE_ACCELERATION_BIT_EXT"sv) return VK_PIPELINE_CREATION_FEEDBACK_BASE_PIPELINE_ACCELERATION_BIT_EXT;
    if (a_str == "VK_PIPELINE_CREATION_FEEDBACK_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_PIPELINE_CREATION_FEEDBACK_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPipelineCreationFeedbackFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkPipelineCreationFeedbackFlagBitsEXT(VkPipelineCreationFeedbackFlagBitsEXT e)
{
    switch (e) {
        case VK_PIPELINE_CREATION_FEEDBACK_VALID_BIT_EXT: return "VK_PIPELINE_CREATION_FEEDBACK_VALID_BIT_EXT";
        case VK_PIPELINE_CREATION_FEEDBACK_APPLICATION_PIPELINE_CACHE_HIT_BIT_EXT: return "VK_PIPELINE_CREATION_FEEDBACK_APPLICATION_PIPELINE_CACHE_HIT_BIT_EXT";
        case VK_PIPELINE_CREATION_FEEDBACK_BASE_PIPELINE_ACCELERATION_BIT_EXT: return "VK_PIPELINE_CREATION_FEEDBACK_BASE_PIPELINE_ACCELERATION_BIT_EXT";
        case VK_PIPELINE_CREATION_FEEDBACK_FLAG_BITS_MAX_ENUM_EXT: return "VK_PIPELINE_CREATION_FEEDBACK_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkPipelineCreationFeedbackFlagBitsEXT";
}

std::string str_from_VkPipelineCreationFeedbackFlagsEXT(VkPipelineCreationFeedbackFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPipelineCreationFeedbackFlagBitsEXT(static_cast<VkPipelineCreationFeedbackFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPipelineCreationFeedbackFlagBitsEXT(static_cast<VkPipelineCreationFeedbackFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_INTEL_performance_query
VkPerformanceConfigurationTypeINTEL str_to_VkPerformanceConfigurationTypeINTEL(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_CONFIGURATION_TYPE_COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL"sv) return VK_PERFORMANCE_CONFIGURATION_TYPE_COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL;
    if (a_str == "VK_PERFORMANCE_CONFIGURATION_TYPE_MAX_ENUM_INTEL"sv) return VK_PERFORMANCE_CONFIGURATION_TYPE_MAX_ENUM_INTEL;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceConfigurationTypeINTEL>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceConfigurationTypeINTEL(VkPerformanceConfigurationTypeINTEL e)
{
    switch (e) {
        case VK_PERFORMANCE_CONFIGURATION_TYPE_COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL: return "VK_PERFORMANCE_CONFIGURATION_TYPE_COMMAND_QUEUE_METRICS_DISCOVERY_ACTIVATED_INTEL";
        case VK_PERFORMANCE_CONFIGURATION_TYPE_MAX_ENUM_INTEL: return "VK_PERFORMANCE_CONFIGURATION_TYPE_MAX_ENUM_INTEL";
        default: break;
    }
    return "Unknown VkPerformanceConfigurationTypeINTEL";
}

VkQueryPoolSamplingModeINTEL str_to_VkQueryPoolSamplingModeINTEL(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_QUERY_POOL_SAMPLING_MODE_MANUAL_INTEL"sv) return VK_QUERY_POOL_SAMPLING_MODE_MANUAL_INTEL;
    if (a_str == "VK_QUERY_POOL_SAMPLING_MODE_MAX_ENUM_INTEL"sv) return VK_QUERY_POOL_SAMPLING_MODE_MAX_ENUM_INTEL;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkQueryPoolSamplingModeINTEL>(0x7FFFFFFF);
}

std::string_view str_from_VkQueryPoolSamplingModeINTEL(VkQueryPoolSamplingModeINTEL e)
{
    switch (e) {
        case VK_QUERY_POOL_SAMPLING_MODE_MANUAL_INTEL: return "VK_QUERY_POOL_SAMPLING_MODE_MANUAL_INTEL";
        case VK_QUERY_POOL_SAMPLING_MODE_MAX_ENUM_INTEL: return "VK_QUERY_POOL_SAMPLING_MODE_MAX_ENUM_INTEL";
        default: break;
    }
    return "Unknown VkQueryPoolSamplingModeINTEL";
}

VkPerformanceOverrideTypeINTEL str_to_VkPerformanceOverrideTypeINTEL(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_OVERRIDE_TYPE_NULL_HARDWARE_INTEL"sv) return VK_PERFORMANCE_OVERRIDE_TYPE_NULL_HARDWARE_INTEL;
    if (a_str == "VK_PERFORMANCE_OVERRIDE_TYPE_FLUSH_GPU_CACHES_INTEL"sv) return VK_PERFORMANCE_OVERRIDE_TYPE_FLUSH_GPU_CACHES_INTEL;
    if (a_str == "VK_PERFORMANCE_OVERRIDE_TYPE_MAX_ENUM_INTEL"sv) return VK_PERFORMANCE_OVERRIDE_TYPE_MAX_ENUM_INTEL;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceOverrideTypeINTEL>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceOverrideTypeINTEL(VkPerformanceOverrideTypeINTEL e)
{
    switch (e) {
        case VK_PERFORMANCE_OVERRIDE_TYPE_NULL_HARDWARE_INTEL: return "VK_PERFORMANCE_OVERRIDE_TYPE_NULL_HARDWARE_INTEL";
        case VK_PERFORMANCE_OVERRIDE_TYPE_FLUSH_GPU_CACHES_INTEL: return "VK_PERFORMANCE_OVERRIDE_TYPE_FLUSH_GPU_CACHES_INTEL";
        case VK_PERFORMANCE_OVERRIDE_TYPE_MAX_ENUM_INTEL: return "VK_PERFORMANCE_OVERRIDE_TYPE_MAX_ENUM_INTEL";
        default: break;
    }
    return "Unknown VkPerformanceOverrideTypeINTEL";
}

VkPerformanceParameterTypeINTEL str_to_VkPerformanceParameterTypeINTEL(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_PARAMETER_TYPE_HW_COUNTERS_SUPPORTED_INTEL"sv) return VK_PERFORMANCE_PARAMETER_TYPE_HW_COUNTERS_SUPPORTED_INTEL;
    if (a_str == "VK_PERFORMANCE_PARAMETER_TYPE_STREAM_MARKER_VALID_BITS_INTEL"sv) return VK_PERFORMANCE_PARAMETER_TYPE_STREAM_MARKER_VALID_BITS_INTEL;
    if (a_str == "VK_PERFORMANCE_PARAMETER_TYPE_MAX_ENUM_INTEL"sv) return VK_PERFORMANCE_PARAMETER_TYPE_MAX_ENUM_INTEL;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceParameterTypeINTEL>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceParameterTypeINTEL(VkPerformanceParameterTypeINTEL e)
{
    switch (e) {
        case VK_PERFORMANCE_PARAMETER_TYPE_HW_COUNTERS_SUPPORTED_INTEL: return "VK_PERFORMANCE_PARAMETER_TYPE_HW_COUNTERS_SUPPORTED_INTEL";
        case VK_PERFORMANCE_PARAMETER_TYPE_STREAM_MARKER_VALID_BITS_INTEL: return "VK_PERFORMANCE_PARAMETER_TYPE_STREAM_MARKER_VALID_BITS_INTEL";
        case VK_PERFORMANCE_PARAMETER_TYPE_MAX_ENUM_INTEL: return "VK_PERFORMANCE_PARAMETER_TYPE_MAX_ENUM_INTEL";
        default: break;
    }
    return "Unknown VkPerformanceParameterTypeINTEL";
}

VkPerformanceValueTypeINTEL str_to_VkPerformanceValueTypeINTEL(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PERFORMANCE_VALUE_TYPE_UINT32_INTEL"sv) return VK_PERFORMANCE_VALUE_TYPE_UINT32_INTEL;
    if (a_str == "VK_PERFORMANCE_VALUE_TYPE_UINT64_INTEL"sv) return VK_PERFORMANCE_VALUE_TYPE_UINT64_INTEL;
    if (a_str == "VK_PERFORMANCE_VALUE_TYPE_FLOAT_INTEL"sv) return VK_PERFORMANCE_VALUE_TYPE_FLOAT_INTEL;
    if (a_str == "VK_PERFORMANCE_VALUE_TYPE_BOOL_INTEL"sv) return VK_PERFORMANCE_VALUE_TYPE_BOOL_INTEL;
    if (a_str == "VK_PERFORMANCE_VALUE_TYPE_STRING_INTEL"sv) return VK_PERFORMANCE_VALUE_TYPE_STRING_INTEL;
    if (a_str == "VK_PERFORMANCE_VALUE_TYPE_MAX_ENUM_INTEL"sv) return VK_PERFORMANCE_VALUE_TYPE_MAX_ENUM_INTEL;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPerformanceValueTypeINTEL>(0x7FFFFFFF);
}

std::string_view str_from_VkPerformanceValueTypeINTEL(VkPerformanceValueTypeINTEL e)
{
    switch (e) {
        case VK_PERFORMANCE_VALUE_TYPE_UINT32_INTEL: return "VK_PERFORMANCE_VALUE_TYPE_UINT32_INTEL";
        case VK_PERFORMANCE_VALUE_TYPE_UINT64_INTEL: return "VK_PERFORMANCE_VALUE_TYPE_UINT64_INTEL";
        case VK_PERFORMANCE_VALUE_TYPE_FLOAT_INTEL: return "VK_PERFORMANCE_VALUE_TYPE_FLOAT_INTEL";
        case VK_PERFORMANCE_VALUE_TYPE_BOOL_INTEL: return "VK_PERFORMANCE_VALUE_TYPE_BOOL_INTEL";
        case VK_PERFORMANCE_VALUE_TYPE_STRING_INTEL: return "VK_PERFORMANCE_VALUE_TYPE_STRING_INTEL";
        case VK_PERFORMANCE_VALUE_TYPE_MAX_ENUM_INTEL: return "VK_PERFORMANCE_VALUE_TYPE_MAX_ENUM_INTEL";
        default: break;
    }
    return "Unknown VkPerformanceValueTypeINTEL";
}

#endif
#if VK_AMD_shader_core_properties2
VkShaderCorePropertiesFlagBitsAMD str_to_VkShaderCorePropertiesFlagBitsAMD(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHADER_CORE_PROPERTIES_FLAG_BITS_MAX_ENUM_AMD"sv) return VK_SHADER_CORE_PROPERTIES_FLAG_BITS_MAX_ENUM_AMD;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkShaderCorePropertiesFlagBitsAMD>(0x7FFFFFFF);
}

std::string_view str_from_VkShaderCorePropertiesFlagBitsAMD(VkShaderCorePropertiesFlagBitsAMD e)
{
    switch (e) {
        case VK_SHADER_CORE_PROPERTIES_FLAG_BITS_MAX_ENUM_AMD: return "VK_SHADER_CORE_PROPERTIES_FLAG_BITS_MAX_ENUM_AMD";
        default: break;
    }
    return "Unknown VkShaderCorePropertiesFlagBitsAMD";
}

std::string str_from_VkShaderCorePropertiesFlagsAMD(VkShaderCorePropertiesFlagsAMD f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkShaderCorePropertiesFlagBitsAMD(static_cast<VkShaderCorePropertiesFlagBitsAMD>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkShaderCorePropertiesFlagBitsAMD(static_cast<VkShaderCorePropertiesFlagBitsAMD>(0)));
    return result;
}

 #endif
#if VK_EXT_tooling_info
VkToolPurposeFlagBitsEXT str_to_VkToolPurposeFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_TOOL_PURPOSE_VALIDATION_BIT_EXT"sv) return VK_TOOL_PURPOSE_VALIDATION_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_PROFILING_BIT_EXT"sv) return VK_TOOL_PURPOSE_PROFILING_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_TRACING_BIT_EXT"sv) return VK_TOOL_PURPOSE_TRACING_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_ADDITIONAL_FEATURES_BIT_EXT"sv) return VK_TOOL_PURPOSE_ADDITIONAL_FEATURES_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_MODIFYING_FEATURES_BIT_EXT"sv) return VK_TOOL_PURPOSE_MODIFYING_FEATURES_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_DEBUG_REPORTING_BIT_EXT"sv) return VK_TOOL_PURPOSE_DEBUG_REPORTING_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_DEBUG_MARKERS_BIT_EXT"sv) return VK_TOOL_PURPOSE_DEBUG_MARKERS_BIT_EXT;
    if (a_str == "VK_TOOL_PURPOSE_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_TOOL_PURPOSE_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkToolPurposeFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkToolPurposeFlagBitsEXT(VkToolPurposeFlagBitsEXT e)
{
    switch (e) {
        case VK_TOOL_PURPOSE_VALIDATION_BIT_EXT: return "VK_TOOL_PURPOSE_VALIDATION_BIT_EXT";
        case VK_TOOL_PURPOSE_PROFILING_BIT_EXT: return "VK_TOOL_PURPOSE_PROFILING_BIT_EXT";
        case VK_TOOL_PURPOSE_TRACING_BIT_EXT: return "VK_TOOL_PURPOSE_TRACING_BIT_EXT";
        case VK_TOOL_PURPOSE_ADDITIONAL_FEATURES_BIT_EXT: return "VK_TOOL_PURPOSE_ADDITIONAL_FEATURES_BIT_EXT";
        case VK_TOOL_PURPOSE_MODIFYING_FEATURES_BIT_EXT: return "VK_TOOL_PURPOSE_MODIFYING_FEATURES_BIT_EXT";
        case VK_TOOL_PURPOSE_DEBUG_REPORTING_BIT_EXT: return "VK_TOOL_PURPOSE_DEBUG_REPORTING_BIT_EXT";
        case VK_TOOL_PURPOSE_DEBUG_MARKERS_BIT_EXT: return "VK_TOOL_PURPOSE_DEBUG_MARKERS_BIT_EXT";
        case VK_TOOL_PURPOSE_FLAG_BITS_MAX_ENUM_EXT: return "VK_TOOL_PURPOSE_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkToolPurposeFlagBitsEXT";
}

std::string str_from_VkToolPurposeFlagsEXT(VkToolPurposeFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkToolPurposeFlagBitsEXT(static_cast<VkToolPurposeFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkToolPurposeFlagBitsEXT(static_cast<VkToolPurposeFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_EXT_validation_features
VkValidationFeatureEnableEXT str_to_VkValidationFeatureEnableEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT"sv) return VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT"sv) return VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT"sv) return VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT"sv) return VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT"sv) return VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_ENABLE_MAX_ENUM_EXT"sv) return VK_VALIDATION_FEATURE_ENABLE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkValidationFeatureEnableEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkValidationFeatureEnableEXT(VkValidationFeatureEnableEXT e)
{
    switch (e) {
        case VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT: return "VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT";
        case VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT: return "VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT";
        case VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT: return "VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT";
        case VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT: return "VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT";
        case VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT: return "VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT";
        case VK_VALIDATION_FEATURE_ENABLE_MAX_ENUM_EXT: return "VK_VALIDATION_FEATURE_ENABLE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkValidationFeatureEnableEXT";
}

VkValidationFeatureDisableEXT str_to_VkValidationFeatureDisableEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_ALL_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_ALL_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_SHADERS_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_SHADERS_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_SHADER_VALIDATION_CACHE_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_SHADER_VALIDATION_CACHE_EXT;
    if (a_str == "VK_VALIDATION_FEATURE_DISABLE_MAX_ENUM_EXT"sv) return VK_VALIDATION_FEATURE_DISABLE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkValidationFeatureDisableEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkValidationFeatureDisableEXT(VkValidationFeatureDisableEXT e)
{
    switch (e) {
        case VK_VALIDATION_FEATURE_DISABLE_ALL_EXT: return "VK_VALIDATION_FEATURE_DISABLE_ALL_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_SHADERS_EXT: return "VK_VALIDATION_FEATURE_DISABLE_SHADERS_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT: return "VK_VALIDATION_FEATURE_DISABLE_THREAD_SAFETY_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT: return "VK_VALIDATION_FEATURE_DISABLE_API_PARAMETERS_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT: return "VK_VALIDATION_FEATURE_DISABLE_OBJECT_LIFETIMES_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT: return "VK_VALIDATION_FEATURE_DISABLE_CORE_CHECKS_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT: return "VK_VALIDATION_FEATURE_DISABLE_UNIQUE_HANDLES_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_SHADER_VALIDATION_CACHE_EXT: return "VK_VALIDATION_FEATURE_DISABLE_SHADER_VALIDATION_CACHE_EXT";
        case VK_VALIDATION_FEATURE_DISABLE_MAX_ENUM_EXT: return "VK_VALIDATION_FEATURE_DISABLE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkValidationFeatureDisableEXT";
}

#endif
#if VK_NV_cooperative_matrix
VkComponentTypeNV str_to_VkComponentTypeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COMPONENT_TYPE_FLOAT16_NV"sv) return VK_COMPONENT_TYPE_FLOAT16_NV;
    if (a_str == "VK_COMPONENT_TYPE_FLOAT32_NV"sv) return VK_COMPONENT_TYPE_FLOAT32_NV;
    if (a_str == "VK_COMPONENT_TYPE_FLOAT64_NV"sv) return VK_COMPONENT_TYPE_FLOAT64_NV;
    if (a_str == "VK_COMPONENT_TYPE_SINT8_NV"sv) return VK_COMPONENT_TYPE_SINT8_NV;
    if (a_str == "VK_COMPONENT_TYPE_SINT16_NV"sv) return VK_COMPONENT_TYPE_SINT16_NV;
    if (a_str == "VK_COMPONENT_TYPE_SINT32_NV"sv) return VK_COMPONENT_TYPE_SINT32_NV;
    if (a_str == "VK_COMPONENT_TYPE_SINT64_NV"sv) return VK_COMPONENT_TYPE_SINT64_NV;
    if (a_str == "VK_COMPONENT_TYPE_UINT8_NV"sv) return VK_COMPONENT_TYPE_UINT8_NV;
    if (a_str == "VK_COMPONENT_TYPE_UINT16_NV"sv) return VK_COMPONENT_TYPE_UINT16_NV;
    if (a_str == "VK_COMPONENT_TYPE_UINT32_NV"sv) return VK_COMPONENT_TYPE_UINT32_NV;
    if (a_str == "VK_COMPONENT_TYPE_UINT64_NV"sv) return VK_COMPONENT_TYPE_UINT64_NV;
    if (a_str == "VK_COMPONENT_TYPE_MAX_ENUM_NV"sv) return VK_COMPONENT_TYPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkComponentTypeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkComponentTypeNV(VkComponentTypeNV e)
{
    switch (e) {
        case VK_COMPONENT_TYPE_FLOAT16_NV: return "VK_COMPONENT_TYPE_FLOAT16_NV";
        case VK_COMPONENT_TYPE_FLOAT32_NV: return "VK_COMPONENT_TYPE_FLOAT32_NV";
        case VK_COMPONENT_TYPE_FLOAT64_NV: return "VK_COMPONENT_TYPE_FLOAT64_NV";
        case VK_COMPONENT_TYPE_SINT8_NV: return "VK_COMPONENT_TYPE_SINT8_NV";
        case VK_COMPONENT_TYPE_SINT16_NV: return "VK_COMPONENT_TYPE_SINT16_NV";
        case VK_COMPONENT_TYPE_SINT32_NV: return "VK_COMPONENT_TYPE_SINT32_NV";
        case VK_COMPONENT_TYPE_SINT64_NV: return "VK_COMPONENT_TYPE_SINT64_NV";
        case VK_COMPONENT_TYPE_UINT8_NV: return "VK_COMPONENT_TYPE_UINT8_NV";
        case VK_COMPONENT_TYPE_UINT16_NV: return "VK_COMPONENT_TYPE_UINT16_NV";
        case VK_COMPONENT_TYPE_UINT32_NV: return "VK_COMPONENT_TYPE_UINT32_NV";
        case VK_COMPONENT_TYPE_UINT64_NV: return "VK_COMPONENT_TYPE_UINT64_NV";
        case VK_COMPONENT_TYPE_MAX_ENUM_NV: return "VK_COMPONENT_TYPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkComponentTypeNV";
}

VkScopeNV str_to_VkScopeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SCOPE_DEVICE_NV"sv) return VK_SCOPE_DEVICE_NV;
    if (a_str == "VK_SCOPE_WORKGROUP_NV"sv) return VK_SCOPE_WORKGROUP_NV;
    if (a_str == "VK_SCOPE_SUBGROUP_NV"sv) return VK_SCOPE_SUBGROUP_NV;
    if (a_str == "VK_SCOPE_QUEUE_FAMILY_NV"sv) return VK_SCOPE_QUEUE_FAMILY_NV;
    if (a_str == "VK_SCOPE_MAX_ENUM_NV"sv) return VK_SCOPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkScopeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkScopeNV(VkScopeNV e)
{
    switch (e) {
        case VK_SCOPE_DEVICE_NV: return "VK_SCOPE_DEVICE_NV";
        case VK_SCOPE_WORKGROUP_NV: return "VK_SCOPE_WORKGROUP_NV";
        case VK_SCOPE_SUBGROUP_NV: return "VK_SCOPE_SUBGROUP_NV";
        case VK_SCOPE_QUEUE_FAMILY_NV: return "VK_SCOPE_QUEUE_FAMILY_NV";
        case VK_SCOPE_MAX_ENUM_NV: return "VK_SCOPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkScopeNV";
}

#endif
#if VK_NV_coverage_reduction_mode
VkCoverageReductionModeNV str_to_VkCoverageReductionModeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_COVERAGE_REDUCTION_MODE_MERGE_NV"sv) return VK_COVERAGE_REDUCTION_MODE_MERGE_NV;
    if (a_str == "VK_COVERAGE_REDUCTION_MODE_TRUNCATE_NV"sv) return VK_COVERAGE_REDUCTION_MODE_TRUNCATE_NV;
    if (a_str == "VK_COVERAGE_REDUCTION_MODE_MAX_ENUM_NV"sv) return VK_COVERAGE_REDUCTION_MODE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkCoverageReductionModeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkCoverageReductionModeNV(VkCoverageReductionModeNV e)
{
    switch (e) {
        case VK_COVERAGE_REDUCTION_MODE_MERGE_NV: return "VK_COVERAGE_REDUCTION_MODE_MERGE_NV";
        case VK_COVERAGE_REDUCTION_MODE_TRUNCATE_NV: return "VK_COVERAGE_REDUCTION_MODE_TRUNCATE_NV";
        case VK_COVERAGE_REDUCTION_MODE_MAX_ENUM_NV: return "VK_COVERAGE_REDUCTION_MODE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkCoverageReductionModeNV";
}

#endif
#if VK_EXT_provoking_vertex
VkProvokingVertexModeEXT str_to_VkProvokingVertexModeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PROVOKING_VERTEX_MODE_FIRST_VERTEX_EXT"sv) return VK_PROVOKING_VERTEX_MODE_FIRST_VERTEX_EXT;
    if (a_str == "VK_PROVOKING_VERTEX_MODE_LAST_VERTEX_EXT"sv) return VK_PROVOKING_VERTEX_MODE_LAST_VERTEX_EXT;
    if (a_str == "VK_PROVOKING_VERTEX_MODE_MAX_ENUM_EXT"sv) return VK_PROVOKING_VERTEX_MODE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkProvokingVertexModeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkProvokingVertexModeEXT(VkProvokingVertexModeEXT e)
{
    switch (e) {
        case VK_PROVOKING_VERTEX_MODE_FIRST_VERTEX_EXT: return "VK_PROVOKING_VERTEX_MODE_FIRST_VERTEX_EXT";
        case VK_PROVOKING_VERTEX_MODE_LAST_VERTEX_EXT: return "VK_PROVOKING_VERTEX_MODE_LAST_VERTEX_EXT";
        case VK_PROVOKING_VERTEX_MODE_MAX_ENUM_EXT: return "VK_PROVOKING_VERTEX_MODE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkProvokingVertexModeEXT";
}

#endif
#if VK_EXT_line_rasterization
VkLineRasterizationModeEXT str_to_VkLineRasterizationModeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_LINE_RASTERIZATION_MODE_DEFAULT_EXT"sv) return VK_LINE_RASTERIZATION_MODE_DEFAULT_EXT;
    if (a_str == "VK_LINE_RASTERIZATION_MODE_RECTANGULAR_EXT"sv) return VK_LINE_RASTERIZATION_MODE_RECTANGULAR_EXT;
    if (a_str == "VK_LINE_RASTERIZATION_MODE_BRESENHAM_EXT"sv) return VK_LINE_RASTERIZATION_MODE_BRESENHAM_EXT;
    if (a_str == "VK_LINE_RASTERIZATION_MODE_RECTANGULAR_SMOOTH_EXT"sv) return VK_LINE_RASTERIZATION_MODE_RECTANGULAR_SMOOTH_EXT;
    if (a_str == "VK_LINE_RASTERIZATION_MODE_MAX_ENUM_EXT"sv) return VK_LINE_RASTERIZATION_MODE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkLineRasterizationModeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkLineRasterizationModeEXT(VkLineRasterizationModeEXT e)
{
    switch (e) {
        case VK_LINE_RASTERIZATION_MODE_DEFAULT_EXT: return "VK_LINE_RASTERIZATION_MODE_DEFAULT_EXT";
        case VK_LINE_RASTERIZATION_MODE_RECTANGULAR_EXT: return "VK_LINE_RASTERIZATION_MODE_RECTANGULAR_EXT";
        case VK_LINE_RASTERIZATION_MODE_BRESENHAM_EXT: return "VK_LINE_RASTERIZATION_MODE_BRESENHAM_EXT";
        case VK_LINE_RASTERIZATION_MODE_RECTANGULAR_SMOOTH_EXT: return "VK_LINE_RASTERIZATION_MODE_RECTANGULAR_SMOOTH_EXT";
        case VK_LINE_RASTERIZATION_MODE_MAX_ENUM_EXT: return "VK_LINE_RASTERIZATION_MODE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkLineRasterizationModeEXT";
}

#endif
#if VK_NV_device_generated_commands
VkIndirectCommandsTokenTypeNV str_to_VkIndirectCommandsTokenTypeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_SHADER_GROUP_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_SHADER_GROUP_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_STATE_FLAGS_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_STATE_FLAGS_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_INDEX_BUFFER_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_INDEX_BUFFER_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_VERTEX_BUFFER_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_VERTEX_BUFFER_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_PUSH_CONSTANT_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_PUSH_CONSTANT_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_INDEXED_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_INDEXED_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_TASKS_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_TASKS_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_TOKEN_TYPE_MAX_ENUM_NV"sv) return VK_INDIRECT_COMMANDS_TOKEN_TYPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkIndirectCommandsTokenTypeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkIndirectCommandsTokenTypeNV(VkIndirectCommandsTokenTypeNV e)
{
    switch (e) {
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_SHADER_GROUP_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_SHADER_GROUP_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_STATE_FLAGS_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_STATE_FLAGS_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_INDEX_BUFFER_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_INDEX_BUFFER_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_VERTEX_BUFFER_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_VERTEX_BUFFER_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_PUSH_CONSTANT_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_PUSH_CONSTANT_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_INDEXED_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_INDEXED_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_TASKS_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_DRAW_TASKS_NV";
        case VK_INDIRECT_COMMANDS_TOKEN_TYPE_MAX_ENUM_NV: return "VK_INDIRECT_COMMANDS_TOKEN_TYPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkIndirectCommandsTokenTypeNV";
}

VkIndirectStateFlagBitsNV str_to_VkIndirectStateFlagBitsNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_INDIRECT_STATE_FLAG_FRONTFACE_BIT_NV"sv) return VK_INDIRECT_STATE_FLAG_FRONTFACE_BIT_NV;
    if (a_str == "VK_INDIRECT_STATE_FLAG_BITS_MAX_ENUM_NV"sv) return VK_INDIRECT_STATE_FLAG_BITS_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkIndirectStateFlagBitsNV>(0x7FFFFFFF);
}

std::string_view str_from_VkIndirectStateFlagBitsNV(VkIndirectStateFlagBitsNV e)
{
    switch (e) {
        case VK_INDIRECT_STATE_FLAG_FRONTFACE_BIT_NV: return "VK_INDIRECT_STATE_FLAG_FRONTFACE_BIT_NV";
        case VK_INDIRECT_STATE_FLAG_BITS_MAX_ENUM_NV: return "VK_INDIRECT_STATE_FLAG_BITS_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkIndirectStateFlagBitsNV";
}

std::string str_from_VkIndirectStateFlagsNV(VkIndirectStateFlagsNV f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkIndirectStateFlagBitsNV(static_cast<VkIndirectStateFlagBitsNV>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkIndirectStateFlagBitsNV(static_cast<VkIndirectStateFlagBitsNV>(0)));
    return result;
}

 VkIndirectCommandsLayoutUsageFlagBitsNV str_to_VkIndirectCommandsLayoutUsageFlagBitsNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_EXPLICIT_PREPROCESS_BIT_NV"sv) return VK_INDIRECT_COMMANDS_LAYOUT_USAGE_EXPLICIT_PREPROCESS_BIT_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV"sv) return VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV"sv) return VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV;
    if (a_str == "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_FLAG_BITS_MAX_ENUM_NV"sv) return VK_INDIRECT_COMMANDS_LAYOUT_USAGE_FLAG_BITS_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkIndirectCommandsLayoutUsageFlagBitsNV>(0x7FFFFFFF);
}

std::string_view str_from_VkIndirectCommandsLayoutUsageFlagBitsNV(VkIndirectCommandsLayoutUsageFlagBitsNV e)
{
    switch (e) {
        case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_EXPLICIT_PREPROCESS_BIT_NV: return "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_EXPLICIT_PREPROCESS_BIT_NV";
        case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV: return "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_INDEXED_SEQUENCES_BIT_NV";
        case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV: return "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_UNORDERED_SEQUENCES_BIT_NV";
        case VK_INDIRECT_COMMANDS_LAYOUT_USAGE_FLAG_BITS_MAX_ENUM_NV: return "VK_INDIRECT_COMMANDS_LAYOUT_USAGE_FLAG_BITS_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkIndirectCommandsLayoutUsageFlagBitsNV";
}

std::string str_from_VkIndirectCommandsLayoutUsageFlagsNV(VkIndirectCommandsLayoutUsageFlagsNV f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkIndirectCommandsLayoutUsageFlagBitsNV(static_cast<VkIndirectCommandsLayoutUsageFlagBitsNV>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkIndirectCommandsLayoutUsageFlagBitsNV(static_cast<VkIndirectCommandsLayoutUsageFlagBitsNV>(0)));
    return result;
}

 #endif
#if VK_EXT_device_memory_report
VkDeviceMemoryReportEventTypeEXT str_to_VkDeviceMemoryReportEventTypeEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATE_EXT"sv) return VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATE_EXT;
    if (a_str == "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_FREE_EXT"sv) return VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_FREE_EXT;
    if (a_str == "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_IMPORT_EXT"sv) return VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_IMPORT_EXT;
    if (a_str == "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_UNIMPORT_EXT"sv) return VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_UNIMPORT_EXT;
    if (a_str == "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATION_FAILED_EXT"sv) return VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATION_FAILED_EXT;
    if (a_str == "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_MAX_ENUM_EXT"sv) return VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDeviceMemoryReportEventTypeEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkDeviceMemoryReportEventTypeEXT(VkDeviceMemoryReportEventTypeEXT e)
{
    switch (e) {
        case VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATE_EXT: return "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATE_EXT";
        case VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_FREE_EXT: return "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_FREE_EXT";
        case VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_IMPORT_EXT: return "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_IMPORT_EXT";
        case VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_UNIMPORT_EXT: return "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_UNIMPORT_EXT";
        case VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATION_FAILED_EXT: return "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_ALLOCATION_FAILED_EXT";
        case VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_MAX_ENUM_EXT: return "VK_DEVICE_MEMORY_REPORT_EVENT_TYPE_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkDeviceMemoryReportEventTypeEXT";
}

#endif
#if VK_EXT_private_data
VkPrivateDataSlotCreateFlagBitsEXT str_to_VkPrivateDataSlotCreateFlagBitsEXT(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_PRIVATE_DATA_SLOT_CREATE_FLAG_BITS_MAX_ENUM_EXT"sv) return VK_PRIVATE_DATA_SLOT_CREATE_FLAG_BITS_MAX_ENUM_EXT;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkPrivateDataSlotCreateFlagBitsEXT>(0x7FFFFFFF);
}

std::string_view str_from_VkPrivateDataSlotCreateFlagBitsEXT(VkPrivateDataSlotCreateFlagBitsEXT e)
{
    switch (e) {
        case VK_PRIVATE_DATA_SLOT_CREATE_FLAG_BITS_MAX_ENUM_EXT: return "VK_PRIVATE_DATA_SLOT_CREATE_FLAG_BITS_MAX_ENUM_EXT";
        default: break;
    }
    return "Unknown VkPrivateDataSlotCreateFlagBitsEXT";
}

std::string str_from_VkPrivateDataSlotCreateFlagsEXT(VkPrivateDataSlotCreateFlagsEXT f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkPrivateDataSlotCreateFlagBitsEXT(static_cast<VkPrivateDataSlotCreateFlagBitsEXT>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkPrivateDataSlotCreateFlagBitsEXT(static_cast<VkPrivateDataSlotCreateFlagBitsEXT>(0)));
    return result;
}

 #endif
#if VK_NV_device_diagnostics_config
VkDeviceDiagnosticsConfigFlagBitsNV str_to_VkDeviceDiagnosticsConfigFlagBitsNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_SHADER_DEBUG_INFO_BIT_NV"sv) return VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_SHADER_DEBUG_INFO_BIT_NV;
    if (a_str == "VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_RESOURCE_TRACKING_BIT_NV"sv) return VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_RESOURCE_TRACKING_BIT_NV;
    if (a_str == "VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_AUTOMATIC_CHECKPOINTS_BIT_NV"sv) return VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_AUTOMATIC_CHECKPOINTS_BIT_NV;
    if (a_str == "VK_DEVICE_DIAGNOSTICS_CONFIG_FLAG_BITS_MAX_ENUM_NV"sv) return VK_DEVICE_DIAGNOSTICS_CONFIG_FLAG_BITS_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkDeviceDiagnosticsConfigFlagBitsNV>(0x7FFFFFFF);
}

std::string_view str_from_VkDeviceDiagnosticsConfigFlagBitsNV(VkDeviceDiagnosticsConfigFlagBitsNV e)
{
    switch (e) {
        case VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_SHADER_DEBUG_INFO_BIT_NV: return "VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_SHADER_DEBUG_INFO_BIT_NV";
        case VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_RESOURCE_TRACKING_BIT_NV: return "VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_RESOURCE_TRACKING_BIT_NV";
        case VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_AUTOMATIC_CHECKPOINTS_BIT_NV: return "VK_DEVICE_DIAGNOSTICS_CONFIG_ENABLE_AUTOMATIC_CHECKPOINTS_BIT_NV";
        case VK_DEVICE_DIAGNOSTICS_CONFIG_FLAG_BITS_MAX_ENUM_NV: return "VK_DEVICE_DIAGNOSTICS_CONFIG_FLAG_BITS_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkDeviceDiagnosticsConfigFlagBitsNV";
}

std::string str_from_VkDeviceDiagnosticsConfigFlagsNV(VkDeviceDiagnosticsConfigFlagsNV f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkDeviceDiagnosticsConfigFlagBitsNV(static_cast<VkDeviceDiagnosticsConfigFlagBitsNV>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkDeviceDiagnosticsConfigFlagBitsNV(static_cast<VkDeviceDiagnosticsConfigFlagBitsNV>(0)));
    return result;
}

 #endif
#if VK_NV_fragment_shading_rate_enums
VkFragmentShadingRateTypeNV str_to_VkFragmentShadingRateTypeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_TYPE_FRAGMENT_SIZE_NV"sv) return VK_FRAGMENT_SHADING_RATE_TYPE_FRAGMENT_SIZE_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_TYPE_ENUMS_NV"sv) return VK_FRAGMENT_SHADING_RATE_TYPE_ENUMS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_TYPE_MAX_ENUM_NV"sv) return VK_FRAGMENT_SHADING_RATE_TYPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFragmentShadingRateTypeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkFragmentShadingRateTypeNV(VkFragmentShadingRateTypeNV e)
{
    switch (e) {
        case VK_FRAGMENT_SHADING_RATE_TYPE_FRAGMENT_SIZE_NV: return "VK_FRAGMENT_SHADING_RATE_TYPE_FRAGMENT_SIZE_NV";
        case VK_FRAGMENT_SHADING_RATE_TYPE_ENUMS_NV: return "VK_FRAGMENT_SHADING_RATE_TYPE_ENUMS_NV";
        case VK_FRAGMENT_SHADING_RATE_TYPE_MAX_ENUM_NV: return "VK_FRAGMENT_SHADING_RATE_TYPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkFragmentShadingRateTypeNV";
}

VkFragmentShadingRateNV str_to_VkFragmentShadingRateNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_PIXEL_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_PIXEL_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_1X2_PIXELS_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_1X2_PIXELS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X1_PIXELS_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X1_PIXELS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X2_PIXELS_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X2_PIXELS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X4_PIXELS_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X4_PIXELS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X2_PIXELS_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X2_PIXELS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X4_PIXELS_NV"sv) return VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X4_PIXELS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_2_INVOCATIONS_PER_PIXEL_NV"sv) return VK_FRAGMENT_SHADING_RATE_2_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_4_INVOCATIONS_PER_PIXEL_NV"sv) return VK_FRAGMENT_SHADING_RATE_4_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_8_INVOCATIONS_PER_PIXEL_NV"sv) return VK_FRAGMENT_SHADING_RATE_8_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_16_INVOCATIONS_PER_PIXEL_NV"sv) return VK_FRAGMENT_SHADING_RATE_16_INVOCATIONS_PER_PIXEL_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_NO_INVOCATIONS_NV"sv) return VK_FRAGMENT_SHADING_RATE_NO_INVOCATIONS_NV;
    if (a_str == "VK_FRAGMENT_SHADING_RATE_MAX_ENUM_NV"sv) return VK_FRAGMENT_SHADING_RATE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkFragmentShadingRateNV>(0x7FFFFFFF);
}

std::string_view str_from_VkFragmentShadingRateNV(VkFragmentShadingRateNV e)
{
    switch (e) {
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_PIXEL_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_PIXEL_NV";
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_1X2_PIXELS_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_1X2_PIXELS_NV";
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X1_PIXELS_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X1_PIXELS_NV";
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X2_PIXELS_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X2_PIXELS_NV";
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X4_PIXELS_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_2X4_PIXELS_NV";
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X2_PIXELS_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X2_PIXELS_NV";
        case VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X4_PIXELS_NV: return "VK_FRAGMENT_SHADING_RATE_1_INVOCATION_PER_4X4_PIXELS_NV";
        case VK_FRAGMENT_SHADING_RATE_2_INVOCATIONS_PER_PIXEL_NV: return "VK_FRAGMENT_SHADING_RATE_2_INVOCATIONS_PER_PIXEL_NV";
        case VK_FRAGMENT_SHADING_RATE_4_INVOCATIONS_PER_PIXEL_NV: return "VK_FRAGMENT_SHADING_RATE_4_INVOCATIONS_PER_PIXEL_NV";
        case VK_FRAGMENT_SHADING_RATE_8_INVOCATIONS_PER_PIXEL_NV: return "VK_FRAGMENT_SHADING_RATE_8_INVOCATIONS_PER_PIXEL_NV";
        case VK_FRAGMENT_SHADING_RATE_16_INVOCATIONS_PER_PIXEL_NV: return "VK_FRAGMENT_SHADING_RATE_16_INVOCATIONS_PER_PIXEL_NV";
        case VK_FRAGMENT_SHADING_RATE_NO_INVOCATIONS_NV: return "VK_FRAGMENT_SHADING_RATE_NO_INVOCATIONS_NV";
        case VK_FRAGMENT_SHADING_RATE_MAX_ENUM_NV: return "VK_FRAGMENT_SHADING_RATE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkFragmentShadingRateNV";
}

#endif
#if VK_NV_ray_tracing_motion_blur
VkAccelerationStructureMotionInstanceTypeNV str_to_VkAccelerationStructureMotionInstanceTypeNV(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_STATIC_NV"sv) return VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_STATIC_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MATRIX_MOTION_NV"sv) return VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MATRIX_MOTION_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_SRT_MOTION_NV"sv) return VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_SRT_MOTION_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MAX_ENUM_NV"sv) return VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MAX_ENUM_NV;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccelerationStructureMotionInstanceTypeNV>(0x7FFFFFFF);
}

std::string_view str_from_VkAccelerationStructureMotionInstanceTypeNV(VkAccelerationStructureMotionInstanceTypeNV e)
{
    switch (e) {
        case VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_STATIC_NV: return "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_STATIC_NV";
        case VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MATRIX_MOTION_NV: return "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MATRIX_MOTION_NV";
        case VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_SRT_MOTION_NV: return "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_SRT_MOTION_NV";
        case VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MAX_ENUM_NV: return "VK_ACCELERATION_STRUCTURE_MOTION_INSTANCE_TYPE_MAX_ENUM_NV";
        default: break;
    }
    return "Unknown VkAccelerationStructureMotionInstanceTypeNV";
}

#endif
#if VK_KHR_acceleration_structure
VkBuildAccelerationStructureModeKHR str_to_VkBuildAccelerationStructureModeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
    if (a_str == "VK_BUILD_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR"sv) return VK_BUILD_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkBuildAccelerationStructureModeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkBuildAccelerationStructureModeKHR(VkBuildAccelerationStructureModeKHR e)
{
    switch (e) {
        case VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR";
        case VK_BUILD_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR: return "VK_BUILD_ACCELERATION_STRUCTURE_MODE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkBuildAccelerationStructureModeKHR";
}

VkAccelerationStructureBuildTypeKHR str_to_VkAccelerationStructureBuildTypeKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_KHR"sv) return VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR"sv) return VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_OR_DEVICE_KHR"sv) return VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_OR_DEVICE_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_MAX_ENUM_KHR"sv) return VK_ACCELERATION_STRUCTURE_BUILD_TYPE_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccelerationStructureBuildTypeKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkAccelerationStructureBuildTypeKHR(VkAccelerationStructureBuildTypeKHR e)
{
    switch (e) {
        case VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_KHR: return "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_KHR";
        case VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR: return "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR";
        case VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_OR_DEVICE_KHR: return "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_HOST_OR_DEVICE_KHR";
        case VK_ACCELERATION_STRUCTURE_BUILD_TYPE_MAX_ENUM_KHR: return "VK_ACCELERATION_STRUCTURE_BUILD_TYPE_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkAccelerationStructureBuildTypeKHR";
}

VkAccelerationStructureCompatibilityKHR str_to_VkAccelerationStructureCompatibilityKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCELERATION_STRUCTURE_COMPATIBILITY_COMPATIBLE_KHR"sv) return VK_ACCELERATION_STRUCTURE_COMPATIBILITY_COMPATIBLE_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_COMPATIBILITY_INCOMPATIBLE_KHR"sv) return VK_ACCELERATION_STRUCTURE_COMPATIBILITY_INCOMPATIBLE_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_COMPATIBILITY_MAX_ENUM_KHR"sv) return VK_ACCELERATION_STRUCTURE_COMPATIBILITY_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccelerationStructureCompatibilityKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkAccelerationStructureCompatibilityKHR(VkAccelerationStructureCompatibilityKHR e)
{
    switch (e) {
        case VK_ACCELERATION_STRUCTURE_COMPATIBILITY_COMPATIBLE_KHR: return "VK_ACCELERATION_STRUCTURE_COMPATIBILITY_COMPATIBLE_KHR";
        case VK_ACCELERATION_STRUCTURE_COMPATIBILITY_INCOMPATIBLE_KHR: return "VK_ACCELERATION_STRUCTURE_COMPATIBILITY_INCOMPATIBLE_KHR";
        case VK_ACCELERATION_STRUCTURE_COMPATIBILITY_MAX_ENUM_KHR: return "VK_ACCELERATION_STRUCTURE_COMPATIBILITY_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkAccelerationStructureCompatibilityKHR";
}

VkAccelerationStructureCreateFlagBitsKHR str_to_VkAccelerationStructureCreateFlagBitsKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR"sv) return VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR;
    if (a_str == "VK_ACCELERATION_STRUCTURE_CREATE_MOTION_BIT_NV"sv) return VK_ACCELERATION_STRUCTURE_CREATE_MOTION_BIT_NV;
    if (a_str == "VK_ACCELERATION_STRUCTURE_CREATE_FLAG_BITS_MAX_ENUM_KHR"sv) return VK_ACCELERATION_STRUCTURE_CREATE_FLAG_BITS_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkAccelerationStructureCreateFlagBitsKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkAccelerationStructureCreateFlagBitsKHR(VkAccelerationStructureCreateFlagBitsKHR e)
{
    switch (e) {
        case VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR: return "VK_ACCELERATION_STRUCTURE_CREATE_DEVICE_ADDRESS_CAPTURE_REPLAY_BIT_KHR";
        case VK_ACCELERATION_STRUCTURE_CREATE_MOTION_BIT_NV: return "VK_ACCELERATION_STRUCTURE_CREATE_MOTION_BIT_NV";
        case VK_ACCELERATION_STRUCTURE_CREATE_FLAG_BITS_MAX_ENUM_KHR: return "VK_ACCELERATION_STRUCTURE_CREATE_FLAG_BITS_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkAccelerationStructureCreateFlagBitsKHR";
}

std::string str_from_VkAccelerationStructureCreateFlagsKHR(VkAccelerationStructureCreateFlagsKHR f){
    std::string result;
    int index = 0;
    while(f) {
        if (f & 1) {
            if( !result.empty()) result.append(" | ");
            result.append(str_from_VkAccelerationStructureCreateFlagBitsKHR(static_cast<VkAccelerationStructureCreateFlagBitsKHR>(1U << index)));
        }
        ++index;
        f >>= 1;
    }
    if( result.empty()) result.append(str_from_VkAccelerationStructureCreateFlagBitsKHR(static_cast<VkAccelerationStructureCreateFlagBitsKHR>(0)));
    return result;
}

 #endif
#if VK_KHR_ray_tracing_pipeline
VkShaderGroupShaderKHR str_to_VkShaderGroupShaderKHR(std::string_view a_str, bool* a_ok_ptr)
{
    using namespace std::literals;
    if (a_ok_ptr) *a_ok_ptr = true;
    if (a_str == "VK_SHADER_GROUP_SHADER_GENERAL_KHR"sv) return VK_SHADER_GROUP_SHADER_GENERAL_KHR;
    if (a_str == "VK_SHADER_GROUP_SHADER_CLOSEST_HIT_KHR"sv) return VK_SHADER_GROUP_SHADER_CLOSEST_HIT_KHR;
    if (a_str == "VK_SHADER_GROUP_SHADER_ANY_HIT_KHR"sv) return VK_SHADER_GROUP_SHADER_ANY_HIT_KHR;
    if (a_str == "VK_SHADER_GROUP_SHADER_INTERSECTION_KHR"sv) return VK_SHADER_GROUP_SHADER_INTERSECTION_KHR;
    if (a_str == "VK_SHADER_GROUP_SHADER_MAX_ENUM_KHR"sv) return VK_SHADER_GROUP_SHADER_MAX_ENUM_KHR;
    if (a_ok_ptr) *a_ok_ptr = false;
    return static_cast<VkShaderGroupShaderKHR>(0x7FFFFFFF);
}

std::string_view str_from_VkShaderGroupShaderKHR(VkShaderGroupShaderKHR e)
{
    switch (e) {
        case VK_SHADER_GROUP_SHADER_GENERAL_KHR: return "VK_SHADER_GROUP_SHADER_GENERAL_KHR";
        case VK_SHADER_GROUP_SHADER_CLOSEST_HIT_KHR: return "VK_SHADER_GROUP_SHADER_CLOSEST_HIT_KHR";
        case VK_SHADER_GROUP_SHADER_ANY_HIT_KHR: return "VK_SHADER_GROUP_SHADER_ANY_HIT_KHR";
        case VK_SHADER_GROUP_SHADER_INTERSECTION_KHR: return "VK_SHADER_GROUP_SHADER_INTERSECTION_KHR";
        case VK_SHADER_GROUP_SHADER_MAX_ENUM_KHR: return "VK_SHADER_GROUP_SHADER_MAX_ENUM_KHR";
        default: break;
    }
    return "Unknown VkShaderGroupShaderKHR";
}

#endif
#if VK_VERSION_1_0
VkBufferMemoryBarrier make_VkBufferMemoryBarrier() { return VkBufferMemoryBarrier { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER }; }
VkImageMemoryBarrier make_VkImageMemoryBarrier() { return VkImageMemoryBarrier { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER }; }
VkMemoryBarrier make_VkMemoryBarrier() { return VkMemoryBarrier { VK_STRUCTURE_TYPE_MEMORY_BARRIER }; }
VkApplicationInfo make_VkApplicationInfo() { return VkApplicationInfo { VK_STRUCTURE_TYPE_APPLICATION_INFO }; }
VkInstanceCreateInfo make_VkInstanceCreateInfo() { return VkInstanceCreateInfo { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO }; }
VkDeviceQueueCreateInfo make_VkDeviceQueueCreateInfo() { return VkDeviceQueueCreateInfo { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO }; }
VkDeviceCreateInfo make_VkDeviceCreateInfo() { return VkDeviceCreateInfo { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO }; }
VkSubmitInfo make_VkSubmitInfo() { return VkSubmitInfo { VK_STRUCTURE_TYPE_SUBMIT_INFO }; }
VkMappedMemoryRange make_VkMappedMemoryRange() { return VkMappedMemoryRange { VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE }; }
VkMemoryAllocateInfo make_VkMemoryAllocateInfo() { return VkMemoryAllocateInfo { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO }; }
VkBindSparseInfo make_VkBindSparseInfo() { return VkBindSparseInfo { VK_STRUCTURE_TYPE_BIND_SPARSE_INFO }; }
VkFenceCreateInfo make_VkFenceCreateInfo() { return VkFenceCreateInfo { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO }; }
VkSemaphoreCreateInfo make_VkSemaphoreCreateInfo() { return VkSemaphoreCreateInfo { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO }; }
VkEventCreateInfo make_VkEventCreateInfo() { return VkEventCreateInfo { VK_STRUCTURE_TYPE_EVENT_CREATE_INFO }; }
VkQueryPoolCreateInfo make_VkQueryPoolCreateInfo() { return VkQueryPoolCreateInfo { VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO }; }
VkBufferCreateInfo make_VkBufferCreateInfo() { return VkBufferCreateInfo { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO }; }
VkBufferViewCreateInfo make_VkBufferViewCreateInfo() { return VkBufferViewCreateInfo { VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO }; }
VkImageCreateInfo make_VkImageCreateInfo() { return VkImageCreateInfo { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO }; }
VkImageViewCreateInfo make_VkImageViewCreateInfo() { return VkImageViewCreateInfo { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO }; }
VkShaderModuleCreateInfo make_VkShaderModuleCreateInfo() { return VkShaderModuleCreateInfo { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO }; }
VkPipelineCacheCreateInfo make_VkPipelineCacheCreateInfo() { return VkPipelineCacheCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO }; }
VkPipelineShaderStageCreateInfo make_VkPipelineShaderStageCreateInfo() { return VkPipelineShaderStageCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO }; }
VkComputePipelineCreateInfo make_VkComputePipelineCreateInfo() { return VkComputePipelineCreateInfo { VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO }; }
VkPipelineVertexInputStateCreateInfo make_VkPipelineVertexInputStateCreateInfo() { return VkPipelineVertexInputStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO }; }
VkPipelineInputAssemblyStateCreateInfo make_VkPipelineInputAssemblyStateCreateInfo() { return VkPipelineInputAssemblyStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO }; }
VkPipelineTessellationStateCreateInfo make_VkPipelineTessellationStateCreateInfo() { return VkPipelineTessellationStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO }; }
VkPipelineViewportStateCreateInfo make_VkPipelineViewportStateCreateInfo() { return VkPipelineViewportStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO }; }
VkPipelineRasterizationStateCreateInfo make_VkPipelineRasterizationStateCreateInfo() { return VkPipelineRasterizationStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO }; }
VkPipelineMultisampleStateCreateInfo make_VkPipelineMultisampleStateCreateInfo() { return VkPipelineMultisampleStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO }; }
VkPipelineDepthStencilStateCreateInfo make_VkPipelineDepthStencilStateCreateInfo() { return VkPipelineDepthStencilStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO }; }
VkPipelineColorBlendStateCreateInfo make_VkPipelineColorBlendStateCreateInfo() { return VkPipelineColorBlendStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO }; }
VkPipelineDynamicStateCreateInfo make_VkPipelineDynamicStateCreateInfo() { return VkPipelineDynamicStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO }; }
VkGraphicsPipelineCreateInfo make_VkGraphicsPipelineCreateInfo() { return VkGraphicsPipelineCreateInfo { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO }; }
VkPipelineLayoutCreateInfo make_VkPipelineLayoutCreateInfo() { return VkPipelineLayoutCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO }; }
VkSamplerCreateInfo make_VkSamplerCreateInfo() { return VkSamplerCreateInfo { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO }; }
VkCopyDescriptorSet make_VkCopyDescriptorSet() { return VkCopyDescriptorSet { VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET }; }
VkDescriptorPoolCreateInfo make_VkDescriptorPoolCreateInfo() { return VkDescriptorPoolCreateInfo { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO }; }
VkDescriptorSetAllocateInfo make_VkDescriptorSetAllocateInfo() { return VkDescriptorSetAllocateInfo { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO }; }
VkDescriptorSetLayoutCreateInfo make_VkDescriptorSetLayoutCreateInfo() { return VkDescriptorSetLayoutCreateInfo { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO }; }
VkWriteDescriptorSet make_VkWriteDescriptorSet() { return VkWriteDescriptorSet { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET }; }
VkFramebufferCreateInfo make_VkFramebufferCreateInfo() { return VkFramebufferCreateInfo { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO }; }
VkRenderPassCreateInfo make_VkRenderPassCreateInfo() { return VkRenderPassCreateInfo { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO }; }
VkCommandPoolCreateInfo make_VkCommandPoolCreateInfo() { return VkCommandPoolCreateInfo { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO }; }
VkCommandBufferAllocateInfo make_VkCommandBufferAllocateInfo() { return VkCommandBufferAllocateInfo { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO }; }
VkCommandBufferInheritanceInfo make_VkCommandBufferInheritanceInfo() { return VkCommandBufferInheritanceInfo { VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO }; }
VkCommandBufferBeginInfo make_VkCommandBufferBeginInfo() { return VkCommandBufferBeginInfo { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO }; }
VkRenderPassBeginInfo make_VkRenderPassBeginInfo() { return VkRenderPassBeginInfo { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO }; }
#endif

#if VK_VERSION_1_1
VkPhysicalDeviceSubgroupProperties make_VkPhysicalDeviceSubgroupProperties() { return VkPhysicalDeviceSubgroupProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES }; }
VkBindBufferMemoryInfo make_VkBindBufferMemoryInfo() { return VkBindBufferMemoryInfo { VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO }; }
VkBindImageMemoryInfo make_VkBindImageMemoryInfo() { return VkBindImageMemoryInfo { VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO }; }
VkPhysicalDevice16BitStorageFeatures make_VkPhysicalDevice16BitStorageFeatures() { return VkPhysicalDevice16BitStorageFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES }; }
VkMemoryDedicatedRequirements make_VkMemoryDedicatedRequirements() { return VkMemoryDedicatedRequirements { VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS }; }
VkMemoryDedicatedAllocateInfo make_VkMemoryDedicatedAllocateInfo() { return VkMemoryDedicatedAllocateInfo { VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO }; }
VkMemoryAllocateFlagsInfo make_VkMemoryAllocateFlagsInfo() { return VkMemoryAllocateFlagsInfo { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO }; }
VkDeviceGroupRenderPassBeginInfo make_VkDeviceGroupRenderPassBeginInfo() { return VkDeviceGroupRenderPassBeginInfo { VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO }; }
VkDeviceGroupCommandBufferBeginInfo make_VkDeviceGroupCommandBufferBeginInfo() { return VkDeviceGroupCommandBufferBeginInfo { VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO }; }
VkDeviceGroupSubmitInfo make_VkDeviceGroupSubmitInfo() { return VkDeviceGroupSubmitInfo { VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO }; }
VkDeviceGroupBindSparseInfo make_VkDeviceGroupBindSparseInfo() { return VkDeviceGroupBindSparseInfo { VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO }; }
VkBindBufferMemoryDeviceGroupInfo make_VkBindBufferMemoryDeviceGroupInfo() { return VkBindBufferMemoryDeviceGroupInfo { VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO }; }
VkBindImageMemoryDeviceGroupInfo make_VkBindImageMemoryDeviceGroupInfo() { return VkBindImageMemoryDeviceGroupInfo { VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO }; }
VkPhysicalDeviceGroupProperties make_VkPhysicalDeviceGroupProperties() { return VkPhysicalDeviceGroupProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES }; }
VkDeviceGroupDeviceCreateInfo make_VkDeviceGroupDeviceCreateInfo() { return VkDeviceGroupDeviceCreateInfo { VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO }; }
VkBufferMemoryRequirementsInfo2 make_VkBufferMemoryRequirementsInfo2() { return VkBufferMemoryRequirementsInfo2 { VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2 }; }
VkImageMemoryRequirementsInfo2 make_VkImageMemoryRequirementsInfo2() { return VkImageMemoryRequirementsInfo2 { VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2 }; }
VkImageSparseMemoryRequirementsInfo2 make_VkImageSparseMemoryRequirementsInfo2() { return VkImageSparseMemoryRequirementsInfo2 { VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2 }; }
VkMemoryRequirements2 make_VkMemoryRequirements2() { return VkMemoryRequirements2 { VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2 }; }
VkSparseImageMemoryRequirements2 make_VkSparseImageMemoryRequirements2() { return VkSparseImageMemoryRequirements2 { VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2 }; }
VkPhysicalDeviceFeatures2 make_VkPhysicalDeviceFeatures2() { return VkPhysicalDeviceFeatures2 { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 }; }
VkPhysicalDeviceProperties2 make_VkPhysicalDeviceProperties2() { return VkPhysicalDeviceProperties2 { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2 }; }
VkFormatProperties2 make_VkFormatProperties2() { return VkFormatProperties2 { VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2 }; }
VkImageFormatProperties2 make_VkImageFormatProperties2() { return VkImageFormatProperties2 { VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2 }; }
VkPhysicalDeviceImageFormatInfo2 make_VkPhysicalDeviceImageFormatInfo2() { return VkPhysicalDeviceImageFormatInfo2 { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2 }; }
VkQueueFamilyProperties2 make_VkQueueFamilyProperties2() { return VkQueueFamilyProperties2 { VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2 }; }
VkPhysicalDeviceMemoryProperties2 make_VkPhysicalDeviceMemoryProperties2() { return VkPhysicalDeviceMemoryProperties2 { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2 }; }
VkSparseImageFormatProperties2 make_VkSparseImageFormatProperties2() { return VkSparseImageFormatProperties2 { VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2 }; }
VkPhysicalDeviceSparseImageFormatInfo2 make_VkPhysicalDeviceSparseImageFormatInfo2() { return VkPhysicalDeviceSparseImageFormatInfo2 { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2 }; }
VkPhysicalDevicePointClippingProperties make_VkPhysicalDevicePointClippingProperties() { return VkPhysicalDevicePointClippingProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES }; }
VkRenderPassInputAttachmentAspectCreateInfo make_VkRenderPassInputAttachmentAspectCreateInfo() { return VkRenderPassInputAttachmentAspectCreateInfo { VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO }; }
VkImageViewUsageCreateInfo make_VkImageViewUsageCreateInfo() { return VkImageViewUsageCreateInfo { VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO }; }
VkPipelineTessellationDomainOriginStateCreateInfo make_VkPipelineTessellationDomainOriginStateCreateInfo() { return VkPipelineTessellationDomainOriginStateCreateInfo { VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO }; }
VkRenderPassMultiviewCreateInfo make_VkRenderPassMultiviewCreateInfo() { return VkRenderPassMultiviewCreateInfo { VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO }; }
VkPhysicalDeviceMultiviewFeatures make_VkPhysicalDeviceMultiviewFeatures() { return VkPhysicalDeviceMultiviewFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES }; }
VkPhysicalDeviceMultiviewProperties make_VkPhysicalDeviceMultiviewProperties() { return VkPhysicalDeviceMultiviewProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES }; }
VkPhysicalDeviceVariablePointersFeatures make_VkPhysicalDeviceVariablePointersFeatures() { return VkPhysicalDeviceVariablePointersFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES }; }
VkPhysicalDeviceProtectedMemoryFeatures make_VkPhysicalDeviceProtectedMemoryFeatures() { return VkPhysicalDeviceProtectedMemoryFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES }; }
VkPhysicalDeviceProtectedMemoryProperties make_VkPhysicalDeviceProtectedMemoryProperties() { return VkPhysicalDeviceProtectedMemoryProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES }; }
VkDeviceQueueInfo2 make_VkDeviceQueueInfo2() { return VkDeviceQueueInfo2 { VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2 }; }
VkProtectedSubmitInfo make_VkProtectedSubmitInfo() { return VkProtectedSubmitInfo { VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO }; }
VkSamplerYcbcrConversionCreateInfo make_VkSamplerYcbcrConversionCreateInfo() { return VkSamplerYcbcrConversionCreateInfo { VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO }; }
VkSamplerYcbcrConversionInfo make_VkSamplerYcbcrConversionInfo() { return VkSamplerYcbcrConversionInfo { VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO }; }
VkBindImagePlaneMemoryInfo make_VkBindImagePlaneMemoryInfo() { return VkBindImagePlaneMemoryInfo { VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO }; }
VkImagePlaneMemoryRequirementsInfo make_VkImagePlaneMemoryRequirementsInfo() { return VkImagePlaneMemoryRequirementsInfo { VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO }; }
VkPhysicalDeviceSamplerYcbcrConversionFeatures make_VkPhysicalDeviceSamplerYcbcrConversionFeatures() { return VkPhysicalDeviceSamplerYcbcrConversionFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES }; }
VkSamplerYcbcrConversionImageFormatProperties make_VkSamplerYcbcrConversionImageFormatProperties() { return VkSamplerYcbcrConversionImageFormatProperties { VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES }; }
VkDescriptorUpdateTemplateCreateInfo make_VkDescriptorUpdateTemplateCreateInfo() { return VkDescriptorUpdateTemplateCreateInfo { VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO }; }
VkPhysicalDeviceExternalImageFormatInfo make_VkPhysicalDeviceExternalImageFormatInfo() { return VkPhysicalDeviceExternalImageFormatInfo { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO }; }
VkExternalImageFormatProperties make_VkExternalImageFormatProperties() { return VkExternalImageFormatProperties { VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES }; }
VkPhysicalDeviceExternalBufferInfo make_VkPhysicalDeviceExternalBufferInfo() { return VkPhysicalDeviceExternalBufferInfo { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO }; }
VkExternalBufferProperties make_VkExternalBufferProperties() { return VkExternalBufferProperties { VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES }; }
VkPhysicalDeviceIDProperties make_VkPhysicalDeviceIDProperties() { return VkPhysicalDeviceIDProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES }; }
VkExternalMemoryImageCreateInfo make_VkExternalMemoryImageCreateInfo() { return VkExternalMemoryImageCreateInfo { VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO }; }
VkExternalMemoryBufferCreateInfo make_VkExternalMemoryBufferCreateInfo() { return VkExternalMemoryBufferCreateInfo { VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO }; }
VkExportMemoryAllocateInfo make_VkExportMemoryAllocateInfo() { return VkExportMemoryAllocateInfo { VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO }; }
VkPhysicalDeviceExternalFenceInfo make_VkPhysicalDeviceExternalFenceInfo() { return VkPhysicalDeviceExternalFenceInfo { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO }; }
VkExternalFenceProperties make_VkExternalFenceProperties() { return VkExternalFenceProperties { VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES }; }
VkExportFenceCreateInfo make_VkExportFenceCreateInfo() { return VkExportFenceCreateInfo { VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO }; }
VkExportSemaphoreCreateInfo make_VkExportSemaphoreCreateInfo() { return VkExportSemaphoreCreateInfo { VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO }; }
VkPhysicalDeviceExternalSemaphoreInfo make_VkPhysicalDeviceExternalSemaphoreInfo() { return VkPhysicalDeviceExternalSemaphoreInfo { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO }; }
VkExternalSemaphoreProperties make_VkExternalSemaphoreProperties() { return VkExternalSemaphoreProperties { VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES }; }
VkPhysicalDeviceMaintenance3Properties make_VkPhysicalDeviceMaintenance3Properties() { return VkPhysicalDeviceMaintenance3Properties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES }; }
VkDescriptorSetLayoutSupport make_VkDescriptorSetLayoutSupport() { return VkDescriptorSetLayoutSupport { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT }; }
VkPhysicalDeviceShaderDrawParametersFeatures make_VkPhysicalDeviceShaderDrawParametersFeatures() { return VkPhysicalDeviceShaderDrawParametersFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES }; }
#endif

#if VK_VERSION_1_2
VkPhysicalDeviceVulkan11Features make_VkPhysicalDeviceVulkan11Features() { return VkPhysicalDeviceVulkan11Features { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES }; }
VkPhysicalDeviceVulkan11Properties make_VkPhysicalDeviceVulkan11Properties() { return VkPhysicalDeviceVulkan11Properties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES }; }
VkPhysicalDeviceVulkan12Features make_VkPhysicalDeviceVulkan12Features() { return VkPhysicalDeviceVulkan12Features { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES }; }
VkPhysicalDeviceVulkan12Properties make_VkPhysicalDeviceVulkan12Properties() { return VkPhysicalDeviceVulkan12Properties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES }; }
VkImageFormatListCreateInfo make_VkImageFormatListCreateInfo() { return VkImageFormatListCreateInfo { VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO }; }
VkAttachmentDescription2 make_VkAttachmentDescription2() { return VkAttachmentDescription2 { VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2 }; }
VkAttachmentReference2 make_VkAttachmentReference2() { return VkAttachmentReference2 { VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2 }; }
VkSubpassDescription2 make_VkSubpassDescription2() { return VkSubpassDescription2 { VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2 }; }
VkSubpassDependency2 make_VkSubpassDependency2() { return VkSubpassDependency2 { VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2 }; }
VkRenderPassCreateInfo2 make_VkRenderPassCreateInfo2() { return VkRenderPassCreateInfo2 { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2 }; }
VkSubpassBeginInfo make_VkSubpassBeginInfo() { return VkSubpassBeginInfo { VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO }; }
VkSubpassEndInfo make_VkSubpassEndInfo() { return VkSubpassEndInfo { VK_STRUCTURE_TYPE_SUBPASS_END_INFO }; }
VkPhysicalDevice8BitStorageFeatures make_VkPhysicalDevice8BitStorageFeatures() { return VkPhysicalDevice8BitStorageFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES }; }
VkPhysicalDeviceDriverProperties make_VkPhysicalDeviceDriverProperties() { return VkPhysicalDeviceDriverProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES }; }
VkPhysicalDeviceShaderAtomicInt64Features make_VkPhysicalDeviceShaderAtomicInt64Features() { return VkPhysicalDeviceShaderAtomicInt64Features { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES }; }
VkPhysicalDeviceShaderFloat16Int8Features make_VkPhysicalDeviceShaderFloat16Int8Features() { return VkPhysicalDeviceShaderFloat16Int8Features { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES }; }
VkPhysicalDeviceFloatControlsProperties make_VkPhysicalDeviceFloatControlsProperties() { return VkPhysicalDeviceFloatControlsProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES }; }
VkDescriptorSetLayoutBindingFlagsCreateInfo make_VkDescriptorSetLayoutBindingFlagsCreateInfo() { return VkDescriptorSetLayoutBindingFlagsCreateInfo { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO }; }
VkPhysicalDeviceDescriptorIndexingFeatures make_VkPhysicalDeviceDescriptorIndexingFeatures() { return VkPhysicalDeviceDescriptorIndexingFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES }; }
VkPhysicalDeviceDescriptorIndexingProperties make_VkPhysicalDeviceDescriptorIndexingProperties() { return VkPhysicalDeviceDescriptorIndexingProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES }; }
VkDescriptorSetVariableDescriptorCountAllocateInfo make_VkDescriptorSetVariableDescriptorCountAllocateInfo() { return VkDescriptorSetVariableDescriptorCountAllocateInfo { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO }; }
VkDescriptorSetVariableDescriptorCountLayoutSupport make_VkDescriptorSetVariableDescriptorCountLayoutSupport() { return VkDescriptorSetVariableDescriptorCountLayoutSupport { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT }; }
VkSubpassDescriptionDepthStencilResolve make_VkSubpassDescriptionDepthStencilResolve() { return VkSubpassDescriptionDepthStencilResolve { VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE }; }
VkPhysicalDeviceDepthStencilResolveProperties make_VkPhysicalDeviceDepthStencilResolveProperties() { return VkPhysicalDeviceDepthStencilResolveProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES }; }
VkPhysicalDeviceScalarBlockLayoutFeatures make_VkPhysicalDeviceScalarBlockLayoutFeatures() { return VkPhysicalDeviceScalarBlockLayoutFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES }; }
VkImageStencilUsageCreateInfo make_VkImageStencilUsageCreateInfo() { return VkImageStencilUsageCreateInfo { VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO }; }
VkSamplerReductionModeCreateInfo make_VkSamplerReductionModeCreateInfo() { return VkSamplerReductionModeCreateInfo { VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO }; }
VkPhysicalDeviceSamplerFilterMinmaxProperties make_VkPhysicalDeviceSamplerFilterMinmaxProperties() { return VkPhysicalDeviceSamplerFilterMinmaxProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES }; }
VkPhysicalDeviceVulkanMemoryModelFeatures make_VkPhysicalDeviceVulkanMemoryModelFeatures() { return VkPhysicalDeviceVulkanMemoryModelFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES }; }
VkPhysicalDeviceImagelessFramebufferFeatures make_VkPhysicalDeviceImagelessFramebufferFeatures() { return VkPhysicalDeviceImagelessFramebufferFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES }; }
VkFramebufferAttachmentImageInfo make_VkFramebufferAttachmentImageInfo() { return VkFramebufferAttachmentImageInfo { VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO }; }
VkFramebufferAttachmentsCreateInfo make_VkFramebufferAttachmentsCreateInfo() { return VkFramebufferAttachmentsCreateInfo { VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO }; }
VkRenderPassAttachmentBeginInfo make_VkRenderPassAttachmentBeginInfo() { return VkRenderPassAttachmentBeginInfo { VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO }; }
VkPhysicalDeviceUniformBufferStandardLayoutFeatures make_VkPhysicalDeviceUniformBufferStandardLayoutFeatures() { return VkPhysicalDeviceUniformBufferStandardLayoutFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES }; }
VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures make_VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures() { return VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES }; }
VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures make_VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures() { return VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES }; }
VkAttachmentReferenceStencilLayout make_VkAttachmentReferenceStencilLayout() { return VkAttachmentReferenceStencilLayout { VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT }; }
VkAttachmentDescriptionStencilLayout make_VkAttachmentDescriptionStencilLayout() { return VkAttachmentDescriptionStencilLayout { VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT }; }
VkPhysicalDeviceHostQueryResetFeatures make_VkPhysicalDeviceHostQueryResetFeatures() { return VkPhysicalDeviceHostQueryResetFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES }; }
VkPhysicalDeviceTimelineSemaphoreFeatures make_VkPhysicalDeviceTimelineSemaphoreFeatures() { return VkPhysicalDeviceTimelineSemaphoreFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES }; }
VkPhysicalDeviceTimelineSemaphoreProperties make_VkPhysicalDeviceTimelineSemaphoreProperties() { return VkPhysicalDeviceTimelineSemaphoreProperties { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES }; }
VkSemaphoreTypeCreateInfo make_VkSemaphoreTypeCreateInfo() { return VkSemaphoreTypeCreateInfo { VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO }; }
VkTimelineSemaphoreSubmitInfo make_VkTimelineSemaphoreSubmitInfo() { return VkTimelineSemaphoreSubmitInfo { VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO }; }
VkSemaphoreWaitInfo make_VkSemaphoreWaitInfo() { return VkSemaphoreWaitInfo { VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO }; }
VkSemaphoreSignalInfo make_VkSemaphoreSignalInfo() { return VkSemaphoreSignalInfo { VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO }; }
VkPhysicalDeviceBufferDeviceAddressFeatures make_VkPhysicalDeviceBufferDeviceAddressFeatures() { return VkPhysicalDeviceBufferDeviceAddressFeatures { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES }; }
VkBufferDeviceAddressInfo make_VkBufferDeviceAddressInfo() { return VkBufferDeviceAddressInfo { VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO }; }
VkBufferOpaqueCaptureAddressCreateInfo make_VkBufferOpaqueCaptureAddressCreateInfo() { return VkBufferOpaqueCaptureAddressCreateInfo { VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO }; }
VkMemoryOpaqueCaptureAddressAllocateInfo make_VkMemoryOpaqueCaptureAddressAllocateInfo() { return VkMemoryOpaqueCaptureAddressAllocateInfo { VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO }; }
VkDeviceMemoryOpaqueCaptureAddressInfo make_VkDeviceMemoryOpaqueCaptureAddressInfo() { return VkDeviceMemoryOpaqueCaptureAddressInfo { VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO }; }
#endif

#if VK_KHR_swapchain
VkSwapchainCreateInfoKHR make_VkSwapchainCreateInfoKHR() { return VkSwapchainCreateInfoKHR { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR }; }
VkPresentInfoKHR make_VkPresentInfoKHR() { return VkPresentInfoKHR { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR }; }
VkImageSwapchainCreateInfoKHR make_VkImageSwapchainCreateInfoKHR() { return VkImageSwapchainCreateInfoKHR { VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR }; }
VkBindImageMemorySwapchainInfoKHR make_VkBindImageMemorySwapchainInfoKHR() { return VkBindImageMemorySwapchainInfoKHR { VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR }; }
VkAcquireNextImageInfoKHR make_VkAcquireNextImageInfoKHR() { return VkAcquireNextImageInfoKHR { VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR }; }
VkDeviceGroupPresentCapabilitiesKHR make_VkDeviceGroupPresentCapabilitiesKHR() { return VkDeviceGroupPresentCapabilitiesKHR { VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR }; }
VkDeviceGroupPresentInfoKHR make_VkDeviceGroupPresentInfoKHR() { return VkDeviceGroupPresentInfoKHR { VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR }; }
VkDeviceGroupSwapchainCreateInfoKHR make_VkDeviceGroupSwapchainCreateInfoKHR() { return VkDeviceGroupSwapchainCreateInfoKHR { VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR }; }
#endif

#if VK_KHR_display
VkDisplayModeCreateInfoKHR make_VkDisplayModeCreateInfoKHR() { return VkDisplayModeCreateInfoKHR { VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR }; }
VkDisplaySurfaceCreateInfoKHR make_VkDisplaySurfaceCreateInfoKHR() { return VkDisplaySurfaceCreateInfoKHR { VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR }; }
#endif

#if VK_KHR_display_swapchain
VkDisplayPresentInfoKHR make_VkDisplayPresentInfoKHR() { return VkDisplayPresentInfoKHR { VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR }; }
#endif

#if VK_KHR_dynamic_rendering
VkRenderingAttachmentInfoKHR make_VkRenderingAttachmentInfoKHR() { return VkRenderingAttachmentInfoKHR { VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR }; }
VkRenderingInfoKHR make_VkRenderingInfoKHR() { return VkRenderingInfoKHR { VK_STRUCTURE_TYPE_RENDERING_INFO_KHR }; }
VkPipelineRenderingCreateInfoKHR make_VkPipelineRenderingCreateInfoKHR() { return VkPipelineRenderingCreateInfoKHR { VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO_KHR }; }
VkPhysicalDeviceDynamicRenderingFeaturesKHR make_VkPhysicalDeviceDynamicRenderingFeaturesKHR() { return VkPhysicalDeviceDynamicRenderingFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR }; }
VkCommandBufferInheritanceRenderingInfoKHR make_VkCommandBufferInheritanceRenderingInfoKHR() { return VkCommandBufferInheritanceRenderingInfoKHR { VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDERING_INFO_KHR }; }
VkRenderingFragmentShadingRateAttachmentInfoKHR make_VkRenderingFragmentShadingRateAttachmentInfoKHR() { return VkRenderingFragmentShadingRateAttachmentInfoKHR { VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR }; }
VkRenderingFragmentDensityMapAttachmentInfoEXT make_VkRenderingFragmentDensityMapAttachmentInfoEXT() { return VkRenderingFragmentDensityMapAttachmentInfoEXT { VK_STRUCTURE_TYPE_RENDERING_FRAGMENT_DENSITY_MAP_ATTACHMENT_INFO_EXT }; }
VkAttachmentSampleCountInfoAMD make_VkAttachmentSampleCountInfoAMD() { return VkAttachmentSampleCountInfoAMD { VK_STRUCTURE_TYPE_ATTACHMENT_SAMPLE_COUNT_INFO_AMD }; }
VkMultiviewPerViewAttributesInfoNVX make_VkMultiviewPerViewAttributesInfoNVX() { return VkMultiviewPerViewAttributesInfoNVX { VK_STRUCTURE_TYPE_MULTIVIEW_PER_VIEW_ATTRIBUTES_INFO_NVX }; }
#endif

#if VK_KHR_external_memory_fd
VkImportMemoryFdInfoKHR make_VkImportMemoryFdInfoKHR() { return VkImportMemoryFdInfoKHR { VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR }; }
VkMemoryFdPropertiesKHR make_VkMemoryFdPropertiesKHR() { return VkMemoryFdPropertiesKHR { VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR }; }
VkMemoryGetFdInfoKHR make_VkMemoryGetFdInfoKHR() { return VkMemoryGetFdInfoKHR { VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR }; }
#endif

#if VK_KHR_external_semaphore_fd
VkImportSemaphoreFdInfoKHR make_VkImportSemaphoreFdInfoKHR() { return VkImportSemaphoreFdInfoKHR { VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR }; }
VkSemaphoreGetFdInfoKHR make_VkSemaphoreGetFdInfoKHR() { return VkSemaphoreGetFdInfoKHR { VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR }; }
#endif

#if VK_KHR_push_descriptor
VkPhysicalDevicePushDescriptorPropertiesKHR make_VkPhysicalDevicePushDescriptorPropertiesKHR() { return VkPhysicalDevicePushDescriptorPropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR }; }
#endif

#if VK_KHR_incremental_present
VkPresentRegionsKHR make_VkPresentRegionsKHR() { return VkPresentRegionsKHR { VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR }; }
#endif

#if VK_KHR_shared_presentable_image
VkSharedPresentSurfaceCapabilitiesKHR make_VkSharedPresentSurfaceCapabilitiesKHR() { return VkSharedPresentSurfaceCapabilitiesKHR { VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR }; }
#endif

#if VK_KHR_external_fence_fd
VkImportFenceFdInfoKHR make_VkImportFenceFdInfoKHR() { return VkImportFenceFdInfoKHR { VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR }; }
VkFenceGetFdInfoKHR make_VkFenceGetFdInfoKHR() { return VkFenceGetFdInfoKHR { VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR }; }
#endif

#if VK_KHR_performance_query
VkPhysicalDevicePerformanceQueryFeaturesKHR make_VkPhysicalDevicePerformanceQueryFeaturesKHR() { return VkPhysicalDevicePerformanceQueryFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR }; }
VkPhysicalDevicePerformanceQueryPropertiesKHR make_VkPhysicalDevicePerformanceQueryPropertiesKHR() { return VkPhysicalDevicePerformanceQueryPropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR }; }
VkPerformanceCounterKHR make_VkPerformanceCounterKHR() { return VkPerformanceCounterKHR { VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR }; }
VkPerformanceCounterDescriptionKHR make_VkPerformanceCounterDescriptionKHR() { return VkPerformanceCounterDescriptionKHR { VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR }; }
VkQueryPoolPerformanceCreateInfoKHR make_VkQueryPoolPerformanceCreateInfoKHR() { return VkQueryPoolPerformanceCreateInfoKHR { VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR }; }
VkAcquireProfilingLockInfoKHR make_VkAcquireProfilingLockInfoKHR() { return VkAcquireProfilingLockInfoKHR { VK_STRUCTURE_TYPE_ACQUIRE_PROFILING_LOCK_INFO_KHR }; }
VkPerformanceQuerySubmitInfoKHR make_VkPerformanceQuerySubmitInfoKHR() { return VkPerformanceQuerySubmitInfoKHR { VK_STRUCTURE_TYPE_PERFORMANCE_QUERY_SUBMIT_INFO_KHR }; }
#endif

#if VK_KHR_get_surface_capabilities2
VkPhysicalDeviceSurfaceInfo2KHR make_VkPhysicalDeviceSurfaceInfo2KHR() { return VkPhysicalDeviceSurfaceInfo2KHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR }; }
VkSurfaceCapabilities2KHR make_VkSurfaceCapabilities2KHR() { return VkSurfaceCapabilities2KHR { VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR }; }
VkSurfaceFormat2KHR make_VkSurfaceFormat2KHR() { return VkSurfaceFormat2KHR { VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR }; }
#endif

#if VK_KHR_get_display_properties2
VkDisplayProperties2KHR make_VkDisplayProperties2KHR() { return VkDisplayProperties2KHR { VK_STRUCTURE_TYPE_DISPLAY_PROPERTIES_2_KHR }; }
VkDisplayPlaneProperties2KHR make_VkDisplayPlaneProperties2KHR() { return VkDisplayPlaneProperties2KHR { VK_STRUCTURE_TYPE_DISPLAY_PLANE_PROPERTIES_2_KHR }; }
VkDisplayModeProperties2KHR make_VkDisplayModeProperties2KHR() { return VkDisplayModeProperties2KHR { VK_STRUCTURE_TYPE_DISPLAY_MODE_PROPERTIES_2_KHR }; }
VkDisplayPlaneInfo2KHR make_VkDisplayPlaneInfo2KHR() { return VkDisplayPlaneInfo2KHR { VK_STRUCTURE_TYPE_DISPLAY_PLANE_INFO_2_KHR }; }
VkDisplayPlaneCapabilities2KHR make_VkDisplayPlaneCapabilities2KHR() { return VkDisplayPlaneCapabilities2KHR { VK_STRUCTURE_TYPE_DISPLAY_PLANE_CAPABILITIES_2_KHR }; }
#endif

#if VK_KHR_shader_clock
VkPhysicalDeviceShaderClockFeaturesKHR make_VkPhysicalDeviceShaderClockFeaturesKHR() { return VkPhysicalDeviceShaderClockFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR }; }
#endif

#if VK_KHR_shader_terminate_invocation
VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR make_VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR() { return VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_TERMINATE_INVOCATION_FEATURES_KHR }; }
#endif

#if VK_KHR_fragment_shading_rate
VkFragmentShadingRateAttachmentInfoKHR make_VkFragmentShadingRateAttachmentInfoKHR() { return VkFragmentShadingRateAttachmentInfoKHR { VK_STRUCTURE_TYPE_FRAGMENT_SHADING_RATE_ATTACHMENT_INFO_KHR }; }
VkPipelineFragmentShadingRateStateCreateInfoKHR make_VkPipelineFragmentShadingRateStateCreateInfoKHR() { return VkPipelineFragmentShadingRateStateCreateInfoKHR { VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_STATE_CREATE_INFO_KHR }; }
VkPhysicalDeviceFragmentShadingRateFeaturesKHR make_VkPhysicalDeviceFragmentShadingRateFeaturesKHR() { return VkPhysicalDeviceFragmentShadingRateFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_FEATURES_KHR }; }
VkPhysicalDeviceFragmentShadingRatePropertiesKHR make_VkPhysicalDeviceFragmentShadingRatePropertiesKHR() { return VkPhysicalDeviceFragmentShadingRatePropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_PROPERTIES_KHR }; }
VkPhysicalDeviceFragmentShadingRateKHR make_VkPhysicalDeviceFragmentShadingRateKHR() { return VkPhysicalDeviceFragmentShadingRateKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_KHR }; }
#endif

#if VK_KHR_surface_protected_capabilities
VkSurfaceProtectedCapabilitiesKHR make_VkSurfaceProtectedCapabilitiesKHR() { return VkSurfaceProtectedCapabilitiesKHR { VK_STRUCTURE_TYPE_SURFACE_PROTECTED_CAPABILITIES_KHR }; }
#endif

#if VK_KHR_present_wait
VkPhysicalDevicePresentWaitFeaturesKHR make_VkPhysicalDevicePresentWaitFeaturesKHR() { return VkPhysicalDevicePresentWaitFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR }; }
#endif

#if VK_KHR_pipeline_executable_properties
VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR make_VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR() { return VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR }; }
VkPipelineInfoKHR make_VkPipelineInfoKHR() { return VkPipelineInfoKHR { VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR }; }
VkPipelineExecutablePropertiesKHR make_VkPipelineExecutablePropertiesKHR() { return VkPipelineExecutablePropertiesKHR { VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR }; }
VkPipelineExecutableInfoKHR make_VkPipelineExecutableInfoKHR() { return VkPipelineExecutableInfoKHR { VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR }; }
VkPipelineExecutableStatisticKHR make_VkPipelineExecutableStatisticKHR() { return VkPipelineExecutableStatisticKHR { VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR }; }
VkPipelineExecutableInternalRepresentationKHR make_VkPipelineExecutableInternalRepresentationKHR() { return VkPipelineExecutableInternalRepresentationKHR { VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR }; }
#endif

#if VK_KHR_shader_integer_dot_product
VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR make_VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR() { return VkPhysicalDeviceShaderIntegerDotProductFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_FEATURES_KHR }; }
VkPhysicalDeviceShaderIntegerDotProductPropertiesKHR make_VkPhysicalDeviceShaderIntegerDotProductPropertiesKHR() { return VkPhysicalDeviceShaderIntegerDotProductPropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_DOT_PRODUCT_PROPERTIES_KHR }; }
#endif

#if VK_KHR_pipeline_library
VkPipelineLibraryCreateInfoKHR make_VkPipelineLibraryCreateInfoKHR() { return VkPipelineLibraryCreateInfoKHR { VK_STRUCTURE_TYPE_PIPELINE_LIBRARY_CREATE_INFO_KHR }; }
#endif

#if VK_KHR_present_id
VkPresentIdKHR make_VkPresentIdKHR() { return VkPresentIdKHR { VK_STRUCTURE_TYPE_PRESENT_ID_KHR }; }
VkPhysicalDevicePresentIdFeaturesKHR make_VkPhysicalDevicePresentIdFeaturesKHR() { return VkPhysicalDevicePresentIdFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR }; }
#endif

#if VK_KHR_synchronization2
VkMemoryBarrier2KHR make_VkMemoryBarrier2KHR() { return VkMemoryBarrier2KHR { VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR }; }
VkBufferMemoryBarrier2KHR make_VkBufferMemoryBarrier2KHR() { return VkBufferMemoryBarrier2KHR { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2_KHR }; }
VkImageMemoryBarrier2KHR make_VkImageMemoryBarrier2KHR() { return VkImageMemoryBarrier2KHR { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR }; }
VkDependencyInfoKHR make_VkDependencyInfoKHR() { return VkDependencyInfoKHR { VK_STRUCTURE_TYPE_DEPENDENCY_INFO_KHR }; }
VkSemaphoreSubmitInfoKHR make_VkSemaphoreSubmitInfoKHR() { return VkSemaphoreSubmitInfoKHR { VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR }; }
VkCommandBufferSubmitInfoKHR make_VkCommandBufferSubmitInfoKHR() { return VkCommandBufferSubmitInfoKHR { VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR }; }
VkSubmitInfo2KHR make_VkSubmitInfo2KHR() { return VkSubmitInfo2KHR { VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR }; }
VkPhysicalDeviceSynchronization2FeaturesKHR make_VkPhysicalDeviceSynchronization2FeaturesKHR() { return VkPhysicalDeviceSynchronization2FeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR }; }
VkQueueFamilyCheckpointProperties2NV make_VkQueueFamilyCheckpointProperties2NV() { return VkQueueFamilyCheckpointProperties2NV { VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_2_NV }; }
VkCheckpointData2NV make_VkCheckpointData2NV() { return VkCheckpointData2NV { VK_STRUCTURE_TYPE_CHECKPOINT_DATA_2_NV }; }
#endif

#if VK_KHR_shader_subgroup_uniform_control_flow
VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR make_VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR() { return VkPhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_UNIFORM_CONTROL_FLOW_FEATURES_KHR }; }
#endif

#if VK_KHR_zero_initialize_workgroup_memory
VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR make_VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR() { return VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ZERO_INITIALIZE_WORKGROUP_MEMORY_FEATURES_KHR }; }
#endif

#if VK_KHR_workgroup_memory_explicit_layout
VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR make_VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR() { return VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_WORKGROUP_MEMORY_EXPLICIT_LAYOUT_FEATURES_KHR }; }
#endif

#if VK_KHR_copy_commands2
VkBufferCopy2KHR make_VkBufferCopy2KHR() { return VkBufferCopy2KHR { VK_STRUCTURE_TYPE_BUFFER_COPY_2_KHR }; }
VkCopyBufferInfo2KHR make_VkCopyBufferInfo2KHR() { return VkCopyBufferInfo2KHR { VK_STRUCTURE_TYPE_COPY_BUFFER_INFO_2_KHR }; }
VkImageCopy2KHR make_VkImageCopy2KHR() { return VkImageCopy2KHR { VK_STRUCTURE_TYPE_IMAGE_COPY_2_KHR }; }
VkCopyImageInfo2KHR make_VkCopyImageInfo2KHR() { return VkCopyImageInfo2KHR { VK_STRUCTURE_TYPE_COPY_IMAGE_INFO_2_KHR }; }
VkBufferImageCopy2KHR make_VkBufferImageCopy2KHR() { return VkBufferImageCopy2KHR { VK_STRUCTURE_TYPE_BUFFER_IMAGE_COPY_2_KHR }; }
VkCopyBufferToImageInfo2KHR make_VkCopyBufferToImageInfo2KHR() { return VkCopyBufferToImageInfo2KHR { VK_STRUCTURE_TYPE_COPY_BUFFER_TO_IMAGE_INFO_2_KHR }; }
VkCopyImageToBufferInfo2KHR make_VkCopyImageToBufferInfo2KHR() { return VkCopyImageToBufferInfo2KHR { VK_STRUCTURE_TYPE_COPY_IMAGE_TO_BUFFER_INFO_2_KHR }; }
VkImageBlit2KHR make_VkImageBlit2KHR() { return VkImageBlit2KHR { VK_STRUCTURE_TYPE_IMAGE_BLIT_2_KHR }; }
VkBlitImageInfo2KHR make_VkBlitImageInfo2KHR() { return VkBlitImageInfo2KHR { VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2_KHR }; }
VkImageResolve2KHR make_VkImageResolve2KHR() { return VkImageResolve2KHR { VK_STRUCTURE_TYPE_IMAGE_RESOLVE_2_KHR }; }
VkResolveImageInfo2KHR make_VkResolveImageInfo2KHR() { return VkResolveImageInfo2KHR { VK_STRUCTURE_TYPE_RESOLVE_IMAGE_INFO_2_KHR }; }
#endif

#if VK_KHR_format_feature_flags2
VkFormatProperties3KHR make_VkFormatProperties3KHR() { return VkFormatProperties3KHR { VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_3_KHR }; }
#endif

#if VK_KHR_maintenance4
VkPhysicalDeviceMaintenance4FeaturesKHR make_VkPhysicalDeviceMaintenance4FeaturesKHR() { return VkPhysicalDeviceMaintenance4FeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_FEATURES_KHR }; }
VkPhysicalDeviceMaintenance4PropertiesKHR make_VkPhysicalDeviceMaintenance4PropertiesKHR() { return VkPhysicalDeviceMaintenance4PropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_4_PROPERTIES_KHR }; }
VkDeviceBufferMemoryRequirementsKHR make_VkDeviceBufferMemoryRequirementsKHR() { return VkDeviceBufferMemoryRequirementsKHR { VK_STRUCTURE_TYPE_DEVICE_BUFFER_MEMORY_REQUIREMENTS_KHR }; }
VkDeviceImageMemoryRequirementsKHR make_VkDeviceImageMemoryRequirementsKHR() { return VkDeviceImageMemoryRequirementsKHR { VK_STRUCTURE_TYPE_DEVICE_IMAGE_MEMORY_REQUIREMENTS_KHR }; }
#endif

#if VK_EXT_debug_report
VkDebugReportCallbackCreateInfoEXT make_VkDebugReportCallbackCreateInfoEXT() { return VkDebugReportCallbackCreateInfoEXT { VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT }; }
#endif

#if VK_AMD_rasterization_order
VkPipelineRasterizationStateRasterizationOrderAMD make_VkPipelineRasterizationStateRasterizationOrderAMD() { return VkPipelineRasterizationStateRasterizationOrderAMD { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD }; }
#endif

#if VK_EXT_debug_marker
VkDebugMarkerObjectNameInfoEXT make_VkDebugMarkerObjectNameInfoEXT() { return VkDebugMarkerObjectNameInfoEXT { VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT }; }
VkDebugMarkerObjectTagInfoEXT make_VkDebugMarkerObjectTagInfoEXT() { return VkDebugMarkerObjectTagInfoEXT { VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT }; }
VkDebugMarkerMarkerInfoEXT make_VkDebugMarkerMarkerInfoEXT() { return VkDebugMarkerMarkerInfoEXT { VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT }; }
#endif

#if VK_NV_dedicated_allocation
VkDedicatedAllocationImageCreateInfoNV make_VkDedicatedAllocationImageCreateInfoNV() { return VkDedicatedAllocationImageCreateInfoNV { VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV }; }
VkDedicatedAllocationBufferCreateInfoNV make_VkDedicatedAllocationBufferCreateInfoNV() { return VkDedicatedAllocationBufferCreateInfoNV { VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV }; }
VkDedicatedAllocationMemoryAllocateInfoNV make_VkDedicatedAllocationMemoryAllocateInfoNV() { return VkDedicatedAllocationMemoryAllocateInfoNV { VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV }; }
#endif

#if VK_EXT_transform_feedback
VkPhysicalDeviceTransformFeedbackFeaturesEXT make_VkPhysicalDeviceTransformFeedbackFeaturesEXT() { return VkPhysicalDeviceTransformFeedbackFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT }; }
VkPhysicalDeviceTransformFeedbackPropertiesEXT make_VkPhysicalDeviceTransformFeedbackPropertiesEXT() { return VkPhysicalDeviceTransformFeedbackPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_PROPERTIES_EXT }; }
VkPipelineRasterizationStateStreamCreateInfoEXT make_VkPipelineRasterizationStateStreamCreateInfoEXT() { return VkPipelineRasterizationStateStreamCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_STREAM_CREATE_INFO_EXT }; }
#endif

#if VK_NVX_binary_import
VkCuModuleCreateInfoNVX make_VkCuModuleCreateInfoNVX() { return VkCuModuleCreateInfoNVX { VK_STRUCTURE_TYPE_CU_MODULE_CREATE_INFO_NVX }; }
VkCuFunctionCreateInfoNVX make_VkCuFunctionCreateInfoNVX() { return VkCuFunctionCreateInfoNVX { VK_STRUCTURE_TYPE_CU_FUNCTION_CREATE_INFO_NVX }; }
VkCuLaunchInfoNVX make_VkCuLaunchInfoNVX() { return VkCuLaunchInfoNVX { VK_STRUCTURE_TYPE_CU_LAUNCH_INFO_NVX }; }
#endif

#if VK_NVX_image_view_handle
VkImageViewHandleInfoNVX make_VkImageViewHandleInfoNVX() { return VkImageViewHandleInfoNVX { VK_STRUCTURE_TYPE_IMAGE_VIEW_HANDLE_INFO_NVX }; }
VkImageViewAddressPropertiesNVX make_VkImageViewAddressPropertiesNVX() { return VkImageViewAddressPropertiesNVX { VK_STRUCTURE_TYPE_IMAGE_VIEW_ADDRESS_PROPERTIES_NVX }; }
#endif

#if VK_AMD_texture_gather_bias_lod
VkTextureLODGatherFormatPropertiesAMD make_VkTextureLODGatherFormatPropertiesAMD() { return VkTextureLODGatherFormatPropertiesAMD { VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD }; }
#endif

#if VK_NV_corner_sampled_image
VkPhysicalDeviceCornerSampledImageFeaturesNV make_VkPhysicalDeviceCornerSampledImageFeaturesNV() { return VkPhysicalDeviceCornerSampledImageFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV }; }
#endif

#if VK_NV_external_memory
VkExternalMemoryImageCreateInfoNV make_VkExternalMemoryImageCreateInfoNV() { return VkExternalMemoryImageCreateInfoNV { VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV }; }
VkExportMemoryAllocateInfoNV make_VkExportMemoryAllocateInfoNV() { return VkExportMemoryAllocateInfoNV { VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV }; }
#endif

#if VK_EXT_validation_flags
VkValidationFlagsEXT make_VkValidationFlagsEXT() { return VkValidationFlagsEXT { VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT }; }
#endif

#if VK_EXT_texture_compression_astc_hdr
VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT make_VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT() { return VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT }; }
#endif

#if VK_EXT_astc_decode_mode
VkImageViewASTCDecodeModeEXT make_VkImageViewASTCDecodeModeEXT() { return VkImageViewASTCDecodeModeEXT { VK_STRUCTURE_TYPE_IMAGE_VIEW_ASTC_DECODE_MODE_EXT }; }
VkPhysicalDeviceASTCDecodeFeaturesEXT make_VkPhysicalDeviceASTCDecodeFeaturesEXT() { return VkPhysicalDeviceASTCDecodeFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT }; }
#endif

#if VK_EXT_conditional_rendering
VkConditionalRenderingBeginInfoEXT make_VkConditionalRenderingBeginInfoEXT() { return VkConditionalRenderingBeginInfoEXT { VK_STRUCTURE_TYPE_CONDITIONAL_RENDERING_BEGIN_INFO_EXT }; }
VkPhysicalDeviceConditionalRenderingFeaturesEXT make_VkPhysicalDeviceConditionalRenderingFeaturesEXT() { return VkPhysicalDeviceConditionalRenderingFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT }; }
VkCommandBufferInheritanceConditionalRenderingInfoEXT make_VkCommandBufferInheritanceConditionalRenderingInfoEXT() { return VkCommandBufferInheritanceConditionalRenderingInfoEXT { VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT }; }
#endif

#if VK_NV_clip_space_w_scaling
VkPipelineViewportWScalingStateCreateInfoNV make_VkPipelineViewportWScalingStateCreateInfoNV() { return VkPipelineViewportWScalingStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV }; }
#endif

#if VK_EXT_display_surface_counter
VkSurfaceCapabilities2EXT make_VkSurfaceCapabilities2EXT() { return VkSurfaceCapabilities2EXT { VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES2_EXT }; }
#endif

#if VK_EXT_display_control
VkDisplayPowerInfoEXT make_VkDisplayPowerInfoEXT() { return VkDisplayPowerInfoEXT { VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT }; }
VkDeviceEventInfoEXT make_VkDeviceEventInfoEXT() { return VkDeviceEventInfoEXT { VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT }; }
VkDisplayEventInfoEXT make_VkDisplayEventInfoEXT() { return VkDisplayEventInfoEXT { VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT }; }
VkSwapchainCounterCreateInfoEXT make_VkSwapchainCounterCreateInfoEXT() { return VkSwapchainCounterCreateInfoEXT { VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT }; }
#endif

#if VK_GOOGLE_display_timing
VkPresentTimesInfoGOOGLE make_VkPresentTimesInfoGOOGLE() { return VkPresentTimesInfoGOOGLE { VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE }; }
#endif

#if VK_NVX_multiview_per_view_attributes
VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX make_VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX() { return VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX }; }
#endif

#if VK_NV_viewport_swizzle
VkPipelineViewportSwizzleStateCreateInfoNV make_VkPipelineViewportSwizzleStateCreateInfoNV() { return VkPipelineViewportSwizzleStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV }; }
#endif

#if VK_EXT_discard_rectangles
VkPhysicalDeviceDiscardRectanglePropertiesEXT make_VkPhysicalDeviceDiscardRectanglePropertiesEXT() { return VkPhysicalDeviceDiscardRectanglePropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT }; }
VkPipelineDiscardRectangleStateCreateInfoEXT make_VkPipelineDiscardRectangleStateCreateInfoEXT() { return VkPipelineDiscardRectangleStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_conservative_rasterization
VkPhysicalDeviceConservativeRasterizationPropertiesEXT make_VkPhysicalDeviceConservativeRasterizationPropertiesEXT() { return VkPhysicalDeviceConservativeRasterizationPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT }; }
VkPipelineRasterizationConservativeStateCreateInfoEXT make_VkPipelineRasterizationConservativeStateCreateInfoEXT() { return VkPipelineRasterizationConservativeStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_depth_clip_enable
VkPhysicalDeviceDepthClipEnableFeaturesEXT make_VkPhysicalDeviceDepthClipEnableFeaturesEXT() { return VkPhysicalDeviceDepthClipEnableFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT }; }
VkPipelineRasterizationDepthClipStateCreateInfoEXT make_VkPipelineRasterizationDepthClipStateCreateInfoEXT() { return VkPipelineRasterizationDepthClipStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_DEPTH_CLIP_STATE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_hdr_metadata
VkHdrMetadataEXT make_VkHdrMetadataEXT() { return VkHdrMetadataEXT { VK_STRUCTURE_TYPE_HDR_METADATA_EXT }; }
#endif

#if VK_EXT_debug_utils
VkDebugUtilsLabelEXT make_VkDebugUtilsLabelEXT() { return VkDebugUtilsLabelEXT { VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT }; }
VkDebugUtilsObjectNameInfoEXT make_VkDebugUtilsObjectNameInfoEXT() { return VkDebugUtilsObjectNameInfoEXT { VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT }; }
VkDebugUtilsMessengerCallbackDataEXT make_VkDebugUtilsMessengerCallbackDataEXT() { return VkDebugUtilsMessengerCallbackDataEXT { VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT }; }
VkDebugUtilsMessengerCreateInfoEXT make_VkDebugUtilsMessengerCreateInfoEXT() { return VkDebugUtilsMessengerCreateInfoEXT { VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT }; }
VkDebugUtilsObjectTagInfoEXT make_VkDebugUtilsObjectTagInfoEXT() { return VkDebugUtilsObjectTagInfoEXT { VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT }; }
#endif

#if VK_EXT_inline_uniform_block
VkPhysicalDeviceInlineUniformBlockFeaturesEXT make_VkPhysicalDeviceInlineUniformBlockFeaturesEXT() { return VkPhysicalDeviceInlineUniformBlockFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT }; }
VkPhysicalDeviceInlineUniformBlockPropertiesEXT make_VkPhysicalDeviceInlineUniformBlockPropertiesEXT() { return VkPhysicalDeviceInlineUniformBlockPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_PROPERTIES_EXT }; }
VkWriteDescriptorSetInlineUniformBlockEXT make_VkWriteDescriptorSetInlineUniformBlockEXT() { return VkWriteDescriptorSetInlineUniformBlockEXT { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK_EXT }; }
VkDescriptorPoolInlineUniformBlockCreateInfoEXT make_VkDescriptorPoolInlineUniformBlockCreateInfoEXT() { return VkDescriptorPoolInlineUniformBlockCreateInfoEXT { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_INLINE_UNIFORM_BLOCK_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_sample_locations
VkSampleLocationsInfoEXT make_VkSampleLocationsInfoEXT() { return VkSampleLocationsInfoEXT { VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT }; }
VkRenderPassSampleLocationsBeginInfoEXT make_VkRenderPassSampleLocationsBeginInfoEXT() { return VkRenderPassSampleLocationsBeginInfoEXT { VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT }; }
VkPipelineSampleLocationsStateCreateInfoEXT make_VkPipelineSampleLocationsStateCreateInfoEXT() { return VkPipelineSampleLocationsStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT }; }
VkPhysicalDeviceSampleLocationsPropertiesEXT make_VkPhysicalDeviceSampleLocationsPropertiesEXT() { return VkPhysicalDeviceSampleLocationsPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT }; }
VkMultisamplePropertiesEXT make_VkMultisamplePropertiesEXT() { return VkMultisamplePropertiesEXT { VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT }; }
#endif

#if VK_EXT_blend_operation_advanced
VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT make_VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT() { return VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT }; }
VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT make_VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT() { return VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT }; }
VkPipelineColorBlendAdvancedStateCreateInfoEXT make_VkPipelineColorBlendAdvancedStateCreateInfoEXT() { return VkPipelineColorBlendAdvancedStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT }; }
#endif

#if VK_NV_fragment_coverage_to_color
VkPipelineCoverageToColorStateCreateInfoNV make_VkPipelineCoverageToColorStateCreateInfoNV() { return VkPipelineCoverageToColorStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV }; }
#endif

#if VK_NV_framebuffer_mixed_samples
VkPipelineCoverageModulationStateCreateInfoNV make_VkPipelineCoverageModulationStateCreateInfoNV() { return VkPipelineCoverageModulationStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV }; }
#endif

#if VK_NV_shader_sm_builtins
VkPhysicalDeviceShaderSMBuiltinsPropertiesNV make_VkPhysicalDeviceShaderSMBuiltinsPropertiesNV() { return VkPhysicalDeviceShaderSMBuiltinsPropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV }; }
VkPhysicalDeviceShaderSMBuiltinsFeaturesNV make_VkPhysicalDeviceShaderSMBuiltinsFeaturesNV() { return VkPhysicalDeviceShaderSMBuiltinsFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV }; }
#endif

#if VK_EXT_image_drm_format_modifier
VkDrmFormatModifierPropertiesListEXT make_VkDrmFormatModifierPropertiesListEXT() { return VkDrmFormatModifierPropertiesListEXT { VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT }; }
VkPhysicalDeviceImageDrmFormatModifierInfoEXT make_VkPhysicalDeviceImageDrmFormatModifierInfoEXT() { return VkPhysicalDeviceImageDrmFormatModifierInfoEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT }; }
VkImageDrmFormatModifierListCreateInfoEXT make_VkImageDrmFormatModifierListCreateInfoEXT() { return VkImageDrmFormatModifierListCreateInfoEXT { VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT }; }
VkImageDrmFormatModifierExplicitCreateInfoEXT make_VkImageDrmFormatModifierExplicitCreateInfoEXT() { return VkImageDrmFormatModifierExplicitCreateInfoEXT { VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT }; }
VkImageDrmFormatModifierPropertiesEXT make_VkImageDrmFormatModifierPropertiesEXT() { return VkImageDrmFormatModifierPropertiesEXT { VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT }; }
VkDrmFormatModifierPropertiesList2EXT make_VkDrmFormatModifierPropertiesList2EXT() { return VkDrmFormatModifierPropertiesList2EXT { VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_2_EXT }; }
#endif

#if VK_EXT_validation_cache
VkValidationCacheCreateInfoEXT make_VkValidationCacheCreateInfoEXT() { return VkValidationCacheCreateInfoEXT { VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT }; }
VkShaderModuleValidationCacheCreateInfoEXT make_VkShaderModuleValidationCacheCreateInfoEXT() { return VkShaderModuleValidationCacheCreateInfoEXT { VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT }; }
#endif

#if VK_NV_shading_rate_image
VkPipelineViewportShadingRateImageStateCreateInfoNV make_VkPipelineViewportShadingRateImageStateCreateInfoNV() { return VkPipelineViewportShadingRateImageStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV }; }
VkPhysicalDeviceShadingRateImageFeaturesNV make_VkPhysicalDeviceShadingRateImageFeaturesNV() { return VkPhysicalDeviceShadingRateImageFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV }; }
VkPhysicalDeviceShadingRateImagePropertiesNV make_VkPhysicalDeviceShadingRateImagePropertiesNV() { return VkPhysicalDeviceShadingRateImagePropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV }; }
VkPipelineViewportCoarseSampleOrderStateCreateInfoNV make_VkPipelineViewportCoarseSampleOrderStateCreateInfoNV() { return VkPipelineViewportCoarseSampleOrderStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV }; }
#endif

#if VK_NV_ray_tracing
VkRayTracingShaderGroupCreateInfoNV make_VkRayTracingShaderGroupCreateInfoNV() { return VkRayTracingShaderGroupCreateInfoNV { VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV }; }
VkRayTracingPipelineCreateInfoNV make_VkRayTracingPipelineCreateInfoNV() { return VkRayTracingPipelineCreateInfoNV { VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV }; }
VkGeometryTrianglesNV make_VkGeometryTrianglesNV() { return VkGeometryTrianglesNV { VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV }; }
VkGeometryAABBNV make_VkGeometryAABBNV() { return VkGeometryAABBNV { VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV }; }
VkGeometryNV make_VkGeometryNV() { return VkGeometryNV { VK_STRUCTURE_TYPE_GEOMETRY_NV }; }
VkAccelerationStructureInfoNV make_VkAccelerationStructureInfoNV() { return VkAccelerationStructureInfoNV { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV }; }
VkAccelerationStructureCreateInfoNV make_VkAccelerationStructureCreateInfoNV() { return VkAccelerationStructureCreateInfoNV { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV }; }
VkBindAccelerationStructureMemoryInfoNV make_VkBindAccelerationStructureMemoryInfoNV() { return VkBindAccelerationStructureMemoryInfoNV { VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV }; }
VkWriteDescriptorSetAccelerationStructureNV make_VkWriteDescriptorSetAccelerationStructureNV() { return VkWriteDescriptorSetAccelerationStructureNV { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV }; }
VkAccelerationStructureMemoryRequirementsInfoNV make_VkAccelerationStructureMemoryRequirementsInfoNV() { return VkAccelerationStructureMemoryRequirementsInfoNV { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV }; }
VkPhysicalDeviceRayTracingPropertiesNV make_VkPhysicalDeviceRayTracingPropertiesNV() { return VkPhysicalDeviceRayTracingPropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV }; }
#endif

#if VK_NV_representative_fragment_test
VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV make_VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV() { return VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV }; }
VkPipelineRepresentativeFragmentTestStateCreateInfoNV make_VkPipelineRepresentativeFragmentTestStateCreateInfoNV() { return VkPipelineRepresentativeFragmentTestStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_REPRESENTATIVE_FRAGMENT_TEST_STATE_CREATE_INFO_NV }; }
#endif

#if VK_EXT_filter_cubic
VkPhysicalDeviceImageViewImageFormatInfoEXT make_VkPhysicalDeviceImageViewImageFormatInfoEXT() { return VkPhysicalDeviceImageViewImageFormatInfoEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT }; }
VkFilterCubicImageViewImageFormatPropertiesEXT make_VkFilterCubicImageViewImageFormatPropertiesEXT() { return VkFilterCubicImageViewImageFormatPropertiesEXT { VK_STRUCTURE_TYPE_FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT }; }
#endif

#if VK_EXT_global_priority
VkDeviceQueueGlobalPriorityCreateInfoEXT make_VkDeviceQueueGlobalPriorityCreateInfoEXT() { return VkDeviceQueueGlobalPriorityCreateInfoEXT { VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_external_memory_host
VkImportMemoryHostPointerInfoEXT make_VkImportMemoryHostPointerInfoEXT() { return VkImportMemoryHostPointerInfoEXT { VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT }; }
VkMemoryHostPointerPropertiesEXT make_VkMemoryHostPointerPropertiesEXT() { return VkMemoryHostPointerPropertiesEXT { VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT }; }
VkPhysicalDeviceExternalMemoryHostPropertiesEXT make_VkPhysicalDeviceExternalMemoryHostPropertiesEXT() { return VkPhysicalDeviceExternalMemoryHostPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT }; }
#endif

#if VK_AMD_pipeline_compiler_control
VkPipelineCompilerControlCreateInfoAMD make_VkPipelineCompilerControlCreateInfoAMD() { return VkPipelineCompilerControlCreateInfoAMD { VK_STRUCTURE_TYPE_PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD }; }
#endif

#if VK_EXT_calibrated_timestamps
VkCalibratedTimestampInfoEXT make_VkCalibratedTimestampInfoEXT() { return VkCalibratedTimestampInfoEXT { VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT }; }
#endif

#if VK_AMD_shader_core_properties
VkPhysicalDeviceShaderCorePropertiesAMD make_VkPhysicalDeviceShaderCorePropertiesAMD() { return VkPhysicalDeviceShaderCorePropertiesAMD { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_AMD }; }
#endif

#if VK_AMD_memory_overallocation_behavior
VkDeviceMemoryOverallocationCreateInfoAMD make_VkDeviceMemoryOverallocationCreateInfoAMD() { return VkDeviceMemoryOverallocationCreateInfoAMD { VK_STRUCTURE_TYPE_DEVICE_MEMORY_OVERALLOCATION_CREATE_INFO_AMD }; }
#endif

#if VK_EXT_vertex_attribute_divisor
VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT make_VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT() { return VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT }; }
VkPipelineVertexInputDivisorStateCreateInfoEXT make_VkPipelineVertexInputDivisorStateCreateInfoEXT() { return VkPipelineVertexInputDivisorStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT }; }
VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT make_VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT() { return VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT }; }
#endif

#if VK_EXT_pipeline_creation_feedback
VkPipelineCreationFeedbackCreateInfoEXT make_VkPipelineCreationFeedbackCreateInfoEXT() { return VkPipelineCreationFeedbackCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT }; }
#endif

#if VK_NV_compute_shader_derivatives
VkPhysicalDeviceComputeShaderDerivativesFeaturesNV make_VkPhysicalDeviceComputeShaderDerivativesFeaturesNV() { return VkPhysicalDeviceComputeShaderDerivativesFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV }; }
#endif

#if VK_NV_mesh_shader
VkPhysicalDeviceMeshShaderFeaturesNV make_VkPhysicalDeviceMeshShaderFeaturesNV() { return VkPhysicalDeviceMeshShaderFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV }; }
VkPhysicalDeviceMeshShaderPropertiesNV make_VkPhysicalDeviceMeshShaderPropertiesNV() { return VkPhysicalDeviceMeshShaderPropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV }; }
#endif

#if VK_NV_fragment_shader_barycentric
VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV make_VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV() { return VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV }; }
#endif

#if VK_NV_shader_image_footprint
VkPhysicalDeviceShaderImageFootprintFeaturesNV make_VkPhysicalDeviceShaderImageFootprintFeaturesNV() { return VkPhysicalDeviceShaderImageFootprintFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV }; }
#endif

#if VK_NV_scissor_exclusive
VkPipelineViewportExclusiveScissorStateCreateInfoNV make_VkPipelineViewportExclusiveScissorStateCreateInfoNV() { return VkPipelineViewportExclusiveScissorStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV }; }
VkPhysicalDeviceExclusiveScissorFeaturesNV make_VkPhysicalDeviceExclusiveScissorFeaturesNV() { return VkPhysicalDeviceExclusiveScissorFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV }; }
#endif

#if VK_NV_device_diagnostic_checkpoints
VkQueueFamilyCheckpointPropertiesNV make_VkQueueFamilyCheckpointPropertiesNV() { return VkQueueFamilyCheckpointPropertiesNV { VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV }; }
VkCheckpointDataNV make_VkCheckpointDataNV() { return VkCheckpointDataNV { VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV }; }
#endif

#if VK_INTEL_shader_integer_functions2
VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL make_VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL() { return VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL }; }
#endif

#if VK_INTEL_performance_query
VkInitializePerformanceApiInfoINTEL make_VkInitializePerformanceApiInfoINTEL() { return VkInitializePerformanceApiInfoINTEL { VK_STRUCTURE_TYPE_INITIALIZE_PERFORMANCE_API_INFO_INTEL }; }
VkQueryPoolPerformanceQueryCreateInfoINTEL make_VkQueryPoolPerformanceQueryCreateInfoINTEL() { return VkQueryPoolPerformanceQueryCreateInfoINTEL { VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_QUERY_CREATE_INFO_INTEL }; }
VkPerformanceMarkerInfoINTEL make_VkPerformanceMarkerInfoINTEL() { return VkPerformanceMarkerInfoINTEL { VK_STRUCTURE_TYPE_PERFORMANCE_MARKER_INFO_INTEL }; }
VkPerformanceStreamMarkerInfoINTEL make_VkPerformanceStreamMarkerInfoINTEL() { return VkPerformanceStreamMarkerInfoINTEL { VK_STRUCTURE_TYPE_PERFORMANCE_STREAM_MARKER_INFO_INTEL }; }
VkPerformanceOverrideInfoINTEL make_VkPerformanceOverrideInfoINTEL() { return VkPerformanceOverrideInfoINTEL { VK_STRUCTURE_TYPE_PERFORMANCE_OVERRIDE_INFO_INTEL }; }
VkPerformanceConfigurationAcquireInfoINTEL make_VkPerformanceConfigurationAcquireInfoINTEL() { return VkPerformanceConfigurationAcquireInfoINTEL { VK_STRUCTURE_TYPE_PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL }; }
#endif

#if VK_EXT_pci_bus_info
VkPhysicalDevicePCIBusInfoPropertiesEXT make_VkPhysicalDevicePCIBusInfoPropertiesEXT() { return VkPhysicalDevicePCIBusInfoPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT }; }
#endif

#if VK_AMD_display_native_hdr
VkDisplayNativeHdrSurfaceCapabilitiesAMD make_VkDisplayNativeHdrSurfaceCapabilitiesAMD() { return VkDisplayNativeHdrSurfaceCapabilitiesAMD { VK_STRUCTURE_TYPE_DISPLAY_NATIVE_HDR_SURFACE_CAPABILITIES_AMD }; }
VkSwapchainDisplayNativeHdrCreateInfoAMD make_VkSwapchainDisplayNativeHdrCreateInfoAMD() { return VkSwapchainDisplayNativeHdrCreateInfoAMD { VK_STRUCTURE_TYPE_SWAPCHAIN_DISPLAY_NATIVE_HDR_CREATE_INFO_AMD }; }
#endif

#if VK_EXT_fragment_density_map
VkPhysicalDeviceFragmentDensityMapFeaturesEXT make_VkPhysicalDeviceFragmentDensityMapFeaturesEXT() { return VkPhysicalDeviceFragmentDensityMapFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT }; }
VkPhysicalDeviceFragmentDensityMapPropertiesEXT make_VkPhysicalDeviceFragmentDensityMapPropertiesEXT() { return VkPhysicalDeviceFragmentDensityMapPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_PROPERTIES_EXT }; }
VkRenderPassFragmentDensityMapCreateInfoEXT make_VkRenderPassFragmentDensityMapCreateInfoEXT() { return VkRenderPassFragmentDensityMapCreateInfoEXT { VK_STRUCTURE_TYPE_RENDER_PASS_FRAGMENT_DENSITY_MAP_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_subgroup_size_control
VkPhysicalDeviceSubgroupSizeControlFeaturesEXT make_VkPhysicalDeviceSubgroupSizeControlFeaturesEXT() { return VkPhysicalDeviceSubgroupSizeControlFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT }; }
VkPhysicalDeviceSubgroupSizeControlPropertiesEXT make_VkPhysicalDeviceSubgroupSizeControlPropertiesEXT() { return VkPhysicalDeviceSubgroupSizeControlPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES_EXT }; }
VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT make_VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT() { return VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO_EXT }; }
#endif

#if VK_AMD_shader_core_properties2
VkPhysicalDeviceShaderCoreProperties2AMD make_VkPhysicalDeviceShaderCoreProperties2AMD() { return VkPhysicalDeviceShaderCoreProperties2AMD { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_2_AMD }; }
#endif

#if VK_AMD_device_coherent_memory
VkPhysicalDeviceCoherentMemoryFeaturesAMD make_VkPhysicalDeviceCoherentMemoryFeaturesAMD() { return VkPhysicalDeviceCoherentMemoryFeaturesAMD { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD }; }
#endif

#if VK_EXT_shader_image_atomic_int64
VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT make_VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT() { return VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_ATOMIC_INT64_FEATURES_EXT }; }
#endif

#if VK_EXT_memory_budget
VkPhysicalDeviceMemoryBudgetPropertiesEXT make_VkPhysicalDeviceMemoryBudgetPropertiesEXT() { return VkPhysicalDeviceMemoryBudgetPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT }; }
#endif

#if VK_EXT_memory_priority
VkPhysicalDeviceMemoryPriorityFeaturesEXT make_VkPhysicalDeviceMemoryPriorityFeaturesEXT() { return VkPhysicalDeviceMemoryPriorityFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT }; }
VkMemoryPriorityAllocateInfoEXT make_VkMemoryPriorityAllocateInfoEXT() { return VkMemoryPriorityAllocateInfoEXT { VK_STRUCTURE_TYPE_MEMORY_PRIORITY_ALLOCATE_INFO_EXT }; }
#endif

#if VK_NV_dedicated_allocation_image_aliasing
VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV make_VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV() { return VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV }; }
#endif

#if VK_EXT_buffer_device_address
VkPhysicalDeviceBufferDeviceAddressFeaturesEXT make_VkPhysicalDeviceBufferDeviceAddressFeaturesEXT() { return VkPhysicalDeviceBufferDeviceAddressFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT }; }
VkBufferDeviceAddressCreateInfoEXT make_VkBufferDeviceAddressCreateInfoEXT() { return VkBufferDeviceAddressCreateInfoEXT { VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_tooling_info
VkPhysicalDeviceToolPropertiesEXT make_VkPhysicalDeviceToolPropertiesEXT() { return VkPhysicalDeviceToolPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT }; }
#endif

#if VK_EXT_validation_features
VkValidationFeaturesEXT make_VkValidationFeaturesEXT() { return VkValidationFeaturesEXT { VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT }; }
#endif

#if VK_NV_cooperative_matrix
VkCooperativeMatrixPropertiesNV make_VkCooperativeMatrixPropertiesNV() { return VkCooperativeMatrixPropertiesNV { VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_NV }; }
VkPhysicalDeviceCooperativeMatrixFeaturesNV make_VkPhysicalDeviceCooperativeMatrixFeaturesNV() { return VkPhysicalDeviceCooperativeMatrixFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV }; }
VkPhysicalDeviceCooperativeMatrixPropertiesNV make_VkPhysicalDeviceCooperativeMatrixPropertiesNV() { return VkPhysicalDeviceCooperativeMatrixPropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_NV }; }
#endif

#if VK_NV_coverage_reduction_mode
VkPhysicalDeviceCoverageReductionModeFeaturesNV make_VkPhysicalDeviceCoverageReductionModeFeaturesNV() { return VkPhysicalDeviceCoverageReductionModeFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV }; }
VkPipelineCoverageReductionStateCreateInfoNV make_VkPipelineCoverageReductionStateCreateInfoNV() { return VkPipelineCoverageReductionStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV }; }
VkFramebufferMixedSamplesCombinationNV make_VkFramebufferMixedSamplesCombinationNV() { return VkFramebufferMixedSamplesCombinationNV { VK_STRUCTURE_TYPE_FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV }; }
#endif

#if VK_EXT_fragment_shader_interlock
VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT make_VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT() { return VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT }; }
#endif

#if VK_EXT_ycbcr_image_arrays
VkPhysicalDeviceYcbcrImageArraysFeaturesEXT make_VkPhysicalDeviceYcbcrImageArraysFeaturesEXT() { return VkPhysicalDeviceYcbcrImageArraysFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT }; }
#endif

#if VK_EXT_provoking_vertex
VkPhysicalDeviceProvokingVertexFeaturesEXT make_VkPhysicalDeviceProvokingVertexFeaturesEXT() { return VkPhysicalDeviceProvokingVertexFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_FEATURES_EXT }; }
VkPhysicalDeviceProvokingVertexPropertiesEXT make_VkPhysicalDeviceProvokingVertexPropertiesEXT() { return VkPhysicalDeviceProvokingVertexPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROVOKING_VERTEX_PROPERTIES_EXT }; }
VkPipelineRasterizationProvokingVertexStateCreateInfoEXT make_VkPipelineRasterizationProvokingVertexStateCreateInfoEXT() { return VkPipelineRasterizationProvokingVertexStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_PROVOKING_VERTEX_STATE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_headless_surface
VkHeadlessSurfaceCreateInfoEXT make_VkHeadlessSurfaceCreateInfoEXT() { return VkHeadlessSurfaceCreateInfoEXT { VK_STRUCTURE_TYPE_HEADLESS_SURFACE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_line_rasterization
VkPhysicalDeviceLineRasterizationFeaturesEXT make_VkPhysicalDeviceLineRasterizationFeaturesEXT() { return VkPhysicalDeviceLineRasterizationFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT }; }
VkPhysicalDeviceLineRasterizationPropertiesEXT make_VkPhysicalDeviceLineRasterizationPropertiesEXT() { return VkPhysicalDeviceLineRasterizationPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT }; }
VkPipelineRasterizationLineStateCreateInfoEXT make_VkPipelineRasterizationLineStateCreateInfoEXT() { return VkPipelineRasterizationLineStateCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_shader_atomic_float
VkPhysicalDeviceShaderAtomicFloatFeaturesEXT make_VkPhysicalDeviceShaderAtomicFloatFeaturesEXT() { return VkPhysicalDeviceShaderAtomicFloatFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT }; }
#endif

#if VK_EXT_index_type_uint8
VkPhysicalDeviceIndexTypeUint8FeaturesEXT make_VkPhysicalDeviceIndexTypeUint8FeaturesEXT() { return VkPhysicalDeviceIndexTypeUint8FeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT }; }
#endif

#if VK_EXT_extended_dynamic_state
VkPhysicalDeviceExtendedDynamicStateFeaturesEXT make_VkPhysicalDeviceExtendedDynamicStateFeaturesEXT() { return VkPhysicalDeviceExtendedDynamicStateFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT }; }
#endif

#if VK_EXT_shader_atomic_float2
VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT make_VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT() { return VkPhysicalDeviceShaderAtomicFloat2FeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_2_FEATURES_EXT }; }
#endif

#if VK_EXT_shader_demote_to_helper_invocation
VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT make_VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT() { return VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT }; }
#endif

#if VK_NV_device_generated_commands
VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV make_VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV() { return VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_PROPERTIES_NV }; }
VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV make_VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV() { return VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_GENERATED_COMMANDS_FEATURES_NV }; }
VkGraphicsShaderGroupCreateInfoNV make_VkGraphicsShaderGroupCreateInfoNV() { return VkGraphicsShaderGroupCreateInfoNV { VK_STRUCTURE_TYPE_GRAPHICS_SHADER_GROUP_CREATE_INFO_NV }; }
VkGraphicsPipelineShaderGroupsCreateInfoNV make_VkGraphicsPipelineShaderGroupsCreateInfoNV() { return VkGraphicsPipelineShaderGroupsCreateInfoNV { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_SHADER_GROUPS_CREATE_INFO_NV }; }
VkIndirectCommandsLayoutTokenNV make_VkIndirectCommandsLayoutTokenNV() { return VkIndirectCommandsLayoutTokenNV { VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_TOKEN_NV }; }
VkIndirectCommandsLayoutCreateInfoNV make_VkIndirectCommandsLayoutCreateInfoNV() { return VkIndirectCommandsLayoutCreateInfoNV { VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NV }; }
VkGeneratedCommandsInfoNV make_VkGeneratedCommandsInfoNV() { return VkGeneratedCommandsInfoNV { VK_STRUCTURE_TYPE_GENERATED_COMMANDS_INFO_NV }; }
VkGeneratedCommandsMemoryRequirementsInfoNV make_VkGeneratedCommandsMemoryRequirementsInfoNV() { return VkGeneratedCommandsMemoryRequirementsInfoNV { VK_STRUCTURE_TYPE_GENERATED_COMMANDS_MEMORY_REQUIREMENTS_INFO_NV }; }
#endif

#if VK_NV_inherited_viewport_scissor
VkPhysicalDeviceInheritedViewportScissorFeaturesNV make_VkPhysicalDeviceInheritedViewportScissorFeaturesNV() { return VkPhysicalDeviceInheritedViewportScissorFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INHERITED_VIEWPORT_SCISSOR_FEATURES_NV }; }
VkCommandBufferInheritanceViewportScissorInfoNV make_VkCommandBufferInheritanceViewportScissorInfoNV() { return VkCommandBufferInheritanceViewportScissorInfoNV { VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_VIEWPORT_SCISSOR_INFO_NV }; }
#endif

#if VK_EXT_texel_buffer_alignment
VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT make_VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT() { return VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT }; }
VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT make_VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT() { return VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_PROPERTIES_EXT }; }
#endif

#if VK_QCOM_render_pass_transform
VkRenderPassTransformBeginInfoQCOM make_VkRenderPassTransformBeginInfoQCOM() { return VkRenderPassTransformBeginInfoQCOM { VK_STRUCTURE_TYPE_RENDER_PASS_TRANSFORM_BEGIN_INFO_QCOM }; }
VkCommandBufferInheritanceRenderPassTransformInfoQCOM make_VkCommandBufferInheritanceRenderPassTransformInfoQCOM() { return VkCommandBufferInheritanceRenderPassTransformInfoQCOM { VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDER_PASS_TRANSFORM_INFO_QCOM }; }
#endif

#if VK_EXT_device_memory_report
VkPhysicalDeviceDeviceMemoryReportFeaturesEXT make_VkPhysicalDeviceDeviceMemoryReportFeaturesEXT() { return VkPhysicalDeviceDeviceMemoryReportFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEVICE_MEMORY_REPORT_FEATURES_EXT }; }
VkDeviceMemoryReportCallbackDataEXT make_VkDeviceMemoryReportCallbackDataEXT() { return VkDeviceMemoryReportCallbackDataEXT { VK_STRUCTURE_TYPE_DEVICE_MEMORY_REPORT_CALLBACK_DATA_EXT }; }
VkDeviceDeviceMemoryReportCreateInfoEXT make_VkDeviceDeviceMemoryReportCreateInfoEXT() { return VkDeviceDeviceMemoryReportCreateInfoEXT { VK_STRUCTURE_TYPE_DEVICE_DEVICE_MEMORY_REPORT_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_robustness2
VkPhysicalDeviceRobustness2FeaturesEXT make_VkPhysicalDeviceRobustness2FeaturesEXT() { return VkPhysicalDeviceRobustness2FeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT }; }
VkPhysicalDeviceRobustness2PropertiesEXT make_VkPhysicalDeviceRobustness2PropertiesEXT() { return VkPhysicalDeviceRobustness2PropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_PROPERTIES_EXT }; }
#endif

#if VK_EXT_custom_border_color
VkSamplerCustomBorderColorCreateInfoEXT make_VkSamplerCustomBorderColorCreateInfoEXT() { return VkSamplerCustomBorderColorCreateInfoEXT { VK_STRUCTURE_TYPE_SAMPLER_CUSTOM_BORDER_COLOR_CREATE_INFO_EXT }; }
VkPhysicalDeviceCustomBorderColorPropertiesEXT make_VkPhysicalDeviceCustomBorderColorPropertiesEXT() { return VkPhysicalDeviceCustomBorderColorPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_PROPERTIES_EXT }; }
VkPhysicalDeviceCustomBorderColorFeaturesEXT make_VkPhysicalDeviceCustomBorderColorFeaturesEXT() { return VkPhysicalDeviceCustomBorderColorFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CUSTOM_BORDER_COLOR_FEATURES_EXT }; }
#endif

#if VK_EXT_private_data
VkPhysicalDevicePrivateDataFeaturesEXT make_VkPhysicalDevicePrivateDataFeaturesEXT() { return VkPhysicalDevicePrivateDataFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIVATE_DATA_FEATURES_EXT }; }
VkDevicePrivateDataCreateInfoEXT make_VkDevicePrivateDataCreateInfoEXT() { return VkDevicePrivateDataCreateInfoEXT { VK_STRUCTURE_TYPE_DEVICE_PRIVATE_DATA_CREATE_INFO_EXT }; }
VkPrivateDataSlotCreateInfoEXT make_VkPrivateDataSlotCreateInfoEXT() { return VkPrivateDataSlotCreateInfoEXT { VK_STRUCTURE_TYPE_PRIVATE_DATA_SLOT_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_pipeline_creation_cache_control
VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT make_VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT() { return VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_CREATION_CACHE_CONTROL_FEATURES_EXT }; }
#endif

#if VK_NV_device_diagnostics_config
VkPhysicalDeviceDiagnosticsConfigFeaturesNV make_VkPhysicalDeviceDiagnosticsConfigFeaturesNV() { return VkPhysicalDeviceDiagnosticsConfigFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DIAGNOSTICS_CONFIG_FEATURES_NV }; }
VkDeviceDiagnosticsConfigCreateInfoNV make_VkDeviceDiagnosticsConfigCreateInfoNV() { return VkDeviceDiagnosticsConfigCreateInfoNV { VK_STRUCTURE_TYPE_DEVICE_DIAGNOSTICS_CONFIG_CREATE_INFO_NV }; }
#endif

#if VK_NV_fragment_shading_rate_enums
VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV make_VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV() { return VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_FEATURES_NV }; }
VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV make_VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV() { return VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADING_RATE_ENUMS_PROPERTIES_NV }; }
VkPipelineFragmentShadingRateEnumStateCreateInfoNV make_VkPipelineFragmentShadingRateEnumStateCreateInfoNV() { return VkPipelineFragmentShadingRateEnumStateCreateInfoNV { VK_STRUCTURE_TYPE_PIPELINE_FRAGMENT_SHADING_RATE_ENUM_STATE_CREATE_INFO_NV }; }
#endif

#if VK_NV_ray_tracing_motion_blur
VkAccelerationStructureGeometryMotionTrianglesDataNV make_VkAccelerationStructureGeometryMotionTrianglesDataNV() { return VkAccelerationStructureGeometryMotionTrianglesDataNV { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_MOTION_TRIANGLES_DATA_NV }; }
VkAccelerationStructureMotionInfoNV make_VkAccelerationStructureMotionInfoNV() { return VkAccelerationStructureMotionInfoNV { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MOTION_INFO_NV }; }
VkPhysicalDeviceRayTracingMotionBlurFeaturesNV make_VkPhysicalDeviceRayTracingMotionBlurFeaturesNV() { return VkPhysicalDeviceRayTracingMotionBlurFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_MOTION_BLUR_FEATURES_NV }; }
#endif

#if VK_EXT_ycbcr_2plane_444_formats
VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT make_VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT() { return VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_2_PLANE_444_FORMATS_FEATURES_EXT }; }
#endif

#if VK_EXT_fragment_density_map2
VkPhysicalDeviceFragmentDensityMap2FeaturesEXT make_VkPhysicalDeviceFragmentDensityMap2FeaturesEXT() { return VkPhysicalDeviceFragmentDensityMap2FeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_FEATURES_EXT }; }
VkPhysicalDeviceFragmentDensityMap2PropertiesEXT make_VkPhysicalDeviceFragmentDensityMap2PropertiesEXT() { return VkPhysicalDeviceFragmentDensityMap2PropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_2_PROPERTIES_EXT }; }
#endif

#if VK_QCOM_rotated_copy_commands
VkCopyCommandTransformInfoQCOM make_VkCopyCommandTransformInfoQCOM() { return VkCopyCommandTransformInfoQCOM { VK_STRUCTURE_TYPE_COPY_COMMAND_TRANSFORM_INFO_QCOM }; }
#endif

#if VK_EXT_image_robustness
VkPhysicalDeviceImageRobustnessFeaturesEXT make_VkPhysicalDeviceImageRobustnessFeaturesEXT() { return VkPhysicalDeviceImageRobustnessFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_ROBUSTNESS_FEATURES_EXT }; }
#endif

#if VK_EXT_4444_formats
VkPhysicalDevice4444FormatsFeaturesEXT make_VkPhysicalDevice4444FormatsFeaturesEXT() { return VkPhysicalDevice4444FormatsFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT }; }
#endif

#if VK_EXT_rgba10x6_formats
VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT make_VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT() { return VkPhysicalDeviceRGBA10X6FormatsFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RGBA10X6_FORMATS_FEATURES_EXT }; }
#endif

#if VK_VALVE_mutable_descriptor_type
VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE make_VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE() { return VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MUTABLE_DESCRIPTOR_TYPE_FEATURES_VALVE }; }
VkMutableDescriptorTypeCreateInfoVALVE make_VkMutableDescriptorTypeCreateInfoVALVE() { return VkMutableDescriptorTypeCreateInfoVALVE { VK_STRUCTURE_TYPE_MUTABLE_DESCRIPTOR_TYPE_CREATE_INFO_VALVE }; }
#endif

#if VK_EXT_vertex_input_dynamic_state
VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT make_VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT() { return VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT }; }
VkVertexInputBindingDescription2EXT make_VkVertexInputBindingDescription2EXT() { return VkVertexInputBindingDescription2EXT { VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT }; }
VkVertexInputAttributeDescription2EXT make_VkVertexInputAttributeDescription2EXT() { return VkVertexInputAttributeDescription2EXT { VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT }; }
#endif

#if VK_EXT_physical_device_drm
VkPhysicalDeviceDrmPropertiesEXT make_VkPhysicalDeviceDrmPropertiesEXT() { return VkPhysicalDeviceDrmPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRM_PROPERTIES_EXT }; }
#endif

#if VK_EXT_primitive_topology_list_restart
VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT make_VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT() { return VkPhysicalDevicePrimitiveTopologyListRestartFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRIMITIVE_TOPOLOGY_LIST_RESTART_FEATURES_EXT }; }
#endif

#if VK_HUAWEI_subpass_shading
VkSubpassShadingPipelineCreateInfoHUAWEI make_VkSubpassShadingPipelineCreateInfoHUAWEI() { return VkSubpassShadingPipelineCreateInfoHUAWEI { VK_STRUCTURE_TYPE_SUBPASS_SHADING_PIPELINE_CREATE_INFO_HUAWEI }; }
VkPhysicalDeviceSubpassShadingFeaturesHUAWEI make_VkPhysicalDeviceSubpassShadingFeaturesHUAWEI() { return VkPhysicalDeviceSubpassShadingFeaturesHUAWEI { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_FEATURES_HUAWEI }; }
VkPhysicalDeviceSubpassShadingPropertiesHUAWEI make_VkPhysicalDeviceSubpassShadingPropertiesHUAWEI() { return VkPhysicalDeviceSubpassShadingPropertiesHUAWEI { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBPASS_SHADING_PROPERTIES_HUAWEI }; }
#endif

#if VK_HUAWEI_invocation_mask
VkPhysicalDeviceInvocationMaskFeaturesHUAWEI make_VkPhysicalDeviceInvocationMaskFeaturesHUAWEI() { return VkPhysicalDeviceInvocationMaskFeaturesHUAWEI { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INVOCATION_MASK_FEATURES_HUAWEI }; }
#endif

#if VK_NV_external_memory_rdma
VkMemoryGetRemoteAddressInfoNV make_VkMemoryGetRemoteAddressInfoNV() { return VkMemoryGetRemoteAddressInfoNV { VK_STRUCTURE_TYPE_MEMORY_GET_REMOTE_ADDRESS_INFO_NV }; }
VkPhysicalDeviceExternalMemoryRDMAFeaturesNV make_VkPhysicalDeviceExternalMemoryRDMAFeaturesNV() { return VkPhysicalDeviceExternalMemoryRDMAFeaturesNV { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_RDMA_FEATURES_NV }; }
#endif

#if VK_EXT_extended_dynamic_state2
VkPhysicalDeviceExtendedDynamicState2FeaturesEXT make_VkPhysicalDeviceExtendedDynamicState2FeaturesEXT() { return VkPhysicalDeviceExtendedDynamicState2FeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_2_FEATURES_EXT }; }
#endif

#if VK_EXT_color_write_enable
VkPhysicalDeviceColorWriteEnableFeaturesEXT make_VkPhysicalDeviceColorWriteEnableFeaturesEXT() { return VkPhysicalDeviceColorWriteEnableFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COLOR_WRITE_ENABLE_FEATURES_EXT }; }
VkPipelineColorWriteCreateInfoEXT make_VkPipelineColorWriteCreateInfoEXT() { return VkPipelineColorWriteCreateInfoEXT { VK_STRUCTURE_TYPE_PIPELINE_COLOR_WRITE_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_global_priority_query
VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT make_VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT() { return VkPhysicalDeviceGlobalPriorityQueryFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GLOBAL_PRIORITY_QUERY_FEATURES_EXT }; }
VkQueueFamilyGlobalPriorityPropertiesEXT make_VkQueueFamilyGlobalPriorityPropertiesEXT() { return VkQueueFamilyGlobalPriorityPropertiesEXT { VK_STRUCTURE_TYPE_QUEUE_FAMILY_GLOBAL_PRIORITY_PROPERTIES_EXT }; }
#endif

#if VK_EXT_multi_draw
VkPhysicalDeviceMultiDrawFeaturesEXT make_VkPhysicalDeviceMultiDrawFeaturesEXT() { return VkPhysicalDeviceMultiDrawFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_FEATURES_EXT }; }
VkPhysicalDeviceMultiDrawPropertiesEXT make_VkPhysicalDeviceMultiDrawPropertiesEXT() { return VkPhysicalDeviceMultiDrawPropertiesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTI_DRAW_PROPERTIES_EXT }; }
#endif

#if VK_EXT_border_color_swizzle
VkPhysicalDeviceBorderColorSwizzleFeaturesEXT make_VkPhysicalDeviceBorderColorSwizzleFeaturesEXT() { return VkPhysicalDeviceBorderColorSwizzleFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BORDER_COLOR_SWIZZLE_FEATURES_EXT }; }
VkSamplerBorderColorComponentMappingCreateInfoEXT make_VkSamplerBorderColorComponentMappingCreateInfoEXT() { return VkSamplerBorderColorComponentMappingCreateInfoEXT { VK_STRUCTURE_TYPE_SAMPLER_BORDER_COLOR_COMPONENT_MAPPING_CREATE_INFO_EXT }; }
#endif

#if VK_EXT_pageable_device_local_memory
VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT make_VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT() { return VkPhysicalDevicePageableDeviceLocalMemoryFeaturesEXT { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PAGEABLE_DEVICE_LOCAL_MEMORY_FEATURES_EXT }; }
#endif

#if VK_KHR_acceleration_structure
VkAccelerationStructureGeometryTrianglesDataKHR make_VkAccelerationStructureGeometryTrianglesDataKHR() { return VkAccelerationStructureGeometryTrianglesDataKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR }; }
VkAccelerationStructureGeometryAabbsDataKHR make_VkAccelerationStructureGeometryAabbsDataKHR() { return VkAccelerationStructureGeometryAabbsDataKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR }; }
VkAccelerationStructureGeometryInstancesDataKHR make_VkAccelerationStructureGeometryInstancesDataKHR() { return VkAccelerationStructureGeometryInstancesDataKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR }; }
VkAccelerationStructureGeometryKHR make_VkAccelerationStructureGeometryKHR() { return VkAccelerationStructureGeometryKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR }; }
VkAccelerationStructureBuildGeometryInfoKHR make_VkAccelerationStructureBuildGeometryInfoKHR() { return VkAccelerationStructureBuildGeometryInfoKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR }; }
VkAccelerationStructureCreateInfoKHR make_VkAccelerationStructureCreateInfoKHR() { return VkAccelerationStructureCreateInfoKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR }; }
VkWriteDescriptorSetAccelerationStructureKHR make_VkWriteDescriptorSetAccelerationStructureKHR() { return VkWriteDescriptorSetAccelerationStructureKHR { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR }; }
VkPhysicalDeviceAccelerationStructureFeaturesKHR make_VkPhysicalDeviceAccelerationStructureFeaturesKHR() { return VkPhysicalDeviceAccelerationStructureFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR }; }
VkPhysicalDeviceAccelerationStructurePropertiesKHR make_VkPhysicalDeviceAccelerationStructurePropertiesKHR() { return VkPhysicalDeviceAccelerationStructurePropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR }; }
VkAccelerationStructureDeviceAddressInfoKHR make_VkAccelerationStructureDeviceAddressInfoKHR() { return VkAccelerationStructureDeviceAddressInfoKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR }; }
VkAccelerationStructureVersionInfoKHR make_VkAccelerationStructureVersionInfoKHR() { return VkAccelerationStructureVersionInfoKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_VERSION_INFO_KHR }; }
VkCopyAccelerationStructureToMemoryInfoKHR make_VkCopyAccelerationStructureToMemoryInfoKHR() { return VkCopyAccelerationStructureToMemoryInfoKHR { VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_TO_MEMORY_INFO_KHR }; }
VkCopyMemoryToAccelerationStructureInfoKHR make_VkCopyMemoryToAccelerationStructureInfoKHR() { return VkCopyMemoryToAccelerationStructureInfoKHR { VK_STRUCTURE_TYPE_COPY_MEMORY_TO_ACCELERATION_STRUCTURE_INFO_KHR }; }
VkCopyAccelerationStructureInfoKHR make_VkCopyAccelerationStructureInfoKHR() { return VkCopyAccelerationStructureInfoKHR { VK_STRUCTURE_TYPE_COPY_ACCELERATION_STRUCTURE_INFO_KHR }; }
VkAccelerationStructureBuildSizesInfoKHR make_VkAccelerationStructureBuildSizesInfoKHR() { return VkAccelerationStructureBuildSizesInfoKHR { VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR }; }
#endif

#if VK_KHR_ray_tracing_pipeline
VkRayTracingShaderGroupCreateInfoKHR make_VkRayTracingShaderGroupCreateInfoKHR() { return VkRayTracingShaderGroupCreateInfoKHR { VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR }; }
VkRayTracingPipelineInterfaceCreateInfoKHR make_VkRayTracingPipelineInterfaceCreateInfoKHR() { return VkRayTracingPipelineInterfaceCreateInfoKHR { VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_INTERFACE_CREATE_INFO_KHR }; }
VkRayTracingPipelineCreateInfoKHR make_VkRayTracingPipelineCreateInfoKHR() { return VkRayTracingPipelineCreateInfoKHR { VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR }; }
VkPhysicalDeviceRayTracingPipelineFeaturesKHR make_VkPhysicalDeviceRayTracingPipelineFeaturesKHR() { return VkPhysicalDeviceRayTracingPipelineFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR }; }
VkPhysicalDeviceRayTracingPipelinePropertiesKHR make_VkPhysicalDeviceRayTracingPipelinePropertiesKHR() { return VkPhysicalDeviceRayTracingPipelinePropertiesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR }; }
#endif

#if VK_KHR_ray_query
VkPhysicalDeviceRayQueryFeaturesKHR make_VkPhysicalDeviceRayQueryFeaturesKHR() { return VkPhysicalDeviceRayQueryFeaturesKHR { VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR }; }
#endif



#if VK_EXT_debug_utils
#if VK_VERSION_1_0
VkObjectType get_vk_object_type(VkBuffer a_vk_handle)
{
    return VK_OBJECT_TYPE_BUFFER;
}
VkObjectType get_vk_object_type(VkImage a_vk_handle)
{
    return VK_OBJECT_TYPE_IMAGE;
}
VkObjectType get_vk_object_type(VkInstance a_vk_handle)
{
    return VK_OBJECT_TYPE_INSTANCE;
}
VkObjectType get_vk_object_type(VkPhysicalDevice a_vk_handle)
{
    return VK_OBJECT_TYPE_PHYSICAL_DEVICE;
}
VkObjectType get_vk_object_type(VkDevice a_vk_handle)
{
    return VK_OBJECT_TYPE_DEVICE;
}
VkObjectType get_vk_object_type(VkQueue a_vk_handle)
{
    return VK_OBJECT_TYPE_QUEUE;
}
VkObjectType get_vk_object_type(VkSemaphore a_vk_handle)
{
    return VK_OBJECT_TYPE_SEMAPHORE;
}
VkObjectType get_vk_object_type(VkCommandBuffer a_vk_handle)
{
    return VK_OBJECT_TYPE_COMMAND_BUFFER;
}
VkObjectType get_vk_object_type(VkFence a_vk_handle)
{
    return VK_OBJECT_TYPE_FENCE;
}
VkObjectType get_vk_object_type(VkDeviceMemory a_vk_handle)
{
    return VK_OBJECT_TYPE_DEVICE_MEMORY;
}
VkObjectType get_vk_object_type(VkEvent a_vk_handle)
{
    return VK_OBJECT_TYPE_EVENT;
}
VkObjectType get_vk_object_type(VkQueryPool a_vk_handle)
{
    return VK_OBJECT_TYPE_QUERY_POOL;
}
VkObjectType get_vk_object_type(VkBufferView a_vk_handle)
{
    return VK_OBJECT_TYPE_BUFFER_VIEW;
}
VkObjectType get_vk_object_type(VkImageView a_vk_handle)
{
    return VK_OBJECT_TYPE_IMAGE_VIEW;
}
VkObjectType get_vk_object_type(VkShaderModule a_vk_handle)
{
    return VK_OBJECT_TYPE_SHADER_MODULE;
}
VkObjectType get_vk_object_type(VkPipelineCache a_vk_handle)
{
    return VK_OBJECT_TYPE_PIPELINE_CACHE;
}
VkObjectType get_vk_object_type(VkPipelineLayout a_vk_handle)
{
    return VK_OBJECT_TYPE_PIPELINE_LAYOUT;
}
VkObjectType get_vk_object_type(VkPipeline a_vk_handle)
{
    return VK_OBJECT_TYPE_PIPELINE;
}
VkObjectType get_vk_object_type(VkRenderPass a_vk_handle)
{
    return VK_OBJECT_TYPE_RENDER_PASS;
}
VkObjectType get_vk_object_type(VkDescriptorSetLayout a_vk_handle)
{
    return VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT;
}
VkObjectType get_vk_object_type(VkSampler a_vk_handle)
{
    return VK_OBJECT_TYPE_SAMPLER;
}
VkObjectType get_vk_object_type(VkDescriptorSet a_vk_handle)
{
    return VK_OBJECT_TYPE_DESCRIPTOR_SET;
}
VkObjectType get_vk_object_type(VkDescriptorPool a_vk_handle)
{
    return VK_OBJECT_TYPE_DESCRIPTOR_POOL;
}
VkObjectType get_vk_object_type(VkFramebuffer a_vk_handle)
{
    return VK_OBJECT_TYPE_FRAMEBUFFER;
}
VkObjectType get_vk_object_type(VkCommandPool a_vk_handle)
{
    return VK_OBJECT_TYPE_COMMAND_POOL;
}
#endif

#if VK_VERSION_1_1
VkObjectType get_vk_object_type(VkSamplerYcbcrConversion a_vk_handle)
{
    return VK_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION;
}
VkObjectType get_vk_object_type(VkDescriptorUpdateTemplate a_vk_handle)
{
    return VK_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE;
}
#endif

#if VK_KHR_surface
VkObjectType get_vk_object_type(VkSurfaceKHR a_vk_handle)
{
    return VK_OBJECT_TYPE_SURFACE_KHR;
}
#endif

#if VK_KHR_swapchain
VkObjectType get_vk_object_type(VkSwapchainKHR a_vk_handle)
{
    return VK_OBJECT_TYPE_SWAPCHAIN_KHR;
}
#endif

#if VK_KHR_display
VkObjectType get_vk_object_type(VkDisplayKHR a_vk_handle)
{
    return VK_OBJECT_TYPE_DISPLAY_KHR;
}
VkObjectType get_vk_object_type(VkDisplayModeKHR a_vk_handle)
{
    return VK_OBJECT_TYPE_DISPLAY_MODE_KHR;
}
#endif

#if VK_KHR_deferred_host_operations
VkObjectType get_vk_object_type(VkDeferredOperationKHR a_vk_handle)
{
    return VK_OBJECT_TYPE_DEFERRED_OPERATION_KHR;
}
#endif

#if VK_EXT_debug_report
VkObjectType get_vk_object_type(VkDebugReportCallbackEXT a_vk_handle)
{
    return VK_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT;
}
#endif

#if VK_NVX_binary_import
VkObjectType get_vk_object_type(VkCuModuleNVX a_vk_handle)
{
    return VK_OBJECT_TYPE_CU_MODULE_NVX;
}
VkObjectType get_vk_object_type(VkCuFunctionNVX a_vk_handle)
{
    return VK_OBJECT_TYPE_CU_FUNCTION_NVX;
}
#endif

#if VK_EXT_debug_utils
VkObjectType get_vk_object_type(VkDebugUtilsMessengerEXT a_vk_handle)
{
    return VK_OBJECT_TYPE_DEBUG_UTILS_MESSENGER_EXT;
}
#endif

#if VK_EXT_validation_cache
VkObjectType get_vk_object_type(VkValidationCacheEXT a_vk_handle)
{
    return VK_OBJECT_TYPE_VALIDATION_CACHE_EXT;
}
#endif

#if VK_NV_ray_tracing
VkObjectType get_vk_object_type(VkAccelerationStructureNV a_vk_handle)
{
    return VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV;
}
#endif

#if VK_INTEL_performance_query
VkObjectType get_vk_object_type(VkPerformanceConfigurationINTEL a_vk_handle)
{
    return VK_OBJECT_TYPE_PERFORMANCE_CONFIGURATION_INTEL;
}
#endif

#if VK_NV_device_generated_commands
VkObjectType get_vk_object_type(VkIndirectCommandsLayoutNV a_vk_handle)
{
    return VK_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NV;
}
#endif

#if VK_EXT_private_data
VkObjectType get_vk_object_type(VkPrivateDataSlotEXT a_vk_handle)
{
    return VK_OBJECT_TYPE_PRIVATE_DATA_SLOT_EXT;
}
#endif

#if VK_KHR_acceleration_structure
VkObjectType get_vk_object_type(VkAccelerationStructureKHR a_vk_handle)
{
    return VK_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR;
}
#endif

#endif // VK_EXT_debug_utils

} // namespace spock

#endif // SPOCK_GENERATED_IMPLEMENTATION

