#pragma once

#include "context.hpp"

#include <string>
#include <set>
#include <string_view>
#include <glm/glm.hpp>
#include <glm/ext/scalar_constants.hpp> 
#include <filesystem>
#include <iosfwd>


namespace spock
{

class context_t;
class command_pool_t;
class queue_t;
class buffer_t;

class camera_t;
struct mip_2d_info_t;
struct mip_2d_layout_t;
class sampler_2d_info_t;

/// direct to vulkan clip space, with reversed depth https://vincent-p.github.io/notes/20201216234910-the_projection_matrix_in_vulkan/
/// fovy in radians
glm::mat4 perspective_reversed_z(float a_fovy, float a_aspect, float a_near_plane, float a_far_plane);


/// camera space: x = right; y = up; z = outward
/// view_mat: 0th row = right, 1st row = up, 2nd row: outward
/// fovy_: in radians
class camera_t {
private:
    bool reversed_z_;
    bool view_outdated_;         // update at most once per frame
    bool proj_outdated_;         // update at most once per frame

    float fovy_;            // radians
    float aspect_;
    float near_plane_;
    float far_plane_;

    glm::vec3 eye_;
    glm::vec3 right_;
    glm::vec3 outward_;
    glm::vec3 up_;

    glm::mat4 view_mat_;
    glm::mat4 inv_view_mat_;
    glm::mat4 proj_mat_;
    glm::mat4 view_proj_mat_;

    void init();

public:
    camera_t();

    camera_t(float a_aspect, const ordered_json& a_camera_config_json);

    camera_t(
        bool a_reversed_z, float a_fovy, float a_aspect, float a_near_plane, float a_far_plane,
        const glm::vec3& a_eye, const glm::vec3& a_up, const glm::vec3& a_outward
    );

    bool reversed_z() const { return reversed_z_; }
    float aspect() const { return aspect_;  }
    float fovy() const { return fovy_;  }
    float near_plane() const { return near_plane_;  }
    float far_plane() const { return far_plane_;  }

    void reverse_z();
    void aspect(float a_aspect);
    void fovy(float a_fovy); /// in radians
    void near_plane(float a_near_plane);
    void far_plane(float a_far_plane);
 
    float ndc_far() const { return reversed_z_ ? 0.f : 1.f; }
    VkCompareOp depth_compare_op() const { return reversed_z_ ? VK_COMPARE_OP_GREATER_OR_EQUAL : VK_COMPARE_OP_LESS_OR_EQUAL; }
    const glm::vec3& eye() const { return eye_; }
    const glm::vec3& right() const { return right_; }
    const glm::vec3& outward() const { return outward_; }
    const glm::vec3& up() const { return up_; }

    bool outdated() const { return view_outdated_ || proj_outdated_; }

    void update();
    const glm::mat4& view_mat() const;
    const glm::mat4& inv_view_mat() const;
    const glm::mat4& view_proj_mat() const;
    const glm::mat4& proj_mat() const;

    /// camera space: x = right; y = up; z = outward
    /// view_mat: 0th row = right, 1st row = up, 2nd row: outward
    void translate(const glm::vec3& a_delta_in_camera_space);
    void rotate(float a_degrees, const glm::vec3& a_axis);
    void rotate(const glm::mat3& a_rotation_mat);


};


class sampler_2d_info_t {
private:
    std::array<VkFilter, 2> mag_min_filters_;
    VkSamplerMipmapMode mipmap_mode_;
    std::array<VkSamplerAddressMode, 2> uv_address_modes_;

public:
    sampler_2d_info_t(const sampler_2d_info_t&) = default;
    sampler_2d_info_t(sampler_2d_info_t&&) = default;
    sampler_2d_info_t& operator= (const sampler_2d_info_t&) = default;
    sampler_2d_info_t& operator= (sampler_2d_info_t&&) = default;

    sampler_2d_info_t();
    sampler_2d_info_t(VkFilter a_filter, VkSamplerMipmapMode a_mipmap_mode, VkSamplerAddressMode a_address_mode);
    sampler_2d_info_t(const std::array<VkFilter, 2>& a_mag_min_filters, VkSamplerMipmapMode a_mipmap_mode, const std::array<VkSamplerAddressMode, 2>& a_uv_address_modes);
    /// advance *a_bin_stream_ptr to the end (one past the last) of sampler_2d_info bin stream
    sampler_2d_info_t(const uint8_t** a_bin_stream_ptr);

    VkFilter mag_filter() const { return mag_min_filters_[0]; }
    VkFilter min_filter() const { return mag_min_filters_[1]; }
    VkSamplerMipmapMode mipmap_mode() const { return mipmap_mode_; }
    VkSamplerAddressMode u_address_mode() const { return uv_address_modes_[0]; }
    VkSamplerAddressMode v_address_mode() const { return uv_address_modes_[1]; }

    void mag_filter(VkFilter a_mag_filter) { mag_min_filters_[0] = a_mag_filter; }
    void min_filter(VkFilter a_min_filter) { mag_min_filters_[1] = a_min_filter; }
    void mipmap_mode(VkSamplerMipmapMode a_mipmap_mode) { mipmap_mode_ = a_mipmap_mode; }
    void u_address_mode(VkSamplerAddressMode a_u_address_mode) { uv_address_modes_[0] = a_u_address_mode; }
    void v_address_mode(VkSamplerAddressMode a_v_address_mode) { uv_address_modes_[1] = a_v_address_mode; }

    std::vector<uint8_t> serialize() const;
    /// advance *a_bin_stream_ptr to the end (one past the last) of sampler_2d_info bin stream
    void deserialize(const uint8_t** a_bin_stream_ptr);
    VkSamplerCreateInfo get_vk_sampler_create_info(float a_max_anisotropy) const;

    constexpr static VkDeviceSize
    calc_serialized_bytes()
    {
        static_assert(sizeof(uint32_t) >= sizeof(VkFilter), "VkFilter cannot be represented by uint32_t");
        static_assert(sizeof(uint32_t) >= sizeof(VkSamplerMipmapMode), "VkFilter cannot be represented by uint32_t");
        static_assert(sizeof(uint32_t) >= sizeof(VkSamplerAddressMode), "VkSamplerAddressMode cannot be represented by uint32_t");

        return 2 * sizeof(uint32_t) + sizeof(uint32_t) + 2 * sizeof(uint32_t);
    }
};

extern const uint8_t k_mip_2d_padding_zeroes[];

uint32_t get_texel_block_bytes(VkFormat a_format);
glm::uvec2 get_texel_block_dim(VkFormat a_format);
bool is_texel_block_compressed(VkFormat a_format);
bool is_format_srgb(VkFormat a_format);

std::vector<VkExtent2D> calc_mip_dims(const VkExtent2D& a_mip0_dim, uint8_t a_num_mips);
std::vector<VkExtent3D> calc_mip_dims(const VkExtent3D& a_mip0_dim, uint8_t a_num_mips);

struct mip_2d_layout_t {
    std::vector<VkExtent2D> dims;
    std::vector<VkExtent2D> padded_dims;
    std::vector<VkDeviceSize> byte_offsets; // byte offset of texel data for each mip level, plus total mip buffer bytes as the last element

    VkDeviceSize get_mip_buffer_bytes() const { return byte_offsets.back(); }
    VkDeviceSize get_mip0_buffer_bytes() const { return byte_offsets[1] - byte_offsets[0]; }
};

bool operator== (const mip_2d_layout_t& a, const mip_2d_layout_t& b);

/// mip_buffer: texel data for all mip levels.
/// only do this right before uploading to staging buffer 
void relocate_mip_buffer(VkDeviceSize a_location, std::vector<VkDeviceSize>& mip_byte_offsets); 

class mip_2d_info_t {
private:
    VkFormat format_;
    VkExtent2D mip0_dim_;
    uint16_t sampler_2d_idx_;
    uint8_t num_mips_;

public:
    /// max texel block size in bytes for all blocked images, cf. vkCmdCopyBufferToImage requirement on blocked images
    static constexpr VkDeviceSize BLOCK_ALIGNMENT_BYTES = 16; 

    mip_2d_info_t(const mip_2d_info_t&) = default;
    mip_2d_info_t& operator= (const mip_2d_info_t&) = default;
    mip_2d_info_t(mip_2d_info_t&&) = default;
    mip_2d_info_t& operator= (mip_2d_info_t&&) = default;

    mip_2d_info_t() : format_{ VK_FORMAT_UNDEFINED }, mip0_dim_{}, sampler_2d_idx_{}, num_mips_{} {}

    /// advance *a_bin_stream_ptr to the end (one past the last) of mip_2d_info bin stream
    mip_2d_info_t(const uint8_t** a_bin_stream_ptr);
    mip_2d_info_t(VkFormat a_format, const VkExtent2D& a_mip0_dim, uint16_t a_sampler_2d_idx = 0, uint8_t a_num_mips = static_cast<uint8_t>(-1));
    mip_2d_info_t(VkFormat a_format, const mip_2d_info_t& a_src_mip_2d_info);

    VkFormat format() const { return format_; }
    uint16_t sampler_2d_idx() const { return sampler_2d_idx_; }
    uint8_t num_mips() const { return num_mips_; }
    VkExtent2D mip0_dim() const { return mip0_dim_; }

    mip_2d_layout_t calc_mip_2d_layout() const;

    /// expensive call, mainly for debug assertion, use with caution
    VkDeviceSize calc_mip_buffer_bytes() const; 

    bool valid() const;
    bool empty() const;
    static VkDeviceSize calc_serialized_bytes();

    std::vector<uint8_t> serialize() const;

    /// advance *a_bin_stream_ptr to the end (one past the last) of mip_2d_info bin stream
    void deserialize(const uint8_t** a_bin_stream_ptr);
    VkImageCreateInfo get_vk_image_create_info() const;
    VkImageViewCreateInfo get_vk_image_view_create_info(VkImage a_vk_image) const;

};


void 
serialize_mip_2d_to_file(
    const sampler_2d_info_t& a_sampler_2d_info, 
    const mip_2d_info_t& a_mip_2d_info, 
    const uint8_t* a_mip_buffer, 
    VkDeviceSize a_mip_buffer_bytes, 
    const std::string& a_file_path
);


void
deserialize_mip_2d_from_file(
    const std::string& a_file_path, 
    sampler_2d_info_t* a_sampler_2d_info, 
    mip_2d_info_t* a_mip_2d_info, 
    std::unique_ptr<uint8_t[]>* a_mip_buffer
);

/// upload full mip levels
[[nodiscard SPOCK_NODISCARD_REASON("returned staging buffer must not be destroyed until upload is done")]]
buffer_t
cmd_upload_to_image_2d(
    const context_t& a_context,
    VkCommandBuffer a_command_buffer,
    const image_t& a_image,
    const mip_2d_layout_t& a_mip_2d_layout,
    const uint8_t* a_mip_2d_buffer,
    VkImageAspectFlags a_image_aspect_flags,
    VkImageLayout a_final_image_layout,
    VkPipelineStageFlags a_next_stages,
    VkAccessFlags a_next_accesses
);


/// copy full mipmap levels in a_staging_mip_buffer to a_dst_image
/// a_dst_image: 
///     init layout:  VK_IMAGE_LAYOUT_UNDEFINED
///     final layout: a_final_image_layout for all mip levels
void cmd_copy_staging_mip_buffer_to_image_2d(
    VkCommandBuffer a_command_buffer,
    const mip_2d_layout_t& a_mip_2d_layout,
    VkBuffer a_staging_mip_buffer,
    VkImage a_dst_image,
    VkImageAspectFlags a_image_aspect_flags,
    VkImageLayout a_final_image_layout,
    VkPipelineStageFlags a_next_stages,
    VkAccessFlags a_next_accesses
);


/// a_mip_2d_info.num_mips() controls how many mip levels will be generated
/// a_src_format: if not undefined and different than a_mip_2d_info.format(), perform format conversion before gen mipmap
/// return the host-visible and host-cached buffer of the generated mipmaps
buffer_t 
dev_gen_mip_2d_buffer(
    const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout, const void* a_src_mip0_buffer,
    VkFormat a_src_mip0_buffer_format = VK_FORMAT_UNDEFINED
);


/// for a 2048x2048 image, ~70ms (30%) faster than i7-9750H
/// a_src_mip_2d_info.format() =  VK_FORMAT_R8G8B8A8_UNORM
/// a_dst_mip_2d_info.format() = VK_FORMAT_BC1_RGB_UNORM_BLOCK
/// a_src_mip0_buffer_format: if not undefined and different than a_src_mip_2d_info.format(), perform format conversion before gen mipmap
std::unique_ptr<uint8_t[]>
dev_gen_mip_and_bc1_encode_2d(
    const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue,
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    VkFormat a_src_mip0_buffer_format = VK_FORMAT_UNDEFINED
);


/// for a 2048x2048 image, ~80ms (47%) faster than i7-9750H (cpu conversion between fixed point and floating very slow)
/// a_src_mip_2d_info.format() =  VK_FORMAT_R8G8B8A8_UNORM
/// a_dst_mip_2d_info.format() = VK_FORMAT_BC3_UNORM_BLOCK
/// a_src_mip0_buffer_format: if not undefined and different than a_src_mip_2d_info.format(), perform format conversion before gen mipmap
std::unique_ptr<uint8_t[]>
dev_gen_mip_and_bc3_encode_2d(
    const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue,
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    VkFormat a_src_mip0_buffer_format = VK_FORMAT_UNDEFINED
);


/// summary: given a mip0 image, generate mipmaps with box filter if needed; then bc encodes the image (all mip levels).
/// a_src_mip_2d_info.num_mips() controls how many mip levels will be generated;
/// a_src_mip_2d_info.format() =  VK_FORMAT_R8G8B8A8_UNORM
/// a_dst_mip_2d_info.format() = VK_FORMAT_BC1_RGB_UNORM_BLOCK
/// a_src_mip0_buffer_format: if not undefined and different than a_src_mip_2d_info.format(), perform format conversion before gen mipmap
std::unique_ptr<uint8_t[]>
gen_mipmap_and_bc1_encode_2d(
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    VkFormat a_src_mip0_buffer_format = VK_FORMAT_UNDEFINED
);


/// a_src_mip_2d_info.format() =  VK_FORMAT_R8G8B8A8_UNORM
/// a_dst_mip_2d_info.format() = VK_FORMAT_BC3_UNORM_BLOCK
/// a_src_mip0_buffer_format: if not undefined and different than a_src_mip_2d_info.format(), perform format conversion before gen mipmap
std::unique_ptr<uint8_t[]>
gen_mipmap_and_bc3_encode_2d(
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    VkFormat a_src_mip0_buffer_format = VK_FORMAT_UNDEFINED
);


/// input format = VK_FORMAT_R8G8B8A8_UNORM
/// output format = VK_FORMAT_BC5_UNORM_BLOCK
std::unique_ptr<uint8_t[]>
xyz_to_bc5_encode_oct_normal_texture (
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout
);


/// input format = VK_FORMAT_R8G8B8A8_SRGB
/// output format = VK_FORMAT_BC1_RGB_UNORM_BLOCK
/// rgbcx BC1 encoder completely ignores the alpha channel
std::unique_ptr<uint8_t[]>
gen_mipmap_and_bc1_alpha_encode_2d(
    const mip_2d_info_t& a_src_mip_2d_info, const void* a_src_mip0_buffer,
    const mip_2d_info_t& a_dst_mip_2d_info, const mip_2d_layout_t& a_dst_mip_2d_layout,
    float a_alpha_cutoff
);


void write_mipmaps_to_bmp(
    const std::string& a_image_path_base, 
    uint32_t a_num_channels, const mip_2d_layout_t& a_mip_2d_layout, const uint8_t* a_mip_buffer
);


void 
write_mipmaps_to_hdr(
    const std::string& a_image_path_base,
    uint32_t a_num_channels, const mip_2d_layout_t& a_mip_2d_layout, const float* a_mip_buffer
);


enum mtl_type_e : uint8_t {
    MTL_TYPE_PRIMARY = 0,
    MTL_TYPE_GRAY_DOT, // gltf mtl name started with "SPOCK_GRAY_DOT", can only be opaque gltf basic material (gray dot: cf. first easter egg)
    MTL_TYPE_ALPHA_CUTOFF,
    MTL_TYPE_BLEND,
    MTL_TYPE_COUNT
}; // mtl_type_e


enum punctual_light_type_e : int32_t {
    PUNCTUAL_LIGHT_TYPE_SPOT = 0,
    PUNCTUAL_LIGHT_TYPE_POINT,
    PUNCTUAL_LIGHT_TYPE_DIRECTIONAL,
    PUNCTUAL_LIGHT_TYPE_COUNT
}; // punctual_light_type_e


std::string_view str_from_mtl_type_e(mtl_type_e);
std::string_view str_from_punctual_light_type_e(punctual_light_type_e);

/// specular glossiness (not supported at the moment)
///     specularglossiness texture:
///         RGB: specular
///         A: glossiness (default: 1)
/// metallicroughness
///     metallicroughness texture:
///         R: occlusion (0: no ambient light; 1: full ambient light)
///         G: roughness
///         B: metalness
/// untextured primitives are converted to textured primitives
/// primary: 
///     metallicroughness: textures { Base, Metallicroughness, Normal },
///     mask mode cutoff = 0.f --> fully opaque; cutoff > 0.0039 ==> mask mode 
///     duplicate indices (reverse order) for double sided, 
///     index of emissive texture in rgba_unorm_buffer, < 256; offset in exported glsl
uint32_t get_mtl_texture_stride(mtl_type_e a_mtl_type); // in terms of mtl_param_type
bool is_mtl_alpha_cutoff(mtl_type_e a_mtl_type);

/// access: VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT
class texture_descriptor_array_t {
private:
    const context_t* context_;
    std::vector<VmaAllocation> vma_allocations_;
    std::vector<VkImage> vk_images_;
    std::vector<VkImageView> vk_image_views_;
    std::vector<VkDescriptorImageInfo> vk_descriptor_image_infos_;

public:
    texture_descriptor_array_t() : context_{ nullptr } {}

    texture_descriptor_array_t(const texture_descriptor_array_t&) = delete;
    texture_descriptor_array_t& operator =(const texture_descriptor_array_t&) = delete;

    texture_descriptor_array_t(texture_descriptor_array_t&&) noexcept;
    texture_descriptor_array_t& operator =(texture_descriptor_array_t&&) noexcept;

    texture_descriptor_array_t(const context_t& a_context);
    ~texture_descriptor_array_t();

    const VkDescriptorImageInfo* image_infos() const { return vk_descriptor_image_infos_.data(); }
    bool consistent() const;
    bool empty() const;
    void reserve(uint32_t num_textures);
 
    /// return the end (one pass the last) of textures bit stream
    const uint8_t* upload(
        const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue, const std::vector<VkSampler>& a_vk_samplers,
        const uint8_t* a_bin_stream, uint64_t a_batch_max_mebibytes,
        VkPipelineStageFlags a_next_stages, VkAccessFlags a_next_accesses
    );

    void upload_one_texture(
        const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue, VkSampler a_vk_sampler, 
        const mip_2d_info_t& a_mip_2d_info, const mip_2d_layout_t& a_mip_2d_layout, const uint8_t* a_mip_2d_buffer,
        VkPipelineStageFlags a_next_stages, VkAccessFlags a_next_accesses
    );
};

using timestamp_t = std::filesystem::file_time_type;

struct mtl_cfg_t {
    std::string name;
    int32_t gltf_model_idx;
    const void* gltf_base_color_image_ptr; // gltf: srgb non-linear, local to gltf_model
    const void* gltf_occlusion_image_ptr;
    const void* gltf_metallic_roughness_image_ptr; // gltf: linear transfer function
    const void* gltf_normal_image_ptr;
    uint16_t base_color_sampler_idx;
    uint16_t occlusion_sampler_idx;
    uint16_t metallic_roughness_sampler_idx;
    uint16_t normal_sampler_idx;
    uint32_t emissive_texture_idx; // gltf: srgb non-linear
    glm::vec4 base_color_factor;
    float occlusion_strength;
    float roughness_factor;
    float metallic_factor;
    bool alpha_mask_enabled;
    float alpha_mask_cutoff;
    bool alpha_blend_enabled;
    float ior;
    bool double_sided;

    mtl_cfg_t () 
        : gltf_model_idx{ -1 }
        , gltf_base_color_image_ptr{ nullptr }
        , gltf_occlusion_image_ptr{ nullptr }
        , gltf_metallic_roughness_image_ptr{ nullptr }
        , gltf_normal_image_ptr{ nullptr }
        , base_color_sampler_idx{ 0 }
        , occlusion_sampler_idx{ 0 }
        , metallic_roughness_sampler_idx{ 0 }
        , normal_sampler_idx{ 0 }
        , emissive_texture_idx{ 0 }
        , base_color_factor{ glm::vec4(1.f) }
        , occlusion_strength{ 1.f }
        , roughness_factor{ 1.f }
        , metallic_factor{ 1.f }
        , alpha_mask_enabled { false }
        , alpha_mask_cutoff { 0.f }
        , alpha_blend_enabled { false }
        , ior { 1.5f }
        , double_sided{ false }
    {}

    mtl_type_e get_mtl_type() const;

};


struct primitive_info_t {
    std::string name;
    const float* position_buffer_ptr;
    const float* normal_buffer_ptr;
    const float* uv_buffer_ptr;
    const void * index_buffer_ptr;
    int32_t position_byte_stride;
    int32_t normal_byte_stride;
    int32_t uv_byte_stride;
    int32_t index_byte_stride;
    spock::mtl_type_e mtl_type;
    int32_t mtl_instance_idx; // within each mtl_type
    bool double_sided;
    int32_t transform_idx;
    int32_t winding; // 1: ccw, -1: cw
    uint32_t num_vertices;
    uint32_t num_indices; // always single-sided

    primitive_info_t() 
        : position_buffer_ptr{ nullptr } , normal_buffer_ptr{ nullptr } , uv_buffer_ptr{ nullptr } , index_buffer_ptr{ nullptr }
        , position_byte_stride{ -1 } , normal_byte_stride{ -1 } , uv_byte_stride{ -1 }, index_byte_stride{ -1 }
        , mtl_type{ spock::MTL_TYPE_COUNT } , mtl_instance_idx{ -1 }, double_sided{ false }
        , transform_idx{ -1 }, winding{ 1 }
        , num_vertices{ 0 } , num_indices{ 0 }
    {}
};


struct directional_light_t {
    glm::vec3 direction;
    float padding0;
    glm::vec3 intensity;
    float padding1;

    static constexpr uint32_t DIRECTIONAL_LIGHT_SIZE_IN_VEC4 = 2;
    static constexpr VkDeviceSize DIRECTIONAL_LIGHT_BYTES = sizeof(glm::vec4) * DIRECTIONAL_LIGHT_SIZE_IN_VEC4;

    directional_light_t(const directional_light_t&) = default;
    directional_light_t& operator= (const directional_light_t&) = default;
    directional_light_t(directional_light_t&&) = default;
    directional_light_t& operator= (directional_light_t&&) = default;
    directional_light_t();
    std::array<uint8_t, DIRECTIONAL_LIGHT_BYTES> serialize() const;
    void deserialize(uint8_t** a_bin_stream);
    static std::string definition_to_glsl_str();
};


inline bool operator==(const directional_light_t& lhs, const directional_light_t& rhs) 
{ 
    return lhs.direction == rhs.direction && lhs.intensity == rhs.intensity; 
}
inline bool operator!=(const directional_light_t& lhs, const directional_light_t& rhs) { return !(lhs == rhs); }


struct point_light_t {
    glm::vec3 position;
    float rcp_range;
    glm::vec3 intensity;
    float padding;

    static constexpr uint32_t POINT_LIGHT_SIZE_IN_VEC4 = 2;
    static constexpr VkDeviceSize POINT_LIGHT_BYTES = POINT_LIGHT_SIZE_IN_VEC4 * sizeof(glm::vec4);

    point_light_t(const point_light_t&) = default;
    point_light_t& operator= (const point_light_t&) = default;
    point_light_t(point_light_t&&) = default;
    point_light_t& operator= (point_light_t&&) = default;
    point_light_t();
    std::array<uint8_t, POINT_LIGHT_BYTES> serialize() const;
    void deserialize(uint8_t** a_bin_stream);
    static std::string definition_to_glsl_str();
};


inline bool operator==(const point_light_t& lhs, const point_light_t& rhs) 
{ 
    return lhs.position == rhs.position && lhs.intensity == rhs.intensity && lhs.rcp_range == rhs.rcp_range;
}
inline bool operator!=(const point_light_t& lhs, const point_light_t& rhs) { return !(lhs == rhs); }


// https://github.com/KhronosGroup/glTF/blob/main/extensions/2.0/Khronos/KHR_lights_punctual/README.md#inner-and-outer-cone-angles

std::pair<float, float> calc_spotlight_falloff_params(float a_inner_cone_angle, float a_outer_cone_angle);


struct spotlight_t {
    glm::vec3 position;
    float rcp_range; // 1 / range
    glm::vec3 direction;
    float angle_scale; // 1.0f / max(0.001f, cos(innerConeAngle) - cos(outerConeAngle))
    glm::vec3 intensity; // gltf_light.color * gltf_light.intensity
    float angle_offset; // -cos(outerConeAngle) * lightAngleScale

    static constexpr uint32_t SPOTLIGHT_SIZE_IN_VEC4 = 3;
    static constexpr VkDeviceSize SPOTLIGHT_BYTES = SPOTLIGHT_SIZE_IN_VEC4 * sizeof(glm::vec4);

    spotlight_t(const spotlight_t&) = default;
    spotlight_t& operator= (const spotlight_t&) = default;
    spotlight_t(spotlight_t&&) = default;
    spotlight_t& operator= (spotlight_t&&) = default;
    spotlight_t();
    std::array<uint8_t, SPOTLIGHT_BYTES> serialize() const;
    void deserialize(uint8_t** a_bin_stream);
    static std::string definition_to_glsl_str();
};

inline bool operator==(const spotlight_t& lhs, const spotlight_t& rhs)
{
    return lhs.position == rhs.position && lhs.direction == rhs.direction 
        && lhs.intensity == rhs.intensity && lhs.rcp_range == rhs.rcp_range 
        && lhs.angle_scale == rhs.angle_scale && lhs.angle_offset == rhs.angle_offset;
}
inline bool operator!=(const spotlight_t& lhs, const spotlight_t& rhs) { return !(lhs == rhs); }


class sampler_array_t {
private:
    VkDevice vk_dev_;
    std::vector<VkSampler> vk_samplers_;

public:
    sampler_array_t(const sampler_array_t&) = delete;
    sampler_array_t& operator= (const sampler_array_t&) = delete;

    sampler_array_t(sampler_array_t&&) noexcept;
    sampler_array_t& operator= (sampler_array_t&&) noexcept;
    sampler_array_t() : vk_dev_{ VK_NULL_HANDLE } {}
    sampler_array_t(const context_t& a_context, const std::vector<sampler_2d_info_t>& a_sampler_2d_infos);
    const uint32_t size() const { return static_cast<uint32_t>(vk_samplers_.size()); }
    VkSampler operator[](size_t i) const { return vk_samplers_[i]; }
    const std::vector<VkSampler>& vk_samplers() const { return vk_samplers_; }
    ~sampler_array_t();
};


/*
support for r32g32b32 as uniform_texel_buffer:

apple (mac, ios): no
Mali: at least since Mali-G78
adreno: since 630
intel: at least since hd 620
nvidia: yes (at least since gt 750)
amd: yes (at least since r7 200)
*/
enum spock_scene_flag_bits_e : int32_t {
    SPOCK_SCENE_POSITION_NORMAL_UNIFORM_TEXEL_BUFFER_BIT = 0x00000001, // position and normal buffer usages have VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT
    SPOCK_SCENE_POSITION_NORMAL_STORAGE_BUFFER_BIT       = 0x00000002, // position and normal buffer usages have VK_BUFFER_USAGE_STORAGE_BUFFER_BIT (in case R32G32B32_SFLOAT does not support TEXEL_BUFFER_BIT)
    SPOCK_SCENE_ALIGNED_BUFFER_UNIFORM_TEXEL_BUFFER_BIT  = 0x00000004, // uv, index, triangle_mtl_idx, punctual_light and mtl_param buffer usages have VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT
    SPOCK_SCENE_ALIGNED_BUFFER_STORAGE_BUFFER_BIT        = 0x00000008, // uv, index, triangle_mtl_idx, punctual_light and mtl_param buffer usages have VK_BUFFER_USAGE_STORAGE_BUFFER_BIT
    SPOCK_SCENE_RAY_TRACING_SUPPORT_BIT                  = 0x00000010, // position and index buffer usages have VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR
    SPOCK_SCENE_FLAG_BITS_MAX_ENUM                       = 0x7FFFFFFF
};
using spock_scene_flags_t = int32_t;

class scene_t {
private:
    static constexpr uint32_t SCENE_CACHE_VERSION = 20211005;

    VkBufferUsageFlags position_buffer_usage_{};
    VkBufferUsageFlags normal_buffer_usage_{};
    VkBufferUsageFlags uv_buffer_usage_{};
    VkBufferUsageFlags index_buffer_usage_{};
    VkBufferUsageFlags triangle_mtl_idx_buffer_usage_{};
    VkBufferUsageFlags offsets_of_mtl_type_buffer_usage_{};
    VkBufferUsageFlags punctual_light_buffer_usage_{};
    VkBufferUsageFlags mtl_param_buffer_usage_{};

    // offsets_of_mtl_type_t must be padded to glm::u32vec4
    struct offsets_of_mtl_type_t {
        uint32_t mtl_param_emissive_idx;
        uint32_t mtl_param_alpha_cutoff;
        uint32_t texture_stride;
        uint32_t texture;
        uint32_t triangle_idx;
        uint32_t pad0;
        uint32_t pad1;
        uint32_t pad2;
    };

    std::string identifier_;
    const context_t* context_;

    std::unique_ptr<glm::vec4[]> unique_punctual_light_buffer_;
    std::array<int16_t, PUNCTUAL_LIGHT_TYPE_COUNT + 1> punctual_light_counts_;
    std::array<uint32_t, PUNCTUAL_LIGHT_TYPE_COUNT + 1> punctual_light_offsets_; // in terms of vec4 count
    buffer_t punctual_light_buffer_; // uniform buffer, size must be less than maxUniformBufferRange (>= 16384 bytes)

    buffer_t offsets_of_mtl_type_buffer_;

    glm::vec3 aabb_lower_;
    glm::vec3 aabb_upper_;
    std::array<uint32_t, MTL_TYPE_COUNT + 1> vertex_offsets_; // vertex offset for each mtl_type
    std::array<uint32_t, MTL_TYPE_COUNT + 1> index_offsets_; // index offset for each mtl_type
    std::array<uint32_t, MTL_TYPE_COUNT + 1> mtl_param_emissive_idx_offsets_; // in terms of mtl_param_type count, total count include padding
    std::array<uint32_t, MTL_TYPE_COUNT + 1> mtl_param_alpha_cutoff_offsets_; // in terms of mtl_param_type count, total count include padding

    // TODO: uint64_t position_buffer_; uint32_t (oct encode) normal_buffer_; sort by morton codes
    buffer_t position_buffer_; // position_type
    buffer_t normal_buffer_; // normal_type
    buffer_t uv_buffer_; // uv_type, will be generated for gltf primitives w/o
    buffer_t index_buffer_; // index_type 

    buffer_view_t position_buffer_view_;
    buffer_view_t normal_buffer_view_;
    buffer_view_t uv_buffer_view_;
    buffer_view_t index_buffer_view_;

    buffer_t triangle_mtl_idx_buffer_; // uint16_t (VK_FORMAT_R16_UINT), higher 4 bits: mtl type, lower 12 bits: mtl instance index
    buffer_view_t triangle_mtl_idx_buffer_view_;

    /// in the order of:
    /// mtl_param_emissive_idx: emissive texture index(uint32_t, default: 0 ==> black)
    /// mtl_param_alpha_cutoff: alpha mask cutoff (float, default: 0)
    /// TODO: blend support
    buffer_t mtl_param_buffer_; // mtl_param_type[], uniform texel buffer
    buffer_view_t mtl_param_buffer_view_;

    sampler_array_t sampler_array_;

    uint32_t num_emissive_textures_;
    std::array<uint32_t, MTL_TYPE_COUNT> texture_strides_;
    std::array<uint32_t, MTL_TYPE_COUNT + 1> texture_offsets_; // texture index offset for each mtl_type
    texture_descriptor_array_t texture_descriptor_array_; // first emissive textures, then textures for each material type

    void init_buffer_usage_flags(spock_scene_flags_t a_scene_flags);
    uint32_t get_curr_cache_version() const;
    std::vector<const char*> deserialize_gltf_paths(const uint8_t** a_bin_stream_ptr); // *a_bin_stream_ptr: points to the beginning of gltf_paths_count
    void serialize_gltf_paths(const std::vector<std::string>& a_file_paths, std::ofstream& a_ofs); // serialize gltf_paths_bytes, then gltf_paths_count and cstrs

    void load_sampler_from_cache(const uint8_t** a_bin_stream);
    void load_from_cache(const command_pool_t& a_command_pool, const queue_t& a_queue, const uint8_t** a_bin_stream);

    // return null if cache outdated
    std::unique_ptr<uint8_t[]> get_cache_body(
        const std::string& a_cache_path,
        const std::vector<std::string>& a_gltf_paths,
        const directional_light_t& a_fallback_directional_light
    );

    void serialize_buffers(
        const command_pool_t& a_command_pool, const queue_t& a_queue,
        const std::vector<mtl_cfg_t>* a_mtl_cfgs,
        const uint32_t* a_vertex_counts, const uint32_t* a_index_counts,
        const std::vector<primitive_info_t>& a_primitive_infos, const std::vector<glm::mat4>& a_mesh_transforms,
        std::ofstream& a_ofs
    );

    void serialize_emissive_image(
        const command_pool_t& a_command_pool, const queue_t& a_queue,
        const glm::vec3& a_factor, const uint8_t* a_gltf_buffer, const mip_2d_info_t& a_dst_mip_2d_info, 
        std::ofstream& a_ofs
    );

    void serialize_base_color_image(
        const command_pool_t& a_command_pool, const queue_t& a_queue,
        const mtl_cfg_t& a_mtl_cfg, const uint8_t* a_gltf_buffer, const mip_2d_info_t& a_dst_mip_2d_info, 
        std::ofstream& a_ofs
    );

    void serialize_metallic_roughness_image(
        const command_pool_t& a_command_pool, const queue_t& a_queue,
        const mtl_cfg_t& a_mtl_cfg, 
        const uint8_t* a_metallic_roughness_buffer, const uint8_t* a_occlusion_buffer, const mip_2d_info_t& a_dst_mip_2d_info,
        std::ofstream& a_ofs
    );

    void serialize_normal_image(
        const command_pool_t& a_command_pool, const queue_t& a_queue,
        const mtl_cfg_t& a_mtl_cfg, 
        const uint8_t* a_gltf_buffer, const mip_2d_info_t& a_dst_mip_2d_info,
        std::ofstream& a_ofs
    );

    std::array<offsets_of_mtl_type_t, MTL_TYPE_COUNT> assemble_offsets_of_mtl_types() const;

    void write_glsl(const std::string& a_glsl_path) const;

    /// scene binary file layout:
    ///     uint32_t: version (yyyymmddb); // digit b: converted_to_textured_mtl_ (0 or 1)
    ///     uint32_t: num_material_types (verification: should be MTL_TYPE_COUNT)
    ///     uint32_t: gltf_paths_bytes (size does not include itself)
    ///     uint16_t: gltf_paths_count
    ///     uint8_t[gltf_paths_bytes]: gltf_paths_count of null-terminated strings 
    ///     int16_t[PUNCTUAL_LIGHT_TYPE_COUNT + 1]: punctual_light_counts_ // if num_total_lights == -1: scene has exactly one directional_light, namely the user-provided fallback
    ///     uint32_t[PUNCTUAL_LIGHT_TYPE_COUNT + 1]: punctual_light_offsets_ 
    ///     vec4[light_size_in_vec4]: unique_punctual_light_buffer (light_size_in_vec4 = punctual_light_offsets_[PUNCTUAL_LIGHT_TYPE_COUNT])
    ///     float[3]: aabb_lower_ // cache_body starts from this line
    ///     float[3]: aabb_upper_
    ///     uint32_t[num_material_types + 1]: vertex_offsets_
    ///     uint32_t[num_material_types + 1]: index_offsets_
    ///     uint32_t[num_material_types + 1]: mtl_param_emissive_idx_offsets_
    ///     uint32_t[num_material_types + 1]: mtl_param_alpha_cutoff_offsets_
    ///     uint32_t: num_emissive_textures_; // at least one black texture
    ///     uint32_t[MTL_TYPE_COUNT]: texture_strides_
    ///     uint32_t[MTL_TYPE_COUNT + 1]: texture_offsets_; // texture index offset for each mtl_type
    ///     position_buffer
    ///     normal_buffer
    ///     uv_buffer
    ///     index_buffer
    ///     triangle_mtl_idx_buffer
    ///     mtl_param_buffer_; // scene_t::mtl_param_type[], uniform texel buffer
    ///     uint16_t: num_sampler_2d_info
    ///     sampler_2d_info_t[num_samplers]
    ///     texture_descriptor_array_
    ///         uint16_t: num_textures; 
    ///         mip_2d_info_t[num_textures];
    ///         mip_buffer[num_textures]; (with padding in between if necessary; alignment: mip_2d_info_t::BLOCK_ALIGNMENT_BYTES)
    ///     uint32_t: curr_cache_version // verify integrity
    void create_cache(
        const command_pool_t& a_command_pool, const queue_t& a_queue, 
        const std::vector<std::string>& a_gltf_paths, const std::string_view& a_cache_path,
        const directional_light_t& a_fallback_directional_light,
        bool* a_cache_created
    );

public:

    using position_type = glm::vec3;
    using normal_type = glm::vec3;
    using uv_type = glm::vec2;
    using index_type = uint32_t;

    using triangle_mtl_idx_type = uint16_t; // triangle_mtl_idx_buffer_, cf. scene.cpp: anonymous_namespace::calc_triangle_mtl_idx(...)
    static constexpr uint32_t k_mtl_type_idx_bit_count = 4;
    static constexpr uint32_t k_mtl_instance_idx_bit_count = sizeof(triangle_mtl_idx_type) * 8 - spock::scene_t::k_mtl_type_idx_bit_count;
    static constexpr uint64_t k_mtl_type_idx_max = (1ULL << k_mtl_type_idx_bit_count) - 1ULL;
    static constexpr uint64_t k_mtl_instance_idx_max = (1ULL << k_mtl_instance_idx_bit_count) - 1ULL;
    static constexpr VkFormat k_vk_format_triangle_mtl_idx = VK_FORMAT_R16_UINT;

    using mtl_param_type = uint32_t;
    static constexpr VkFormat k_vk_format_mtl_param = VK_FORMAT_R32_UINT;

    static constexpr VkFormat k_vk_format_position = VK_FORMAT_R32G32B32_SFLOAT;
    static constexpr VkFormat k_vk_format_normal = VK_FORMAT_R32G32B32_SFLOAT;
    static constexpr VkFormat k_vk_format_uv = VK_FORMAT_R32G32_SFLOAT;
    static constexpr VkFormat k_vk_format_index = VK_FORMAT_R32G32B32_UINT;

    static constexpr VkIndexType k_vk_index_type = VK_INDEX_TYPE_UINT32;
    static constexpr uint32_t k_vertex_binding_count = 3; // vertex, normal, uv
    static constexpr uint32_t k_mtl_param_emissive_idx_stride = 1; // in terms of mtl_param_type
    static constexpr uint32_t k_mtl_param_alpha_cutoff_stride = 1; // in terms of mtl_param_type
    static constexpr uint32_t k_mtl_param_alignment = 4; // in terms of mtl_param_type; between different param types, e.g. emissive_idx and alpha_cutoff

    static constexpr VkDeviceSize k_maxUniformBufferRange_min = 16384; // vkspec-1.2.187: ch42: Table 53. Required Limits

    scene_t(const scene_t&) = delete;
    scene_t& operator= (const scene_t&) = delete;

    scene_t(scene_t&&) = default;
    scene_t& operator= (scene_t&&) = default;
    scene_t() : context_{}, punctual_light_counts_{}, punctual_light_offsets_{}, aabb_lower_{}, aabb_upper_{} , vertex_offsets_{}, index_offsets_{}, 
        mtl_param_emissive_idx_offsets_{}, mtl_param_alpha_cutoff_offsets_{}, num_emissive_textures_{} , texture_strides_{}, texture_offsets_{} 
    {}

    /// if a_convert_to_textured_mtl = true, create uv0 and textures for untextured primitives
    scene_t(
        const context_t& a_context, const command_pool_t& a_command_pool, const queue_t& a_queue,
        uint32_t a_gltf_count, const std::string* a_gltf_paths, const std::string& a_cache_path, const std::string& a_glsl_path,
        const std::string & a_identifier = {}, spock_scene_flags_t a_scene_flags = {},
        const directional_light_t& a_fallback_directional_light = directional_light_t{}
    ); 

    const std::string& identifier() const { return identifier_; }
    const std::array<uint32_t, MTL_TYPE_COUNT + 1>& index_offsets() const { return index_offsets_; }
    const std::array<uint32_t, MTL_TYPE_COUNT + 1>& vertex_offsets() const { return vertex_offsets_; }

    static vertex_input_descriptions_t get_vertex_input_descriptions(vertex_input_flags_t a_flags);
    vertex_bindings_t get_vertex_bindings(vertex_input_flags_t a_flags) const;

    index_binding_t get_index_binding() const;

    std::pair<uint32_t, const directional_light_t*> get_directional_lights() const;
    std::pair<uint32_t, const point_light_t*> get_point_lights() const;
    std::pair<uint32_t, const spotlight_t*> get_spotlights() const;

    size_t get_position_buffer_bytes() const { return vertex_offsets_[MTL_TYPE_COUNT] * sizeof(position_type); }
    const VkBuffer position_buffer() const { return position_buffer_; }
    const VkBufferView* position_buffer_view() const { return position_buffer_view_.handle(); }

    size_t get_normal_buffer_bytes() const { return vertex_offsets_[MTL_TYPE_COUNT] * sizeof(normal_type); }
    const VkBuffer normal_buffer() const { return normal_buffer_; }
    const VkBufferView* normal_buffer_view() const { return normal_buffer_view_.handle(); }

    size_t get_uv_buffer_bytes() const { return vertex_offsets_[MTL_TYPE_COUNT] * sizeof(uv_type); }
    const VkBuffer uv_buffer() const { return uv_buffer_; }
    const VkBufferView* uv_buffer_view() const { return uv_buffer_view_.handle(); }

    size_t get_index_buffer_bytes() const { return index_offsets_[MTL_TYPE_COUNT] * sizeof(index_type); }
    const VkBuffer index_buffer() const { return index_buffer_; }
    const VkBufferView* index_buffer_view() const { return index_buffer_view_.handle(); }

    size_t get_triangle_mtl_idx_buffer_bytes() const { return index_offsets_[MTL_TYPE_COUNT] / 3 * sizeof(uint16_t); }
    const VkBuffer triangle_mtl_idx_buffer() const { return triangle_mtl_idx_buffer_; }
    const VkBufferView* triangle_mtl_idx_buffer_view() const { return triangle_mtl_idx_buffer_view_.handle(); }

    size_t get_offsets_of_mtl_type_buffer_bytes() const { return MTL_TYPE_COUNT * sizeof(offsets_of_mtl_type_t);}
    VkBuffer offsets_of_mtl_type_buffer() const { return offsets_of_mtl_type_buffer_; }

    size_t get_mtl_param_buffer_bytes() const;
    const VkBufferView* mtl_param_buffer_view() const { return mtl_param_buffer_view_.handle(); }
    VkBuffer mtl_param_buffer() const { return mtl_param_buffer_; }

    size_t get_punctual_light_buffer_bytes() const { return punctual_light_offsets_.back() * sizeof(glm::vec4); }
    VkBuffer punctual_light_buffer() const { return punctual_light_buffer_; }

    uint32_t get_num_textures() const { return texture_offsets_[MTL_TYPE_COUNT]; }
    const VkDescriptorImageInfo* image_infos() const { return texture_descriptor_array_.image_infos(); }

    const glm::vec3& aabb_lower() const { return aabb_lower_; }
    const glm::vec3& aabb_upper() const { return aabb_upper_; }
    const glm::vec3 calc_aabb() const { return aabb_upper_ - aabb_lower_; }


};



} // namespace spock

